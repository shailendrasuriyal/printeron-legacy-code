//
//  TouchIDAuth.h
//  PrinterOn
//
//  Created by Mark Burns on 2014-09-24.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@protocol TouchIDAuthDelegate

- (void)didFinishAuthWithSuccess;

@optional

- (void)didFinishAuthWithError;

@end

@interface TouchIDAuth : NSObject

+ (BOOL)isTouchIDAvailable;
+ (void)authenticateTouchIDWithTitle:(NSString *)title setDelegate:(id <TouchIDAuthDelegate>)delegate;

@end
