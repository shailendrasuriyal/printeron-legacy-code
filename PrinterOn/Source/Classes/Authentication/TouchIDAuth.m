//
//  TouchIDAuth.m
//  PrinterOn
//
//  Created by Mark Burns on 2014-09-24.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "TouchIDAuth.h"

#import <LocalAuthentication/LocalAuthentication.h>

NSString* const keychainItemIdentifier = @"fingerprintKeychainEntry";

@implementation TouchIDAuth

+ (BOOL)isTouchIDAvailable
{
    if ([LAContext class]) {
        LAContext *authContext = [LAContext new];
        if ([authContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]) {
            return YES;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

+ (void)setupKeychainItem
{
    // Create the keychain entry attributes.
    NSMutableDictionary	*attributes = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        (__bridge id)(kSecClassGenericPassword), kSecClass,
                                        keychainItemIdentifier, kSecAttrAccount,
                                        [NSBundle mainBundle].bundleIdentifier, kSecAttrService, nil];

    // Require a fingerprint scan or passcode validation when the keychain entry is read.
    // Apple also offers an option to destroy the keychain entry if the user ever removes the
    // passcode from his iPhone, but we don't need that option here.
    CFErrorRef accessControlError = NULL;
    SecAccessControlRef accessControlRef = SecAccessControlCreateWithFlags(kCFAllocatorDefault,
                                                                           kSecAttrAccessibleWhenUnlockedThisDeviceOnly,
                                                                           kSecAccessControlUserPresence,
                                                                           &accessControlError);

    if (accessControlRef == NULL || accessControlError != NULL) {
        NSLog(@"Cannot create SecAccessControlRef for identifier “%@” in the key chain: %@.", keychainItemIdentifier, accessControlError);
        return;
    }

    attributes[(__bridge id)kSecAttrAccessControl] = (__bridge id)accessControlRef;
    attributes[(__bridge id)kSecUseNoAuthenticationUI] = @YES;
    attributes[(__bridge id)kSecValueData] = [@"4324b13d49921337a814c0bd66dce490" dataUsingEncoding:NSUTF8StringEncoding];

    OSStatus osStatus = SecItemAdd((__bridge CFDictionaryRef)attributes, NULL);

    if (osStatus != noErr && osStatus != errSecDuplicateItem) {
        NSError * error = [[NSError alloc] initWithDomain:NSOSStatusErrorDomain code:osStatus userInfo:nil];
        NSLog(@"Adding identifier “%@” to keychain failed with OSError %d: %@.", keychainItemIdentifier, (int)osStatus, error);
    }
}

+ (void)authenticateTouchIDWithTitle:(NSString *)title setDelegate:(id <TouchIDAuthDelegate>)delegate
{
    // Make sure we have a fingerprint keychain item to validate against.
    [TouchIDAuth setupKeychainItem];

    // The keychain operation shall be performed by the global queue. Otherwise nothing might happen.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {

        // Create the keychain query attributes
        NSMutableDictionary * query = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       (__bridge id)(kSecClassGenericPassword), kSecClass,
                                       keychainItemIdentifier, kSecAttrAccount,
                                       [NSBundle mainBundle].bundleIdentifier, kSecAttrService,
                                       title, kSecUseOperationPrompt,
                                       nil];

        // Start the query and the fingerprint scan and/or device passcode validation.
        OSStatus userPresenceStatus = SecItemCopyMatching((__bridge CFDictionaryRef)query, NULL);

        // Ignore the found content of the key chain entry and only evaluate the return code.
        if (userPresenceStatus == noErr) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (delegate) [delegate didFinishAuthWithSuccess];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (delegate) [delegate didFinishAuthWithError];
            });
        }
    });
}

@end
