//
//  ThemeLoader.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "ThemeLoader.h"

#import "GCDSingleton.h"

@interface ThemeLoader ()

@property (nonatomic, strong, readwrite) NSDictionary *theme;
@property (nonatomic, strong, readwrite) NSCache *colorCache;

@end

@implementation ThemeLoader

SINGLETON_GCD(ThemeLoader);

- (instancetype)init
{
    if (self = [super init]) {
        self.colorCache = [NSCache new];
    }
    return self;
}

+ (void)configThemeWithPlist:(NSString *)name
{
    NSString *themeFilePath = [[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    [self sharedThemeLoader].theme = [NSDictionary dictionaryWithContentsOfFile:themeFilePath];
}

#pragma mark - For-Key-Methods
    
+ (BOOL)boolForKey:(NSString *)key
{
    id obj = [[self sharedThemeLoader] objectForKey:key];
    if (obj == nil) {
        return NO;
    }
    return [obj boolValue];
}
    
+ (NSString *)stringForKey:(NSString *)key
{
    id obj = [[self sharedThemeLoader] objectForKey:key];
    if (obj == nil) {
        return nil;
    }
    if ([obj isKindOfClass:[NSString class]]) {
        return obj;
    }
    if ([obj isKindOfClass:[NSNumber class]]) {
        return [obj stringValue];
    }
    return nil;
}
    
+ (NSInteger)integerForKey:(NSString *)key
{
    id obj = [[self sharedThemeLoader] objectForKey:key];
    if (obj == nil) {
        return 0;
    }
    return [obj integerValue];
}
    
+ (CGFloat)floatForKey:(NSString *)key
{
    id obj = [[self sharedThemeLoader] objectForKey:key];
    if (obj == nil) {
        return  0.0f;
    }
    return [obj floatValue];
}
    
+ (NSTimeInterval)timeIntervalForKey:(NSString *)key
{
    id obj = [[self sharedThemeLoader] objectForKey:key];
    if (obj == nil) {
        return 0.0;
    }
    return [obj doubleValue];
}
    
+ (UIColor *)colorForKey:(NSString *)key
{
    ThemeLoader *loader = [self sharedThemeLoader];
    
    UIColor *cachedColor = [loader.colorCache objectForKey:key];
    if (cachedColor != nil) {
        return cachedColor;
    }
    
    NSString *colorString = [loader stringForKey:key];
    UIColor *color = colorWithHexString(colorString);
    if (color == nil) {
        color = [UIColor blackColor];
    }
    
    [loader.colorCache setObject:color forKey:key];
    
    return color;
}
    
+ (CGPoint)pointForKey:(NSString *)key
{
    CGFloat pointX = [self floatForKey:[key stringByAppendingString:@".x"]];
    CGFloat pointY = [self floatForKey:[key stringByAppendingString:@".y"]];
    
    CGPoint point = CGPointMake(pointX, pointY);
    return point;
}
    
+ (CGSize)sizeForKey:(NSString *)key
{
    CGFloat width = [self floatForKey:[key stringByAppendingString:@".width"]];
    CGFloat height = [self floatForKey:[key stringByAppendingString:@".height"]];
    
    CGSize size = CGSizeMake(width, height);
    return size;
}
    
+ (CGRect)rectForKey:(NSString *)key
{
    CGPoint origin = [self pointForKey:key];
    CGSize size = [self sizeForKey:key];
    
    CGRect rect = CGRectMake(origin.x, origin.y, size.width, size.height);
    return rect;
}
    
#pragma mark - Helper-Methods
    
- (NSString *)stringForKey:(NSString *)key
{
    id obj = [self objectForKey:key];
    if (obj == nil) {
        return nil;
    }
    if ([obj isKindOfClass:[NSString class]]) {
        return obj;
    }
    if ([obj isKindOfClass:[NSNumber class]]) {
        return [obj stringValue];
    }
    return nil;
}
    
- (id)objectForKey:(NSString *)key
{
    id obj = [self.theme valueForKeyPath:key];
    return obj;
}
    
    
static UIColor *colorWithHexString(NSString *hexString)
{
    if (stringIsEmpty(hexString)) {
        return [UIColor blackColor];
    }
        
    NSMutableString *s = hexString.mutableCopy;
    [s replaceOccurrencesOfString:@"#" withString:@"" options:0 range:NSMakeRange(0, hexString.length)];
    CFStringTrimWhitespace((__bridge CFMutableStringRef)s);
        
    NSString *redString = [s substringToIndex:2];
    NSString *greenString = [s substringWithRange:NSMakeRange(2, 2)];
    NSString *blueString = [s substringWithRange:NSMakeRange(4, 2)];
        
    unsigned int red = 0, green = 0, blue = 0;
    [[NSScanner scannerWithString:redString] scanHexInt:&red];
    [[NSScanner scannerWithString:greenString] scanHexInt:&green];
    [[NSScanner scannerWithString:blueString] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(CGFloat)red/255.0f green:(CGFloat)green/255.0f blue:(CGFloat)blue/255.0f alpha:1.0f];
}
    
static BOOL stringIsEmpty(NSString *s)
{
    return s == nil || s.length == 0;
}

@end
