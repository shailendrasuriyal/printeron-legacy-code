//
//  ThemeLoader.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface ThemeLoader : NSObject

+ (ThemeLoader *)sharedThemeLoader;

+ (void)configThemeWithPlist:(NSString *)name;
    
+ (BOOL)boolForKey:(NSString *)key;
+ (NSString *)stringForKey:(NSString *)key;
+ (NSInteger)integerForKey:(NSString *)key;
+ (CGFloat)floatForKey:(NSString *)key;
+ (NSTimeInterval)timeIntervalForKey:(NSString *)key;
+ (UIColor *)colorForKey:(NSString *)key;
+ (CGPoint)pointForKey:(NSString *)key;
+ (CGSize)sizeForKey:(NSString *)key;
+ (CGRect)rectForKey:(NSString *)key;

@end
