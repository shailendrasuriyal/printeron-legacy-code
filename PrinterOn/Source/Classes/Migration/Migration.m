//
//  Migration.m
//  PrinterOn
//
//  Created by Mark Burns on 2/24/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "Migration.h"

#import "EmailAccount.h"
#import "NetworkBrowser.h"
#import "Service.h"
#import "UserAccount.h"

@implementation Migration

+ (void)performMigration
{
    NSString *lastRunVersion = [Migration getLastRunVersion];
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];

    if ([Migration isVersion:currentVersion higherThan:lastRunVersion]) {
        // Use a switch without breaks for each case so that if you are migrating from an older version you
        // will do all of the migration code from the previous version all the way to the current.
        NSArray *versions = @[@"3.0.2", @"3.0.3", @"3.0.4", @"3.1", @"3.1.1", @"3.2", @"3.2.1", @"3.2.2", @"3.2.3", @"3.3", @"3.3.1", @"3.3.2", @"3.3.3", @"3.4", @"3.4.1", @"3.4.2", @"3.4.3", @"3.4.4"];
        int versionNum = (int)[versions indexOfObject:lastRunVersion];
        switch (versionNum) {
            case 0: // 3.0.2 to 3.0.3
                [Migration cleanOldAppDataAndSettings];
            case 1: // 3.0.3 to 3.0.4
                [Migration updateServiceURLs];
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17: // 3.4.4 to 3.5
            {
                [Migration removeAllServiceCapabilities];
                [Migration updateUserAccountsToUniqueID];
                [Migration removeGMailAuthentication];
            }
            default:
                break;
        }

        // Store the current version so we don't do migration next run
        [Migration updateVersion];
    }
}

+ (NSString *)getLastRunVersion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ver = [defaults stringForKey:@"CURRENT_VERSION"];
    return !ver ? @"3.0.2" : ver;
}

+ (void)updateVersion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"] forKey:@"CURRENT_VERSION"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isVersion:(NSString *)oneString higherThan:(NSString *)otherString {
    // LOWER
    if ([oneString compare:otherString options:NSNumericSearch] == NSOrderedAscending) {
        return NO;
    }
    
    // EQUAL
    if ([oneString compare:otherString options:NSNumericSearch] == NSOrderedSame) {
        return NO;
    }
    
    //HIGHER
    return YES;
}

+ (void)cleanOldAppDataAndSettings
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults removeObjectForKey:@"userEmail"];
    [defaults removeObjectForKey:@"userPassword"];
    [defaults removeObjectForKey:@"useCredsInRequest"];
    [defaults removeObjectForKey:@"usePasswordForRequests"];
    [defaults removeObjectForKey:@"dirSearchManualURL"];
    [defaults removeObjectForKey:@"useManualDirSearchURL"];
    [defaults removeObjectForKey:@"docAPIManualURL"];
    [defaults removeObjectForKey:@"useManualDocAPIURL"];
    [defaults removeObjectForKey:@"numberResults"];
    [defaults removeObjectForKey:@"distToSearch"];
    [defaults removeObjectForKey:@"loc_Units"];
    [defaults removeObjectForKey:@"searchTimeout"];
    [defaults removeObjectForKey:@"url"];
    [defaults removeObjectForKey:@"useGoogleAnalytics"];
    [defaults removeObjectForKey:@"VERSION_SETTING"];
    [defaults removeObjectForKey:@"firstLaunch"];

    [[NSUserDefaults standardUserDefaults] synchronize];

    NSArray *paths = @[[RKApplicationDataDirectory() stringByAppendingPathComponent:@"PrintSpots.sqlite"], [RKApplicationDataDirectory() stringByAppendingPathComponent:@"PrintSpots.sqlite-shm"], [RKApplicationDataDirectory() stringByAppendingPathComponent:@"PrintSpots.sqlite-wal"]];
    for (NSString *storePath in paths) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:storePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
        }
    }
}

+ (void)updateServiceURLs
{
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;

    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];

    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

    if (results.count > 0) {
        for (Service *service in results) {
            service.serviceURL = [Service cleanServiceURL:service.serviceURL];
        }

        // Save to persistence
        NSError *error;
        [context saveToPersistentStore:&error];
        if (error) NSLog(@"Error saving service: %@", error);

        // Requery the network services because we may have updated a service that can now be trusted
        [NetworkBrowser requeryPONNetworkServices];
    }
}

+ (void)removeAllServiceCapabilities
{
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"ServiceCapabilities"];
    fetchRequest.includesPropertyValues = NO;

    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

    if (results.count > 0) {
        for (NSManagedObject *object in results) {
            [context deleteObject:object];
        }

        // Save to persistence
        NSError *error;
        [context saveToPersistentStore:&error];
        if (error) NSLog(@"Error clearing service capabilities during migration: %@", error);
    }
}

+ (void)updateUserAccountsToUniqueID
{
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"UserAccount"];
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    
    if (results.count > 0) {
        for (UserAccount *account in results) {
            [account migrateOldUserAccountPassword];
        }
    }
}

+ (void)removeGMailAuthentication
{
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"EmailAccount"];
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    
    if (results.count > 0) {
        for (EmailAccount *account in results) {
            if (account.mailType.intValue == EmailAccountTypeGMail) {
                [account deleteOAuth2Authentication];
            }
        }
    }
}

@end
