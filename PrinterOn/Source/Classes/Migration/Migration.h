//
//  Migration.h
//  PrinterOn
//
//  Created by Mark Burns on 2/24/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface Migration : NSObject

+ (void)performMigration;

@end
