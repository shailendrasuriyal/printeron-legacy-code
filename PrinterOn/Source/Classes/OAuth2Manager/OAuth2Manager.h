//
//  OAuth2Manager.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-03-07.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "GTMOAuth2ViewControllerTouch.h"

#import "AppAuth.h"

@interface OAuth2Manager : NSObject

+ (id<OIDAuthorizationFlowSession>)showGoogleAuthOnViewController:(UIViewController *)vc withClientID:(NSString *)clientID callback:(OIDAuthStateAuthorizationCallback)callback;

+ (id<OIDAuthorizationFlowSession>)showOutlookAuthOnViewController:(UIViewController *)vc clientID:(NSString *)clientID clientSecret:(NSString *)clientSecret callback:(OIDAuthStateAuthorizationCallback)callback;

+ (GTMOAuth2Authentication *)authForOutlook:(NSString *)keychainName clientID:(NSString *)clientID clientSecret:(NSString *)clientSecret error:(NSError *__autoreleasing *)error;
//+ (GTMOAuth2Authentication *)authForYahoo:(NSString *)keychainName error:(NSError *__autoreleasing *)error;

+ (GTMOAuth2ViewControllerTouch *)viewControllerForOutlook:(GTMOAuth2ViewControllerCompletionHandler)handler clientID:(NSString *)clientID clientSecret:(NSString *)clientSecret;
//+ (GTMOAuth2ViewControllerTouch *)viewControllerForYahoo:(GTMOAuth2ViewControllerCompletionHandler)handler;

+ (GTMOAuth2Authentication *)authForPON:(NSString *)keychainName serviceURL:(NSString *)url error:(NSError *__autoreleasing *)error;
+ (GTMOAuth2ViewControllerTouch *)viewControllerForPON:(GTMOAuth2ViewControllerCompletionHandler)handler serviceURL:(NSString *)url;

+ (void)authorizeRequest:(NSMutableURLRequest *)request forService:(NSString *)url completionHandler:(void (^)(NSError *error))handler;
+ (void)userInfoForServiceURL:(NSString *)serviceURL withAuth:(GTMOAuth2Authentication *)auth completionHandler:(void (^)(NSDictionary *JSON, NSError *error))handler;

+ (NSString *)accessTokenForAuth:(id)auth;
+ (NSString *)userEmailForAuth:(id)auth;

+ (BOOL)isAuthSettingsError:(NSError *)error;
+ (void)showAuthSettingsError:(NSError *)error overController:(UINavigationController *)nav;
+ (void)showUserSettingsForService:(NSString *)serviceURL overController:(UINavigationController *)nav;

@end
