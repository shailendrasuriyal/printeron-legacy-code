//
//  OAuth2Manager.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-03-07.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "OAuth2Manager.h"

#import "NSData+Transform.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "UAObfuscatedString.h"
#import "UserAccount.h"
#import "UserSetupViewController.h"

#import "AFNetworking.h"

@implementation OAuth2Manager

+ (id<OIDAuthorizationFlowSession>)showGoogleAuthOnViewController:(UIViewController *)vc withClientID:(NSString *)clientID callback:(OIDAuthStateAuthorizationCallback)callback
{
    NSURL *authorizationEndpoint = [NSURL URLWithString:@"https://accounts.google.com/o/oauth2/v2/auth"];
    NSURL *tokenEndpoint = [NSURL URLWithString:@"https://www.googleapis.com/oauth2/v4/token"];

    OIDServiceConfiguration *configuration = [[OIDServiceConfiguration alloc] initWithAuthorizationEndpoint:authorizationEndpoint tokenEndpoint:tokenEndpoint];

    OIDAuthorizationRequest *request = [[OIDAuthorizationRequest alloc] initWithConfiguration:configuration
                                                  clientId:clientID
                                              clientSecret:nil
                                                    scopes:@[OIDScopeOpenID, OIDScopeEmail, @"https://mail.google.com/"]
                                               redirectURL:[NSURL URLWithString:@"com.googleusercontent.apps.102917463087-f4vgp3fl149jf8fjq8g78jie7jo9qi7p:/oauthredirect"]
                                              responseType:OIDResponseTypeCode
                                      additionalParameters:nil];

    id<OIDAuthorizationFlowSession> currentAuthorizationFlow = [OIDAuthState authStateByPresentingAuthorizationRequest:request presentingViewController:vc callback:callback];
    return currentAuthorizationFlow;
}

+ (id<OIDAuthorizationFlowSession>)showOutlookAuthOnViewController:(UIViewController *)vc clientID:(NSString *)clientID clientSecret:(NSString *)clientSecret callback:(OIDAuthStateAuthorizationCallback)callback
{
    NSURL *authorizationEndpoint = [NSURL URLWithString:@"https://login.live.com/oauth20_authorize.srf"];
    NSURL *tokenEndpoint = [NSURL URLWithString:@"https://login.live.com/oauth20_token.srf"];

    OIDServiceConfiguration *configuration = [[OIDServiceConfiguration alloc] initWithAuthorizationEndpoint:authorizationEndpoint tokenEndpoint:tokenEndpoint];

    OIDAuthorizationRequest *request = [[OIDAuthorizationRequest alloc] initWithConfiguration:configuration
                                                    clientId:clientID
                                                clientSecret:clientSecret
                                                      scopes:@[@"wl.imap", @"wl.offline_access"]
                                                 redirectURL:[NSURL URLWithString:@"https://sentinel.printeron.net/oauthredirect"]
                                                responseType:OIDResponseTypeCode
                                        additionalParameters:nil];

    id<OIDAuthorizationFlowSession> currentAuthorizationFlow = [OIDAuthState authStateByPresentingAuthorizationRequest:request presentingViewController:vc callback:callback];
    return currentAuthorizationFlow;
}

+ (GTMOAuth2Authentication *)authForOutlookWithClientID:(NSString *)clientID clientSecret:(NSString *)clientSecret
{
    GTMOAuth2Authentication *auth = [GTMOAuth2Authentication authenticationWithServiceProvider:@"Outlook.com" tokenURL:[NSURL URLWithString:@"https://login.live.com/oauth20_token.srf"] redirectURI:@"http://localhost/" clientID:clientID clientSecret:clientSecret];

    return auth;
}

+ (GTMOAuth2Authentication *)authForOutlook:(NSString *)keychainName clientID:(NSString *)clientID clientSecret:(NSString *)clientSecret error:(NSError *__autoreleasing *)error
{
    GTMOAuth2Authentication *auth = [OAuth2Manager authForOutlookWithClientID:clientID clientSecret:clientSecret];
    [GTMOAuth2ViewControllerTouch authorizeFromKeychainForName:keychainName authentication:auth error:error];

    return auth;
}

+ (GTMOAuth2ViewControllerTouch *)viewControllerForOutlook:(GTMOAuth2ViewControllerCompletionHandler)handler clientID:(NSString *)clientID clientSecret:(NSString *)clientSecret
{
    GTMOAuth2Authentication *auth = [OAuth2Manager authForOutlookWithClientID:clientID clientSecret:clientSecret];
    auth.scope = @"wl.imap wl.offline_access";

    GTMOAuth2ViewControllerTouch *viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithAuthentication:auth authorizationURL:[NSURL URLWithString:@"https://login.live.com/oauth20_authorize.srf"] keychainItemName:nil completionHandler:handler];

    return viewController;
}

/*
+ (GTMOAuth2Authentication *)authForYahoo
{
    GTMOAuth2Authentication *auth = [GTMOAuth2Authentication authenticationWithServiceProvider:@"Yahoo!" tokenURL:[NSURL URLWithString:@"https://api.login.yahoo.com/oauth2/get_token"] redirectURI:@"oob" clientID:@"dj0yJmk9WGlUUTdPSExybE9EJmQ9WVdrOVRuRkhVbWhqTjJzbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD02NQ--" clientSecret:@"103f42024a0add94346046c7e85c1f8db345cb5d"];
    
    return auth;
}

+ (GTMOAuth2Authentication *)authForYahoo:(NSString *)keychainName error:(NSError *__autoreleasing *)error
{
    GTMOAuth2Authentication *auth = [OAuth2Manager authForYahoo];
    [GTMOAuth2ViewControllerTouch authorizeFromKeychainForName:keychainName authentication:auth error:error];

    return auth;
}

+ (GTMOAuth2ViewControllerTouch *)viewControllerForYahoo:(GTMOAuth2ViewControllerCompletionHandler)handler
{
    GTMOAuth2Authentication *auth = [OAuth2Manager authForOutlook];
    auth.scope = @"mail-x";

    GTMOAuth2ViewControllerTouch *viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithAuthentication:auth authorizationURL:[NSURL URLWithString:@"https://api.login.yahoo.com/oauth2/request_auth"] keychainItemName:nil completionHandler:handler];

    return viewController;
}
*/

+ (NSString *)PONCIDData
{
    static NSString *data = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // PrinterOn OAuth2 iOS client ID
        // Encode: Key string -> NSData -> XOR -> Hex string
        //NSData *temp = [@"75c89e20-59b7-4d01-8991-de1ae7459b78" dataUsingEncoding:NSUTF8StringEncoding];
        //[temp transform];
        //NSLog(@"TEMP: %@", temp.hexString);
        NSData *bytes = [NSData toBytes:NSMutableString.new._6._0._5.d._0._6._5._6._7.d._0.a._5.c._5._5._1._7._5.c._5._7._0.b._4._3._7.a._5.d._1._0._5._8._7._2._4._5._5._9._4.b._5._8._5._2._5._9._0._1._1._7._4._2._2.f._0.a._7._4._5.b._4._5._4._0._5._8._5.b._5.d];
        [bytes transform];
        data = [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
    });
    return data;
}

+ (NSString *)PONCSData
{
    static NSString *data = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // PrinterOn OAuth2 iOS client secret
        // Encode: Key string -> NSData -> XOR -> Hex string
        //NSData *temp = [@"7c67d3f7-9447-49b4-ba8c-ee28d641b17c" dataUsingEncoding:NSUTF8StringEncoding];
        //[temp transform];
        //NSLog(@"TEMP: %@", temp.hexString);
        NSData *bytes = [NSData toBytes:NSMutableString.new._6._0._0.b._5._3._5._9._2._0._5.c._0._8._5._2._1._7._5._0._5.a._5.d._4._3._7.a._5.d._4.d._0.a._7._7._4._5._0._3._1._3._5._9._0._0._5._9._0._0._1._7._4._1._7._6._0.b._7._5._5.b._4._1._1.b._0.b._5.b._0._6];
        [bytes transform];
        data = [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
    });
    return data;
}

+ (GTMOAuth2Authentication *)authForPONWithServiceURL:(NSString *)url
{
    NSString *tokenURL = [ServiceCapabilities getOAuthTokenURLForService:url];

    GTMOAuth2Authentication *auth = [GTMOAuth2Authentication authenticationWithServiceProvider:@"PrinterOn" tokenURL:[NSURL URLWithString:tokenURL] redirectURI:@"http://127.0.0.1:64000/" clientID:[self PONCIDData] clientSecret:[self PONCSData]];

    return auth;
}

+ (GTMOAuth2Authentication *)authForPON:(NSString *)keychainName serviceURL:(NSString *)url error:(NSError *__autoreleasing *)error
{
    GTMOAuth2Authentication *auth = [OAuth2Manager authForPONWithServiceURL:url];
    [GTMOAuth2ViewControllerTouch authorizeFromKeychainForName:keychainName authentication:auth error:error];

    return auth;
}

+ (GTMOAuth2ViewControllerTouch *)viewControllerForPON:(GTMOAuth2ViewControllerCompletionHandler)handler serviceURL:(NSString *)url
{
    GTMOAuth2Authentication *auth = [OAuth2Manager authForPONWithServiceURL:url];
    auth.scope = @"openid";

    NSString *authorizeURL = [ServiceCapabilities getOAuthAuthorizeURLForService:url];

    GTMOAuth2ViewControllerTouch *viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithAuthentication:auth authorizationURL:[NSURL URLWithString:authorizeURL] keychainItemName:nil completionHandler:handler];

    return viewController;
}

+ (void)authorizeRequest:(NSMutableURLRequest *)request forService:(NSString *)url completionHandler:(void (^)(NSError *error))handler
{
    [self authorizeRequest:request forService:url repeat:NO completionHandler:handler];
}

+ (void)authorizeRequest:(NSMutableURLRequest *)request forService:(NSString *)url repeat:(BOOL)repeat completionHandler:(void (^)(NSError *error))handler
{
    if (request == nil) {
        if (handler) handler([NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorBadURL userInfo:nil]);
        return;
    }

    // Get the user account for the supplied service
    UserAccount *userAccount = [UserAccount getUserAccountForURL:[NSURL URLWithString:url]];

    // Can't authorize using the anonymous user account
    if (userAccount.isAnonymous.boolValue) {
        if (handler) handler(nil);
        return;
    }

    // Get the OAuth information stored for this user account and service
    GTMOAuth2Authentication *auth = [userAccount getOAuth2AuthenticationForService:url];

    BOOL supportsResource = [ServiceCapabilities serviceSupportsOAuthFirstParty:url];

    if (![auth canAuthorize] && supportsResource) {
        // Authentication wasn't found for the user for this service, we will need to try to re-auth if possible
        [self resourceOwnerForServiceURL:[NSURL URLWithString:url] completionHandler:^(GTMOAuth2Authentication *newauth, NSError *error) {
            if ([newauth canAuthorize]) {
                [self authorizeRequest:request forService:url repeat:repeat completionHandler:handler];
                return;
            }
            if (error == nil) {
                error = [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorBadServerResponse userInfo:nil];
            }
            if (handler) handler(error);
        }];
        return;
    }

    // We have a request and auth information so try to authorize the request
    [auth authorizeRequest:request completionHandler:^(NSError *error) {
        if (error && supportsResource && !repeat && [self isGTMAuthenticationError:error]) {
            [userAccount deleteOAuth2AuthenticationForService:url];
            [self authorizeRequest:request forService:url repeat:YES completionHandler:handler];
            return;
        }

        if (handler) {
            if ( ([error.domain isEqualToString:@"com.google.HTTPStatus"] && (error.code == 400 || error.code == 401)) ||
            ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorAuthorizationFailed) ) {
                handler([NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : url }]);
            } else {
                handler(error);
            }
        }
    }];
}

+ (void)resourceOwnerForServiceURL:(NSURL *)serviceURL completionHandler:(void (^)(GTMOAuth2Authentication *auth, NSError *error))handler
{
    if (handler == nil) return;

    UserAccount *userAccount = [UserAccount getUserAccountForURL:serviceURL];
    if (!userAccount || [userAccount.userName isEqualToString:[UserAccount anonymousUser].userName]) {
        handler(nil, nil);
        return;
    }

    NSString *tokenURL = [ServiceCapabilities getOAuthTokenURLForService:serviceURL.absoluteString];
    if (tokenURL == nil) {
        handler(nil, [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorBadURL userInfo:nil]);
        return;
    }

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];

    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    // Create dictionary of parameters to send
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:@"password" forKey:@"grant_type"];
    [parameters setObject:@"openid" forKey:@"scope"];
    [parameters setObject:userAccount.userName forKey:@"username"];
    if (userAccount.hasPassword.boolValue) {
        [parameters setObject:[userAccount getUserAccountPassword] forKey:@"password"];
    }

    // Add the Authorization header to the request
    NSString *authString = [NSString stringWithFormat:@"%@:%@", [self PONCIDData], [self PONCSData]];
    NSData *data = [authString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *value = [NSString stringWithFormat:@"Basic %@", [data base64EncodedStringWithOptions:0]];
    [manager.requestSerializer setValue:value forHTTPHeaderField:@"Authorization"];

    [manager POST:tokenURL parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (![responseObject isKindOfClass:[NSDictionary class]]) {
            handler(nil, [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorBadServerResponse userInfo:nil]);
            return;
        }

        NSDictionary *jsonDict = (NSDictionary *)responseObject;
        GTMOAuth2Authentication *auth = [userAccount getOAuth2AuthenticationForService:serviceURL.absoluteString];
        if (auth) {
            id value = jsonDict[@"expires_in"];
            if ([value isKindOfClass:[NSNumber class]]) {
                [auth setExpiresIn:value];
            }

            [auth setAccessToken:jsonDict[@"access_token"]];
            [auth setTokenType:jsonDict[@"token_type"]];
            [auth setRefreshToken:jsonDict[@"refresh_token"]];
            [auth setScope:@"openid"];
            [auth setIDToken:jsonDict[@"id_token"]];

            [userAccount saveOAuth2Authentication:auth forService:serviceURL.absoluteString];
        }

        handler(auth, nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode == 400 || response.statusCode == 401) {
            handler(nil, [NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : serviceURL.absoluteString }]);
        } else {
            handler(nil, error);
        }
    }];
}

+ (void)userInfoForServiceURL:(NSString *)serviceURL withAuth:(GTMOAuth2Authentication *)auth completionHandler:(void (^)(NSDictionary *JSON, NSError *error))handler
{
    if (handler == nil) return;

    NSString *userInfoURL = [ServiceCapabilities getOAuthUserInfoURLForService:serviceURL];
    if (userInfoURL == nil) {
        handler(nil, [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorBadURL userInfo:nil]);
        return;
    }

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
    
    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:userInfoURL]];

    // Add the Authorization header to the request
    [self authorizeUserInfoRequest:request forService:serviceURL withAuth:auth completionHandler:^(NSError *error) {
        if (error) {
            handler(nil, error);
            return;
        }

        // Add the Authorization header to the request
        [manager.requestSerializer setValue:request.allHTTPHeaderFields[@"Authorization"] forHTTPHeaderField:@"Authorization"];

        [manager GET:userInfoURL parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            if (![responseObject isKindOfClass:[NSDictionary class]]) {
                handler(nil, [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorBadServerResponse userInfo:nil]);
                return;
            }

            NSDictionary *jsonDict = (NSDictionary *)responseObject;
            handler(jsonDict, nil);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            if (response.statusCode == 400 || response.statusCode == 401) {
                handler(nil, [NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : serviceURL }]);
            } else {
                handler(nil, error);
            }
        }];
    }];
}

+ (void)authorizeUserInfoRequest:(NSMutableURLRequest *)request forService:(NSString *)url withAuth:(GTMOAuth2Authentication *)auth completionHandler:(void (^)(NSError *error))handler
{
    if (request == nil) {
        if (handler) handler([NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorBadURL userInfo:nil]);
        return;
    }

    // We have a request and auth information so try to authorize the request
    [auth authorizeRequest:request completionHandler:^(NSError *error) {
        if (handler) {
            if ( ([error.domain isEqualToString:@"com.google.HTTPStatus"] && (error.code == 400 || error.code == 401)) ||
                ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorAuthorizationFailed) ) {
                handler([NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : url }]);
            } else {
                handler(error);
            }
        }
    }];
}

+ (NSString *)accessTokenForAuth:(id)auth
{
    if ([auth isMemberOfClass:[GTMOAuth2Authentication class]]) {
        return ((GTMOAuth2Authentication *)auth).accessToken;
    } else if ([auth isMemberOfClass:[OIDAuthState class]]) {
        return ((OIDAuthState *)auth).lastTokenResponse.accessToken;
    } else {
        return nil;
    }
}

+ (NSString *)userEmailForAuth:(id)auth
{
    if ([auth isMemberOfClass:[GTMOAuth2Authentication class]]) {
        return ((GTMOAuth2Authentication *)auth).userEmail;
    } else if ([auth isMemberOfClass:[OIDAuthState class]]) {
        // Decodes the ID Token locally to extract the email address.
        NSString *idToken = ((OIDAuthState *)auth).lastTokenResponse.idToken ? : ((OIDAuthState *)auth).lastAuthorizationResponse.idToken;
        if (idToken) {
            NSDictionary *claimsDictionary = [self extractIDTokenClaimsNoVerification:idToken];
            if (claimsDictionary) {
                return (NSString *)[claimsDictionary[@"email"] copy];
            }
        }
    }
    return nil;
}

# pragma mark - ID Token Extraction

+ (NSDictionary *)extractIDTokenClaimsNoVerification:(NSString *)idToken
{
    NSArray *sections = [idToken componentsSeparatedByString:@"."];
    if (sections.count > 1) {
        // Gets the JWT payload section.
        NSMutableString *body = [sections[1] mutableCopy];

        // Converts base64url to base64.
        NSRange range = NSMakeRange(0, body.length);
        [body replaceOccurrencesOfString:@"-" withString:@"+" options:NSLiteralSearch range:range];
        [body replaceOccurrencesOfString:@"_" withString:@"/" options:NSLiteralSearch range:range];

        // Converts base64 no padding to base64 with padding
        while (body.length % 4 != 0) {
            [body appendString:@"="];
        }

        // Decodes base64 string.
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:body options:0];

        // Parses JSON.
        NSError *error;
        id object = [NSJSONSerialization JSONObjectWithData:decodedData options:0 error:&error];
        if (error) {
            NSLog(@"Error %@ parsing token payload %@", error, body);
        }
        if ([object isKindOfClass:[NSDictionary class]]) {
            return (NSDictionary *)object;
        }
    }
    return nil;
}

#pragma mark - Error Handling

+ (BOOL)isGTMAuthenticationError:(NSError *)error
{
    if ([error.domain isEqualToString:@"com.google.HTTPStatus"] && (error.code >= 400 && error.code <= 500)) {
        return YES;
    } else if ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorAuthorizationFailed) {
        return YES;
    }
    return NO;
}

+ (BOOL)isAuthSettingsError:(NSError *)error
{
    if ([error.domain isEqualToString:@"com.printeron.printeron.AuthStatus"] && error.code == -1013) {
        return YES;
    }
    return NO;
}

+ (void)showAuthSettingsError:(NSError *)error overController:(UINavigationController *)nav
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_CHECK_AUTHENTICATION", nil) preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"TITLE_SETTINGS", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

        dispatch_async(dispatch_get_main_queue(), ^{
            if ([error.userInfo[@"serviceURL"] isKindOfClass:[NSString class]]) {
                [self showUserSettingsForService:error.userInfo[@"serviceURL"] overController:nav];
            }
        });
    }];
    [alertVC addAction:settingsAction];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_CANCEL", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [nav presentViewController:alertVC animated:YES completion:nil];
}

+ (void)showUserSettingsForService:(NSString *)serviceURL overController:(UINavigationController *)nav
{
    UserAccount *account = [UserAccount getUserAccountForURL:[NSURL URLWithString:serviceURL]];
    if (account.isAnonymous.boolValue) return;

    if (![ServiceCapabilities isServiceUsingOAuth:serviceURL]) return;

    if ([ServiceCapabilities serviceSupportsOAuthFirstParty:serviceURL]) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
        UserSetupViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"userSetup"];
        controller.userAccount = account;
        controller.hideServices = YES;
        [nav pushViewController:controller animated:YES];
    } else {
        GTMOAuth2ViewControllerCompletionHandler handler = ^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error)
        {
            BOOL userCancelled = error && ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorWindowClosed);
            if (userCancelled) return;

            if (error) {
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
                [alertVC addAction:cancelAction];

                [nav presentViewController:alertVC animated:YES completion:nil];
            } else {
                [account saveOAuth2Authentication:auth forService:serviceURL];
            }
        };

        GTMOAuth2ViewControllerTouch *vc = [OAuth2Manager viewControllerForPON:handler serviceURL:serviceURL];
        vc.navigationItem.title = NSLocalizedPONString(@"TITLE_USERSETUP", nil);
        [nav pushViewController:vc animated:YES];
    }
}

@end
