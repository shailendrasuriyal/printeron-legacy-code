//
//  PONBrowserURLProtocol.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-08-20.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "PONBrowserURLProtocol.h"

#import "Service.h"

@interface PONBrowserURLProtocol ()

@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSURLAuthenticationChallenge *challenge;
@property (nonatomic, assign) BOOL wasCancelled;

@end

@implementation PONBrowserURLProtocol

+ (BOOL)canInitWithRequest:(NSURLRequest *)request
{
    // Only handle NSURLRequest that have been tagged to use our handler
    if ([[NSURLProtocol propertyForKey:@"PONBrowserRequest" inRequest:request] isEqual:@YES]) {
        if ([[NSURLProtocol propertyForKey:@"PONBrowserRequestHandled" inRequest:request] isEqual:@YES]) {
            return NO;
        } else {
            return YES;
        }
    }
    
    return NO;
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request
{
    return request;
}

+ (BOOL)requestIsCacheEquivalent:(NSURLRequest *)a toRequest:(NSURLRequest *)b
{
    return [super requestIsCacheEquivalent:a toRequest:b];
}

- (void)startLoading
{
    NSMutableURLRequest *newRequest = self.request.mutableCopy;
    [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequestHandled" inRequest:newRequest];
    self.connection = [NSURLConnection connectionWithRequest:newRequest delegate:self];
}

- (void)stopLoading
{
    if (self.connection != nil) {
        [self.connection cancel];
    }
    self.connection = nil;
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.client URLProtocol:self didLoadData:data];
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
    if (response) {
        [self.client URLProtocol:self wasRedirectedToRequest:request redirectResponse:response];
    }

    return request;
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    NSURLProtectionSpace *space = challenge.protectionSpace;
    
    if ([space.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic] ||
        [space.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPDigest] ||
        ([space.authenticationMethod isEqualToString:NSURLAuthenticationMethodDefault] && ([space.protocol.lowercaseString isEqualToString:@"http"] || [space.protocol.lowercaseString isEqualToString:@"https"]))
        ) {

        NSURLCredential *credential = [[NSURLCredentialStorage sharedCredentialStorage] defaultCredentialForProtectionSpace:space];
        if (credential) {
            if (challenge.previousFailureCount == 0) {
                [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
                return;
            } else {
                [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:credential forProtectionSpace:space];
            }
        }
        
        if (challenge.previousFailureCount == 0) {
            _challenge = challenge;
            
            NSString *messageText = [NSString stringWithFormat:@"%@://%@:%ld", space.protocol, space.host, (long)space.port];
            if (space.realm.length > 0) messageText = [messageText stringByAppendingFormat:@": %@", space.realm];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIWindow* window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                window.rootViewController = [UIViewController new];
                window.windowLevel = UIWindowLevelAlert + 1;

                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_AUTH_REQUIRED", nil) message:messageText preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *loginAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_LOGIN", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

                    NSURLCredential *credential = [NSURLCredential credentialWithUser:alertVC.textFields[0].text password:alertVC.textFields[1].text persistence:NSURLCredentialPersistenceForSession];
                    [[NSURLCredentialStorage sharedCredentialStorage] setDefaultCredential:credential forProtectionSpace:_challenge.protectionSpace];
                    [_challenge.sender useCredential:credential forAuthenticationChallenge:_challenge];
                    _challenge = nil;

                    // This keeps a reference to the window until the action is invoked.
                    window.hidden = YES;
                }];
                [alertVC addAction:loginAction];

                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

                    self.wasCancelled = YES;
                    [_challenge.sender cancelAuthenticationChallenge:_challenge];
                    _challenge = nil;

                    // This keeps a reference to the window until the action is invoked.
                    window.hidden = YES;
                }];
                [alertVC addAction:cancelAction];

                [alertVC addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    textField.placeholder = NSLocalizedPONString(@"LABEL_LOGIN", nil);
                }];
                [alertVC addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    textField.placeholder = NSLocalizedPONString(@"LABEL_PASSWORD", nil);
                    textField.secureTextEntry = YES;
                }];

                [window makeKeyAndVisible];
                [window.rootViewController presentViewController:alertVC animated:YES completion:nil];
            });
            
            // Not sure why but if we call this it seems to make it so that the connection doesn't timeout while waiting for the user
            [self.client URLProtocol:self didReceiveAuthenticationChallenge:challenge];
        } else {
            [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
        }
    } else {
        [Service validateServerTrust:challenge];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if (self.wasCancelled) {
        self.wasCancelled = NO;
        
        // Replace the error with one that we will fail silently
        error = [NSError errorWithDomain:NSURLErrorDomain code:-999 userInfo:nil];
    }
    
    [self.client URLProtocol:self didFailWithError:error];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.client URLProtocolDidFinishLoading:self];
}

@end
