//
//  PrintJobSessionManager.m
//  PrinterOn
//
//  Created by Mark Burns on 1/7/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PrintJobSessionManager.h"

#import "ALAlertBanner.h"
#import "DocProcess.h"
#import "DocStatus.h"
#import "GCDSingleton.h"
#import "JobAccountingInputs.h"
#import "MainViewController.h"
#import "NSManagedObject+Clone.h"
#import "OAuth2Manager.h"
#import "PrintDocument.h"
#import "Printer.h"
#import "PrintHistoryViewController.h"
#import "PrintJob.h"
#import "PrintJobSessionItem.h"
#import "Service.h"

#import "AFNetworking.h"
#import "XMLReader.h"

@interface PrintJobSessionManager ()

@property (nonatomic, strong) NSMutableDictionary *jobTable;
@property (nonatomic, assign) BOOL useBackground;

@end

@implementation PrintJobSessionManager

SINGLETON_GCD(PrintJobSessionManager);

- (instancetype)init
{
    if (self = [super init]) {
        // Setup when NSURLSession should use background sessions
        self.useBackground = NO;
        #if PON_USE_BACKGROUND_SESSIONS
            self.useBackground = YES;
        #endif

        // Find the MainViewController and store a reference too it
        UIViewController *root = [UIApplication sharedApplication].delegate.window.rootViewController;
        if ([root isKindOfClass:[UINavigationController class]]) {
            root = ((UINavigationController *)root).topViewController;
        }
        if ([root isMemberOfClass:[MainViewController class]]) {
            self.mainView = (MainViewController *)root;
        }

        // Perform initialization tasks
        [self initRestKit];
        [self initLocalNotifications];
        [self initJobTable];

        // Begin listening for changes to the core data store for print job insertion and deletion
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(objectChangedNotification:) name:NSManagedObjectContextObjectsDidChangeNotification object:[RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext];
    }
    return self;
}

- (void)dealloc
{
    // Remove listener on core data if and when the manager dealloc's
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextObjectsDidChangeNotification object:[RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext];
}

- (void)objectChangedNotification:(NSNotification *)notification
{
    // Check to see if any new PrintJob were added to the persistent store
    NSArray *inserted = notification.userInfo[NSInsertedObjectsKey];
    if (inserted.count > 0) {
        for (NSManagedObject *obj in inserted) {
            if ([obj.entity.name isEqualToString:@"PrintJob"]) {
                // Add the job to the print job table and queue the job for printing immediately
                PrintJob *job = (PrintJob *)obj;

                JobStatusCode code = (JobStatusCode)[job getJobStatusValue];
                if (code == JobStatusCodeFailed || code == JobStatusCodeCancelled || code == JobStatusCodeSuccess) {
                    return;
                }

                [self updatePrintJob:job withStatus:JobStatusCodeCreated];
                [self queuePrintJob:[self getJobItemForJobIdentifier:job.jobUUID] afterDelay:0];
            }
        }
    }

    // Check to see if any PrintJob were deleted from the persistent store
    NSArray *deleted = notification.userInfo[NSDeletedObjectsKey];
    if (deleted.count > 0) {
        for (NSManagedObject *obj in deleted) {
            if ([obj.entity.name isEqualToString:@"PrintJob"]) {
                // Remove the job from the print job table and cancel any queued operations
                PrintJob *job = (PrintJob *)obj;
                if (job.jobUUID) {
                    PrintJobSessionItem *item = [self getJobItemForJobIdentifier:job.jobUUID];
                    [self.jobTable removeObjectForKey:job.jobUUID];
                    [self finishPrintJob:item];
                }
            }
        }
    }
}

- (void)initLocalNotifications
{
    double delayInSeconds = 3.0;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    });
}

- (void)initJobTable
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    [context performBlockAndWait:^() {
        // Get a list of all print jobs
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"PrintJob" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

        // Recreate all of the PrintJobItem's and put them in the job table
        for (PrintJob *job in results) {
            JobStatusCode code = (JobStatusCode)[job getJobStatusValue];
            if (code == JobStatusCodeCreated || code == JobStatusCodeUpload || code == JobStatusCodePending || code == JobStatusCodeHeld) {
                PrintJobSessionItem *item = [PrintJobSessionItem new];
                item.jobUUID = job.jobUUID;
                dictionary[job.jobUUID] = item;
                [self prepareSession:item];
            }
        }
    }];

    self.jobTable = dictionary;

    dispatch_async(dispatch_get_main_queue(), ^{
        [self checkPrintJobs];
    });
}

- (void)refreshJobs
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block NSMutableDictionary *dictionary = self.jobTable == nil ? [NSMutableDictionary dictionary] : [NSMutableDictionary dictionaryWithDictionary:self.jobTable];

    [context performBlockAndWait:^() {
        // Get a list of all print jobs
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"PrintJob" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

        for (PrintJob *job in results) {
            JobStatusCode code = (JobStatusCode)[job getJobStatusValue];
            if (code == JobStatusCodePending || code == JobStatusCodeHeld) {
                PrintJobSessionItem *item = [self getJobItemForJobIdentifier:job.jobUUID];
                if (item == nil) {
                    item = [PrintJobSessionItem new];
                    item.jobUUID = job.jobUUID;
                    dictionary[job.jobUUID] = item;
                    [self prepareSession:item];
                }
            }
        }
    }];

    self.jobTable = dictionary;

    dispatch_async(dispatch_get_main_queue(), ^{
        [self checkPrintJobs];
    });
}

- (void)recreatePrintJobItem:(NSString *)jobUUID
{
    PrintJobSessionItem *item = [PrintJobSessionItem new];
    item.jobUUID = jobUUID;
    self.jobTable[jobUUID] = item;
    [self prepareSession:item];
}

#pragma mark - PrintJob Operations

- (PrintJobSessionItem *)getJobItemForPrintJob:(PrintJob *)job
{
    return self.jobTable[job.jobUUID];
}

- (PrintJobSessionItem *)getJobItemForJobIdentifier:(NSString *)identifier
{
    return self.jobTable[identifier];
}

- (NSString *)generateJobUUID
{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    return (__bridge_transfer NSString *)string;
}

- (void)createPrintJob:(Printer *)printer
          withDocument:(PrintDocument *)document
           withOptions:(NSDictionary *)options
            withInputs:(NSDictionary *)inputs
{
    // Create a temporary child context so that we can clone the printer and create the print job in one transaction that will not be
    // affected by other saves to the main context.
    NSManagedObjectContext *tempObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:YES];
    tempObjectContext.undoManager = nil;

    // Clone the printer as a JobPrinter
    Printer *jobPrinter = [printer clonePrinterInContext:tempObjectContext forEntityName:@"JobPrinter" updateExisting:YES];

    // Create the PrintJob and set it's properties
    PrintJob *printJob = [NSEntityDescription insertNewObjectForEntityForName:@"PrintJob" inManagedObjectContext:tempObjectContext];
    printJob.printer = jobPrinter;
    printJob.jobUUID = [self generateJobUUID];
    printJob.jobStatus = @(JobStatusCodeCreated);
    printJob.dateTime = [NSDate date];
    printJob.docType = [self setDocumentType:document];
    printJob.docData = [self setDocumentData:document];
    [self setJobInputs:printJob withInputs:inputs];
    printJob.printOptions = options;
    [self prepareDocument:printJob withURL:[document getDocumentURL] isReprint:document.reprint previewPage:document.previewPage];

    // Save the temporary context to the persistent store
    [tempObjectContext saveToPersistentStore:nil];
}

- (NSNumber *)setDocumentType:(PrintDocument *)document
{
    if (document.reprint) {
        return document.reprintType;
    } else {
        return @([document getDocumentType]);
    }
}

- (NSMutableDictionary *)setDocumentData:(PrintDocument *)document
{
    NSMutableDictionary *dictionary;
    if (document.reprint) {
        dictionary = [NSMutableDictionary dictionaryWithDictionary:document.reprintData];
        dictionary[kDocumentReprintKey] = @(document.reprint);
    } else {
        dictionary = [document getDocumentData];
    }
    return dictionary;
}

- (void)setJobInputs:(PrintJob *)job withInputs:(NSDictionary *)inputs
{
    JobAccountingInputs *accounting = job.printer.jobAccountingInputs;

    if (!inputs || !accounting) return;

    job.clientInput = [job storeJobInput:inputs[kJobInputTypeClientUIDKey]];
    job.emailInput = [job storeJobInput:inputs[kJobInputTypeEmailAddressKey]];
    job.networkInput = [job storeJobInput:inputs[kJobInputTypeNetworkLoginKey]];
    job.sessionInput = [job storeJobInput:inputs[kJobInputTypeSessionMetadataKey]];
    job.releaseInput = inputs[kJobInputTypeReleaseCodeKey];
}

- (void)prepareDocument:(PrintJob *)job
                withURL:(NSURL *)url
              isReprint:(BOOL)reprint
            previewPage:(UIImage *)image
{
    NSFileManager *fileManager = [NSFileManager defaultManager];

    // Check to see if the Jobs directory exists, create it if it doesn't
    NSString *jobsPath = [[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"Jobs"] stringByAppendingPathComponent:job.jobUUID];
    BOOL isDir;
    BOOL exist = [fileManager fileExistsAtPath:jobsPath isDirectory:&isDir];

    if (!exist || (exist && !isDir)) {
        NSError *error;
        if (![fileManager createDirectoryAtPath:jobsPath withIntermediateDirectories:YES attributes:nil error:&error]) {
            job.errorText = error ? [NSString stringWithFormat:@"%@  %@: %ld.", NSLocalizedPONString(@"ERROR_DIRCREATE", nil), error.domain, (long)error.code] : NSLocalizedPONString(@"ERROR_DIRCREATE", nil);
            [self updatePrintJob:job withStatus:JobStatusCodeFailed];
            return;
        }
    }

    // Copy or move the document to the Jobs directory
    NSURL *destURL = [NSURL fileURLWithPath:[jobsPath stringByAppendingPathComponent:url.lastPathComponent]];
    if (![fileManager fileExistsAtPath:destURL.path]) {
        NSError *error;
        BOOL success = reprint ? [fileManager copyItemAtPath:url.path toPath:destURL.path error:&error] : [fileManager moveItemAtPath:url.path toPath:destURL.path error:&error];
        if (!success) {
            job.errorText = error ? [NSString stringWithFormat:@"%@  %@: %ld.", NSLocalizedPONString(@"ERROR_COPYDOC", nil), error.domain, (long)error.code] : NSLocalizedPONString(@"ERROR_COPYDOC", nil);
            [self updatePrintJob:job withStatus:JobStatusCodeFailed];
            return;
        }
    }

    // Save the preview image to this print jobs directory
    NSString *previewPath = [NSString stringWithFormat:@"%@/preview.jpg", [destURL URLByDeletingLastPathComponent].path];
    NSData *data = UIImageJPEGRepresentation(image, 0.85);
    [data writeToFile:previewPath atomically:YES];

    // Everything was successful so we can set the job's final document uri
    job.documentURI = destURL.absoluteString;
}

- (void)updatePrintJob:(PrintJob *)job
            withStatus:(JobStatusCode)code
{
    JobStatusCode oldcode = (JobStatusCode)[job getJobStatusValue];

    if (oldcode != code) {
        job.jobStatus = @((int)code);

        // Only write the status change to core data if the value actually changed
        [job.managedObjectContext saveToPersistentStore:nil];
    }

    if (code == JobStatusCodeFailed || code == JobStatusCodeCancelled || code == JobStatusCodeSuccess) {
        PrintJobSessionItem *item = [self getJobItemForPrintJob:job];
        [self.jobTable removeObjectForKey:job.jobUUID];
        [self finishPrintJob:item];
        [self cleanupJobFiles:item];
    } else if (code == JobStatusCodeCreated) {
        PrintJobSessionItem *item = [PrintJobSessionItem new];
        item.jobUUID = job.jobUUID;
        self.jobTable[job.jobUUID] = item;
        [self prepareSession:item];
    }

    // Only display notifications when the status actually changes
    if (code == JobStatusCodeCreated || oldcode != code) {
        [self displayNotificationForJob:job withStatus:code];
    }
}

#pragma mark - PrintJob Scheduling

- (void)queuePrintJob:(PrintJobSessionItem *)item afterDelay:(NSTimeInterval)delay
{
    UIApplicationState state = [UIApplication sharedApplication].applicationState;

    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];
    JobStatusCode code = (JobStatusCode)[job getJobStatusValue];

    switch (code) {
        case JobStatusCodeCreated:
        case JobStatusCodeUpload:
            if (state == UIApplicationStateBackground) {
                [NSThread sleepForTimeInterval:delay];
                [self uploadPrintJob:item];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    item.timer = [NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(uploadPrintJobFromTimer:) userInfo:item repeats:NO];
                });
            }
            break;
        case JobStatusCodePending:
        case JobStatusCodeHeld:
            if (state == UIApplicationStateBackground) {
                [NSThread sleepForTimeInterval:delay];
                [self queryPrintJobStatus:item];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    item.timer = [NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(queryPrintJobStatusForTimer:) userInfo:item repeats:NO];
                });
            }
            break;
        case JobStatusCodeUploadExtension:
        default:
            break;
    }
}

- (void)checkPrintJobs
{
    for (PrintJobSessionItem *item in self.jobTable.allValues) {
        if (item.task) {
            NSURLSessionTaskState state = item.task.state;
            switch (state) {
                case NSURLSessionTaskStateRunning:
                case NSURLSessionTaskStateCanceling:
                case NSURLSessionTaskStateCompleted:
                    break;
                case NSURLSessionTaskStateSuspended:
                    [item.task resume];
                    break;
            }
        } else {
            PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];
            JobStatusCode code = (JobStatusCode)[job getJobStatusValue];
            if (code != JobStatusCodeUpload && code != JobStatusCodeUploadExtension) {
                [self queuePrintJob:item afterDelay:0];
            }
        }
    }
}

- (void)finishPrintJob:(PrintJobSessionItem *)item
{
    if (item == nil) return;

    // Invalidate the timer if it exists so it won't fire
    if (item.timer) {
        [item.timer invalidate];
        item.timer = nil;
    }

    // Invalidate the session so resources are properly deallocated
    if (item.session) {
        [item.session invalidateSessionCancelingTasks:YES];
        item.session = nil;
    }

    item.task = nil;
    item.progress = nil;

    // If a background session handler exists run it to close out the background session properly
    if (item.backgroundSessionCompletionHandler) {
        void (^completionHandler)() = item.backgroundSessionCompletionHandler;
        item.backgroundSessionCompletionHandler = nil;
        completionHandler();
    }
}

- (void)fireAllTasks
{
    for (PrintJobSessionItem *item in self.jobTable.allValues) {
        if (item.timer) {
            [item.timer fire];
        }
    }
}

- (void)authorizedPrintJob:(NSString *)jobUUID
{
    PrintJob *job = [PrintJob fetchPrintJobByUUID:jobUUID inContext:nil];
    job.jobAuthURI = nil;
    [self updatePrintJob:job withStatus:JobStatusCodePending];
    [self queuePrintJob:[self getJobItemForJobIdentifier:jobUUID] afterDelay:5.0];
}

# pragma mark - Notifications

- (void)displayNotificationForJob:(PrintJob *)job withStatus:(JobStatusCode)code
{
    UIApplicationState appState = [UIApplication sharedApplication].applicationState;
    BOOL isAppInForeground = (appState == UIApplicationStateActive || appState == UIApplicationStateInactive);

    switch (code) {
        case JobStatusCodeFailed:
        case JobStatusCodeCancelled:
            if (isAppInForeground) {
                [self displayInAppNotificationWithStyle:ALAlertBannerStyleFailure withTitle:NSLocalizedPONString(@"LABEL_PRINTFAIL", nil) withSubtitle:NSLocalizedPONString(@"LABEL_NOTIFY_FAIL_DETAILS", nil)];
            } else {
                [self displaySystemNotificationForJob:job withMessage:NSLocalizedPONString(@"LABEL_PRINTFAIL", nil)];
            }
            break;
        case JobStatusCodeSuccess:
            if (isAppInForeground) {
                [self displayInAppNotificationWithStyle:ALAlertBannerStyleSuccess withTitle:NSLocalizedPONString(@"LABEL_PRINTSUCCESS", nil) withSubtitle:NSLocalizedPONString(@"LABEL_NOTIFY_SUCCESS_DETAILS", nil)];
            } else {
                [self displaySystemNotificationForJob:job withMessage:NSLocalizedPONString(@"LABEL_PRINTSUCCESS", nil)];
            }
            break;
        case JobStatusCodeCreated:
            if (isAppInForeground) {
                [self displayInAppNotificationWithStyle:ALAlertBannerStyleNotify withTitle:NSLocalizedPONString(@"LABEL_PRINTSTART", nil) withSubtitle:NSLocalizedPONString(@"LABEL_NOTIFY_START_DETAILS", nil)];
            }
            break;
        case JobStatusCodeHeld:
            if (isAppInForeground) {
                [self displayInAppNotificationWithStyle:ALAlertBannerStyleWarning withTitle:NSLocalizedPONString(@"LABEL_PRINTINPUT", nil) withSubtitle:NSLocalizedPONString(@"LABEL_NOTIFY_INPUT_DETAILS", nil)];
            } else {
                [self displaySystemNotificationForJob:job withMessage:NSLocalizedPONString(@"LABEL_PRINTINPUT", nil)];
            }
            break;
        case JobStatusCodePending:
        case JobStatusCodeUpload:
        case JobStatusCodeUploadExtension:
        default:
            return;
    }
}

- (void)displayInAppNotificationWithStyle:(ALAlertBannerStyle)style withTitle:(NSString *)title withSubtitle:(NSString *)subtitle
{
    dispatch_async(dispatch_get_main_queue(), ^{
        // Don't display an in-app notification if the user is inside of the Print History screen
        UINavigationController *root = self.mainView.navigationController;
        UIViewController *presented = (root.presentedViewController == nil) ? root : root.presentedViewController;
        UINavigationController *nav = [presented isKindOfClass:[UINavigationController class]] ? (UINavigationController *)presented : presented.navigationController;
        for (UIViewController *vc in nav.viewControllers) {
            if ([vc isMemberOfClass:[PrintHistoryViewController class]]) {
                return;
            }
        }

        ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.mainView.navigationController.view style:style position:ALAlertBannerPositionUnderNavBar title:title subtitle:subtitle tappedBlock:nil];
        banner.secondsToShow = 5.0f;
        banner.showAnimationDuration = 0.5f;
        banner.hideAnimationDuration = 0.5f;
        [banner show];
    });
}

- (void)displaySystemNotificationForJob:(PrintJob *)job withMessage:(NSString *)message
{
    UILocalNotification *notification = [UILocalNotification new];
    if (notification == nil)
        return;

    notification.alertBody = message;
    notification.alertAction = @"View";
    notification.userInfo = @{@"jobUUID": job.jobUUID ?: @""};

    UIApplication *shared = [UIApplication sharedApplication];
    UIUserNotificationSettings *settings = [shared currentUserNotificationSettings];
    if (settings.types & UIUserNotificationTypeSound) {
        notification.soundName = UILocalNotificationDefaultSoundName;
    }

    [shared presentLocalNotificationNow:notification];
}

# pragma mark - Network Operations - General

- (void)prepareSession:(PrintJobSessionItem *)item
{
    if (item.session == nil) {
        PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];
        JobStatusCode code = (JobStatusCode)[job getJobStatusValue];

        // Create a session configuration and manager based on the unique job UUID
        NSString *appGroupId = [[NSBundle mainBundle].infoDictionary valueForKey:@"APP_GROUP_ID"];
        NSString *identifier = [NSString stringWithFormat:@"%@.%@", appGroupId, item.jobUUID];
        if (code == JobStatusCodeUploadExtension) identifier = [identifier stringByAppendingString:@".upload"];

        NSURLSessionConfiguration *configuration = self.useBackground ? [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier] : [NSURLSessionConfiguration ephemeralSessionConfiguration];

        // Set the shared container app group identifier
        configuration.sharedContainerIdentifier = appGroupId;

        // Create the session with the above configuration
        item.session = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        item.session.session.sessionDescription = identifier;

        // Set the session managers authentication challenge response
        [item.session setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
            return [Service validateServerTrust:challenge withCredentials:credential];
        }];

        // Set the sessions block to run when response data is received
        [item.session setDataTaskDidReceiveDataBlock:^(NSURLSession *session, NSURLSessionDataTask *dataTask, NSData *data) {
            [[PrintJobSessionManager sharedPrintJobSessionManager] saveResponseData:data fromSession:session];
        }];

        // Set the sessions block to be run when tasks complete
        [item.session setTaskDidCompleteBlock:^(NSURLSession *session, NSURLSessionTask *task, NSError *error) {
            [[PrintJobSessionManager sharedPrintJobSessionManager] taskDidComplete:task forSession:session error:error];
        }];
    }
}

- (NSString *)getUUIDFromIdentifier:(NSString *)identifier
{
    if ([identifier hasSuffix:@".upload"]) {
        identifier = [identifier substringToIndex:identifier.length - 7];
    }

    NSRange lastDotRange = [identifier rangeOfString:@"." options:NSBackwardsSearch];
    if (lastDotRange.location != NSNotFound) {
        identifier = [identifier substringFromIndex:lastDotRange.location + 1];
    }

    return identifier;
}

- (void)saveResponseData:(NSData *)data fromSession:(NSURLSession *)session
{
    NSString *identifier = [self getUUIDFromIdentifier:session.sessionDescription];
    PrintJobSessionItem *item = [self getJobItemForJobIdentifier:identifier];
    if (item == nil) return;

    NSMutableData *responseData = item.response;
    if (!responseData) {
        item.response = [NSMutableData dataWithData:data];
    } else {
        [responseData appendData:data];
    }
}

- (void)taskDidComplete:(NSURLSessionTask *)task forSession:(NSURLSession *)session error:(NSError *)error
{
    NSString *identifier = [self getUUIDFromIdentifier:session.sessionDescription];
    PrintJobSessionItem *item = [self getJobItemForJobIdentifier:identifier];
    if (item == nil) return;

    item.task = nil;

    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];
    JobStatusCode code = (JobStatusCode)[job getJobStatusValue];
    BOOL isUpload = code == JobStatusCodeUpload || code == JobStatusCodeUploadExtension;

    if (error) {
        if (isUpload) {
            [self uploadJobFailure:item error:error];
        } else {
            [self statusJobFailure:item error:error];
        }
        return;
    }

    error = [self checkResponseForErrors:task.response];
    if (error) {
        if (isUpload) {
            [self uploadJobFailure:item error:error];
        } else {
            [self statusJobFailure:item error:error];
        }
        return;
    }

    NSDictionary *response = [XMLReader dictionaryForXMLData:item.response error:&error];
    item.response = nil;
    if (error) {
        if (isUpload) {
            [self uploadJobFailure:item error:error];
        } else {
            [self statusJobFailure:item error:error];
        }
        return;
    }

    if (isUpload) {
        DocProcess *info = [[DocProcess alloc] initWithDictionary:response];
        if (info == nil) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
            userInfo[NSLocalizedDescriptionKey] = [NSString stringWithFormat:@"%@", NSLocalizedPONString(@"Response data could not be parsed", nil)];
            error = [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorCannotParseResponse userInfo:userInfo];
            [self uploadJobFailure:item error:error];
        } else {
            [self uploadJobSuccess:item withInfo:info];
        }
    } else {
        DocStatus *info = [[DocStatus alloc] initWithDictionary:response];
        if (info == nil) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
            userInfo[NSLocalizedDescriptionKey] = [NSString stringWithFormat:@"%@", NSLocalizedPONString(@"Response data could not be parsed", nil)];
            error = [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorCannotParseResponse userInfo:userInfo];
            [self statusJobFailure:item error:error];
        } else {
            [self statusJobSuccess:item withInfo:info];
        }
    }
}

- (NSError *)checkResponseForErrors:(NSURLResponse *)response
{
    NSUInteger statusCode = ([response isKindOfClass:[NSHTTPURLResponse class]]) ? (NSUInteger)((NSHTTPURLResponse *)response).statusCode : 200;

    if (statusCode < 200 || statusCode > 299) {
        if (statusCode == 401) {
            return [NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:nil];
        }

        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        userInfo[NSLocalizedDescriptionKey] = [NSString stringWithFormat:NSLocalizedPONString(@"Expected status code in (200-299), got %d", nil), statusCode];

        return [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorBadServerResponse userInfo:userInfo];
    }

    NSString *contentType = response.MIMEType ?: @"application/octet-stream";
    if (![contentType.lowercaseString hasPrefix:@"text/xml"]) {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        userInfo[NSLocalizedDescriptionKey] = [NSString stringWithFormat:NSLocalizedPONString(@"Expected content type text/xml, got %@", nil), response.MIMEType];

        return [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorCannotDecodeContentData userInfo:userInfo];
    }

    return nil;
}

- (void)cleanupJobFiles:(PrintJobSessionItem *)item
{
    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];
    NSURL *file = [NSURL URLWithString:[[job getDocumentPath] stringByAppendingString:@".upload"]];
    NSString *filePath = file.path;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }

    file = [NSURL URLWithString:[[job getDocumentPath] stringByAppendingString:@".status"]];
    filePath = file.path;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
}

# pragma mark - Network Operations - Upload

- (void)uploadPrintJobFromTimer:(NSTimer *)timer
{
    if (timer) {
        PrintJobSessionItem *item = timer.userInfo;
        item.timer = nil;

        // Run as a background task so this code will continue to execute on transitions of the app
        // to the background state.  If we don't do this and the app is backgrounded while this is running
        // it will immediately be placed on the run loop and wait until the app is foregrounded instead of
        // continuing to finish processing.

        UIApplication *app = [UIApplication sharedApplication];

        item.bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:item.bgTask];
            item.bgTask = UIBackgroundTaskInvalid;
        }];

        //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self uploadPrintJob:item];
            [app endBackgroundTask:item.bgTask];
            item.bgTask = UIBackgroundTaskInvalid;
        });
    }
}

- (void)uploadPrintJob:(PrintJobSessionItem *)item
{
    if (item == nil) return;

    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];
    if (job == nil) return;

    [DocProcess prepareSessionUpload:item inBackground:self.useBackground completionHandler:^(NSError *error) {
        if (error) {
            job.errorText = [OAuth2Manager isAuthSettingsError:error] ? NSLocalizedPONString(@"ERROR_CHECK_AUTHENTICATION", nil) : error.localizedDescription;
            [self updatePrintJob:job withStatus:JobStatusCodeFailed];
        } else {
            [self updatePrintJob:job withStatus:JobStatusCodeUpload];
            [item.task resume];
        }
    }];
}

- (void)uploadJobFailure:(PrintJobSessionItem *)item error:(NSError *)error
{
    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];

    id backgroundKey = error.userInfo[@"NSURLErrorBackgroundTaskCancelledReasonKey"];
    if (backgroundKey && [error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
        if ([backgroundKey integerValue] == NSURLErrorCancelledReasonUserForceQuitApplication) {
            job.errorText = @"Cancelled: Application force quit";
        } else if ([backgroundKey integerValue] == NSURLErrorCancelledReasonBackgroundUpdatesDisabled) {
            job.errorText = @"Cancelled: Background updates disabled";
        } else if ([backgroundKey integerValue] == NSURLErrorCancelledReasonInsufficientSystemResources) {
            job.errorText = @"Cancelled: Insufficient system resources";
        }
    } else if ([OAuth2Manager isAuthSettingsError:error]) {
        job.errorText = NSLocalizedPONString(@"ERROR_CHECK_AUTHENTICATION", nil);
    } else {
        job.errorText = error.localizedDescription;
    }

    [self updatePrintJob:job withStatus:JobStatusCodeFailed];
}

- (void)uploadJobSuccess:(PrintJobSessionItem *)item withInfo:(DocProcess *)info
{
    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];

    // The result is actually an error so send to failure
    if (![info.returnCode isEqualToString:@"0"]) {
        job.errorCode = info.returnCode;
        job.errorText = info.userMessage;
        [self updatePrintJob:job withStatus:JobStatusCodeFailed];
        return;
    }

    // A job with jobState 7 (cancelled) or 8 (aborted) are considered cancelled
    int jobState = info.jobState.intValue;
    if (jobState == 7 || jobState == 8) {
        job.errorText = info.userMessage;
        [self updatePrintJob:job withStatus:JobStatusCodeCancelled];
        return;
    }

    job.jobState = @(info.jobState.integerValue);
    job.referenceID = info.jobReferenceID;
    job.releaseCode = info.jobReleaseCode;

    [self updatePrintJob:job withStatus:JobStatusCodePending];
    [self queuePrintJob:item afterDelay:5.0];
}

# pragma mark - Network Operations - Status Query

- (void)queryPrintJobStatusForTimer:(NSTimer *)timer
{
    if (timer) {
        PrintJobSessionItem *item = timer.userInfo;
        item.timer = nil;

        // Run as a background task so this code will continue to execute on transitions of the app
        // to the background state.  If we don't do this and the app is backgrounded while this is running
        // it will immediately be placed on the run loop and wait until the app is foregrounded instead of
        // continuing to finish processing.

        UIApplication *app = [UIApplication sharedApplication];
        
        item.bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:item.bgTask];
            item.bgTask = UIBackgroundTaskInvalid;
        }];
        
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self queryPrintJobStatus:item];
            [app endBackgroundTask:item.bgTask];
            item.bgTask = UIBackgroundTaskInvalid;
        });
    }
}

- (void)queryPrintJobStatus:(PrintJobSessionItem *)item
{
    if (item == nil) return;

    [DocStatus prepareSessionStatusQuery:item inBackground:self.useBackground completionHandler:^(NSError *error) {
        if (error) {
            PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];
            job.errorText = [OAuth2Manager isAuthSettingsError:error] ? NSLocalizedPONString(@"ERROR_CHECK_AUTHENTICATION", nil) : error.localizedDescription;
            [self updatePrintJob:job withStatus:JobStatusCodeFailed];
        } else {
            [item.task resume];
        }
    }];
}

- (void)statusJobFailure:(PrintJobSessionItem *)item error:(NSError *)error
{
    if (error) {
        if ([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
            id backgroundKey = error.userInfo[@"NSURLErrorBackgroundTaskCancelledReasonKey"];
            if (backgroundKey && [backgroundKey integerValue] == NSURLErrorCancelledReasonUserForceQuitApplication) {
                // The app was force quit so we don't want to queue another query, when we relaunch a query will
                // automatically get created to check on status from checkPrintJobs method.
                return;
            }
        } else if ([OAuth2Manager isAuthSettingsError:error]) {
            PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];
            job.errorText = NSLocalizedPONString(@"ERROR_CHECK_AUTHENTICATION", nil);
            [self updatePrintJob:job withStatus:JobStatusCodeFailed];
            return;
        }
    }

    [self queuePrintJob:item afterDelay:15.0];
}

- (void)statusJobSuccess:(PrintJobSessionItem *)item withInfo:(DocStatus *)info
{
    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];

    // The result is actually an error so send to failure
    if (![info.returnCode isEqualToString:@"0"]) {
        job.errorCode = info.returnCode;
        job.errorText = info.userMessage;
        [self updatePrintJob:job withStatus:JobStatusCodeFailed];
        return;
    }
    
    int jobState = info.jobState.intValue;
    if (jobState != job.jobState.intValue) {
        job.jobState = @(info.jobState.integerValue);
    }

    // A job with jobState 7 (cancelled) or 8 (aborted) are considered cancelled
    if (jobState == 7 || jobState == 8) {
        job.errorText = info.userMessage;
        [self updatePrintJob:job withStatus:JobStatusCodeCancelled];
        return;
    }
    
    // The job requires user authentication or authorization to proceed
    if (jobState == 4 || jobState == 6) {
        job.jobAuthURI = [info getJobAuthAddress];
        [self updatePrintJob:job withStatus:JobStatusCodeHeld];
        return;
    }
    
    // The job was completed successfully
    if (jobState == 9) {
        job.jobPageCount = @(info.jobPageCount.integerValue);
        job.docPageCount = @(info.docPageCount.integerValue);
        [self updatePrintJob:job withStatus:JobStatusCodeSuccess];
        return;
    }
    
    [self updatePrintJob:job withStatus:JobStatusCodePending];
    [self queuePrintJob:item afterDelay:5.0];
}

@end
