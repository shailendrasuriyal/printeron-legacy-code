//
//  PrintJobManager.h
//  PrinterOn
//
//  Created by Mark Burns on 1/7/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@class MainViewController, PrintDocument, Printer, PrintJob, PrintJobItem;

@interface PrintJobManager : NSObject

@property (nonatomic, strong) MainViewController *mainView;

+ (PrintJobManager *)sharedPrintJobManager;

- (void)initRestKit;
- (void)createPrintJob:(Printer *)printer
          withDocument:(PrintDocument *)document
           withOptions:(NSDictionary *)options
            withInputs:(NSDictionary *)inputs;

- (void)authorizedPrintJob:(NSString *)jobUUID;
- (PrintJobItem *)getJobItemForPrintJob:(PrintJob *)job;
- (PrintJobItem *)getJobItemForJobIdentifier:(NSString *)identifier;

- (void)didEnterBackground;
- (void)willEnterForeground;

@end
