//
//  PrintJobItem.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-01-13.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface PrintJobItem : NSObject

@property (strong, nonatomic) NSProgress *progress;

@end
