//
//  PrintJobSessionItem.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-01-13.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "PrintJobItem.h"

@class AFURLSessionManager;

@interface PrintJobSessionItem : PrintJobItem

@property (strong, nonatomic) NSString *jobUUID;
@property (strong, nonatomic) AFURLSessionManager *session;
@property (strong, nonatomic) NSURLSessionTask *task;
@property (strong, nonatomic) NSMutableURLRequest *request;
@property (strong, nonatomic) NSMutableData *response;
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) UIBackgroundTaskIdentifier bgTask;
@property (copy) void (^backgroundSessionCompletionHandler)();

@end
