//
//  PrintJobManager.m
//  PrinterOn
//
//  Created by Mark Burns on 1/7/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PrintJobManager.h"

#import "MainViewController.h"
#import "PrintDocument.h"
#import "Printer.h"
#import "PrintJob.h"
#import "PrintJobItem.h"
#import "PrintJobSessionItem.h"
#import "PrintJobSessionManager.h"
#import "RKXMLReaderSerialization.h"

@implementation PrintJobManager

+ (PrintJobManager *)sharedPrintJobManager
{
    return [PrintJobSessionManager sharedPrintJobSessionManager];
}

- (void)initRestKit
{
    if ([RKMIMETypeSerialization serializationClassForMIMEType:@"text/xml"] != [RKXMLReaderSerialization class]) {
        [RKMIMETypeSerialization registerClass:[RKXMLReaderSerialization class] forMIMEType:@"text/xml"];
    }
}

- (void)createPrintJob:(Printer *)printer
          withDocument:(PrintDocument *)document
           withOptions:(NSDictionary *)options
            withInputs:(NSDictionary *)inputs
{
    // Empty.  Overide in each subclass with specific implementation.
}

- (void)authorizedPrintJob:(NSString *)jobUUID
{
    // Empty.  Overide in each subclass with specific implementation.
}

- (PrintJobItem *)getJobItemForPrintJob:(PrintJob *)job
{
    PrintJobManager *manager = [PrintJobManager sharedPrintJobManager];
    if ([manager isMemberOfClass:[PrintJobSessionManager class]]) {
        PrintJobSessionManager *sessionManager = (PrintJobSessionManager *)manager;
        return [sessionManager getJobItemForPrintJob:job];
    }
    return nil;
}

- (PrintJobItem *)getJobItemForJobIdentifier:(NSString *)identifier
{
    PrintJobManager *manager = [PrintJobManager sharedPrintJobManager];
    if ([manager isMemberOfClass:[PrintJobSessionManager class]]) {
        PrintJobSessionManager *sessionManager = (PrintJobSessionManager *)manager;
        return [sessionManager getJobItemForJobIdentifier:identifier];
    }
    return nil;
}

- (void)didEnterBackground
{
    PrintJobManager *manager = [PrintJobManager sharedPrintJobManager];
    if ([manager isMemberOfClass:[PrintJobSessionManager class]]) {
        PrintJobSessionManager *sessionManager = (PrintJobSessionManager *)manager;
        [sessionManager fireAllTasks];
    }
}

- (void)willEnterForeground
{
    PrintJobManager *manager = [PrintJobManager sharedPrintJobManager];
    if ([manager isMemberOfClass:[PrintJobSessionManager class]]) {
        PrintJobSessionManager *sessionManager = (PrintJobSessionManager *)manager;
        [sessionManager refreshJobs];
    }
}

@end
