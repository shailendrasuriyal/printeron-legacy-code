//
//  PrintJobSessionManager.h
//  PrinterOn
//
//  Created by Mark Burns on 1/7/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PrintJobManager.h"

@class PrintJob, PrintJobSessionItem;

@interface PrintJobSessionManager : PrintJobManager

+ (PrintJobSessionManager *)sharedPrintJobSessionManager;

- (PrintJobSessionItem *)getJobItemForPrintJob:(PrintJob *)job;
- (PrintJobSessionItem *)getJobItemForJobIdentifier:(NSString *)identifier;

- (void)refreshJobs;
- (void)recreatePrintJobItem:(NSString *)jobUUID;
- (void)fireAllTasks;

- (NSString *)getUUIDFromIdentifier:(NSString *)identifier;

@end
