//
//  EmailAttachmentManager.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-05-25.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "EmailAttachmentManager.h"

#import "GCDSingleton.h"

#import <CommonCrypto/CommonDigest.h>

@interface EmailAttachmentManager ()

@property (nonatomic, strong) NSMutableDictionary *attachmentTable;

@end

@implementation EmailAttachmentManager

SINGLETON_GCD(EmailAttachmentManager);

- (instancetype)init
{
    if ((self = [super init])) {
        [self initAttachmentTable];
    }
    return self;
}

- (void)initAttachmentTable
{
    self.attachmentTable = [NSMutableDictionary dictionary];
}

- (NSString *)uniqueAttachment:(NSString *)account messageID:(NSString *)uid partID:(NSString *)pid fileName:(NSString *)name
{
    NSString *path = [NSString stringWithFormat:@"%@-%@-%@", account, uid, pid];
    return [NSString stringWithFormat:@"%@/%@", [self md5ForString:path], name];
}

- (BOOL)doesAttachmentExist:(NSString *)attachment
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = (paths.count > 0) ? [paths[0] stringByAppendingString:@"/EmailAttachments"] : nil;

    BOOL isDirectory = NO;
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", basePath, attachment] isDirectory:&isDirectory];
    if (isDirectory) return NO;

    return exists;
}

- (id)progressForAttachment:(NSString *)attachment
{
    if ([self doesAttachmentExist:attachment]) {
        return [NSNull null];
    } else {
        if (![self.attachmentTable[attachment] isMemberOfClass:[NSProgress class]]) {
            self.attachmentTable[attachment] = nil;
        }
    }

    return self.attachmentTable[attachment];
}

- (NSProgress *)startAttachment:(NSString *)attachment
{
    self.attachmentTable[attachment] = [NSProgress new];
    return self.attachmentTable[attachment];
}

- (void)stopAttachment:(NSString *)attachment
{
    self.attachmentTable[attachment] = nil;
}

- (void)writeAttachment:(NSString *)attachment data:(NSData *)data
{
    if (data == nil) return;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = (paths.count > 0) ? [paths[0] stringByAppendingString:@"/EmailAttachments"] : nil;

    NSURL *dirURL = [NSURL fileURLWithPath:basePath isDirectory:YES];
    NSURL *fileURL = [dirURL URLByAppendingPathComponent:attachment];

    [[NSFileManager defaultManager] createDirectoryAtURL:[fileURL URLByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil];

    [data writeToURL:fileURL atomically:NO];
}

- (NSURL *)retrieveURLForAttachment:(NSString *)attachment
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = (paths.count > 0) ? [paths[0] stringByAppendingString:@"/EmailAttachments"] : nil;

    NSURL *dirURL = [NSURL fileURLWithPath:basePath isDirectory:YES];
    NSURL *fileURL = [dirURL URLByAppendingPathComponent:attachment];

    return fileURL;
}

- (NSString *)md5ForString:(NSString*)string
{
    if (!string) return string;

    //md5 hash the string
    const char *str = [string UTF8String];
    unsigned char outBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (int)strlen(str), outBuffer);

    NSMutableString *hash = [NSMutableString string];
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [hash appendFormat:@"%02x", outBuffer[i]];
    }

    return hash;
}

@end
