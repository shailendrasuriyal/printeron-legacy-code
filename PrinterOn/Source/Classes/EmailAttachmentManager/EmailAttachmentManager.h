//
//  EmailAttachmentManager.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-05-25.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@interface EmailAttachmentManager : NSObject

+ (EmailAttachmentManager *)sharedEmailAttachmentManager;

- (NSString *)uniqueAttachment:(NSString *)account messageID:(NSString *)uid partID:(NSString *)pid fileName:(NSString *)name;

- (id)progressForAttachment:(NSString *)attachment;
- (NSProgress *)startAttachment:(NSString *)attachment;
- (void)stopAttachment:(NSString *)attachment;
- (void)writeAttachment:(NSString *)attachment data:(NSData *)data;
- (NSURL *)retrieveURLForAttachment:(NSString *)attachment;

@end
