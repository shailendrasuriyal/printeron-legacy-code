//
//  EmailDocument.h
//  PrinterOn
//
//  Created by Mark Burns on 1/10/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "WebDocument.h"

@class MCOIMAPMessage;

@interface EmailDocument : WebDocument

- (instancetype)initWithWebView:(UIWebView *)webView
                       withIMAP:(MCOIMAPMessage *)imap;

@end
