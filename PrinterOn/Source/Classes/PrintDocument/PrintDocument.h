//
//  PrintDocument.h
//  PrinterOn
//
//  Created by Mark Burns on 11/23/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

FOUNDATION_EXPORT NSString* const kDocumentTypeEmailFromKey;
FOUNDATION_EXPORT NSString* const kDocumentTypeEmailSubjectKey;
FOUNDATION_EXPORT NSString* const kDocumentTypeFileNameKey;
FOUNDATION_EXPORT NSString* const kDocumentTypePhotoSourceKey;
FOUNDATION_EXPORT NSString* const kDocumentTypeWebHostNameKey;
FOUNDATION_EXPORT NSString* const kDocumentTypeWebURLKey;
FOUNDATION_EXPORT NSString* const kDocumentReprintKey;

FOUNDATION_EXPORT float const kPrintDPI;
FOUNDATION_EXPORT float const kPrintDMM;

typedef NS_ENUM(NSUInteger, DocumentType) {
    DocumentTypeEmail = 0,
    DocumentTypeFile = 1,
    DocumentTypePhoto = 2,
    DocumentTypeWeb = 3,
};

@class PrintDocumentDelegate;

@protocol PrintDocumentDelegate <NSObject>
@optional

- (void)didStartRendering;
- (void)didFinishRendering;
- (void)didStartPreviewing;
- (void)didFinishPreviewing;
- (void)setItemImage:(UIImage *)image atIndex:(NSInteger)index;
- (void)startActivityAtIndex:(NSInteger)index;
- (void)endActivityAtIndex:(NSInteger)index;

@end

@interface PrintDocument : NSObject

@property (nonatomic, weak) id<PrintDocumentDelegate> delegate;
@property (nonatomic, assign) BOOL reprint;
@property (nonatomic, strong) NSNumber *reprintType;
@property (nonatomic, strong) NSDictionary *reprintData;
@property (nonatomic, strong) NSMutableDictionary *reprintOptions;
@property (nonatomic, strong) NSString *docUUID;
@property (nonatomic, strong) UIImage *previewPage;

+ (PrintDocument *)getDocumentFromFile:(NSURL *)fileURL fromSource:(NSString *)source;

- (NSMutableDictionary *)initialPrintOptions;
- (BOOL)canBePreviewed;
- (BOOL)previewIsLocalRendered;
- (BOOL)previewIsServerRendered;
- (BOOL)renderBlocksMainUI;
- (void)renderDocumentWithPrintOptions:(NSDictionary *)options;
- (NSURL *)getDocumentURL;
- (NSInteger)getDocumentType;
- (NSMutableDictionary *)getDocumentData;
- (NSUInteger)getPageCount;
- (UIImage *)getPagePreviewAtIndex:(NSUInteger)index withSize:(CGSize)size forPaperSize:(CGSize)paperSize;
- (BOOL)showDocumentDefaultOrientation;

+ (NSArray *)documentUTIs;
+ (BOOL)isSupportedFileType:(NSString *)filePath;

@end
