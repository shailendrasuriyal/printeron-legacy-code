//
//  ImageDocument.h
//  PrinterOn
//
//  Created by Mark Burns on 12/4/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrintDocument.h"

@interface ImageDocument : PrintDocument

- (instancetype)initWithFileURL:(NSURL *)fileURL fromSource:(NSString *)source;
- (instancetype)initWithImage:(UIImage *)image fromSource:(NSString *)source;

@end
