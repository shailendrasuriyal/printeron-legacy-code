//
//  ImageDocument.m
//  PrinterOn
//
//  Created by Mark Burns on 12/4/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "ImageDocument.h"

#import "PaperSize.h"
#import "UIImage+Resize.h"

#import <SGImageCache/SGImageCache.h>

@interface ImageDocument ()

@property (nonatomic, strong) NSURL *documentFile;
@property (nonatomic, strong) UIImage *originalImage;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSString *renderFile;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) CGPDFDocumentRef pdfDocument;
@property (nonatomic, assign) NSUInteger pageCount;

@end

@implementation ImageDocument

- (instancetype)initWithFileURL:(NSURL *)fileURL fromSource:(NSString *)source
{
    self = [super init];
    
    if (self) {
        self.documentFile = fileURL;
        self.source = source;
        self.renderFile = [self getTempFileName];
    }

    return self;
}

- (instancetype)initWithImage:(UIImage *)image fromSource:(NSString *)source
{
    self = [super init];
    
    if (self) {
        self.originalImage = image;
        self.source = source;
        self.renderFile = [self getTempFileName];
    }

    return self;
}

- (NSMutableDictionary *)initialPrintOptions
{
    UIImage *image = (self.originalImage == nil) ? [UIImage imageWithData:[NSData dataWithContentsOfURL:self.documentFile]] : self.originalImage;

    if ((image.size.height / image.size.width) >= 1.0f) {
        return [NSMutableDictionary dictionaryWithObject:@"Portrait" forKey:@"poOrientation"];
    } else {
        return [NSMutableDictionary dictionaryWithObject:@"Landscape" forKey:@"poOrientation"];
    }
}

- (BOOL)canBePreviewed
{
    return YES;
}

- (BOOL)previewIsLocalRendered
{
    return YES;
}

- (NSURL *)getDocumentURL
{
    return [NSURL fileURLWithPath:self.renderFile];
}

- (NSInteger)getDocumentType
{
    return DocumentTypePhoto;
}

- (NSMutableDictionary *)getDocumentData
{
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:self.source, kDocumentTypePhotoSourceKey, nil];
}

- (NSString *)getTempFileName
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    dateFormat.dateFormat = @"MMddyyyyHHmmssSSS";
    return [NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"photo-%@.pdf", [dateFormat stringFromDate:date]]];
}

- (void)deleteTempFile
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.renderFile]) {
        [[NSFileManager defaultManager] removeItemAtPath:self.renderFile error:nil];
    }
}

- (void)renderDocumentWithPrintOptions:(NSDictionary *)options
{
    if (self.originalImage) {
        self.image = self.originalImage.copy;
    } else {
        self.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.documentFile]];
    }

    // If the file couldn't be opened return instead of rendering an empty document
    if (self.image == nil) return;

    if ([self.delegate respondsToSelector:@selector(didStartRendering)]) {
        [self.delegate didStartRendering];
    }

    [self performSelector:@selector(finishRenderWithPrintOptions:) withObject:options afterDelay:0];
}

- (void)finishRenderWithPrintOptions:(NSDictionary *)options
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        PaperSize *paperSize = [PaperSize getPaperSizeFromPrintOptions:options];
        float printResolution = paperSize.metric ? kPrintDMM : kPrintDPI;

        float paperWidth = paperSize.width * printResolution;
        float paperHeight = paperSize.height * printResolution;
        float topMargin = 0.25f * kPrintDPI;
        float bottomMargin = 0.25f * kPrintDPI;
        float leftMargin = 0.25f * kPrintDPI;
        float rightMargin = 0.25f * kPrintDPI;

        CGRect paperRect = CGRectMake(0, 0, paperWidth, paperHeight);
        CGRect printableRect = CGRectMake(leftMargin, topMargin, paperRect.size.width - leftMargin - rightMargin, paperRect.size.height - topMargin - bottomMargin);

        //if ( ((paperSize.width / paperSize.height) > 1.0f && (self.image.size.width / self.image.size.height) < 1.0f) ||
        //     ((paperSize.height / paperSize.width) > 1.0f && (self.image.size.height / self.image.size.width) < 1.0f) )
        //{
        //    self.image = [self rotateImage:self.image];
        //}
        self.image = [self.image resizedImageToFitInSize:CGSizeMake(printableRect.size.width, printableRect.size.height) scaleIfSmaller:NO];

        NSMutableData *pdfData = [NSMutableData data];
        UIGraphicsBeginPDFContextToData(pdfData, paperRect, nil);

        UIGraphicsBeginPDFPage();
        [self drawInCenter:self.image withBounds:printableRect];
        self.image = nil;

        UIGraphicsEndPDFContext();

        [self deleteTempFile];
        [pdfData writeToFile:self.renderFile atomically:YES];

        self.pdfDocument = CGPDFDocumentCreateWithURL((CFURLRef)[NSURL fileURLWithPath:self.renderFile]);
        self.pageCount = CGPDFDocumentGetNumberOfPages(self.pdfDocument);

        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(didFinishRendering)]) {
                [self.delegate didFinishRendering];
            }
        });
    });
}

- (UIImage *)rotateImage:(UIImage *)image
{
    CGSize rotatedSize = CGSizeMake(image.size.height, image.size.width);

    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();

    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    CGContextRotateCTM(bitmap, M_PI_2);

    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-image.size.width/2, -image.size.height/2, image.size.width, image.size.height), image.CGImage);

    UIImage *new = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return new;
}

- (void)drawInCenter:(UIImage *)image withBounds:(CGRect)bounds
{
    [image drawInRect:CGRectMake(bounds.origin.x + (bounds.size.width - image.size.width)/2, bounds.origin.y + (bounds.size.height - image.size.height)/2, image.size.width, image.size.height)];
}

- (NSUInteger)getPageCount
{
    if ([self canBePreviewed]) {
        if (!self.pageCount) {
            self.pageCount = CGPDFDocumentGetNumberOfPages(self.pdfDocument);
        }
        
        return self.pageCount;
    }
    
    return 0;
}

- (UIImage *)getPagePreviewAtIndex:(NSUInteger)index
                          withSize:(CGSize)size
                      forPaperSize:(CGSize)paperSize
{
    UIImage *image = [super getPagePreviewAtIndex:index withSize:size forPaperSize:paperSize];
    if (image) {
        return image;
    }

    NSString *imageURL = [NSString stringWithFormat:@"%@%lu", self.docUUID, (unsigned long)index];
    image = [SGImageCache imageForURL:imageURL];

    if (!image || !CGSizeEqualToSize(image.size, size)) {
        if ([self.delegate respondsToSelector:@selector(startActivityAtIndex:)]) {
            // Dispatch this call after the current run cycle so the view has time to be loaded completely
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate startActivityAtIndex:index];
            });
        }

        __weak __typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (!weakSelf.delegate) return;
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            CGPDFPageRef pageRef = CGPDFDocumentGetPage(strongSelf.pdfDocument, index + 1);

            UIGraphicsBeginImageContext(size);

            CGContextRef context = UIGraphicsGetCurrentContext();

            CGContextTranslateCTM(context, 0.0, size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextConcatCTM(context, CGPDFPageGetDrawingTransform(pageRef, kCGPDFCropBox, CGRectMake(0, 0, size.width, size.height), 0, true));

            CGContextDrawPDFPage(context, pageRef);

            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();

            if (newImage) {
                [SGImageCache addImage:newImage forURL:imageURL];

                if (index == 0) {
                    strongSelf.previewPage = newImage;
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([strongSelf.delegate respondsToSelector:@selector(setItemImage:atIndex:)]) {
                        [strongSelf.delegate setItemImage:newImage atIndex:index];
                    }
                });
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strongSelf.delegate respondsToSelector:@selector(endActivityAtIndex:)]) {
                    [strongSelf.delegate endActivityAtIndex:index];
                }
            });
        });
    }

    return image;
}

- (BOOL)showDocumentDefaultOrientation
{
    return NO;
}

- (void)dealloc
{
    CGPDFDocumentRelease(self.pdfDocument);
    [self deleteTempFile];
}

@end
