//
//  PDFDocument.m
//  PrinterOn
//
//  Created by Mark Burns on 12/3/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PDFDocument.h"

#import <SGImageCache/SGImageCache.h>

@interface PDFDocument ()

@property (nonatomic, strong) NSURL *documentFile;
@property (nonatomic, assign) CGPDFDocumentRef pdfDocument;
@property (nonatomic, assign) NSUInteger pageCount;

@end

@implementation PDFDocument

- (instancetype)initWithFileURL:(NSURL *)fileURL
{
    self = [super init];

    if (self) {
        self.documentFile = fileURL;
        self.pdfDocument = CGPDFDocumentCreateWithURL((CFURLRef)fileURL);
    }

    return self;
}

- (NSMutableDictionary *)initialPrintOptions
{
    CGPDFPageRef page = CGPDFDocumentGetPage(self.pdfDocument, 1);
    CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
    
    if ((pageRect.size.height / pageRect.size.width) >= 1.0f) {
        return [NSMutableDictionary dictionaryWithObject:@"Portrait" forKey:@"poOrientation"];
    } else {
        return [NSMutableDictionary dictionaryWithObject:@"Landscape" forKey:@"poOrientation"];
    }
}

- (BOOL)canBePreviewed
{
    return YES;
}

- (NSURL *)getDocumentURL
{
    return self.documentFile;
}

- (NSInteger)getDocumentType
{
    return DocumentTypeFile;
}

- (NSMutableDictionary *)getDocumentData
{
    return nil;
}

- (NSUInteger)getPageCount
{
    if ([self canBePreviewed]) {
        if (!self.pageCount) {
            self.pageCount = CGPDFDocumentGetNumberOfPages(self.pdfDocument);
        }
        
        return self.pageCount;
    }
    
    return 0;
}

- (UIImage *)getPagePreviewAtIndex:(NSUInteger)index
                          withSize:(CGSize)size
                      forPaperSize:(CGSize)paperSize
{
    UIImage *image = [super getPagePreviewAtIndex:index withSize:size forPaperSize:paperSize];
    if (image) {
        return image;
    }

    NSString *imageURL = [NSString stringWithFormat:@"%@%lu", self.docUUID, (unsigned long)index];
    image = [SGImageCache imageForURL:imageURL];

    if (!image || !CGSizeEqualToSize(image.size, size)) {
        if ([self.delegate respondsToSelector:@selector(startActivityAtIndex:)]) {
            // Dispatch this call after the current run cycle so the view has time to be loaded completely
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate startActivityAtIndex:index];
            });
        }

        __weak __typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (!weakSelf.delegate) return;
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            CGPDFPageRef pageRef = CGPDFDocumentGetPage(strongSelf.pdfDocument, index + 1);

            // If the paper size inside of the PDF is smaller then the paper size don't scale it up to fit
            // We do this because currently our server doesn't scale PDF's up when printed, it only scales down
            CGRect pdfPaperSize = CGPDFPageGetBoxRect(pageRef, kCGPDFCropBox);
            CGSize newSize = CGSizeMake(size.width, size.height);
            if (pdfPaperSize.size.width <= paperSize.width && pdfPaperSize.size.height <= paperSize.height) {
                newSize.height = newSize.height * (pdfPaperSize.size.height / paperSize.height);
                newSize.width = newSize.width * (pdfPaperSize.size.width / paperSize.width);
            }

            UIGraphicsBeginImageContext(newSize);

            CGContextRef context = UIGraphicsGetCurrentContext();

            CGContextTranslateCTM(context, 0.0, newSize.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextConcatCTM(context, CGPDFPageGetDrawingTransform(pageRef, kCGPDFCropBox, CGRectMake(0, 0, newSize.width, newSize.height), 0, true));

            CGContextDrawPDFPage(context, pageRef);

            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();

            if (newImage) {
                [SGImageCache addImage:newImage forURL:imageURL];

                if (index == 0) {
                    strongSelf.previewPage = newImage;
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([strongSelf.delegate respondsToSelector:@selector(setItemImage:atIndex:)]) {
                        [strongSelf.delegate setItemImage:newImage atIndex:index];
                    }
                });
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strongSelf.delegate respondsToSelector:@selector(endActivityAtIndex:)]) {
                    [strongSelf.delegate endActivityAtIndex:index];
                }
            });
        });
    }

    return image;
}

- (BOOL)showDocumentDefaultOrientation
{
    return NO;
}

- (void)dealloc
{
    CGPDFDocumentRelease(self.pdfDocument);
}

@end
