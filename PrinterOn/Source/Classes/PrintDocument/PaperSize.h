//
//  PaperSize.h
//  PrinterOn
//
//  Created by Mark Burns on 2014-10-22.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface PaperSize : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) float height;
@property (nonatomic, assign) float width;
@property (nonatomic, assign) BOOL metric;
@property (nonatomic, assign) CGSize size;

+ (int)getMediaSizeNumberFromPrintOptions:(NSDictionary *)options;
+ (PaperSize *)getPaperSizeFromPrintOptions:(NSDictionary *)options;
+ (PaperSize *)getPaperSizeFromMediaSizeNumber:(int)mediaNumber;
+ (CGRect)fitPaperSize:(PaperSize *)paperSize toAreaOfSize:(CGSize)size;

@end
