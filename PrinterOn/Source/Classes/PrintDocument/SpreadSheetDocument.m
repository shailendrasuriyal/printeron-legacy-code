//
//  SpreadSheetDocument.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-08-10.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "SpreadSheetDocument.h"

@implementation SpreadSheetDocument

- (NSMutableDictionary *)initialPrintOptions
{
    return [NSMutableDictionary dictionaryWithObject:@"Active" forKey:@"poXLSSheetRange"];
}

@end
