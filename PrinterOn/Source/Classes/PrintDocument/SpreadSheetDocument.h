//
//  SpreadSheetDocument.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-08-10.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "OtherDocument.h"

@interface SpreadSheetDocument : OtherDocument

@end
