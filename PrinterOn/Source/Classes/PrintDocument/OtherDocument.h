//
//  OtherDocument.h
//  PrinterOn
//
//  Created by Mark Burns on 12/4/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrintDocument.h"

@interface OtherDocument : PrintDocument

- (instancetype)initWithFileURL:(NSURL *)fileURL;

- (void)updatedServiceURL:(NSURL *)serviceURL;

@end
