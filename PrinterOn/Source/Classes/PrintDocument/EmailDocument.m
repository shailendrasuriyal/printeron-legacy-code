//
//  EmailDocument.m
//  PrinterOn
//
//  Created by Mark Burns on 1/10/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "EmailDocument.h"

#import <MailCore/MailCore.h>

@interface EmailDocument ()

@property (nonatomic, strong) MCOIMAPMessage *imapMessage;

@end

@implementation EmailDocument

- (instancetype)initWithWebView:(UIWebView *)webView
                       withIMAP:(MCOIMAPMessage *)imap
{
    self = [super initWithWebView:webView];

    if (self) {
        self.imapMessage = imap;
    }

    return self;
}

- (NSInteger)getDocumentType
{
    return DocumentTypeEmail;
}

- (NSMutableDictionary *)getDocumentData
{
    if (self.imapMessage) {
        NSString *from = (!self.imapMessage.header.from.displayName) ? self.imapMessage.header.from.mailbox : self.imapMessage.header.from.displayName;
        NSString *subject = self.imapMessage.header.subject.length == 0 ? NSLocalizedPONString(@"LABEL_NOSUBJECT", nil) : self.imapMessage.header.subject;

        return [NSMutableDictionary dictionaryWithObjectsAndKeys:from, kDocumentTypeEmailFromKey, subject, kDocumentTypeEmailSubjectKey, nil];
    }

    return nil;
}

- (NSString *)getTempFileName
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    dateFormat.dateFormat = @"MMddyyyyHHmmssSSS";
    return [NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"email-%@.pdf", [dateFormat stringFromDate:date]]];
}

@end
