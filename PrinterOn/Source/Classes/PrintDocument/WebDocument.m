//
//  WebDocument.m
//  PrinterOn
//
//  Created by Mark Burns on 11/23/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "WebDocument.h"

#import "PaperSize.h"

#import <SGImageCache/SGImageCache.h>
#import <WebKit/WebKit.h>

@interface WebDocument ()

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) WKWebView *kitWebView;
@property (nonatomic, strong) NSString *renderFile;
@property (nonatomic, assign) CGPDFDocumentRef pdfDocument;
@property (nonatomic, assign) NSUInteger pageCount;

@end

@implementation WebDocument

- (instancetype)initWithWebView:(UIWebView *)webView
{
    self = [super init];

    if (self) {
        self.renderFile = [self getTempFileName];
        self.webView = webView;
        self.webView.delegate = nil;
    }

    return self;
}

- (instancetype)initWithWebKitView:(WKWebView *)webView
{
    self = [super init];
    
    if (self) {
        self.renderFile = [self getTempFileName];
        self.kitWebView = webView;
    }
    
    return self;
}

- (instancetype)initWithURL:(NSURL *)url
{
    self = [super init];

    if (self) {
        self.renderFile = [self getTempFileName];

        self.webView = [UIWebView new];
        self.webView.scalesPageToFit = YES;

        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:request];
    }

    return self;
}

- (NSMutableDictionary *)initialPrintOptions
{
    return [NSMutableDictionary dictionaryWithObject:@"Portrait" forKey:@"poOrientation"];
}

- (BOOL)canBePreviewed
{
    return YES;
}

- (BOOL)previewIsLocalRendered
{
    return YES;
}

- (BOOL)renderBlocksMainUI
{
    return YES;
}

- (NSURL *)getDocumentURL
{
    return [NSURL fileURLWithPath:self.renderFile];
}

- (NSInteger)getDocumentType
{
    return DocumentTypeWeb;
}

- (NSDictionary *)getDocumentData
{
    NSURL *url = self.webView ? self.webView.request.URL : self.kitWebView.URL;
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:url.absoluteString, kDocumentTypeWebURLKey, url.host, kDocumentTypeWebHostNameKey, nil];
}

- (void)renderDocumentWithPrintOptions:(NSDictionary *)options
{
    if ([self.delegate respondsToSelector:@selector(didStartRendering)]) {
        [self.delegate didStartRendering];
    }

    [self performSelector:@selector(performRenderWithPrintOptions:) withObject:options afterDelay:0];
}

- (void)performRenderWithPrintOptions:(NSDictionary *)options
{
    UIPrintPageRenderer *renderer = [UIPrintPageRenderer new];
    if (self.webView) {
        [renderer addPrintFormatter:self.webView.viewPrintFormatter startingAtPageAtIndex:0];
    } else {
        [renderer addPrintFormatter:self.kitWebView.viewPrintFormatter startingAtPageAtIndex:0];
    }

    PaperSize *paperSize = [PaperSize getPaperSizeFromPrintOptions:options];
    float printResolution = paperSize.metric ? kPrintDMM : kPrintDPI;

    float paperWidth = paperSize.width * printResolution;
    float paperHeight = paperSize.height * printResolution;
    float topMargin = 0.25f * kPrintDPI;
    float bottomMargin = 0.25f * kPrintDPI;
    float leftMargin = 0.25f * kPrintDPI;
    float rightMargin = 0.25f * kPrintDPI;

    CGRect paperRect = CGRectMake(0, 0, paperWidth, paperHeight);
    CGRect printableRect = CGRectMake(leftMargin, topMargin, paperRect.size.width - leftMargin - rightMargin, paperRect.size.height - topMargin - bottomMargin);
    [renderer setValue:[NSValue valueWithCGRect:paperRect] forKey:@"paperRect"];
    [renderer setValue:[NSValue valueWithCGRect:printableRect] forKey:@"printableRect"];
    
    NSMutableData *pdfData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(pdfData, paperRect, nil);
    
    NSInteger pages = [renderer numberOfPages];
    [renderer prepareForDrawingPages:NSMakeRange(0, pages)];
    
    for (NSInteger i = 0; i < pages; i++) {
        UIGraphicsBeginPDFPage();
        [renderer drawPageAtIndex:i inRect:printableRect];
    }

    UIGraphicsEndPDFContext();
    
    [self deleteTempFile];
    [pdfData writeToFile:self.renderFile atomically:YES];
    
    self.pdfDocument = CGPDFDocumentCreateWithURL((CFURLRef)[NSURL fileURLWithPath:self.renderFile]);
    self.pageCount = CGPDFDocumentGetNumberOfPages(self.pdfDocument);
    
    if ([self.delegate respondsToSelector:@selector(didFinishRendering)]) {
        [self.delegate didFinishRendering];
    }
}

- (NSString *)getTempFileName
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    dateFormat.dateFormat = @"MMddyyyyHHmmssSSS";
    return [NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"web-%@.pdf", [dateFormat stringFromDate:date]]];
}

- (void)deleteTempFile
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.renderFile]) {
        [[NSFileManager defaultManager] removeItemAtPath:self.renderFile error:nil];
    }
}

- (NSUInteger)getPageCount
{
    if ([self canBePreviewed]) {
        if (!self.pageCount) {
            self.pageCount = CGPDFDocumentGetNumberOfPages(self.pdfDocument);
        }

        return self.pageCount;
    }

    return 0;
}

- (UIImage *)getPagePreviewAtIndex:(NSUInteger)index
                          withSize:(CGSize)size
                      forPaperSize:(CGSize)paperSize
{
    UIImage *image = [super getPagePreviewAtIndex:index withSize:size forPaperSize:paperSize];
    if (image) {
        return image;
    }

    NSString *imageURL = [NSString stringWithFormat:@"%@%lu", self.docUUID, (unsigned long)index];
    image = [SGImageCache imageForURL:imageURL];

    if (!image || !CGSizeEqualToSize(image.size, size)) {
        if ([self.delegate respondsToSelector:@selector(startActivityAtIndex:)]) {
            // Dispatch this call after the current run cycle so the view has time to be loaded completely
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate startActivityAtIndex:index];
            });
        }
        
        __weak __typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (!weakSelf.delegate) return;
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            CGPDFPageRef pageRef = CGPDFDocumentGetPage(self.pdfDocument, index + 1);

            UIGraphicsBeginImageContext(size);

            CGContextRef context = UIGraphicsGetCurrentContext();

            CGContextTranslateCTM(context, 0.0, size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextConcatCTM(context, CGPDFPageGetDrawingTransform(pageRef, kCGPDFCropBox, CGRectMake(0, 0, size.width, size.height), 0, true));

            CGContextDrawPDFPage(context, pageRef);

            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();

            if (newImage) {
                [SGImageCache addImage:newImage forURL:imageURL];

                if (index == 0) {
                    strongSelf.previewPage = newImage;
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([strongSelf.delegate respondsToSelector:@selector(setItemImage:atIndex:)]) {
                        [strongSelf.delegate setItemImage:newImage atIndex:index];
                    }
                });
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strongSelf.delegate respondsToSelector:@selector(endActivityAtIndex:)]) {
                    [strongSelf.delegate endActivityAtIndex:index];
                }
            });
        });
    }

    return image;
}

- (BOOL)showDocumentDefaultOrientation
{
    return NO;
}

- (void)dealloc
{
    CGPDFDocumentRelease(self.pdfDocument);
    [self deleteTempFile];
}

@end
