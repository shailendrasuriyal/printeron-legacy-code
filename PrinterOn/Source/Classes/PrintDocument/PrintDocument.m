//
//  PrintDocument.m
//  PrinterOn
//
//  Created by Mark Burns on 11/23/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrintDocument.h"

#import "DeviceCapabilities.h"
#import "ImageDocument.h"
#import "OtherDocument.h"
#import "PDFDocument.h"
#import "SpreadSheetDocument.h"
#import "WebDocument.h"

NSString* const kDocumentTypeEmailFromKey = @"msgFrom";
NSString* const kDocumentTypeEmailSubjectKey = @"msgSubject";
NSString* const kDocumentTypeFileNameKey = @"docFileName";
NSString* const kDocumentTypePhotoSourceKey = @"photoSource";
NSString* const kDocumentTypeWebHostNameKey = @"hostName";
NSString* const kDocumentTypeWebURLKey = @"URL";
NSString* const kDocumentReprintKey = @"docReprint";

float const kPrintDPI = 72.0f;
float const kPrintDMM = 2.834645669291f;

@implementation PrintDocument

+ (PrintDocument *)getDocumentFromFile:(NSURL *)fileURL fromSource:(NSString *)source
{
    PrintDocument *document = nil;

    if (fileURL && fileURL.isFileURL) {
        if ([fileURL.path hasPrefix:NSTemporaryDirectory()]) {
            // Sometimes it seems that files put in the "tmp" directory on the device are removed before we are done with them.
            // For example on iOS 9 a file from a document provider is removed after 30-60s.  So we move them to a new location
            // in the "tmp" directory so they can't be prematurely erased before we are done with them.
            fileURL = [PrintDocument moveDocumentToTempInbox:fileURL];
        }

        CFStringRef ext = (__bridge CFStringRef)fileURL.pathExtension;
        CFStringRef uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, ext, NULL);

        if (UTTypeEqual(uti, kUTTypePDF)) {
            document = [[PDFDocument alloc] initWithFileURL:fileURL];
        } else if (UTTypeConformsTo(uti, kUTTypeImage)) {
            document = [[ImageDocument alloc] initWithFileURL:fileURL fromSource:source];
        } else {
            NSString *utiString = (__bridge NSString*)uti;
            NSArray *sheetArray = [NSArray arrayWithObjects:
                                   @"com.microsoft.excel.xls",
                                   @"com.microsoft.excel.xlt",
                                   @"com.microsoft.excel.sheet.binary",
                                   @"com.microsoft.excel.sheet.binary.macroenabled",
                                   @"org.openxmlformats.spreadsheetml.sheet",
                                   @"org.openxmlformats.spreadsheetml.sheet.macroenabled",
                                   @"org.openxmlformats.spreadsheetml.template",
                                   @"org.openxmlformats.spreadsheetml.template.macroenabled",
                                   @"org.openoffice.spreadsheet",
                                   @"org.openoffice.spreadsheet-template",
                                   @"org.oasis-open.opendocument.spreadsheet",
                                   @"org.oasis-open.opendocument.spreadsheet-template",
                                   nil];

            if ([sheetArray containsObject:utiString]) {
                document = [[SpreadSheetDocument alloc] initWithFileURL:fileURL];
            } else {
                document = [[OtherDocument alloc] initWithFileURL:fileURL];
            }
        }

        CFRelease(uti);
    }

    return document;
}

+ (NSURL *)moveDocumentToTempInbox:(NSURL *)fileURL
{
    NSString *inboxPath = [[NSTemporaryDirectory() stringByAppendingPathComponent:@"Inbox"] stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
    NSURL *destURL = [NSURL fileURLWithPath:[inboxPath stringByAppendingPathComponent:fileURL.lastPathComponent]];

    // Check to see if the directory exists, create it if it doesn't
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    BOOL exist = [fileManager fileExistsAtPath:inboxPath isDirectory:&isDir];

    if (!exist || (exist && !isDir)) {
        [fileManager createDirectoryAtPath:inboxPath withIntermediateDirectories:YES attributes:nil error:nil];
    }

    if (![fileManager fileExistsAtPath:destURL.path]) {
        if ([fileManager moveItemAtPath:fileURL.path toPath:destURL.path error:nil]) {
            return destURL;
        }
    }

    return fileURL;
}

- (instancetype)init
{
    self = [super init];

    if (self) {
        self.docUUID = [[NSUUID UUID] UUIDString];
    }

    return self;
}

- (NSMutableDictionary *)initialPrintOptions
{
    return [NSMutableDictionary dictionary];
}

- (BOOL)canBePreviewed
{
    return NO;
}

- (BOOL)previewIsLocalRendered
{
    return NO;
}

- (BOOL)previewIsServerRendered
{
    return NO;
}

- (BOOL)renderBlocksMainUI
{
    return NO;
}

- (void)renderDocumentWithPrintOptions:(NSDictionary *)options
{
}

- (NSURL *)getDocumentURL
{
    return nil;
}

- (NSInteger)getDocumentType
{
    return DocumentTypeFile;
}

- (NSMutableDictionary *)getDocumentData
{
    return nil;
}

- (NSUInteger)getPageCount
{
    return 0;
}

- (UIImage *)getPagePreviewAtIndex:(NSUInteger)index
                          withSize:(CGSize)size
                      forPaperSize:(CGSize)paperSize
{
    if (index == 0 && self.previewPage) {
        return self.previewPage;
    }

    return nil;
}

- (BOOL)showDocumentDefaultOrientation
{
    return YES;
}

+ (NSArray *)documentUTIs
{
    NSMutableArray *UTIs = [NSMutableArray array];

    // Fax
    [UTIs addObject:@"public.fax"];

    // Images
    [UTIs addObject:(NSString *)kUTTypeImage];
    [UTIs addObject:(NSString *)kUTTypeAppleICNS];
    [UTIs addObject:(NSString *)kUTTypeBMP];
    [UTIs addObject:(NSString *)kUTTypeGIF];
    [UTIs addObject:(NSString *)kUTTypeICO];
    [UTIs addObject:(NSString *)kUTTypeJPEG];
    [UTIs addObject:(NSString *)kUTTypeJPEG2000];
    [UTIs addObject:(NSString *)kUTTypePICT];
    [UTIs addObject:(NSString *)kUTTypePNG];
    [UTIs addObject:(NSString *)kUTTypeQuickTimeImage];
    [UTIs addObject:(NSString *)kUTTypeTIFF];
    [UTIs addObject:(NSString *)kUTTypeRawImage];
    [UTIs addObject:(NSString *)kUTTypeScalableVectorGraphics];

    // Hangul Word Processor
    [UTIs addObject:@"com.printeron.hangul"];

    // Microsoft Excel
    [UTIs addObject:@"com.microsoft.excel.xls"];
    [UTIs addObject:@"com.microsoft.excel.xlt"];
    [UTIs addObject:@"com.microsoft.excel.sheet.binary"];
    [UTIs addObject:@"com.microsoft.excel.sheet.binary.macroenabled"];
    [UTIs addObject:@"org.openxmlformats.spreadsheetml.sheet"];
    [UTIs addObject:@"org.openxmlformats.spreadsheetml.sheet.macroenabled"];
    [UTIs addObject:@"org.openxmlformats.spreadsheetml.template"];
    [UTIs addObject:@"org.openxmlformats.spreadsheetml.template.macroenabled"];

    // Microsoft PowerPoint
    [UTIs addObject:@"com.microsoft.powerpoint.ppt"];
    [UTIs addObject:@"com.microsoft.powerpoint.pot"];
    [UTIs addObject:@"org.openxmlformats.presentationml.presentation"];
    [UTIs addObject:@"org.openxmlformats.presentationml.presentation.macroenabled"];
    [UTIs addObject:@"org.openxmlformats.presentationml.slideshow"];
    [UTIs addObject:@"org.openxmlformats.presentationml.slideshow.macroenabled"];
    [UTIs addObject:@"org.openxmlformats.presentationml.template"];
    [UTIs addObject:@"org.openxmlformats.presentationml.template.macroenabled"];

    // Microsoft Visio
    [UTIs addObject:@"com.printeron.visio"];

    // Microsoft Word
    [UTIs addObject:@"com.microsoft.word.doc"];
    [UTIs addObject:@"com.microsoft.word.dot"];
    [UTIs addObject:@"org.openxmlformats.wordprocessingml.document"];
    [UTIs addObject:@"org.openxmlformats.wordprocessingml.document.macroenabled"];
    [UTIs addObject:@"org.openxmlformats.wordprocessingml.template"];
    [UTIs addObject:@"org.openxmlformats.wordprocessingml.template.macroenabled"];

    // OpenDocument Formula
    [UTIs addObject:@"org.openoffice.formula"];
    [UTIs addObject:@"org.openoffice.formula-template"];
    [UTIs addObject:@"org.oasis-open.opendocument.formula"];
    [UTIs addObject:@"org.oasis-open.opendocument.formula-template"];

    // OpenDocument Graphics
    [UTIs addObject:@"org.openoffice.graphics"];
    [UTIs addObject:@"org.openoffice.graphics-template"];
    [UTIs addObject:@"org.oasis-open.opendocument.graphics"];
    [UTIs addObject:@"org.oasis-open.opendocument.graphics-template"];

    // OpenDocument Presentation
    [UTIs addObject:@"org.openoffice.presentation"];
    [UTIs addObject:@"org.openoffice.presentation-template"];
    [UTIs addObject:@"org.oasis-open.opendocument.presentation"];
    [UTIs addObject:@"org.oasis-open.opendocument.presentation-template"];

    // OpenDocument Spreadsheet
    [UTIs addObject:@"org.openoffice.spreadsheet"];
    [UTIs addObject:@"org.openoffice.spreadsheet-template"];
    [UTIs addObject:@"org.oasis-open.opendocument.spreadsheet"];
    [UTIs addObject:@"org.oasis-open.opendocument.spreadsheet-template"];

    // OpenDocument Text
    [UTIs addObject:@"org.openoffice.text"];
    [UTIs addObject:@"org.openoffice.text-template"];
    [UTIs addObject:@"org.oasis-open.opendocument.text"];
    [UTIs addObject:@"org.oasis-open.opendocument.text-template"];

    // PDF
    [UTIs addObject:(NSString *)kUTTypePDF];

    // Text
    [UTIs addObject:(NSString *)kUTTypeText];
    [UTIs addObject:(NSString *)kUTTypePlainText];
    [UTIs addObject:(NSString *)kUTTypeUTF8PlainText];
    [UTIs addObject:(NSString *)kUTTypeUTF16ExternalPlainText];
    [UTIs addObject:(NSString *)kUTTypeUTF16PlainText];
    [UTIs addObject:(NSString *)kUTTypeRTF];
    [UTIs addObject:(NSString *)kUTTypeDelimitedText];
    [UTIs addObject:(NSString *)kUTTypeCommaSeparatedText];
    [UTIs addObject:(NSString *)kUTTypeTabSeparatedText];
    [UTIs addObject:(NSString *)kUTTypeUTF8TabSeparatedText];

    [UTIs addObject:(NSString *)kUTTypeHTML];
    [UTIs addObject:(NSString *)kUTTypeXML];
    [UTIs addObject:(NSString *)kUTTypeJSON];

    // XPS
    [UTIs addObject:@"com.printeron.xps"];
    
    return UTIs;
}

+ (BOOL)isSupportedFileType:(NSString *)filePath
{
    NSArray *fileExtArray = [self documentUTIs];
    
    CFStringRef ext = (__bridge CFStringRef)filePath.pathExtension;
    CFStringRef uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, ext, NULL);
    
    BOOL supported = NO;
    for (NSString *file in fileExtArray) {
        CFStringRef e = (__bridge CFStringRef)file;
        if (UTTypeConformsTo(uti, e)) {
            supported = YES;
            break;
        }
    }
    
    CFRelease(uti);
    return supported;
}

@end
