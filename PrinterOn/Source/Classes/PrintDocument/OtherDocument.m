//
//  OtherDocument.m
//  PrinterOn
//
//  Created by Mark Burns on 12/4/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "OtherDocument.h"

#import "DocProcess.h"
#import "DocStatus.h"
#import "Printer.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "ServiceDescription.h"

#import "AFNetworking.h"
#import <SGImageCache/SGImageCache.h>

@interface OtherDocument ()

@property (nonatomic, strong) NSURL *documentFile;
@property (nonatomic, strong) NSURL *previewServiceURL;
@property (nonatomic, assign) BOOL previewCapabilityAvailable;
@property (nonatomic, assign) BOOL documentUploadInProgress;
@property (nonatomic, assign) BOOL documentUploadAttempted;
@property (nonatomic, assign) NSInteger docPageCount;
@property (nonatomic, strong) NSString *previewURI;
@property (nonatomic, strong) NSString *previewPrefix;
@property (nonatomic, strong) NSString *previewExt;

@end

@implementation OtherDocument

- (instancetype)initWithFileURL:(NSURL *)fileURL
{
    self = [super init];

    if (self) {
        self.documentFile = fileURL;
        self.docPageCount = 0;
    }

    return self;
}

- (BOOL)canBePreviewed
{
    return YES;
}

- (BOOL)previewIsServerRendered
{
    return YES;
}

- (NSURL *)getDocumentURL
{
    return self.documentFile;
}

- (NSInteger)getDocumentType
{
    return DocumentTypeFile;
}

- (NSMutableDictionary *)getDocumentData
{
    return nil;
}

- (NSUInteger)getPageCount;
{
    return self.docPageCount;
}

- (void)updatedServiceURL:(NSURL *)serviceURL;
{
    NSString *URL = [DocProcess getBaseURLForService:serviceURL.absoluteString];

    if (![URL isEqualToString:self.previewServiceURL.absoluteString]) {
        self.previewServiceURL = nil;
        self.previewCapabilityAvailable = NO;
        self.documentUploadAttempted = NO;
    }
}

- (void)renderDocumentWithPrintOptions:(NSDictionary *)options
{
    // We already have preview information set, so we have already uploaded the document successfully to a preview server.
    // This ensures we only upload the document one time for a preview.
    if (self.previewURI != nil) {
        // We call finishPreview because this will trigger the view to refresh.
        [self finishPreview];
        return;
    }

    // Check to see if we have a printer set.
    // No printer means we can't preview this document.
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    if (printer == nil) {
        // We call finishPreview because this will trigger the view to refresh.
        [self finishPreview];
        return;
    }

    NSURL *serviceURL = [printer getDocAPIAddress];
    serviceURL = [NSURL URLWithString:[DocProcess getBaseURLForService:serviceURL.absoluteString]];

    // Check if we have already queried capabilities for this service URL
    if (self.previewServiceURL) {
        if (self.previewCapabilityAvailable) {
            [self startPreview];

            dispatch_async(dispatch_get_main_queue(), ^{
                [self performPreviewWithPrinter:printer];
            });
        }
    } else {
        [self startPreview];

        // Query service capabilities for this printer
        __weak __typeof(self)weakSelf = self;
        [ServiceDescription capabilitiesForServiceWithURL:serviceURL forceUpdate:NO completionBlock:^(NSManagedObjectID *capabilitiesID) {
            if (!weakSelf.delegate) return;
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            if (capabilitiesID) {
                NSManagedObjectContext *context = [CoreDataManager getManagedContext];
                ServiceCapabilities *capabilities = [context objectWithID:capabilitiesID];
                strongSelf.previewCapabilityAvailable = [capabilities.documentPreview boolValue];
            }

            strongSelf.previewServiceURL = serviceURL;
            if (strongSelf.previewCapabilityAvailable) {
                [strongSelf performPreviewWithPrinter:printer];
            } else {
                if (!strongSelf.documentUploadInProgress) {
                    [strongSelf finishPreview];
                }
            }
        }];
    }
}

- (void)performPreviewWithPrinter:(Printer *)printer
{
    if (self.documentUploadInProgress) {
        return;
    }

    if (self.documentUploadAttempted) {
        [self finishPreview];
        return;
    }

    __weak __typeof(self)weakSelf = self;
    [DocProcess PreviewDocument:self.documentFile forPrinter:printer success:^(DocProcess *process) {
        if (!weakSelf.delegate) return;
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        strongSelf.documentUploadAttempted = YES;

        // The result is actually an error so send to failure
        if (![process.returnCode isEqualToString:@"0"]) {
            [strongSelf finishPreview];
            return;
        }

        // A job with jobState 7 (cancelled) or 8 (aborted) are considered cancelled
        int jobState = process.jobState.intValue;
        if (jobState == 7 || jobState == 8) {
            [strongSelf finishPreview];
            return;
        }

        [strongSelf checkPreviewStatusForJob:process.jobReferenceID forPrinter:printer];
    } failure:^(NSError *error) {
        if (!weakSelf.delegate) return;
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        strongSelf.documentUploadAttempted = YES;
        [strongSelf finishPreview];
    } completionHandler:^(NSURLSessionTask *task, NSError *error) {
        if (!weakSelf.delegate) return;
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            strongSelf.documentUploadAttempted = YES;
            [strongSelf finishPreview];
        } else {
            strongSelf.documentUploadInProgress = YES;
            [task resume];
        }
    }];
}

- (void)checkPreviewStatusForJob:(NSString *)jobID forPrinter:(Printer *)printer
{
    __weak __typeof(self)weakSelf = self;
    [DocStatus getPreviewStatus:jobID forPrinter:printer success:^(DocStatus *status) {

        if (!weakSelf.delegate) return;
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        // The result is actually an error so send to failure
        if (![status.returnCode isEqualToString:@"0"]) {
            [strongSelf finishPreview];
            return;
        }

        // A job with jobState 7 (cancelled) or 8 (aborted) are considered cancelled
        int jobState = status.jobState.intValue;
        if (jobState == 4 || jobState == 6 || jobState == 7 || jobState == 8) {
            [strongSelf finishPreview];
            return;
        }

        // The job was completed successfully
        if (jobState == 9) {
            strongSelf.docPageCount = status.docPageCount.integerValue;
            strongSelf.previewURI = status.outputURI;
            strongSelf.previewPrefix = status.outputFilePrefix;
            strongSelf.previewExt = status.outputFormat;

            [strongSelf finishPreview];
            return;
        }

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if (!weakSelf.delegate) return;
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            [strongSelf checkPreviewStatusForJob:jobID forPrinter:printer];
        });

    } failure:^(NSError *error) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 15 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if (!weakSelf.delegate) return;
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            [strongSelf checkPreviewStatusForJob:jobID forPrinter:printer];
        });
    } completionHandler:^(NSURLSessionTask *task, NSError *error) {
        if (!weakSelf.delegate) return;
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf finishPreview];
        } else {
            [task resume];
        }
    }];
}

- (void)startPreview
{
    if ([self.delegate respondsToSelector:@selector(didStartPreviewing)]) {
        [self.delegate didStartPreviewing];
    }
}

- (void)finishPreview
{
    self.documentUploadInProgress = NO;

    if ([self.delegate respondsToSelector:@selector(didFinishPreviewing)]) {
        [self.delegate didFinishPreviewing];
    }
}

- (UIImage *)getPagePreviewAtIndex:(NSUInteger)index
                          withSize:(CGSize)size
                      forPaperSize:(CGSize)paperSize
{
    UIImage *image = [super getPagePreviewAtIndex:index withSize:size forPaperSize:paperSize];
    if (image) {
        return image;
    }

    if (self.previewURI == nil || self.previewPrefix == nil || self.previewExt == nil) {
        return [UIImage new];
    }

    NSString *imageURL = [NSString stringWithFormat:@"%@/%@%lu.%@", self.previewURI, self.previewPrefix, (unsigned long)index + 1, self.previewExt];
    image = [SGImageCache imageForURL:imageURL];
    if (image) {
        return image;
    }

    if ([self.delegate respondsToSelector:@selector(startActivityAtIndex:)]) {
        // Dispatch this call after the current run cycle so the view has time to be loaded completely
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate startActivityAtIndex:index];
        });
    }

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
    manager.responseSerializer = [AFImageResponseSerializer serializer];

    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    __weak __typeof(self)weakSelf = self;
    [manager GET:imageURL parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (!weakSelf.delegate) return;
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if ([responseObject isKindOfClass:[UIImage class]]) {
                UIImage *image = (UIImage *)responseObject;
                if (image) {
                    [SGImageCache addImage:image forURL:imageURL];

                    if (index == 0) {
                        strongSelf.previewPage = image;
                    }

                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([strongSelf.delegate respondsToSelector:@selector(setItemImage:atIndex:)]) {
                            [strongSelf.delegate setItemImage:image atIndex:index];
                        }
                    });
                }
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strongSelf.delegate respondsToSelector:@selector(endActivityAtIndex:)]) {
                    [strongSelf.delegate endActivityAtIndex:index];
                }
            });
        });
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (!weakSelf.delegate) return;
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        dispatch_async(dispatch_get_main_queue(), ^{
            if ([strongSelf.delegate respondsToSelector:@selector(endActivityAtIndex:)]) {
                [strongSelf.delegate endActivityAtIndex:index];
            }
        });
    }];

    return nil;
}

@end
