//
//  PDFDocument.h
//  PrinterOn
//
//  Created by Mark Burns on 12/3/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrintDocument.h"

@interface PDFDocument : PrintDocument

- (instancetype)initWithFileURL:(NSURL *)fileURL;

@end
