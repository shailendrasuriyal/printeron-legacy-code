//
//  PaperSize.m
//  PrinterOn
//
//  Created by Mark Burns on 2014-10-22.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PaperSize.h"

#import "PrintDocument.h"

@implementation PaperSize

+ (int)getMediaSizeNumberFromPrintOptions:(NSDictionary *)options
{
    NSString *mediaSizeNum = options[@"poMediaSizeNum"];
    int mediaNumber = (mediaSizeNum != nil) ? mediaSizeNum.intValue : 1;
    return mediaNumber;
}

+ (PaperSize *)getPaperSizeFromPrintOptions:(NSDictionary *)options
{
    PaperSize *mediaSize = [PaperSize getPaperSizeFromMediaSizeNumber:[PaperSize getMediaSizeNumberFromPrintOptions:options]];
    NSString *orientation = options[@"poOrientation"];
    if (orientation != nil) {
        if ( ((mediaSize.width / mediaSize.height) > 1.0f && ([orientation.lowercaseString isEqualToString:@"portrait"])) ||
            ((mediaSize.height / mediaSize.width) > 1.0f && ([orientation.lowercaseString isEqualToString:@"landscape"])) ) {
            
            // Switch the height and width of the paper size to change orientation
            CGSize newSize = CGSizeMake(mediaSize.size.height, mediaSize.size.width);
            mediaSize.size = newSize;
            mediaSize.height = mediaSize.height + mediaSize.width;
            mediaSize.width = mediaSize.height - mediaSize.width;
            mediaSize.height = mediaSize.height - mediaSize.width;
        }
    }
    return mediaSize;
}

+ (PaperSize *)getPaperSizeFromMediaSizeNumber:(int)mediaNumber
{
    PaperSize *paperSize = [PaperSize new];

    switch (mediaNumber) {

		case 1: // Letter - 8.5 x 11.0 in
		{
		    paperSize.title = @"Letter";
		    paperSize.width = 8.5f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 2: // Letter Small - 8.5 x 11.0 in
		{
		    paperSize.title = @"Letter Small";
		    paperSize.width = 8.5f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 3: // Tabloid - 11.0 x 17.0 in
		{
		    paperSize.title = @"Tabloid";
		    paperSize.width = 11.0f;
		    paperSize.height = 17.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 4: // Ledger - 17.0 x 11.0 in
		{
		    paperSize.title = @"Ledger";
		    paperSize.width = 17.0f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 5: // Legal - 8.5 x 14.0 in
		{
		    paperSize.title = @"Legal";
		    paperSize.width = 8.5f;
		    paperSize.height = 14.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 6: // Statement - 5.5 x 8.5 in
		{
		    paperSize.title = @"Statement";
		    paperSize.width = 5.5f;
		    paperSize.height = 8.5f;
		    paperSize.metric = NO;
		    break;
		}

		case 7: // Executive - 7.25 x 10.5 in
		{
		    paperSize.title = @"Executive";
		    paperSize.width = 7.25f;
		    paperSize.height = 10.5f;
		    paperSize.metric = NO;
		    break;
		}

		case 8: // A3 - 297.0 x 420.0 mm
		{
		    paperSize.title = @"A3";
		    paperSize.width = 297.0f;
		    paperSize.height = 420.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 9: // A4 - 210.0 x 297.0 mm
		{
		    paperSize.title = @"A4";
		    paperSize.width = 210.0f;
		    paperSize.height = 297.0f;
		    paperSize.metric = YES;
		    break;
		}
		
		case 10: // A4 Small - 210.0 x 297.0 mm
		{
		    paperSize.title = @"A4 Small";
		    paperSize.width = 210.0f;
		    paperSize.height = 297.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 11: // A5 - 148.0 x 210.0 mm
		{
		    paperSize.title = @"A5";
		    paperSize.width = 148.0f;
		    paperSize.height = 210.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 12: // B4 (JIS) - 250.0 x 354.0 mm
		{
		    paperSize.title = @"B4 (JIS)";
		    paperSize.width = 250.0f;
		    paperSize.height = 354.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 13: // B5 (JIS) - 182.0 x 257.0 mm
		{
		    paperSize.title = @"B5 (JIS)";
		    paperSize.width = 182.0f;
		    paperSize.height = 257.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 14: // Folio - 8.5 x 13.0 in
		{
		    paperSize.title = @"Folio";
		    paperSize.width = 8.5f;
		    paperSize.height = 13.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 15: // Quarto - 215.0 x 275.0 mm
		{
		    paperSize.title = @"Quarto";
		    paperSize.width = 215.0f;
		    paperSize.height = 275.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 16: // 10x14 in - 10.0 x 14.0 in
		{
		    paperSize.title = @"10x14 in";
		    paperSize.width = 10.0f;
		    paperSize.height = 14.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 17: // 11x17 in - 11.0 x 17.0 in
		{
		    paperSize.title = @"11x17 in";
		    paperSize.width = 11.0f;
		    paperSize.height = 17.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 18: // Note - 8.5 x 11.0 in
		{
		    paperSize.title = @"Note";
		    paperSize.width = 8.5f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 19: // Envelope #9 - 3.875 x 8.875 in
		{
		    paperSize.title = @"Envelope #9";
		    paperSize.width = 3.875f;
		    paperSize.height = 8.875f;
		    paperSize.metric = NO;
		    break;
		}

		case 20: // Envelope #10 - 4.125 x 9.5 in
		{
		    paperSize.title = @"Envelope #10";
		    paperSize.width = 4.125f;
		    paperSize.height = 9.5f;
		    paperSize.metric = NO;
		    break;
		}

		case 21: // Envelope #11 - 4.5 x 10.375 in
		{
		    paperSize.title = @"Envelope #11";
		    paperSize.width = 4.5f;
		    paperSize.height = 10.375f;
		    paperSize.metric = NO;
		    break;
		}

		case 22: // Envelope #12 - 4.276 x 11.0 in
		{
		    paperSize.title = @"Envelope #12";
		    paperSize.width = 4.276f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 23: // Envelope #14 - 5.0 x 11.5 in
		{
		    paperSize.title = @"Envelope #14";
		    paperSize.width = 5.0f;
		    paperSize.height = 11.5f;
		    paperSize.metric = NO;
		    break;
		}

		case 27: // Envelope DL - 110.0 x 220.0 mm
		{
		    paperSize.title = @"Envelope DL";
		    paperSize.width = 110.0f;
		    paperSize.height = 220.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 28: // Envelope C5 - 162.0 x 229.0 mm
		{
		    paperSize.title = @"Envelope C5";
		    paperSize.width = 162.0f;
		    paperSize.height = 229.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 29: // Envelope C3 - 324.0 x 458.0 mm
		{
		    paperSize.title = @"Envelope C3";
		    paperSize.width = 324.0f;
		    paperSize.height = 458.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 30: // Envelope C4 - 229.0 x 324.0 mm
		{
		    paperSize.title = @"Envelope C4";
		    paperSize.width = 229.0f;
		    paperSize.height = 324.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 31: // Envelope C6 - 114.0 x 162.0 mm
		{
		    paperSize.title = @"Envelope C6";
		    paperSize.width = 114.0f;
		    paperSize.height = 162.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 32: // Envelope C65 - 114.0 x 229.0 mm
		{
		    paperSize.title = @"Envelope C65";
		    paperSize.width = 114.0f;
		    paperSize.height = 229.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 33: // Envelope B4 - 250.0 x 353.0 mm
		{
		    paperSize.title = @"Envelope B4";
		    paperSize.width = 250.0f;
		    paperSize.height = 353.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 34: // Envelope B5 - 176.0 x 250.0 mm
		{
		    paperSize.title = @"Envelope B5";
		    paperSize.width = 176.0f;
		    paperSize.height = 250.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 35: // Envelope B6 - 176.0 x 125.0 mm
		{
		    paperSize.title = @"Envelope B6";
		    paperSize.width = 176.0f;
		    paperSize.height = 125.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 36: // Envelope - 110.0 x 230.0 mm
		{
		    paperSize.title = @"Envelope";
		    paperSize.width = 110.0f;
		    paperSize.height = 230.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 37: // Envelope Monarch - 3.875 x 7.0 in
		{
		    paperSize.title = @"Envelope Monarch";
		    paperSize.width = 3.875f;
		    paperSize.height = 7.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 38: // 6 3/4 Envelope - 3.625 x 6.5 in
		{
		    paperSize.title = @"6 3/4 Envelope";
		    paperSize.width = 3.625f;
		    paperSize.height = 6.5f;
		    paperSize.metric = NO;
		    break;
		}

		case 39: // US Std Fanfold - 14.875 x 11.0 in
		{
		    paperSize.title = @"US Std Fanfold";
		    paperSize.width = 14.875f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 40: // German Std Fanfold - 8.5 x 12.0 in
		{
		    paperSize.title = @"German Std Fanfold";
		    paperSize.width = 8.5f;
		    paperSize.height = 12.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 41: // German Legal Fanfold - 8.5 x 13.0 in
		{
		    paperSize.title = @"German Legal Fanfold";
		    paperSize.width = 8.5f;
		    paperSize.height = 13.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 42: // B4 (ISO) - 250.0 x 353.0 mm
		{
		    paperSize.title = @"B4 (ISO)";
		    paperSize.width = 250.0f;
		    paperSize.height = 353.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 43: // Japanese Postcard - 100.0 x 148.0 mm
		{
		    paperSize.title = @"Japanese Postcard";
		    paperSize.width = 100.0f;
		    paperSize.height = 148.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 44: // 9 x 11 in - 9.0 x 11 in
		{
		    paperSize.title = @"9 x 11 in";
		    paperSize.width = 9.0f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 45: // 10 x 11 in - 10.0 x 11 in
		{
		    paperSize.title = @"10 x 11 in";
		    paperSize.width = 10.0f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 46: // 15 x 11 in - 15.0 x  in
		{
		    paperSize.title = @"15 x 11 in";
		    paperSize.width = 15.0f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 47: // Envelope Invite - 220.0 x 220.0 mm
		{
		    paperSize.title = @"Envelope Invite";
		    paperSize.width = 220.0f;
		    paperSize.height = 220.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 50: // Letter Extra - 9.275 x 12.0 in
		{
		    paperSize.title = @"Letter Extra";
		    paperSize.width = 9.275f;
		    paperSize.height = 12.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 51: // Legal Extra - 9.275 x 15.0 in
		{
		    paperSize.title = @"Legal Extra";
		    paperSize.width = 9.275f;
		    paperSize.height = 15.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 52: // Tabloid Extra - 11.69 x 18.0 in
		{
		    paperSize.title = @"Tabloid Extra";
		    paperSize.width = 11.69f;
		    paperSize.height = 18.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 53: // A4 Extra - 9.27 x 12.0 in
		{
		    paperSize.title = @"A4 Extra";
		    paperSize.width = 9.27f;
		    paperSize.height = 12.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 54: // Letter Transverse - 8.275 x 11.0 in
		{
		    paperSize.title = @"Letter Transverse";
		    paperSize.width = 8.275f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 55: // A4 Transverse - 210.0 x 297.0 mm
		{
		    paperSize.title = @"A4 Transverse";
		    paperSize.width = 210.0f;
		    paperSize.height = 297.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 56: // Letter Extra Transverse - 9.275 x 12.0 in
		{
		    paperSize.title = @"Letter Extra Transverse";
		    paperSize.width = 9.275f;
		    paperSize.height = 12.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 57: // SuperA/SuperA/A4 - 227.0 x 356.0 mm
		{
		    paperSize.title = @"SuperA/SuperA/A4";
		    paperSize.width = 227.0f;
		    paperSize.height = 356.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 58: // SuperB/SuperB/A3 - 305.0 x 487.0 mm
		{
		    paperSize.title = @"SuperB/SuperB/A3";
		    paperSize.width = 305.0f;
		    paperSize.height = 487.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 59: // Letter Plus - 8.5 x 12.0 in
		{
		    paperSize.title = @"Letter Plus";
		    paperSize.width = 8.5f;
		    paperSize.height = 12.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 60: // A4 Plus - 210.0 x 330.0 mm
		{
		    paperSize.title = @"A4 Plus";
		    paperSize.width = 210.0f;
		    paperSize.height = 330.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 61: // A5 Transverse - 148.0 x 210.0 mm
		{
		    paperSize.title = @"A5 Transverse";
		    paperSize.width = 148.0f;
		    paperSize.height = 210.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 62: // B5 (JIS) Transverse - 182.0 x 257.0 mm
		{
		    paperSize.title = @"B5 (JIS) Transverse";
		    paperSize.width = 182.0f;
		    paperSize.height = 257.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 63: // A3 Extra - 322.0 x 445.0 mm
		{
		    paperSize.title = @"A3 Extra";
		    paperSize.width = 322.0f;
		    paperSize.height = 445.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 64: // A5 Extra - 174.0 x 235.0 mm
		{
		    paperSize.title = @"A5 Extra";
		    paperSize.width = 174.0f;
		    paperSize.height = 235.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 65: // B5 (ISO) Extra - 201.0 x 276.0 mm
		{
		    paperSize.title = @"B5 (ISO) Extra";
		    paperSize.width = 201.0f;
		    paperSize.height = 276.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 66: // A2 - 420.0 x 594.0 mm
		{
		    paperSize.title = @"A2";
		    paperSize.width = 420.0f;
		    paperSize.height = 594.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 67: // A3 Transverse - 297.0 x 420.0 mm
		{
		    paperSize.title = @"A3 Transverse";
		    paperSize.width = 297.0f;
		    paperSize.height = 420.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 68: // A3 Extra Transverse - 322.0 x 445.0 mm
		{
		    paperSize.title = @"A3 Extra Transverse";
		    paperSize.width = 322.0f;
		    paperSize.height = 445.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 69: // Japanese Double Postcard - 200.0 x 148.0 mm
		{
		    paperSize.title = @"Japanese Double Postcard";
		    paperSize.width = 200.0f;
		    paperSize.height = 148.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 70: // A6 - 105.0 x 148.0 mm
		{
		    paperSize.title = @"A6";
		    paperSize.width = 105.0f;
		    paperSize.height = 148.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 75: // Letter Rotated - 11.0 x 19.5 in
		{
		    paperSize.title = @"Letter Rotated";
		    paperSize.width = 11.0f;
		    paperSize.height = 19.5f;
		    paperSize.metric = NO;
		    break;
		}

		case 76: // A3 Rotated - 420.0 x 297.0 mm
		{
		    paperSize.title = @"A3 Rotated";
		    paperSize.width = 420.0f;
		    paperSize.height = 297.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 77: // A4 Rotated - 297.0 x 210.0 mm
		{
		    paperSize.title = @"A4 Rotated";
		    paperSize.width = 297.0f;
		    paperSize.height = 210.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 78: // A5 Rotated - 210.0 x 148.0 mm
		{
		    paperSize.title = @"A5 Rotated";
		    paperSize.width = 210.0f;
		    paperSize.height = 148.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 79: // B4 (JIS) Rotated - 364.0 x 257.0 mm
		{
		    paperSize.title = @"B4 (JIS) Rotated";
		    paperSize.width = 364.0f;
		    paperSize.height = 257.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 80: // B5 (JIS) Rotated - 257.0 x 182.0 mm
		{
		    paperSize.title = @"B5 (JIS) Rotated";
		    paperSize.width = 257.0f;
		    paperSize.height = 182.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 81: // Japanese Postcard Rotated - 148.0 x 100.0 mm
		{
		    paperSize.title = @"Japanese Postcard Rotated";
		    paperSize.width = 148.0f;
		    paperSize.height = 100.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 82: // Double Japanese Postcard Rotated - 148.0 x 200.0 mm
		{
		    paperSize.title = @"Double Japanese Postcard Rotated";
		    paperSize.width = 148.0f;
		    paperSize.height = 200.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 83: // A6 Rotated - 148.0 x 105.0 mm
		{
		    paperSize.title = @"A6 Rotated";
		    paperSize.width = 148.0f;
		    paperSize.height = 105.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 88: // B6 (JIS) - 128.0 x 182.0 mm
		{
		    paperSize.title = @"B6 (JIS)";
		    paperSize.width = 128.0f;
		    paperSize.height = 182.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 89: // B6 (JIS) Rotated - 182.0 x 128.0 mm
		{
		    paperSize.title = @"B6 (JIS) Rotated";
		    paperSize.width = 182.0f;
		    paperSize.height = 128.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 90: // 12 x 11 in - 12.0 x 11 in
		{
		    paperSize.title = @"12 x 11 in";
		    paperSize.width = 12.0f;
		    paperSize.height = 11.0f;
		    paperSize.metric = NO;
		    break;
		}

		case 93: // PRC 16K - 146.0 x 215.0 mm
		{
		    paperSize.title = @"PRC 16K";
		    paperSize.width = 146.0f;
		    paperSize.height = 215.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 94: // PRC 32K - 97.0 x 151.0 mm
		{
		    paperSize.title = @"PRC 32K";
		    paperSize.width = 97.0f;
		    paperSize.height = 151.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 95: // PRC 32K(Big) - 97.0 x 151.0 mm
		{
		    paperSize.title = @"PRC 32K(Big)";
		    paperSize.width = 97.0f;
		    paperSize.height = 151.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 96: // PRC Envelope #1 - 102.0 x 165.0 mm
		{
		    paperSize.title = @"PRC Envelope #1";
		    paperSize.width = 102.0f;
		    paperSize.height = 165.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 97: // PRC Envelope #2 - 102.0 x 176.0 mm
		{
		    paperSize.title = @"PRC Envelope #2";
		    paperSize.width = 102.0f;
		    paperSize.height = 176.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 98: // PRC Envelope #3 - 125.0 x 176.0 mm
		{
		    paperSize.title = @"PRC Envelope #3";
		    paperSize.width = 125.0f;
		    paperSize.height = 176.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 99: // PRC Envelope #4 - 110.0 x 208.0 mm
		{
		    paperSize.title = @"PRC Envelope #4";
		    paperSize.width = 110.0f;
		    paperSize.height = 208.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 100: // PRC Envelope #5 - 110.0 x 220.0 mm
		{
		    paperSize.title = @"PRC Envelope #5";
		    paperSize.width = 110.0f;
		    paperSize.height = 220.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 101: // PRC Envelope #6 - 120.0 x 230.0 mm
		{
		    paperSize.title = @"PRC Envelope #6";
		    paperSize.width = 120.0f;
		    paperSize.height = 230.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 102: // PRC Envelope #7 - 160.0 x 230.0 mm
		{
		    paperSize.title = @"PRC Envelope #7";
		    paperSize.width = 160.0f;
		    paperSize.height = 230.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 103: // PRC Envelope #8 - 120.0 x 309.0 mm
		{
		    paperSize.title = @"PRC Envelope #8";
		    paperSize.width = 120.0f;
		    paperSize.height = 309.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 104: // PRC Envelope #9 - 229.0 x 324.0 mm
		{
		    paperSize.title = @"PRC Envelope #9";
		    paperSize.width = 229.0f;
		    paperSize.height = 324.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 105: // PRC Envelope #10 - 324.0 x 458.0 mm
		{
		    paperSize.title = @"PRC Envelope #10";
		    paperSize.width = 324.0f;
		    paperSize.height = 458.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 109: // PRC Envelope #1 Rotated - 165.0 x 102.0 mm
		{
		    paperSize.title = @"PRC Envelope #1 Rotated";
		    paperSize.width = 165.0f;
		    paperSize.height = 102.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 110: // PRC Envelope #2 Rotated - 176.0 x 102.0 mm
		{
		    paperSize.title = @"PRC Envelope #2 Rotated";
		    paperSize.width = 176.0f;
		    paperSize.height = 102.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 111: // PRC Envelope #3 Rotated - 176.0 x 125.0 mm
		{
		    paperSize.title = @"PRC Envelope #3 Rotated";
		    paperSize.width = 176.0f;
		    paperSize.height = 125.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 112: // PRC Envelope #4 Rotated - 208.0 x 110.0 mm
		{
		    paperSize.title = @"PRC Envelope #4 Rotated";
		    paperSize.width = 208.0f;
		    paperSize.height = 110.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 113: // PRC Envelope #5 Rotated - 220.0 x 110.0 mm
		{
		    paperSize.title = @"PRC Envelope #5 Rotated";
		    paperSize.width = 220.0f;
		    paperSize.height = 110.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 114: // PRC Envelope #6 Rotated - 230.0 x 120.0 mm
		{
		    paperSize.title = @"PRC Envelope #6 Rotated";
		    paperSize.width = 230.0f;
		    paperSize.height = 120.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 115: // PRC Envelope #7 Rotated - 230.0 x 160.0 mm
		{
		    paperSize.title = @"PRC Envelope #7 Rotated";
		    paperSize.width = 230.0f;
		    paperSize.height = 160.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 116: // PRC Envelope #8 Rotated - 309.0 x 120.0 mm
		{
		    paperSize.title = @"PRC Envelope #8 Rotated";
		    paperSize.width = 309.0f;
		    paperSize.height = 120.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 117: // PRC Envelope #9 Rotated - 324.0 x 229.0 mm
		{
		    paperSize.title = @"PRC Envelope #9 Rotated";
		    paperSize.width = 324.0f;
		    paperSize.height = 229.0f;
		    paperSize.metric = YES;
		    break;
		}

		case 118: // PRC Envelope #10 Rotated - 458.0 x 324.0 mm
		{
		    paperSize.title = @"PRC Envelope #10 Rotated";
		    paperSize.width = 458.0f;
		    paperSize.height = 324.0f;
		    paperSize.metric = YES;
		    break;
		}

        default: // Letter - 8.5 x 11 in
        {
            paperSize.title = @"Letter";
            paperSize.width = 8.5f;
            paperSize.height = 11.0f;
            paperSize.metric = NO;
            break;
        }
    }

    [paperSize setPaperSize];
    return paperSize;
}

+ (CGRect)fitPaperSize:(PaperSize *)paperSize toAreaOfSize:(CGSize)size
{
    float originalAspectRatio = paperSize.width / paperSize.height;
    float maxAspectRatio = size.width / size.height;

    CGRect newRect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    if (originalAspectRatio > maxAspectRatio) {
        newRect.size.height = size.width * paperSize.height / paperSize.width;
    } else {
        newRect.size.width = size.height * paperSize.width / paperSize.height;
    }
    
    return CGRectIntegral(newRect);
}

- (void)setPaperSize
{
    float resolution = self.metric ? kPrintDMM : kPrintDPI;
    self.size = CGSizeMake(self.width * resolution, self.height * resolution);
}

@end
