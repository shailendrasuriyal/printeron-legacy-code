//
//  WebDocument.h
//  PrinterOn
//
//  Created by Mark Burns on 11/23/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrintDocument.h"

@class WKWebView;

@interface WebDocument : PrintDocument

- (instancetype)initWithWebView:(UIWebView *)webView;
- (instancetype)initWithWebKitView:(WKWebView *)webView;
- (instancetype)initWithURL:(NSURL *)url;

@end
