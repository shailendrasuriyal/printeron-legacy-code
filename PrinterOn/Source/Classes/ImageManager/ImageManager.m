//
//  ImageManager.m
//  PrinterOn
//
//  Created by Mark Burns on 2014-08-29.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "ImageManager.h"

#import "GCDSingleton.h"

@interface ImageManager ()

@property (nonatomic, strong) NSBundle *imagesBundle;
@property (nonatomic, strong) NSCache *imageCache;

@end

@implementation ImageManager

SINGLETON_GCD(ImageManager);

+ (NSString *)mainBundleIdentifier
{
    NSString *bundleId = [NSBundle mainBundle].bundleIdentifier;
    if ([bundleId containsString:@".b2b"]) {
        NSRange foundRange = [bundleId rangeOfString:@".b2b" options:NSBackwardsSearch];
        if (foundRange.location != NSNotFound) {
            bundleId = [bundleId stringByReplacingCharactersInRange:foundRange withString:@""];
        }
    }
    return bundleId;
}

- (instancetype)init
{
    if ((self = [super init])) {
        self.imagesBundle = [NSBundle mainBundle];
    }
    return self;
}

- (void)setBundleIdentifier:(NSString *)identifier
{
    //self.imagesBundle = [NSBundle bundleWithIdentifier:identifier];
    NSRange resourceRange = [identifier rangeOfString:@"." options:NSBackwardsSearch];
    NSURL *bundleURL = [[NSBundle mainBundle] URLForResource:[identifier substringFromIndex:resourceRange.location + resourceRange.length] withExtension:@"bundle"];
    self.imagesBundle = [NSBundle bundleWithURL:bundleURL];
}

- (UIImage *)imageNamed:(NSString *)name
{
    return [self imageNamed:name inBundle:self.imagesBundle cacheImage:YES];
}

- (UIImage *)imageNamed:(NSString *)name cacheImage:(BOOL)cache
{
    return [self imageNamed:name inBundle:self.imagesBundle cacheImage:cache];
}

- (UIImage *)imageNamed:(NSString *)name inBundle:(NSBundle *)bundle cacheImage:(BOOL)cache
{
    /*
       Commented the new iOS 8 UIImage imageNamed:inBundle:compatibleWithTraitCollection: usage because it was causing AdHoc and Enterprise builds to crash on launch on iOS 8.0 devices.  The simulator was fine and debug developer signed builds were ok on device.  Seems to be a bug in Xcode 6 or iOS 8.0.  Leaving code so it can be uncommented in the future if/when this is resolved.
     */

    //if (cache && [UIImage respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)]) {
        // iOS 8+ use existing OS function which will find an image in alternate bundles and use the system cache
        //return [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
    //} else {
        // Custom image caching using NSCache
        if (cache) {
            // Create the image cache if it doesn't exist already
            if (self.imageCache == nil) {
                self.imageCache = [NSCache new];
            }

            // Check to see if the image is already in the cache
            UIImage *image = [self.imageCache objectForKey:name];
            if (image == nil) {
                // Load the image and store it in the cache
                image = [UIImage imageWithContentsOfFile:[bundle pathForResource:name ofType:@"png"]];
                if (image) {
                    [self.imageCache setObject:image forKey:name];
                }
            }
            return image;
        }

        return [UIImage imageWithContentsOfFile:[bundle pathForResource:name ofType:@"png"]];
    //}
}

@end
