//
//  ImageManager.h
//  PrinterOn
//
//  Created by Mark Burns on 2014-08-29.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface ImageManager : NSObject

+ (ImageManager *)sharedImageManager;
+ (NSString *)mainBundleIdentifier;

- (void)setBundleIdentifier:(NSString *)identifier;
- (UIImage *)imageNamed:(NSString *)name;
- (UIImage *)imageNamed:(NSString *)name cacheImage:(BOOL)cache;
- (UIImage *)imageNamed:(NSString *)name inBundle:(NSBundle *)bundle cacheImage:(BOOL)cache;

@end
