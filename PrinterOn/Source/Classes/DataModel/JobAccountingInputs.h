//
//  JobAccountingInputs.h
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

FOUNDATION_EXPORT NSString* const kJobInputTypeEmailAddressKey;
FOUNDATION_EXPORT NSString* const kJobInputTypeClientUIDKey;
FOUNDATION_EXPORT NSString* const kJobInputTypeNetworkLoginKey;
FOUNDATION_EXPORT NSString* const kJobInputTypeSessionMetadataKey;
FOUNDATION_EXPORT NSString* const kJobInputTypeReleaseCodeKey;

@class ClientUIDInput, EmailAddressInput, NetworkLoginInput, Printer, ReleaseCodeInput, SessionMetadataInput;

@interface JobAccountingInputs : NSManagedObject

@property (nonatomic, retain) Printer *printer;
@property (nonatomic, retain) NetworkLoginInput *networkLogin;
@property (nonatomic, retain) EmailAddressInput *emailAddress;
@property (nonatomic, retain) ClientUIDInput *clientUID;
@property (nonatomic, retain) SessionMetadataInput *sessionMetadata;
@property (nonatomic, retain) ReleaseCodeInput *releaseCode;

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store;

+ (BOOL)isJobInputStringRequired:(NSString *)string;
+ (BOOL)isJobInputStringRequiredOrOptional:(NSString *)string;
+ (NSString *)placeholderForInputString:(NSString *)string;
+ (NSString *)releaseCodeTypeString:(NSString *)string;
+ (NSString *)releaseCodePatternForPrinter:(Printer *)printer;
+ (BOOL)printerRequiresJobInputs:(Printer *)printer;
+ (NSDictionary *)generatePopulatedInputs:(Printer *)printer;

@end

@protocol JobAccountingInputsDelegate

- (void)didFinishWithJobInputs:(NSDictionary *)inputs;

@end
