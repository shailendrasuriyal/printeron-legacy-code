//
//  EmailFolder.m
//  
//
//  Created by Mark Burns on 2015-10-21.
//
//

#import "EmailFolder.h"

#import "EmailAccount.h"
#import "EmailMessage.h"

@implementation EmailFolder

@dynamic folderName;
@dynamic order;
@dynamic emailAccount;
@dynamic emailMessage;

- (void)clearAttachmentsCacheForFolder
{
    for (EmailMessage *message in self.emailMessage) {
        [message clearAttachmentsCacheForMessage];
    }
}

@end
