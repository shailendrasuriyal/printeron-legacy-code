//
//  NetworkLoginInput.m
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "NetworkLoginInput.h"

@implementation NetworkLoginInput

@dynamic use;
@dynamic label;
@dynamic defaultValue;
@dynamic jobAccountingInputs;


#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"use": @"use",
                       @"label": @"label",
                       @"default": @"defaultValue",
                       };
    });
    return attributes;
}

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store
{
    RKEntityMapping *aliasMapping = [RKEntityMapping mappingForEntityForName:@"NetworkLoginInput" inManagedObjectStore:store];
    [aliasMapping addAttributeMappingsFromDictionary:[NetworkLoginInput attributeMappings]];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"jobAccounting.jobAccountingInputs.networkLogin" toKeyPath:@"jobAccountingInputs.networkLogin" withMapping:aliasMapping];
}

@end
