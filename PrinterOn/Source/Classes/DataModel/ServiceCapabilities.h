//
//  ServiceCapabilities.h
//  
//
//  Created by Mark Burns on 2016-04-13.
//
//

@class ServiceAccess, ServiceLinks;

NS_ASSUME_NONNULL_BEGIN

@interface ServiceCapabilities : NSManagedObject

@property (nullable, nonatomic, retain) NSString *company;
@property (nullable, nonatomic, retain) NSString *deploymentMode;
@property (nullable, nonatomic, retain) NSNumber *dirSearch;
@property (nullable, nonatomic, retain) NSNumber *docAPI;
@property (nullable, nonatomic, retain) NSNumber *documentPreview;
@property (nullable, nonatomic, retain) NSString *lastModifiedDate;
@property (nullable, nonatomic, retain) NSNumber *maxUploadSize;
@property (nullable, nonatomic, retain) NSNumber *releaseAnywhere;
@property (nullable, nonatomic, retain) NSString *serviceName;
@property (nullable, nonatomic, retain) NSString *serviceURL;
@property (nullable, nonatomic, retain) NSString *version;
@property (nullable, nonatomic, retain) NSSet<ServiceAccess *> *access;
@property (nullable, nonatomic, retain) NSSet<ServiceLinks *> *links;

+ (ServiceCapabilities *)getCapabilitiesForService:(NSString *)serviceURL inContext:(nullable NSManagedObjectContext *)context;

+ (NSString *)getDirectorySearchURLForService:(NSString *)serviceURL;
+ (NSString *)getOAuthAuthorizeURLForService:(NSString *)serviceURL;
+ (NSString *)getOAuthTokenURLForService:(NSString *)serviceURL;
+ (NSString *)getOAuthUserInfoURLForService:(NSString *)serviceURL;
+ (NSString *)getJobsURLForService:(NSString *)serviceURL;

+ (BOOL)isServiceUsingOAuth:(NSString *)serviceURL;
+ (BOOL)serviceSupportsOAuthFirstParty:(NSString *)serviceURL;
+ (BOOL)serviceSupportsOAuthThirdParty:(NSString *)serviceURL;

- (ServiceAccess *)getAccessForType:(NSString *)accessType;
- (ServiceLinks *)getLinkForType:(NSString *)linkType;

@end

@interface ServiceCapabilities (CoreDataGeneratedAccessors)

- (void)addAccessObject:(ServiceAccess *)value;
- (void)removeAccessObject:(ServiceAccess *)value;
- (void)addAccess:(NSSet<ServiceAccess *> *)values;
- (void)removeAccess:(NSSet<ServiceAccess *> *)values;

- (void)addLinksObject:(ServiceLinks *)value;
- (void)removeLinksObject:(ServiceLinks *)value;
- (void)addLinks:(NSSet<ServiceLinks *> *)values;
- (void)removeLinks:(NSSet<ServiceLinks *> *)values;

@end

NS_ASSUME_NONNULL_END
