//
//  EmailMessageTransformer.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-04-11.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "EmailMessageTransformer.h"
#import "NSData+Transform.h"

@implementation EmailMessageTransformer

+ (Class)transformedValueClass
{
    return [NSData class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    NSData *message = [NSKeyedArchiver archivedDataWithRootObject:value];
    [message transform];
    return message;
}

- (id)reverseTransformedValue:(id)value
{
    if ([value isKindOfClass:[NSData class]]) {
        [(NSData *)value transform];
        return [NSKeyedUnarchiver unarchiveObjectWithData:value];
    }
    return value;
}

@end
