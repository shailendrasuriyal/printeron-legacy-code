//
//  EmailAddressInput.m
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "EmailAddressInput.h"

@implementation EmailAddressInput

@dynamic use;
@dynamic label;
@dynamic defaultValue;
@dynamic jobAccountingInputs;


#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"use": @"use",
                       @"label": @"label",
                       @"default": @"defaultValue",
                       };
    });
    return attributes;
}

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store
{
    RKEntityMapping *aliasMapping = [RKEntityMapping mappingForEntityForName:@"EmailAddressInput" inManagedObjectStore:store];
    [aliasMapping addAttributeMappingsFromDictionary:[EmailAddressInput attributeMappings]];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"jobAccounting.jobAccountingInputs.emailAddress" toKeyPath:@"jobAccountingInputs.emailAddress" withMapping:aliasMapping];
}

@end
