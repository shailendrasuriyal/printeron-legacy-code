//
//  EmailAddressInput.h
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface EmailAddressInput : NSManagedObject

@property (nonatomic, retain) NSString * use;
@property (nonatomic, retain) NSString * label;
@property (nonatomic, retain) NSString * defaultValue;
@property (nonatomic, retain) NSManagedObject *jobAccountingInputs;

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store;

@end
