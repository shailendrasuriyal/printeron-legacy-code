//
//  ServiceAccess.m
//  
//
//  Created by Mark Burns on 2016-04-13.
//
//

#import "ServiceAccess.h"

#import "ServiceCapabilities.h"
#import "ServiceLinks.h"
#import "UserAccount.h"

@implementation ServiceAccess

@dynamic issuer;
@dynamic scopes;
@dynamic type;
@dynamic capabilities;
@dynamic links;

- (ServiceLinks *)getLinkForType:(NSString *)linkType
{
    for (ServiceLinks *accessLink in self.links) {
        if ([accessLink.rel.lowercaseString isEqualToString:linkType.lowercaseString]) {
            return accessLink;
        }
    }
    return nil;
}

- (void)clearOAuth
{
    if ([self.type.lowercaseString isEqualToString:@"oauth2"]) {
        UserAccount *account = [UserAccount getUserAccountForURL:[NSURL URLWithString:self.capabilities.serviceURL]];
        if (account && ![account.userName isEqualToString:[UserAccount anonymousUser].userName] && self.capabilities) {
            [account deleteOAuth2AuthenticationForService:self.capabilities.serviceURL];
        }
    }
}

- (void)prepareForDeletion
{
    // Delete stored OAuth information here instead of willSave because by the time we hit willSave
    // the relationship with capabilities will have been nullified and we won't find any stored keys
    [self clearOAuth];
}

@end
