//
//  EmailMessage.m
//  
//
//  Created by Mark Burns on 2015-10-28.
//
//

#import "EmailMessage.h"

#import "EmailAccount.h"
#import "EmailAttachmentManager.h"
#import "EmailFolder.h"

#import <MailCore/MailCore.h>

@implementation EmailMessage

@dynamic uid;
@dynamic message;
@dynamic emailFolder;

- (void)clearAttachmentsCacheForMessage
{
    MCOIMAPMessage *imapMessage = self.message;
    
    NSMutableArray *parts = [NSMutableArray arrayWithArray:imapMessage.requiredPartsForRendering];
    [parts addObjectsFromArray:imapMessage.attachments];
    [parts addObjectsFromArray:imapMessage.htmlInlineAttachments];
    
    for (MCOIMAPPart *part in parts) {
        NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:self.emailFolder.emailAccount.emailAddress messageID:[NSString stringWithFormat:@"%d", imapMessage.uid] partID:part.partID fileName:part.filename];
        NSURL *attachURL = [[EmailAttachmentManager sharedEmailAttachmentManager] retrieveURLForAttachment:attachID];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:attachURL.path]) {
            [[NSFileManager defaultManager] removeItemAtPath:[attachURL.path stringByDeletingLastPathComponent] error:nil];
        }
    }
}

@end
