//
//  Service.h
//  PrinterOn
//
//  Created by Mark Burns on 1/19/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@class UserAccount;

@interface Service : NSManagedObject

+ (NSString *)hostData;
+ (Service *)createService:(NSString *)URL
               description:(NSString *)description
                 isDefault:(BOOL)boolDefault
                  isLocked:(BOOL)boolLocked
                     isMDM:(BOOL)boolMDM
              isRestricted:(BOOL)boolRestricted
           completionBlock:(void (^)(NSManagedObjectID *capabilitiesID))completionBlock;

- (void)updateService:(NSString *)URL
          description:(NSString *)description
            isDefault:(BOOL)boolDefault
             isLocked:(BOOL)boolLocked
                isMDM:(BOOL)boolMDM
         isRestricted:(BOOL)boolRestricted
      completionBlock:(void (^)(NSManagedObjectID *capabilitiesID))completionBlock;

- (BOOL)hasChangesForService:(NSString *)URL
                 description:(NSString *)description
                   isDefault:(BOOL)boolDefault;

+ (BOOL)compareURL:(NSString *)url1 toURL:(NSString *)url2;
+ (NSString *)cleanServiceURL:(NSString *)urlString;
+ (NSArray *)doesServiceExist:(NSString *)URL inContext:(NSManagedObjectContext *)context;
+ (Service *)getDefaultService;
+ (Service *)getMDMService;
+ (Service *)getRestrictedService;
+ (Service *)hostedService;

+ (BOOL)isHostedURL:(NSURL *)serviceURL andPublic:(BOOL)publicHosted;
+ (BOOL)isHostedString:(NSString *)serviceURL andPublic:(BOOL)publicHosted;
+ (BOOL)isSecureURL:(NSURL *)serviceURL;
+ (BOOL)isHostTrusted:(NSString *)host;
+ (void)validateServerTrust:(NSURLAuthenticationChallenge *)challenge;
+ (NSURLCredential *)credentialForServerTrust:(NSURLAuthenticationChallenge *)challenge;
+ (NSURLSessionAuthChallengeDisposition)validateServerTrust:(NSURLAuthenticationChallenge *)challenge withCredentials:(NSURLCredential *__autoreleasing *)credential;
+ (void)validateServerTrust:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))handler;

@property (nonatomic, retain) NSNumber *isDefault;
@property (nonatomic, retain) NSNumber *isMDM;
@property (nonatomic, retain) NSNumber *isRestricted;
@property (nonatomic, retain) NSString *serviceDescription;
@property (nonatomic, retain) NSNumber *serviceLock;
@property (nonatomic, retain) NSString *serviceURL;
@property (nonatomic, retain) UserAccount *defaultUser;

@end
