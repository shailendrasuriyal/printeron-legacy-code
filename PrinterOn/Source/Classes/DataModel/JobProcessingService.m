//
//  JobProcessingService.m
//  PrinterOn
//
//  Created by Mark Burns on 11/13/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "JobProcessingService.h"

@implementation JobProcessingService

@dynamic enabled;
@dynamic serviceName;
@dynamic serviceType;
@dynamic version;
@dynamic serviceUI;
@dynamic serviceURI;
@dynamic printer;


#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"enabled": @"enabled",
                       @"serviceName.text": @"serviceName",
                       @"serviceType.text": @"serviceType",
                       @"serviceUI.text" : @"serviceUI",
                       @"serviceURI.text": @"serviceURI",
                       @"version": @"version",
                       };
    });
    return attributes;
}

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store
{
    RKEntityMapping *aliasMapping = [RKEntityMapping mappingForEntityForName:@"JobProcessingService" inManagedObjectStore:store];
    [aliasMapping addAttributeMappingsFromDictionary:[JobProcessingService attributeMappings]];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"jobProcessingServices.jobProcessingService" toKeyPath:@"jobProcessingService" withMapping:aliasMapping];
}

@end
