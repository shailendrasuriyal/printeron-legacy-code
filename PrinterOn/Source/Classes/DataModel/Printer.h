//
//  Printer.h
//  PrinterOn
//
//  Created by Mark Burns on 13-09-17.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class EmailAlias, JobAccountingInputs, JobProcessingService, Media, PrintJob;

NS_ASSUME_NONNULL_BEGIN

@interface Printer : NSManagedObject

+ (NSArray *)identificationAttributes;
+ (RKEntityMapping *)getEntityMappingForName:(NSString *)entity
                                     inStore:(RKManagedObjectStore *)store;

- (NSString *)getDisplayNameLabel;
- (NSString *)getStatusLabel;
- (NSString *)getManufacturerLabel;
- (NSString *)getModelLabel;
- (CLLocationCoordinate2D)getCoordinates;
- (NSString *)getCountryLabel;
- (NSString *)getAddressLineLabel;
- (NSDictionary *)getAddressDictionary;
- (NSString *)getLocationDescriptionLabel;
- (NSString *)getHoursLabel;
- (NSString *)getEmailAddressOfType:(NSString *)type;
- (NSString *)getWebPortalAddressStripPrefix:(BOOL)prefix;
- (NSURL *)getDocAPIAddress;
- (NSString *)getRemoteReleaseCMDAddress;
- (NSString *)getRemoteReleaseAPIAddress;
- (NSString *)getWebReleaseUIAddress;
- (BOOL)doesSupportColor;
- (NSString *)getInkTypeLabel;
- (BOOL)doesSupportDuplex;
- (NSString *)getDuplexModeLabel;
- (NSString *)getCoverPageLabel;
- (NSString *)getJobFeesLabel;
- (NSString *)getPageLimitLabel;
- (NSString *)getPaperSizesLabel;
- (int)getDefaultPaperSizeNum;
- (NSString *)getReleaseCodeModeLabel;
- (NSString *)getPrintUserCredentialsLabel;
- (BOOL)requiresAuthToPrint;

- (Printer *)clonePrinterInContext:(NSManagedObjectContext *)context
                     forEntityName:(NSString *)entityName
                    updateExisting:(BOOL)update;
- (Printer *)clonePrinterInContext:(NSManagedObjectContext *)context
                     forEntityName:(NSString *)entityName
                    updateExisting:(BOOL)update
                   checkDuplicates:(BOOL)dupe;

- (void)updateWithPrinter:(Printer *)printer;

+ (void)connectRelationshipsFromMapping:(NSDictionary *)results inContext:(NSManagedObjectContext *)context;

+ (Printer *)getPrinter:(Printer *)printer forEntityName:(NSString *)entityName;
+ (Printer *)getSingletonPrinterForEntity:(NSString *)entityName;
+ (void)setSingletonPrinter:(Printer *)printer
                  forEntity:(NSString *)entityName;
+ (BOOL)isSingletonPrinter:(Printer *)printer
                 forEntity:(NSString *)entityName;

@property (nullable, nonatomic, retain) NSString *accessClass;
@property (nullable, nonatomic, retain) NSString *addressCity;
@property (nullable, nonatomic, retain) NSString *addressCountry;
@property (nullable, nonatomic, retain) NSString *addressGeoDistanceFrom;
@property (nullable, nonatomic, retain) NSString *addressGeoLocation;
@property (nullable, nonatomic, retain) NSString *addressGeoUnits;
@property (nullable, nonatomic, retain) NSString *addressLine1;
@property (nullable, nonatomic, retain) NSString *addressLine2;
@property (nullable, nonatomic, retain) NSString *addressPostal;
@property (nullable, nonatomic, retain) NSString *addressState;
@property (nullable, nonatomic, retain) NSString *aliasAddress;
@property (nullable, nonatomic, retain) NSString *aliasName;
@property (nullable, nonatomic, retain) NSString *aliasNumeric;
@property (nullable, nonatomic, retain) NSNumber *attended;
@property (nullable, nonatomic, retain) NSString *color;
@property (nullable, nonatomic, retain) NSNumber *colorCapable;
@property (nullable, nonatomic, retain) NSString *colorPermit;
@property (nullable, nonatomic, retain) NSNumber *coverPage;
@property (nullable, nonatomic, retain) NSString *crypto;
@property (nullable, nonatomic, retain) NSString *displayName;
@property (nullable, nonatomic, retain) NSNumber *duplex;
@property (nullable, nonatomic, retain) NSString *duplexPermit;
@property (nullable, nonatomic, retain) NSString *hoursOfOperation;
@property (nullable, nonatomic, retain) NSString *jobReleaseLabel;
@property (nullable, nonatomic, retain) NSString *jobReleaseMode;
@property (nullable, nonatomic, retain) NSString *jobReleaseUse;
@property (nullable, nonatomic, retain) NSString *locationDescription;
@property (nullable, nonatomic, retain) NSString *locatorCode;
@property (nullable, nonatomic, retain) NSString *manufacturer;
@property (nullable, nonatomic, retain) NSString *model;
@property (nullable, nonatomic, retain) NSString *network;
@property (nullable, nonatomic, retain) NSNumber *online;
@property (nullable, nonatomic, retain) NSString *organizationBrand;
@property (nullable, nonatomic, retain) NSString *organizationDept;
@property (nullable, nonatomic, retain) NSString *organizationDisplayName;
@property (nullable, nonatomic, retain) NSString *organizationLocationDesc;
@property (nullable, nonatomic, retain) NSString *organizationLocationType;
@property (nullable, nonatomic, retain) NSString *organizationName;
@property (nullable, nonatomic, retain) NSString *pageLimit;
@property (nullable, nonatomic, retain) NSArray *parentIDs;
@property (nullable, nonatomic, retain) NSNumber *payForPrint;
@property (nullable, nonatomic, retain) NSString *payForPrintDesc;
@property (nullable, nonatomic, retain) NSString *printAuthentication;
@property (nullable, nonatomic, retain) NSString *printerClass;
@property (nullable, nonatomic, retain) NSString *printerID;
@property (nullable, nonatomic, retain) NSNumber *searchOrder;
@property (nullable, nonatomic, retain) NSNumber *searchPageNum;
@property (nullable, nonatomic, retain) NSString *searchURL;
@property (nullable, nonatomic, retain) NSString *serviceID;
@property (nullable, nonatomic, retain) NSString *siteClass;
@property (nullable, nonatomic, retain) NSNumber *siteNum;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSSet<Printer *> *children;
@property (nullable, nonatomic, retain) NSSet<EmailAlias *> *emailAlias;
@property (nullable, nonatomic, retain) JobAccountingInputs *jobAccountingInputs;
@property (nullable, nonatomic, retain) NSSet<JobProcessingService *> *jobProcessingService;
@property (nullable, nonatomic, retain) NSSet<Media *> *mediaSupported;
@property (nullable, nonatomic, retain) NSSet<Printer *> *parents;
@property (nullable, nonatomic, retain) NSSet<PrintJob *> *printJob;

@end

@interface Printer (CoreDataGeneratedAccessors)

- (void)addChildrenObject:(Printer *)value;
- (void)removeChildrenObject:(Printer *)value;
- (void)addChildren:(NSSet<Printer *> *)values;
- (void)removeChildren:(NSSet<Printer *> *)values;

- (void)addEmailAliasObject:(EmailAlias *)value;
- (void)removeEmailAliasObject:(EmailAlias *)value;
- (void)addEmailAlias:(NSSet<EmailAlias *> *)values;
- (void)removeEmailAlias:(NSSet<EmailAlias *> *)values;

- (void)addJobProcessingServiceObject:(JobProcessingService *)value;
- (void)removeJobProcessingServiceObject:(JobProcessingService *)value;
- (void)addJobProcessingService:(NSSet<JobProcessingService *> *)values;
- (void)removeJobProcessingService:(NSSet<JobProcessingService *> *)values;

- (void)addMediaSupportedObject:(Media *)value;
- (void)removeMediaSupportedObject:(Media *)value;
- (void)addMediaSupported:(NSSet<Media *> *)values;
- (void)removeMediaSupported:(NSSet<Media *> *)values;

- (void)addParentsObject:(Printer *)value;
- (void)removeParentsObject:(Printer *)value;
- (void)addParents:(NSSet<Printer *> *)values;
- (void)removeParents:(NSSet<Printer *> *)values;

- (void)addPrintJobObject:(PrintJob *)value;
- (void)removePrintJobObject:(PrintJob *)value;
- (void)addPrintJob:(NSSet<PrintJob *> *)values;
- (void)removePrintJob:(NSSet<PrintJob *> *)values;

@end

NS_ASSUME_NONNULL_END
