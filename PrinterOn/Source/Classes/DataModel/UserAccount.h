//
//  UserAccount.h
//  PrinterOn
//
//  Created by Mark Burns on 11/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class GTMOAuth2Authentication, PrintJobFetched, Service;

@interface UserAccount : NSManagedObject

@property (nonatomic, retain) NSString * accountDescription;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSNumber * hasPassword;
@property (nonatomic, retain) NSNumber * isAnonymous;
@property (nonatomic, retain) NSNumber * isHostedDefault;
@property (nonatomic, retain) NSNumber * fromMDM;
@property (nonatomic, retain) PrintJobFetched * printJobFetched;
@property (nonatomic, retain) NSSet *serviceDefaults;

+ (UserAccount *)createUserAccount:(NSString *)account
                          password:(NSString *)password
                       description:(NSString *)description
                     hostedDefault:(BOOL)hosted
                           fromMDM:(BOOL)fromMDM
                   defaultServices:(NSSet *)defaults;

- (void)updateUserAccount:(NSString *)account
                 password:(NSString *)password
              description:(NSString *)description
            hostedDefault:(BOOL)hosted
                  fromMDM:(BOOL)fromMDM
          defaultServices:(NSSet *)defaults;

- (BOOL)hasChangesForUserAccount:(NSString *)account
                        password:(NSString *)password
                     description:(NSString *)description
                   hostedDefault:(BOOL)hosted
                 defaultServices:(NSSet *)defaults;

- (void)setServiceDefaults:(NSSet *)services inContext:(NSManagedObjectContext *)context;
+ (void)removeHostedDefault:(NSManagedObjectContext *)context;
- (void)setHostedDefault:(NSManagedObjectContext *)context;
- (NSString *)getUserAccountPassword;
- (void)migrateOldUserAccountPassword;
- (void)saveOAuth2Authentication:(GTMOAuth2Authentication *)auth forService:(NSString *)service;
- (void)deleteOAuth2AuthenticationForService:(NSString *)service;
- (GTMOAuth2Authentication *)getOAuth2AuthenticationForService:(NSString *)service;

+ (UserAccount *)anonymousUser;
+ (UserAccount *)getUserAccountForURL:(NSURL *)url;
+ (UserAccount *)getMDMUserAccount;

@end

@interface UserAccount (CoreDataGeneratedAccessors)

- (void)addServiceDefaultsObject:(Service *)value;
- (void)removeServiceDefaultsObject:(Service *)value;
- (void)addServiceDefaults:(NSSet *)values;
- (void)removeServiceDefaults:(NSSet *)values;

@end

@protocol UserAccountDelegate

- (void)didFinishWithUserAccount:(UserAccount *)account;

@end
