//
//  UserAccount.m
//  PrinterOn
//
//  Created by Mark Burns on 11/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "UserAccount.h"

#import "GTMOAuth2Authentication.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "ImageManager.h"
#import "NetworkBrowser.h"
#import "NSData+Transform.h"
#import "OAuth2Manager.h"
#import "PrintJobFetched.h"
#import "Service.h"
#import "ServiceAccess.h"
#import "ServiceCapabilities.h"
#import "ServiceLinks.h"

#import <SAMKeychain/SAMKeychain.h>

@implementation UserAccount

@dynamic accountDescription;
@dynamic userName;
@dynamic hasPassword;
@dynamic isAnonymous;
@dynamic isHostedDefault;
@dynamic fromMDM;
@dynamic printJobFetched;
@dynamic serviceDefaults;

- (NSString *)getUserAccountServiceName
{
    static NSString *serviceName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *bundle = [ImageManager mainBundleIdentifier];

        #if defined(AF_APP_EXTENSIONS)
            NSRange lastDotRange = [bundle rangeOfString:@"." options:NSBackwardsSearch];
            if (lastDotRange.location != NSNotFound) {
                bundle = [bundle substringToIndex:lastDotRange.location];
            }
        #endif

        serviceName = [NSString stringWithFormat:@"%@.%@", bundle, @"UserAccount"];
        NSData *data = [serviceName dataUsingEncoding:NSUTF8StringEncoding];
        [data transform];
        serviceName = [data hexString];
    });
    return serviceName;
}

+ (UserAccount *)createUserAccount:(NSString *)account
                 password:(NSString *)password
              description:(NSString *)description
            hostedDefault:(BOOL)hosted
                  fromMDM:(BOOL)fromMDM
          defaultServices:(NSSet *)defaults
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block UserAccount *userAccount;
    [context performBlockAndWait:^() {
        userAccount = [NSEntityDescription insertNewObjectForEntityForName:@"UserAccount" inManagedObjectContext:context];
        userAccount.userName = account;

        if (hosted) {
            [UserAccount removeHostedDefault:context];
        }

        userAccount.accountDescription = (description && description.length > 0) ? description : account;
        userAccount.isAnonymous = @NO;
        userAccount.isHostedDefault = @(hosted);
        userAccount.fromMDM = @(fromMDM);

        [userAccount setServiceDefaults:defaults inContext:context];
        [userAccount addServiceDefaults:defaults];

        userAccount.hasPassword = @((BOOL)(password && password.length > 0));

        // Save to persistence
        NSError *error;
        [context saveToPersistentStore:&error];
        if (error) {
            NSLog(@"Error saving user account: %@", error);
        } else {
            [userAccount saveUserAccountPassword:password];
        }
    }];

    // Requery the network services because we may have added an account that will add/remove printer access
    [NetworkBrowser requeryPONNetworkServices];

    return userAccount;
}

- (void)updateUserAccount:(NSString *)account
                 password:(NSString *)password
              description:(NSString *)description
            hostedDefault:(BOOL)hosted
                  fromMDM:(BOOL)fromMDM
          defaultServices:(NSSet *)defaults
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    [context performBlockAndWait:^() {
        // If the account name changed we need to delete any old OAuth stored tokens issued by CPS
        // If the password changed we need to delete any old OAuth stored tokens issued by CPS
        BOOL userChanged = ![self.userName isEqualToString:account];
        BOOL passwordChanged = ![[self getUserAccountPassword] isEqualToString:password];
        if (passwordChanged || userChanged) {
            for (Service *service in self.serviceDefaults) {
                if ([ServiceCapabilities serviceSupportsOAuthFirstParty:service.serviceURL]) {
                    [self deleteOAuth2AuthenticationForService:service.serviceURL];
                }
            }
        }

        self.userName = account;

        if (hosted && !self.isHostedDefault.boolValue) {
            [UserAccount removeHostedDefault:context];
        }

        self.accountDescription = (description && description.length > 0) ? description : account;
        self.isAnonymous = @NO;
        self.isHostedDefault = @(hosted);
        self.fromMDM = @(fromMDM);

        [self setServiceDefaults:defaults inContext:context];
        [self addServiceDefaults:defaults];
        self.hasPassword = @((BOOL)(password && password.length > 0));

        // Save to persistence
        NSError *error;
        [context saveToPersistentStore:&error];
        if (error) {
            NSLog(@"Error saving user account: %@", error);
        } else if (passwordChanged) {
            [self saveUserAccountPassword:password];
        }
    }];

    // Requery the network services because we may have added an account that will add/remove printer access
    [NetworkBrowser requeryPONNetworkServices];
}

- (BOOL)hasChangesForUserAccount:(NSString *)account
                        password:(NSString *)password
                     description:(NSString *)description
                   hostedDefault:(BOOL)hosted
                 defaultServices:(NSSet *)defaults
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block BOOL hasChanges = NO;

    [context performBlockAndWait:^() {
        if (hosted != self.isHostedDefault.boolValue) {
            hasChanges = YES;
            return;
        }

        if (![self.userName isEqualToString:account]) {
            hasChanges = YES;
            return;
        }

        NSString *modifiedDesc = [self.accountDescription isEqualToString:self.userName] ? @"" : self.accountDescription;
        if (![modifiedDesc isEqualToString:description] && ![description isEqualToString:account]) {
            hasChanges = YES;
            return;
        }

        NSString *modifiedPass = self.hasPassword.boolValue ? [self getUserAccountPassword] : @"";
        if (![modifiedPass isEqualToString:password]) {
            hasChanges = YES;
            return;
        }

        NSSet *oldDefaults = [self getServiceDefaults];
        if (defaults.count != oldDefaults.count) {
            hasChanges = YES;
            return;
        }

        for (Service *service in defaults) {
            if (![oldDefaults containsObject:service]) {
                hasChanges = YES;
                return;
            }
        }
    }];

    return hasChanges;
}

- (NSSet *)getServiceDefaults
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block NSMutableSet *defaults = [NSMutableSet set];
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

        for (Service *service in results) {
            if ([service.defaultUser isEqual:self]) {
                [defaults addObject:service];
            }
        }
    }];

    return defaults;
}

- (void)setServiceDefaults:(NSSet *)services inContext:(NSManagedObjectContext *)context
{
    if (context == nil) context = [CoreDataManager getManagedContext];

    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

        BOOL shouldSave = NO;
        for (Service *service in results) {
            if ([service.defaultUser isEqual:self]) {
                if (![services containsObject:service]) {
                    service.defaultUser = nil;
                    [self deleteOAuth2AuthenticationForService:service.serviceURL];
                    shouldSave = YES;
                }
            } else {
                if ([services containsObject:service]) {
                    // If we are removing an already set defaultUser make sure we clear any stored OAuth information
                    if (service.defaultUser) {
                        [service.defaultUser deleteOAuth2AuthenticationForService:service.serviceURL];
                    }
                    service.defaultUser = self;
                    shouldSave = YES;
                }
            }
        }

        // Save to persistence
        if (shouldSave) {
            NSError *error;
            [context saveToPersistentStore:&error];
            if (error) NSLog(@"Error saving service defaults: %@", error);
        }
    }];
}

+ (void)removeHostedDefault:(NSManagedObjectContext *)context
{
    if (context == nil) context = [CoreDataManager getManagedContext];

    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"UserAccount" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

        BOOL shouldSave = NO;
        for (UserAccount *account in results) {
            account.isHostedDefault = @NO;
            shouldSave = YES;
        }

        // Save to persistence
        if (shouldSave) {
            NSError *error;
            [context saveToPersistentStore:&error];
            if (error) NSLog(@"Error removing hosted default: %@", error);
        }
    }];
}

- (void)setHostedDefault:(NSManagedObjectContext *)context
{
    if (context == nil) context = [CoreDataManager getManagedContext];
    
    [context performBlockAndWait:^() {
        self.isHostedDefault = @YES;

        NSError *error;
        [context saveToPersistentStore:&error];
        if (error) NSLog(@"Error setting hosted default: %@", error);
    }];
}

- (NSString *)getUserAccountPassword
{
    __block NSString *password = nil;
    [self.managedObjectContext performBlockAndWait:^() {
        if (!self.hasPassword.boolValue) return;

        NSData *uData = [self.objectID.URIRepresentation.absoluteString dataUsingEncoding:NSUTF8StringEncoding];
        [uData transform];

        NSError *error;
        password = [SAMKeychain passwordForService:[self getUserAccountServiceName] account:[uData hexString] error:&error];
        if (error) NSLog(@"Error retrieving user account password from keychain: %@", error);
    }];

    if (password) {
        // Decrypt password
        NSData *bytes = [NSData toBytes:password];
        [bytes transform];
        password = [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
    }

    return password;
}

- (void)saveUserAccountPassword:(NSString *)password
{
    [self.managedObjectContext performBlockAndWait:^() {
        if (!self.hasPassword.boolValue) return;
        
        // Encrypt the password and user account name
        NSData *pData = [password dataUsingEncoding:NSUTF8StringEncoding];
        [pData transform];
        NSData *uData = [self.objectID.URIRepresentation.absoluteString dataUsingEncoding:NSUTF8StringEncoding];
        [uData transform];
        
        // Save the password to the keychain
        NSError *error;
        [SAMKeychain setPassword:[pData hexString] forService:[self getUserAccountServiceName] account:[uData hexString] error:&error];
        if (error) NSLog(@"Error saving user account password to keychain: %@", error);
    }];
}

- (void)deleteUserAccountPassword
{
    [self.managedObjectContext performBlockAndWait:^() {
        NSData *uData = [self.objectID.URIRepresentation.absoluteString dataUsingEncoding:NSUTF8StringEncoding];
        [uData transform];
        
        NSError *error;
        [SAMKeychain deletePasswordForService:[self getUserAccountServiceName] account:[uData hexString] error:&error];
        if (error) NSLog(@"Error deleting user account password from keychain: %@", error);
    }];
}

- (void)migrateOldUserAccountPassword
{
    [self.managedObjectContext performBlockAndWait:^() {
        if (!self.hasPassword.boolValue) return;

        NSData *uData = [self.userName dataUsingEncoding:NSUTF8StringEncoding];
        [uData transform];

        // Get the password stored in old keychain location
        NSError *error;
        NSString *password = [SAMKeychain passwordForService:[self getUserAccountServiceName] account:[self.objectID.URIRepresentation.host stringByAppendingString:[uData hexString]] error:&error];
        if (error) {
            NSLog(@"Error retrieving user account password from keychain: %@", error);
            return;
        }

        if (password) {
            // Decrypt password
            NSData *bytes = [NSData toBytes:password];
            [bytes transform];
            password = [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
        }

        // Delete the old keychain item
        [SAMKeychain deletePasswordForService:[self getUserAccountServiceName] account:[self.objectID.URIRepresentation.host stringByAppendingString:[uData hexString]] error:&error];
        if (error) NSLog(@"Error deleting user account password from keychain: %@", error);

        // Save the password to the new keychain location
        if (password && password.length > 0) {
            [self saveUserAccountPassword:password];
        }
    }];
}

- (void)saveOAuth2Authentication:(GTMOAuth2Authentication *)auth forService:(NSString *)service
{
    service = [Service cleanServiceURL:service];

    NSData *uData = [self.objectID.URIRepresentation.absoluteString dataUsingEncoding:NSUTF8StringEncoding];
    [uData transform];
    NSData *sData = [service dataUsingEncoding:NSUTF8StringEncoding];
    [sData transform];
    NSString *keychainName = [NSString stringWithFormat:@"%@.%@.%@", [self getUserAccountServiceName], uData.hexString, sData.hexString];

    NSError *error;
    [GTMOAuth2ViewControllerTouch saveParamsToKeychainForName:keychainName accessibility:kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly authentication:auth error:&error];
    if (error) NSLog(@"Error saving user account OAuth to keychain: %@", error);
}

- (void)deleteOAuth2AuthenticationForService:(NSString *)service
{
    NSMutableArray *serviceSet = nil;
    if (service == nil || service.length == 0) {
        if (self.serviceDefaults.count > 0) {
            serviceSet = [NSMutableArray array];
            for (Service *loopService in self.serviceDefaults) {
                [serviceSet addObject:loopService.serviceURL];
            }
        }
    } else {
        service = [Service cleanServiceURL:service];
        serviceSet = [NSMutableArray arrayWithObject:service];
    }

    for (NSString *serviceURL in serviceSet) {
        NSData *uData = [self.objectID.URIRepresentation.absoluteString dataUsingEncoding:NSUTF8StringEncoding];
        [uData transform];
        NSData *sData = [serviceURL dataUsingEncoding:NSUTF8StringEncoding];
        [sData transform];
        NSString *keychainName = [NSString stringWithFormat:@"%@.%@.%@", [self getUserAccountServiceName], uData.hexString, sData.hexString];

        [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:keychainName];
    }
}

- (GTMOAuth2Authentication *)getOAuth2AuthenticationForService:(NSString *)service
{
    service = [Service cleanServiceURL:service];

    NSData *uData = [self.objectID.URIRepresentation.absoluteString dataUsingEncoding:NSUTF8StringEncoding];
    [uData transform];
    NSData *sData = [service dataUsingEncoding:NSUTF8StringEncoding];
    [sData transform];
    NSString *keychainName = [NSString stringWithFormat:@"%@.%@.%@", [self getUserAccountServiceName], uData.hexString, sData.hexString];

    NSError *error;
    GTMOAuth2Authentication *auth = [OAuth2Manager authForPON:keychainName serviceURL:service error:&error];
    if (error) NSLog(@"Error retrieving user account OAuth from keychain: %@", error);

    return auth;
}

+ (UserAccount *)getHostedDefaultUserAccount
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block UserAccount *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"UserAccount" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isHostedDefault == YES"];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        } else {
            result = [UserAccount anonymousUser];
        }
    }];

    return result;
}

+ (UserAccount *)anonymousUser
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block UserAccount *account;
    [context performBlockAndWait:^(){
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserAccount" inManagedObjectContext:context];
        account = (UserAccount *)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
        account.accountDescription = @"Anonymous Account";
        account.userName = @"ios@guest.user";
        account.hasPassword = @NO;
        account.isAnonymous = @YES;
        account.isHostedDefault = @YES;
        account.fromMDM = @NO;
    }];

    return account;
}

+ (UserAccount *)getMDMUserAccount
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block UserAccount *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"UserAccount" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        for (UserAccount *account in results) {
            if([account.fromMDM isEqualToNumber:@YES]){
                result = account;
                break;
            }
        }
    }];

    return result;
}

+ (UserAccount *)getUserAccountForURL:(NSURL *)url
{
    NSString *serviceHost = [NSURL URLWithString:[Service hostedService].serviceURL].host;
    NSString *targetHost = url.host;

    // Check if the host is a match for hosted service
    if ([serviceHost.lowercaseString isEqualToString:targetHost.lowercaseString]) {
        return [UserAccount getHostedDefaultUserAccount];
    }

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block UserAccount *result = [UserAccount anonymousUser];
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

        for (Service *service in results) {
            NSString *serviceHost = [NSURL URLWithString:service.serviceURL].host;
            if ([targetHost.lowercaseString isEqualToString:serviceHost.lowercaseString]) {
                if (service.defaultUser) {
                    result = service.defaultUser;
                }
                break;
            }
        }
    }];

    return result;
}

- (void)prepareForDeletion
{
    // Delete stored OAuth information here instead of willSave because by the time we hit willSave
    // the relationship with defaultServices will have been nullified and we won't find any stored keys
    [self deleteOAuth2AuthenticationForService:nil];
}

- (void)willSave
{
    [super willSave];

    if (self.deleted) {
        // Delete the account from the keychain when this object is deleted
        [self deleteUserAccountPassword];

        // Requery the network services because we may have deleted an account that will add/remove printer access
        [NetworkBrowser requeryPONNetworkServices];
    }
}

@end
