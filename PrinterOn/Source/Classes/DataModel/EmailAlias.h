//
//  EmailAlias.h
//  PrinterOn
//
//  Created by Mark Burns on 11/13/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class Printer;

@interface EmailAlias : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * enabled;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) Printer *printer;

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store;

@end
