//
//  Media.h
//  PrinterOn
//
//  Created by Mark Burns on 11/13/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class Printer;

@interface Media : NSManagedObject

@property (nonatomic, retain) NSNumber * isDefault;
@property (nonatomic, retain) NSNumber * mediaSizeNum;
@property (nonatomic, retain) NSString * mediaSizeName;
@property (nonatomic, retain) NSString * mediaSizeDim;
@property (nonatomic, retain) Printer *printer;

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store;

@end
