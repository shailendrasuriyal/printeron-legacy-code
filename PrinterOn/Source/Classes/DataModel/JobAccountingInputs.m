//
//  JobAccountingInputs.m
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "JobAccountingInputs.h"

#import "ClientUIDInput.h"
#import "EmailAddressInput.h"
#import "NetworkLoginInput.h"
#import "NSString+Email.h"
#import "Printer.h"
#import "ReleaseCodeInput.h"
#import "SessionMetadataInput.h"
#import "UserAccount.h"

NSString* const kJobInputTypeEmailAddressKey = @"emailAddress";
NSString* const kJobInputTypeClientUIDKey = @"clientUID";
NSString* const kJobInputTypeNetworkLoginKey = @"networkLogin";
NSString* const kJobInputTypeSessionMetadataKey = @"sessionMetadata";
NSString* const kJobInputTypeReleaseCodeKey = @"releaseCode";

@implementation JobAccountingInputs

@dynamic printer;
@dynamic networkLogin;
@dynamic emailAddress;
@dynamic clientUID;
@dynamic sessionMetadata;
@dynamic releaseCode;


#pragma mark - RestKit Mappings

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store
{
    RKEntityMapping *aliasMapping = [RKEntityMapping mappingForEntityForName:@"JobAccountingInputs" inManagedObjectStore:store];
    [aliasMapping addAttributeMappingsFromDictionary:@{}];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"jobAccounting.jobAccountingInputs" toKeyPath:@"jobAccountingInputs" withMapping:aliasMapping];
}

#pragma mark - Utility Methods

+ (BOOL)isJobInputStringRequired:(NSString *)string
{
    if (string.length > 0 && [string.lowercaseString isEqualToString:@"required"]) {
        return YES;
    }
    return NO;
}

+ (BOOL)isJobInputStringRequiredOrOptional:(NSString *)string
{
    if (string.length > 0 && ([string.lowercaseString isEqualToString:@"required"] || [string.lowercaseString isEqualToString:@"optional"])) {
        return YES;
    }
    return NO;
}

+ (NSString *)placeholderForInputString:(NSString *)string
{
    if (!string || string.length == 0) return nil;
    string = string.lowercaseString;

    if ([string isEqualToString:@"required"]) {
        return NSLocalizedPONString(@"LABEL_REQUIRED", nil);
    } else if ([string isEqualToString:@"optional"]) {
        return NSLocalizedPONString(@"LABEL_OPTIONAL", nil);
    }
    return nil;
}

+ (NSString *)releaseCodeTypeString:(NSString *)string
{
    if (!string || string.length == 0) return nil;
    string = string.lowercaseString;
    
    if ([string isEqualToString:@"numeric"]) {
        return NSLocalizedPONString(@"LABEL_NUMERIC", nil);
    } else if ([string isEqualToString:@"alphanumeric"]) {
        return NSLocalizedPONString(@"LABEL_ALPHANUMERIC", nil);
    }
    return nil;
}

+ (NSString *)releaseCodePatternForPrinter:(Printer *)printer
{
    NSString *pattern = printer.jobAccountingInputs.releaseCode.pattern;

    // Create a valid pattern based on the printer information if one doesn't exist
    if (pattern == nil || pattern.length == 0) {
        pattern = [printer.jobAccountingInputs.releaseCode.type.lowercaseString isEqualToString:@"alphanumeric"] ? @"^[0-9,a-z,A-Z]" : @"^[0-9]";
        int minLength = printer.jobAccountingInputs.releaseCode.minLength.intValue;
        int maxLength = printer.jobAccountingInputs.releaseCode.maxLength.intValue;
        [pattern stringByAppendingString:[NSString stringWithFormat:@"{%d,%d}$", minLength, maxLength]];
    }

    return pattern;
}

+ (BOOL)printerRequiresJobInputs:(Printer *)printer
{
    if (printer == nil) return NO;

    __block BOOL required = NO;
    [printer.managedObjectContext performBlockAndWait:^() {
        JobAccountingInputs *inputs = printer.jobAccountingInputs;
        if (inputs == nil) return;

        ClientUIDInput *clientUID = inputs.clientUID;
        if (clientUID && [JobAccountingInputs isJobInputStringRequiredOrOptional:clientUID.use]) {
            required = YES;
            return;
        }

        SessionMetadataInput *sessionMeta = inputs.sessionMetadata;
        if (sessionMeta && [JobAccountingInputs isJobInputStringRequiredOrOptional:sessionMeta.use]) {
            required = YES;
            return;
        }

        ReleaseCodeInput *releaseCode = inputs.releaseCode;
        if (releaseCode && [JobAccountingInputs isJobInputStringRequiredOrOptional:releaseCode.use]) {
            if (!releaseCode.autoCreate.boolValue) {
                required = YES;
                return;
            }
        }
        
        UserAccount *account;
        NetworkLoginInput *networkLogin = inputs.networkLogin;
        BOOL needsNetworkLogin = networkLogin ? [JobAccountingInputs isJobInputStringRequiredOrOptional:networkLogin.use] : NO;
        if (needsNetworkLogin) {
            if (!account) account = [UserAccount getUserAccountForURL:[printer getDocAPIAddress]];
            if (account.userName.length == 0) {
                required = YES;
                return;
            } else if ([account.userName isEqualToString:[UserAccount anonymousUser].userName]) {
                required = YES;
                return;
            }
        }

        EmailAddressInput *emailAddress = inputs.emailAddress;
        if (emailAddress && [JobAccountingInputs isJobInputStringRequiredOrOptional:emailAddress.use]) {
            if (!account) account = [UserAccount getUserAccountForURL:[printer getDocAPIAddress]];
            if (needsNetworkLogin) {
                required = YES;
                return;
            } else if ([account.userName isEqualToString:[UserAccount anonymousUser].userName]) {
                required = YES;
                return;
            } else if (![account.userName isEmailAddress]) {
                required = YES;
                return;
            }
        }
    }];

    return required;
}

+ (NSDictionary *)generatePopulatedInputs:(Printer *)printer
{
    if (printer == nil) return nil;

    __block NSMutableDictionary *dictionary;
    [printer.managedObjectContext performBlockAndWait:^() {
        JobAccountingInputs *inputs = printer.jobAccountingInputs;
        if (inputs == nil) return;

        UserAccount *account;

        EmailAddressInput *emailInput = inputs.emailAddress;
        if (emailInput && [JobAccountingInputs isJobInputStringRequiredOrOptional:emailInput.use]) {
            if (!account) account = [UserAccount getUserAccountForURL:[printer getDocAPIAddress]];
            if (account) {
                if (!dictionary) dictionary = [NSMutableDictionary dictionary];
                dictionary[kJobInputTypeEmailAddressKey] = account.userName;
            }
        }

        NetworkLoginInput *networkInput = inputs.networkLogin;
        if (networkInput && [JobAccountingInputs isJobInputStringRequiredOrOptional:networkInput.use]) {
            if (!account) account = [UserAccount getUserAccountForURL:[printer getDocAPIAddress]];
            if (account) {
                if (!dictionary) dictionary = [NSMutableDictionary dictionary];
                dictionary[kJobInputTypeNetworkLoginKey] = account.userName;
            }
        }
    }];

    if (dictionary.count > 0) return dictionary;
    return nil;
}

@end
