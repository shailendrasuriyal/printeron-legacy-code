//
//  EmailMessage.h
//  
//
//  Created by Mark Burns on 2015-10-28.
//
//

@class EmailFolder;

NS_ASSUME_NONNULL_BEGIN

@interface EmailMessage : NSManagedObject

@property (nullable, nonatomic, retain) NSNumber *uid;
@property (nullable, nonatomic, retain) id message;
@property (nullable, nonatomic, retain) EmailFolder *emailFolder;

- (void)clearAttachmentsCacheForMessage;

@end

NS_ASSUME_NONNULL_END
