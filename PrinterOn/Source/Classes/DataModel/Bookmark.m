//
//  Bookmark.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-08-11.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "Bookmark.h"

@implementation Bookmark

@dynamic displayOrder;
@dynamic bookmarkTitle;
@dynamic bookmarkURL;

@end
