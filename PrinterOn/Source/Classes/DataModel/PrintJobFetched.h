//
//  PrintJobFetched.h
//  PrinterOn
//
//  Created by Mark Burns on 2017-05-09.
//  Copyright (c) 2017 PrinterOn Inc. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@class UserAccount;

@interface PrintJobFetched : NSManagedObject

@property (nullable, nonatomic, retain) NSIndexSet *idSet;
@property (nullable, nonatomic, copy) NSString *serviceURL;
@property (nullable, nonatomic, retain) UserAccount *userAccount;

+ (NSFetchRequest<PrintJobFetched *> *)fetchRequest;

+ (nullable NSMutableIndexSet *)getJobIDsForService:(NSString *)serviceURL forUser:(UserAccount *)userAccount;
+ (void)updateJobIDsForService:(NSString *)serviceURL forUser:(UserAccount *)userAccount withIDs:(NSMutableIndexSet *)indexSet;

@end

NS_ASSUME_NONNULL_END
