//
//  EmailAccount.m
//  PrinterOn
//
//  Created by Mark Burns on 11/26/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "EmailAccount.h"

#import "AppConstants.h"
#import "EmailFolder.h"
#import "GTMOAuth2Authentication.h"
#import "ImageManager.h"
#import "NSData+Transform.h"
#import "OAuth2Manager.h"

#import "AppAuth.h"
#import <SAMKeychain/SAMKeychain.h>

@implementation EmailAccount

@dynamic emailAddress;
@dynamic hostName;
@dynamic port;
@dynamic useSSL;
@dynamic mailType;
@dynamic protocol;
@dynamic userName;
@dynamic emailFolder;

- (NSString *)getEmailAccountServiceName
{
    static NSString *serviceName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *bundle = [ImageManager mainBundleIdentifier];

        #if defined(AF_APP_EXTENSIONS)
            NSRange lastDotRange = [bundle rangeOfString:@"." options:NSBackwardsSearch];
            if (lastDotRange.location != NSNotFound) {
                bundle = [bundle substringToIndex:lastDotRange.location];
            }
        #endif

        serviceName = [NSString stringWithFormat:@"%@.%@", bundle, @"EmailAccount"];
        NSData *data = [serviceName dataUsingEncoding:NSUTF8StringEncoding];
        [data transform];
        serviceName = [data hexString];
    });
    return serviceName;
}

+ (void)createEmailAccount:(NSString *)address
                  password:(NSString *)password
                      auth:(id)auth
                      host:(NSString *)host
                      port:(NSUInteger)port
                       ssl:(BOOL)ssl
                  protocol:(EmailAccountProtocol)protocol
                      type:(EmailAccountType)type
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    [context performBlockAndWait:^() {
        // Check to see if an entry exists already
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"EmailAccount" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"emailAddress ==[c] '%@'", address]];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

        EmailAccount *account;
        if (results.count > 0) {
            account = results[0];
        } else {
            account = [NSEntityDescription insertNewObjectForEntityForName:@"EmailAccount" inManagedObjectContext:context];
            account.emailAddress = address;

            if (protocol == EmailAccountProtocolIMAP) {
                EmailFolder *emailFolder = [NSEntityDescription insertNewObjectForEntityForName:@"EmailFolder" inManagedObjectContext:context];
                emailFolder.folderName = @"INBOX";
                emailFolder.order = @(0);
                [account addEmailFolderObject:emailFolder];
            }
        }

        //account.userName = user;
        account.hostName = host;
        account.port = @(port);
        account.useSSL = @(ssl);
        account.mailType = @((int)type);
        account.protocol = @((int)protocol);

        // Save to persistence
        NSError *error;
        [context saveToPersistentStore:&error];
        if (error) NSLog(@"Error saving email account: %@", error);

        // Save the password to the keychain
        if ([account isUsingOAuth2] && auth) {
            [account saveOAuth2Authentication:auth];
        } else {
            [account saveEmailAccountPassword:password];
        }
    }];
}

- (NSString *)getEmailAccountPassword
{
    NSData *uData = [self.emailAddress dataUsingEncoding:NSUTF8StringEncoding];
    [uData transform];

    NSError *error;
    NSString *password = [SAMKeychain passwordForService:[self getEmailAccountServiceName] account:[self.objectID.URIRepresentation.host stringByAppendingString:[uData hexString]] error:&error];
    if (error) NSLog(@"Error retrieving user account password from keychain: %@", error);

    // Decrypt password
    NSData *bytes = [NSData toBytes:password];
    [bytes transform];
    return [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
}

- (void)saveEmailAccountPassword:(NSString *)password
{
    // Encrypt the password and user account name
    NSData *pData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [pData transform];
    NSData *uData = [self.emailAddress dataUsingEncoding:NSUTF8StringEncoding];
    [uData transform];

    // Save the password to the keychain
    NSError *error;
    [SAMKeychain setPassword:[pData hexString] forService:[self getEmailAccountServiceName] account:[self.objectID.URIRepresentation.host stringByAppendingString:[uData hexString]] error:&error];
    if (error) NSLog(@"Error saving user account password to keychain: %@", error);
}

- (void)deleteEmailAccountPassword
{
    NSData *uData = [self.emailAddress dataUsingEncoding:NSUTF8StringEncoding];
    [uData transform];
    
    NSError *error;
    [SAMKeychain deletePasswordForService:[self getEmailAccountServiceName] account:[self.objectID.URIRepresentation.host stringByAppendingString:[uData hexString]] error:&error];
    if (error) NSLog(@"Error deleting user account password from keychain: %@", error);
}

- (EmailFolder *)getFolderWithName:(NSString *)name
{
    if (!name) return nil;

    NSSet *folderSet = [self.emailFolder objectsPassingTest:^BOOL(id obj, BOOL *stop) {
        if ([obj isMemberOfClass:[EmailFolder class]]) {
            if ([((EmailFolder *)obj).folderName isEqualToString:name]) {
                *stop = YES;
                return YES;
            }
        }
        return NO;
    }];
    EmailFolder *emailFolder = [folderSet anyObject];
    return emailFolder;
}

- (id)getOAuth2Authentication
{
    NSData *uData = [self.emailAddress dataUsingEncoding:NSUTF8StringEncoding];
    [uData transform];
    NSString *keychainName = [[self getEmailAccountServiceName] stringByAppendingString:uData.hexString];

    NSError *error;
    if (self.mailType.intValue == EmailAccountTypeGMail) {
        NSData *authorizationData = [SAMKeychain passwordDataForService:keychainName account:@"OAuth" error:&error];
        if (error) NSLog(@"Error retrieving email account authentication from keychain: %@", error);

        if (!authorizationData) return nil;

        OIDAuthState *auth = (OIDAuthState *)[NSKeyedUnarchiver unarchiveObjectWithData:authorizationData];
        return auth;
    } else if (self.mailType.intValue == EmailAccountTypeOutlook) {
        GTMOAuth2Authentication *auth = [OAuth2Manager authForOutlook:keychainName clientID:[AppConstants outlookOAuth2ClientID] clientSecret:[AppConstants outlookOAuth2ClientSecret] error:&error];
        if (error) NSLog(@"Error retrieving email account authentication from keychain: %@", error);
        return auth;
    } else {
        return nil;
    }
}

- (void)saveOAuth2Authentication:(id)auth
{
    NSData *uData = [self.emailAddress dataUsingEncoding:NSUTF8StringEncoding];
    [uData transform];
    NSString *keychainName = [[self getEmailAccountServiceName] stringByAppendingString:uData.hexString];

    NSError *error;
    if (self.mailType.intValue == EmailAccountTypeGMail) {
        NSData *authorizationData = [NSKeyedArchiver archivedDataWithRootObject:auth];
        [SAMKeychain setPasswordData:authorizationData forService:keychainName account:@"OAuth" error:&error];
    } else if (self.mailType.intValue == EmailAccountTypeOutlook) {
        [GTMOAuth2ViewControllerTouch saveParamsToKeychainForName:keychainName accessibility:kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly authentication:auth error:&error];
    }
    if (error) NSLog(@"Error saving email account authentication to keychain: %@", error);
}

- (void)deleteOAuth2Authentication
{
    NSData *uData = [self.emailAddress dataUsingEncoding:NSUTF8StringEncoding];
    [uData transform];
    NSString *keychainName = [[self getEmailAccountServiceName] stringByAppendingString:uData.hexString];

    if (self.mailType.intValue == EmailAccountTypeGMail) {
        NSError *error;
        [SAMKeychain deletePasswordForService:keychainName account:@"OAuth" error:&error];
        if (error) NSLog(@"Error deleting email account authentication from keychain: %@", error);
    } else if (self.mailType.intValue == EmailAccountTypeOutlook) {
        [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:keychainName];
    }
}

- (BOOL)isUsingOAuth2
{
    EmailAccountType type = self.mailType.intValue;
    return (type == EmailAccountTypeGMail || type == EmailAccountTypeOutlook);
}

+ (NSString *)getHostNameForType:(EmailAccountType)type
{
    switch (type) {
        case EmailAccountTypeICloud:
            return @"imap.mail.me.com";
        case EmailAccountTypeGMail:
            return @"imap.gmail.com";
        case EmailAccountTypeOutlook:
            return @"imap-mail.outlook.com";
        case EmailAccountTypeYahoo:
            return @"imap.mail.yahoo.com";
        default:
            return nil;
    }
}

- (void)clearAttachmentsCacheForAccount
{
    for (EmailFolder *folder in self.emailFolder) {
        [folder clearAttachmentsCacheForFolder];
    }
}

- (void)willSave
{
    [super willSave];
    
    // Delete the account from the keychain when this object is deleted
    if (self.deleted) {
        if ([self isUsingOAuth2]) {
            [self deleteOAuth2Authentication];
        } else {
            [self deleteEmailAccountPassword];
        }
    }
}

@end
