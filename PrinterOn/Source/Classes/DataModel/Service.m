//
//  Service.m
//  PrinterOn
//
//  Created by Mark Burns on 1/19/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "Service.h"

#import "NetworkBrowser.h"
#import "NSData+Transform.h"
#import "ServiceDescription.h"
#import "UAObfuscatedString.h"
#import "UserAccount.h"

@implementation Service

@dynamic serviceDescription;
@dynamic serviceLock;
@dynamic serviceURL;
@dynamic isDefault;
@dynamic isMDM;
@dynamic isRestricted;
@dynamic defaultUser;


+ (NSString *)hostData
{
    static NSString *data = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Hosted service API key
        // Encode: Key string -> NSData -> XOR -> Hex string
        NSData *bytes = [NSData toBytes:NSMutableString.new._1._3._2.a._3.d._3._0._6._6._5._8._3._1._1.a._2._6._0._6._1.a._5._4._5._2._2.d._1.a._5.f._3.a._1._6._2.b._4._1._3._9._1.b._3.a._2.a._4.b._5._9._2.d._4._3._5._1._4._3._0._0._1.c];
        [bytes transform];
        data = [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
    });
    return data;
}

+ (Service *)createService:(NSString *)URL
               description:(NSString *)description
                 isDefault:(BOOL)boolDefault
                  isLocked:(BOOL)boolLocked
                     isMDM:(BOOL)boolMDM
              isRestricted:(BOOL)boolRestricted
           completionBlock:(void (^)(NSManagedObjectID *capabilitiesID))completionBlock
{
    URL = [Service cleanServiceURL:URL];

    if ([Service getRestrictedService] != nil) return nil;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block Service *service;
    [context performBlockAndWait:^() {
        NSArray *results = [Service doesServiceExist:URL inContext:context];
        
        if (results.count > 0) {
            service = results[0];
            if (service.serviceLock.boolValue) {
                return;
            }
        } else {
            service = [NSEntityDescription insertNewObjectForEntityForName:@"Service" inManagedObjectContext:context];
            service.serviceURL = URL;
        }

        if (boolDefault) {
            Service *currentDefault = [Service getDefaultService];
            if (!currentDefault.serviceLock.boolValue) {
                [Service removeServiceDefault:context];
                service.isDefault = @(boolDefault);
            }
        } else {
            service.isDefault = @(boolDefault);
        }

        service.serviceDescription = (description && description.length > 0) ? description : [NSURL URLWithString:URL].host;
        service.serviceLock = @(boolLocked);

        service.isMDM = @(boolMDM);
        service.isRestricted = @(boolRestricted);
        if (boolRestricted) {
            [service removeOtherServices:context];
            [UserAccount removeHostedDefault:context];
        }

        // Save to persistence
        if (context.hasChanges) {
            NSError *error;
            [context saveToPersistentStore:&error];
            if (error) {
                NSLog(@"Error saving service: %@", error);
            }
        }
    }];

    // Query Service Capabilities after creating
    [ServiceDescription capabilitiesForServiceWithURL:[NSURL URLWithString:service.serviceURL] forceUpdate:YES completionBlock:completionBlock];

    // Requery the network services because we may have added a service that can now be trusted
    [NetworkBrowser requeryPONNetworkServices];

    return service;
}

- (void)updateService:(NSString *)URL
          description:(NSString *)description
            isDefault:(BOOL)boolDefault
             isLocked:(BOOL)boolLocked
                isMDM:(BOOL)boolMDM
         isRestricted:(BOOL)boolRestricted
      completionBlock:(void (^)(NSManagedObjectID *capabilitiesID))completionBlock
{
    URL = [Service cleanServiceURL:URL];

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    [context performBlockAndWait:^() {
        NSArray *results = [Service doesServiceExist:URL inContext:context];
        if (results.count > 0) {
            Service *found = results.firstObject;
            if (![found.objectID isEqual:self.objectID]) {
                [context deleteObject:found];
            }
        }

        self.serviceURL = URL;
        self.serviceDescription = (description && description.length > 0) ? description : [NSURL URLWithString:URL].host;
        self.serviceLock = @(boolLocked);

        if (boolDefault && !self.isDefault.boolValue) {
            Service *currentDefault = [Service getDefaultService];
            if (!currentDefault.serviceLock.boolValue || [currentDefault.objectID isEqual:self.objectID]) {
                [Service removeServiceDefault:context];
                self.isDefault = @(boolDefault);
            }
        }

        if (!boolDefault) {
            self.isDefault = @(boolDefault);
        }

        self.isMDM = @(boolMDM);
        self.isRestricted = @(boolRestricted);
        if (boolRestricted) {
            [self removeOtherServices:context];
            [UserAccount removeHostedDefault:context];
        }

        // Save to persistence
        if (context.hasChanges) {
            NSError *error;
            [context saveToPersistentStore:&error];
            if (error) NSLog(@"Error saving service: %@", error);
        }
    }];

    // Query Service Capabilities after creating
    [ServiceDescription capabilitiesForServiceWithURL:[NSURL URLWithString:self.serviceURL] forceUpdate:YES completionBlock:completionBlock];

    // Requery the network services because we may have updated a service that can now be trusted
    [NetworkBrowser requeryPONNetworkServices];
}

- (BOOL)hasChangesForService:(NSString *)URL
                 description:(NSString *)description
                   isDefault:(BOOL)boolDefault
{
    URL = [Service cleanServiceURL:URL];

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block BOOL hasChanges = NO;

    [context performBlockAndWait:^() {
        if (boolDefault != self.isDefault.boolValue) {
            hasChanges = YES;
            return;
        }

        if (![URL isEqualToString:self.serviceURL]) {
            hasChanges = YES;
            return;
        }

        NSString *modifiedDesc = [self.serviceDescription isEqualToString:[NSURL URLWithString:self.serviceURL].host] ? @"" : self.serviceDescription;
        if (![modifiedDesc isEqualToString:description] && ![description isEqualToString:[NSURL URLWithString:URL].host]) {
            hasChanges = YES;
            return;
        }
    }];

    return hasChanges;
}

- (void)removeOtherServices:(NSManagedObjectContext *)context
{
    if (context == nil) context = [CoreDataManager getManagedContext];

    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        for (Service *service in results) {
            if (![service isEqual:self]) {
                [context deleteObject:service];
            }
        }
    }];
}

+ (BOOL)compareURL:(NSString *)url1 toURL:(NSString *)url2
{
    if (url1 == nil || url2 == nil) return NO;

    NSURL *firstURL = [NSURL URLWithString:url1];
    NSURL *secondURL = [NSURL URLWithString:url2];

    if (firstURL == nil || secondURL == nil) return NO;

    if (![firstURL.scheme.lowercaseString isEqualToString:secondURL.scheme.lowercaseString]) return NO;
    if (![firstURL.host.lowercaseString isEqualToString:secondURL.host.lowercaseString]) return NO;
    if ([firstURL.path compare:secondURL.path] != NSOrderedSame) return NO;

    return YES;
}

+ (NSString *)cleanServiceURL:(NSString *)urlString
{
    if (urlString.length == 0) return nil;

    if ([urlString hasSuffix:@"/"]) {
        urlString = [urlString substringToIndex:urlString.length - 1];
    }

    NSURL *url = [NSURL URLWithString:urlString];

    NSNumber *port = url.port;
    if (port == nil) return url.absoluteString;

    NSString *scheme = url.scheme.lowercaseString;
    int portValue = port.intValue;

    if ([scheme isEqualToString:@"https"] && portValue == 443) {
        NSString *newURL = url.absoluteString;
        NSRange range = [newURL rangeOfString:@":443"];
        if (range.location != NSNotFound) {
            newURL = [newURL stringByReplacingCharactersInRange:range withString:@""];
        }
        return newURL;
    }

    if ([scheme isEqualToString:@"http"] && portValue == 80) {
        NSString *newURL = url.absoluteString;
        NSRange range = [newURL rangeOfString:@":80"];
        if (range.location != NSNotFound) {
            newURL = [newURL stringByReplacingCharactersInRange:range withString:@""];
        }
        return newURL;
    }

    return url.absoluteString;
}

+ (NSArray *)doesServiceExist:(NSString *)URL inContext:(NSManagedObjectContext *)context
{
    URL = [Service cleanServiceURL:URL];

    if (context == nil) context = [CoreDataManager getManagedContext];

    __block NSArray *results = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"serviceURL ==[c] '%@'", URL]];
        fetchRequest.predicate = predicate;

        results = [context executeFetchRequest:fetchRequest error:nil];
    }];

    return results;
}

+ (void)removeServiceDefault:(NSManagedObjectContext *)context
{
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

        for (Service *service in results) {
            service.isDefault = @NO;
        }
    }];
}

+ (Service *)getDefaultService
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block Service *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault == YES"];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        } else {
            result = [Service hostedService];
        }
    }];

    return result;
}

+ (Service *)getMDMService
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block Service *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isMDM == YES"];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        }
    }];

    return result;
}

+ (Service *)getRestrictedService
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block Service *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isRestricted == YES"];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        }
    }];

    return result;
}

+ (Service *)hostedService
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block Service *service;
    [context performBlockAndWait:^(){
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];
        service = (Service *)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
        service.serviceDescription = [NSString stringWithFormat:@"PrinterOn %@", NSLocalizedPONString(@"LABEL_HOSTED", nil)];
        service.serviceLock = @NO;
        service.serviceURL = @"https://www.printeron.net";
        service.isDefault = @YES;
        service.isMDM = @NO;
    }];

    return service;
}

+ (BOOL)isHostedURL:(NSURL *)serviceURL andPublic:(BOOL)publicHosted
{
    if (!serviceURL) return NO;

    NSString *host = serviceURL.host.lowercaseString;
    if (!host || host.length == 0) return NO;

    if (publicHosted) {
        return [host isEqualToString:@"www.printeron.net"] || [host isEqualToString:@"printeron.net"];
    } else {
        return [host hasSuffix:@".printeron.net"] || [host isEqualToString:@"printeron.net"];
    }
}

+ (BOOL)isHostedString:(NSString *)serviceURL andPublic:(BOOL)publicHosted
{
    if (!serviceURL) return NO;

    NSURL *url = [NSURL URLWithString:serviceURL];
    return [Service isHostedURL:url andPublic:publicHosted];
}

+ (BOOL)isSecureURL:(NSURL *)serviceURL
{
    NSString *schema = serviceURL.scheme.lowercaseString;
    return [schema isEqualToString:@"https"];
}

+ (BOOL)isHostTrusted:(NSString *)host
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block BOOL result = NO;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        for (Service *service in results) {
            NSURL *url = [NSURL URLWithString:service.serviceURL];
            if ([url.host.lowercaseString isEqualToString:host.lowercaseString]) {
                result = YES;
                break;
            }
        }
    }];

    return result;
}

+ (void)validateServerTrust:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        // Verify certificate
        SecTrustResultType trustResult;
        OSStatus status = SecTrustEvaluate(challenge.protectionSpace.serverTrust, &trustResult);
        BOOL trusted = (status == errSecSuccess) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
        
        if (trusted) {
            // The certificate was trusted
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        } else {
            // The certificate was not trusted, check to see if we trust the host
            if ([Service isHostTrusted:challenge.protectionSpace.host]) {
                [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
            } else {
                // Host was not trusted, fail authentication
                //[challenge.sender cancelAuthenticationChallenge:challenge];
                [challenge.sender performDefaultHandlingForAuthenticationChallenge:challenge];
            }
        }
    } else {
        // Was not a certificate challenge so perform the default handling
        [challenge.sender performDefaultHandlingForAuthenticationChallenge:challenge];
    }
}

+ (NSURLCredential *)credentialForServerTrust:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        // Verify certificate
        SecTrustResultType trustResult;
        OSStatus status = SecTrustEvaluate(challenge.protectionSpace.serverTrust, &trustResult);
        BOOL trusted = (status == errSecSuccess) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
        
        if (trusted) {
            // The certificate was trusted
            return [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        } else {
            // The certificate was not trusted, check to see if we trust the host
            if ([Service isHostTrusted:challenge.protectionSpace.host]) {
                return [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            }
        }
    }

    return nil;
}

+ (NSURLSessionAuthChallengeDisposition)validateServerTrust:(NSURLAuthenticationChallenge *)challenge withCredentials:(NSURLCredential *__autoreleasing *)credential
{
    NSInteger previousFailureCount = [challenge previousFailureCount];
    if (previousFailureCount >= 3) {
        // We've failed auth 3 times.  Return with code NSURLErrorCancelled.
        return NSURLSessionAuthChallengeCancelAuthenticationChallenge;
    }

    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        // Verify certificate
        SecTrustResultType trustResult;
        OSStatus status = SecTrustEvaluate(challenge.protectionSpace.serverTrust, &trustResult);
        BOOL trusted = (status == errSecSuccess) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
        
        if (trusted) {
            // The certificate was trusted
            *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            return NSURLSessionAuthChallengeUseCredential;
        } else {
            // The certificate was not trusted, check to see if we trust the host
            if ([Service isHostTrusted:challenge.protectionSpace.host]) {
                *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
                return NSURLSessionAuthChallengeUseCredential;
            } else {
                // Host was not trusted, fail authentication
                //return NSURLSessionAuthChallengeCancelAuthenticationChallenge;
                return NSURLSessionAuthChallengePerformDefaultHandling;
            }
        }
    } else {
        // Was not a certificate challenge so perform the default handling
        return NSURLSessionAuthChallengePerformDefaultHandling;
    }
}

+ (void)validateServerTrust:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))handler
{
    NSInteger previousFailureCount = [challenge previousFailureCount];
    if (previousFailureCount >= 3) {
        // We've failed auth 3 times.  The completion handler will be called with code NSURLErrorCancelled.
        handler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    }

    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        // Verify certificate
        SecTrustResultType trustResult;
        OSStatus status = SecTrustEvaluate(challenge.protectionSpace.serverTrust, &trustResult);
        BOOL trusted = (status == errSecSuccess) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));

        if (trusted) {
            // The certificate was trusted
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            handler(NSURLSessionAuthChallengeUseCredential, credential);
        } else {
            // The certificate was not trusted, check to see if we trust the host
            if ([Service isHostTrusted:challenge.protectionSpace.host]) {
                NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
                handler(NSURLSessionAuthChallengeUseCredential, credential);
            } else {
                // Host was not trusted, fail authentication
                handler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
            }
        }
    } else {
        // Was not a certificate challenge so perform the default handling
        handler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}

- (void)prepareForDeletion
{
    // Attempt to delete any stored OAuth information if necessary.  This has to be done here and not in willSave
    // because the relationship will be nullified before willSave runs.
    if (self.defaultUser) {
        [self.defaultUser deleteOAuth2AuthenticationForService:self.serviceURL];
    }
}

- (void)willSave
{
    [super willSave];
    
    // Requery network services when a service is deleted because trust may have changed
    if (self.deleted) {
        [NetworkBrowser requeryPONNetworkServices];
    }
}

@end
