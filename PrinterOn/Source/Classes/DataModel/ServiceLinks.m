//
//  ServiceLinks.m
//  
//
//  Created by Mark Burns on 2016-12-07.
//
//

#import "ServiceLinks.h"

#import "ServiceAccess.h"
#import "ServiceCapabilities.h"

@implementation ServiceLinks

@dynamic rel;
@dynamic type;
@dynamic href;
@dynamic access;
@dynamic capabilities;

@end
