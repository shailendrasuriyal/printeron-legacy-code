//
//  EmailAccount.h
//  PrinterOn
//
//  Created by Mark Burns on 11/26/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

typedef NS_ENUM(NSUInteger, EmailAccountProtocol) {
    EmailAccountProtocolIMAP = 0,
};

typedef NS_ENUM(NSUInteger, EmailAccountType) {
    EmailAccountTypeICloud = 0,
    EmailAccountTypeGMail = 1,
    EmailAccountTypeOutlook = 2,
    EmailAccountTypeYahoo = 3,
    EmailAccountTypeOther = 4,
};

@class EmailFolder;

@interface EmailAccount : NSManagedObject

@property (nonatomic, retain) NSString * emailAddress;
@property (nonatomic, retain) NSString * hostName;
@property (nonatomic, retain) NSNumber * port;
@property (nonatomic, retain) NSNumber * useSSL;
@property (nonatomic, retain) NSNumber * mailType;
@property (nonatomic, retain) NSNumber * protocol;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSSet *emailFolder;

+ (void)createEmailAccount:(NSString *)address
                  password:(NSString *)password
                      auth:(id)auth
                      host:(NSString *)host
                      port:(NSUInteger)port
                       ssl:(BOOL)ssl
                  protocol:(EmailAccountProtocol)protocol
                      type:(EmailAccountType)type;

- (NSString *)getEmailAccountPassword;
- (EmailFolder *)getFolderWithName:(NSString *)name;
- (id)getOAuth2Authentication;
- (void)saveOAuth2Authentication:(id)auth;
- (void)deleteOAuth2Authentication;
- (BOOL)isUsingOAuth2;

- (void)clearAttachmentsCacheForAccount;

+ (NSString *)getHostNameForType:(EmailAccountType)type;

@end

@interface EmailAccount (CoreDataGeneratedAccessors)

- (void)addEmailFolderObject:(EmailFolder *)value;
- (void)removeEmailFolderObject:(EmailFolder *)value;
- (void)addEmailFolder:(NSSet *)values;
- (void)removeEmailFolder:(NSSet *)values;

@end
