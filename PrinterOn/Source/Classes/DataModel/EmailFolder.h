//
//  EmailFolder.h
//  
//
//  Created by Mark Burns on 2015-10-21.
//
//

@class EmailAccount;

NS_ASSUME_NONNULL_BEGIN

@interface EmailFolder : NSManagedObject

@property (nullable, nonatomic, retain) NSString *folderName;
@property (nullable, nonatomic, retain) NSNumber *order;
@property (nullable, nonatomic, retain) EmailAccount *emailAccount;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *emailMessage;

- (void)clearAttachmentsCacheForFolder;

@end

@interface EmailFolder (CoreDataGeneratedAccessors)

- (void)addEmailMessageObject:(NSManagedObject *)value;
- (void)removeEmailMessageObject:(NSManagedObject *)value;
- (void)addEmailMessage:(NSSet<NSManagedObject *> *)values;
- (void)removeEmailMessage:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
