//
//  ReleaseCodeInput.m
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "ReleaseCodeInput.h"

@implementation ReleaseCodeInput

@dynamic use;
@dynamic label;
@dynamic type;
@dynamic minLength;
@dynamic maxLength;
@dynamic autoLength;
@dynamic autoCreate;
@dynamic userEdit;
@dynamic pattern;
@dynamic jobAccountingInputs;


#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"use": @"use",
                       @"label": @"label",
                       @"type": @"type",
                       @"minLength": @"minLength",
                       @"maxLength": @"maxLength",
                       @"autoLength": @"autoLength",
                       @"autoCreate": @"autoCreate",
                       @"userEdit": @"userEdit",
                       @"pattern": @"pattern",
                       };
    });
    return attributes;
}

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store
{
    RKEntityMapping *aliasMapping = [RKEntityMapping mappingForEntityForName:@"ReleaseCodeInput" inManagedObjectStore:store];
    [aliasMapping addAttributeMappingsFromDictionary:[ReleaseCodeInput attributeMappings]];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"jobAccounting.jobAccountingInputs.releaseCode" toKeyPath:@"jobAccountingInputs.releaseCode" withMapping:aliasMapping];
}

@end
