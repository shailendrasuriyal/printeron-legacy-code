//
//  CoreDataManager.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-16.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface CoreDataManager : NSObject

+ (NSManagedObjectContext *)getManagedContext;
+ (BOOL)setupCoreData;

@end
