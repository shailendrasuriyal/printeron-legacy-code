//
//  PrintJob.h
//  PrinterOn
//
//  Created by Mark Burns on 1/8/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

typedef NS_ENUM(NSUInteger, JobStatusCode) {
    JobStatusCodeCreated = 0,
    JobStatusCodeUpload = 1,
    JobStatusCodePending = 2,
    JobStatusCodeSuccess = 3,
    JobStatusCodeFailed = 4,
    JobStatusCodeCancelled = 5,
    JobStatusCodeHeld = 6,
    JobStatusCodeUploadExtension = 7,
};

@class Printer;

@interface PrintJob : NSManagedObject

@property (nonatomic, retain) NSDate * dateTime;
@property (nonatomic, retain) NSString * documentURI;
@property (nonatomic, retain) NSString * errorCode;
@property (nonatomic, retain) NSString * jobAuthURI;
@property (nonatomic, retain) NSNumber * jobState;
@property (nonatomic, retain) NSNumber * jobStatus;
@property (nonatomic, retain) NSString * jobUUID;
@property (nonatomic, retain) NSString * referenceID;
@property (nonatomic, retain) NSString * releaseCode;
@property (nonatomic, retain) NSString * errorText;
@property (nonatomic, retain) NSString * jobKey;
@property (nonatomic, retain) NSNumber * jobPageCount;
@property (nonatomic, retain) NSNumber * docPageCount;
@property (nonatomic, retain) NSNumber * docType;
@property (nonatomic, retain) NSDictionary * docData;
@property (nonatomic, retain) NSDictionary * printOptions;
@property (nonatomic, retain) NSString * clientInput;
@property (nonatomic, retain) NSString * emailInput;
@property (nonatomic, retain) NSString * networkInput;
@property (nonatomic, retain) NSString * sessionInput;
@property (nonatomic, retain) NSString * releaseInput;
@property (nonatomic, retain) Printer *printer;

+ (NSFetchRequest<PrintJob *> *)fetchRequest;

- (NSInteger)getJobStatusValue;
- (NSString *)getJobStatusLabel;
- (void)setJobStatusFromJobState;
- (NSInteger)getJobPageCountValue;
- (NSInteger)getDocumentTypeValue;
- (UIImage *)getJobStatusImage;
- (UIImage *)getJobTypeImage;
- (NSString *)getJobTypeLabel;
- (NSString *)getJobErrorText;
- (NSString *)getDocumentDataLabel:(NSInteger)index;
- (NSString *)getDocumentDataValue:(NSInteger)index;
- (NSString *)storeJobInput:(NSString *)input;
- (NSString *)retrieveJobInput:(NSString *)input;
- (NSString *)getDocumentPath;

+ (PrintJob *)fetchPrintJobByUUID:(NSString *)uuid  inContext:(NSManagedObjectContext *)context;

@end
