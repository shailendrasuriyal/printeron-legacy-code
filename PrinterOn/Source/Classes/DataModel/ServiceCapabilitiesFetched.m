//
//  ServiceCapabilitiesFetched.m
//  PrinterOn
//
//  Created by Mark Burns on 2017-01-30.
//  Copyright (c) 2017 PrinterOn Inc. All rights reserved.
//

#import "ServiceCapabilitiesFetched.h"

@implementation ServiceCapabilitiesFetched

@dynamic lastFetched;
@dynamic serviceURL;

+ (NSFetchRequest<ServiceCapabilitiesFetched *> *)fetchRequest {
    return [[NSFetchRequest alloc] initWithEntityName:@"ServiceCapabilitiesFetched"];
}

+ (nullable NSDate *)getLastFetchForService:(NSString *)serviceURL inContext:(nullable NSManagedObjectContext *)context
{
    if (context == nil) context = [CoreDataManager getManagedContext];

    // Fetch any existing capabilities for this service
    NSFetchRequest *fetchRequest = [self fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"serviceURL ==[c] '%@'", serviceURL]];
    fetchRequest.predicate = predicate;
    
    ServiceCapabilitiesFetched *fetched = nil;
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if (results.count > 0) {
        fetched = results.firstObject;
        return fetched.lastFetched;
    }

    return nil;
}

+ (void)updateLastFetchedForServiceNow:(NSString *)serviceURL inContext:(nullable NSManagedObjectContext *)context
{
    if (context == nil) context = [CoreDataManager getManagedContext];

    // Fetch any existing capabilities for this service
    NSFetchRequest *fetchRequest = [self fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"serviceURL ==[c] '%@'", serviceURL]];
    fetchRequest.predicate = predicate;

    ServiceCapabilitiesFetched *fetched = nil;
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if (results.count > 0) {
        fetched = results.firstObject;
    } else {
        fetched = [NSEntityDescription insertNewObjectForEntityForName:@"ServiceCapabilitiesFetched" inManagedObjectContext:context];
        fetched.serviceURL = serviceURL;
    }
    fetched.lastFetched = [NSDate date];

    // Save to persistence
    NSError *error;
    [context saveToPersistentStore:&error];
    if (error) NSLog(@"Error saving service capabilities fetched date: %@", error);
}

@end
