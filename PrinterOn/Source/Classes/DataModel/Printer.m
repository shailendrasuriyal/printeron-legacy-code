//
//  Printer.m
//  PrinterOn
//
//  Created by Mark Burns on 13-09-17.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "Printer.h"

#import "ClientUIDInput.h"
#import "EmailAddressInput.h"
#import "EmailAlias.h"
#import "JobAccountingInputs.h"
#import "JobProcessingService.h"
#import "Media.h"
#import "NetworkLoginInput.h"
#import "NSManagedObject+Clone.h"
#import "PrintJob.h"
#import "ReleaseCodeInput.h"
#import "SessionMetadataInput.h"

#import <AddressBook/AddressBook.h>
#import <MMWormhole/MMWormhole.h>

@implementation Printer

@dynamic accessClass;
@dynamic addressCity;
@dynamic addressCountry;
@dynamic addressGeoDistanceFrom;
@dynamic addressGeoLocation;
@dynamic addressGeoUnits;
@dynamic addressLine1;
@dynamic addressLine2;
@dynamic addressPostal;
@dynamic addressState;
@dynamic aliasAddress;
@dynamic aliasName;
@dynamic attended;
@dynamic color;
@dynamic colorCapable;
@dynamic colorPermit;
@dynamic coverPage;
@dynamic crypto;
@dynamic displayName;
@dynamic duplex;
@dynamic duplexPermit;
@dynamic hoursOfOperation;
@dynamic locationDescription;
@dynamic locatorCode;
@dynamic manufacturer;
@dynamic model;
@dynamic network;
@dynamic online;
@dynamic organizationBrand;
@dynamic organizationDept;
@dynamic organizationDisplayName;
@dynamic organizationLocationDesc;
@dynamic organizationLocationType;
@dynamic organizationName;
@dynamic pageLimit;
@dynamic parentIDs;
@dynamic payForPrint;
@dynamic payForPrintDesc;
@dynamic printAuthentication;
@dynamic printerID;
@dynamic searchOrder;
@dynamic searchPageNum;
@dynamic serviceID;
@dynamic searchURL;
@dynamic siteClass;
@dynamic type;
@dynamic aliasNumeric;
@dynamic printerClass;
@dynamic jobReleaseMode;
@dynamic jobReleaseLabel;
@dynamic jobReleaseUse;
@dynamic siteNum;
@dynamic children;
@dynamic emailAlias;
@dynamic jobAccountingInputs;
@dynamic jobProcessingService;
@dynamic mediaSupported;
@dynamic parents;
@dynamic printJob;


#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"printerID": @"printerID",
                       @"accessClass.text": @"accessClass",
                       @"address.city.text": @"addressCity",
                       @"address.country.text": @"addressCountry",
                       @"address.geoCalcDistanceFrom.text": @"addressGeoDistanceFrom",
                       @"address.geoLocation.text": @"addressGeoLocation",
                       @"address.geoCalcDistanceFrom.units": @"addressGeoUnits",
                       @"address.addrLine1.text": @"addressLine1",
                       @"address.addrLine2.text": @"addressLine2",
                       @"address.postal.text": @"addressPostal",
                       @"address.state.text": @"addressState",
                       @"alias.addressAlias.text": @"aliasAddress",
                       @"alias.nameAlias.text": @"aliasName",
                       @"alias.numericAlias.text": @"aliasNumeric",
                       @"attended.text": @"attended",
                       @"color.text": @"color",
                       @"colorCapable.text": @"colorCapable",
                       @"colorPermit.text": @"colorPermit",
                       @"coverPage.text": @"coverPage",
                       @"crypto": @"crypto",
                       @"duplex.text": @"duplex",
                       @"duplexPermit.text": @"duplexPermit",
                       @"hoursOfOperation.hours.text": @"hoursOfOperation",
                       @"jobRelease.label": @"jobReleaseLabel",
                       @"jobRelease.releaseMode": @"jobReleaseMode",
                       @"jobRelease.use": @"jobReleaseUse",
                       @"abstract.printerAbstract.location.text": @"locationDescription",
                       @"locatorCode": @"locatorCode",
                       @"manufacturer.text": @"manufacturer",
                       @"model.text": @"model",
                       @"network.text": @"network",
                       @"online": @"online",
                       @"organization.orgBrand.text": @"organizationBrand",
                       @"organization.orgDept.text": @"organizationDept",
                       @"organization.orgDisplayName.text": @"organizationDisplayName",
                       @"organization.serviceID.text": @"serviceID",
                       @"organization.locationDesc.text": @"organizationLocationDesc",
                       @"organization.locationType.text": @"organizationLocationType",
                       @"organization.orgName.text": @"organizationName",
                       @"pageLimit.text": @"pageLimit",
                       @"payForPrint.text": @"payForPrint",
                       @"payForPrintDesc.text": @"payForPrintDesc",
                       @"authData.prn_auth_supported.text": @"printAuthentication",
                       @"printerClass.text": @"printerClass",
                       @"submitJobPrinter.text": @"parentIDs",
                       @"@metadata.mapping.collectionIndex": @"searchOrder",
                       @"siteClass.text": @"siteClass",
                       @"siteNum.text": @"siteNum",
                       @"type.text": @"type",
                       @"@metadata.HTTP.request.URL": @"searchURL",
                       };
    });
    return attributes;
}

+ (NSArray *)identificationAttributes
{
    static NSArray *identification = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        identification = @[@"printerID"];
    });
    return identification;
}

+ (RKEntityMapping *)getEntityMappingForName:(NSString *)entity
                                     inStore:(RKManagedObjectStore *)store
{
    RKEntityMapping *printerMapping = [RKEntityMapping mappingForEntityForName:entity inManagedObjectStore:store];
    [printerMapping addAttributeMappingsFromDictionary:[Printer attributeMappings]];
    printerMapping.identificationAttributes = [Printer identificationAttributes];

    [printerMapping addPropertyMapping:[EmailAlias getRelationshipMapping:store]];
    [printerMapping addPropertyMapping:[JobProcessingService getRelationshipMapping:store]];
    [printerMapping addPropertyMapping:[Media getRelationshipMapping:store]];

    [printerMapping addPropertyMapping:[JobAccountingInputs getRelationshipMapping:store]];
    [printerMapping addPropertyMapping:[ClientUIDInput getRelationshipMapping:store]];
    [printerMapping addPropertyMapping:[EmailAddressInput getRelationshipMapping:store]];
    [printerMapping addPropertyMapping:[NetworkLoginInput getRelationshipMapping:store]];
    [printerMapping addPropertyMapping:[ReleaseCodeInput getRelationshipMapping:store]];
    [printerMapping addPropertyMapping:[SessionMetadataInput getRelationshipMapping:store]];

    return printerMapping;
}

#pragma mark - Printer Utility Methods

- (NSString *)getDisplayNameLabel
{
    __block NSString *displayName;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.organizationDisplayName.length == 0) {
            NSString *newName = [NSString stringWithFormat:@"%@ %@", self.organizationBrand ?: @"", self.organizationName ?: @""];
            displayName = [newName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        } else {
            displayName = self.organizationDisplayName;
        }
    }];

    return displayName;
}

- (NSString *)getStatusLabel
{
    __block NSString *statusLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.online.intValue == 1) {
            statusLabel = NSLocalizedPONString(@"LABEL_ONLINE", nil);
        } else if (self.online.intValue == 0) {
            statusLabel = NSLocalizedPONString(@"LABEL_OFFLINE", nil);
        } else {
            statusLabel = NSLocalizedPONString(@"", nil);
        }
    }];

    return statusLabel;
}

- (NSString *)getManufacturerLabel
{
    __block NSString *manufacturerLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.manufacturer.length > 0) {
            manufacturerLabel = self.manufacturer;
        } else {
            manufacturerLabel = NSLocalizedPONString(@"LABEL_UNIVERSAL", nil);
        }
    }];

    return manufacturerLabel;
}

- (NSString *)getModelLabel
{
    __block NSString *modelLabel = self.model;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.manufacturer.length > 0) {
            NSRange first = [self.model rangeOfString:self.manufacturer];
            if (first.location != NSNotFound) {
                modelLabel = [[self.model stringByReplacingOccurrencesOfString:self.manufacturer withString:@"" options:0 range:first] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            }
        }
    }];

    return modelLabel;
}

- (CLLocationCoordinate2D)getCoordinates
{
    __block CLLocationCoordinate2D location = CLLocationCoordinate2DMake(0.0, 0.0);
    [self.managedObjectContext performBlockAndWait:^() {
        NSRange range = [self.addressGeoLocation rangeOfString:@","];
        if (range.location != NSNotFound) {
            location = CLLocationCoordinate2DMake([self.addressGeoLocation substringToIndex:range.location].doubleValue, [self.addressGeoLocation substringFromIndex:range.location + 1].doubleValue);
        }
    }];

    return location;
}

- (NSString *)getCountryLabel
{
    __block NSString *countryLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        NSString *identifier = [NSLocale localeIdentifierFromComponents:@{NSLocaleCountryCode: self.addressCountry ?: @""}];
        countryLabel = [[NSLocale currentLocale] displayNameForKey:NSLocaleIdentifier value:identifier];
    }];

    return countryLabel;
}

- (NSString *)getAddressLineLabel
{
    __block NSString *addressLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        NSString *country = [self getCountryLabel];
        
        if ([country.lowercaseString isEqualToString:self.addressState.lowercaseString]) {
            addressLabel = [NSString stringWithFormat:@"%@, %@", self.addressCity, self.addressPostal];
        } else {
            addressLabel = [NSString stringWithFormat:@"%@, %@, %@", self.addressCity, self.addressState, self.addressPostal];
        }
    }];

    return addressLabel;
}

- (NSDictionary *)getAddressDictionary
{
    __block NSMutableDictionary *address = [NSMutableDictionary new];
    [self.managedObjectContext performBlockAndWait:^() {
        address[(NSString *)kABPersonAddressStreetKey] = self.addressLine1;
        address[(NSString *)kABPersonAddressCityKey] = self.addressCity;

        NSString *country = [self getCountryLabel];
        if ([country caseInsensitiveCompare:self.addressState] != NSOrderedSame) {
            address[(NSString *)kABPersonAddressStateKey] = self.addressState;
        }

        address[(NSString *)kABPersonAddressCountryKey] = country;
    }];

    return address;
}

- (NSString *)getLocationDescriptionLabel
{
    __block NSString *locationLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.locationDescription.length > 0 && [self.locationDescription caseInsensitiveCompare:@"Not Entered"] == NSOrderedSame) {
            return;
        }
        locationLabel = self.locationDescription;
    }];

    return locationLabel;
}

- (NSString *)getHoursLabel
{
    __block NSString *hoursLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.hoursOfOperation.length > 0 && [self.hoursOfOperation caseInsensitiveCompare:@"Not Entered"] == NSOrderedSame) {
            return;
        }
        hoursLabel = self.hoursOfOperation;
    }];

    return hoursLabel;
}

- (NSString *)getEmailAddressOfType:(NSString *)type
{
    __block NSString *emailAddress;
    [self.managedObjectContext performBlockAndWait:^() {
        NSSet *emails = self.emailAlias;

        for (EmailAlias *email in emails) {
            if (email.enabled.boolValue) {
                if ([email.type.lowercaseString isEqualToString:type.lowercaseString]) {
                    emailAddress = email.address;
                    return;
                }
            }
        }
    }];

    return emailAddress;
}

- (NSString *)getWebPortalAddressStripPrefix:(BOOL)prefix
{
    __block NSString *address;
    [self.managedObjectContext performBlockAndWait:^() {
        NSSet *services = self.jobProcessingService;
        
        for (JobProcessingService *service in services) {
            if (service.enabled.boolValue && [service.serviceType.lowercaseString isEqualToString:@"webprinturl"]) {
                if (prefix) {
                    NSRange http = [service.serviceURI rangeOfString:@"://"];
                    if (http.location != NSNotFound) {
                        address = [service.serviceURI substringFromIndex:http.location+3];
                        return;
                    }
                }
                
                address = service.serviceURI;
                return;
            }
        }
    }];

    return address;
}

- (NSURL *)getDocAPIAddress
{
    __block NSString *address;
    [self.managedObjectContext performBlockAndWait:^() {
        NSSet *services = self.jobProcessingService;
        
        for (JobProcessingService *service in services) {
            if (service.enabled.boolValue && [service.serviceType.lowercaseString isEqualToString:@"documentapi"]) {
                address = service.serviceURI;
                
                NSRange http = [address rangeOfString:@"://"];
                if (http.location == NSNotFound) {
                    address = [NSString stringWithFormat:@"http://%@", address];
                }
                
                return;
            }
        }
    }];

    return address ? [NSURL URLWithString:address] : nil;
}

- (NSString *)getRemoteReleaseCMDAddress
{
    __block NSString *address;
    [self.managedObjectContext performBlockAndWait:^() {
        NSSet *services = self.jobProcessingService;
        
        for (JobProcessingService *service in services) {
            if (service.enabled.boolValue && [service.serviceType.lowercaseString isEqualToString:@"remotecmdreleaseurl"]) {
                address = service.serviceURI;
                return;
            }
        }
    }];

    return address;
}

- (NSString *)getRemoteReleaseAPIAddress
{
    __block NSString *address;
    [self.managedObjectContext performBlockAndWait:^() {
        NSSet *services = self.jobProcessingService;

        for (JobProcessingService *service in services) {
            if (service.enabled.boolValue && [service.serviceType.lowercaseString isEqualToString:@"remotereleaseapi"]) {
                address = service.serviceURI;
                return;
            }
        }
    }];
    
    return address;
}

- (NSString *)getWebReleaseUIAddress
{
    __block NSString *address;
    [self.managedObjectContext performBlockAndWait:^() {
        NSSet *services = self.jobProcessingService;

        for (JobProcessingService *service in services) {
            if (service.enabled.boolValue && [service.serviceType.lowercaseString isEqualToString:@"webreleaseui"]) {
                address = service.serviceURI ?: service.serviceUI;
                return;
            }
        }
    }];

    return address;
}

- (BOOL)doesSupportColor
{
    __block BOOL color = NO;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.color == nil) {
            color = NO;
        } else if (self.color.intValue == 0) {
            color = NO;
        } else if (self.color.intValue == 1) {
            if ([self.colorPermit.lowercaseString isEqualToString:@"mono_only"]) {
                color = NO;
            } else {
                color = YES;
            }
        }
    }];

    return color;
}

- (NSString *)getInkTypeLabel
{
    __block NSString *inkLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.color == nil) {
            inkLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        } else if (self.color.intValue == 0) {
            inkLabel = NSLocalizedPONString(@"LABEL_BW", nil);
        } else if (self.color.intValue == 1) {
            if ([self.colorPermit.lowercaseString isEqualToString:@"mono_only"]) {
                inkLabel = NSLocalizedPONString(@"LABEL_BW", nil);
            } else {
                inkLabel = NSLocalizedPONString(@"LABEL_COLOR", nil);
            }
        } else {
            inkLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        }
    }];

    return inkLabel;
}

- (BOOL)doesSupportDuplex
{
    __block BOOL duplex = NO;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.duplex == nil) {
            duplex = NO;
        } else if (self.duplex.intValue == 0) {
            duplex = NO;
        } else if (self.duplex.intValue == 1) {
            if ([self.duplexPermit.lowercaseString isEqualToString:@"simplex_only"]) {
                duplex = NO;
            } else {
                duplex = YES;
            }
        }
    }];

    return duplex;
}

- (NSString *)getDuplexModeLabel
{
    __block NSString *duplexLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.duplex == nil) {
            duplexLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        } else if (self.duplex.intValue == 1) {
            if ([self.duplexPermit.lowercaseString isEqualToString:@"simplex_only"]) {
                duplexLabel = NSLocalizedPONString(@"LABEL_NOTAVAILABLE", nil);
            } else {
                duplexLabel = NSLocalizedPONString(@"LABEL_AVAILABLE", nil);
            }
        } else if (self.duplex.intValue == 0) {
            duplexLabel = NSLocalizedPONString(@"LABEL_NOTAVAILABLE", nil);
        } else {
            duplexLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        }
    }];

    return duplexLabel;
}

- (NSString *)getCoverPageLabel
{
    __block NSString *coverLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.coverPage == nil) {
            coverLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        } else if (self.coverPage.intValue == 0) {
            coverLabel = NSLocalizedPONString(@"LABEL_NO", nil);
        } else if (self.coverPage.intValue == 1) {
            coverLabel = NSLocalizedPONString(@"LABEL_YES", nil);
        } else {
            coverLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        }
    }];

    return coverLabel;
}

- (NSString *)getJobFeesLabel
{
    __block NSString *feeLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.payForPrint == nil) {
            feeLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        } else if (self.payForPrint.intValue == 0) {
            feeLabel = NSLocalizedPONString(@"LABEL_NO", nil);
        } else if (self.payForPrint.intValue == 1) {
            feeLabel = NSLocalizedPONString(@"LABEL_YES", nil);
        } else {
            feeLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        }
    }];

    return feeLabel;
}

- (NSString *)getPageLimitLabel
{
    __block NSString *limitLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.pageLimit == nil) {
            limitLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        } else {
            limitLabel = self.pageLimit;
        }
    }];

    return limitLabel;
}

- (NSString *)getPaperSizesLabel
{
    __block NSString *sizeLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        NSSet *sizes = self.mediaSupported;
        if (sizes == nil || sizes.count == 0) {
            sizeLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
            return;
        }

        sizeLabel = @"";
        int count = 0;
        for (Media *size in sizes) {
            if (count != 0) {
                sizeLabel = [sizeLabel stringByAppendingString:@", "];
            }
            sizeLabel = [sizeLabel stringByAppendingString:size.mediaSizeName];
            count++;
        }
    }];

    return sizeLabel;
}

- (int)getDefaultPaperSizeNum
{
    __block int num = -1;
    [self.managedObjectContext performBlockAndWait:^() {
        NSSet *sizes = self.mediaSupported;
        if (sizes == nil || sizes.count == 0) return;

        for (Media *size in self.mediaSupported) {
            if (size.isDefault.boolValue) {
                num = size.mediaSizeNum.intValue;
                return;
            }
        }

        // There was no default found, but we know the set contains items, so we pick one
        Media *size = sizes.anyObject;
        num = size.mediaSizeNum.intValue;
    }];

    return num;
}

- (NSString *)getReleaseCodeModeLabel
{
    __block NSString *codeLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        NSString *releaseUse = self.jobReleaseUse.lowercaseString;
        if ([releaseUse isEqualToString:@"required"]) {
            codeLabel = NSLocalizedPONString(@"LABEL_REQUIRED", nil);
        } else if ([releaseUse isEqualToString:@"optional"]) {
            codeLabel = NSLocalizedPONString(@"LABEL_OPTIONAL", nil);
        } else if ([releaseUse isEqualToString:@"disabled"]) {
            codeLabel = NSLocalizedPONString(@"LABEL_DISABLED", nil);
        } else {
            codeLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        }
    }];

    return codeLabel;
}

- (NSString *)getPrintUserCredentialsLabel
{
    __block NSString *credentialsLabel;
    [self.managedObjectContext performBlockAndWait:^() {
        NSString *printAuth = self.printAuthentication.lowercaseString;
        if ([printAuth isEqualToString:@"pon-basic"]) {
            credentialsLabel = NSLocalizedPONString(@"LABEL_REQUIRED", nil);
        } else if ([printAuth isEqualToString:@"none"]) {
            credentialsLabel = NSLocalizedPONString(@"LABEL_DISABLED", nil);
        } else {
            credentialsLabel = NSLocalizedPONString(@"LABEL_NOTSET", nil);
        }
    }];

    return credentialsLabel;
}

- (BOOL)requiresAuthToPrint
{
    __block BOOL auth = NO;
    [self.managedObjectContext performBlockAndWait:^() {
        if ([self.printAuthentication.lowercaseString isEqualToString:@"pon-basic"]) {
            auth = YES;
        }
    }];

    return auth;
}

- (Printer *)clonePrinterInContext:(NSManagedObjectContext *)context
                     forEntityName:(NSString *)entityName
                    updateExisting:(BOOL)update
{
    return [self clonePrinterInContext:context forEntityName:entityName updateExisting:update checkDuplicates:YES];
}

- (Printer *)clonePrinterInContext:(NSManagedObjectContext *)context
                     forEntityName:(NSString *)entityName
                    updateExisting:(BOOL)update
                   checkDuplicates:(BOOL)dupe
{
    // You shouldn't try to clone a printer to itself in the same context, this will trigger enumerator change exceptions.
    if ([entityName isEqualToString:self.entity.name] && context == self.managedObjectContext) {
        return self;
    }

    // By default cloning a Printer should check for duplicates based on "printerID" and it should not clone printJobs and parent/child relationships
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(printerID == %@)", self.printerID];
    Printer *cloned = (Printer *)[self cloneInContext:context entityForName:entityName duplicateIdentifier:dupe ? predicate : nil excludeEntities:nil excludeRelationships:@[@"printJob", @"parents", @"children"] updateValues:update];

    if (cloned.parentIDs.count == 0) return cloned;

    // Manually clone "parents" and their relationships.  This can't be done in the clone above because it causes too many recursive issues.
    for (Printer *parent in self.parents) {
        Printer *clonedParent = (Printer *)[parent cloneInContext:context entityForName:@"ParentPrinter" duplicateIdentifier:predicate excludeEntities:nil excludeRelationships:@[@"printJob", @"parents", @"children"] updateValues:YES];
        if (clonedParent != nil) [cloned addParentsObject:clonedParent];
    }

    return cloned;
}

- (void)updateWithPrinter:(Printer *)printer
{
    // Update the selected printer if it needs to be updated
    if ([Printer isSingletonPrinter:self forEntity:@"SelectedPrinter"]) {
        [Printer setSingletonPrinter:printer forEntity:@"SelectedPrinter"];
    }

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    __block BOOL shouldSave = NO;

    // Update saved printer if it exists
    Printer *savedPrinter = [Printer getPrinter:self forEntityName:@"SavedPrinter"];
    if (savedPrinter) {
        [context performBlockAndWait:^() {
            [printer clonePrinterInContext:context forEntityName:@"SavedPrinter" updateExisting:YES];
            shouldSave = YES;
        }];
    }

    // Update printer for print job history if one exists
    Printer *historyPrinter = [Printer getPrinter:self forEntityName:@"JobPrinter"];
    if (historyPrinter) {
        [context performBlockAndWait:^() {
            [printer clonePrinterInContext:context forEntityName:@"JobPrinter" updateExisting:YES];
            shouldSave = YES;
        }];
    }

    if (shouldSave) {
        NSError *error;
        [context saveToPersistentStore:&error];
        if (error) NSLog(@"Error updating printers: %@", error);
    }
}

+ (Printer *)getPrinter:(Printer *)printer forEntityName:(NSString *)entityName
{
    if (printer == nil) return nil;
    if (entityName == nil || entityName.length == 0) return nil;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block Printer *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
        fetchRequest.entity = entity;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(printerID == %@)", printer.printerID];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        }
    }];

    return result;
}

+ (void)connectRelationshipsFromMapping:(NSDictionary *)results inContext:(NSManagedObjectContext *)context
{
    // Populate any printers from the result set.  Return immediately if there aren't any printers to process
    id resultPrinters = results[@"DirSearch.printer"];
    NSArray *printers = resultPrinters == nil ? nil : [resultPrinters isKindOfClass:[NSArray class]] ? [NSArray arrayWithArray:resultPrinters] : [NSArray arrayWithObject:resultPrinters];
    if (printers == nil || printers.count == 0) return;

    // Populate any parents from the result set
    id resultParents = results[@"DirSearch.parent"];
    NSMutableArray *parents = resultParents == nil ? [NSMutableArray array] : [resultParents isKindOfClass:[NSArray class]] ? [NSMutableArray arrayWithArray:resultParents] : [NSMutableArray arrayWithObject:resultParents];

    // Loop through each printer and create parent/child relationships
    for (Printer *printer in printers) {
        // Return if the printer has no parents or they already exist
        if (printer.parentIDs.count == 0 || printer.parents.count > 0) continue;

        for (NSString *parentID in printer.parentIDs) {
            Printer *parent = nil;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(printerID == %@)", parentID];

            NSArray *filteredParents = [parents filteredArrayUsingPredicate:predicate];
            if (filteredParents.count == 0) {
                // We couldn't find the parent in the parents results so it should exist as a printer in the printer results
                NSArray *filteredPrinters = [printers filteredArrayUsingPredicate:predicate];
                if (filteredPrinters.count > 0) {
                    // Clone the printer found as a parent and add it to the parents set
                    Printer *found = (Printer *)filteredPrinters.firstObject;
                    parent = [found clonePrinterInContext:context forEntityName:@"ParentPrinter" updateExisting:YES];
                    [parents addObject:parent];
                }
            } else {
                parent = filteredParents.firstObject;
            }

            // Create the parent/child relationship
            if (parent) {
                [printer addParentsObject:parent];
            }
        }
    }
}

+ (Printer *)getSingletonPrinterForEntity:(NSString *)entityName
{
    if (entityName == nil || entityName.length == 0) return nil;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    __block Printer *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        }
    }];

    return result;
}

+ (void)setSingletonPrinter:(Printer *)printer
                  forEntity:(NSString *)entityName
{
    NSManagedObjectContext *context = [CoreDataManager getManagedContext];

    Printer *selectedPrinter = [Printer getSingletonPrinterForEntity:entityName];

    // If both are nil exit right away
    if (printer == nil && selectedPrinter == nil) {
        return;
    }

    [context performBlockAndWait:^() {
        BOOL shouldSave = NO;
        
        // If we have a stored printer delete it
        if (selectedPrinter) {
            [context deleteObject:selectedPrinter];

            NSError *error;
            [context save:&error];
            if (error) NSLog(@"Error setting selected printer: %@", error);
            shouldSave = YES;
        }

        // If a new printer is set clone it into store
        if (printer) {
            [printer clonePrinterInContext:context forEntityName:entityName updateExisting:NO];
            shouldSave = YES;
        }

        // If we made changes save them to the persistent store
        if (shouldSave) {
            NSError *error;
            [context saveToPersistentStore:&error];
            if (error) NSLog(@"Error setting selected printer: %@", error);
        }
    }];

    // If we changed the Selected Printer post a notification
    if ([entityName isEqualToString:@"SelectedPrinter"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PONPrinterChangedNotification" object:nil];
        [[WormholeManager sharedWormholeManager].wormHole passMessageObject:@{@"changed" : @(1)} identifier:@"PONPrinterChangedNotification"];
    }
}

+ (BOOL)isSingletonPrinter:(Printer *)printer
                 forEntity:(NSString *)entityName
{
    Printer *selectedPrinter = [Printer getSingletonPrinterForEntity:entityName];

    if (printer && selectedPrinter && [printer.printerID isEqualToString:selectedPrinter.printerID]) {
        return YES;
    }

    return NO;
}

- (BOOL)validateParentIDs:(id *)ioValue error:(NSError **)outError
{
    if ([*ioValue isKindOfClass:[NSArray class]]) {
        NSArray *array = (NSArray *)*ioValue;
        if (array.count > 0 && [array[0] isKindOfClass:[NSString class]] && [array[0] rangeOfString:@","].location != NSNotFound) {
            *ioValue = [array[0] componentsSeparatedByString:@","];
        }

        NSString *pid = self.printerID;
        if (pid && [*ioValue containsObject:pid]) {
            NSMutableArray *new = [NSMutableArray arrayWithArray:*ioValue];
            [new removeObject:pid];
            *ioValue = new.count > 0 ? new : nil;
        }
    }

    return YES;
}

- (void)didChangeValueForKey:(NSString *)inKey
              withSetMutation:(NSKeyValueSetMutationKind)inMutationKind
                 usingObjects:(NSSet *)inObjects
{
    [super didChangeValueForKey:inKey withSetMutation:inMutationKind usingObjects:inObjects];

    // Check when children are removed from parent printers and remove the parent printer when none remain
    if ([self.entity.name isEqualToString:@"ParentPrinter"] && [inKey isEqualToString:@"children"]) {
        if (!self.deleted && self.children.count == 0) {
            [self.managedObjectContext deleteObject:self];
        }
    }
}

- (void)willSave
{
    [super willSave];

    // Set the computed display name if it has changed
    NSString *displayName = [self getDisplayNameLabel];
    if (![self.displayName isEqualToString:displayName]) {
        self.displayName = displayName;
    }

    // If this is a JobPrinter when all PrintJob are deleted we should also remove the printer
    if ([self.entity.name isEqualToString:@"JobPrinter"]) {
        if (!self.deleted && self.printJob.count == 0) {
            [self.managedObjectContext deleteObject:self];
        }
    }
}

@end
