//
//  ServiceAccess.h
//  
//
//  Created by Mark Burns on 2016-04-13.
//
//

@class ServiceCapabilities, ServiceLinks;

NS_ASSUME_NONNULL_BEGIN

@interface ServiceAccess : NSManagedObject

@property (nullable, nonatomic, retain) NSString *issuer;
@property (nullable, nonatomic, retain) NSArray *scopes;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) ServiceCapabilities *capabilities;
@property (nullable, nonatomic, retain) NSSet<ServiceLinks *> *links;

- (ServiceLinks *)getLinkForType:(NSString *)linkType;
- (void)clearOAuth;

@end

@interface ServiceAccess (CoreDataGeneratedAccessors)

- (void)addLinksObject:(ServiceLinks *)value;
- (void)removeLinksObject:(ServiceLinks *)value;
- (void)addLinks:(NSSet<ServiceLinks *> *)values;
- (void)removeLinks:(NSSet<ServiceLinks *> *)values;

@end

NS_ASSUME_NONNULL_END
