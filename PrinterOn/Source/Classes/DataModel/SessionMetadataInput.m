//
//  SessionMetadataInput.m
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "SessionMetadataInput.h"

@implementation SessionMetadataInput

@dynamic use;
@dynamic label;
@dynamic defaultValue;
@dynamic secure;
@dynamic jobAccountingInputs;


#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"use": @"use",
                       @"label": @"label",
                       @"default": @"defaultValue",
                       @"secure": @"secure",
                       };
    });
    return attributes;
}

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store
{
    RKEntityMapping *aliasMapping = [RKEntityMapping mappingForEntityForName:@"SessionMetadataInput" inManagedObjectStore:store];
    [aliasMapping addAttributeMappingsFromDictionary:[SessionMetadataInput attributeMappings]];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"jobAccounting.jobAccountingInputs.sessionMetaData" toKeyPath:@"jobAccountingInputs.sessionMetadata" withMapping:aliasMapping];
}

@end
