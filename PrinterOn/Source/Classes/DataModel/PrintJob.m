//
//  PrintJob.m
//  PrinterOn
//
//  Created by Mark Burns on 1/8/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PrintJob.h"

#import "NSData+Transform.h"
#import "PrintDocument.h"
#import "Printer.h"

@implementation PrintJob

@dynamic dateTime;
@dynamic documentURI;
@dynamic errorCode;
@dynamic jobAuthURI;
@dynamic jobState;
@dynamic jobStatus;
@dynamic jobUUID;
@dynamic referenceID;
@dynamic releaseCode;
@dynamic errorText;
@dynamic jobKey;
@dynamic jobPageCount;
@dynamic docPageCount;
@dynamic docType;
@dynamic docData;
@dynamic printOptions;
@dynamic clientInput;
@dynamic emailInput;
@dynamic networkInput;
@dynamic sessionInput;
@dynamic releaseInput;
@dynamic printer;

+ (NSFetchRequest<PrintJob *> *)fetchRequest {
    return [[NSFetchRequest alloc] initWithEntityName:@"PrintJob"];
}

- (NSInteger)getJobStatusValue
{
    __block NSInteger status;
    [self.managedObjectContext performBlockAndWait:^() {
        status = self.jobStatus.integerValue;
    }];
    return status;
}

- (NSString *)getJobStatusLabel
{
    NSInteger jobStatus = [self getJobStatusValue];
    if (jobStatus == JobStatusCodeCreated || jobStatus == JobStatusCodeUpload || jobStatus == JobStatusCodeUploadExtension) {
        return NSLocalizedPONString(@"LABEL_UPLOAD", nil);
    } else if (jobStatus == JobStatusCodeCancelled) {
        return NSLocalizedPONString(@"LABEL_CANCELLED", nil);
    } else if (jobStatus == JobStatusCodeFailed) {
        return NSLocalizedPONString(@"LABEL_FAILED", nil);
    } else if (jobStatus == JobStatusCodeSuccess) {
        return NSLocalizedPONString(@"LABEL_SUCCESS", nil);
    } else if (jobStatus == JobStatusCodePending) {
        return NSLocalizedPONString(@"LABEL_PROCESS", nil);
    } else if (jobStatus == JobStatusCodeHeld) {
        return NSLocalizedPONString(@"LABEL_PENDING", nil);
    }
    return nil;
}

- (void)setJobStatusFromJobState
{
    int state = self.jobState.intValue;
    if (state == 7 || state == 8) {
        self.jobStatus = @((int)JobStatusCodeFailed);
    } else if (state == 4 || state == 9) {
        self.jobStatus = @((int)JobStatusCodeSuccess);
    }
}

- (NSInteger)getJobPageCountValue
{
    __block NSInteger count;
    [self.managedObjectContext performBlockAndWait:^() {
        count = self.jobPageCount.integerValue;
    }];
    return count;
}

- (NSInteger)getDocumentTypeValue
{
    __block NSInteger type;
    [self.managedObjectContext performBlockAndWait:^() {
        type = self.docType.integerValue;
    }];
    return type;
}

- (UIImage *)getJobStatusImage
{
    NSInteger jobStatus = [self getJobStatusValue];
    if (jobStatus == JobStatusCodeCancelled || jobStatus == JobStatusCodeFailed) {
        return [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.JobFail.Image"]];
    } else if (jobStatus == JobStatusCodeSuccess) {
        return [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.JobSuccess.Image"]];
    } else if (jobStatus == JobStatusCodeHeld) {
        return [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.JobHeld.Image"]];
    }

    return nil;
}

- (UIImage *)getJobTypeImage
{
    NSInteger docType = [self getDocumentTypeValue];
    if (docType == DocumentTypeWeb) {
        return [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.WebJob.Image"]];
    } else if (docType == DocumentTypeFile) {
        return [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.FileJob.Image"]];
    } else if (docType == DocumentTypeEmail) {
        return [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.EmailJob.Image"]];
    } else if (docType == DocumentTypePhoto) {
        return [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.PhotoJob.Image"]];
    }

    return nil;
}

- (NSString *)getJobTypeLabel
{
    NSInteger docType = [self getDocumentTypeValue];
    if (docType == DocumentTypeWeb) {
        return NSLocalizedPONString(@"LABEL_WEB", nil);
    } else if (docType == DocumentTypeFile) {
        return NSLocalizedPONString(@"LABEL_DOCUMENTS", nil);
    } else if (docType == DocumentTypeEmail) {
        return NSLocalizedPONString(@"LABEL_EMAIL", nil);
    } else if (docType == DocumentTypePhoto) {
        return NSLocalizedPONString(@"LABEL_PHOTOS", nil);
    }

    return nil;
}

- (NSString *)getJobErrorText
{
    __block NSString *text;
    [self.managedObjectContext performBlockAndWait:^() {
        if (self.errorText.length > 0) {
            text = self.errorText;
        } else if (self.errorCode.length > 0) {
            text = self.errorCode;
        }
    }];
    return text;
}

- (NSString *)getDocumentDataLabel:(NSInteger)index
{
    NSInteger docType = [self getDocumentTypeValue];
    if (docType == DocumentTypeWeb) {
        switch (index) {
            case 1:
                return [NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_HOST", nil)];
            case 2:
                return [NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_URL", nil)];
        }
    } else if (docType == DocumentTypeFile) {
        return nil;
    } else if (docType == DocumentTypeEmail) {
        switch (index) {
            case 1:
                return [NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_FROM", nil)];
            case 2:
                return [NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_SUBJECT", nil)];
        }
    } else if (docType == DocumentTypePhoto) {
        switch (index) {
            case 1:
                return [NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_SOURCE", nil)];
            case 2:
                return nil;
        }
    }
    
    return nil;
}

- (NSString *)getDocumentDataValue:(NSInteger)index
{
    NSInteger docType = [self getDocumentTypeValue];
    __block NSDictionary *data;
    [self.managedObjectContext performBlockAndWait:^() {
        data = self.docData;
    }];

    if (docType == DocumentTypeWeb) {
        switch (index) {
            case 1:
                return data[kDocumentTypeWebHostNameKey];
            case 2:
                return data[kDocumentTypeWebURLKey];
        }
    } else if (docType == DocumentTypeFile) {
        return nil;
    } else if (docType == DocumentTypeEmail) {
        switch (index) {
            case 1:
                return data[kDocumentTypeEmailFromKey];
            case 2:
                return data[kDocumentTypeEmailSubjectKey];
        }
    } else if (docType == DocumentTypePhoto) {
        switch (index) {
            case 1:
                return data[kDocumentTypePhotoSourceKey];
            case 2:
                return nil;
        }
    }

    return nil;
}

- (NSString *)storeJobInput:(NSString *)input
{
    if (!input) return nil;
    
    NSData *data = [input dataUsingEncoding:NSUTF8StringEncoding];
    [data transform];
    return [data hexString];
}

- (NSString *)retrieveJobInput:(NSString *)input
{
    if (!input) return nil;

    NSData *bytes = [NSData toBytes:input];
    [bytes transform];
    return [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
}

- (NSString *)getDocumentPath
{
    __block NSString *path;
    [self.managedObjectContext performBlockAndWait:^() {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSString *appGroupId = [[NSBundle mainBundle].infoDictionary valueForKey:@"APP_GROUP_ID"];
        NSURL *groupURL = [fileManager containerURLForSecurityApplicationGroupIdentifier:appGroupId];
        NSString *jobsPath = [[groupURL.relativePath stringByAppendingPathComponent:@"Jobs"] stringByAppendingPathComponent:self.jobUUID];
        BOOL isDir;
        BOOL exist = [fileManager fileExistsAtPath:jobsPath isDirectory:&isDir];
        
        if (exist) {
            path = [NSString stringWithFormat:@"file://%@", [jobsPath stringByAppendingPathComponent:self.documentURI.lastPathComponent]];
        } else {
            NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString *append = [NSString stringWithFormat:@"/Jobs/%@/%@", self.jobUUID, self.documentURI.lastPathComponent];
            path = [NSString stringWithFormat:@"file://%@%@", documentsPath, append];
        }
    }];
    return path;
}

+ (PrintJob *)fetchPrintJobByUUID:(NSString *)uuid inContext:(NSManagedObjectContext *)context
{
    if (context == nil) context = [CoreDataManager getManagedContext];

    __block PrintJob *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [PrintJob fetchRequest];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"jobUUID == '%@'", uuid]];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        }
    }];

    return result;
}

- (void)willSave
{
    [super willSave];

    // Delete the job document when this object is deleted
    if (self.deleted) {
        NSURL *file = [NSURL URLWithString:[self getDocumentPath]];
        NSString *filePath = [file.path stringByDeletingLastPathComponent];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
    }
}

@end
