//
//  Bookmark.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-08-11.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface Bookmark : NSManagedObject

@property (nonatomic, retain) NSNumber * displayOrder;
@property (nonatomic, retain) NSString * bookmarkTitle;
@property (nonatomic, retain) NSString * bookmarkURL;

@end
