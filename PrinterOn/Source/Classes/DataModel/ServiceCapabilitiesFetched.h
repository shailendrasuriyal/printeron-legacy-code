//
//  ServiceCapabilitiesFetched.h
//  PrinterOn
//
//  Created by Mark Burns on 2017-01-30.
//  Copyright (c) 2017 PrinterOn Inc. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface ServiceCapabilitiesFetched : NSManagedObject

@property (nullable, nonatomic, copy) NSDate *lastFetched;
@property (nullable, nonatomic, copy) NSString *serviceURL;

+ (NSFetchRequest<ServiceCapabilitiesFetched *> *)fetchRequest;

+ (nullable NSDate *)getLastFetchForService:(NSString *)serviceURL inContext:(nullable NSManagedObjectContext *)context;
+ (void)updateLastFetchedForServiceNow:(NSString *)serviceURL inContext:(nullable NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END
