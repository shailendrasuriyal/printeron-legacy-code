//
//  CoreDataManager.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-16.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "CoreDataManager.h"

#import <MMWormhole/MMWormhole.h>

@implementation CoreDataManager

+ (NSManagedObjectContext *)getManagedContext
{
    NSManagedObjectContext *context = [self managedContextForThread];

    if (context == nil) {
        if ([self setupCoreData]) {
            context = [self managedContextForThread];
        }
    }

    return context;
}

+ (NSManagedObjectContext *)managedContextForThread
{
    RKManagedObjectStore *managedObjectStore = [RKManagedObjectStore defaultStore];
    NSManagedObjectContext *context = [NSThread isMainThread] ? managedObjectStore.mainQueueManagedObjectContext : managedObjectStore.persistentStoreManagedObjectContext;

    if (context == nil && [NSThread isMainThread]) {
        context = managedObjectStore.persistentStoreManagedObjectContext;
    }

    return context;
}

+ (BOOL)setupCoreData
{
    RKManagedObjectStore *managedObjectStore = [RKManagedObjectStore defaultStore];
    if (managedObjectStore == nil) {
        managedObjectStore = [self initializeManagedObjectStore];
        if ([RKManagedObjectStore defaultStore] == nil) return NO;
    }

    NSPersistentStore *persistentStore = managedObjectStore.persistentStoreCoordinator.persistentStores.firstObject;
    if (persistentStore == nil) {
        persistentStore = [self setupPersistentStore];
        if (managedObjectStore.persistentStoreCoordinator.persistentStores.firstObject == nil) return NO;
    }

    return [self setupContexts];
}

+ (RKManagedObjectStore *)initializeManagedObjectStore
{
    NSString *modelPath = [[NSBundle mainBundle] pathForResource:@"PrinterOn" ofType:@"momd"];
    NSURL *momURL = [NSURL fileURLWithPath:modelPath];
    NSManagedObjectModel *managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    return managedObjectStore;
}

+ (NSPersistentStore *)setupPersistentStore
{
    NSString *storePath;

    // Set the persistent store path
    #if defined(AF_APP_EXTENSIONS)
        NSString *appGroupId = [[NSBundle mainBundle].infoDictionary valueForKey:@"APP_GROUP_ID"];
        NSURL *groupURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:appGroupId];
        storePath = [groupURL.relativePath stringByAppendingPathComponent:@"PrinterOn.sqlite"];
    #else
        if ([[UIDevice currentDevice].systemVersion compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending) {
            NSString *appGroupId = [[NSBundle mainBundle].infoDictionary valueForKey:@"APP_GROUP_ID"];
            NSURL *groupURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:appGroupId];
            storePath = [groupURL.relativePath stringByAppendingPathComponent:@"PrinterOn.sqlite"];

            // Migrate old database to shared app group if it exists
            NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:appGroupId];
            if (![[sharedDefaults stringForKey:@"migratedCoreData"] isEqualToString:@"YES"]) {
                NSArray *paths = @[[RKApplicationDataDirectory() stringByAppendingPathComponent:@"PrinterOn.sqlite"], [RKApplicationDataDirectory() stringByAppendingPathComponent:@"PrinterOn.sqlite-shm"], [RKApplicationDataDirectory() stringByAppendingPathComponent:@"PrinterOn.sqlite-wal"]];

                for (NSString *tempPath in paths) {
                    if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
                        [[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:[groupURL.relativePath stringByAppendingPathComponent:tempPath.lastPathComponent] error:nil];
                    }
                }

                [sharedDefaults setObject:@"YES" forKey:@"migratedCoreData"];
                [sharedDefaults synchronize];
                [[WormholeManager sharedWormholeManager].wormHole passMessageObject:@{@"migrated" : @(1)} identifier:@"PONCoreDataAppGroupMigrated"];
            }
        } else {
            storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"PrinterOn.sqlite"];
        }
    #endif

    // Setup the persistent store
    RKManagedObjectStore *managedObjectStore = [RKManagedObjectStore defaultStore];
    NSError *error;
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];

    // If we had an error with the data schema changing or migrating schemas
    if (error && error.code == 134130 && [error.domain isEqualToString:NSCocoaErrorDomain]) {
        // Delete the current store
        if ([[NSFileManager defaultManager] fileExistsAtPath:storePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
        }

        // Attempt to re-create the store
        persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    }

    return persistentStore;
}

+ (BOOL)setupContexts
{
    RKManagedObjectStore *managedObjectStore = [RKManagedObjectStore defaultStore];

    // Create the managed object contexts
    if (managedObjectStore.mainQueueManagedObjectContext == nil && managedObjectStore.persistentStoreManagedObjectContext == nil) {
        [managedObjectStore createManagedObjectContexts];
    }

    // Disable using the cache so that we fetch the most up to date data from disk everytime so we are in sync with the extension
    managedObjectStore.mainQueueManagedObjectContext.stalenessInterval = 0.0;
    managedObjectStore.persistentStoreManagedObjectContext.stalenessInterval = 0.0;

    return managedObjectStore.mainQueueManagedObjectContext || managedObjectStore.persistentStoreManagedObjectContext;
}

@end
