//
//  EmailAlias.m
//  PrinterOn
//
//  Created by Mark Burns on 11/13/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "EmailAlias.h"

@implementation EmailAlias

@dynamic address;
@dynamic enabled;
@dynamic type;
@dynamic printer;


#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"enabled": @"enabled",
                       @"type": @"type",
                       @"text": @"address",
                       };
    });
    return attributes;
}

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store
{
    RKEntityMapping *aliasMapping = [RKEntityMapping mappingForEntityForName:@"EmailAlias" inManagedObjectStore:store];
    [aliasMapping addAttributeMappingsFromDictionary:[EmailAlias attributeMappings]];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"alias.emailAlias" toKeyPath:@"emailAlias" withMapping:aliasMapping];
}

@end
