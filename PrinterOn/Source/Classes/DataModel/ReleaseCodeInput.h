//
//  ReleaseCodeInput.h
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface ReleaseCodeInput : NSManagedObject

@property (nonatomic, retain) NSString * use;
@property (nonatomic, retain) NSString * label;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * minLength;
@property (nonatomic, retain) NSNumber * maxLength;
@property (nonatomic, retain) NSNumber * autoLength;
@property (nonatomic, retain) NSNumber * autoCreate;
@property (nonatomic, retain) NSNumber * userEdit;
@property (nonatomic, retain) NSString * pattern;
@property (nonatomic, retain) NSManagedObject *jobAccountingInputs;

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store;

@end
