//
//  JobProcessingService.h
//  PrinterOn
//
//  Created by Mark Burns on 11/13/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class Printer;

@interface JobProcessingService : NSManagedObject

@property (nonatomic, retain) NSNumber * enabled;
@property (nonatomic, retain) NSString * serviceName;
@property (nonatomic, retain) NSString * serviceType;
@property (nonatomic, retain) NSString * version;
@property (nonatomic, retain) NSString * serviceUI;
@property (nonatomic, retain) NSString * serviceURI;
@property (nonatomic, retain) Printer *printer;

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store;

@end
