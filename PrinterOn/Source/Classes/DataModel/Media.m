//
//  Media.m
//  PrinterOn
//
//  Created by Mark Burns on 11/13/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "Media.h"

@implementation Media

@dynamic isDefault;
@dynamic mediaSizeNum;
@dynamic mediaSizeName;
@dynamic mediaSizeDim;
@dynamic printer;


#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"default": @"isDefault",
                       @"mediaSizeNum": @"mediaSizeNum",
                       @"mediaSizeName.text": @"mediaSizeName",
                       @"mediaSizeDim.text": @"mediaSizeDim",
                       };
    });
    return attributes;
}

+ (RKRelationshipMapping *)getRelationshipMapping:(RKManagedObjectStore *)store
{
    RKEntityMapping *aliasMapping = [RKEntityMapping mappingForEntityForName:@"Media" inManagedObjectStore:store];
    [aliasMapping addAttributeMappingsFromDictionary:[Media attributeMappings]];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"mediaSupported.media" toKeyPath:@"mediaSupported" withMapping:aliasMapping];
}

@end
