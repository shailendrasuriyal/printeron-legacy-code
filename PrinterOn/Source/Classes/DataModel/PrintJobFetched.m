//
//  PrintJobFetched.m
//  PrinterOn
//
//  Created by Mark Burns on 2017-05-09.
//  Copyright (c) 2017 PrinterOn Inc. All rights reserved.
//

#import "PrintJobFetched.h"

#import "UserAccount.h"

@implementation PrintJobFetched

@dynamic idSet;
@dynamic serviceURL;
@dynamic userAccount;

+ (NSFetchRequest<PrintJobFetched *> *)fetchRequest {
    return [[NSFetchRequest alloc] initWithEntityName:@"PrintJobFetched"];
}

+ (nullable NSMutableIndexSet *)getJobIDsForService:(NSString *)serviceURL forUser:(UserAccount *)userAccount
{
    // Fetch any existing capabilities for this service
    NSFetchRequest *fetchRequest = [self fetchRequest];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"serviceURL ==[c] '%@'", serviceURL]];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"userAccount = %@", userAccount];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    fetchRequest.predicate = predicate;

    PrintJobFetched *fetched = nil;
    NSArray *results = [userAccount.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if (results.count > 0) {
        fetched = results.firstObject;
        return fetched.idSet.mutableCopy;
    }

    return nil;
}

+ (void)updateJobIDsForService:(NSString *)serviceURL forUser:(UserAccount *)userAccount withIDs:(NSMutableIndexSet *)indexSet
{
    // Fetch any existing capabilities for this service
    NSFetchRequest *fetchRequest = [self fetchRequest];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"serviceURL ==[c] '%@'", serviceURL]];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"userAccount = %@", userAccount];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    fetchRequest.predicate = predicate;

    PrintJobFetched *fetched = nil;
    NSArray *results = [userAccount.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if (results.count > 0) {
        fetched = results.firstObject;
    } else {
        fetched = [NSEntityDescription insertNewObjectForEntityForName:@"PrintJobFetched" inManagedObjectContext:userAccount.managedObjectContext];
        fetched.serviceURL = serviceURL;
        fetched.userAccount = userAccount;
    }
    fetched.idSet = indexSet.copy;

    // Save to persistence
    NSError *error;
    [userAccount.managedObjectContext saveToPersistentStore:&error];
    if (error) NSLog(@"Error saving print job fetched date: %@", error);
}

@end
