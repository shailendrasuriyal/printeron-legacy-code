//
//  ServiceLinks.h
//  
//
//  Created by Mark Burns on 2016-12-07.
//
//

@class ServiceAccess, ServiceCapabilities;

NS_ASSUME_NONNULL_BEGIN

@interface ServiceLinks : NSManagedObject

@property (nullable, nonatomic, retain) NSString *rel;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSString *href;
@property (nullable, nonatomic, retain) ServiceAccess *access;
@property (nullable, nonatomic, retain) ServiceCapabilities *capabilities;

@end

NS_ASSUME_NONNULL_END
