//
//  ServiceCapabilities.m
//  
//
//  Created by Mark Burns on 2016-04-13.
//
//

#import "ServiceCapabilities.h"

#import "ServiceAccess.h"
#import "ServiceLinks.h"

@implementation ServiceCapabilities

@dynamic company;
@dynamic deploymentMode;
@dynamic dirSearch;
@dynamic docAPI;
@dynamic documentPreview;
@dynamic lastModifiedDate;
@dynamic maxUploadSize;
@dynamic releaseAnywhere;
@dynamic serviceName;
@dynamic serviceURL;
@dynamic version;
@dynamic access;
@dynamic links;

+ (ServiceCapabilities *)getCapabilitiesForService:(NSString *)serviceURL inContext:(nullable NSManagedObjectContext *)context
{
    if (context == nil) context = [CoreDataManager getManagedContext];

    // Fetch any existing capabilities for this service
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    fetchRequest.entity = [NSEntityDescription entityForName:@"ServiceCapabilities" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"serviceURL ==[c] '%@'", serviceURL]];
    fetchRequest.predicate = predicate;

    ServiceCapabilities *capabilities = nil;
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if (results.count > 0) {
        capabilities = results.firstObject;
    }

    return capabilities;
}

+ (NSString *)getDirectorySearchURLForService:(NSString *)serviceURL
{
    __block NSString *searchURL = nil;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    [context performBlockAndWait:^ {
        ServiceCapabilities *capabilities = [self getCapabilitiesForService:serviceURL inContext:context];
        if (capabilities) {
            ServiceLinks *link = [capabilities getLinkForType:@"http://printeron.net/schemas/cloud/discovery/service/endpoint/directory_search"];
            if (link) {
                searchURL = link.href;
            }
        }
    }];

    if (searchURL == nil) {
        searchURL = [serviceURL stringByAppendingString:@"/DirSearch"];
    }

    return searchURL;
}

+ (NSString *)getOAuthAuthorizeURLForService:(NSString *)serviceURL
{
    __block NSString *authorizeURL = nil;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    [context performBlockAndWait:^ {
        ServiceCapabilities *capabilities = [self getCapabilitiesForService:serviceURL inContext:context];
        if (capabilities) {
            ServiceAccess *access = [capabilities getAccessForType:@"oauth2"];
            if (access) {
                ServiceLinks *link = [access getLinkForType:@"authorization"];
                authorizeURL = link.href;
            }
        }
    }];

    return authorizeURL;
}

+ (NSString *)getOAuthTokenURLForService:(NSString *)serviceURL
{
    __block NSString *tokenURL = nil;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    [context performBlockAndWait:^ {
        ServiceCapabilities *capabilities = [self getCapabilitiesForService:serviceURL inContext:context];
        if (capabilities) {
            ServiceAccess *access = [capabilities getAccessForType:@"oauth2"];
            if (access) {
                ServiceLinks *link = [access getLinkForType:@"token"];
                tokenURL = link.href;
            }
        }
    }];

    return tokenURL;
}

+ (NSString *)getOAuthUserInfoURLForService:(NSString *)serviceURL
{
    __block NSString *userURL = nil;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    [context performBlockAndWait:^ {
        ServiceCapabilities *capabilities = [self getCapabilitiesForService:serviceURL inContext:context];
        if (capabilities) {
            ServiceAccess *access = [capabilities getAccessForType:@"oauth2"];
            if (access) {
                ServiceLinks *link = [access getLinkForType:@"userinfo"];
                userURL = link.href;
            }
        }
    }];

    return userURL;
}

+ (NSString *)getJobsURLForService:(NSString *)serviceURL
{
    __block NSString *jobsURL = nil;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    [context performBlockAndWait:^ {
        ServiceCapabilities *capabilities = [self getCapabilitiesForService:serviceURL inContext:context];
        if (capabilities) {
            ServiceLinks *link = [capabilities getLinkForType:@"http://printeron.net/schemas/cloud/discovery/service/endpoint/jobs"];
            if (link) {
                jobsURL = link.href;
            }
        }
    }];

    return jobsURL;
}

+ (BOOL)isServiceUsingOAuth:(NSString *)serviceURL
{
    __block BOOL isOAuth2 = NO;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    [context performBlockAndWait:^ {
        ServiceCapabilities *capabilities = [self getCapabilitiesForService:serviceURL inContext:context];
        if (capabilities) {
            ServiceAccess *accessOAuth = [capabilities getAccessForType:@"oauth2"];
            if (accessOAuth) {
                isOAuth2 = YES;
            }
        }
    }];

    return isOAuth2;
}

+ (BOOL)serviceSupportsOAuthFirstParty:(NSString *)serviceURL
{
    __block BOOL supportsFirstParty = NO;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    [context performBlockAndWait:^ {
        ServiceCapabilities *capabilities = [self getCapabilitiesForService:serviceURL inContext:context];
        if (capabilities) {
            ServiceAccess *accessOAuth = [capabilities getAccessForType:@"oauth2"];
            if (accessOAuth && [accessOAuth.issuer.lowercaseString isEqualToString:@"cps.printeron.com"]) {
                supportsFirstParty = YES;
            }
        }
    }];

    return supportsFirstParty;
}

+ (BOOL)serviceSupportsOAuthThirdParty:(NSString *)serviceURL
{
    __block BOOL supportsThirdParty = NO;

    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
    [context performBlockAndWait:^ {
        ServiceCapabilities *capabilities = [self getCapabilitiesForService:serviceURL inContext:context];
        if (capabilities) {
            ServiceAccess *accessOAuth = [capabilities getAccessForType:@"oauth2"];
            if (accessOAuth && ![accessOAuth.issuer.lowercaseString isEqualToString:@"cps.printeron.com"]) {
                supportsThirdParty = YES;
            }
        }
    }];

    return supportsThirdParty;
}

- (ServiceAccess *)getAccessForType:(NSString *)accessType
{
    for (ServiceAccess *access in self.access) {
        if ([access.type.lowercaseString isEqualToString:accessType.lowercaseString]) {
            return access;
        }
    }
    return nil;
}

- (ServiceLinks *)getLinkForType:(NSString *)linkType
{
    for (ServiceLinks *accessLink in self.links) {
        if ([accessLink.rel.lowercaseString isEqualToString:linkType.lowercaseString]) {
            return accessLink;
        }
    }
    return nil;
}

@end
