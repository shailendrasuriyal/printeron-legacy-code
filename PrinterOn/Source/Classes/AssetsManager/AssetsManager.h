//
//  AssetsManager.h
//  PrinterOn
//
//  Created by Mark Burns on 2/8/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@class ALAssetsLibrary;

@interface AssetsManager : NSObject

@property (nonatomic, strong) ALAssetsLibrary *assetLibrary;

+ (AssetsManager *)sharedAssetsManager;

@end
