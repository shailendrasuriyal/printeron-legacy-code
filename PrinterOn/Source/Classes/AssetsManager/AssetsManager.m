//
//  AssetsManager.m
//  PrinterOn
//
//  Created by Mark Burns on 2/8/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "AssetsManager.h"

#import "GCDSingleton.h"

#import <AssetsLibrary/AssetsLibrary.h>

@implementation AssetsManager

SINGLETON_GCD(AssetsManager);

- (instancetype)init
{
    if ((self = [super init])) {
        self.assetLibrary = [ALAssetsLibrary new];
    }
    return self;
}

@end
