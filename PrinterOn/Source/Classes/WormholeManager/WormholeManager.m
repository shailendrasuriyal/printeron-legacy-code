//
//  WormholeManager.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-13.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "WormholeManager.h"

#import "GCDSingleton.h"

#import <MMWormhole/MMWormhole.h>

@implementation WormholeManager

SINGLETON_GCD(WormholeManager);

- (instancetype)init
{
    if ((self = [super init])) {
        NSString *appGroupId = [[NSBundle mainBundle].infoDictionary valueForKey:@"APP_GROUP_ID"];
        self.wormHole = [[MMWormhole alloc] initWithApplicationGroupIdentifier:appGroupId optionalDirectory:@"Wormhole"];
    }
    return self;
}

@end
