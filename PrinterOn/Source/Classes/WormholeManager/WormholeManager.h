//
//  WormholeManager.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-13.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@class MMWormhole;

@interface WormholeManager : NSObject

@property (nonatomic, strong) MMWormhole *wormHole;

+ (WormholeManager *)sharedWormholeManager;

@end
