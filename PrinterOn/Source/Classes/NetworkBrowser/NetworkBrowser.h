//
//  NetworkBrowser.h
//  PrinterOn
//
//  Created by Mark Burns on 1/21/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

FOUNDATION_EXPORT NSString* const kNetworkBrowserServiceChangeNotification;

@interface NetworkBrowser : NSObject <NSNetServiceBrowserDelegate, NSNetServiceDelegate>

+ (NetworkBrowser *)sharedNetworkBrowser;

+ (void)startNetworkSearch;
+ (void)stopNetworkSearch;
+ (void)requeryPONNetworkServices;
+ (NSMutableArray *)getPONServicesArray;

@end
