//
//  PONNetworkService.m
//  PrinterOn
//
//  Created by Mark Burns on 1/23/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PONNetworkService.h"

#import <arpa/inet.h>

@implementation PONNetworkService

- (instancetype)initWithService:(NSNetService *)service
{
    self = [super init];

    if (self) {
        self.serviceName = service.name;
        self.printerCount = NSNotFound;
        self.isUntrustedCert = NO;

        NSData *txtRecordData = service.TXTRecordData;
        if (txtRecordData && txtRecordData.length > 0) {
            NSDictionary *dictionary = [NSNetService dictionaryFromTXTRecordData:txtRecordData];
            
            NSString *disc = [self copyStringFromTXTDictionary:dictionary forKey:@"disc"];
            self.isSearchable = (!disc || disc.length == 0) ? YES : disc.boolValue;
            
            self.serviceType = [self copyStringFromTXTDictionary:dictionary forKey:@"type"].intValue;
            
            self.serviceID = [self copyStringFromTXTDictionary:dictionary forKey:@"serviceID"];
            
            NSString *serviceURL = [self copyStringFromTXTDictionary:dictionary forKey:@"srvuri"];
            if (serviceURL.length > 0) {
                self.serviceURL = serviceURL;
            } else if (self.serviceType == 2) {
                self.serviceURL = @"https://www.printeron.net";
            } else {
                NSString *host = [self ipFromNetService:service];
                BOOL secure = [self copyStringFromTXTDictionary:dictionary forKey:@"sec"].boolValue;
                long portnum = service.port;
                NSString *port = secure ? (portnum == 443) ? @"" : [NSString stringWithFormat:@":%ld", portnum] : (portnum == 0 || portnum == 80) ? @"" : [NSString stringWithFormat:@":%ld", portnum];
                NSString *schema = secure ? @"https" : @"http";
                NSString *path = [self copyStringFromTXTDictionary:dictionary forKey:@"path"];
                if (path.length > 0 && ![path hasPrefix:@"/"]) {
                    path = [NSString stringWithFormat:@"/%@", path];
                }
                NSString *user = [self copyStringFromTXTDictionary:dictionary forKey:@"u"];
                NSString *password = [self copyStringFromTXTDictionary:dictionary forKey:@"p"];
                
                self.serviceURL = [NSString stringWithFormat:@"%@://%@%@%@%@%@%@%@", schema, user ?: @"", password ? @":" : @"", password ?: @"", (user || password) ? @"@" : @"", host, port, path ?: @""];
            }
        }
    }

    return self;
}


- (NSString *)copyStringFromTXTDictionary:(NSDictionary *)dictionary forKey:(NSString *)key
{
	NSData *data = dictionary[key];
	return data ? [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] : nil;
}

- (NSString *)ipFromNetService:(NSNetService*)service
{
    char addressBuffer[INET6_ADDRSTRLEN];
    
    for (NSData *data in service.addresses) {
        memset(addressBuffer, 0, INET6_ADDRSTRLEN);
        
        typedef union {
            struct sockaddr sa;
            struct sockaddr_in ipv4;
            struct sockaddr_in6 ipv6;
        } ip_socket_address;
        
        ip_socket_address *socketAddress = (ip_socket_address *)data.bytes;
        
        if (socketAddress && (socketAddress->sa.sa_family == AF_INET || socketAddress->sa.sa_family == AF_INET6)) {
            const char *addressStr = inet_ntop(socketAddress->sa.sa_family, (socketAddress->sa.sa_family == AF_INET ? (void *)&(socketAddress->ipv4.sin_addr) : (void *)&(socketAddress->ipv6.sin6_addr)), addressBuffer, sizeof(addressBuffer));
            
            if (addressStr) {
                return @(addressStr);
            }
        }
    }
    
    return nil;
}

@end
