//
//  PONNetworkService.h
//  PrinterOn
//
//  Created by Mark Burns on 1/23/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface PONNetworkService : NSObject

@property (nonatomic, strong) NSString *serviceName;
@property (nonatomic, strong) NSString *serviceURL;
@property (nonatomic, strong) NSString *serviceID;
@property (nonatomic, assign) int serviceType;
@property (nonatomic, assign) long printerCount;
@property (nonatomic, assign) BOOL isSearchable;
@property (nonatomic, assign) BOOL isUntrustedCert;

- (instancetype)initWithService:(NSNetService *)service;

@end
