//
//  NetworkBrowser.m
//  PrinterOn
//
//  Created by Mark Burns on 1/21/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "NetworkBrowser.h"

#import "DirSearch.h"
#import "GCDSingleton.h"
#import "PONNetworkService.h"

NSString* const kNetworkBrowserServiceChangeNotification = @"com.printeron.NetworkBrowser.ChangeNotification";

@interface NetworkBrowser ()

@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSNetServiceBrowser *browser;
@property (nonatomic, strong) NSMutableArray *resolvingServices;
@property (nonatomic, strong) NSMutableArray *ponServices;
@property (nonatomic, strong) NSComparator ponServiceComparator;
@property dispatch_queue_t ponServicesQueue;
@property BOOL searching;

@end

@implementation NetworkBrowser

+ (NetworkBrowser *)sharedNetworkBrowser
{
    __strong static NetworkBrowser *sharedNetworkBrowser = nil;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        if (![ThemeLoader boolForKey:@"PrintersScreen.Tabbar.HideNetworkBrowser"]) {
            sharedNetworkBrowser = [self new];
        }
    });
    return sharedNetworkBrowser;
}

- (instancetype)init
{
    if ((self = [super init])) {
        self.queue = [NSOperationQueue new];
        self.queue.name = @"Network Browser Queue";

        self.ponServicesQueue = dispatch_queue_create("PONNetworkServiceQueue", NULL);

        self.resolvingServices = [NSMutableArray array];
        self.ponServices = [NSMutableArray array];
        self.ponServiceComparator = ^NSComparisonResult(id obj1, id obj2) {
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"serviceName" ascending:YES];
            NSComparisonResult result = [descriptor compareObject:obj1 toObject:obj2];
            if (result != NSOrderedSame) {
                return result;
            }
            return NSOrderedSame;
        };

        self.browser = [NSNetServiceBrowser new];
        self.browser.delegate = self;

        self.searching = NO;
    }
    return self;
}

+ (void)startNetworkSearch
{
    [[NetworkBrowser sharedNetworkBrowser] startSearch];
}

+ (void)stopNetworkSearch
{
    [[NetworkBrowser sharedNetworkBrowser] stopSearch];
}

+ (void)requeryPONNetworkServices
{
    NetworkBrowser *browser = [NetworkBrowser sharedNetworkBrowser];
    if (browser == nil) return;

    dispatch_async(browser.ponServicesQueue, ^{
        for (PONNetworkService *ponService in browser.ponServices) {
            [browser fetchPONNetworkServicePrinterCount:ponService];
        }
    });
}

+ (NSMutableArray *)getPONServicesArray
{
    return [NetworkBrowser sharedNetworkBrowser].ponServices;
}

- (void)startSearch
{
    if (!self.searching) {
        [self.browser searchForServicesOfType:@"_poncps._tcp." inDomain:@"local."];
    }
}

- (void)stopSearch
{
    [self.browser stop];
}

- (void)searchStopped
{
    self.searching = NO;

    // Stop resolving any services that are still pending
    for (NSNetService *service in self.resolvingServices) {
        [service stop];
    }

    // Remove all services
    [self.resolvingServices removeAllObjects];
}

- (PONNetworkService *)getPONNetworkService:(NSNetService *)service
{
    return [self getPONNetworkServiceWithName:service.name];
}

- (PONNetworkService *)getPONNetworkServiceWithName:(NSString *)name
{
    __block PONNetworkService *netService;
    dispatch_async(self.ponServicesQueue, ^{
        for (PONNetworkService *ponService in self.ponServices) {
            if ([ponService.serviceName isEqualToString:name]) {
                netService = ponService;
            }
        }
    });
    return netService;
}

- (void)addPONNetworkService:(NSNetService *)service
{
    if (![self getPONNetworkService:service]) {
        PONNetworkService *ponService = [[PONNetworkService alloc] initWithService:service];
        dispatch_async(self.ponServicesQueue, ^{
            NSUInteger addIndex = [self.ponServices indexOfObject:ponService inSortedRange:NSMakeRange(0, self.ponServices.count) options:NSBinarySearchingInsertionIndex usingComparator:self.ponServiceComparator];
            [self.ponServices insertObject:ponService atIndex:addIndex];
            [self fetchPONNetworkServicePrinterCount:ponService];
        });
    }
}

- (void)removePONNetworkService:(NSNetService *)service
{
    dispatch_async(self.ponServicesQueue, ^{
        int index = 0;
        for (PONNetworkService *ponService in self.ponServices) {
            if ([ponService.serviceName isEqualToString:service.name]) {
                break;
            }
            index++;
        }

        if (index < self.ponServices.count) {
            [self.ponServices removeObjectAtIndex:index];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkBrowserServiceChangeNotification object:[NetworkBrowser sharedNetworkBrowser]];
            });
        }
    });
}

- (void)fetchPONNetworkServicePrinterCount:(PONNetworkService *)service
{
    if (!service.isSearchable) return;

    RKObjectManager *searchObjectManager = [DirSearch setupHeaderSearchWithService:service.serviceURL];

    NSDictionary *params;
    if (service.serviceType == 2) {
        params = @{
                   @"searchType": @"searchByServiceID",
                   @"searchServiceID": service.serviceID ?: @""
                   };
    }

    __block PONNetworkService *netService = service;
    [DirSearch createHeaderSearchWithParameters:params objectManager:searchObjectManager success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Update the total count returned
        for (id value in mappingResult.dictionary.objectEnumerator) {
            if ([value isMemberOfClass:[DirSearch class]]) {
                DirSearch *info = value;
                netService.isUntrustedCert = NO;
                netService.printerCount = info.resultCount;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkBrowserServiceChangeNotification object:[NetworkBrowser sharedNetworkBrowser]];
                });
                break;
            }
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if ([self isUntrustedCertificateError:error]) {
            netService.isUntrustedCert = YES;
            netService.printerCount = NSNotFound;
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkBrowserServiceChangeNotification object:[NetworkBrowser sharedNetworkBrowser]];
            });
        }
    } completionHandler:^(RKObjectRequestOperation *operation, NSError *error) {
        if (error == nil) {
            [self.queue addOperation:operation];
        }
    }];
}

- (BOOL)isUntrustedCertificateError:(NSError *)error
{
    if ([error.domain isEqualToString:NSURLErrorDomain]) {
        if (error.code == kCFURLErrorUserCancelledAuthentication ||
            error.code == kCFURLErrorSecureConnectionFailed ||
            error.code == kCFURLErrorServerCertificateHasBadDate ||
            error.code == kCFURLErrorServerCertificateUntrusted ||
            error.code == kCFURLErrorServerCertificateHasUnknownRoot ||
            error.code == kCFURLErrorServerCertificateNotYetValid ||
            error.code == kCFURLErrorClientCertificateRejected
            )
        {
            return YES;
        }
    }
    return NO;
}

#pragma mark - NSNetServiceBrowserDelegate

- (void)netServiceBrowserWillSearch:(NSNetServiceBrowser *)browser
{
    self.searching = YES;

    // Clear the list of PrinterOn services
    dispatch_async(self.ponServicesQueue, ^{
        [self.ponServices removeAllObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkBrowserServiceChangeNotification object:[NetworkBrowser sharedNetworkBrowser]];
        });
    });
}

- (void)netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)browser
{
    [self searchStopped];
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
             didNotSearch:(NSDictionary *)errorDict
{
    [self searchStopped];
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
           didFindService:(NSNetService *)service
               moreComing:(BOOL)moreComing
{
    [self.resolvingServices addObject:service];

    // Start resolving new service
    service.delegate = self;
    [service resolveWithTimeout:0.0];
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
         didRemoveService:(NSNetService *)service
               moreComing:(BOOL)moreComing
{
    // Stop resolving the service
    [service stop];

    // Remove from all stores
    [self.resolvingServices removeObject:service];
    [self removePONNetworkService:service];
}

#pragma mark - NSNetServiceDelegate

- (void)netServiceDidResolveAddress:(NSNetService *)service
{
    // No need to resolve more, so stop
    [service stop];
    [self.resolvingServices removeObject:service];

    // Add the new PONNetworkService in the background
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(addPONNetworkService:) object:service];
    [self.queue addOperation:operation];
}

- (void)netService:(NSNetService *)service
     didNotResolve:(NSDictionary *)errorDict
{
}

@end
