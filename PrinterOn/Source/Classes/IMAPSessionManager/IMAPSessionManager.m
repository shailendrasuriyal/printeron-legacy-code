//
//  IMAPSessionManager.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-10-20.
//  Copyright © 2015 PrinterOn Inc. All rights reserved.
//

#import "IMAPSessionManager.h"

#import "EmailAttachmentManager.h"
#import "EmailFolder.h"
#import "EmailMessage.h"
#import "GTMOAuth2Authentication.h"
#import "NSData+Transform.h"
#import "OAuth2Manager.h"

#import "AppAuth.h"
#import <MailCore/MailCore.h>

@interface IMAPSessionManager ()

@property (nonatomic, strong) MCOIMAPSession *imapSession;
@property (nonatomic, strong) EmailAccount *emailAccount;
@property (nonatomic, strong) id auth;
@property (readwrite, strong, nonatomic) dispatch_queue_t completionQueue;

@property (nonatomic, strong) UIAlertController *alertVC;
@property (nonatomic, weak) UINavigationController *alertController;

@end

@implementation IMAPSessionManager

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;

    self.imapSession = [MCOIMAPSession new];
    self.completionQueue = dispatch_queue_create("PONIMAPSessionManager", DISPATCH_QUEUE_CONCURRENT);

    return self;
}

- (void)dealloc
{
    if ([self.auth isMemberOfClass:[GTMOAuth2Authentication class]]) {
        // Cancel any pending OAuth2 operations
        [(GTMOAuth2Authentication *)self.auth stopAuthorization];
    }

    if (self.imapSession) {
        // Cancel any outstanding operations before closing the IMAP session
        [self.imapSession cancelAllOperations];

        // Disconnect the IMAP session
        MCOIMAPOperation *disconnect = [self.imapSession disconnectOperation];
        [disconnect start:^(NSError *error) {}];
    }
}

#pragma mark - Setup

- (void)setupUsingEmailAccount:(EmailAccount *)emailAccount
{
    if (!emailAccount) return;

    BOOL isUsingOAuth2 = [emailAccount isUsingOAuth2];
    id auth = isUsingOAuth2 ? [emailAccount getOAuth2Authentication] : nil;
    NSString *password = isUsingOAuth2 ? nil : [emailAccount getEmailAccountPassword];

    [self setupWithHost:emailAccount.hostName port:emailAccount.port.intValue userName:emailAccount.emailAddress password:password auth:auth ssl:emailAccount.useSSL.boolValue type:emailAccount.mailType.intValue account:emailAccount];
}

- (void)setupWithHost:(NSString *)hostname port:(unsigned int)port userName:(NSString *)username password:(NSString *)password auth:(id)auth ssl:(BOOL)ssl type:(EmailAccountType)type account:(EmailAccount *)account
{
    if (!self.imapSession) return;
    self.emailAccount = account;
    self.auth = auth;

    [self.imapSession setHostname:hostname];
    [self.imapSession setPort:port];
    [self.imapSession setConnectionType:ssl ? MCOConnectionTypeTLS : MCOConnectionTypeClear];
    [self.imapSession setTimeout:30];

    if (self.auth == nil) {
        [self.imapSession setUsername:username];
        [self.imapSession setPassword:password];
    } else {
        [self.imapSession setUsername:[OAuth2Manager userEmailForAuth:self.auth] ?: username];

        [self authorizeRequest:nil completionHandler:^(NSError *error) {
            if (error == nil) {
                [self.imapSession setAuthType:type == EmailAccountTypeOutlook ? MCOAuthTypeXOAuth2Outlook : MCOAuthTypeXOAuth2];
                [self.imapSession setOAuth2Token:[OAuth2Manager accessTokenForAuth:self.auth]];
                [self.emailAccount saveOAuth2Authentication:self.auth];
            }
        }];
    }
}

- (void)authorizeRequest:(NSMutableURLRequest *)request completionHandler:(void (^)(NSError *))handler
{
    if ([self.auth isMemberOfClass:[GTMOAuth2Authentication class]]) {
        [(GTMOAuth2Authentication *)self.auth authorizeRequest:request completionHandler:handler];
    } else if ([self.auth isMemberOfClass:[OIDAuthState class]]) {
        [(OIDAuthState *)self.auth performActionWithFreshTokens:^(NSString *_Nullable accessToken, NSString *_Nullable idToken, NSError *_Nullable error) {

            NSURL *requestURL = request.URL;
            NSString *scheme = requestURL.scheme;
            BOOL isAuthorizableRequest = !requestURL || (scheme && [scheme caseInsensitiveCompare:@"https"] == NSOrderedSame) || [requestURL isFileURL];

            if (isAuthorizableRequest && accessToken && accessToken.length > 0) {
                if (request) {
                    // Adds the authorization header to the request.
                    NSString *value = [NSString stringWithFormat:@"%@ %@", @"Bearer", accessToken];
                    [request setValue:value forHTTPHeaderField:@"Authorization"];
                }
                if (handler) handler(nil);
            } else {
                if (!isAuthorizableRequest || !error) {
                    error = [NSError errorWithDomain:kGTMOAuth2ErrorDomain
                                                code:GTMOAuth2ErrorUnauthorizableRequest
                                            userInfo:[error.userInfo copy]];
                }
                if (handler) handler(error);
            }
        }];
    } else {
        if (handler) handler(nil);
    }
}

- (void)checkAccountWithCompletionBlock:(void (^)(NSError *error))completionBlock
{
    if (!self.imapSession) {
        if (completionBlock) {
            completionBlock([NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorUnknown userInfo:nil]);
        }
        return;
    }

    if (self.auth) {
        __weak __typeof(self)weakSelf = self;
        [self authorizeRequest:nil completionHandler:^(NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            [strongSelf doCheckAccountWithCompletionBlock:completionBlock];
        }];
    } else {
        [self doCheckAccountWithCompletionBlock:completionBlock];
    }
}

- (void)doCheckAccountWithCompletionBlock:(void (^)(NSError *error))completionBlock
{
    [[self.imapSession checkAccountOperation] start:^(NSError *error) {
        if (completionBlock) {
            completionBlock(error);
        }
    }];
}

- (MCOIMAPSession *)getIMAPSession
{
    return self.imapSession;
}

- (EmailAccount *)getEmailAccount
{
    return self.emailAccount;
}

#pragma mark - Folder Updates

- (void)fetchAllFoldersForAccount:(EmailAccount *)account completionBlock:(void (^)(NSError *error))completionBlock
{
    if (!self.imapSession || !account) {
        if (completionBlock) {
            completionBlock(nil);
        }
        return;
    }

    if (self.auth) {
        __weak __typeof(self)weakSelf = self;
        [self authorizeRequest:nil completionHandler:^(NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            [strongSelf doFetchAllFoldersForAccount:account completionBlock:completionBlock];
        }];
    } else {
        [self doFetchAllFoldersForAccount:account completionBlock:completionBlock];
    }
}

- (void)doFetchAllFoldersForAccount:(EmailAccount *)account completionBlock:(void (^)(NSError *error))completionBlock
{
    NSManagedObjectID *accountID = account.objectID;

    __weak __typeof(self)weakSelf = self;
    [[self.imapSession fetchAllFoldersOperation] start:^(NSError *error, NSArray *folders) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        dispatch_async(strongSelf.completionQueue, ^{
            if (!error && folders.count > 0) {
                NSManagedObjectContext *context = [CoreDataManager getManagedContext];
                EmailAccount *account = [context objectWithID:accountID];

                BOOL updates = [strongSelf addNewFolders:folders toAccount:account inContext:context];

                if (account.emailFolder.count > folders.count) {
                    updates = [strongSelf deleteFolders:folders fromAccount:account inContext:context];
                }

                if (updates) {
                    [context saveToPersistentStore:nil];
                }
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if (completionBlock) {
                    completionBlock(error);
                }
            });
        });
    }];
}

- (BOOL)addNewFolders:(NSArray *)folders toAccount:(EmailAccount *)account inContext:(NSManagedObjectContext *)context
{
    BOOL added = NO;
    BOOL updated = NO;

    for (int i = 0; i < folders.count; i++) {
        MCOIMAPFolder *imapFolder = [folders objectAtIndex:i];
        BOOL found = NO;
        
        for (EmailFolder *emailFolder in account.emailFolder) {
            if ([imapFolder.path isEqualToString:emailFolder.folderName]) {
                found = YES;
                if ([emailFolder.order intValue] != i) {
                    emailFolder.order = @(i);
                    updated = YES;
                }
                break;
            }
        }

        if (!found) {
            [context performBlockAndWait:^() {
                EmailFolder *emailFolder = [NSEntityDescription insertNewObjectForEntityForName:@"EmailFolder" inManagedObjectContext:context];
                emailFolder.folderName = imapFolder.path;
                emailFolder.order = @(i);
                [account addEmailFolderObject:emailFolder];
            }];
            added = YES;
        }
    }

    return added || updated;
}

- (BOOL)deleteFolders:(NSArray *)folders fromAccount:(EmailAccount *)account inContext:(NSManagedObjectContext *)context
{
    NSMutableArray *deleteArray = [NSMutableArray array];
    
    for (EmailFolder *emailFolder in account.emailFolder) {
        BOOL found = NO;

        for (MCOIMAPFolder *imapFolder in folders) {
            if ([emailFolder.folderName isEqualToString:imapFolder.path]) {
                found = YES;
                break;
            }
        }
        
        if (!found) {
            [deleteArray addObject:emailFolder];
        }
    }
    
    if (deleteArray.count > 0) {
        for (EmailFolder *emailFolder in deleteArray) {
            [emailFolder clearAttachmentsCacheForFolder];
            [context deleteObject:emailFolder];
        }
        return YES;
    }

    return NO;
}

#pragma mark - Message Updates

- (void)fetchAllMessageUIDsForFolder:(EmailFolder *)folder withAccount:(EmailAccount *)account completionBlock:(void (^)(EmailFolder *folder, NSError *error))completionBlock
{
    if (!self.imapSession || !folder || !account) {
        if (completionBlock) {
            completionBlock(folder, nil);
        }
        return;
    }

    if (self.auth) {
        __weak __typeof(self)weakSelf = self;
        [self authorizeRequest:nil completionHandler:^(NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            [strongSelf doFetchAllMessageUIDsForFolder:folder withAccount:account completionBlock:completionBlock];
        }];
    } else {
        [self doFetchAllMessageUIDsForFolder:folder withAccount:account completionBlock:completionBlock];
    }
}

- (void)doFetchAllMessageUIDsForFolder:(EmailFolder *)folder withAccount:(EmailAccount *)account completionBlock:(void (^)(EmailFolder *folder, NSError *error))completionBlock
{
    MCOIMAPMessagesRequestKind requestKind = (MCOIMAPMessagesRequestKind)MCOIMAPMessagesRequestKindUid;
    MCOIndexSet *numbers = [MCOIndexSet indexSetWithRange:MCORangeMake(1, UINT64_MAX)];
    MCOIMAPFetchMessagesOperation *fetchOperation = [self.imapSession fetchMessagesOperationWithFolder:folder.folderName requestKind:requestKind uids:numbers];

    NSManagedObjectID *folderID = folder.objectID;

    __weak __typeof(self)weakSelf = self;
    [fetchOperation start:^(NSError *error, NSArray *messages, MCOIndexSet *vanished) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        dispatch_async(strongSelf.completionQueue, ^{
            if (!error) {
                NSManagedObjectContext *context = [CoreDataManager getManagedContext];
                EmailFolder *folder = [context objectWithID:folderID];

                BOOL updates = NO;

                if (messages.count > 0) {
                    updates = [strongSelf addNewMessages:messages toFolder:folder inContext:context];

                    if (folder.emailMessage.count > messages.count) {
                        updates = [strongSelf deleteMessages:messages fromFolder:folder inContext:context];
                    }
                } else {
                    for (EmailMessage *emailMessage in folder.emailMessage) {
                        [context deleteObject:emailMessage];
                        updates = YES;
                    }
                }

                if (updates) {
                    [context saveToPersistentStore:nil];
                }
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if (completionBlock) {
                    completionBlock(folder, error);
                }
            });
        });
    }];
}

- (BOOL)addNewMessages:(NSArray *)messages toFolder:(EmailFolder *)folder inContext:(NSManagedObjectContext *)context
{
    BOOL added = NO;

    NSSet *existingUIDs = [folder.emailMessage valueForKey:@"uid"];
    BOOL empty = existingUIDs.count == 0;

    for (MCOIMAPMessage *message in messages) {
        BOOL exists = !empty && [existingUIDs containsObject:@(message.uid)];
        if (!exists) {
            [context performBlockAndWait:^() {
                EmailMessage *emailMessage = [NSEntityDescription insertNewObjectForEntityForName:@"EmailMessage" inManagedObjectContext:context];
                emailMessage.uid = @(message.uid);
                [folder addEmailMessageObject:emailMessage];
            }];
            added = YES;
        }
    }

    return added;
}

- (BOOL)deleteMessages:(NSArray *)messages fromFolder:(EmailFolder *)folder inContext:(NSManagedObjectContext *)context
{
    NSMutableArray *deleteArray = [NSMutableArray array];

    for (EmailMessage *emailMessage in folder.emailMessage) {
        BOOL found = NO;

        for (MCOIMAPMessage *message in messages) {
            if (emailMessage.uid.unsignedIntValue == message.uid) {
                found = YES;
                break;
            }
        }

        if (!found) {
            [deleteArray addObject:emailMessage];
        }
    }

    if (deleteArray.count > 0) {
        for (EmailMessage *emailMessage in deleteArray) {
            [emailMessage clearAttachmentsCacheForMessage];
            [context deleteObject:emailMessage];
        }
        return YES;
    }

    return NO;
}

- (void)fetchNextMessagesForFolder:(EmailFolder *)folder count:(NSUInteger)count newOnly:(BOOL)new completionBlock:(void (^)(EmailFolder *folder, NSError *error))completionBlock
{
    if (!self.imapSession || !folder || folder.emailMessage.count == 0) {
        if (completionBlock) {
            completionBlock(folder, nil);
        }
        return;
    }

    if (self.auth) {
        __weak __typeof(self)weakSelf = self;
        [self authorizeRequest:nil completionHandler:^(NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            [strongSelf doFetchNextMessagesForFolder:folder count:count newOnly:new completionBlock:completionBlock];
        }];
    } else {
        [self doFetchNextMessagesForFolder:folder count:count newOnly:new completionBlock:completionBlock];
    }
}

- (void)doFetchNextMessagesForFolder:(EmailFolder *)folder count:(NSUInteger)count newOnly:(BOOL)new completionBlock:(void (^)(EmailFolder *folder, NSError *error))completionBlock
{
    MCOIMAPMessagesRequestKind requestKind = (MCOIMAPMessagesRequestKind) (MCOIMAPMessagesRequestKindHeaders | MCOIMAPMessagesRequestKindStructure);
    MCOIndexSet *uids = [self getMessageUIDsToFetch:folder count:count newOnly:new];

    if (uids.count > 0) {
        MCOIMAPFetchMessagesOperation *fetchOperation = [self.imapSession fetchMessagesOperationWithFolder:folder.folderName requestKind:requestKind uids:uids];

        NSManagedObjectID *folderID = folder.objectID;

        __weak __typeof(self)weakSelf = self;
        [fetchOperation start:^(NSError *error, NSArray *messages, MCOIndexSet *vanished) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            dispatch_async(strongSelf.completionQueue, ^{
                if (!error && messages.count > 0) {
                    NSManagedObjectContext *context = [CoreDataManager getManagedContext];
                    EmailFolder *folderContext = [context objectWithID:folderID];

                    BOOL save = NO;

                    for (MCOIMAPMessage *message in messages) {
                        for (EmailMessage *email in folderContext.emailMessage) {
                            if (email.uid.unsignedIntValue == message.uid) {
                                email.message = message;
                                save = YES;
                            }
                        }
                    }

                    if (save) {
                        [context saveToPersistentStore:nil];
                    }
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    if (completionBlock) {
                        completionBlock(folder, error);
                    }
                });
            });
        }];
    } else {
        if (completionBlock) {
            completionBlock(folder, nil);
        }
    }
}

- (MCOIndexSet *)getMessageUIDsToFetch:(EmailFolder *)folder count:(NSUInteger)count newOnly:(BOOL)new
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"uid" ascending:NO];
    NSArray *sortedUIDs = [folder.emailMessage sortedArrayUsingDescriptors:@[sortDescriptor]];

    MCOIndexSet *uids = [MCOIndexSet indexSet];

    if (new) {
        BOOL firstFetch = YES;

        for (EmailMessage *message in sortedUIDs) {
            if (message.message) {
                firstFetch = NO;
                break;
            }

            [uids addIndex:message.uid.unsignedLongLongValue];
        }

        if (firstFetch) {
            MCOIndexSet *new = [MCOIndexSet indexSet];
            for (EmailMessage *message in sortedUIDs) {
                if (new.count < count) {
                    [new addIndex:message.uid.unsignedLongLongValue];
                } else {
                    break;
                }
            }
            uids = new;
        }
    } else {
        long long lastUID = -1;

        for (EmailMessage *message in sortedUIDs) {
            if (message.message) {
                lastUID = message.uid.longLongValue;
                continue;
            }

            if (lastUID == -1) {
                continue;
            } else {
                if (uids.count < count) {
                    [uids addIndex:message.uid.unsignedLongLongValue];
                } else {
                    break;
                }
            }
        }
    }
    
    return uids;
}

- (void)fetchContentsForMessage:(EmailMessage *)message completionBlock:(void (^)(EmailMessage *message, NSError *error))completionBlock
{
    if (!self.imapSession || !message) {
        if (completionBlock) {
            completionBlock(message, nil);
        }
        return;
    }

    if (self.auth) {
        __weak __typeof(self)weakSelf = self;
        [self authorizeRequest:nil completionHandler:^(NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            [strongSelf doFetchContentsForMessage:message completionBlock:completionBlock];
        }];
    } else {
        [self doFetchContentsForMessage:message completionBlock:completionBlock];
    }
}

- (void)doFetchContentsForMessage:(EmailMessage *)message completionBlock:(void (^)(EmailMessage *message, NSError *error))completionBlock
{
    MCOIMAPMessage *imapMessage = message.message;

    NSArray *requiredParts = [imapMessage requiredPartsForRendering];
    unsigned long requiredCount = requiredParts.count;

    if (requiredCount == 0) {
        if (completionBlock) {
            completionBlock(message, nil);
        }
        return;
    }

    __block int partCount = 0;
    __block int errorCount = 0;
    __block NSError *partError = nil;

    for (MCOIMAPPart *part in requiredParts) {
        NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:self.emailAccount.emailAddress messageID:[NSString stringWithFormat:@"%d", imapMessage.uid] partID:part.partID fileName:part.filename];
        id attachment = [[EmailAttachmentManager sharedEmailAttachmentManager] progressForAttachment:attachID];

        if (attachment == [NSNull null]) {
            partCount++;
            continue;
        }

        __weak __typeof(self)weakSelf = self;
        [self doFetchMessageAttachmentWithFolder:message.emailFolder.folderName uid:imapMessage.uid partID:part.partID encoding:part.encoding filename:part.filename progress:nil completionBlock:^(NSData *data, NSError *error) {

            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            dispatch_async(strongSelf.completionQueue, ^{
                partCount++;

                if (error) {
                    errorCount++;
                    partError = error;
                } else if (data) {
                    [data transform];
                    [[EmailAttachmentManager sharedEmailAttachmentManager] writeAttachment:attachID data:data];
                }

                if (partCount == requiredCount) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (completionBlock) {
                            completionBlock(message, errorCount > 0 ? partError : nil);
                        }
                    });
                }
            });
        }];
    }

    if (partCount == requiredCount) {
        if (completionBlock) {
            completionBlock(message, errorCount > 0 ? partError : nil);
        }
    }
}

#pragma mark - Attachments

- (void)fetchMessageAttachmentWithFolder:(NSString *)folder uid:(uint32_t)uid partID:(NSString *)partID encoding:(MCOEncoding)encoding filename:(NSString *)filename progress:(NSProgress *)progress completionBlock:(void (^)(NSData *data, NSError *error))completionBlock
{
    if (!self.imapSession || !folder || !partID) {
        if (completionBlock) {
            completionBlock(nil, nil);
        }
        return;
    }

    if (self.auth) {
        __weak __typeof(self)weakSelf = self;
        [self authorizeRequest:nil completionHandler:^(NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            [strongSelf doFetchMessageAttachmentWithFolder:folder uid:uid partID:partID encoding:encoding filename:filename progress:progress completionBlock:completionBlock];
        }];
    } else {
        [self doFetchMessageAttachmentWithFolder:folder uid:uid partID:partID encoding:encoding filename:filename progress:progress completionBlock:completionBlock];
    }
}

- (void)doFetchMessageAttachmentWithFolder:(NSString *)folder uid:(uint32_t)uid partID:(NSString *)partID encoding:(MCOEncoding)encoding filename:(NSString *)filename progress:(NSProgress *)progress completionBlock:(void (^)(NSData *data, NSError *error))completionBlock
{
    MCOIMAPFetchContentOperation *fetchOperation = [self.imapSession fetchMessageAttachmentOperationWithFolder:folder uid:uid partID:partID encoding:encoding];
    if (progress) {
        fetchOperation.progress = ^(unsigned int current, unsigned int maximum) {
            progress.totalUnitCount = maximum;
            progress.completedUnitCount = current;
        };
    }

    NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:self.emailAccount.emailAddress messageID:[NSString stringWithFormat:@"%d", uid] partID:partID fileName:filename];

    __weak __typeof(self)weakSelf = self;
    [fetchOperation start:^(NSError *error, NSData *data) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        dispatch_async(strongSelf.completionQueue, ^{
            if (!error && data) {
                [[EmailAttachmentManager sharedEmailAttachmentManager] writeAttachment:attachID data:data];
            }
            [[EmailAttachmentManager sharedEmailAttachmentManager] stopAttachment:attachID];

            dispatch_async(dispatch_get_main_queue(), ^{
                if (completionBlock) {
                    completionBlock(data, error);
                }
            });
        });
    }];
}

#pragma mark - Error Handling

- (BOOL)isAuthenticationError:(NSError *)error
{
    return [error.domain isEqualToString:MCOErrorDomain] && error.code == MCOErrorAuthentication;
}

- (BOOL)isConnectionError:(NSError *)error
{
    return [error.domain isEqualToString:MCOErrorDomain] && (error.code == MCOErrorConnection || error.code == MCOErrorTLSNotAvailable);
}

- (void)showError:(NSError *)error overController:(UINavigationController *)nav
{
    if (self.alertVC.isViewLoaded && self.alertVC.view.window != nil) {
        return;
    }

    if ([self isAuthenticationError:error]) {
        self.alertController = nav;
        self.alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:[NSString stringWithFormat:NSLocalizedPONString(@"ERROR_AUTHENTICATION", nil), self.emailAccount.emailAddress] preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"TITLE_SETTINGS", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                NSMutableArray *vcs = [NSMutableArray array];
                for (UIViewController *vc in self.alertController.viewControllers) {
                    [vcs addObject:vc];
                    if ([vc isMemberOfClass:NSClassFromString(@"EmailAccountsViewController")]) {
                        [vc performSegueWithIdentifier:@"showEmailSetup" sender:self.emailAccount];
                        [vcs addObject:self.alertController.viewControllers.lastObject];
                        break;
                    }
                }
                [self.alertController setViewControllers:vcs animated:NO];

                self.alertController = nil;
                self.alertVC = nil;
            });
        }];
        [self.alertVC addAction:settingsAction];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            self.alertController = nil;
            self.alertVC = nil;
        }];
        [self.alertVC addAction:cancelAction];

        [self.alertController presentViewController:self.alertVC animated:YES completion:nil];
    } else if ([self isConnectionError:error]) {
        self.alertController = nav;
        self.alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_CONNECTION", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            self.alertController = nil;
            self.alertVC = nil;
        }];
        [self.alertVC addAction:cancelAction];

        [self.alertController presentViewController:self.alertVC animated:YES completion:nil];
    }
}

@end
