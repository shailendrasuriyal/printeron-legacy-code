//
//  IMAPSessionManager.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-10-20.
//  Copyright © 2015 PrinterOn Inc. All rights reserved.
//

#import "EmailAccount.h"

#import <MailCore/MCOConstants.h>

@class EmailFolder, EmailMessage, MCOIMAPSession;

@interface IMAPSessionManager : NSObject

- (void)setupUsingEmailAccount:(EmailAccount *)emailAccount;
- (void)setupWithHost:(NSString *)hostname port:(unsigned int)port userName:(NSString *)username password:(NSString *)password auth:(id)auth ssl:(BOOL)ssl type:(EmailAccountType)type account:(EmailAccount *)account;
- (void)checkAccountWithCompletionBlock:(void (^)(NSError *error))completionBlock;

- (MCOIMAPSession *)getIMAPSession;
- (EmailAccount *)getEmailAccount;

- (void)fetchAllFoldersForAccount:(EmailAccount *)account completionBlock:(void (^)(NSError *error))completionBlock;
- (void)fetchAllMessageUIDsForFolder:(EmailFolder *)folder withAccount:(EmailAccount *)account completionBlock:(void (^)(EmailFolder *folder, NSError *error))completionBlock;
- (void)fetchNextMessagesForFolder:(EmailFolder *)folder count:(NSUInteger)count newOnly:(BOOL)new completionBlock:(void (^)(EmailFolder *folder, NSError *error))completionBlock;

- (void)fetchContentsForMessage:(EmailMessage *)message completionBlock:(void (^)(EmailMessage *message, NSError *error))completionBlock;

- (void)fetchMessageAttachmentWithFolder:(NSString *)folder uid:(uint32_t)uid partID:(NSString *)partID encoding:(MCOEncoding)encoding filename:(NSString *)filename progress:(NSProgress *)progress completionBlock:(void (^)(NSData *data, NSError *error))completionBlock;

- (void)showError:(NSError *)error overController:(UINavigationController *)nav;

@end
