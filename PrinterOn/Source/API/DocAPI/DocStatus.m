//
//  DocStatus.m
//  PrinterOn
//
//  Created by Mark Burns on 11/25/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "DocStatus.h"

#import "DirSearch.h"
#import "DocProcess.h"
#import "JobInteractionService.h"
#import "OAuth2Manager.h"
#import "Printer.h"
#import "PrintJob.h"
#import "PrintJobSessionItem.h"
#import "RKObjectMappingOperationDataSource.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "UserAccount.h"

#import "AFNetworking.h"
#import "XMLReader.h"

@implementation DocStatus

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"DocAPIver": @"apiVersion",
                       @"docSWver": @"swVersion",
                       @"returnCode.text": @"returnCode",
                       @"returnCodeReason.text": @"returnCodeReason",
                       @"jobState.text": @"jobState",
                       @"jobStateText.text": @"jobStateText",
                       @"jobStateReason.text": @"jobStateReason",
                       @"jobReferenceID.text": @"jobReferenceID",
                       @"jobKey.text": @"jobKey",
                       @"jobDestination.text": @"jobDestination",
                       @"jobPageCount.text": @"jobPageCount",
                       @"docPageCount.text": @"docPageCount",
                       @"documentURI.text": @"documentURI",
                       @"userMessageID.text": @"userMessageID",
                       @"userMessage.text": @"userMessage",
                       @"errText.text": @"errText",
                       @"viewOutputInfo.outputFormat.text": @"outputFormat",
                       @"viewOutputInfo.outputURI.text" : @"outputURI",
                       @"viewOutputInfo.previewFilePrefix.text" : @"outputFilePrefix",
                       };
    });
    return attributes;
}

+ (NSDictionary *)requiredParameters
{
    static NSDictionary *required = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        required = @{
                    @"docAPIfunc": @"DocStatus",
                    @"clientAPIver": @"3.1.1",
                    @"clientSWProtocol" : @"DocAPI",
                    @"clientSWName": @"PrinterOn-iOS",
                    @"clientSWVer": [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"] ?: @"",
                    @"userSWName": @"PrinterOn-iOS",
                    @"userLang": NSLocalizedPONString(@"USER_LANG", nil),
                    @"userLanguage": NSLocalizedPONString(@"USER_LANG", nil),
                    };
    });
    return required;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if (self = [super init]) {
        if (dictionary == nil || dictionary.count == 0) {
            return nil;
        }

        RKObjectMapping *docAPIMapping = [RKObjectMapping mappingForClass:[DocStatus class]];
        [docAPIMapping addAttributeMappingsFromDictionary:[DocStatus attributeMappings]];
        [docAPIMapping addPropertyMapping:[JobInteractionService getRelationshipMapping]];

        id source = dictionary[@"DocProcess"];
        if (source == nil) {
            return nil;
        }

        RKMappingOperation *operation = [[RKMappingOperation alloc] initWithSourceObject:source destinationObject:self mapping:docAPIMapping];
        RKObjectMappingOperationDataSource *mappingDS = [RKObjectMappingOperationDataSource new];
        operation.dataSource = mappingDS;

        if (![operation performMapping:nil]) {
            return nil;
        }
    }
    
    return self;
}

+ (void)prepareSessionStatusQuery:(PrintJobSessionItem *)item inBackground:(BOOL)background completionHandler:(void (^)(NSError *error))handler
{
    if (handler == nil) return;

    if (item.task != nil) {
        handler(nil);
        return;
    }

    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];

    // Get URL to use for Doc API from the printer
    NSURL *printerURL = [job.printer getDocAPIAddress];

    // Determine if the service we are searching against is using OAuth authentication
    NSString *serviceURL = [DocProcess getBaseURLForService:printerURL.absoluteString];
    BOOL isUsingOAuth = [ServiceCapabilities isServiceUsingOAuth:serviceURL];

    // Create the parameters dictionary
    NSMutableDictionary *apiParameters = [self parametersForJob:job withURL:printerURL];

    if (item.request == nil) {
        // Create the request
        item.request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:nil error:nil];
    } else {
        // Copy the original request so we do not need to re-create it entirely
        item.request = item.request.mutableCopy;
    }

    void (^operationBlock)(NSError *error) = ^(NSError *error) {
        // Cancel the operation if there was an error
        if (error) {
            handler(error);
            return;
        }

        // Strip the "userEmail" parameter if OAuth Bearer token was attached to the request
        NSString *authHeader = [item.request valueForHTTPHeaderField:@"Authorization"];
        if (authHeader && [authHeader hasPrefix:@"Bearer"]) {
            apiParameters[@"userEmail"] = nil;
            item.request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:nil error:nil];
            [item.request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }

        // Create the task
        NSURLSessionTask *uploadTask = nil;
        if (background) {
            NSString *path = [[job getDocumentPath] stringByAppendingString:@".status"];
            NSURL *fileURL = [NSURL URLWithString:path];
            [DirSearch outputStream:item.request.HTTPBodyStream.copy toFile:path];
            uploadTask = [item.session uploadTaskWithRequest:item.request fromFile:fileURL progress:nil completionHandler:nil];
        } else {
            uploadTask = [item.session dataTaskWithRequest:item.request completionHandler:nil];
        }

        item.task = uploadTask;

        handler(nil);
    };

    if (isUsingOAuth) {
        // We are using OAuth authentication so we need to authorize the request
        [OAuth2Manager authorizeRequest:item.request forService:serviceURL completionHandler:operationBlock];
    } else {
        // Not using OAuth so we can return the operation immediately
        operationBlock(nil);
    }
}

+ (NSMutableDictionary *)parametersForJob:(PrintJob *)job withURL:(NSURL *)url
{
    // Combine the required parameters with the passed parameters
    NSMutableDictionary *apiParameters = [[NSMutableDictionary alloc] initWithDictionary:[DocStatus requiredParameters]];
    apiParameters[@"jobReferenceID"] = job.referenceID;
    apiParameters[@"jobDestination"] = job.printer.printerID;
    apiParameters[@"blockTimeout"] = @"55000";

    // Add API key information if necessary for the set service
    apiParameters[@"clientSWKey"] = [Service isHostedURL:url andPublic:NO] && [Service isSecureURL:url] ? [Service hostData] : kApiKey;

    // Add User Information
    if (job.emailInput) {
        apiParameters[@"userEmail"] = [job retrieveJobInput:job.emailInput];
    } else {
        UserAccount *userAccount = [UserAccount getUserAccountForURL:url];
        apiParameters[@"userEmail"] = userAccount.userName;
    }

    return apiParameters;
}

- (NSString *)getJobAuthAddress
{
    NSSet *services = self.jobInteractionService;

    for (JobInteractionService *service in services) {
        if ([service.serviceUse.lowercaseString isEqualToString:@"required"] || [service.serviceUse.lowercaseString isEqualToString:@"optional"]) {
            return service.serviceURI;
        }
    }

    return nil;
}

+ (void)getPreviewStatus:(NSString *)jobID forPrinter:(Printer *)printer
                 success:(void (^)(DocStatus *status))success
                 failure:(void (^)(NSError *error))failure
       completionHandler:(void (^)(NSURLSessionTask *task, NSError *error))handler
{
    if (handler == nil) return;

    // Get URL to use for Doc API from the printer
    NSURL *printerURL = [printer getDocAPIAddress];

    // Setup the session manager
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    // Determine if the service we are searching against is using OAuth authentication
    NSString *serviceURL = [DocProcess getBaseURLForService:printerURL.absoluteString];
    BOOL isUsingOAuth = [ServiceCapabilities isServiceUsingOAuth:serviceURL];

    // Create the parameters dictionary
    NSMutableDictionary *apiParameters = [self parametersForPreview:jobID forPrinter:printer withURL:[NSURL URLWithString:serviceURL]];

    // Create the request
    __block NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:nil error:nil];

    void (^operationBlock)(NSError *error) = ^(NSError *error) {
        // Cancel the operation if there was an error
        if (error) {
            handler(nil, error);
            return;
        }

        // Strip the "userEmail" parameter if OAuth Bearer token was attached to the request
        NSString *authHeader = [request valueForHTTPHeaderField:@"Authorization"];
        if (authHeader && [authHeader hasPrefix:@"Bearer"]) {
            apiParameters[@"userEmail"] = nil;
            request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:nil error:nil];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }

        // Create the task
        NSURLSessionTask *uploadTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {

            if (error) {
                if (failure == nil) return;

                NSInteger statusCode = 0;
                if ([response isMemberOfClass:[NSHTTPURLResponse class]]) {
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                    statusCode = httpResponse.statusCode;
                }

                if (statusCode == 401) {
                    failure([NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : serviceURL }]);
                    return;
                }

                failure(error);
                return;
            }

            if ([responseObject isKindOfClass:[NSData class]]) {
                NSDictionary *responseDict = [XMLReader dictionaryForXMLData:responseObject error:&error];
                if (error == nil) {
                    DocStatus *info = [[DocStatus alloc] initWithDictionary:responseDict];
                    if (success) {
                        success(info);
                    }
                    return;
                }
            }

            if (failure) {
                failure([NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCannotParseResponse userInfo:nil]);
            }

        }];

        handler(uploadTask, nil);
    };

    if (isUsingOAuth) {
        // We are using OAuth authentication so we need to authorize the request
        [OAuth2Manager authorizeRequest:request forService:serviceURL completionHandler:operationBlock];
    } else {
        // Not using OAuth so we can return the operation immediately
        operationBlock(nil);
    }
}

+ (NSMutableDictionary *)parametersForPreview:(NSString *)jobID forPrinter:(Printer *)printer withURL:(NSURL *)url
{
    // Combine the required parameters with the passed parameters
    NSMutableDictionary *apiParameters = [[NSMutableDictionary alloc] initWithDictionary:[DocStatus requiredParameters]];
    apiParameters[@"clientAPIver"] = @"3.2.0";
    apiParameters[@"jobReferenceID"] = jobID;
    apiParameters[@"jobDestination"] = printer.printerID;
    apiParameters[@"blockTimeout"] = @"55000";

    // Add API key information if necessary for the set service
    apiParameters[@"clientSWKey"] = [Service isHostedURL:url andPublic:NO] && [Service isSecureURL:url] ? [Service hostData] : kApiKey;

    // Add User Information
    UserAccount *account = [UserAccount getUserAccountForURL:url];
    apiParameters[@"userEmail"] = account.userName;

    return apiParameters;
}

@end
