//
//  JobInteractionService.m
//  PrinterOn
//
//  Created by Mark Burns on 2/13/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "JobInteractionService.h"

#import "PrintJob.h"

@implementation JobInteractionService

#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"use": @"serviceUse",
                       @"type": @"serviceType",
                       @"uri": @"serviceURI",
                       @"action": @"serviceAction",
                       };
    });
    return attributes;
}

+ (RKRelationshipMapping *)getRelationshipMapping
{
    RKObjectMapping *aliasMapping = [RKObjectMapping mappingForClass:[JobInteractionService class]];
    [aliasMapping addAttributeMappingsFromDictionary:[JobInteractionService attributeMappings]];
    
    return [RKRelationshipMapping relationshipMappingFromKeyPath:@"jobProcessingServices.jobProcessingService" toKeyPath:@"jobInteractionService" withMapping:aliasMapping];
}

@end
