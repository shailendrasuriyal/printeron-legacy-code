//
//  JobInteractionService.h
//  PrinterOn
//
//  Created by Mark Burns on 2/13/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface JobInteractionService : NSObject

@property (nonatomic, strong) NSString * serviceType;
@property (nonatomic, strong) NSString * serviceURI;
@property (nonatomic, strong) NSString * serviceAction;
@property (nonatomic, strong) NSString * serviceUse;

+ (NSDictionary *)attributeMappings;
+ (RKRelationshipMapping *)getRelationshipMapping;

@end
