//
//  DocProcess.h
//  PrinterOn
//
//  Created by Mark Burns on 11/25/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class AFURLSessionManager, Printer, PrintJob, PrintJobSessionItem;

@interface DocProcess : NSObject

@property (nonatomic, strong) NSString *apiVersion;
@property (nonatomic, strong) NSString *swVersion;
@property (nonatomic, strong) NSString *jobDestination;
@property (nonatomic, strong) NSString *returnCode;
@property (nonatomic, strong) NSString *returnCodeReason;
@property (nonatomic, strong) NSString *jobComplete;
@property (nonatomic, strong) NSString *jobState;
@property (nonatomic, strong) NSString *jobStateText;
@property (nonatomic, strong) NSString *jobReferenceID;
@property (nonatomic, strong) NSString *jobReleaseCode;
@property (nonatomic, strong) NSString *userMessageID;
@property (nonatomic, strong) NSString *userMessage;
@property (nonatomic, strong) NSString *errText;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

+ (void)prepareSessionUpload:(PrintJobSessionItem *)job
                inBackground:(BOOL)background
           completionHandler:(void (^)(NSError *error))handler;

+ (void)prepareUploadInExtension:(PrintJob *)job
                     withSession:(AFURLSessionManager *)session
               completionHandler:(void (^)(NSURLSessionUploadTask *task, NSError *error))handler;

+ (void)PreviewDocument:(NSURL *)documentURI forPrinter:(Printer *)printer
                success:(void (^)(DocProcess *process))success
                failure:(void (^)(NSError *error))failure
      completionHandler:(void (^)(NSURLSessionTask *task, NSError *error))handler;

+ (NSString *)getBaseURLForService:(NSString *)serviceURL;

@end
