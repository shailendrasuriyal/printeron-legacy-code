//
//  DocProcess.m
//  PrinterOn
//
//  Created by Mark Burns on 11/25/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "DocProcess.h"

#import "DirSearch.h"
#import "OAuth2Manager.h"
#import "Printer.h"
#import "PrintJob.h"
#import "PrintJobSessionItem.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "UserAccount.h"

#import "AFNetworking.h"
#import "XMLReader.h"

@implementation DocProcess

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"DocAPIver": @"apiVersion",
                       @"docSWver": @"swVersion",
                       @"jobDestination.text": @"jobDestination",
                       @"returnCode.text": @"returnCode",
                       @"returnCodeReason.text": @"returnCodeReason",
                       @"jobComplete.text": @"jobComplete",
                       @"jobState.text": @"jobState",
                       @"jobStateText.text": @"jobStateText",
                       @"jobReferenceID.text": @"jobReferenceID",
                       @"jobReleaseCode.text": @"jobReleaseCode",
                       @"userMessageID.text": @"userMessageID",
                       @"userMessage.text": @"userMessage",
                       @"errText.text": @"errText"
                       };
    });
    return attributes;
}

+ (NSDictionary *)requiredParameters
{
    static NSDictionary *required = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        required = @{
                     @"docAPIfunc": @"DocPrint",
                     @"clientAPIver": @"3.1.1",
                     @"clientSWProtocol" : @"DocAPI",
                     @"clientSWName": @"PrinterOn-iOS",
                     @"clientSWVer": [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"] ?: @"",
                     @"userSWName": @"PrinterOn-iOS",
                     @"userLang": NSLocalizedPONString(@"USER_LANG", nil),
                     @"userLanguage": NSLocalizedPONString(@"USER_LANG", nil),
                     };
    });
    return required;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if (self = [super init]) {
        if (dictionary == nil || dictionary.count == 0) {
            return nil;
        }

        RKObjectMapping *docAPIMapping = [RKObjectMapping mappingForClass:[DocProcess class]];
        [docAPIMapping addAttributeMappingsFromDictionary:[DocProcess attributeMappings]];

        id source = dictionary[@"DocProcess"];
        if (source == nil) {
            return nil;
        }

        RKMappingOperation *operation = [[RKMappingOperation alloc] initWithSourceObject:source destinationObject:self mapping:docAPIMapping];

        if (![operation performMapping:nil]) {
            return nil;
        }
    }

    return self;
}

+ (void)prepareSessionUpload:(PrintJobSessionItem *)item inBackground:(BOOL)background completionHandler:(void (^)(NSError *error))handler
{
    if (handler == nil) return;

    if (item.task != nil) {
        handler(nil);
        return;
    }

    PrintJob *job = [PrintJob fetchPrintJobByUUID:item.jobUUID inContext:nil];

    // Get URL to use for Doc API from the printer
    NSURL *printerURL = [job.printer getDocAPIAddress];

    // Determine if the service we are searching against is using OAuth authentication
    NSString *serviceURL = [self getBaseURLForService:printerURL.absoluteString];
    BOOL isUsingOAuth = [ServiceCapabilities isServiceUsingOAuth:serviceURL];

    // Create the parameters dictionary
    NSMutableDictionary *apiParameters = [self parametersForJob:job withURL:[NSURL URLWithString:serviceURL] usesOAuth:isUsingOAuth];

    // Create the request
    NSString *docPath = [job getDocumentPath];
    NSURL *docURL = [NSURL URLWithString:docPath];
    __block NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileURL:docURL name:@"documentURI" error:nil];
    } error:nil];

    void (^operationBlock)(NSError *error) = ^(NSError *error) {
        // Cancel the operation if there was an error
        if (error) {
            handler(error);
            return;
        }

        // Strip the "userEmail" parameter if OAuth Bearer token was attached to the request
        NSString *authHeader = [request valueForHTTPHeaderField:@"Authorization"];
        if (authHeader && [authHeader hasPrefix:@"Bearer"]) {
            apiParameters[@"userEmail"] = nil;
            request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                [formData appendPartWithFileURL:docURL name:@"documentURI" error:nil];
            } error:nil];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }

        // Create the task
        NSURLSessionTask *uploadTask = nil;
        if (background) {
            NSURL *fileURL = [DirSearch outputStream:request.HTTPBodyStream.copy toFile:[docPath stringByAppendingString:@".upload"]];
            uploadTask = [item.session uploadTaskWithRequest:request fromFile:fileURL progress:nil completionHandler:nil];
        } else {
            uploadTask = [item.session dataTaskWithRequest:request completionHandler:nil];
        }

        item.progress = [item.session uploadProgressForTask:uploadTask];
        item.task = uploadTask;

        handler(nil);
    };

    if (isUsingOAuth) {
        // We are using OAuth authentication so we need to authorize the request
        [OAuth2Manager authorizeRequest:request forService:serviceURL completionHandler:operationBlock];
    } else {
        // Not using OAuth so we can return the operation immediately
        operationBlock(nil);
    }
}

+ (void)prepareUploadInExtension:(PrintJob *)job
                     withSession:(AFURLSessionManager *)session
               completionHandler:(void (^)(NSURLSessionUploadTask *task, NSError *error))handler
{
    if (handler == nil) return;

    // Get URL to use for Doc API from the printer
    NSURL *printerURL = [job.printer getDocAPIAddress];

    // Determine if the service we are searching against is using OAuth authentication
    NSString *serviceURL = [self getBaseURLForService:printerURL.absoluteString];
    BOOL isUsingOAuth = [ServiceCapabilities isServiceUsingOAuth:serviceURL];

    // Create the parameters dictionary
    NSMutableDictionary *apiParameters = [self parametersForJob:job withURL:[NSURL URLWithString:serviceURL] usesOAuth:isUsingOAuth];

    // Create the request
    __block NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL URLWithString:[job getDocumentPath]] name:@"documentURI" error:nil];
    } error:nil];

    void (^operationBlock)(NSError *error) = ^(NSError *error) {
        // Cancel the operation if there was an error
        if (error) {
            handler(nil, error);
            return;
        }

        // Strip the "userEmail" parameter if OAuth Bearer token was attached to the request
        NSString *authHeader = [request valueForHTTPHeaderField:@"Authorization"];
        if (authHeader && [authHeader hasPrefix:@"Bearer"]) {
            apiParameters[@"userEmail"] = nil;
            request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                [formData appendPartWithFileURL:[NSURL URLWithString:[job getDocumentPath]] name:@"documentURI" error:nil];
            } error:nil];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }

        NSURL *fileURL = [DirSearch outputStream:request.HTTPBodyStream.copy toFile:[[job getDocumentPath] stringByAppendingString:@".upload"]];
        
        // Create the task
        NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request fromFile:fileURL progress:nil completionHandler:nil];
        handler(uploadTask, nil);
    };

    if (isUsingOAuth) {
        // We are using OAuth authentication so we need to authorize the request
        [OAuth2Manager authorizeRequest:request forService:serviceURL completionHandler:operationBlock];
    } else {
        // Not using OAuth so we can return the operation immediately
        operationBlock(nil);
    }
}

+ (NSMutableDictionary *)parametersForJob:(PrintJob *)job withURL:(NSURL *)url usesOAuth:(BOOL)oauth
{
    // Combine the required parameters with the passed parameters
    NSMutableDictionary *apiParameters = [[NSMutableDictionary alloc] initWithDictionary:[self requiredParameters]];
    apiParameters[@"jobDestination"] = job.printer.printerID;
    [apiParameters addEntriesFromDictionary:job.printOptions];

    // Add API key information if necessary for the set service
    apiParameters[@"clientSWKey"] = [Service isHostedURL:url andPublic:NO] && [Service isSecureURL:url] ? [Service hostData] : kApiKey;

    // Add User Information
    if (job.emailInput) {
        apiParameters[@"userEmail"] = [job retrieveJobInput:job.emailInput];
    } else {
        UserAccount *userAccount = [UserAccount getUserAccountForURL:url];
        apiParameters[@"userEmail"] = userAccount.userName;

        // Add the user password if necessary
        if (!oauth && userAccount.hasPassword.boolValue) {
            apiParameters[@"userPassword"] = [userAccount getUserAccountPassword];
        } else if (oauth) {
            GTMOAuth2Authentication *auth = [userAccount getOAuth2AuthenticationForService:url.absoluteString];
            NSString *idToken = auth.parameters[@"id_token"];
            if (idToken) {
                apiParameters[@"IDToken"] = auth.parameters[@"id_token"];
            }
        }
    }

    // Add Job Accounting Inputs
    if (job.clientInput) apiParameters[@"clientUID"] = [job retrieveJobInput:job.clientInput];
    if (job.networkInput) apiParameters[@"networkLogin"] = [job retrieveJobInput:job.networkInput];
    if (job.sessionInput) apiParameters[@"sessionMetaData"] = [job retrieveJobInput:job.sessionInput];
    if (job.releaseInput) apiParameters[@"jobReleaseCode"] = job.releaseInput;

    return apiParameters;
}

+ (void)PreviewDocument:(NSURL *)documentURI forPrinter:(Printer *)printer
                success:(void (^)(DocProcess *process))success
                failure:(void (^)(NSError *error))failure
      completionHandler:(void (^)(NSURLSessionTask *task, NSError *error))handler
{
    if (handler == nil) return;

    // Get URL to use for Doc API from the printer
    NSURL *printerURL = [printer getDocAPIAddress];

    // Setup the session manager
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    // Determine if the service we are searching against is using OAuth authentication
    NSString *serviceURL = [self getBaseURLForService:printerURL.absoluteString];
    BOOL isUsingOAuth = [ServiceCapabilities isServiceUsingOAuth:serviceURL];

    // Create the parameters dictionary
    NSMutableDictionary *apiParameters = [self parametersForPreview:printer withURL:[NSURL URLWithString:serviceURL] usesOAuth:isUsingOAuth];

    // Create the request
    __block NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:documentURI name:@"documentURI" error:nil];
    } error:nil];

    void (^operationBlock)(NSError *error) = ^(NSError *error) {
        // Cancel the operation if there was an error
        if (error) {
            handler(nil, error);
            return;
        }

        // Strip the "userEmail" parameter if OAuth Bearer token was attached to the request
        NSString *authHeader = [request valueForHTTPHeaderField:@"Authorization"];
        if (authHeader && [authHeader hasPrefix:@"Bearer"]) {
            apiParameters[@"userEmail"] = nil;
            request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:printerURL.absoluteString parameters:apiParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                [formData appendPartWithFileURL:documentURI name:@"documentURI" error:nil];
            } error:nil];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }

        // Create the task
        NSURLSessionTask *uploadTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {

            if (error) {
                if (failure == nil) return;

                NSInteger statusCode = 0;
                if ([response isMemberOfClass:[NSHTTPURLResponse class]]) {
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                    statusCode = httpResponse.statusCode;
                }

                if (statusCode == 401) {
                    failure([NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : serviceURL }]);
                    return;
                }

                failure(error);
                return;
            }

            if ([responseObject isKindOfClass:[NSData class]]) {
                NSDictionary *responseDict = [XMLReader dictionaryForXMLData:responseObject error:&error];
                if (error == nil) {
                    DocProcess *info = [[DocProcess alloc] initWithDictionary:responseDict];
                    if (success) {
                        success(info);
                    }
                    return;
                }
            }

            if (failure) {
                failure([NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCannotParseResponse userInfo:nil]);
            }
        }];

        handler(uploadTask, nil);
    };

    if (isUsingOAuth) {
        // We are using OAuth authentication so we need to authorize the request
        [OAuth2Manager authorizeRequest:request forService:serviceURL completionHandler:operationBlock];
    } else {
        // Not using OAuth so we can return the operation immediately
        operationBlock(nil);
    }
}

+ (NSMutableDictionary *)parametersForPreview:(Printer *)printer withURL:(NSURL *)url usesOAuth:(BOOL)oauth
{
    // Combine the required parameters with the passed parameters
    NSMutableDictionary *apiParameters = [[NSMutableDictionary alloc] initWithDictionary:[self requiredParameters]];
    apiParameters[@"clientAPIver"] = @"3.2.0";
    apiParameters[@"docJobStage"] = @"Preview";
    apiParameters[@"jobDestination"] = printer.printerID;

    // Add API key information if necessary for the set service
    apiParameters[@"clientSWKey"] = [Service isHostedURL:url andPublic:NO] && [Service isSecureURL:url] ? [Service hostData] : kApiKey;

    // Add User Information
    UserAccount *account = [UserAccount getUserAccountForURL:url];
    apiParameters[@"userEmail"] = account.userName;
    if (!oauth && account.hasPassword.boolValue) {
        apiParameters[@"userPassword"] = [account getUserAccountPassword];
    } else if (oauth) {
        GTMOAuth2Authentication *auth = [account getOAuth2AuthenticationForService:url.absoluteString];
        NSString *idToken = auth.parameters[@"id_token"];
        if (idToken) {
            apiParameters[@"IDToken"] = auth.parameters[@"id_token"];
        }
    }

    return apiParameters;
}

+ (NSString *)getBaseURLForService:(NSString *)serviceURL
{
    if (serviceURL && serviceURL.length > 0) {
        NSURL *url = [NSURL URLWithString:serviceURL];
        if ([serviceURL.lastPathComponent.lowercaseString isEqualToString:@"docapi"]) {
            NSArray *pathComponents = url.pathComponents;
            if (pathComponents.count > 2) {
                url = [NSURL URLWithString:[NSString stringWithFormat:@"/%@",pathComponents[1]] relativeToURL:url];
            } else {
                url = [url URLByDeletingLastPathComponent];
            }
        }
        serviceURL = [Service cleanServiceURL:url.absoluteString];
    }
    return serviceURL;
}

@end
