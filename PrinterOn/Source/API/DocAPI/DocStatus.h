//
//  DocStatus.h
//  PrinterOn
//
//  Created by Mark Burns on 11/25/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class Printer, PrintJob, PrintJobSessionItem;

@interface DocStatus : NSObject

@property (nonatomic, strong) NSString *apiVersion;
@property (nonatomic, strong) NSString *swVersion;
@property (nonatomic, strong) NSString *returnCode;
@property (nonatomic, strong) NSString *returnCodeReason;
@property (nonatomic, strong) NSString *jobState;
@property (nonatomic, strong) NSString *jobStateText;
@property (nonatomic, strong) NSString *jobStateReason;
@property (nonatomic, strong) NSString *jobReferenceID;
@property (nonatomic, strong) NSString *jobKey;
@property (nonatomic, strong) NSString *jobDestination;
@property (nonatomic, strong) NSString *jobPageCount;
@property (nonatomic, strong) NSString *docPageCount;
@property (nonatomic, strong) NSString *documentURI;
@property (nonatomic, strong) NSString *userMessageID;
@property (nonatomic, strong) NSString *userMessage;
@property (nonatomic, strong) NSString *errText;
@property (nonatomic, strong) NSString *outputFormat;
@property (nonatomic, strong) NSString *outputURI;
@property (nonatomic, strong) NSString *outputFilePrefix;
@property (nonatomic, strong) NSSet *jobInteractionService;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

+ (void)prepareSessionStatusQuery:(PrintJobSessionItem *)item
                     inBackground:(BOOL)background
                completionHandler:(void (^)(NSError *error))handler;

- (NSString *)getJobAuthAddress;

+ (void)getPreviewStatus:(NSString *)jobID forPrinter:(Printer *)printer
                 success:(void (^)(DocStatus *status))success
                 failure:(void (^)(NSError *error))failure
       completionHandler:(void (^)(NSURLSessionTask *task, NSError *error))handler;

@end
