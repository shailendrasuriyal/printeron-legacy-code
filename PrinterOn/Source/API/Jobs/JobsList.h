//
//  JobsList.h
//  PrinterOn
//
//  Created by Mark Burns on 2017-05-08.
//  Copyright © 2017 PrinterOn Inc. All rights reserved.
//

@class Service;

@interface JobsList : NSObject

+ (void)jobsForServiceURL:(NSURL *)serviceURL startID:(NSString *)startID endID:(NSString *)endID maxResults:(NSString *)maxResults completionBlock:(void (^)(NSError *error, NSIndexSet *addedIDs))completionBlock;

@end
