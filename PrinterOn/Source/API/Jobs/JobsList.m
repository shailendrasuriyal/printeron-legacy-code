//
//  JobsList.m
//  PrinterOn
//
//  Created by Mark Burns on 2017-05-08.
//  Copyright © 2017 PrinterOn Inc. All rights reserved.
//

#import "JobsList.h"

#import "DirSearch.h"
#import "OAuth2Manager.h"
#import "PrintDocument.h"
#import "PrintJob.h"
#import "Printer.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "UserAccount.h"

#import "AFNetworking.h"

@implementation JobsList

+ (NSDateFormatter *)jobsDateFormatter
{
    static NSDateFormatter *dateFormat = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormat = [NSDateFormatter new];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        dateFormat.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return dateFormat;
}

+ (void)jobsForServiceURL:(NSURL *)serviceURL startID:(NSString *)startID endID:(NSString *)endID maxResults:(NSString *)maxResults completionBlock:(void (^)(NSError *error, NSIndexSet *addedIDs))completionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if (serviceURL == nil) {
            if (completionBlock) completionBlock([NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorBadURL userInfo:nil], nil);
            return;
        }

        // Create the parameters dictionary
        NSMutableDictionary *apiParameters = [NSMutableDictionary dictionary];

        // Set the start id, end id, and max results to query for jobs from the service
        if (startID) {
            apiParameters[@"startJobId"] = startID;
        }
        if (endID) {
            apiParameters[@"endJobId"] = endID;
        }
        if (maxResults) {
            apiParameters[@"maxResults"] = maxResults;
        }

        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];

        [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
            return [Service validateServerTrust:challenge withCredentials:credential];
        }];

        // Get the jobs URL from service capabilities
        NSString *serviceString = [Service cleanServiceURL:serviceURL.absoluteString];
        NSString *jobsURL = [ServiceCapabilities getJobsURLForService:serviceString];
        if (jobsURL == nil) {
            if (completionBlock) completionBlock([NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorBadURL userInfo:nil], nil);
            return;
        }

        // Create the request
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:jobsURL parameters:apiParameters error:nil];

        void (^operationBlock)(NSError *error) = ^(NSError *error) {
            // Cancel the operation if there was an error
            if (error) {
                if (completionBlock) completionBlock(error, nil);
                return;
            }

            // Create the task
            NSURLSessionTask *uploadTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {

                if (error) {
                    NSInteger statusCode = 0;
                    if ([response isMemberOfClass:[NSHTTPURLResponse class]]) {
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                        statusCode = httpResponse.statusCode;
                    }

                    if (statusCode == 401) {
                        error = [NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : serviceURL }];
                    }

                    if (completionBlock) completionBlock(error, nil);
                    return;
                }

                // Process the response into PrintJob objects to Core Data
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    NSArray *jsonArray = ((NSDictionary *)responseObject)[@"jobs"];
                    if (jsonArray.count > 0) {
                        [self processJobs:jsonArray fromService:serviceString completionBlock:completionBlock];
                        return;
                    }
                }

                if (completionBlock) completionBlock(nil, nil);
            }];

            // Start the task
            [uploadTask resume];
        };
    
        // Determine if the service we are searching against is using OAuth authentication
        BOOL isUsingOAuth = [ServiceCapabilities isServiceUsingOAuth:serviceString];

        if (isUsingOAuth) {
            // We are using OAuth authentication so we need to authorize the request
            [OAuth2Manager authorizeRequest:request forService:serviceString completionHandler:operationBlock];
        } else {
            // Not using OAuth so we try to get the user account for the supplied service
            UserAccount *userAccount = [UserAccount getUserAccountForURL:serviceURL];

            // Can't authorize using the anonymous user account
            if (userAccount.isAnonymous.boolValue) {
                if (completionBlock) completionBlock([NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorUserAuthenticationRequired userInfo:nil], nil);
                return;
            }

            // Set Basic Autohrization header for the user account on the request
            NSString *authString = [NSString stringWithFormat:@"%@:%@", userAccount.userName, [userAccount getUserAccountPassword]];
            NSData *data = [authString dataUsingEncoding:NSUTF8StringEncoding];
            NSString *value = [NSString stringWithFormat:@"Basic %@", [data base64EncodedStringWithOptions:0]];
            [request setValue:value forHTTPHeaderField:@"Authorization"];

            operationBlock(nil);
        }
    });
}

+ (void)processJobs:(NSArray *)jobs fromService:(NSString *)serviceURL completionBlock:(void (^)(NSError *error, NSIndexSet *addedIDs))completionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSEnumerator *jobsEnumerator = jobs.objectEnumerator;

        id firstJob = jobsEnumerator.nextObject;
        if (firstJob) {
            [self populateJob:firstJob inContext:nil fromService:serviceURL enumerator:jobsEnumerator jobIDs:nil completionBlock:completionBlock];
        } else {
            if (completionBlock) completionBlock(nil, nil);
        }
    });
}

+ (void)populateJob:(NSDictionary *)jobDict inContext:(NSManagedObjectContext *)context fromService:(NSString *)serviceURL enumerator:(NSEnumerator *)enumerator jobIDs:(NSMutableIndexSet *)idSet completionBlock:(void (^)(NSError *error, NSIndexSet *addedIDs))completionBlock
{
    // Create a temporary child context so that we create the print jobs in one transaction that will not be
    // affected by other saves to the main context.
    if (context == nil) {
        context = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:YES];
        context.undoManager = nil;
    }

    NSMutableIndexSet *jobIDs = (idSet == nil) ? [NSMutableIndexSet indexSet] : idSet;

    void (^operationBlock)(NSEnumerator *jobEnum) = ^(NSEnumerator *jobEnum) {
        id nextJob = jobEnum.nextObject;
        if (nextJob) {
            [self populateJob:nextJob inContext:context fromService:serviceURL enumerator:jobEnum jobIDs:jobIDs completionBlock:completionBlock];
        } else {
            NSError *error = nil;
            [context saveToPersistentStore:&error];
            if (completionBlock) completionBlock(nil, jobIDs);
        }
    };

    if (jobDict && [jobDict isKindOfClass:[NSDictionary class]]) {
        NSNumber *jobState = [self getJobStateFromDictionary:jobDict];
        NSString *jobReferenceID = [self getJobReferenceIDFromDictionary:jobDict];
        NSString *printerID = [self getJobSubmissionPrinterIDFromDictionary:jobDict];

        [self getJobPrinterForPrinterID:printerID inContext:context fromService:serviceURL completionHandler:^(Printer *printer) {

            if ((jobState.intValue == 4 || jobState.intValue == 7 || jobState.intValue == 8 || jobState.intValue == 9) && jobReferenceID && printer && ![self doesPrintJobExistWithID:jobReferenceID forService:serviceURL inContext:context]) {

                PrintJob *printJob = [NSEntityDescription insertNewObjectForEntityForName:@"PrintJob" inManagedObjectContext:context];

                printJob.dateTime = [[self jobsDateFormatter] dateFromString:jobDict[@"jobStartTime"]];
                printJob.docType = @(DocumentTypeFile);
                printJob.jobState = jobState;
                [printJob setJobStatusFromJobState];
                printJob.documentURI = [self getJobDocumentURIFromDictionary:jobDict];
                printJob.jobPageCount = jobDict[@"jobImpressionCount"];
                printJob.referenceID = jobReferenceID;
                printJob.releaseCode = [self getJobReleaseCodeFromDictionary:jobDict];
                printJob.printer = printer;

                //printJob.clientInput = jobDict[@"clientUID"];
                //printJob.emailInput = jobDict[@"userEmail"];
                //printJob.networkInput = jobDict[@"jobOwner"];
                //printJob.sessionInput = jobDict[@"sessionMetaData"];

                [jobIDs addIndex:[self getJobIDFromDictionary:jobDict].unsignedIntegerValue];
            }

            operationBlock(enumerator);
        }];
    } else {
        operationBlock(enumerator);
    }
}

+ (BOOL)doesPrintJobExistWithID:(NSString *)refid forService:(NSString *)serviceURL inContext:(NSManagedObjectContext *)context
{
    if (refid == nil) return NO;

    __block PrintJob *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [PrintJob fetchRequest];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"referenceID == '%@'", refid]];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        for (PrintJob *job in results) {
            NSURL *docApiURL = (job.printer) ? [job.printer getDocAPIAddress] : nil;
            if (docApiURL) {
                NSString *docApiString = [Service cleanServiceURL:docApiURL.absoluteString];
                if ([docApiString.lowercaseString hasPrefix:serviceURL.lowercaseString]) {
                    result = job;
                    break;
                }
            }
        }
    }];

    return result != nil;
}

+ (NSNumber *)getJobIDFromDictionary:(NSDictionary *)dict
{
    id jobID = dict[@"id"];
    NSNumber *jobIDNumber = nil;
    if ([jobID isKindOfClass:[NSNumber class]]) {
        jobIDNumber = jobID;
    } else if ([jobID isKindOfClass:[NSString class]]) {
        jobIDNumber = [NSNumber numberWithInt:((NSString *)jobID).intValue];
    }
    return jobIDNumber;
}

+ (NSNumber *)getJobStateFromDictionary:(NSDictionary *)dict
{
    id jobState = dict[@"jobState"];
    NSNumber *jobStateNumber = nil;
    if ([jobState isKindOfClass:[NSNumber class]]) {
        jobStateNumber = jobState;
    } else if ([jobState isKindOfClass:[NSString class]]) {
        jobStateNumber = [NSNumber numberWithInt:((NSString *)jobState).intValue];
    }
    return jobStateNumber;
}

+ (NSString *)getJobReferenceIDFromDictionary:(NSDictionary *)dict
{
    id jobReferenceID = dict[@"jobReferenceId"];
    NSString *strReferenceID = nil;
    if ([jobReferenceID isKindOfClass:[NSNumber class]]) {
        strReferenceID = ((NSNumber *)jobReferenceID).stringValue;
    } else if ([jobReferenceID isKindOfClass:[NSString class]]) {
        strReferenceID = jobReferenceID;
    }
    return strReferenceID;
}

+ (NSString *)getJobReleaseCodeFromDictionary:(NSDictionary *)dict
{
    id jobReleaseCode = dict[@"releaseCode"];
    NSString *strReleaseCode = nil;
    if ([jobReleaseCode isKindOfClass:[NSNumber class]]) {
        strReleaseCode = ((NSNumber *)jobReleaseCode).stringValue;
    } else if ([jobReleaseCode isKindOfClass:[NSString class]]) {
        strReleaseCode = jobReleaseCode;
    }
    return strReleaseCode;
}

+ (NSString *)getJobDocumentURIFromDictionary:(NSDictionary *)dict
{
    id jobName = dict[@"jobName"];
    if (jobName == nil) {
        jobName = dict[@"documentName"];
    }

    NSString *strJobName = nil;
    if ([jobName isKindOfClass:[NSString class]]) {
        strJobName = jobName;
    }
    return strJobName;
}

+ (NSString *)getJobSubmissionPrinterIDFromDictionary:(NSDictionary *)dict
{
    id printerID = dict[@"submissionDestinationId"];
    NSString *strPrinterID = nil;
    if ([printerID isKindOfClass:[NSNumber class]]) {
        strPrinterID = ((NSNumber *)printerID).stringValue;
    } else if ([printerID isKindOfClass:[NSString class]]) {
        strPrinterID = printerID;
    }
    return strPrinterID;
}

+ (void)getJobPrinterForPrinterID:(NSString *)printerID inContext:(NSManagedObjectContext *)context fromService:(NSString *)serviceURL completionHandler:(void (^)(Printer *printer))handler
{
    if (printerID == nil) {
        if (handler) {
            handler(nil);
        }
        return;
    };

    __block Printer *result = nil;
    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"JobPrinter" inManagedObjectContext:context];
        fetchRequest.entity = entity;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(printerID == %@)", printerID];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        }
    }];

    if (result == nil) {
        [self performDirSearchForPrinter:printerID inContext:context fromService:serviceURL completionHandler:handler];
    } else {
        if (handler) handler(result);
    }
}

+ (void)performDirSearchForPrinter:(NSString *)printerID inContext:(NSManagedObjectContext *)context fromService:(NSString *)serviceURL completionHandler:(void (^)(Printer *printer))handler {

    RKObjectManager *searchObjectManager = [DirSearch setupSearchForEntity:@"QRPrinter" withService:serviceURL];

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                        @"searchType": @"searchByPrinterNum",
                                        @"searchPrinterNum": printerID ?: @"",
                                        @"showChildren": @"1",
                                        @"maxResults": @"1",
                                }];

    NSManagedObjectContext *tempObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:YES];
    tempObjectContext.undoManager = nil;

    [DirSearch createSearchWithParameters:params objectManager:searchObjectManager managedObjectContext:tempObjectContext success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {

        Printer *foundPrinter = nil;

        for (id value in [mappingResult array]) {
            if ([value isMemberOfClass:[DirSearch class]]) {
                DirSearch *info = value;

                // The result is actually an error so send to failure
                if (![info.returnCode isEqualToString:@"0"]) {
                    if (handler) handler(nil);
                    return;
                }

                if (info.resultCount > 0) {
                    [Printer connectRelationshipsFromMapping:mappingResult.dictionary inContext:context];

                    for (id printer in [mappingResult array]) {
                        if ([printer isKindOfClass:[Printer class]] && [printer isKindOfClass:[NSManagedObject class]]) {
                            NSManagedObject *managedObject = (NSManagedObject *)printer;
                            if ([managedObject.entity.name isEqualToString:@"ParentPrinter"]) continue;

                            foundPrinter = [printer clonePrinterInContext:context forEntityName:@"JobPrinter" updateExisting:YES];
                            break;
                        }
                    }
                }

                break;
            }
        }

        if (handler) {
            handler(foundPrinter);
        }

    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (handler) {
            handler(nil);
        }
    } completionHandler:^(RKManagedObjectRequestOperation *operation, NSError *error) {
        operation.savesToPersistentStore = NO;
        [[RKObjectManager sharedManager] enqueueObjectRequestOperation:operation];
    }];
}

@end
