//
//  RemoteRelease.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-01-05.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@interface RemoteRelease : NSObject

+ (void)remoteReleaseCMD:(NSURL *)url withParameters:(NSDictionary *)parameters success:(void (^)())success failure:(void (^)(NSError *error))failure;
+ (void)remoteReleaseAPI:(NSURL *)url withParameters:(NSDictionary *)parameters success:(void (^)())success failure:(void (^)(NSError *error))failure;

@end
