//
//  RemoteRelease.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-01-05.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "RemoteRelease.h"

#import "Service.h"

#import "AFNetworking.h"

@implementation RemoteRelease

+ (void)remoteReleaseCMD:(NSURL *)url withParameters:(NSDictionary *)parameters success:(void (^)())success failure:(void (^)(NSError *error))failure
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    [manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [RemoteRelease redirectReleaseCMD:task.response.URL withParameters:parameters success:success failure:failure];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        failure(error);
    }];
}

+ (void)redirectReleaseCMD:(NSURL *)url withParameters:(NSDictionary *)parameters success:(void (^)())success failure:(void (^)(NSError *error))failure
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    NSString *baseURLString = [[NSURL alloc] initWithScheme:url.scheme host:url.host path:url.path].absoluteString;

    [manager GET:baseURLString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        success();
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        failure(error);
    }];
}

+ (void)remoteReleaseAPI:(NSURL *)url withParameters:(NSDictionary *)parameters success:(void (^)())success failure:(void (^)(NSError *error))failure
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    [manager POST:url.absoluteString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        success();
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

@end
