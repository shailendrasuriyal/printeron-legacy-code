//
//  DirSearch.m
//  PrinterOn
//
//  Created by Mark Burns on 10/29/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "DirSearch.h"

#import "OAuth2Manager.h"
#import "Printer.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "UserAccount.h"

// Dummy key offered up unprotected as a decoy
NSString* const kApiKey = @"32dzADFmjla4n73RI7WD1ii528rjddR9";

@implementation DirSearch

#pragma mark - RestKit Mappings

+ (NSDictionary *)attributeMappings
{
    static NSDictionary *attributes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        attributes = @{
                       @"ponAPIver": @"apiVersion",
                       @"ponSWver": @"swVersion",
                       @"resultCount.text": @"resultCount",
                       @"totalCount.text": @"totalCount",
                       @"returnCode.text": @"returnCode",
                       @"userMessage.text": @"userMessage",
                       @"errText.text": @"errText"
                       };
    });
    return attributes;
}

+ (NSDictionary *)requiredParameters
{
    static NSDictionary *required = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        required = @{
                     @"ponAPIfunc": @"DirSearch",
                     @"clientAPIver": @"1.4.4",
                     @"clientSWName": @"PrinterOn-iOS",
                     @"clientSWVer": [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"]  ?: @"",
                     @"userSWName": @"PrinterOn-iOS",
                     @"searchAccessClass": @"all",
                     @"searchSiteClass": @"all",
                     @"searchType": @"searchByCriteria",
                     @"searchSubmitType": @"DocAPI",
                     @"showChildren": @"0",
                     @"reportDetail": @"9",
                     @"userLang": NSLocalizedPONString(@"USER_LANG", nil),
                     @"userLanguage": NSLocalizedPONString(@"USER_LANG", nil),
                     };
    });
    return required;
}

#pragma mark - DirSearch Methods

+ (RKObjectManager *)setupSearchForEntity:(NSString *)entity
{
    return [DirSearch setupSearchForEntity:entity withService:nil];
}

+ (RKObjectManager *)setupSearchForEntity:(NSString *)entity
                              withService:(NSString *)serviceURL
{
    // Determine the service URL
    serviceURL = [self getBaseURLForService:serviceURL];

    // Get the search URL from service capabilities
    NSString *searchURL = [ServiceCapabilities getDirectorySearchURLForService:serviceURL];

    // Setup baseURL and path
    NSURL *baseURL = [NSURL URLWithString:searchURL];
    NSString *path = baseURL.path;

    // Format the baseURL
    NSString *baseURLString = [NSURL URLWithString:@"/" relativeToURL:baseURL].absoluteString.lowercaseString;
    if ([path hasPrefix:@"/"] && [baseURLString hasSuffix:@"/"]) baseURLString = [baseURLString substringToIndex:baseURLString.length - 1];
    baseURL = [NSURL URLWithString:baseURLString];

    // Setup HTTP client with base URL and to POST using url form encoding
    AFRKHTTPClient *httpClient = [AFRKHTTPClient clientWithBaseURL:baseURL];
    httpClient.parameterEncoding = AFRKFormURLParameterEncoding;

    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:httpClient];
    objectManager.managedObjectStore = [RKManagedObjectStore defaultStore];
    [objectManager setAcceptHeaderWithMIMEType:RKMIMETypeTextXML];

    // Create Mappings and add them to the Object Store
    RKEntityMapping *printerMapping = [Printer getEntityMappingForName:entity inStore:objectManager.managedObjectStore];
    RKEntityMapping *parentMapping = [Printer getEntityMappingForName:@"ParentPrinter" inStore:objectManager.managedObjectStore];

    RKObjectMapping *dirSearchMapping = [RKObjectMapping mappingForClass:[DirSearch class]];
    [dirSearchMapping addAttributeMappingsFromDictionary:[DirSearch attributeMappings]];

    RKResponseDescriptor *dirSearchDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:dirSearchMapping method:RKRequestMethodPOST pathPattern:path keyPath:@"DirSearch" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    RKResponseDescriptor *printerDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:printerMapping method:RKRequestMethodPOST pathPattern:path keyPath:@"DirSearch.printer" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    RKResponseDescriptor *parentDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:parentMapping method:RKRequestMethodPOST pathPattern:path keyPath:@"DirSearch.parent" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];

    [objectManager addResponseDescriptorsFromArray:@[dirSearchDescriptor, printerDescriptor, parentDescriptor]];

    return objectManager;
}

+ (RKObjectManager *)setupHeaderSearchWithService:(NSString *)serviceURL
{
    // Determine the service URL
    serviceURL = [self getBaseURLForService:serviceURL];

    // Get the search URL from service capabilities
    NSString *searchURL = [ServiceCapabilities getDirectorySearchURLForService:serviceURL];

    // Setup baseURL and path
    NSURL *baseURL = [NSURL URLWithString:searchURL];
    NSString *path = baseURL.path;

    // Format the baseURL
    NSString *baseURLString = [NSURL URLWithString:@"/" relativeToURL:baseURL].absoluteString.lowercaseString;
    if ([path hasPrefix:@"/"] && [baseURLString hasSuffix:@"/"]) baseURLString = [baseURLString substringToIndex:baseURLString.length - 1];
    baseURL = [NSURL URLWithString:baseURLString];

    // Setup HTTP client with base URL and to POST using url form encoding
    AFRKHTTPClient *httpClient = [AFRKHTTPClient clientWithBaseURL:baseURL];
    httpClient.parameterEncoding = AFRKFormURLParameterEncoding;

    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:httpClient];
    objectManager.managedObjectStore = [RKManagedObjectStore defaultStore];
    [objectManager setAcceptHeaderWithMIMEType:RKMIMETypeTextXML];

    // Create Mappings and add them to the Object Store
    RKObjectMapping *dirSearchMapping = [RKObjectMapping mappingForClass:[DirSearch class]];
    [dirSearchMapping addAttributeMappingsFromDictionary:[DirSearch attributeMappings]];

    RKResponseDescriptor *dirSearchDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:dirSearchMapping method:RKRequestMethodPOST pathPattern:path keyPath:@"DirSearch" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];

    [objectManager addResponseDescriptorsFromArray:@[dirSearchDescriptor]];

    return objectManager;
}

+ (void)createSearchWithParameters:(NSDictionary *)parameters
                     objectManager:(RKObjectManager *)manager
              managedObjectContext:(NSManagedObjectContext *)context
                           success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                           failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure
                 completionHandler:(void (^)(RKManagedObjectRequestOperation *operation, NSError *error))handler
{
    if (handler == nil) return;

    // Combine the required parameters with the passed parameters
    NSMutableDictionary *searchParameters = [[NSMutableDictionary alloc] initWithDictionary:[DirSearch requiredParameters]];
    [searchParameters addEntriesFromDictionary:parameters];

    // Add API key information if necessary for the set service
    searchParameters[@"clientSWKey"] = [Service isHostedURL:manager.HTTPClient.baseURL andPublic:NO] && [Service isSecureURL:manager.HTTPClient.baseURL] ? [Service hostData] : kApiKey;

    // Add User information if necessary for the set service
    UserAccount *account = [UserAccount getUserAccountForURL:manager.HTTPClient.baseURL];
    searchParameters[@"userEmail"] = account.userName;

    // Get the first response descriptor to obtain the path pattern from
    RKResponseDescriptor *descriptor = manager.responseDescriptors[0];

    // Determine if the service we are searching against is using OAuth authentication
    NSURL *newURL = [manager.HTTPClient.baseURL URLByAppendingPathComponent:descriptor.pathPattern];
    NSString *serviceURL = [self getBaseURLForService:newURL.absoluteString];
    BOOL isUsingOAuth = [ServiceCapabilities isServiceUsingOAuth:serviceURL];

    // Add the user password if necessary
    if (!isUsingOAuth && account.hasPassword.boolValue) {
        searchParameters[@"userPassword"] = [account getUserAccountPassword];
    };

    // Create the request
    __block NSMutableURLRequest *request = [manager requestWithObject:nil method:RKRequestMethodPOST path:descriptor.pathPattern parameters:searchParameters];

    void (^operationBlock)(NSError *error) = ^(NSError *error) {
        // Cancel the operation if there was an error
        if (error) {
            handler(nil, error);
            return;
        }

        // Strip the "userEmail" parameter if OAuth Bearer token was attached to the request
        NSString *authHeader = [request valueForHTTPHeaderField:@"Authorization"];
        if (authHeader && [authHeader hasPrefix:@"Bearer"]) {
            searchParameters[@"userEmail"] = nil;
            request = [manager requestWithObject:nil method:RKRequestMethodPOST path:descriptor.pathPattern parameters:searchParameters];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }

        // Create the operation
        RKManagedObjectRequestOperation *operation = [manager managedObjectRequestOperationWithRequest:request managedObjectContext:context success:success failure:^(RKObjectRequestOperation *operation, NSError *error) {

            if ([error.domain isEqualToString:RKErrorDomain] && error.code == -1011) {
                NSInteger statusCode = 0;
                if (error.userInfo && [error.userInfo[@"AFRKNetworkingOperationFailingURLResponseErrorKey"] isMemberOfClass:[NSHTTPURLResponse class]]) {
                    NSHTTPURLResponse *response = error.userInfo[@"AFRKNetworkingOperationFailingURLResponseErrorKey"];
                    statusCode = response.statusCode;
                }

                if (statusCode == 401) {
                    failure(operation, [NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : serviceURL }]);
                    return;
                }
            }
            failure(operation, error);
        }];

        [operation.HTTPRequestOperation setWillSendRequestForAuthenticationChallengeBlock:^(NSURLConnection *connection, NSURLAuthenticationChallenge *challenge) {
            [Service validateServerTrust:challenge];
        }];
        [operation.HTTPRequestOperation setShouldExecuteAsBackgroundTaskWithExpirationHandler:nil];

        // Return the operation to the completion handler block
        handler(operation, nil);
    };

    if (isUsingOAuth) {
        // We are using OAuth authentication so we need to authorize the request
        [OAuth2Manager authorizeRequest:request forService:serviceURL completionHandler:operationBlock];
    } else {
        // Not using OAuth so we can return the operation immediately
        operationBlock(nil);
    }
}

+ (void)createHeaderSearchWithParameters:(NSDictionary *)parameters
                           objectManager:(RKObjectManager *)manager
                                 success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                                 failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure
                       completionHandler:(void (^)(RKObjectRequestOperation *operation, NSError *error))handler
{
    if (handler == nil) return;

    // Combine the required parameters with the passed parameters
    NSMutableDictionary *searchParameters = [[NSMutableDictionary alloc] initWithDictionary:[DirSearch requiredParameters]];
    [searchParameters addEntriesFromDictionary:parameters];
    searchParameters[@"reportDetail"] = @"1";

    // Add API key information if necessary for the set service
    searchParameters[@"clientSWKey"] = [Service isHostedURL:manager.HTTPClient.baseURL andPublic:NO] && [Service isSecureURL:manager.HTTPClient.baseURL] ? [Service hostData] : kApiKey;

    // Add User information if necessary for the set service
    UserAccount *account = [UserAccount getUserAccountForURL:manager.HTTPClient.baseURL];
    searchParameters[@"userEmail"] = account.userName;

    // Get the first response descriptor to obtain the path pattern from
    RKResponseDescriptor *descriptor = manager.responseDescriptors[0];

    // Determine if the service we are searching against is using OAuth authentication
    NSURL *newURL = [manager.HTTPClient.baseURL URLByAppendingPathComponent:descriptor.pathPattern];
    NSString *serviceURL = [self getBaseURLForService:newURL.absoluteString];
    BOOL isUsingOAuth = [ServiceCapabilities isServiceUsingOAuth:serviceURL];

    // Add the user password if necessary
    if (!isUsingOAuth && account.hasPassword.boolValue) {
        searchParameters[@"userPassword"] = [account getUserAccountPassword];
    };

    // Create the request
    __block NSMutableURLRequest *request = [manager requestWithObject:nil method:RKRequestMethodPOST path:descriptor.pathPattern parameters:searchParameters];

    void (^operationBlock)(NSError *error) = ^(NSError *error) {
        // Cancel the operation if there was an error
        if (error) {
            handler(nil, error);
            return;
        }

        // Strip the "userEmail" parameter if OAuth Bearer token was attached to the request
        NSString *authHeader = [request valueForHTTPHeaderField:@"Authorization"];
        if (authHeader && [authHeader hasPrefix:@"Bearer"]) {
            searchParameters[@"userEmail"] = nil;
            request = [manager requestWithObject:nil method:RKRequestMethodPOST path:descriptor.pathPattern parameters:searchParameters];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }

        // Create the operation
        RKObjectRequestOperation *operation = [manager objectRequestOperationWithRequest:request success:success failure:^(RKObjectRequestOperation *operation, NSError *error) {

            if ([error.domain isEqualToString:RKErrorDomain] && error.code == -1011) {
                NSInteger statusCode = 0;
                if (error.userInfo && [error.userInfo[@"AFRKNetworkingOperationFailingURLResponseErrorKey"] isMemberOfClass:[NSHTTPURLResponse class]]) {
                    NSHTTPURLResponse *response = error.userInfo[@"AFRKNetworkingOperationFailingURLResponseErrorKey"];
                    statusCode = response.statusCode;
                }

                if (statusCode == 401) {
                    failure(operation, [NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:@{ @"serviceURL" : serviceURL }]);
                    return;
                }
            }
            failure(operation, error);
        }];

        [operation.HTTPRequestOperation setWillSendRequestForAuthenticationChallengeBlock:^(NSURLConnection *connection, NSURLAuthenticationChallenge *challenge) {
            [Service validateServerTrust:challenge];
        }];
        [operation.HTTPRequestOperation setShouldExecuteAsBackgroundTaskWithExpirationHandler:nil];

        handler(operation, nil);
    };

    if (isUsingOAuth) {
        // We are using OAuth authentication so we need to authorize the request
        [OAuth2Manager authorizeRequest:request forService:serviceURL completionHandler:operationBlock];
    } else {
        // Not using OAuth so we can return the operation immediately
        operationBlock(nil);
    }
}

+ (NSString *)getBaseURLForService:(NSString *)serviceURL
{
    if (serviceURL && serviceURL.length > 0) {
        NSURL *url = [NSURL URLWithString:serviceURL];
        if ([serviceURL.lastPathComponent.lowercaseString isEqualToString:@"dirsearch"]) {
            NSArray *pathComponents = url.pathComponents;
            if (pathComponents.count > 2) {
                url = [NSURL URLWithString:[NSString stringWithFormat:@"/%@",pathComponents[1]] relativeToURL:url];
            } else {
                url = [url URLByDeletingLastPathComponent];
            }
        }
        return [Service cleanServiceURL:url.absoluteString];
    } else {
        Service *service = [Service getDefaultService];
        return service.serviceURL;
    }
}

+ (NSURL *)outputStream:(NSInputStream *)stream toFile:(NSString *)file
{
    NSURL *fileURL = [NSURL URLWithString:file];
    
    NSOutputStream *output = [NSOutputStream outputStreamToFileAtPath:fileURL.path append:NO];
    
    [output open];
    [stream open];
    
    if (stream.streamStatus != NSStreamStatusOpen) {
        [stream close];
        [output close];
        return nil;
    }
    
    while (stream.hasBytesAvailable) {
        uint8_t buffer[4096];
        NSInteger readCount = [stream read:buffer maxLength:4096];
        if (readCount <= 0) {
            break;
        } else {
            [output write:buffer maxLength:readCount];
        }
    }
    
    [stream close];
    [output close];
    
    return fileURL;
}

@end
