//
//  DirSearch.h
//  PrinterOn
//
//  Created by Mark Burns on 10/29/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

FOUNDATION_EXPORT NSString* const kApiKey;

@interface DirSearch : NSObject

@property (nonatomic, strong) NSString *apiVersion;
@property (nonatomic, strong) NSString *swVersion;
@property (nonatomic, assign) int resultCount;
@property (nonatomic, assign) int totalCount;
@property (nonatomic, strong) NSString *returnCode;
@property (nonatomic, strong) NSString *userMessage;
@property (nonatomic, strong) NSString *errText;

+ (RKObjectManager *)setupSearchForEntity:(NSString *)entity;
+ (RKObjectManager *)setupSearchForEntity:(NSString *)entity
                              withService:(NSString *)serviceURL;
+ (RKObjectManager *)setupHeaderSearchWithService:(NSString *)serviceURL;
+ (void)createSearchWithParameters:(NSDictionary *)parameters
                     objectManager:(RKObjectManager *)manager
              managedObjectContext:(NSManagedObjectContext *)context
                           success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                           failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure
                 completionHandler:(void (^)(RKManagedObjectRequestOperation *operation, NSError *error))handler;
+ (void)createHeaderSearchWithParameters:(NSDictionary *)parameters
                           objectManager:(RKObjectManager *)manager
                                 success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                                 failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure
                       completionHandler:(void (^)(RKObjectRequestOperation *operation, NSError *error))handler;

+ (NSURL *)outputStream:(NSInputStream *)stream toFile:(NSString *)file;

@end
