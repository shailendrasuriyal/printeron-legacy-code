//
//  ServiceDescription.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-04-13.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "ServiceDescription.h"

#import "Service.h"
#import "ServiceAccess.h"
#import "ServiceCapabilities.h"
#import "ServiceCapabilitiesFetched.h"
#import "ServiceLinks.h"

#import "AFNetworking.h"

@implementation ServiceDescription

+ (void)capabilitiesForServiceWithURL:(NSURL *)url forceUpdate:(BOOL)force completionBlock:(void (^)(NSManagedObjectID *capabilitiesID))completionBlock
{
    // Get Service URL
    NSString *serviceURL = [Service cleanServiceURL:url.absoluteString];

    // Check to see if capabilities have been fetched and updated already in the last 5 minutes.  If they have we don't
    // fetch them again.  If forceUpdate is set to YES we skip this and perform the fetch to update capabilities.
    NSDate *lastFetched = [ServiceCapabilitiesFetched getLastFetchForService:serviceURL inContext:nil];
    if (!force && lastFetched && [lastFetched timeIntervalSinceNow] >= -300) {
        if (completionBlock) {
            ServiceCapabilities *capabilities = [ServiceCapabilities getCapabilitiesForService:serviceURL inContext:nil];
            completionBlock(capabilities.objectID);
        }
        return;
    }

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
    [manager.requestSerializer setTimeoutInterval:30]; // Set timeout to 30 seconds

    [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    NSURL *baseURL = [url URLByAppendingPathComponent:[Service isHostedString:serviceURL andPublic:NO] ? @"servicedescription" : @"restapi/servicedescription"];

    [manager GET:baseURL.absoluteString parameters:@{ @"clientAPIver": @"2.0" } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [ServiceCapabilitiesFetched updateLastFetchedForServiceNow:serviceURL inContext:nil];
        NSManagedObjectID *capID = [self processCapabilitiesResponse:responseObject fromServiceURL:serviceURL];

        if (completionBlock) {
            completionBlock(capID);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSManagedObjectID *capID = nil;
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)operation.response;
        if (response.statusCode == 0) {
            capID = [self failureCapabilitiesFromServiceURL:url.absoluteString];
        } else {
            [ServiceCapabilitiesFetched updateLastFetchedForServiceNow:serviceURL inContext:nil];
        }

        if (completionBlock) {
            completionBlock(capID);
        }
    }];
}

+ (NSManagedObjectID *)processCapabilitiesResponse:(id)JSON fromServiceURL:(NSString *)serviceURL
{
    if (![JSON isKindOfClass:[NSDictionary class]]) return nil;

    NSDictionary *dict = (NSDictionary *)JSON;

    NSString *version = [dict[@"version"] isKindOfClass:[NSString class]] ? dict[@"version"] : nil;
    if (version == nil || [self isVersion:version lessThan:@"3.2.4"]) return nil;

    // Private scratch context
    NSManagedObjectContext *context = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
    context.undoManager = nil;

    // Fetch any existing capabilities for this service URL
    BOOL existingCapabilities = NO;
    ServiceCapabilities *capabilities = [ServiceCapabilities getCapabilitiesForService:serviceURL inContext:context];
    if (capabilities) {
        existingCapabilities = YES;
    } else {
        capabilities = [NSEntityDescription insertNewObjectForEntityForName:@"ServiceCapabilities" inManagedObjectContext:context];
    }

    NSArray *services = [dict[@"services"] isKindOfClass:[NSArray class]] ? dict[@"services"] : nil;
    if (services.count == 0) {
        if (existingCapabilities) {
            [context deleteObject:capabilities];
            [context saveToPersistentStore:nil];
        }
        return nil;
    }

    NSDictionary *serviceDict = [services.firstObject isKindOfClass:[NSDictionary class]] ? services.firstObject : nil;
    if (serviceDict == nil) {
        if (existingCapabilities) {
            [context deleteObject:capabilities];
            [context saveToPersistentStore:nil];
        }
        return nil;
    }

    NSDictionary *capDict = [serviceDict[@"capabilities"] isKindOfClass:[NSDictionary class]] ? serviceDict[@"capabilities"] : nil;
    if (capDict == nil) {
        if (existingCapabilities) {
            [context deleteObject:capabilities];
            [context saveToPersistentStore:nil];
        }
        return nil;
    }

    capabilities.serviceURL = serviceURL;
    capabilities.version = version;
    capabilities.company = [serviceDict[@"company"] isKindOfClass:[NSString class]] ? serviceDict[@"company"] : nil;
    capabilities.lastModifiedDate = [serviceDict[@"last_modified"] isKindOfClass:[NSString class]] ? serviceDict[@"last_modified"] : nil;
    capabilities.serviceName = [serviceDict[@"service_name"] isKindOfClass:[NSString class]] ? serviceDict[@"service_name"] : nil;
    capabilities.deploymentMode = [capDict[@"deployment_mode"] isKindOfClass:[NSString class]] ? capDict[@"deployment_mode"] : nil;
    capabilities.maxUploadSize = [capDict[@"max_file_size"] isKindOfClass:[NSNumber class]] ? capDict[@"max_file_size"] : nil;
    capabilities.dirSearch = [capDict[@"dirsearch"] isKindOfClass:[NSNumber class]] ? capDict[@"dirsearch"] : nil;
    capabilities.docAPI = [capDict[@"docapi"] isKindOfClass:[NSNumber class]] ? capDict[@"docapi"] : nil;
    capabilities.documentPreview = [capDict[@"preview"] isKindOfClass:[NSNumber class]] ? capDict[@"preview"] : nil;
    capabilities.releaseAnywhere = [capDict[@"SRA"] isKindOfClass:[NSNumber class]] ? capDict[@"SRA"] : nil;

    // Parse access
    id serviceAccess = serviceDict[@"access"];
    if ([serviceAccess isKindOfClass:[NSArray class]] && ((NSArray *)serviceAccess).count > 0) {
        NSArray *accessArray = (NSArray *)serviceAccess;
        [capabilities addAccess:[self updateAccessV1:capabilities.access newAccess:accessArray inContext:context]];
    } else if ([serviceAccess isKindOfClass:[NSDictionary class]]  && ((NSDictionary *)serviceAccess).count > 0) {
        NSDictionary *accessDict = (NSDictionary *)serviceAccess;
        [capabilities addAccess:[self updateAccess:capabilities.access newAccess:accessDict inContext:context]];
    } else if (capabilities.access.count > 0) {
        for (ServiceAccess *access in capabilities.access) {
            [context deleteObject:access];
        }
        capabilities.access = nil;
    }

    // Parse links
    NSArray *linksArray = serviceDict[@"links"];
    if ([linksArray isKindOfClass:[NSArray class]] && linksArray.count > 0) {
        [capabilities addLinks:[self updateLinks:capabilities.links newLinks:linksArray inContext:context]];
    } else if (capabilities.links.count > 0) {
        for (ServiceLinks *link in capabilities.links) {
            [context deleteObject:link];
        }
        capabilities.links = nil;
    }

    // Save to persistence
    NSError *error;
    [context saveToPersistentStore:&error];
    if (error) NSLog(@"Error saving service capabilities: %@", error);

    return capabilities.objectID;
}

+ (NSSet<ServiceLinks *> *)updateLinks:(NSSet<ServiceLinks *> *)oldLinks newLinks:(NSArray *)newLinks inContext:(NSManagedObjectContext *)context
{
    NSMutableSet<ServiceLinks *> *mutableOldLinks = [NSMutableSet setWithSet:oldLinks];
    oldLinks = nil;

    NSMutableSet<ServiceLinks *> *mutableNewLinks = [NSMutableSet set];

    for (NSDictionary *linkDict in newLinks) {
        if (![linkDict isKindOfClass:[NSDictionary class]]) continue;

        ServiceLinks *link = nil;
        for (ServiceLinks *oldLink in mutableOldLinks) {
            if ([oldLink.rel isEqualToString:linkDict[@"rel"]]) {
                link = oldLink;
                break;
            }
        }
        if (link == nil) {
            link = [NSEntityDescription insertNewObjectForEntityForName:@"ServiceLinks" inManagedObjectContext:context];
        } else {
            [mutableOldLinks removeObject:link];
        }
        link.rel = linkDict[@"rel"];
        link.type = linkDict[@"type"];
        link.href = linkDict[@"href"];
        [mutableNewLinks addObject:link];
    }

    for (ServiceLinks *oldLink in mutableOldLinks) {
        [context deleteObject:oldLink];
    }

    return [NSSet setWithSet:mutableNewLinks];
}

+ (NSSet<ServiceAccess *> *)updateAccessV1:(NSSet<ServiceAccess *> *)old newAccess:(NSArray *)new inContext:(NSManagedObjectContext *)context
{
    NSMutableSet<ServiceAccess *> *mutableOldAccess = [NSMutableSet setWithSet:old];
    old = nil;

    NSMutableSet<ServiceAccess *> *mutableNewAccess = [NSMutableSet set];

    for (NSDictionary *accessType in new) {
        if (![accessType isKindOfClass:[NSDictionary class]]) continue;

        NSArray *subAuths = accessType.allValues.firstObject;
        if ([subAuths isKindOfClass:[NSArray class]] && subAuths.count > 0) {
            for (NSDictionary *subAuth in subAuths) {
                if (![subAuth isKindOfClass:[NSDictionary class]]) continue;

                ServiceAccess *access = nil;
                for (ServiceAccess *oldAccess in mutableOldAccess) {
                    if ([oldAccess.type isEqualToString:accessType.allKeys.firstObject]) {
                        access = oldAccess;
                        break;
                    }
                }
                if (access == nil) {
                    access = [NSEntityDescription insertNewObjectForEntityForName:@"ServiceAccess" inManagedObjectContext:context];
                } else {
                    [mutableOldAccess removeObject:access];
                }

                access.type = accessType.allKeys.firstObject;
                access.issuer = subAuth[@"issuer"];
                access.scopes = nil;
                access.links = nil;
                [mutableNewAccess addObject:access];
            }
        } else {
            ServiceAccess *access = nil;
            for (ServiceAccess *oldAccess in mutableOldAccess) {
                if ([oldAccess.type isEqualToString:accessType.allKeys.firstObject]) {
                    access = oldAccess;
                    break;
                }
            }
            if (access == nil) {
                access = [NSEntityDescription insertNewObjectForEntityForName:@"ServiceAccess" inManagedObjectContext:context];
            } else {
                [mutableOldAccess removeObject:access];
            }

            access.type = accessType.allKeys.firstObject;
            access.issuer = nil;
            access.scopes = nil;
            access.links = nil;
            [mutableNewAccess addObject:access];
        }
    }

    for (ServiceAccess *oldAccess in mutableOldAccess) {
        [context deleteObject:oldAccess];
    }

    return [NSSet setWithSet:mutableNewAccess];
}

+ (NSSet<ServiceAccess *> *)updateAccess:(NSSet<ServiceAccess *> *)oldAccess newAccess:(NSDictionary *)newAccess inContext:(NSManagedObjectContext *)context
{
    NSMutableSet<ServiceAccess *> *mutableOldAccess = [NSMutableSet setWithSet:oldAccess];
    oldAccess = nil;

    NSMutableSet<ServiceAccess *> *mutableNewAccess = [NSMutableSet set];

    for (NSString *key in newAccess) {
        id value = [newAccess objectForKey:key];
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSDictionary *subAuth = (NSDictionary *)value;

            ServiceAccess *access = nil;
            for (ServiceAccess *old in mutableOldAccess) {
                if ([old.type isEqualToString:key]) {
                    access = old;
                    break;
                }
            }
            if (access == nil) {
                access = [NSEntityDescription insertNewObjectForEntityForName:@"ServiceAccess" inManagedObjectContext:context];
            } else {
                [mutableOldAccess removeObject:access];
            }

            access.type = key;

            // If the issuer changed we need to clear any old OAuth token that may be stored
            if (![access.issuer isEqualToString:subAuth[@"issuer"]]) {
                [access clearOAuth];
            }

            access.issuer = subAuth[@"issuer"];

            // Look for scopes
            NSArray *scopeArray = [subAuth[@"scopes"] isKindOfClass:[NSArray class]] ? subAuth[@"scopes"] : nil;
            if (scopeArray.count > 0) {
                NSMutableArray *scopes = [NSMutableArray array];
                for (NSDictionary *scopeDict in scopeArray) {
                    if (![scopeDict isKindOfClass:[NSDictionary class]]) continue;
                    [scopes addObject:scopeDict[@"name"]];
                }
                access.scopes = (scopes.count > 0) ? [NSArray arrayWithArray:scopes] : nil;
            } else {
                access.scopes = nil;
            }

            // Look for links
            NSArray *linksArray = subAuth[@"links"];
            if ([linksArray isKindOfClass:[NSArray class]] && linksArray.count > 0) {
                [access addLinks:[self updateLinks:access.links newLinks:linksArray inContext:context]];
            } else if (access.links.count > 0) {
                for (ServiceLinks *link in access.links) {
                    [context deleteObject:link];
                }
                access.links = nil;
            }

            [mutableNewAccess addObject:access];
        }
    }

    for (ServiceAccess *old in mutableOldAccess) {
        [context deleteObject:old];
    }

    return [NSSet setWithSet:mutableNewAccess];
}

+ (NSManagedObjectID *)failureCapabilitiesFromServiceURL:(NSString *)serviceURL
{
    // Private scratch context
    NSManagedObjectContext *context = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
    context.undoManager = nil;

    ServiceCapabilities *capabilities = [ServiceCapabilities getCapabilitiesForService:serviceURL inContext:context];
    if (capabilities) {
        return capabilities.objectID;
    }

    return nil;
}

+ (BOOL)isVersion:(NSString *)oneString lessThan:(NSString *)otherString {
    // LOWER
    if ([oneString compare:otherString options:NSNumericSearch] == NSOrderedAscending) {
        return YES;
    }

    // EQUAL
    if ([oneString compare:otherString options:NSNumericSearch] == NSOrderedSame) {
        return NO;
    }

    //HIGHER
    return NO;
}

@end
