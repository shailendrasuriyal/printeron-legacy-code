//
//  ServiceDescription.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-04-13.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@interface ServiceDescription : NSObject

+ (void)capabilitiesForServiceWithURL:(NSURL *)url forceUpdate:(BOOL)force completionBlock:(void (^)(NSManagedObjectID *capabilitiesID))completionBlock;
+ (BOOL)isVersion:(NSString *)oneString lessThan:(NSString *)otherString;

@end
