//
//  UIImage+Color.h
//  PrinterOn
//
//  Created by Mark Burns on 10/25/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
