//
//  NSString+Parameters.h
//  PrinterOn
//
//  Created by Mark Burns on 12/20/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface NSString (Parameters)

- (NSMutableDictionary *)parametersSeparatedByInnerString:(NSString *)inner
                                    andOuterString:(NSString *)outer
                                     lowerCaseKeys:(BOOL)lower;

@end
