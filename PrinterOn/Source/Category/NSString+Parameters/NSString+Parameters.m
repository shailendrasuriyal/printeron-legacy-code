//
//  NSString+Parameters.m
//  PrinterOn
//
//  Created by Mark Burns on 12/20/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "NSString+Parameters.h"

@implementation NSString (Parameters)

- (NSMutableDictionary *)parametersSeparatedByInnerString:(NSString *)inner
                                    andOuterString:(NSString *)outer
                                     lowerCaseKeys:(BOOL)lower
{
    NSArray *first = [self componentsSeparatedByString:outer];

    NSInteger count = first.count;
    NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionaryWithCapacity:count];
    for (NSInteger i = 0; i < count; i++) {
        NSArray *second = [(NSString *)first[i] componentsSeparatedByString:inner];
        if (second.count == 2) {
            returnDictionary[lower ? ((NSString *)second[0]).lowercaseString : second[0]] = second[1];
        }
    }

    return returnDictionary;
}

@end
