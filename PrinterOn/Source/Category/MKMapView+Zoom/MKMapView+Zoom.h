//
//  MKMapView+Zoom.h
//  PrinterOn
//
//  Created by Mark Burns on 11/18/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface MKMapView (Zoom)

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;

+ (MKCoordinateSpan)coordinateSpanWithView:(UIView *)view
                         centerCoordinates:(CLLocationCoordinate2D)centerCoordinate
                              andZoomLevel:(NSUInteger)zoomLevel;

@end
