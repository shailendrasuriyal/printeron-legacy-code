//
//  NSManagedObject+Clone.m
//  SearchTest
//
//  Created by Mark Burns on 2013-10-07.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "NSManagedObject+Clone.h"

@implementation NSManagedObject (Clone)

- (NSManagedObject *)cloneInContext:(NSManagedObjectContext *)context
                      entityForName:(NSString *)entityName
                duplicateIdentifier:(NSPredicate *)predicate
                    excludeEntities:(NSArray *)namesOfEntitiesToExclude
               excludeRelationships:(NSArray *)namesOfRelationshipsToExclude
                       updateValues:(BOOL)update
{
    return [self cloneInContext:context entityForName:entityName duplicateIdentifier:predicate withCopiedCache:[NSMutableDictionary dictionary] excludeEntities:namesOfEntitiesToExclude excludeRelationships:namesOfRelationshipsToExclude updateValues:update];
}

- (NSManagedObject *)cloneInContext:(NSManagedObjectContext *)context
                      entityForName:(NSString *)entityName
                duplicateIdentifier:(NSPredicate *)predicate
                    withCopiedCache:(NSMutableDictionary *)alreadyCopied
                    excludeEntities:(NSArray *)namesOfEntitiesToExclude
               excludeRelationships:(NSArray *)namesOfRelationshipsToExclude
                       updateValues:(BOOL)update
{
    if (!entityName) entityName = self.entity.name;
    if (!context) context = self.managedObjectContext;

    if ([namesOfEntitiesToExclude containsObject:entityName]) {
        return nil;
    }

    NSManagedObject *cloned = alreadyCopied[self.objectID];
    if (cloned != nil) {
        return cloned;
    }

    // Check to see if we should search for duplicates before we start the clone
    if (predicate != nil) {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
        //NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"(%@ == \"%@\")", identifier, [self valueForKey:identifier]]];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            cloned = results.firstObject;
            if (!update) return cloned;
        }
    }

    //create new object in data store
    if (cloned == nil) cloned = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    alreadyCopied[self.objectID] = cloned;

    //loop through all attributes and assign then to the clone
    NSDictionary *attributes = [NSEntityDescription entityForName:entityName inManagedObjectContext:context].attributesByName;

    for (NSString *attr in attributes) {
        [cloned setValue:[self valueForKey:attr] forKey:attr];
    }

    //Loop through all relationships, and clone them.
    NSDictionary *relationships = [NSEntityDescription entityForName:entityName inManagedObjectContext:context].relationshipsByName;
    for (NSString *relName in relationships.allKeys) {
        // Skip relationships that should be excluded
        if ([namesOfRelationshipsToExclude containsObject:relName]) {
            continue;
        }

        NSRelationshipDescription *rel = relationships[relName];

        NSString *keyName = rel.name;
        if (rel.isToMany) {
            if (rel.isOrdered) {
                NSMutableOrderedSet *sourceSet = [self mutableOrderedSetValueForKey:keyName];
                NSMutableOrderedSet *clonedSet = [cloned mutableOrderedSetValueForKey:keyName];

                NSManagedObject *relatedObject;
                NSEnumerator *e = clonedSet.objectEnumerator;
                while ( relatedObject = [e nextObject]){
                    [context deleteObject:relatedObject];
                }

                e = sourceSet.objectEnumerator;
                while ( relatedObject = [e nextObject]){
                    //Clone it, and add clone to set
                    NSManagedObject *clonedRelatedObject = [relatedObject cloneInContext:context entityForName:nil duplicateIdentifier:nil withCopiedCache:alreadyCopied excludeEntities:namesOfEntitiesToExclude excludeRelationships:nil updateValues:update];
                    
                    [clonedSet addObject:clonedRelatedObject];
                }
            }
            else {
                NSMutableSet *sourceSet = [self mutableSetValueForKey:keyName];
                NSMutableSet *clonedSet = [cloned mutableSetValueForKey:keyName];

                NSManagedObject *relatedObject;
                NSEnumerator *e = clonedSet.objectEnumerator;
                while ( relatedObject = [e nextObject]){
                    [context deleteObject:relatedObject];
                }
                
                e = sourceSet.objectEnumerator;
                while ( relatedObject = [e nextObject]){
                    //Clone it, and add clone to set
                    NSManagedObject *clonedRelatedObject = [relatedObject cloneInContext:context entityForName:nil duplicateIdentifier:nil withCopiedCache:alreadyCopied excludeEntities:namesOfEntitiesToExclude excludeRelationships:nil updateValues:update];
                    
                    [clonedSet addObject:clonedRelatedObject];
                }
            }
        }
        else {
            NSManagedObject *relatedObject = [cloned valueForKey:keyName];
            if (relatedObject != nil) {
                [context deleteObject:relatedObject];
            }

            relatedObject = [self valueForKey:keyName];
            if (relatedObject != nil) {
                NSManagedObject *clonedRelatedObject = [relatedObject cloneInContext:context entityForName:nil duplicateIdentifier:nil withCopiedCache:alreadyCopied excludeEntities:namesOfEntitiesToExclude excludeRelationships:nil updateValues:update];
                [cloned setValue:clonedRelatedObject forKey:keyName];
            }
        }
        
    }
    
    return cloned;
}

@end
