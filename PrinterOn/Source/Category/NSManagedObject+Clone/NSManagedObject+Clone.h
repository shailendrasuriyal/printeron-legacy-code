//
//  NSManagedObject+Clone.h
//  SearchTest
//
//  Created by Mark Burns on 2013-10-07.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface NSManagedObject (Clone)

- (NSManagedObject *)cloneInContext:(NSManagedObjectContext *)context
                      entityForName:(NSString *)entityName
                duplicateIdentifier:(NSPredicate *)predicate
                    excludeEntities:(NSArray *)namesOfEntitiesToExclude
               excludeRelationships:(NSArray *)namesOfRelationshipsToExclude
                       updateValues:(BOOL)update;

@end
