//
//  NSData+Transform.m
//  PrinterOn
//
//  Created by Mark Burns on 2/12/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "NSData+Transform.h"

@implementation NSData (Transform)

+ (NSData *)toBytes:(NSString *)string
{
    NSMutableData* data = [NSMutableData data];

    for (int idx = 0; idx+2 <= string.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [string substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
        [data appendBytes:&intValue length:1];
    }

    return [NSData dataWithData:data];
}

- (NSString *)hexString
{
    const unsigned char *dataBuffer = (const unsigned char *)self.bytes;
    
    if (!dataBuffer) {
        return [NSString string];
    }
    
    NSUInteger dataLength = self.length;
    NSMutableString *hexString = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i) {
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    }
    
    return [NSString stringWithString:hexString];
}

- (void)transform
{
    NSString *data = @"initWithCharactersNoCopy:length:freeWhenDone:";
    unsigned char *pBytesInput = (unsigned char *)self.bytes;
    unsigned char *pBytesKey = (unsigned char *)[data dataUsingEncoding:NSUTF8StringEncoding].bytes;
    unsigned long vlen = self.length;
    unsigned long klen = data.length;
    unsigned long k = vlen % klen;

    for (unsigned long v = 0; v < vlen; v++) {
        unsigned char c = pBytesInput[v] ^ pBytesKey[k];
        pBytesInput[v] = c;
        k = (++k < klen ? k : 0);
    }
}

@end
