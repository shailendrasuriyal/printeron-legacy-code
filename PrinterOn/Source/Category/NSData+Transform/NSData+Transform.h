//
//  NSData+Transform.h
//  PrinterOn
//
//  Created by Mark Burns on 2/12/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface NSData (Transform)

+ (NSData *)toBytes:(NSString *)string;
- (NSString *)hexString;
- (void)transform;

@end
