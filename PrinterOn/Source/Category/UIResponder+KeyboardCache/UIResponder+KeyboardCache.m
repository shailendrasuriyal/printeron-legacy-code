//
//  UIResponder+KeyboardCache.m
//  PrinterOn
//
//  Created by Mark Burns on 12/20/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "UIResponder+KeyboardCache.h"

static BOOL hasAlreadyCachedKeyboard;

@interface UIResponder (KeyboardCache_Private)

+(void) __cacheKeyboard;

@end

@implementation UIResponder (KeyboardCache)

+(void) cacheKeyboard {
    [[self class] cacheKeyboard:NO];
}

+(void) cacheKeyboard:(BOOL)onNextRunloop {
    if (!hasAlreadyCachedKeyboard) {
        hasAlreadyCachedKeyboard = YES;
        if (onNextRunloop)
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.0), dispatch_get_main_queue(), ^(void){ [[self class] __cacheKeyboard]; });
        else
            [[self class] __cacheKeyboard];
    }
}

+(void) __cacheKeyboard {
    UITextField *field = [UITextField new];
    THEME_KEYBOARD(field);
    [[UIApplication sharedApplication].windows.lastObject addSubview:field];
    [field becomeFirstResponder];
    [field resignFirstResponder];
    [field removeFromSuperview];
}

@end
