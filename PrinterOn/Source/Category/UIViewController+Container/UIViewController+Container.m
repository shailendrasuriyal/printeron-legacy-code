//
//  UIViewController+Container.m
//  PrinterOn
//
//  Created by Mark Burns on 11/21/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "UIViewController+Container.h"

@implementation UIViewController (Container)

- (UIViewController *)addViewControllerToContainer:(UIView *)container
                              storyboardIdentifier:(NSString *)identifier
{
    // Get view controller from storyboard with given identifier and add it as a child
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self addChildViewController:vc];
    
    // Set the views frame to equal the bounds of the container
    vc.view.frame = container.bounds;
    
    // Add the view controllers view to the container
    [vc willMoveToParentViewController:self];
    [container addSubview:vc.view];
    [vc didMoveToParentViewController:self];

    return vc;
}

@end
