//
//  UIViewController+Container.h
//  PrinterOn
//
//  Created by Mark Burns on 11/21/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface UIViewController (Container)

- (UIViewController *)addViewControllerToContainer:(UIView *)container
                              storyboardIdentifier:(NSString *)identifier;

@end
