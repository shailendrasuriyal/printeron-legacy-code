//
//  NSString+Email.h
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface NSString (Email)

- (BOOL)isEmailAddress;

@end
