//
//  NSString+URL.h
//  PrinterOn
//
//  Created by Mark Burns on 12/20/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface NSString (URL)

- (NSString *)URLEncode;
- (NSString *)URLDecode;

@end
