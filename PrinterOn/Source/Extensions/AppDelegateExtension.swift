//
//  AppDelegateExtension.swift
//  PrinterOn
//
//  Created by Alex Cole on 7/26/17.
//  Copyright © 2017 PrinterOn Inc. All rights reserved.
//

import Foundation
import CoreLocation
import UserNotifications

//extension AppDelegate: CLLocationManagerDelegate {
//    
//    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
//        guard region is CLBeaconRegion, let beacon = region as? CLBeaconRegion else { return }
//        print("did enter")
//        scheduleNotification(time: 1)
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
//        guard region is CLBeaconRegion else { return }
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
//        print("Beacon in range")
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
//        print("Beacon didDetermineState state")
//        scheduleNotification(time: 1)
//    }
//    
//    func updateDistance(_ distance: CLProximity) {
//        switch distance {
//        case .unknown:
//            break
//        case .far:
//            break
//        case .near:
//            break
//        case .immediate:
//            break
//        }
//    }
//    
//}
//
//extension AppDelegate: UNUserNotificationCenterDelegate {
//    
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                didReceive response: UNNotificationResponse,
//                                withCompletionHandler completionHandler: @escaping () -> Void) {
//        switch response.actionIdentifier {
//        case Const.NotificationActions.remind:
//            scheduleNotification(time: 30)
//            break
//        case Const.NotificationActions.notify:
//            print("notify")
//            locationManager.requestState(for: wepaRegion)
//            
//            break
//        default:
//            break
//        }
//        completionHandler()
//    }
//    
//    func scheduleNotification(time: Double) {
//        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval:time, repeats: false)
//        
//        let content = UNMutableNotificationContent()
//        content.title = "Ready for Release"
//        content.body = "The OfficeJet 250 is nearby. Would you like to release your print job?"
//        content.sound = UNNotificationSound.default()
//        content.categoryIdentifier = Const.NotificationCategories.releaseJob
//        let request = UNNotificationRequest(identifier: Const.NotificationCategories.releaseJob, content: content, trigger: trigger)
//        UNUserNotificationCenter.current().add(request) {(error) in
//            if let error = error {
//                print("Error: \(error)")
//            }
//        }
//    }
//}
