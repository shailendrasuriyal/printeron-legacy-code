//
//  AddBookmarkViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-08-13.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "AddBookmarkViewController.h"

#import "BarButtonItem.h"
#import "Bookmark.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface AddBookmarkViewController ()

@end

@implementation AddBookmarkViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Set the initial value for the fields
    if (self.bookmark) {
        [self.textTitle setText:self.bookmark.bookmarkTitle];
        [self.textAddress setText:self.bookmark.bookmarkURL];
    } else if (self.webView) {
        [self.textTitle setText:[self.webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
        [self.textAddress setText:[self.webView.request.URL absoluteString]];
    }

    [self validateFields];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.preferredContentSize = CGSizeMake(320.0f, 480.0f);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textTitle];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textAddress];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    self.screenName = @"Add Bookmark Screen";
    [self.textTitle becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textTitle];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textAddress];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(self.editing ? @"TITLE_EDITBOOKMARK" : @"TITLE_ADDBOOKMARK", nil);
    [self.textTitle setPlaceholder:NSLocalizedPONString(@"LABEL_TITLE", nil)];
    [self.textAddress setPlaceholder:NSLocalizedPONString(@"LABEL_ADDRESS", nil)];
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.saveButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.SelectButton.Image"]]];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"WebScreen.BackgroundColor"];

    // Add AccessibilityId for title and address
    [self.textTitle setAccessibilityIdentifier:@"Bookmark Title Field"];
    [self.textAddress setAccessibilityIdentifier:@"Bookmark Address Field"];

    THEME_KEYBOARD(self.textTitle);
    THEME_KEYBOARD(self.textAddress);
}

- (IBAction)savePressed
{
    self.bookmark.bookmarkTitle = self.textTitle.text;
    self.bookmark.bookmarkURL = self.textAddress.text;

    if (!self.editing) {
        Bookmark *bookmark = [NSEntityDescription insertNewObjectForEntityForName:@"Bookmark" inManagedObjectContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext];
        bookmark.bookmarkTitle = self.textTitle.text;
        bookmark.bookmarkURL = self.textAddress.text;

        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Bookmark"];
        NSUInteger count = [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext countForFetchRequest:fetchRequest error:nil];
        if (count == NSNotFound) count = 0;
        bookmark.displayOrder = @((long)count);
    }

    [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)validateFields
{
    [self.saveButtonItem setEnabled:[self areFieldsValid]];
}

- (BOOL)areFieldsValid
{
    if ([[self.textTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
        return NO;

    if ([[self.textAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return NO;
    }

    return YES;
}

- (void)textChanged:(NSNotification *)notification
{
    [self validateFields];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [self.scrollView textFieldShouldReturn:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *temp = [[textField.text stringByReplacingCharactersInRange:range withString:string] stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    if ([textField.text isEqualToString:temp]) {
        return NO;
    }
    
    return YES;
}

@end
