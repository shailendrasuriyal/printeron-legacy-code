//
//  WebBrowserViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/20/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "WebBrowserViewController.h"

#import "BarButtonItem.h"
#import "Bookmark.h"
#import "MZFormSheetController.h"
#import "NSData+Transform.h"
#import "PreviewViewController.h"
#import "ShadowView.h"
#import "URLAddressBar.h"
#import "WebDocument.h"

@interface WebBrowserViewController ()

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSMutableDictionary *lastSession;
@property (nonatomic, assign) BOOL firstLoad;

@end

@implementation WebBrowserViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.webView.scrollView.delegate = self;

    // Add the target actions for buttons
    [self.addressBar.reloadButton addTarget:self.webView action:@selector(reload) forControlEvents:UIControlEventTouchUpInside];
    [self.addressBar.stopButton addTarget:self.webView action:@selector(stopLoading) forControlEvents:UIControlEventTouchUpInside];

    // Setup the webview and button states
    self.printButtonItem.enabled = NO;
    self.pageUpButtonItem.enabled = NO;
    [self updateToolbar];

    // Register for app termination so we can save the last session
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveLastSession) name:UIApplicationWillTerminateNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self saveLastSession];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.webView setDelegate:self];
    if (!self.firstLoad) {
        self.lastSession = [self loadLastSession];
        [self addLastSessionToView:self.lastSession];
        self.firstLoad = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Web Browser Screen";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    // Close the keyboard before leaving the screen.  Makes the keyboard not lag and delay the app.
    // This only happens because the address bar textfield is embedded in the navigation bar instead of inside the screen views.
    [self.addressBar resignFirstResponder];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self centerLastSessionView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    [self.addressBar setPlaceholder:NSLocalizedPONString(@"LABEL_ADDRESSBAR", nil)];
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.printButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.PrintButton.Image"]]];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"WebScreen.BackgroundColor"];

    // Set ToolBar appearance
    [self.toolBar setTintColor:[ThemeLoader colorForKey:@"WebScreen.Toolbar.TextColor"]];
    [self.toolBar setBarTintColor:[ThemeLoader colorForKey:@"WebScreen.Toolbar.BackgroundColor"]];
    self.toolBar.layer.shadowOpacity = 0.75f;
    self.toolBar.layer.shadowRadius = 1.5f;
    self.toolBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.toolBar.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);

    [self.backButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"WebScreen.Toolbar.BackButton.Image"]] forState:UIControlStateNormal];
    [self.forwardButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"WebScreen.Toolbar.ForwardButton.Image"]] forState:UIControlStateNormal];
    [self.bookMarkButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"WebScreen.Toolbar.BookMarkButton.Image"]] forState:UIControlStateNormal];
    [self.pageUpButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"WebScreen.Toolbar.PageUpButton.Image"]] forState:UIControlStateNormal];

    // Set fixed space to be less, because iOS7 has larger toolbar edge insets
    self.fixedSpace.width = 84.0f;
    
    // Set the keyboard color
    THEME_KEYBOARD(self.addressBar);
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Compute the shadow paths after layout
    self.toolBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.toolBar.layer.bounds.origin.x - 3, self.toolBar.layer.bounds.origin.y, self.toolBar.layer.bounds.size.width + 6, self.toolBar.layer.bounds.size.height)].CGPath;
}

- (NSString *)getHostName:(NSURL *)url
{
    NSString *name = [url host];
    if ([[name lowercaseString] hasPrefix:@"www."]) {
        return [name substringFromIndex:4];
    }
    return name;
}

- (void)updateToolbar
{
    self.backButtonItem.enabled = self.webView.canGoBack;
    self.forwardButtonItem.enabled = self.webView.canGoForward;
}

- (IBAction)openBookmarks:(id)sender {
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenBookmarks"];
    if ([navController.topViewController isKindOfClass:[BookmarksViewController class]]) {
        BookmarksViewController *optionsController = (BookmarksViewController *)navController.topViewController;
        optionsController.delegate = self;
        optionsController.webView = self.webView;
        
        [optionsController.view setFrame:CGRectMake(0, 0, 320, 480)];
        [optionsController.view setNeedsLayout];
        [optionsController.view layoutIfNeeded];
        
        MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:navController];
        formSheet.presentedFormSheetSize = CGSizeMake(optionsController.view.frame.size.width, optionsController.view.frame.size.height + navController.navigationBar.frame.size.height);
        formSheet.shouldDismissOnBackgroundViewTap = YES;
        formSheet.shouldCenterVertically = YES;
        formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsDoNothing;
        formSheet.transitionStyle = MZFormSheetTransitionStyleSlideAndBounceFromRight;
        
        [self mz_presentFormSheetController:formSheet animated:YES completionHandler:nil];
    }
}

- (IBAction)scrollToTop:(id)sender
{
    [self.webView.scrollView setContentOffset:CGPointMake(-self.webView.scrollView.contentInset.left, -self.webView.scrollView.contentInset.top) animated:YES];
}

#pragma mark - Last Session

- (void) addLastSessionToView:(NSDictionary *)dict
{
    if (dict) {
        [self.lastSessionTitle setText:dict[@"Title"]];
        [self.lastSessionPreview setImage:dict[@"Screenshot"]];
        [self.lastSessionDelete setImage:[[ImageManager sharedImageManager] imageNamed:@"URLAddressBar-Stop"] forState:UIControlStateNormal];
        [self centerLastSessionView];
        [self.lastSessionView setHidden:NO];
    } else {
        [self.lastSessionView setHidden:YES];
    }
}

- (void)centerLastSessionView
{
    [self.lastSessionView setFrame:CGRectMake((self.webView.frame.size.width - self.lastSessionView.frame.size.width) / 2, (self.webView.frame.size.height - self.lastSessionView.frame.size.height) / 2, self.lastSessionView.frame.size.width, self.lastSessionView.frame.size.height)];
}

- (void)generateLastSession
{
    if (self.lastSession == nil) {
        self.lastSession = [NSMutableDictionary dictionary];
    }

    [self lastSession][@"URL"] = self.webView.request.URL;
    [self lastSession][@"Title"] = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [self takeScreenshot];
}

- (NSMutableDictionary *)loadLastSession
{
    NSData *lastSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastWebSession"];
    if (lastSession) {
        [lastSession transform];
        return [NSKeyedUnarchiver unarchiveObjectWithData:lastSession];
    }
    return nil;
}

- (void)saveLastSession
{
    if (self.lastSession && [self lastSession][@"URL"] && [self lastSession][@"Title"] && [self lastSession][@"Screenshot"]) {
        NSData *lastSession = [NSKeyedArchiver archivedDataWithRootObject:self.lastSession];
        [lastSession transform];
        [[NSUserDefaults standardUserDefaults] setObject:lastSession forKey:@"PONLastWebSession"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)purgeLastSession
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PONLastWebSession"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)takeScreenshot
{
    CGPoint oldOffset = self.webView.scrollView.contentOffset;
    [self.webView.scrollView setContentOffset:CGPointZero animated:NO];
    UIGraphicsBeginImageContext(self.webView.frame.size);
    [self.webView.layer renderInContext:UIGraphicsGetCurrentContext()];
    [self.webView.scrollView setContentOffset:oldOffset animated:NO];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self lastSession][@"Screenshot"] = viewImage;
}

- (IBAction)browseLastSession
{
    [UIView animateWithDuration:0.15f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.lastSessionPreview.frame = CGRectMake(-self.lastSessionView.frame.origin.x, -self.lastSessionView.frame.origin.y, self.webView.frame.size.width, self.webView.frame.size.height);
        self.lastSessionView.alpha = 0.4;
    } completion:^(BOOL finished){
        if (finished) {
            NSURL *url = [self lastSession][@"URL"];
            self.address = url.absoluteString;
            self.addressBar.text = [self getHostName:url];

            [self.webView setHidden:NO];
            [self.lastSessionView setHidden:YES];

            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:request];
            [self.webView loadRequest:request];
        }
    }];
}

- (IBAction)deleteLastSession
{
    [UIView animateWithDuration:0.15f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.lastSessionView.alpha = 0;
    } completion:^(BOOL finished){
        if (finished) {
            [self.lastSessionView setHidden:YES];
            self.lastSession = nil;
            [self purgeLastSession];
        }
    }];
}

#pragma mark - BookmarksDelegate

- (void)didPickBookmark:(Bookmark *)bookmark
{
    NSURL *url = [NSURL URLWithString:bookmark.bookmarkURL];
    if (url == nil) {
        return;
    }

    if ([url.absoluteString rangeOfString:@"://"].location == NSNotFound) {
        NSString *urlString = [@"http://" stringByAppendingString:url.absoluteString];
        url = [NSURL URLWithString:urlString];
    }

    self.address = url.absoluteString;
    self.addressBar.text = [self getHostName:url];

    [self.webView setHidden:NO];
    [self.lastSessionView setHidden:YES];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:request];
    [self.webView loadRequest:request];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;

    if (scrollOffset <= 0) {
        self.pageUpButtonItem.enabled = NO;
    } else {
        self.pageUpButtonItem.enabled = YES;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSURL *url = [NSURL URLWithString:self.addressBar.text];
    if (url == nil) {
        return NO;
    }

    if ([url.absoluteString rangeOfString:@"://"].location == NSNotFound) {
        NSString *urlString = [@"http://" stringByAppendingString:url.absoluteString];
        url = [NSURL URLWithString:urlString];
    }

    self.address = url.absoluteString;

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:request];
    [self.webView loadRequest:request];

    [self.addressBar resignFirstResponder];
    [self.webView setHidden:NO];
    [self.lastSessionView setHidden:YES];

    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.webView setUserInteractionEnabled:NO];
    [self.lastSessionView setUserInteractionEnabled:NO];
    if (self.address) {
        self.addressBar.text = self.address;
    } else if (self.webView.request) {
        self.addressBar.text = self.webView.request.mainDocumentURL.absoluteString;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self.webView setUserInteractionEnabled:YES];
    [self.lastSessionView setUserInteractionEnabled:YES];
    if (self.address) {
        self.addressBar.text = [self getHostName:[NSURL URLWithString:self.address]];
    } else if (self.webView.request) {
        self.addressBar.text = [self getHostName:self.webView.request.mainDocumentURL];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self.addressBar.text length] == 0) {
        self.addressBar.rightView = nil;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.addressBar isFirstResponder] && [touch view] != self.addressBar) {
        [self.addressBar resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([NSURLProtocol propertyForKey:@"PONBrowserRequest" inRequest:request] == nil) {
        if ([request isMemberOfClass:[NSMutableURLRequest class]]) {
            [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:(NSMutableURLRequest *)request];
        }
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.addressBar setLoading:YES];
    [self updateToolbar];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.addressBar setLoading:NO];
    self.address = nil;
    if (![self.addressBar isFirstResponder]) {
        [self.addressBar setText:[self getHostName:self.webView.request.mainDocumentURL]];
    }
    self.printButtonItem.enabled = YES;
    [self updateToolbar];
    [self performSelector:@selector(generateLastSession) withObject:nil afterDelay:0.25f];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.addressBar setLoading:NO];
    [self updateToolbar];

    self.address = nil;
    if (![self.addressBar isFirstResponder]) {
        [self.addressBar setText:[self getHostName:self.webView.request.mainDocumentURL]];
        if ([self.addressBar.text length] == 0) {
            self.addressBar.rightView = nil;
        }
    }

    // Hide the webview if it is empty so you don't have scrollbars showing for blank content
    if (webView.request.URL.absoluteString.length == 0) {
        [webView setHidden:YES];
    }

    // NSURLErrorDomain code -999 means another request was made before the previous completed
    if ([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
        return;
    }

    // Display the error message
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPrintPreview"]) {
        WebDocument *document = [[WebDocument alloc] initWithWebView:self.webView];

        PreviewViewController *destViewController = segue.destinationViewController;
        destViewController.document = document;
    } else if ([segue.identifier isEqualToString:@"openBookmarks"]) {
        UINavigationController *destViewController = segue.destinationViewController;
        if ([destViewController.topViewController isKindOfClass:[BookmarksViewController class]]) {
            BookmarksViewController *vc = (BookmarksViewController *)destViewController.topViewController;
            vc.delegate = self;
            vc.webView = self.webView;
        }
    }
}

@end
