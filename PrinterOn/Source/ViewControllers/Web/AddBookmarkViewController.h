//
//  AddBookmarkViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-08-13.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@class Bookmark, TPKeyboardAvoidingScrollView;

@interface AddBookmarkViewController : BaseViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *textTitle;
@property (weak, nonatomic) IBOutlet UITextField *textAddress;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (nonatomic, strong) Bookmark *bookmark;
@property (nonatomic, assign) BOOL editing;
@property (nonatomic, strong) UIWebView *webView;

@end
