//
//  BookmarksViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-08-10.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@class Bookmark;

@protocol BookmarksDelegate

- (void)didPickBookmark:(Bookmark *)bookmark;

@end

@interface BookmarksViewController : BaseViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *addView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (weak, nonatomic) IBOutlet UIButton *editButton;

@property (nonatomic, weak) id <BookmarksDelegate> delegate;
@property (nonatomic, strong) UIWebView *webView;

@end
