//
//  BookmarksViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-08-10.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "BookmarksViewController.h"

#import "AddBookmarkViewController.h"
#import "Bookmark.h"
#import "BarButtonItem.h"
#import "MZFormSheetController.h"

@interface BookmarksViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign) BOOL reorderingBookmarks;

@end

@implementation BookmarksViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.preferredContentSize = CGSizeMake(320.0f, 480.0f);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Bookmarks Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_BOOKMARKS", nil);
}

- (void)setupTheme
{
    [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];
    [BarButtonItem customizeRightBarButton:self.editButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.EditButton.Image"]]];

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"WebScreen.BackgroundColor"];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    // Setup the "Add Bookmark" appearance
    [self.addButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.AddButton.Image"]] forState:UIControlStateNormal];
    self.addView.backgroundColor = [ThemeLoader colorForKey:@"WebScreen.Toolbar.BackgroundColor"];
    self.addView.layer.cornerRadius = self.addView.frame.size.width / 2;
    self.addView.layer.masksToBounds = NO;
    self.addView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.addView.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.addView.layer.shadowOpacity = 0.5f;
    self.addView.layer.shadowRadius = 1.5f;
    self.addView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.addView.bounds cornerRadius:self.addView.layer.cornerRadius].CGPath;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    [self updateContentHeight];

    // Compute the shadow paths after layout
    self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;
}

- (IBAction)backPressed
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        UIViewController *parentController = self.navigationController.parentViewController;
        if ([parentController isKindOfClass:[MZFormSheetController class]]) {
            MZFormSheetController *formSheetController = (MZFormSheetController *)parentController;
            [formSheetController dismissAnimated:YES completionHandler:nil];
        }
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)editPressed
{
    [UIView animateWithDuration:0.15f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.tableView.editing = !self.tableView.editing;
    } completion:nil];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Bookmark"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"displayOrder" ascending:YES]];

        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;

        [self performFetch];
    }

    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];

    if (self.fetchedResultsController.fetchedObjects.count == 0) {
        [self.editButton setHidden:YES];
        self.tableView.editing = NO;
    } else {
        [self.editButton setHidden:NO];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    if (self.reorderingBookmarks) return;

    //Lets the tableview know we're potentially doing a bunch of updates.
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (self.reorderingBookmarks) return;

    //We're finished updating the tableview's data.
    [self.tableView endUpdates];
    self.tableView.contentSize = [self.tableView sizeThatFits:CGSizeMake(CGRectGetWidth(self.tableView.bounds), CGFLOAT_MAX)];
    [self updateContentHeight];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    if (self.reorderingBookmarks) return;

    if (type == NSFetchedResultsChangeMove && [indexPath isEqual:newIndexPath]) {
        type = NSFetchedResultsChangeUpdate;
    }

    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {

    if (self.reorderingBookmarks) return;

    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"bookmarkCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    // Add AccessibilityId
    [cell setAccessibilityIdentifier:@"bookmarkCell"];

    Bookmark *bookmark = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell.textLabel setText:bookmark.bookmarkTitle];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    // Bypass the delegates temporarily
    self.reorderingBookmarks = YES;

    // Get a handle to the bookmark we're moving
    NSMutableArray *sortedBookmarks = [NSMutableArray arrayWithArray:[self.fetchedResultsController fetchedObjects]];
    Bookmark *bookmarkMoving = sortedBookmarks[sourceIndexPath.row];

    // Remove the bookmark from it's current position
    [sortedBookmarks removeObjectAtIndex:sourceIndexPath.row];

    // Insert it at it's new position
    [sortedBookmarks insertObject:bookmarkMoving atIndex:destinationIndexPath.row];

    // Update the order of them all according to their index in the mutable array
    [sortedBookmarks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Bookmark *bookmark = (Bookmark *)obj;
        bookmark.displayOrder = @((long)idx);
    }];

    // Save the managed object context
    [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];

    // Allow the delegates to work now
    self.reorderingBookmarks = NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Bookmark *bookmark = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if (bookmark) {
            [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext deleteObject:bookmark];
            [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!tableView.editing) {
        if (self.delegate) {
            [self.delegate didPickBookmark:[self.fetchedResultsController objectAtIndexPath:indexPath]];
            [self backPressed];
            return nil;
        }
    }
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"editBookmark"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

        AddBookmarkViewController *destViewController = segue.destinationViewController;
        destViewController.bookmark = [self.fetchedResultsController objectAtIndexPath:indexPath];
        destViewController.editing = YES;
    } else if ([segue.identifier isEqualToString:@"addBookmark"]) {
        AddBookmarkViewController *destViewController = segue.destinationViewController;
        destViewController.webView = self.webView;
        destViewController.editing = NO;
    }
}

@end
