//
//  WebBrowserViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/20/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "BookmarksViewController.h"

@class URLAddressBar;

@interface WebBrowserViewController : BaseViewController <BookmarksDelegate, UIScrollViewDelegate, UITextFieldDelegate, UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@property (weak, nonatomic) IBOutlet URLAddressBar *addressBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *printButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *printButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *forwardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *fixedSpace;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *pageUpButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *pageUpButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bookMarkButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *bookMarkButton;

@property (weak, nonatomic) IBOutlet ShadowView *lastSessionView;
@property (weak, nonatomic) IBOutlet UILabel *lastSessionTitle;
@property (weak, nonatomic) IBOutlet UIButton *lastSessionDelete;
@property (weak, nonatomic) IBOutlet UIImageView *lastSessionPreview;

@end
