//
//  EmailAttachmentsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-05-02.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "EmailAttachmentsViewController.h"

#import "AttachmentCell.h"
#import "EmailAttachmentManager.h"
#import "IMAPSessionManager.h"
#import "MainViewController.h"
#import "PreviewViewController.h"
#import "PrintDocument.h"
#import "PrintJobManager.h"

#import <MailCore/MailCore.h>

@interface EmailAttachmentsViewController ()

@end

@implementation EmailAttachmentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Update the tables when the view will appear, we must do this here to reload/recreate the fetch controllers
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Email Attachments Screen";
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = [NSString stringWithFormat:@"%lu %@", (unsigned long)self.imapMessage.attachments.count, self.imapMessage.attachments.count == 1 ? NSLocalizedPONString(@"LABEL_ATTACHMENT", nil) : NSLocalizedPONString(@"LABEL_ATTACHMENTS", nil)];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.BackgroundColor"];
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

- (void)updateAttachmentCellAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.imapMessage.attachments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"attachmentCell";
    AttachmentCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    MCOIMAPPart *part = self.imapMessage.attachments[indexPath.row];
    NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:[self.imapSession getEmailAccount].emailAddress messageID:[NSString stringWithFormat:@"%d", self.imapMessage.uid] partID:part.partID fileName:part.filename];
    [cell setupCellWithAttachment:attachID withPart:part];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AttachmentCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (!cell.supportedFile) {
        return indexPath;
    }

    MCOIMAPPart *part = self.imapMessage.attachments[indexPath.row];
    NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:[self.imapSession getEmailAccount].emailAddress messageID:[NSString stringWithFormat:@"%d", self.imapMessage.uid] partID:part.partID fileName:part.filename];
    id attachment = [[EmailAttachmentManager sharedEmailAttachmentManager] progressForAttachment:attachID];

    if (attachment == nil) {
        NSProgress *progress = [[EmailAttachmentManager sharedEmailAttachmentManager] startAttachment:attachID];
        [self updateAttachmentCellAtIndexPath:indexPath];

        __weak __typeof(self)weakSelf = self;
        [self.imapSession fetchMessageAttachmentWithFolder:self.imapFolder uid:self.imapMessage.uid partID:part.partID encoding:part.encoding filename:part.filename progress:progress completionBlock:^(NSData *data, NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            if (error) {
                [strongSelf.imapSession showError:error overController:strongSelf.navigationController];
            }

            [strongSelf updateAttachmentCellAtIndexPath:indexPath];
        }];
    } else if (attachment == [NSNull null]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];

            PrintDocument *document = [PrintDocument getDocumentFromFile:[[EmailAttachmentManager sharedEmailAttachmentManager] retrieveURLForAttachment:attachID] fromSource:NSLocalizedPONString(@"LABEL_EMAIL", nil)];
            [[PrintJobManager sharedPrintJobManager].mainView showPrintPreview:document];
        } else {
            [self performSegueWithIdentifier:@"showPrintPreview" sender:[[EmailAttachmentManager sharedEmailAttachmentManager] retrieveURLForAttachment:attachID]];
        }
    }
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPrintPreview"]) {
        PrintDocument *document = [PrintDocument getDocumentFromFile:sender fromSource:NSLocalizedPONString(@"LABEL_EMAIL", nil)];

        PreviewViewController *destViewController = segue.destinationViewController;
        destViewController.document = document;
    }
}

@end
