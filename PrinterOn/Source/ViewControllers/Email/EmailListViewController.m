//
//  EmailListViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/28/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "EmailListViewController.h"

#import "BarButtonItem.h"
#import "EmailAccount.h"
#import "EmailCell.h"
#import "EmailFolder.h"
#import "EmailMessage.h"
#import "EmailPreviewViewController.h"
#import "IMAPSessionManager.h"
#import "ResultsCell.h"

#import <CDRTranslucentSideBar/CDRTranslucentSideBar.h>
#import <MailCore/MailCore.h>
#import <MJRefresh/MJRefresh.h>

@interface EmailListViewController () <CDRTranslucentSideBarDelegate>

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) UITableViewCell *loadingCell;
@property (nonatomic, strong) ResultsCell *resultsCell;

@property (nonatomic, strong) NSFetchedResultsController *foldersFRC;
@property (nonatomic, strong) NSFetchedResultsController *messagesFRC;
@property (nonatomic, assign) BOOL removeMessageLimit;

@property (nonatomic, strong) IMAPSessionManager *sessionManager;
@property (nonatomic, strong) EmailFolder *currentFolder;
@property (nonatomic, strong) CDRTranslucentSideBar *sideBar;

@property (nonatomic, assign) BOOL isRefreshingFolders;
@property (nonatomic, strong) NSMutableDictionary *isRefreshingMessages;
@property (nonatomic, strong) NSMutableDictionary *isLoadingMessages;

@end

@implementation EmailListViewController

- (void)viewDidLoad
{
    self.loadingCell = [self.tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
    self.resultsCell = [self.tableView dequeueReusableCellWithIdentifier:@"resultsCell"];
    [self.resultsCell addTarget:self action:@selector(fetchMoreMessages)];

    // Initialize "INBOX" as the current folder
    self.currentFolder = [self.emailAccount getFolderWithName:@"INBOX"];

    // Create initial dictionaries for folders stored
    [self updateMessagesRefresh];
    [self updateMessagesLoading];

    [super viewDidLoad];

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];

    [self.sideTableView setSeparatorInset:UIEdgeInsetsZero];
    [self.sideTableView setLayoutMargins:UIEdgeInsetsZero];

    self.dateFormatter = [NSDateFormatter new];
    [self.dateFormatter setDateFormat:@"M/d/yyyy"];

    self.sideBar = [[CDRTranslucentSideBar alloc] initWithDirectionFromRight:YES];
    self.sideBar.delegate = self;
    self.sideBar.sideBarWidth = self.sideView.frame.size.width;
    [self.sideBar setContentViewInSideBar:self.sideView];

    self.sideView.backgroundColor = [UIColor whiteColor];
    [self.sideView setShadowWithRadius:2.0f withColor:[UIColor blackColor] withOffset:CGSizeMake(-0.25f, 1.0f) withOpacity:0.75f];

    __weak __typeof(self)weakSelf = self;
    MJRefreshNormalHeader *foldersHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^ {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        if (!strongSelf.isRefreshingFolders) {
            [strongSelf fetchIMAPFolders];
        }
    }];
    foldersHeader.lastUpdatedTimeLabel.hidden = YES;
    foldersHeader.stateLabel.hidden = YES;
    foldersHeader.arrowView.image = [foldersHeader.arrowView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    foldersHeader.arrowView.tintColor = [ThemeLoader colorForKey:@"EmailScreen.Folders.TextColor"];
    foldersHeader.activityIndicatorViewStyle = [ThemeLoader boolForKey:@"EmailScreen.Folders.Activity.isLight"] ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
    self.sideTableView.mj_header = foldersHeader;

    MJRefreshNormalHeader *messagesHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^ {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        if ([strongSelf.isRefreshingMessages[strongSelf.currentFolder.folderName] isEqual:@NO]) {
            [strongSelf fetchNewMessages];
        }
    }];
    messagesHeader.lastUpdatedTimeLabel.hidden = YES;
    messagesHeader.stateLabel.hidden = YES;
    self.scrollView.mj_header = messagesHeader;

    self.sessionManager = [IMAPSessionManager new];
    [self.sessionManager setupUsingEmailAccount:self.emailAccount];
    [self fetchIMAPFolders];
    [self.scrollView.mj_header beginRefreshing];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Update the tables when the view will appear, we must do this here to reload/recreate the fetch controllers
    [self updateFolderTableView];

    [self messagesFRC];
    [self updateMessageTableViewWithReload:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Email List Screen";

    if (!self.removeMessageLimit) {
        self.removeMessageLimit = YES;
        _messagesFRC.delegate = nil;
        _messagesFRC = nil;

        [self updateMessageTableViewWithReload:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    [self updateNavigationTitle];
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.foldersButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.FolderButton.Image"]]];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.BackgroundColor"];
    self.sideTableView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.Folders.BackgroundColor"];

}

- (void)updateNavigationTitle
{
    if (self.currentFolder) {
        self.navigationItem.title = [self localizeFolder:self.currentFolder.folderName];
    }
}

- (NSString *)localizeFolder:(NSString *)folderName
{
    if ([folderName.lowercaseString isEqualToString:@"inbox"]) {
        return NSLocalizedPONString(@"TITLE_INBOX", nil);
    } else {
        return folderName;
    }
}

- (IBAction)openFolders
{
    if (self.sideBar.hasShown) {
        [self.sideBar dismissAnimated:YES];
    } else {
        [self.sideBar showInViewController:self animated:YES];
    }
}

- (void)changeToFolder:(EmailFolder *)folder
{
    [self.scrollView.mj_header endRefreshing];
    self.currentFolder = folder;
    [self updateNavigationTitle];

    _messagesFRC.delegate = nil;
    _messagesFRC = nil;
    self.removeMessageLimit = NO;

    [self messagesFRC];

    [self updateMessageTableViewWithReload:YES];
    [self.scrollView.mj_header beginRefreshing];
}

- (void)fetchIMAPFolders
{
    self.isRefreshingFolders = YES;

    __weak __typeof(self)weakSelf = self;
    [self.sessionManager fetchAllFoldersForAccount:self.emailAccount completionBlock:^(NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf.sessionManager showError:error overController:strongSelf.navigationController];
        }

        [strongSelf updateMessagesRefresh];
        [strongSelf updateMessagesLoading];
        [strongSelf.sideTableView.mj_header endRefreshing];
        strongSelf.isRefreshingFolders = NO;
    }];
}

- (void)updateMessagesRefresh
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (EmailFolder *folder in self.emailAccount.emailFolder) {
        dict[folder.folderName] = [self.isRefreshingMessages objectForKey:folder.folderName] ? self.isRefreshingMessages[folder.folderName] : @NO;
    }
    self.isRefreshingMessages = dict;
}

- (void)updateMessagesLoading
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (EmailFolder *folder in self.emailAccount.emailFolder) {
        dict[folder.folderName] = [self.isLoadingMessages objectForKey:folder.folderName] ? self.isLoadingMessages[folder.folderName] : @NO;
    }
    self.isLoadingMessages = dict;
}

- (void)fetchNewMessages
{
    [self fetchMessagesForFolder:self.currentFolder count:10 newOnly:YES includeUID:YES isRefresh:YES];
}

- (void)fetchMoreMessages
{
    if ([self.isLoadingMessages[self.currentFolder.folderName] isEqual:@NO]) {
        [self fetchMessagesForFolder:self.currentFolder count:25 newOnly:NO includeUID:YES isRefresh:NO];
    }
}

- (void)fetchMessagesForFolder:(EmailFolder *)folder count:(NSUInteger)count newOnly:(BOOL)new includeUID:(BOOL)uid isRefresh:(BOOL)refresh
{
    if (refresh) {
        self.isRefreshingMessages[folder.folderName] = @YES;
    } else {
        self.isLoadingMessages[folder.folderName] = @YES;
        [self updateMessageTableViewWithReload:NO];
    }

    __weak __typeof(self)weakSelf = self;
    void (^messagesBlock)(EmailFolder *folder, NSError *error) = ^(EmailFolder *folder, NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        // If we don't have enough messages left in the table to fill a page because messages were deleted we need to fetch some older
        // messages to keep a page visible in the table.
        if (strongSelf.messagesFRC.fetchedObjects.count < 10 && folder.emailMessage.count > strongSelf.messagesFRC.fetchedObjects.count) {
            NSUInteger numToFetch = 10 - strongSelf.messagesFRC.fetchedObjects.count;
            [strongSelf fetchMessagesForFolder:folder count:numToFetch newOnly:NO includeUID:NO isRefresh:YES];
            return;
        }

        if (error) {
            [strongSelf.sessionManager showError:error overController:strongSelf.navigationController];
        }

        if (refresh) {
            strongSelf.isRefreshingMessages[folder.folderName] = @NO;
            [strongSelf.scrollView.mj_header endRefreshing];
        } else {
            strongSelf.isLoadingMessages[folder.folderName] = @NO;
        }

        [strongSelf updateMessageTableViewWithReload:NO];
    };
    void (^uidsBlock)(EmailFolder *folder, NSError *error) = ^(EmailFolder *folder, NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf.sessionManager showError:error overController:strongSelf.navigationController];
        }

        [strongSelf.sessionManager fetchNextMessagesForFolder:folder count:count newOnly:new completionBlock:messagesBlock];
    };

    if (uid) {
        [self.sessionManager fetchAllMessageUIDsForFolder:folder withAccount:self.emailAccount completionBlock:uidsBlock];
    } else {
        [self.sessionManager fetchNextMessagesForFolder:folder count:count newOnly:new completionBlock:messagesBlock];
    }
}

- (void)updateMessageTableViewWithReload:(BOOL)reload {
    if (reload) {
        [self.tableView reloadData];
    }

    if ([self.isLoadingMessages[self.currentFolder.folderName] isEqual:@YES]) {
        [self.tableView setTableFooterView:self.loadingCell.contentView];
        [self.tableView scrollRectToVisible:[self.tableView convertRect:self.tableView.tableFooterView.bounds fromView:self.tableView.tableFooterView] animated:NO];
    } else {
        if (self.currentFolder.emailMessage.count > self.messagesFRC.fetchedObjects.count) {
            [self.resultsCell setupCell:[NSString stringWithFormat:NSLocalizedPONString(@"%d of %d", nil), self.messagesFRC.fetchedObjects.count, self.currentFolder.emailMessage.count] isTap:YES];
        } else {
            [self.resultsCell setupCell:[NSString stringWithFormat:NSLocalizedPONString(@"%d", nil), self.messagesFRC.fetchedObjects.count] isTap:NO];
        }
        [self.tableView setTableFooterView:self.resultsCell.contentView];
    }

    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)foldersFRC {
    if (!_foldersFRC) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"EmailFolder"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"emailAccount == %@", self.emailAccount];
        fetchRequest.predicate = predicate;
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]];

        self.foldersFRC = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.foldersFRC.delegate = self;

        [self performFolderFetch];
    }

    return _foldersFRC;
}

- (void)performFolderFetch {
    NSError *error;
    [self.foldersFRC performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateFolderTableView {
    [self.sideTableView reloadData];
}

- (NSFetchedResultsController *)messagesFRC {
    if (!_messagesFRC) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"emailFolder == %@ && message != nil", self.currentFolder];

        BOOL restrictFetch = NO;
        if (!self.removeMessageLimit) {
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"EmailMessage"];
            fetchRequest.includesSubentities = NO;
            fetchRequest.predicate = predicate;
            NSUInteger count = [RKManagedObjectStore.defaultStore.mainQueueManagedObjectContext countForFetchRequest:fetchRequest error:nil];
            restrictFetch = count > 25;
        }

        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"EmailMessage"];
        fetchRequest.predicate = predicate;
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"uid" ascending:NO]];
        if (restrictFetch) {
            fetchRequest.fetchLimit = 25;
        } else {
            self.removeMessageLimit = YES;
        }

        self.messagesFRC = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:RKManagedObjectStore.defaultStore.mainQueueManagedObjectContext sectionNameKeyPath:nil cacheName:nil];
        self.messagesFRC.delegate = self;

        [self performMessageFetch];
    }

    return _messagesFRC;
}

- (void)performMessageFetch {
    NSError *error;
    [self.messagesFRC performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    //Lets the tableview know we're potentially doing a bunch of updates.
    if (controller != self.foldersFRC) {
        [self.tableView beginUpdates];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    //We're finished updating the tableview's data.
    if (controller != self.foldersFRC) {
        [self.tableView endUpdates];
        [self updateContentHeight];
    } else {
        [self updateFolderTableView];
        //self.sideTableView.contentSize = [self.sideTableView sizeThatFits:CGSizeMake(CGRectGetWidth(self.sideTableView.bounds), CGFLOAT_MAX)];
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    if (controller == self.foldersFRC) {
        // If the current selected folder is removed on a refresh we change back to the "Inbox"
        if (type == NSFetchedResultsChangeDelete && self.sideTableView.indexPathForSelectedRow.section == indexPath.section && self.sideTableView.indexPathForSelectedRow.row == indexPath.row) {
            EmailFolder *folder = [self.emailAccount getFolderWithName:@"INBOX"];
            [self.sideTableView selectRowAtIndexPath:[self.foldersFRC indexPathForObject:folder] animated:YES scrollPosition:UITableViewScrollPositionNone];
            [self changeToFolder:folder];
        }
        return;
    }

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete: {
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
            
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {

    if (controller == self.foldersFRC) return;

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSFetchedResultsController *results = tableView == self.tableView ? self.messagesFRC : self.foldersFRC;
    return [[results sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSFetchedResultsController *results = tableView == self.tableView ? self.messagesFRC : self.foldersFRC;
    id <NSFetchedResultsSectionInfo> sectionInfo = [results sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView) {
        static NSString *cellIdentifier = @"messageCell";
        EmailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

        EmailMessage *message = [self.messagesFRC objectAtIndexPath:indexPath];
        [cell setupIMAPCell:message.message withFormatter:self.dateFormatter];

        // Add a separator line at the bottom of each cell if it doesn't exist already
        if (cell.contentView.subviews.count == 4) {
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width, 1)];
            lineView.backgroundColor = [UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1];
            [cell.contentView addSubview:lineView];
        }

        return cell;
    } else {
        static NSString *folderCellIdentifier = @"folderCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:folderCellIdentifier forIndexPath:indexPath];

        EmailFolder *folder = [self.foldersFRC objectAtIndexPath:indexPath];

        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.text = [self localizeFolder:folder.folderName];
        cell.textLabel.textColor = [ThemeLoader colorForKey:@"EmailScreen.Folders.TextColor"];
        cell.textLabel.highlightedTextColor = [ThemeLoader colorForKey:@"EmailScreen.Folders.TextHighlightedColor"];
        UIView *backgroundView = [UIView new];
        cell.selectedBackgroundView = backgroundView;
        cell.selectedBackgroundView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.Folders.HighlightColor"];

        if ([folder.folderName isEqualToString:self.currentFolder.folderName]) {
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.sideTableView) {
        EmailFolder *folder = [self.foldersFRC objectAtIndexPath:indexPath];
        if (![folder.folderName isEqualToString:self.currentFolder.folderName]) {
            [self changeToFolder:folder];
        }

        [self.sideBar dismissAnimated:YES];
    }
    return indexPath;
}

#pragma mark - CDRTranslucentSideBarDelegate

- (void)sideBar:(CDRTranslucentSideBar *)sideBar willAppear:(BOOL)animated
{
    self.foldersButton.enabled = NO;

    // If folders are being refreshed make sure we show the header
    if (self.isRefreshingFolders) {
        [self.sideTableView.mj_header beginRefreshing];
    } else {
        // Make sure the selected Folder is visible when we open the sidebar.  Only scroll if we aren't refreshing folders.
        // If the folders are refreshing the scrolling will start to mess up the scrollview height and push things down.
        [self.sideTableView scrollToRowAtIndexPath:[self.sideTableView indexPathForSelectedRow] atScrollPosition:UITableViewScrollPositionNone animated:NO];
    }
}

- (void)sideBar:(CDRTranslucentSideBar *)sideBar didAppear:(BOOL)animated
{
    self.foldersButton.enabled = YES;
}

- (void)sideBar:(CDRTranslucentSideBar *)sideBar willDisappear:(BOOL)animated
{
    self.foldersButton.enabled = NO;
}

- (void)sideBar:(CDRTranslucentSideBar *)sideBar didDisappear:(BOOL)animated
{
    if (!self.removeMessageLimit) {
        self.removeMessageLimit = YES;
        _messagesFRC.delegate = nil;
        _messagesFRC = nil;

        [self updateMessageTableViewWithReload:YES];
    }
    self.foldersButton.enabled = YES;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showEmailPreview"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

        EmailPreviewViewController *destViewController = segue.destinationViewController;
        destViewController.imapSession = self.sessionManager;
        destViewController.emailMessage = (EmailMessage *)[self.messagesFRC objectAtIndexPath:indexPath];
        destViewController.imapFolder = self.currentFolder.folderName;
    }
}

@end
