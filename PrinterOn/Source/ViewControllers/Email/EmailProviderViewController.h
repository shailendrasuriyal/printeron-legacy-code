//
//  EmailProviderViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-02-25.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@interface EmailProviderViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
