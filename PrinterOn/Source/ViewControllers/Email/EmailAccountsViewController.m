//
//  EmailAccountsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/26/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "EmailAccountsViewController.h"

#import "AccountCell.h"
#import "BarButtonItem.h"
#import "EmailAccount.h"
#import "EmailListViewController.h"
#import "EmailSetupViewController.h"

@interface EmailAccountsViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation EmailAccountsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // Compute the shadow paths after layout
        self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Email Accounts Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;

    self.tableView.editing = NO;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_EMAILACCOUNTS", nil);
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.editButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.EditButton.Image"]]];

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.BackgroundColor"];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];
        
        // Setup NavigationBar appearance
        [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
        [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
        self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
        self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
        self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
        self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    }

    // Setup the "Add Service" appearance
    [self.addEmailButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.AddButton.Image"]] forState:UIControlStateNormal];
    self.addView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.Toolbar.BackgroundColor"];
    self.addView.layer.cornerRadius = self.addView.frame.size.width / 2;
    self.addView.layer.masksToBounds = NO;
    self.addView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.addView.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.addView.layer.shadowOpacity = 0.5f;
    self.addView.layer.shadowRadius = 1.5f;
    self.addView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.addView.bounds cornerRadius:self.addView.layer.cornerRadius].CGPath;
}

- (IBAction)closePressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)editPressed
{
    [UIView animateWithDuration:0.15f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.tableView.editing = !self.tableView.editing;
    } completion:nil];
}


#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"EmailAccount"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"emailAddress" ascending:YES]];

        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;
        
        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];

    if (self.fetchedResultsController.fetchedObjects.count == 0) {
        [self.editButton setHidden:YES];
        self.tableView.editing = NO;
    } else {
        [self.editButton setHidden:NO];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    //Lets the tableview know we're potentially doing a bunch of updates.
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    //We're finished updating the tableview's data.
    [self.tableView endUpdates];
    self.tableView.contentSize = [self.tableView sizeThatFits:CGSizeMake(CGRectGetWidth(self.tableView.bounds), CGFLOAT_MAX)];
    [self updateContentHeight];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    if (type == NSFetchedResultsChangeMove && [indexPath isEqual:newIndexPath]) {
        type = NSFetchedResultsChangeUpdate;
    }

    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"emailAccountCell";
    AccountCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    NSArray *images = @[@"iCloudMail",@"GoogleMail",@"OutlookMail",@"YahooMail",@"OtherMail"];
    EmailAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCell:account.emailAddress withImage:[[ImageManager sharedImageManager] imageNamed:images[[account.mailType intValue]]] showLock:NO];

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        EmailAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];

        if (account) {
            [account clearAttachmentsCacheForAccount];

            // iOS 8 bug in FRC with a cascade delete means we need to disable the delegate during a delete
            _fetchedResultsController.delegate = nil;
            _fetchedResultsController = nil;

            [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext deleteObject:account];
            [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];

            [self updateTableView];
        }
    }
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.editing) {
        [self performSegueWithIdentifier:@"showEmailSetup" sender:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        return nil;
    }

    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showEmailList"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        EmailListViewController *destViewController = segue.destinationViewController;
        destViewController.emailAccount = [self.fetchedResultsController objectAtIndexPath:indexPath];
    } else if ([segue.identifier isEqualToString:@"showEmailSetup"]) {
        EmailSetupViewController *destViewController = segue.destinationViewController;
        destViewController.emailAccount = sender;
    }
}

@end
