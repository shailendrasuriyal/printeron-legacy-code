//
//  EmailPreviewViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/28/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class EmailMessage, IMAPSessionManager;

@interface EmailPreviewViewController : BaseViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet UIButton *attachmentsButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *printButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *printButton;

@property (nonatomic, strong) IMAPSessionManager *imapSession;
@property (nonatomic, strong) EmailMessage *emailMessage;
@property (nonatomic, strong) NSString *imapFolder;

@end
