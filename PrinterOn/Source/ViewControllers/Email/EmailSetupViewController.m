//
//  EmailSetupViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/26/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "EmailSetupViewController.h"

#import "AppConstants.h"
#import "AppDelegate.h"
#import "BarButtonItem.h"
#import "EmailAccount.h"
#import "EmailAccountsViewController.h"
#import "HMSegmentedControl.h"
#import "IMAPSessionManager.h"
#import "JCActivityBar.h"
#import "OAuth2Manager.h"
#import "TPKeyboardAvoidingScrollView.h"

#import "AppAuth.h"

@interface EmailSetupViewController ()

@property (strong, nonatomic) HMSegmentedControl *segmentedType;
@property (strong, nonatomic) JCActivityBar *activityBar;
@property (strong, nonatomic) IMAPSessionManager *sessionManager;

@end

@implementation EmailSetupViewController

- (void)viewDidLoad
{
    // Create and position the segmented control before we call super
    self.segmentedType = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"IMAP"]];
    self.segmentedType.titleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:17.0f]};
    self.segmentedType.selectedTitleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:17.0f],  NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedType.selectionIndicatorColor = [UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    self.segmentedType.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentedType.selectionIndicatorBoxOpacity = 1.0f;
    self.segmentedType.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
    self.segmentedType.selectedSegmentIndex = 0;
    self.segmentedType.shouldAnimateUserSelection = YES;
    [self.segmentedType addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.hostView addSubview:self.segmentedType];
    
    // Create the ActivityBar
    self.activityBar = [JCActivityBar new];
    [self.activityBar positionInBottomOfView:self.backgroundView withBottomOffset:24.0];
    [self.backgroundView addSubview:self.activityBar];

    [super viewDidLoad];

    // Adjust switch location because the control is a different size
    [self.switchSSL setFrame:CGRectMake(self.switchSSL.frame.origin.x + 6, self.switchSSL.frame.origin.y - 2, self.switchSSL.frame.size.width, self.switchSSL.frame.size.height)];

    if (self.emailAccount) {
        self.accountType = self.emailAccount.mailType.intValue;
        if (self.emailAccount.isUsingOAuth2) {
            self.auth = [self.emailAccount getOAuth2Authentication];
            [self removePasswordFields];
        } else {
            [self.textPassword setText:[self.emailAccount getEmailAccountPassword]];
        }
        [self.textEmailAddress setText:self.emailAccount.emailAddress];
        [self.textEmailAddress setEnabled:NO];
        self.segmentedType.selectedSegmentIndex = (self.emailAccount.protocol.intValue == EmailAccountProtocolIMAP) ? 0 : 1;
        [self.textHostName setText:self.emailAccount.hostName];
        [self.textPort setText:self.emailAccount.port.stringValue];
        [self.switchSSL setOn:self.emailAccount.useSSL.boolValue];
    } else {
        [self setupForAccountType];
        [self updatePortText];
    }

    [self validateFields];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textEmailAddress];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textPassword];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textHostName];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textPort];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Email Setup Screen";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textEmailAddress];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textPassword];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textHostName];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textPort];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGFloat xCoord = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 156 : 139;
    [self.segmentedType setFrame:CGRectMake(xCoord, 10, self.hostView.frame.size.width - self.labelType.frame.size.width - 24, 30)];

    // Set the content size of the scroll view
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.hostView.frame.origin.y + self.hostView.frame.size.height + 12);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_EMAILSETUP", nil);
    [self.buttonAuth setTitle:NSLocalizedPONString(@"LABEL_REAUTHENTICATE", nil) forState:UIControlStateNormal];
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.saveButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.SelectButton.Image"]]];

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.BackgroundColor"];
    self.buttonAuth.layer.cornerRadius = 4.0f;

    THEME_KEYBOARD(self.textEmailAddress);
    THEME_KEYBOARD(self.textPassword);
    THEME_KEYBOARD(self.textHostName);
    THEME_KEYBOARD(self.textPort);
}

- (IBAction)saveAction:(id)sender
{
    [self setControlStates:NO];
    [self.backgroundView endEditing:YES];
    [self.activityBar displayActivityWithMessage:NSLocalizedPONString(@"LABEL_VERIFYACCOUNT", nil)];

    if (self.segmentedType.selectedSegmentIndex == EmailAccountProtocolIMAP) {
        self.sessionManager = [IMAPSessionManager new];

        [self.sessionManager setupWithHost:self.textHostName.text port:[self.textPort.text intValue] userName:self.textEmailAddress.text password:self.textPassword.text auth:self.auth ssl:self.switchSSL.isOn type:self.accountType account:self.emailAccount];

        __weak __typeof(self) weakSelf = self;
        [self.sessionManager checkAccountWithCompletionBlock:^(NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;
            
            strongSelf.sessionManager = nil;
            [strongSelf checkAccountResponse:error];
        }];
    }
}

- (IBAction)reAuthAction {
    [self.backgroundView endEditing:YES];

    __weak __typeof(self) weakSelf = self;
    GTMOAuth2ViewControllerCompletionHandler handler = ^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error)
    {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        BOOL userCancelled = error && ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorWindowClosed);
        if (userCancelled) return;

        if (error) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [strongSelf presentViewController:alertVC animated:YES completion:nil];
        } else {
            strongSelf.auth = auth;
            dispatch_async(dispatch_get_main_queue(), ^{
                [strongSelf validateFields];
            });
        }
    };

    OIDAuthStateAuthorizationCallback callback = ^(OIDAuthState *_Nullable authState, NSError *_Nullable error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appDelegate.currentAuthorizationBlock = nil;
        appDelegate.currentAuthorizationFlow = nil;

        BOOL userCancelled = error && ([error.domain isEqualToString:OIDGeneralErrorDomain] && (error.code == OIDErrorCodeUserCanceledAuthorizationFlow || error.code == OIDErrorCodeProgramCanceledAuthorizationFlow));
        if (userCancelled) {
            return;
        }

        if (error) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [strongSelf presentViewController:alertVC animated:YES completion:nil];
        } else {
            strongSelf.auth = authState;
            dispatch_async(dispatch_get_main_queue(), ^{
                [strongSelf validateFields];
            });
        }
    };

    switch (self.accountType) {
        case EmailAccountTypeGMail: {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            appDelegate.currentAuthorizationBlock = nil;
            appDelegate.currentAuthorizationFlow = [OAuth2Manager showGoogleAuthOnViewController:self withClientID:[AppConstants googleOAuth2ClientID] callback:callback];
            break;
        }
        case EmailAccountTypeOutlook: {
            GTMOAuth2ViewControllerTouch *vc = [OAuth2Manager viewControllerForOutlook:handler clientID:[AppConstants outlookOAuth2ClientID] clientSecret:[AppConstants outlookOAuth2ClientSecret]];
            vc.navigationItem.title = self.navigationItem.title;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        default:
            break;
    }
}

- (void)checkAccountResponse:(NSError *)error
{
    if (error) {
        [self.activityBar finishWithError:error.localizedDescription];
        [self setControlStates:YES];
    } else {
        [self.activityBar finishWithSuccess:NSLocalizedPONString(@"LABEL_SUCCESS_SETTINGS", nil)];
        [EmailAccount createEmailAccount:self.textEmailAddress.text password:self.textPassword.text auth:self.auth host:self.textHostName.text port:[self.textPort.text intValue] ssl:self.switchSSL.isOn protocol:(int)self.segmentedType.selectedSegmentIndex type:self.accountType];

        NSMutableArray<UIViewController *> *vcs = [NSMutableArray array];
        for (UIViewController *vc in self.navigationController.viewControllers) {
            [vcs addObject:vc];
            if ([vc isMemberOfClass:[EmailAccountsViewController class]]) {
                break;
            }
        }
        [self.navigationController setViewControllers:vcs animated:YES];
    }
}

- (void)setupForAccountType
{
    switch (self.accountType) {
        case EmailAccountTypeICloud:
        case EmailAccountTypeGMail:
        case EmailAccountTypeOutlook:
        case EmailAccountTypeYahoo: {
            if (self.auth) {
                self.textEmailAddress.text = [OAuth2Manager userEmailForAuth:self.auth];
                [self removePasswordFields];
            }
            self.textHostName.text = [EmailAccount getHostNameForType:self.accountType];
            self.switchSSL.on = YES;
            break;
        }
        case EmailAccountTypeOther:
        default: {
            break;
        }
    }
}

- (void)setControlStates:(BOOL)enabled
{
    [self.saveButtonItem setEnabled:enabled];
    [self.textEmailAddress setEnabled:enabled];
    [self.textPassword setEnabled:enabled];
    [self.segmentedType setEnabled:enabled];
    [self.textHostName setEnabled:enabled];
    [self.textPort setEnabled:enabled];
    [self.switchSSL setEnabled:enabled];
}

- (void)removePasswordFields
{
    self.labelPassword.hidden = YES;
    self.textPassword.hidden = YES;
    self.buttonAuth.hidden = NO;

    self.userAccountView.frame = CGRectMake(self.userAccountView.frame.origin.x, self.userAccountView.frame.origin.y, self.userAccountView.frame.size.width, self.buttonAuth.frame.origin.y + self.buttonAuth.frame.size.height + 10);
    self.hostView.frame = CGRectMake(self.hostView.frame.origin.x, self.userAccountView.frame.origin.y + self.userAccountView.frame.size.height + 12, self.hostView.frame.size.width, self.hostView.frame.size.height);
}

- (void)updatePortText
{
    [self.textPort setText:self.segmentedType.selectedSegmentIndex == EmailAccountProtocolIMAP ? self.switchSSL.isOn ? @"993" : @"143" : self.switchSSL.isOn ? @"995" : @"110"];
}

- (void)validateFields
{
    [self.saveButtonItem setEnabled:[self areFieldsValid]];
}

- (BOOL)areFieldsValid
{
    if ([self.textEmailAddress.text length] == 0 || [self.textEmailAddress.text rangeOfString:@"@"].location == NSNotFound)
        return NO;

    if (self.auth == nil && [self.textPassword.text length] == 0)
        return NO;

    if ([self.textHostName.text length] == 0 || [self.textHostName.text rangeOfString:@"."].location == NSNotFound)
        return NO;

    if ([self.textPort.text length] == 0)
        return NO;

    return YES;
}

- (void)textChanged:(NSNotification *)notification
{
    [self validateFields];
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl
{
    [self updatePortText];
}

- (IBAction)sslSwitchChanged:(id)sender
{
    [self updatePortText];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [self.scrollView textFieldShouldReturn:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *temp = [[textField.text stringByReplacingCharactersInRange:range withString:string] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    if ([textField.text isEqualToString:temp]) {
        return NO;
    }

    return YES;
}

@end
