//
//  EmailPreviewViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/28/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "EmailPreviewViewController.h"

#import "BarButtonItem.h"
#import "EmailAttachmentManager.h"
#import "EmailAttachmentsViewController.h"
#import "EmailDocument.h"
#import "EmailMessage.h"
#import "IMAPSessionManager.h"
#import "MainViewController.h"
#import "NSData+Transform.h"
#import "PreviewViewController.h"
#import "PrintJobManager.h"

#import <MailCore/MailCore.h>

@interface EmailPreviewViewController () <MCOHTMLRendererIMAPDelegate>

@property (nonatomic, strong) MCOIMAPMessage *imapMessage;

@end

@implementation EmailPreviewViewController

- (void)viewDidLoad
{
    self.imapMessage = self.emailMessage.message;
    [super viewDidLoad];

    [self.webView.scrollView setBounces:NO];
    [self.printButtonItem setEnabled:NO];

    self.loadingView.center = CGPointMake(self.backgroundView.frame.size.width / 2, self.webView.frame.origin.y + (self.webView.frame.size.height/2));

    [self.webView setHidden:YES];
    [self.attachmentsButton setHidden:YES];

    if (self.imapMessage.attachments.count > 0) {
        [self.webView setFrame:CGRectMake(self.webView.frame.origin.x, self.webView.frame.origin.y, self.webView.frame.size.width, self.webView.frame.size.height - self.attachmentsButton.frame.size.height)];
    }

    if (self.imapMessage && self.imapMessage.htmlInlineAttachments.count > 0) {
        [self fetchInlineImages];
    } else {
        [self renderHTML];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.webView setDelegate:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Email Message Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_MESSAGE", nil);
    [self.loadingLabel setText:NSLocalizedPONString(@"LABEL_LOADINGMESSAGE", nil)];
    [self.attachmentsButton setTitle:[NSString stringWithFormat:@"%lu %@", (unsigned long)self.imapMessage.attachments.count, self.imapMessage.attachments.count == 1 ? NSLocalizedPONString(@"LABEL_ATTACHMENT", nil) : NSLocalizedPONString(@"LABEL_ATTACHMENTS", nil)] forState:UIControlStateNormal];
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.printButton withImage:[[ImageManager sharedImageManager] imageNamed:@"PrintButton"]];

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.BackgroundColor"];

    self.loadingView.layer.borderWidth = 1.5f;
    self.loadingView.layer.borderColor = [UIColor blackColor].CGColor;
    self.loadingView.layer.cornerRadius = 10;
    self.loadingView.layer.masksToBounds = NO;
    self.loadingView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.loadingView.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    self.loadingView.layer.shadowOpacity = 0.75f;
    self.loadingView.layer.shadowRadius = 2.0f;

    // Set ToolBar appearance
    self.attachmentsButton.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.Toolbar.BackgroundColor"];
    self.attachmentsButton.tintColor = [ThemeLoader colorForKey:@"EmailScreen.Toolbar.TextColor"];
    [self.attachmentsButton setTitleColor:[ThemeLoader colorForKey:@"EmailScreen.Toolbar.TextColor"] forState:UIControlStateNormal];
    [self.attachmentsButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"EmailScreen.Attachments.Image"]] forState:UIControlStateNormal];

    self.attachmentsButton.layer.shadowOpacity = 0.75f;
    self.attachmentsButton.layer.shadowRadius = 1.5f;
    self.attachmentsButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.attachmentsButton.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);
}

- (IBAction)printPressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

    EmailDocument *document = [[EmailDocument alloc] initWithWebView:self.webView withIMAP:self.imapMessage];
    [[PrintJobManager sharedPrintJobManager].mainView showPrintPreview:document];
}

#pragma mark - MailCore HTML

- (void)renderHTML
{
    if (self.emailMessage == nil || self.imapMessage == nil) return;

    int partCount = 0;
    for (MCOIMAPPart *part in self.imapMessage.requiredPartsForRendering) {
        NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:[self.imapSession getEmailAccount].emailAddress messageID:[NSString stringWithFormat:@"%d", self.imapMessage.uid] partID:part.partID fileName:part.filename];
        id attachment = [[EmailAttachmentManager sharedEmailAttachmentManager] progressForAttachment:attachID];
        if (attachment == [NSNull null]) partCount++;
    }
    
    if (partCount != self.imapMessage.requiredPartsForRendering.count) {
        __weak __typeof(self)weakSelf = self;
        [self.imapSession fetchContentsForMessage:self.emailMessage completionBlock:^(EmailMessage *message, NSError *error) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            if (error) {
                [strongSelf.imapSession showError:error overController:strongSelf.navigationController];
                [strongSelf.loadingView setHidden:YES];
                [strongSelf.webView setHidden:YES];
                [strongSelf.attachmentsButton setHidden:strongSelf.imapMessage.attachments.count == 0];
                return;
            }

            NSString *msgHTMLBody = [self.imapMessage htmlRenderingWithFolder:self.imapFolder delegate:self];
            [strongSelf.webView loadHTMLString:msgHTMLBody baseURL:nil];
        }];
    } else {
        NSString *msgHTMLBody = [self.imapMessage htmlRenderingWithFolder:self.imapFolder delegate:self];
        [self.webView loadHTMLString:msgHTMLBody baseURL:nil];
    }
}

- (void)fetchInlineImages
{
    if (self.imapSession && self.imapMessage) {
        __weak __typeof(self)weakSelf = self;

        __block int count = 0;
        for (MCOIMAPPart *part in self.imapMessage.htmlInlineAttachments) {
            NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:[self.imapSession getEmailAccount].emailAddress messageID:[NSString stringWithFormat:@"%d", self.imapMessage.uid] partID:part.partID fileName:part.filename];
            id attachment = [[EmailAttachmentManager sharedEmailAttachmentManager] progressForAttachment:attachID];

            if (attachment == nil) {
                NSProgress *progress = [[EmailAttachmentManager sharedEmailAttachmentManager] startAttachment:attachID];

                [self.imapSession fetchMessageAttachmentWithFolder:self.imapFolder uid:self.imapMessage.uid partID:part.partID encoding:part.encoding filename:part.filename progress:progress completionBlock:^(NSData *data, NSError *error) {
                    __strong __typeof(weakSelf) strongSelf = weakSelf;
                    if (!strongSelf) return;

                    count++;

                    if (error) {
                        [strongSelf.imapSession showError:error overController:strongSelf.navigationController];
                    }

                    if (count == strongSelf.imapMessage.htmlInlineAttachments.count) {
                        [strongSelf renderHTML];
                    }
                }];
            } else {
                count++;
            }
        }

        if (count == self.imapMessage.htmlInlineAttachments.count) {
            [self renderHTML];
        }
    }
}

- (void)injectInlineImagesScript:(UIWebView *)webView
{
    [webView stringByEvaluatingJavaScriptFromString:@"var script = document.createElement('script');" \
     "script.type = 'text/javascript';" \
     "script.text = \"var imageElements = function() { " \
        "var imageNodes = document.getElementsByTagName('img'); " \
        "return [].slice.call(imageNodes); " \
     "}; " \
     "var findCIDImageURL = function() { " \
         "var images = imageElements(); " \
         "var imgLinks = []; " \
         "for (var i = 0; i < images.length; i++) { " \
             "var url = images[i].getAttribute('src'); " \
             "if (url.indexOf('cid:') == 0 || url.indexOf('x-mailcore-image:') == 0) " \
                 "imgLinks.push(url); " \
         "} " \
         "return JSON.stringify(imgLinks); " \
     "}; " \
     "var replaceImageSrc = function(info) { " \
        "var images = imageElements(); " \
        "for (var i = 0; i < images.length; i++) { " \
            "var url = images[i].getAttribute('src'); " \
            "if (url.indexOf(info.URLKey) == 0) { " \
                "images[i].setAttribute('src', info.InlineDataKey); " \
                "break; " \
            "} " \
        "} " \
     "};\";" \
     "document.getElementsByTagName('head')[0].appendChild(script);"];
}

- (void)loadInlineImages:(UIWebView *)webView
{
    [self injectInlineImagesScript:webView];

    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"findCIDImageURL()"];
    NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *imagesURLStrings = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    for (NSString *urlString in imagesURLStrings) {
        MCOIMAPPart *part = nil;
        NSURL *url = [NSURL URLWithString:urlString];
        if ([self isCID:url]) {
            part = (MCOIMAPPart *)[self.imapMessage partForContentID:url.resourceSpecifier];
        } else if ([self isXMailcoreImage:url]) {
            part = (MCOIMAPPart *)[self.imapMessage partForUniqueID:url.resourceSpecifier];
        }

        if (part == nil) continue;

        NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:[self.imapSession getEmailAccount].emailAddress messageID:[NSString stringWithFormat:@"%d", self.imapMessage.uid] partID:part.partID fileName:part.filename];
        NSURL *attachURL = [[EmailAttachmentManager sharedEmailAttachmentManager] retrieveURLForAttachment:attachID];

        if ([[NSFileManager defaultManager] fileExistsAtPath:attachURL.path]) {
            NSMutableData *data = [NSMutableData dataWithData:[[NSString stringWithFormat:@"data:%@;base64,", part.mimeType] dataUsingEncoding:NSUTF8StringEncoding]];
            [data appendData:[[NSData dataWithContentsOfFile:attachURL.path] base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength]];
            NSDictionary *args = @{@"URLKey": urlString, @"InlineDataKey": [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]};
            data = nil;
            NSString *jsonString = [NSString stringWithFormat:@"replaceImageSrc(%@)", [self jsonEscapedStringFromDictionary:args]];
            args = nil;

            [webView stringByEvaluatingJavaScriptFromString:jsonString];
        }
    }
}

- (BOOL)isCID:(NSURL *)url
{
    return [url.scheme caseInsensitiveCompare:@"cid"] == NSOrderedSame;
}

- (BOOL)isXMailcoreImage:(NSURL *)url
{
    return [url.scheme caseInsensitiveCompare:@"x-mailcore-image"] == NSOrderedSame;
}

- (NSString *)jsonEscapedStringFromDictionary:(NSDictionary *)dictionary
{
    NSData *json = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    return jsonString;
}

#pragma mark - MCOHTMLRendererIMAPDelegate

- (NSData *)MCOAbstractMessage:(MCOAbstractMessage *)msg dataForIMAPPart:(MCOIMAPPart *)part folder:(NSString *)folder
{
    NSString *attachID = [[EmailAttachmentManager sharedEmailAttachmentManager] uniqueAttachment:[self.imapSession getEmailAccount].emailAddress messageID:[NSString stringWithFormat:@"%d", self.imapMessage.uid] partID:part.partID fileName:part.filename];
    NSURL *attachURL = [[EmailAttachmentManager sharedEmailAttachmentManager] retrieveURLForAttachment:attachID];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:attachURL.path]) {
        NSData *data = [NSData dataWithContentsOfFile:attachURL.path];
        [data transform];
        return data;
    } else {
        return NULL;
    }
}

- (NSString *)MCOAbstractMessage:(MCOAbstractMessage *)msg templateForAttachment:(MCOAbstractPart *)part
{
    return @"";
}

- (NSString *)MCOAbstractMessage_templateForAttachmentSeparator:(MCOAbstractMessage *)msg
{
    return @"";
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }

    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // If the page is wider then the webview we scale it to fit
    // Can't use scalesToFit = YES of the webview, for some reason it screws the scaling up
    if (webView.scrollView.contentSize.width > webView.frame.size.width) {
        NSString *js = [NSString stringWithFormat:@"var meta = document.createElement('meta'); "  \
        "meta.setAttribute( 'name', 'viewport' ); "  \
        "meta.setAttribute( 'content', 'width = device-width, initial-scale = %f, minimum-scale=0.1, maximum-scale=5.0, user-scalable = yes' ); "  \
        "document.getElementsByTagName('head')[0].appendChild(meta)", webView.frame.size.width / webView.scrollView.contentSize.width];
        [webView stringByEvaluatingJavaScriptFromString:js];
    }

    float zoom = webView.frame.size.width / webView.scrollView.contentSize.width;
    NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.zoom = %f;", zoom];
    [webView stringByEvaluatingJavaScriptFromString:jsCommand];

    if (self.imapMessage.htmlInlineAttachments.count > 0) {
        [self loadInlineImages:webView];
    }

    [self.loadingView setHidden:YES];
    [self.webView setHidden:NO];
    [self.attachmentsButton setHidden:self.imapMessage.attachments.count == 0];
    self.printButtonItem.enabled = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // NSURLErrorDomain code -999 means another request was made before the previous completed
    if ([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
        [self webViewDidFinishLoad:webView];
        return;
    }

    [self.loadingView setHidden:YES];
    [self.webView setHidden:NO];
    [self.attachmentsButton setHidden:self.imapMessage.attachments.count == 0];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPrintPreview"]) {
        EmailDocument *document = [[EmailDocument alloc] initWithWebView:self.webView withIMAP:self.imapMessage];
        
        PreviewViewController *destViewController = segue.destinationViewController;
        destViewController.document = document;
    } else if ([segue.identifier isEqualToString:@"showAttachments"]) {
        EmailAttachmentsViewController *destViewController = segue.destinationViewController;
        destViewController.imapSession = self.imapSession;
        destViewController.imapMessage = self.imapMessage;
        destViewController.imapFolder = self.imapFolder;
    }
}

@end
