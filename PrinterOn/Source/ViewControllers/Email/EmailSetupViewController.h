//
//  EmailSetupViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/26/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "EmailAccount.h"

@class TPKeyboardAvoidingScrollView;

@interface EmailSetupViewController : BaseViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *userAccountView;
@property (weak, nonatomic) IBOutlet UILabel *labelEmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *textEmailAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelPassword;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UIButton *buttonAuth;

@property (weak, nonatomic) IBOutlet ShadowView *hostView;
@property (weak, nonatomic) IBOutlet UILabel *labelType;
@property (weak, nonatomic) IBOutlet UILabel *labelHostName;
@property (weak, nonatomic) IBOutlet UITextField *textHostName;
@property (weak, nonatomic) IBOutlet UILabel *labelPort;
@property (weak, nonatomic) IBOutlet UITextField *textPort;
@property (weak, nonatomic) IBOutlet UILabel *labelSSL;
@property (weak, nonatomic) IBOutlet UISwitch *switchSSL;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (nonatomic, strong) EmailAccount *emailAccount;
@property (nonatomic, assign) EmailAccountType accountType;
@property (nonatomic, strong) id auth;

@end
