//
//  EmailLoadingViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-02-26.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "EmailLoadingViewController.h"

#import "EmailSetupViewController.h"

@interface EmailLoadingViewController ()

@property (nonatomic, assign) BOOL didAppear;
@property (nonatomic, strong) NSDictionary *passedValues;

@end

@implementation EmailLoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _didAppear = YES;

    if (self.passedValues) {
        [self showEmailSetupWithParameters:self.passedValues];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_EMAILSETUP", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.BackgroundColor"];
}

- (void)showEmailSetupWithParameters:(NSDictionary *)parameters
{
    if (!self.didAppear) {
        _passedValues = parameters;
        return;
    }

    NSMutableArray *vcs = [NSMutableArray array];
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[EmailLoadingViewController class]]) {
            break;
        }
        [vcs addObject:vc];
    }

    EmailSetupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"emailSetup"];
    if (vc != nil) {
        id accountType = parameters[@"accountType"];
        if (accountType && [accountType isKindOfClass:[NSNumber class]]) {
            vc.accountType = [(NSNumber *)accountType integerValue];
        }
        id auth = parameters[@"auth"];
        if (auth) {
            vc.auth = auth;
        }
        [vcs addObject:vc];
    }

    [self.navigationController setViewControllers:vcs animated:YES];
}

@end
