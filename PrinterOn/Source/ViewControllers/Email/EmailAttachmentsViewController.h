//
//  EmailAttachmentsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-05-02.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@class IMAPSessionManager, MCOIMAPMessage;

@interface EmailAttachmentsViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IMAPSessionManager *imapSession;
@property (nonatomic, strong) MCOIMAPMessage *imapMessage;
@property (nonatomic, strong) NSString *imapFolder;

@end
