//
//  EmailListViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/28/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class EmailAccount;

@interface EmailListViewController : BaseViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet ShadowView *sideView;
@property (weak, nonatomic) IBOutlet UITableView *sideTableView;

@property (weak, nonatomic) IBOutlet UIButton *foldersButton;

@property (nonatomic, strong) EmailAccount *emailAccount;

@end
