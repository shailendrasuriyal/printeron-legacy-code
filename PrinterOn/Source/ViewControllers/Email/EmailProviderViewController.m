//
//  EmailProviderViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-02-25.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "EmailProviderViewController.h"

#import "AppConstants.h"
#import "AppDelegate.h"
#import "EmailAccount.h"
#import "EmailLoadingViewController.h"
#import "EmailProviderCell.h"
#import "EmailSetupViewController.h"
#import "IMAPSessionManager.h"
#import "OAuth2Manager.h"

#import "AppAuth.h"

@interface EmailProviderViewController ()

@end

@implementation EmailProviderViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Email Provider Screen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_EMAILSETUP", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"EmailScreen.BackgroundColor"];
}

- (void)addViewControllerBeneath:(UIViewController *)addController toViewControllers:(NSArray *)controllers
{
    // Create an empty mutable array that will be used as a new stack of view controllers
    NSMutableArray *newControllers = [controllers mutableCopy];
    
    // Add to the top the view controller we want to transition too
    [newControllers insertObject:addController atIndex:controllers.count - 1];
    
    // Perform the transition
    [self.navigationController setViewControllers:newControllers animated:NO];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"emailProviderCell";
    EmailProviderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setupCell:indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __weak __typeof(self) weakSelf = self;
    GTMOAuth2ViewControllerCompletionHandler handler = ^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error)
    {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        BOOL userCancelled = error && ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorWindowClosed);
        if (userCancelled) return;

        if (error) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([strongSelf.navigationController.topViewController isMemberOfClass:[EmailLoadingViewController class]]) {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                });
            }];
            [alertVC addAction:cancelAction];

            [strongSelf presentViewController:alertVC animated:YES completion:nil];
        } else {
            for (UIViewController *vc in strongSelf.navigationController.viewControllers) {
                if ([vc isMemberOfClass:[EmailLoadingViewController class]]) {
                    [(EmailLoadingViewController *)vc showEmailSetupWithParameters:@{ @"accountType": [NSNumber numberWithInteger:indexPath.row], @"auth" : auth }];
                    break;
                }
            }
        }
    };

    OIDAuthStateAuthorizationCallback callback = ^(OIDAuthState *_Nullable authState, NSError *_Nullable error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appDelegate.currentAuthorizationBlock = nil;
        appDelegate.currentAuthorizationFlow = nil;

        BOOL userCancelled = error && ([error.domain isEqualToString:OIDGeneralErrorDomain] && (error.code == OIDErrorCodeUserCanceledAuthorizationFlow || error.code == OIDErrorCodeProgramCanceledAuthorizationFlow));
        if (userCancelled) {
            return;
        }

        if (error) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([strongSelf.navigationController.topViewController isMemberOfClass:[EmailLoadingViewController class]]) {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                });
            }];
            [alertVC addAction:cancelAction];

            [strongSelf presentViewController:alertVC animated:YES completion:nil];
        } else {
            for (UIViewController *vc in strongSelf.navigationController.viewControllers) {
                if ([vc isMemberOfClass:[EmailLoadingViewController class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [(EmailLoadingViewController *)vc showEmailSetupWithParameters:@{ @"accountType": [NSNumber numberWithInteger:indexPath.row], @"auth" : authState }];
                    });
                    break;
                }
            }
        }
    };

    void (^popViewBlock)(void) = ^{
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf addViewControllerBeneath:[strongSelf.storyboard instantiateViewControllerWithIdentifier:@"emailLoading"] toViewControllers:strongSelf.navigationController.viewControllers];
        [strongSelf.navigationController popViewControllerAnimated:YES];
    };

    void (^currentAuthorizationBlock)(void) = ^{
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        dispatch_async(dispatch_get_main_queue(), ^{
            if ([strongSelf.navigationController.topViewController isMemberOfClass:[strongSelf class]]) {
                [strongSelf.navigationController pushViewController:[strongSelf.storyboard instantiateViewControllerWithIdentifier:@"emailLoading"] animated:NO];
            }
        });
    };

    switch (indexPath.row) {
        case EmailAccountTypeGMail: {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            appDelegate.currentAuthorizationBlock = currentAuthorizationBlock;
            appDelegate.currentAuthorizationFlow = [OAuth2Manager showGoogleAuthOnViewController:self withClientID:[AppConstants googleOAuth2ClientID] callback:callback];
            break;
        }
        case EmailAccountTypeOutlook: {
            GTMOAuth2ViewControllerTouch *vc = [OAuth2Manager viewControllerForOutlook:handler clientID:[AppConstants outlookOAuth2ClientID] clientSecret:[AppConstants outlookOAuth2ClientSecret]];
            vc.navigationItem.title = self.navigationItem.title;
            vc.popViewBlock = popViewBlock;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case EmailAccountTypeICloud:
        case EmailAccountTypeYahoo:
        case EmailAccountTypeOther: {
            [self performSegueWithIdentifier:@"showEmailSetup" sender:indexPath];
            break;
        }
    }
    return nil;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showEmailSetup"]) {
        EmailSetupViewController *destViewController = segue.destinationViewController;
        destViewController.accountType = ((NSIndexPath *)sender).row;
    }
}

@end
