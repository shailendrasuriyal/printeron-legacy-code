//
//  EmailLoadingViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-02-26.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@interface EmailLoadingViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (void)showEmailSetupWithParameters:(NSDictionary *)parameters;

@end
