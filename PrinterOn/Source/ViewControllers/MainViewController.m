//
//  MainViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "MainViewController.h"

#import "AppDelegate.h"
#import "PreviewViewController.h"
#import "PrinterBarViewController.h"
#import "PrintDocument.h"
#import "PrintJob.h"
#import "PrintJobManager.h"
#import "SplashScreen.h"
#import "UIViewController+Container.h"

@interface MainViewController ()

@property (nonatomic, strong) UIViewController *containerController;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Store the Main View Controller in the Print Job Manager
    [PrintJobManager sharedPrintJobManager].mainView = self;

    [self orderMainMenuItems];

    // Add PrinterBar view controller from our storyboard to the container view
    self.containerController = [self addViewControllerToContainer:self.printerBarContainer storyboardIdentifier:@"PrinterBar"];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPrintHistory) name:UIApplicationDidBecomeActiveNotification object:nil];

    Class mdmSettings = NSClassFromString(@"MDMCustomSetting");
    if (mdmSettings) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsChangedNotification:) name:@"PONMDMCustomSettingsReceived" object:nil];
    }
}

- (NSArray *)generateMainMenuItems
{
    NSMutableArray *items = [NSMutableArray array];

    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    BOOL showDocuments = [delegate showDocuments];
    if (showDocuments) [items addObject:self.documentView];
    [self.documentView setHidden:!showDocuments];

    BOOL showEmail = [delegate showEmail];
    if (showEmail) [items addObject:self.emailView];
    [self.emailView setHidden:!showEmail];

    BOOL showPhotos = [delegate showPhotos];
    if (showPhotos) [items addObject:self.photoView];
    [self.photoView setHidden:!showPhotos];

    BOOL showWeb = [delegate showWeb];
    if (showWeb) [items addObject:self.webView];
    [self.webView setHidden:!showWeb];

    return items;
}

- (void)orderMainMenuItems
{
    NSArray *items = [self generateMainMenuItems];
    NSArray *xCoords = nil;
    NSArray *yCoords = nil;

    if (self.centerView) {
        CGFloat offset = (self.centerView.frame.size.width - (items.count * 130) - ((items.count - 1) * 20)) / 2;
        xCoords = @[@(offset), @(offset + 150), @(offset + 300), @(offset + 450)];
        yCoords = @[@20, @20, @20, @20];
    } else {
        xCoords = @[@20, @170, @20, @170];
        yCoords = @[@20, @20, @161, @161];
    }

    for (int i = 0; i < items.count; i++) {
        UIView *view = items[i];
        NSNumber *xCoord = (NSNumber *)xCoords[i];
        NSNumber *yCoord = (NSNumber *)yCoords[i];
        [view setFrame:CGRectMake(xCoord.floatValue, yCoord.floatValue, view.frame.size.width, view.frame.size.height)];
    }
}

- (void)settingsChangedNotification:(NSNotification *)notification
{
    [self orderMainMenuItems];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Main Screen";

    if (self.wasOpenedIn) {
        self.wasOpenedIn = NO;

        // If the app was launched from a notification UIApplicationDidBecomeActiveNotification fires too early.
        // So we need to show Print History from here only if main hasn't appeared yet.
        if (self.showPrintJob) {
            [self performSegueWithIdentifier:@"showPrintHistory" sender:self.showPrintJob];
            self.showPrintJob = nil;

            // Hide the splash after a tiny delay, otherwise a black screen will show under the splash
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [SplashScreen hide];
            });
        } else {
            [SplashScreen hide];
        }
    }
}

- (void)showPrintHistory
{
    if (self.showPrintJob) {
        if (!self.wasOpenedIn) {
            [self performSegueWithIdentifier:@"showPrintHistory" sender:self.showPrintJob];
            self.showPrintJob = nil;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    if ([ThemeLoader boolForKey:@"MainScreen.Button.CapitalizeText"]) {
        self.labelDocumentView.text = NSLocalizedPONString(@"LABEL_DOCUMENTS", nil).uppercaseString;
        self.labelEmailView.text = NSLocalizedPONString(@"LABEL_EMAIL", nil).uppercaseString;
        self.labelPhotoView.text = NSLocalizedPONString(@"LABEL_PHOTOS", nil).uppercaseString;
        self.labelWebView.text = NSLocalizedPONString(@"LABEL_WEB", nil).uppercaseString;
    } else {
        self.labelDocumentView.text = NSLocalizedPONString(@"LABEL_DOCUMENTS", nil);
        self.labelEmailView.text = NSLocalizedPONString(@"LABEL_EMAIL", nil);
        self.labelPhotoView.text = NSLocalizedPONString(@"LABEL_PHOTOS", nil);
        self.labelWebView.text = NSLocalizedPONString(@"LABEL_WEB", nil);
    }
    self.jobHistoryButton.title = NSLocalizedPONString(@"TITLE_PRINTHISTORY", nil);
    self.settingsButton.title = NSLocalizedPONString(@"TITLE_SETTINGS", nil);
}

- (void)setupTheme
{
    // Setup NavigationBar appearance
    self.navigationController.navigationBar.tintColor = [ThemeLoader colorForKey:@"NavigationBar.TextColor"];
    self.navigationController.navigationBar.barTintColor = [ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    // Set ToolBar appearance
    self.bottomBar.tintColor = [ThemeLoader colorForKey:@"MainScreen.Toolbar.TextColor"];
    self.bottomBar.barTintColor = [ThemeLoader colorForKey:@"MainScreen.Toolbar.BackgroundColor"];
    
    self.bottomBar.layer.shadowOpacity = 0.75f;
    self.bottomBar.layer.shadowRadius = 1.5f;
    self.bottomBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bottomBar.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"MainScreen.BackgroundColor"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"MainScreen.NavigationBar.Image"] cacheImage:NO]];

    self.imageDocumentView.image = [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"MainScreen.DocumentButton.Image"]];
    self.imageEmailView.image = [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"MainScreen.EmailButton.Image"]];
    self.imagePhotoView.image = [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"MainScreen.PhotoButton.Image"]];
    self.imageWebView.image = [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"MainScreen.WebButton.Image"]];
    
    self.documentView.backgroundColor = [ThemeLoader colorForKey:@"MainScreen.Button.BackgroundColor"];
    [self.documentView setShadowWithRadius:2.0f withColor:[ThemeLoader colorForKey:@"MainScreen.Button.ShadowColor"] withOffset:CGSizeMake(1.5f, 1.5f) withOpacity:0.75f];
    self.labelDocumentView.textColor = [ThemeLoader colorForKey:@"MainScreen.Button.TextColor"];
    self.emailView.backgroundColor = [ThemeLoader colorForKey:@"MainScreen.Button.BackgroundColor"];
    [self.emailView setShadowWithRadius:2.0f withColor:[ThemeLoader colorForKey:@"MainScreen.Button.ShadowColor"] withOffset:CGSizeMake(1.5f, 1.5f) withOpacity:0.75f];
    self.labelEmailView.textColor = [ThemeLoader colorForKey:@"MainScreen.Button.TextColor"];
    self.photoView.backgroundColor = [ThemeLoader colorForKey:@"MainScreen.Button.BackgroundColor"];
    [self.photoView setShadowWithRadius:2.0f withColor:[ThemeLoader colorForKey:@"MainScreen.Button.ShadowColor"] withOffset:CGSizeMake(1.5f, 1.5f) withOpacity:0.75f];
    self.labelPhotoView.textColor = [ThemeLoader colorForKey:@"MainScreen.Button.TextColor"];
    self.webView.backgroundColor = [ThemeLoader colorForKey:@"MainScreen.Button.BackgroundColor"];
    [self.webView setShadowWithRadius:2.0f withColor:[ThemeLoader colorForKey:@"MainScreen.Button.ShadowColor"] withOffset:CGSizeMake(1.5f, 1.5f) withOpacity:0.75f];
    self.labelWebView.textColor = [ThemeLoader colorForKey:@"MainScreen.Button.TextColor"];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.printerBarContainer.backgroundColor = [ThemeLoader colorForKey:@"PrinterBar.BackgroundColor"];
    }
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    // Compute the shadow paths after layout
    self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;

    self.bottomBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.bottomBar.layer.bounds.origin.x - 3, self.bottomBar.layer.bounds.origin.y, self.bottomBar.layer.bounds.size.width + 6, self.bottomBar.layer.bounds.size.height)].CGPath;
}

- (IBAction)showDocuments:(UITapGestureRecognizer *)sender
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"askedDocumentsPermission"] == nil) {
        // Display the permission dialog
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_ALLOW_DOCUMENTS", nil), [NSBundle mainBundle].infoDictionary[@"CFBundleDisplayName"]] message:NSLocalizedPONString(@"LABEL_PRIVACY_DOCUMENTS", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allowDocumentsAccess"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"askedDocumentsPermission"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self showDocuments:nil];
            });
        }];
        [alertVC addAction:yesAction];
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_DONT_ALLOW", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"allowDocumentsAccess"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"askedDocumentsPermission"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            });
        }];
        [alertVC addAction:noAction];

        [self presentViewController:alertVC animated:YES completion:nil];
        return;
    }

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allowDocumentsAccess"]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:[PrintDocument documentUTIs] inMode:UIDocumentPickerModeImport];
            documentPicker.delegate = self;
            [self presentViewController:documentPicker animated:YES completion:nil];
        } else {
            UIDocumentMenuViewController *documentMenu = [[UIDocumentMenuViewController alloc] initWithDocumentTypes:[PrintDocument documentUTIs] inMode:UIDocumentPickerModeImport];
            documentMenu.delegate = self;
            [self presentViewController:documentMenu animated:YES completion:nil];
        }
    } else {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ACCESS_DENIED", nil) message:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_DOCUMENTS_DENIED", nil), [NSBundle mainBundle].infoDictionary[@"CFBundleDisplayName"]] preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:cancelAction];

        [self presentViewController:alertVC animated:YES completion:nil];
    }
}

- (void)showPrintPreview:(PrintDocument *)document
{
    // Load the view from the Storyboard
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
    PreviewViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"PrintPreview"];
    controller.document = document;
    controller.wasOpenedIn = NO;
    [self pushToViewController:controller fromViewController:self];
}

- (void)pushToViewController:(UIViewController *)pushController fromViewController:(UIViewController *)fromController
{
    // Create an empty mutable array that will be used as a new stack of view controllers
    NSMutableArray *newControllers = [NSMutableArray array];
    
    // Go through the stack of current view controllers and add them to the new stack until we get to the base "from"
    // view controller.  Any view controllers between the "from" controller and the "push" controller are removed.
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        [newControllers addObject:vc];
        if ([vc isEqual:fromController]) {
            break;
        }
    }
    
    // Add to the top the view controller we want to transition too
    [newControllers addObject:pushController];
    
    // Perform the transition
    [self.navigationController setViewControllers:newControllers animated:YES];
}

#pragma mark - UIDocumentMenuDelegate

- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker
{
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

#pragma mark - UIDocumentPickerDelegate

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    PrintDocument *document = [PrintDocument getDocumentFromFile:url fromSource:NSLocalizedPONString(@"LABEL_SOURCE_DOCPROVIDER", nil)];

    if (document) {
        [self performSegueWithIdentifier:@"showPrintPreview" sender:document];
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPrintPreview"]) {
        PreviewViewController *destViewController = segue.destinationViewController;
        destViewController.document = sender;
        destViewController.wasOpenedIn = NO;
    }
}

@end
