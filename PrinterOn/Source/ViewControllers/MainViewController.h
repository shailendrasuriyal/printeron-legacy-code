//
//  MainViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class PrintDocument, PrintJob;

@interface MainViewController : BaseViewController <UIDocumentMenuDelegate, UIDocumentPickerDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *printerBarContainer;
@property (weak, nonatomic) IBOutlet UIView *centerView;

@property (weak, nonatomic) IBOutlet ShadowView *documentView;
@property (weak, nonatomic) IBOutlet UIImageView *imageDocumentView;
@property (weak, nonatomic) IBOutlet UILabel *labelDocumentView;

@property (weak, nonatomic) IBOutlet ShadowView *emailView;
@property (weak, nonatomic) IBOutlet UIImageView *imageEmailView;
@property (weak, nonatomic) IBOutlet UILabel *labelEmailView;

@property (weak, nonatomic) IBOutlet ShadowView *photoView;
@property (weak, nonatomic) IBOutlet UIImageView *imagePhotoView;
@property (weak, nonatomic) IBOutlet UILabel *labelPhotoView;

@property (weak, nonatomic) IBOutlet ShadowView *webView;
@property (weak, nonatomic) IBOutlet UIImageView *imageWebView;
@property (weak, nonatomic) IBOutlet UILabel *labelWebView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *jobHistoryButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsButton;
@property (weak, nonatomic) IBOutlet UIToolbar *bottomBar;

@property (nonatomic, assign) BOOL wasOpenedIn;
@property (nonatomic, strong) PrintJob *showPrintJob;

- (void)showPrintPreview:(PrintDocument *)document;

@end
