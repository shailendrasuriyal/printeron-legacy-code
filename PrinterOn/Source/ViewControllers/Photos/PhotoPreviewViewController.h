//
//  PhotoPreviewViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2/3/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "iCarousel.h"
#import "PhotoListViewController.h"

@class ALAsset, ALAssetsGroup, ImageDocument;

@interface PhotoPreviewViewController : BaseViewController <iCarouselDataSource, iCarouselDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *printButton;

@property (weak, nonatomic) IBOutlet iCarousel *carousel;

@property (nonatomic, strong) ALAssetsGroup *assetGroup;
@property (nonatomic, strong) NSMutableArray *assets;
@property (nonatomic, strong) NSNumber *currentIndex;
@property (nonatomic, weak) id <PhotoListDelegate> delegate;

@end
