//
//  PhotoPreviewViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2/3/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PhotoPreviewViewController.h"

#import "BarButtonItem.h"
#import "ImageDocument.h"
#import "MainViewController.h"
#import "PreviewViewController.h"
#import "PrintJobManager.h"
#import "ZoomTransitionProtocol.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface PhotoPreviewViewController () <ZoomTransitionProtocol, UIGestureRecognizerDelegate>

@property (nonatomic, strong) id savedGesture;
@property (nonatomic, assign) BOOL loadedFirstImage;

@end

@implementation PhotoPreviewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.loadedFirstImage = NO;

    self.carousel.type = iCarouselTypeLinear;
    self.carousel.bounceDistance = 0.5f;
    self.carousel.pagingEnabled = YES;
    self.carousel.currentItemIndex = self.assets.count - self.currentIndex.intValue - 1;

    // Layout the subviews now so we can load the items into the carousel so they are ready to be displayed for the transition animation
    [self.carousel layoutSubviews];

    // Create and configure the pinch gesture
    //UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureDetected:)];
    //[pinchGestureRecognizer setDelegate:self];
    //[self.imageItem addGestureRecognizer:pinchGestureRecognizer];

    // Create and configure the rotation gesture
    //UIRotationGestureRecognizer *rotationGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationGestureDetected:)];
    //[rotationGestureRecognizer setDelegate:self];
    //[self.imageItem addGestureRecognizer:rotationGestureRecognizer];

    // Create and configure the pan gesture
    //UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureDetected:)];
    //[panGestureRecognizer setDelegate:self];
    //[self.imageItem addGestureRecognizer:panGestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.savedGesture = self.navigationController.interactivePopGestureRecognizer;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Photo Preview Screen";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = self.savedGesture;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PHOTOPREVIEW", nil);
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.printButton withImage:[[ImageManager sharedImageManager] imageNamed:@"PrintButton"]];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PhotosScreen.BackgroundColor"];
}

- (NSString *)stringForAssetGroup
{
    int type = [[self.assetGroup valueForProperty:ALAssetsGroupPropertyType] intValue];

    switch(type) {
        case ALAssetsGroupLibrary:
            return [NSString stringWithFormat:@"%@", [self.assetGroup valueForProperty:ALAssetsGroupPropertyName]];
        case ALAssetsGroupAlbum:
            return [NSString stringWithFormat:@"%@: %@", NSLocalizedPONString(@"LABEL_ALBUM", nil), [self.assetGroup valueForProperty:ALAssetsGroupPropertyName]];
        case ALAssetsGroupEvent:
            return [NSString stringWithFormat:@"%@: %@", NSLocalizedPONString(@"LABEL_EVENT", nil), [self.assetGroup valueForProperty:ALAssetsGroupPropertyName]];
        case ALAssetsGroupFaces:
            return [NSString stringWithFormat:@"%@: %@", NSLocalizedPONString(@"LABEL_FACES", nil), [self.assetGroup valueForProperty:ALAssetsGroupPropertyName]];
        case ALAssetsGroupSavedPhotos:
            return [NSString stringWithFormat:@"%@", [self.assetGroup valueForProperty:ALAssetsGroupPropertyName]];
        case ALAssetsGroupPhotoStream:
            return [NSString stringWithFormat:@"%@: %@", NSLocalizedPONString(@"LABEL_STREAM", nil), [self.assetGroup valueForProperty:ALAssetsGroupPropertyName]];
        default:
            return nil;
    }
}

- (IBAction)printPressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

    UIView *view = self.carousel.currentItemView;
    if ([view isKindOfClass:[UIImageView class]]) {
        UIImageView *imageView = (UIImageView *)view;

        ImageDocument *document = [[ImageDocument alloc] initWithImage:imageView.image fromSource:[self stringForAssetGroup]];
        [[PrintJobManager sharedPrintJobManager].mainView showPrintPreview:document];
    }
}

- (void)pinchGestureDetected:(UIPinchGestureRecognizer *)recognizer
{
    UIGestureRecognizerState state = [recognizer state];
    
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
    {
        CGFloat scale = [recognizer scale];
        [recognizer.view setTransform:CGAffineTransformScale(recognizer.view.transform, scale, scale)];
        [recognizer setScale:1.0];
    }
}

- (void)rotationGestureDetected:(UIRotationGestureRecognizer *)recognizer
{
    UIGestureRecognizerState state = [recognizer state];
    
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
    {
        CGFloat rotation = [recognizer rotation];
        [recognizer.view setTransform:CGAffineTransformRotate(recognizer.view.transform, rotation)];
        [recognizer setRotation:0];
    }
}

- (void)panGestureDetected:(UIPanGestureRecognizer *)recognizer
{
    UIGestureRecognizerState state = [recognizer state];
    
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [recognizer translationInView:recognizer.view];
        [recognizer.view setTransform:CGAffineTransformTranslate(recognizer.view.transform, translation.x, translation.y)];
        [recognizer setTranslation:CGPointZero inView:recognizer.view];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // Don't accept gestures made to the interactive pop gesture recognizer on this screen
    if (gestureRecognizer == self.navigationController.interactivePopGestureRecognizer) {
        return NO;
    }
    return YES;
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
//{
//    return YES;
//}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPrintPreview"]) {
        UIView *view = self.carousel.currentItemView;
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView *)view;
            ImageDocument *document = [[ImageDocument alloc] initWithImage:imageView.image fromSource:[self stringForAssetGroup]];

            PreviewViewController *destViewController = segue.destinationViewController;
            destViewController.document = document;
        }
    }
}

#pragma mark - ZoomTransitionProtocol

- (UIView *)viewForZoomTransition:(BOOL)isSource
{
    if (isSource) {
        UIView *view = self.carousel.currentItemView;
        if (![view isKindOfClass:[UIImageView class]]) {
            return nil;
        }
        UIImageView *imageView = (UIImageView *)view;

        // Calculate the real size of the image inside the UIImageView frame
        CGSize size = CGSizeAspectFit(imageView.image.size, imageView.frame.size);
        float ratio = size.width / size.height;

        // If the image is a square we can return right away
        if (ratio == 1.0f) {
            return imageView;
        }

        // Create a new UIImageView that will be a square placed over the current image.
        // We do this so that it is sized properly and the original image fades out behind it, which is less jarring to the eye.
        UIImageView *new = [[UIImageView alloc] initWithImage:imageView.image];
        new.contentMode = UIViewContentModeScaleAspectFill;

        if (ratio < 1.0f) {
            [new setFrame:CGRectMake(0 + (imageView.frame.size.width - size.width) / 2, 0 + (imageView.frame.size.height - size.width) / 2, size.width, size.width)];
        } else if (ratio > 1.0f) {
            [new setFrame:CGRectMake(0 + (imageView.frame.size.width - size.height) / 2, 0 + (imageView.frame.size.height - size.height) / 2, size.height, size.height)];
        }
        [self.carousel.currentItemView addSubview:new];

        return new;
    } else {
        // Adjust the view taking into account the status and navigation bar if necessary.
        // Had to add this code for iOS 9 SDK because the view wasn't being sized or positioned properly like it was in previous iOS versions.
        CGFloat navigationBarHeight = [self navigationBarSize];

        if (self.view.frame.origin.y != navigationBarHeight) {
            CGSize newSize = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? self.navigationController.preferredContentSize : self.view.frame.size;

            [self.view setFrame:CGRectMake(self.view.frame.origin.x, navigationBarHeight, newSize.width, newSize.height - navigationBarHeight)];

            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }

        if (!self.loadedFirstImage) {
            self.loadedFirstImage = YES;
        }

        return self.carousel.currentItemView;
    }
}

- (CGFloat)navigationBarSize
{
    CGFloat navigationBarHeight = self.navigationController.navigationBar.frame.size.height;
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
        CGFloat statusBarHeight = MIN(statusBarSize.width, statusBarSize.height);
        navigationBarHeight += statusBarHeight;
    }
    return navigationBarHeight;
}

CGSize CGSizeAspectFit(CGSize aspectRatio, CGSize boundingSize)
{
    float mW = boundingSize.width / aspectRatio.width;
    float mH = boundingSize.height / aspectRatio.height;

    if (mH < mW) {
        boundingSize.width = boundingSize.height / aspectRatio.height * aspectRatio.width;
    } else if (mW < mH) {
        boundingSize.height = boundingSize.width / aspectRatio.width * aspectRatio.height;
    }

    return boundingSize;
}

#pragma mark - iCarouselDataSource

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return self.assets.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    //Create new view if no view is available for recycling
    if (view == nil) {
        UIViewController *controller = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? self.navigationController.presentingViewController : self.navigationController;

        if (controller.view.frame.size.height == carousel.frame.size.height) {
            CGFloat navigationBarHeight = [self navigationBarSize];

            CGSize newSize = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? self.navigationController.preferredContentSize : carousel.frame.size;

            CGRect imageRect = CGRectMake(carousel.frame.origin.x, carousel.frame.origin.y, newSize.width, newSize.height - navigationBarHeight);
            view = [[UIImageView alloc] initWithFrame:imageRect];
        } else {
            view = [[UIImageView alloc] initWithFrame:carousel.frame];
        }

        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }

    if ([view isMemberOfClass:[UIImageView class]]) {
        UIImageView *imageView = (UIImageView *)view;
        imageView.contentMode = UIViewContentModeScaleAspectFit;

        ALAsset *asset = self.assets[self.assets.count - index - 1];
        imageView.image = [UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
    }

    return view;
}

#pragma mark - iCarouselDelegate

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    if (self.delegate) {
        [self.delegate didChangePhotoIndex:self.carousel.currentItemIndex];
    }
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing) {
        return value * 1.1f;
    } else if (option == iCarouselOptionVisibleItems) {
        // Only load 1 item when displaying the carousel the first time
        return self.loadedFirstImage ? 3.0f : 1.0f;
    }
    return value;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    return self.carousel.frame.size.width;
}

@end
