//
//  PhotoGroupViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2/2/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface PhotoGroupViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *infoView;
@property (weak, nonatomic) IBOutlet UIImageView *infoImage;
@property (weak, nonatomic) IBOutlet UILabel *infoHeader;
@property (weak, nonatomic) IBOutlet UILabel *infoText;

@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
