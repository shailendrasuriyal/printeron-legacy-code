//
//  PhotoListViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2/2/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@class ALAssetsGroup;

@protocol PhotoListDelegate

- (void)didChangePhotoIndex:(NSInteger)index;

@end

@interface PhotoListViewController : BaseViewController <UICollectionViewDelegate, UICollectionViewDataSource, PhotoListDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *layout;

@property (nonatomic, strong) ALAssetsGroup *assetGroup;

@end
