//
//  PhotoGroupViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2/2/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PhotoGroupViewController.h"

#import "AssetsManager.h"
#import "BarButtonItem.h"
#import "CSLinearLayoutView.h"
#import "PhotoListViewController.h"
#import "PhotoGroupCell.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface PhotoGroupViewController ()

@property (nonatomic, strong) NSMutableArray *groups;

@property (nonatomic, strong) CSLinearLayoutView *linearLayout;
@property (nonatomic, strong) CSLinearLayoutItem *imageItem;
@property (nonatomic, strong) CSLinearLayoutItem *headerItem;
@property (nonatomic, strong) CSLinearLayoutItem *textItem;

@end

@implementation PhotoGroupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateGroups];

    // Register for changes to the library
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(libraryChangedNotification:) name:ALAssetsLibraryChangedNotification object:nil];
}

- (void)dealloc
{
    // Unregister notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ALAssetsLibraryChangedNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Photo Groups Screen";
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PHOTOS", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PhotosScreen.BackgroundColor"];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];

        // Setup NavigationBar appearance
        [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
        [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
        self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
        self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
        self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
        self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    [self updateContentHeight];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // Compute the shadow paths after layout
        self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;
    }
}

- (void)updateGroups
{
    if (!self.groups) {
        self.groups = [NSMutableArray new];
    } else {
        [self.groups removeAllObjects];
    }

    ALAssetsFilter *assetsFilter = [ALAssetsFilter allPhotos];

    ALAssetsLibraryGroupsEnumerationResultsBlock resultsBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        if (group && group.numberOfAssets > 0) {
            [group setAssetsFilter:assetsFilter];
            [self.groups addObject:group];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self reloadGroupData];
            });
        }
    };

    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showNoAccessView];
        });
    };

    // Enumerate Camera roll first
    [[[AssetsManager sharedAssetsManager] assetLibrary] enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:resultsBlock failureBlock:failureBlock];

    // Then all other groups
    NSUInteger type = ALAssetsGroupLibrary | ALAssetsGroupAlbum | ALAssetsGroupEvent | ALAssetsGroupFaces | ALAssetsGroupPhotoStream;
    [[[AssetsManager sharedAssetsManager] assetLibrary] enumerateGroupsWithTypes:type usingBlock:resultsBlock failureBlock:failureBlock];
}

- (void)reloadGroupData
{
    if (self.groups.count == 0) {
        [self showNoPhotosView];
    } else {
        [self.infoView setHidden:YES];
        [self.contentView setHidden:NO];
    }

    [self.tableView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

- (void)libraryChangedNotification:(NSNotification *)notification
{
    [self updateGroups];
}

- (void)showNoPhotosView
{
    [self showInfoWithHeader:NSLocalizedPONString(@"LABEL_NOPHOTOS", nil)
                    withText:[UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ? NSLocalizedPONString(@"LABEL_NOPHOTOS_CAMERATEXT", nil) : NSLocalizedPONString(@"LABEL_NOPHOTOS_TEXT", nil)
                   withImage:@"NoPhotoIcon"];
}

- (void)showNoAccessView
{
    [self showInfoWithHeader:NSLocalizedPONString(@"LABEL_NOPHOTOACCESS", nil)
                    withText:NSLocalizedPONString(@"LABEL_NOPHOTOACCESS_TEXT", nil)
                   withImage:@"NoPhotoAccessIcon"];
}

- (void)showInfoWithHeader:(NSString *)header withText:(NSString *)text withImage:(NSString *)image
{
    [self.contentView setHidden:YES];

    // Initialize Linear Layout
    if (!self.linearLayout) {
        self.linearLayout = [[CSLinearLayoutView alloc] initWithFrame:self.infoView.bounds];
        self.linearLayout.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.linearLayout.autoAdjustFrameSize = YES;
        self.linearLayout.orientation = CSLinearLayoutViewOrientationVertical;
    } else {
        [self.linearLayout removeAllItems];
    }

    // Add Image
    [self.infoImage setImage:[[ImageManager sharedImageManager] imageNamed:image cacheImage:NO]];
    self.imageItem = [self setupItem:self.imageItem withView:self.infoImage withPadding:CSLinearLayoutMakePadding(10, 10, 10, 10)];
    [self.linearLayout addItem:self.imageItem];

    // Add Header
    [self resizeLabel:self.infoHeader withText:header];
    self.headerItem = [self setupItem:self.headerItem withView:self.infoHeader withPadding:CSLinearLayoutMakePadding(0, 10, 10, 10)];
    [self.linearLayout addItem:self.headerItem];

    // Add Text
    [self resizeLabel:self.infoText withText:text];
    self.textItem = [self setupItem:self.textItem withView:self.infoText withPadding:CSLinearLayoutMakePadding(0, 10, 10, 10)];
    [self.linearLayout addItem:self.textItem];

    // Add layout to the view
    [self.infoView addSubview:self.linearLayout];

    // Adjust the size of the view to fit the content
    [self.infoView setFrame:CGRectMake(self.infoView.frame.origin.x, self.infoView.frame.origin.y, self.infoView.frame.size.width, self.linearLayout.frame.size.height)];

    [self.infoView setHidden:NO];
}

- (CSLinearLayoutItem *)setupItem:(CSLinearLayoutItem *)item withView:(UIView *)view withPadding:(CSLinearLayoutItemPadding)padding
{
    if (!item) {
        item = [CSLinearLayoutItem layoutItemForView:view];
        item.padding = padding;
        item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    }
    [view setHidden:NO];
    return item;
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text
{
    // Resize the label to hold the text
    [label setText:text];
    CGSize maxSize = CGSizeMake(label.frame.size.width, 2000);
    CGSize requiredSize = [label sizeThatFits:maxSize];
    
    if (requiredSize.height > label.frame.size.height) {
        [label setFrame:CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, requiredSize.height)];
    }
}

- (IBAction)closePressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.groups count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"photoGroupCell";
    PhotoGroupCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    [cell setupCell:[self groups][indexPath.row]];

    return cell;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPhotos"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

        PhotoListViewController *destViewController = segue.destinationViewController;
        destViewController.assetGroup = [self groups][indexPath.row];
    }
}

@end
