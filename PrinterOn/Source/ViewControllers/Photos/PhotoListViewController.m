//
//  PhotoListViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2/2/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PhotoListViewController.h"

#import "PhotoCell.h"
#import "PhotoPreviewViewController.h"
#import "ZoomInteractiveTransition.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface PhotoListViewController () <ZoomTransitionProtocol>

@property (nonatomic, strong) NSMutableArray *assets;
@property (nonatomic, strong) ZoomInteractiveTransition *transition;
@property (nonatomic, strong) NSIndexPath *transitionItemPath;
@property (nonatomic, assign) BOOL transitionReload;

@end

@implementation PhotoListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Setup the Collection View
    [self.collectionView registerClass:[PhotoCell class] forCellWithReuseIdentifier:[PhotoCell reuseIdentifier]];
    self.collectionView.allowsMultipleSelection = NO;

    [self updateAssets];
    self.transitionReload = NO;

    // Setup the zoom transition used by the view controllers
    self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    self.transition.handleEdgePanBackGesture = NO;
    self.transition.transitionDuration = 0.2f;

    // Register for changes to the library
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(libraryChangedNotification:) name:ALAssetsLibraryChangedNotification object:nil];
}

- (void)dealloc
{
    [self.transition resetDelegate];

    // Unregister notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ALAssetsLibraryChangedNotification object:nil];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Photo List Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    if (self.transitionReload && self.transitionItemPath) {
        [self.collectionView reloadItemsAtIndexPaths:@[self.transitionItemPath]];
        self.transitionReload = NO;
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = [self.assetGroup valueForProperty:ALAssetsGroupPropertyName];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PhotosScreen.BackgroundColor"];
}

- (void)updateAssets
{
    if (!self.assets) {
        self.assets = [NSMutableArray new];
    } else {
        [self.assets removeAllObjects];
    }

    ALAssetsGroupEnumerationResultsBlock resultsBlock = ^(ALAsset *asset, NSUInteger index, BOOL *stop) {
        if (asset) {
            [self.assets addObject:asset];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self reloadListData];
            });
        }
    };

    [self.assetGroup enumerateAssetsUsingBlock:resultsBlock];
}

- (void)reloadListData
{
    [self.collectionView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    CGSize collection = [self.collectionView.collectionViewLayout collectionViewContentSize];
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, collection.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, collection.height)];
}

- (void)libraryChangedNotification:(NSNotification *)notification
{
    [self updateAssets];
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.assets count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:[PhotoCell reuseIdentifier] forIndexPath:indexPath];

    ALAsset *asset = [self assets][self.assets.count - indexPath.row - 1];
    [cell.image setImage:[UIImage imageWithCGImage:asset.thumbnail]];
    cell.image.alpha = 1;

    return cell;
}

#pragma mark - Collection View Delegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.transitionItemPath = indexPath;
    [self performSegueWithIdentifier:@"showPhoto" sender:[self assets][self.assets.count - indexPath.row - 1]];
    return NO;
}

#pragma mark - PhotoListDelegate

- (void)didChangePhotoIndex:(NSInteger)index
{
    NSIndexPath *newPath = [NSIndexPath indexPathForItem:index inSection:0];
    if (![self.transitionItemPath isEqual:newPath]) {
        self.transitionItemPath = newPath;
        if (index >= 0 && index < 4) {
            [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        } else {
            UICollectionViewCell *cell = [self collectionView:self.collectionView cellForItemAtIndexPath:newPath];
            CGRect point = [self.collectionView convertRect:cell.frame toView:self.scrollView];
            [self.scrollView scrollRectToVisible:point animated:NO];
        }
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPhoto"]) {
        PhotoPreviewViewController *destViewController = segue.destinationViewController;
        destViewController.assetGroup = self.assetGroup;
        destViewController.assets = self.assets;
        destViewController.currentIndex = @(self.assets.count - self.transitionItemPath.row - 1);
        destViewController.delegate = self;
        self.transitionReload = YES;
    }
}

#pragma mark - ZoomTransitionProtocol

-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    PhotoCell *cell = (PhotoCell *)[self.collectionView cellForItemAtIndexPath:self.transitionItemPath];
    return cell.image;
}

@end
