//
//  BaseNavigationController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-08-21.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface BaseNavigationController : UINavigationController <UIGestureRecognizerDelegate, UINavigationControllerDelegate>

@end
