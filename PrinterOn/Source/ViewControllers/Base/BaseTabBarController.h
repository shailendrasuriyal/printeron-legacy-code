//
//  BaseTabBarControllerViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/17/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface BaseTabBarController : UITabBarController

@property (nonatomic, copy) NSString *screenName;

- (void)setupLocalizations;
- (void)setupTheme;

- (void)registerForEnterForegroundNotification;
- (void)customEnterForeground;

@end
