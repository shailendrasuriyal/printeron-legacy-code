//
//  BaseViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import <GoogleAnalytics/GAI.h>

@interface BaseViewController : GAITrackedViewController

- (void)setupLocalizations;
- (void)setupTheme;

- (void)backPressed;

- (void)registerForEnterForegroundNotification;
- (void)customEnterForeground;

@end
