//
//  BaseViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "BarButtonItem.h"

@interface BaseViewController ()

@property (nonatomic, assign) BOOL notifyEnterForeground;

@end

@implementation BaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self replaceBackButton];
    [self setupLocalizations];
    [self setupTheme];
}

- (void)dealloc
{
    if (self.notifyEnterForeground) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    }
}

- (void)setupLocalizations
{
    // Empty.  Overide in each view controller with specific implementation.
}

- (void)setupTheme
{
    // Empty.  Overide in each view controller with specific implementation.
}

- (void)replaceBackButton {
    if ([self.navigationController.viewControllers indexOfObject:self] != 0 && !self.navigationItem.hidesBackButton) {
        // Customize look of the button
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 42, 34);
        [BarButtonItem customizeLeftBarButton:backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.BackButton.Image"]]];

        // Add action for button press
        [backButton addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];

        // Add swipe gesture
        UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleBackSwipe:)];
        swipe.direction = UISwipeGestureRecognizerDirectionRight;
        [backButton addGestureRecognizer:swipe];

        // Add the button to a BarButtonItem
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];

        // Add the button to the left of navigation controller
        NSMutableArray *leftButtons = [NSMutableArray arrayWithObject:backBarButton];
        [leftButtons addObjectsFromArray:self.navigationItem.leftBarButtonItems];
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItems = leftButtons;
    }
}

- (void)backPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)handleBackSwipe:(UIGestureRecognizer *)swipe {
    if ([swipe.view isKindOfClass:[UIButton class]]) {
        [self backPressed];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

- (void)registerForEnterForegroundNotification
{
    self.notifyEnterForeground = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(customEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)customEnterForeground
{
    // Empty.  Overide in each view controller with specific implementation.
}

@end
