//
//  UserSetupViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "UserAccount.h"

@class TPKeyboardAvoidingScrollView;

@interface UserSetupViewController : BaseViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *accountView;
@property (weak, nonatomic) IBOutlet UILabel *labelAccount;
@property (weak, nonatomic) IBOutlet UITextField *textAccount;
@property (weak, nonatomic) IBOutlet UILabel *labelPassword;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UITextField *textDescription;

@property (weak, nonatomic) IBOutlet ShadowView *defaultView;
@property (weak, nonatomic) IBOutlet UILabel *labelDefault;
@property (weak, nonatomic) IBOutlet UITableView *tableDefault;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (nonatomic, weak) id <UserAccountDelegate> delegate;

@property (nonatomic, strong) UserAccount *userAccount;
@property (nonatomic, assign) BOOL hideServices;

@end
