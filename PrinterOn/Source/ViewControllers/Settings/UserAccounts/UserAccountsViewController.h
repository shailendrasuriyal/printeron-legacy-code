//
//  UserAccountsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface UserAccountsViewController : BaseViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *addView;
@property (weak, nonatomic) IBOutlet UIButton *addAccountButton;

@end
