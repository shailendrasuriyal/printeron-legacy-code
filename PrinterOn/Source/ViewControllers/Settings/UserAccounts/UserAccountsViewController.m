//
//  UserAccountsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "UserAccountsViewController.h"

#import "BarButtonItem.h"
#import "AccountCell.h"
#import "UserAccount.h"
#import "UserSetupViewController.h"

@interface UserAccountsViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation UserAccountsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];

    [self registerForEnterForegroundNotification];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"User Accounts Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)customEnterForeground
{
    // Refresh the fetch controller and table
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    [self updateTableView];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_USERACCOUNTS", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];

    // Setup the "Add User" appearance
    [self.addAccountButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.AddButton.Image"]] forState:UIControlStateNormal];
    self.addView.backgroundColor = [ThemeLoader colorForKey:@"MainScreen.Toolbar.BackgroundColor"];
    self.addView.layer.cornerRadius = self.addView.frame.size.width / 2;
    self.addView.layer.masksToBounds = NO;
    self.addView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.addView.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.addView.layer.shadowOpacity = 0.5f;
    self.addView.layer.shadowRadius = 1.5f;
    self.addView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.addView.bounds cornerRadius:self.addView.layer.cornerRadius].CGPath;
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"UserAccount"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"accountDescription" ascending:YES]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;
        
        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    //Lets the tableview know we're potentially doing a bunch of updates.
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    //We're finished updating the tableview's data.
    [self.tableView endUpdates];
    self.tableView.contentSize = [self.tableView sizeThatFits:CGSizeMake(CGRectGetWidth(self.tableView.bounds), CGFLOAT_MAX)];
    [self updateContentHeight];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    if (type == NSFetchedResultsChangeMove && [indexPath isEqual:newIndexPath]) {
        type = NSFetchedResultsChangeUpdate;
    }

    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"userAccountCell";
    AccountCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    UserAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCell:account.accountDescription withImage:[[ImageManager sharedImageManager] imageNamed:([account.isHostedDefault boolValue] || [account.serviceDefaults count] > 0) ? [ThemeLoader stringForKey:@"UserAccounts.DefaultUser.Image"] : [ThemeLoader stringForKey:@"UserAccounts.User.Image"]] showLock:NO];

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UserAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if (account) {
            [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext deleteObject:account];
            [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];
        }
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showUserSetup"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

        UserSetupViewController *destViewController = segue.destinationViewController;
        destViewController.userAccount = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
}

@end
