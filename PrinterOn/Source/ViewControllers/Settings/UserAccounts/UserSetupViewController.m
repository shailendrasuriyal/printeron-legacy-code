//
//  UserSetupViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "UserSetupViewController.h"

#import "BarButtonItem.h"
#import "OAuth2Manager.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "ServiceDefaultCell.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface UserSetupViewController ()

@property (nonatomic, strong) NSMutableArray *services;

@end

@implementation UserSetupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Set the fields if we are editing an account
    if (self.userAccount) {
        [self.textDescription setText:[self.userAccount.userName isEqualToString:self.userAccount.accountDescription] ? nil : self.userAccount.accountDescription];
        [self.textAccount setText:self.userAccount.userName];
        [self.textPassword setText:[self.userAccount getUserAccountPassword]];
    }

    if (self.hideServices) {
        [self.defaultView setHidden:YES];
    }

    [self validateFields];

    [self fetchServices];
    [self updateTableView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textDescription];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textAccount];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textPassword];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"User Setup Screen";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textDescription];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textAccount];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textPassword];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_USERSETUP", nil);
    [self.labelDescription setText:NSLocalizedPONString(@"LABEL_DESCRIPTION", nil)];
    [self.textDescription setPlaceholder:NSLocalizedPONString(@"LABEL_OPTIONAL", nil)];
    [self.labelAccount setText:NSLocalizedPONString(@"LABEL_ACCOUNT", nil)];
    [self.textAccount setPlaceholder:NSLocalizedPONString(@"LABEL_REQUIRED", nil)];
    [self.labelPassword setText:NSLocalizedPONString(@"LABEL_PASSWORD", nil)];
    [self.textPassword setPlaceholder:NSLocalizedPONString(@"LABEL_OPTIONAL", nil)];
    [self.labelDefault setText:NSLocalizedPONString(@"LABEL_DEFAULTACCOUNT", nil)];
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.saveButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.SelectButton.Image"]]];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];

    // Add AccessibilityIdentifier
    [self.textAccount setAccessibilityIdentifier:@"Account Field"];
    [self.textPassword setAccessibilityIdentifier:@"Password Field"];
    [self.textDescription setAccessibilityIdentifier:@"Description Field"];

    THEME_KEYBOARD(self.textDescription);
    THEME_KEYBOARD(self.textAccount);
    THEME_KEYBOARD(self.textPassword);
}

- (void)backPressed
{
    if ([self accountHasChanges]) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedPONString(@"LABEL_SAVE_CHANGES", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_YES", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self saveAccount:nil];
            });
        }];
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_NO", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [super backPressed];
            });
        }];
        [alertVC addAction:yesAction];
        [alertVC addAction:noAction];

        self.accountView.userInteractionEnabled = NO;
        self.defaultView.userInteractionEnabled = NO;
        self.navigationController.navigationBar.userInteractionEnabled = NO;

        [self presentViewController:alertVC animated:YES completion:nil];
    } else {
        [super backPressed];
    }
}

- (BOOL)accountHasChanges
{
    if (![self areFieldsValid]) return NO;

    NSMutableSet *serviceSet = [NSMutableSet set];
    for (int i = 0; i < [self.services count]; i++ ) {
        ServiceDefaultCell *cell = (ServiceDefaultCell *)[self.tableDefault cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (cell && ![cell.labelTitle.text isEqualToString:[Service hostedService].serviceDescription] && [cell.switchDefault isOn]) {
            [serviceSet addObject:[self services][i]];
        }
    }

    ServiceDefaultCell *hostedCell = (ServiceDefaultCell *)[self.tableDefault cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    BOOL isHostedDefault = (hostedCell && [hostedCell.labelTitle.text isEqualToString:[Service hostedService].serviceDescription]) ? hostedCell.switchDefault.isOn : NO;

    if (self.userAccount) {
        return [self.userAccount hasChangesForUserAccount:self.textAccount.text password:self.textPassword.text description:self.textDescription.text hostedDefault:isHostedDefault defaultServices:serviceSet];
    } else {
        if (isHostedDefault || serviceSet.count > 0 || self.textAccount.text.length > 0 || self.textPassword.text.length > 0 || self.textDescription.text.length > 0) return YES;
    }

    return NO;
}

- (void)fetchServices
{
    self.services = [Service getRestrictedService] != nil ? [NSMutableArray array] : [NSMutableArray arrayWithObject:[Service hostedService]];

    NSManagedObjectContext *context = [[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Service" inManagedObjectContext:context]];
    NSArray *sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"serviceDescription" ascending:YES]];
    [fetchRequest setSortDescriptors:sortDescriptors];

    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];

    [self.services addObjectsFromArray:results];
}

- (void)updateTableView
{
    [self.tableDefault reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.tableDefault setFrame:CGRectMake(self.tableDefault.frame.origin.x, self.tableDefault.frame.origin.y, self.tableDefault.frame.size.width, self.tableDefault.contentSize.height)];
    [self.defaultView setFrame:CGRectMake(self.defaultView.frame.origin.x, self.defaultView.frame.origin.y, self.defaultView.frame.size.width, self.tableDefault.frame.origin.y + self.tableDefault.frame.size.height + 8)];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.defaultView.frame.origin.y + self.defaultView.frame.size.height + 12);
}

- (IBAction)saveAccount:(id)sender
{
    [self.backgroundView endEditing:YES];

    NSMutableSet *serviceSet = [NSMutableSet set];
    NSMutableArray *authSet = nil;
    for (int i = 0; i < [self.services count]; i++ ) {
        ServiceDefaultCell *cell = (ServiceDefaultCell *)[self.tableDefault cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (cell && ![cell.labelTitle.text isEqualToString:[Service hostedService].serviceDescription] && [cell.switchDefault isOn]) {
            [serviceSet addObject:[self services][i]];

            if (cell.auth) {
                if (authSet == nil) authSet = [NSMutableArray arrayWithCapacity:1];
                [authSet addObject:cell];
            }
        }
    }

    ServiceDefaultCell *hostedCell = (ServiceDefaultCell *)[self.tableDefault cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    BOOL isHostedDefault = (hostedCell && [hostedCell.labelTitle.text isEqualToString:[Service hostedService].serviceDescription]) ? hostedCell.switchDefault.isOn : NO;

    if (self.userAccount) {
        BOOL fromMDM = [[self.userAccount fromMDM] boolValue];
        [self.userAccount updateUserAccount:self.textAccount.text password:self.textPassword.text description:self.textDescription.text hostedDefault:isHostedDefault fromMDM:fromMDM defaultServices:serviceSet];
    } else {
        self.userAccount = [UserAccount createUserAccount:self.textAccount.text password:self.textPassword.text description:self.textDescription.text hostedDefault:isHostedDefault fromMDM:NO defaultServices:serviceSet];
    }

    for (ServiceDefaultCell *cell in authSet) {
        [self.userAccount saveOAuth2Authentication:cell.auth forService:cell.serviceURL];
    }

    if (self.delegate) {
        [self.delegate didFinishWithUserAccount:self.userAccount];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)validateFields
{
    [self.saveButtonItem setEnabled:[self areFieldsValid]];
}

- (BOOL)areFieldsValid
{
    if ([self.textAccount.text length] == 0)
        return NO;

    if ([self.textAccount.text length] > 0 && [[self.textAccount.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return NO;
    }

    return YES;
}

- (void)textChanged:(NSNotification *)notification
{
    [self validateFields];
}

- (IBAction)serviceSwitchChanged:(UISwitch *)sender
{
    UIView *view = sender.superview;
    while (view && ![view isKindOfClass:[UITableViewCell class]]) {
        view = [view superview];
    }

    if (![view isMemberOfClass:[ServiceDefaultCell class]]) {
        return;
    }

    ServiceDefaultCell *serviceCell = (ServiceDefaultCell *)view;

    if (sender.isOn && !sender.isSelected) {
        sender.selected = YES;
        BOOL supportsThird = [ServiceCapabilities serviceSupportsOAuthThirdParty:serviceCell.serviceURL];
        if (supportsThird && !serviceCell.auth) {
            [self showThirdPartyOAuthLogin:serviceCell.serviceURL forSwitch:sender onCell:serviceCell];
        }
    } else if (!sender.isOn && sender.isSelected) {
        sender.selected = NO;
        if (serviceCell.isFetching) {
            serviceCell.switchDefault.enabled = NO;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.15 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                if (serviceCell.isFetching) {
                    serviceCell.switchDefault.hidden = YES;
                    serviceCell.indicatorLoading.hidden = NO;
                }
            });
        }
    }
}

- (void)showThirdPartyOAuthLogin:(NSString *)serviceURL forSwitch:(UISwitch *)sender onCell:(ServiceDefaultCell *)cell
{
    __weak __typeof(self) weakSelf = self;
    GTMOAuth2ViewControllerCompletionHandler handler = ^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error)
    {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        BOOL userCancelled = error && ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorWindowClosed);
        if (userCancelled) {
            [sender setOn:NO animated:NO];
            [self serviceSwitchChanged:sender];
            return;
        }

        if (error) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];
            
            [strongSelf presentViewController:alertVC animated:YES completion:nil];
            [sender setOn:NO animated:YES];
            [strongSelf serviceSwitchChanged:sender];
            return;
        }

        cell.auth = auth;
    };

    GTMOAuth2ViewControllerTouch *vc = [OAuth2Manager viewControllerForPON:handler serviceURL:serviceURL];
    vc.navigationItem.title = self.navigationItem.title;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [self.scrollView textFieldShouldReturn:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *temp = [[textField.text stringByReplacingCharactersInRange:range withString:string] stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];

    if ([textField.text isEqualToString:temp]) {
        return NO;
    }

    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.services count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"defaultServiceCell";
    ServiceDefaultCell *cell = [self.tableDefault dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    Service *service = [self services][indexPath.row];
    [cell setupCell:service isDefault:[service.serviceDescription isEqualToString:[Service hostedService].serviceDescription] ? [self.userAccount.isHostedDefault boolValue] : [service.defaultUser isEqual:self.userAccount]];
    
    return cell;
}

@end
