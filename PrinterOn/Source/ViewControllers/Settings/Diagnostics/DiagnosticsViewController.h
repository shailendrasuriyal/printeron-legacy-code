//
//  DiagnosticsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2014-04-16.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface DiagnosticsViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet ShadowView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *labelInfo;

@property (weak, nonatomic) IBOutlet ShadowView *switchView;
@property (weak, nonatomic) IBOutlet UISwitch *switchSend;
@property (weak, nonatomic) IBOutlet UILabel *labelSend;

@end
