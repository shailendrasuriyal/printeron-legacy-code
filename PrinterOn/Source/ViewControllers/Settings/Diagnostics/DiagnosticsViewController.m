//
//  DiagnosticsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2014-04-16.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "DiagnosticsViewController.h"

@interface DiagnosticsViewController ()

@property (nonatomic, assign) BOOL savedValue;

@end

@implementation DiagnosticsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Adjust switch location because the control is a different size
    [self.switchSend setFrame:CGRectMake(self.switchSend.frame.origin.x + 6, self.switchSend.frame.origin.y - 2, self.switchSend.frame.size.width, self.switchSend.frame.size.height)];

    // Set the default value for the switch based on the saved user setting
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"sendDiagnosticData"] != nil) {
        self.savedValue = [[[NSUserDefaults standardUserDefaults] stringForKey:@"sendDiagnosticData"] isEqualToString:@"YES"] ? YES : NO;
    } else {
        self.savedValue = YES;
    }
    [self.switchSend setOn:self.savedValue];
}

- (void)dealloc
{
    if (self.savedValue != self.switchSend.isOn) {
        [[NSUserDefaults standardUserDefaults] setObject:self.switchSend.isOn ? @"YES" : @"NO" forKey:@"sendDiagnosticData"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[GAI sharedInstance] setOptOut:!self.switchSend.isOn];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Diagnostics Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_DIAGNOSTICS", nil);
    [self.labelInfo setText:NSLocalizedPONString(@"LABEL_DIAGNOSTICS_INFO", nil)];
    [self.labelSend setText:NSLocalizedPONString(@"LABEL_SEND", nil)];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];
}

@end
