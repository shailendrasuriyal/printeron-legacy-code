//
//  AboutViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2014-04-16.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface AboutViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *versionView;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelVersion;
@property (weak, nonatomic) IBOutlet UILabel *labelCopyright;

@property (weak, nonatomic) IBOutlet ShadowView *groupView;
@property (weak, nonatomic) IBOutlet UILabel *labelLegal;
@property (weak, nonatomic) IBOutlet UIView *groupLine;
@property (weak, nonatomic) IBOutlet UILabel *labelAcknowledgements;

@property (weak, nonatomic) IBOutlet ShadowView *payloadView;
@property (weak, nonatomic) IBOutlet UILabel *labelSettings;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapPayload;

@end
