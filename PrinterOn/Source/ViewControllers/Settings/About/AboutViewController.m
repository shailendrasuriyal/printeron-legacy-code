//
//  AboutViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2014-04-16.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "AboutViewController.h"

#import "AppConstants.h"

@interface AboutViewController ()

@property (nonatomic, strong) NSDictionary *payloadInfo;
@property (nonatomic, strong) NSDateFormatter *dateFormat;

@end

@implementation AboutViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"PONMDMCustomSettingsReceived" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"About Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_ABOUT", nil);
    self.labelVersion.text = [NSString stringWithFormat:kAppVersionText, [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"], kAppVersionName];
    self.labelCopyright.text = NSLocalizedPONString(kAboutCopyrightText, nil);
    self.labelCopyright.font = [UIFont systemFontOfSize:kAboutCopyrightTextSize];
    self.labelLegal.text = NSLocalizedPONString(@"TITLE_LEGAL", nil);
    self.labelAcknowledgements.text = NSLocalizedPONString(@"TITLE_ACKNOWLEDGEMENTS", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];
    self.imageLogo.image = [UIImage imageNamed:@"AboutLogo"];

    [self setupCopyright];

    if ([ThemeLoader boolForKey:@"SettingsScreen.HideLegalScreen"]) {
        self.labelLegal.hidden = YES;
        self.groupLine.hidden = YES;

        self.groupView.frame = CGRectMake(self.groupView.frame.origin.x, self.groupView.frame.origin.y, self.groupView.frame.size.width, self.labelAcknowledgements.frame.size.height);
        self.labelAcknowledgements.frame = CGRectMake(self.labelAcknowledgements.frame.origin.x, 0, self.labelAcknowledgements.frame.size.width, self.labelAcknowledgements.frame.size.height);
    }

    [self updatePayload:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)setupCopyright
{
    // Resize the copyright label to hold the text
    CGSize maxSize = CGSizeMake(self.labelCopyright.frame.size.width, 2000);
    CGSize requiredSize = [self.labelCopyright sizeThatFits:maxSize];
    self.labelCopyright.frame = CGRectMake(self.labelCopyright.frame.origin.x, self.labelCopyright.frame.origin.y, self.labelCopyright.frame.size.width, requiredSize.height);

    // Resize the view the copyright label is inside to fit the new size
    self.versionView.frame = CGRectMake(self.versionView.frame.origin.x, self.versionView.frame.origin.y, self.versionView.frame.size.width, self.labelCopyright.frame.origin.y + self.labelCopyright.frame.size.height + 9);

    // Move the view below to account for changes in height
    self.groupView.frame = CGRectMake(self.groupView.frame.origin.x, self.versionView.frame.origin.y + self.versionView.frame.size.height + 12, self.groupView.frame.size.width, self.groupView.frame.size.height);
}

- (void)updateContentHeight
{
    UIView *lastView = self.payloadView.hidden ? self.groupView : self.payloadView;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, lastView.frame.origin.y + lastView.frame.size.height + 12);
}

- (void)updatePayload:(BOOL)initial
{
    Class mdmSettings = NSClassFromString(@"MDMCustomSetting");
    if (mdmSettings) {
        if (initial) {
            self.dateFormat = [NSDateFormatter new];
            self.dateFormat.dateFormat = @"M/d/yyyy HH:mm:ss";
            self.payloadView.frame = CGRectMake(self.payloadView.frame.origin.x, self.groupView.frame.origin.y + self.groupView.frame.size.height + 12, self.payloadView.frame.size.width, self.payloadView.frame.size.height);
            self.payloadView.hidden = NO;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsChangedNotification:) name:@"PONMDMCustomSettingsReceived" object:nil];
        }

        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wundeclared-selector"
        // Turn off warnings for undeclared selectors for this section of code because we are using reflection.
        // The code is safe because we have checks in place to make sure methods are found before we try to use them.
        if ([mdmSettings respondsToSelector:@selector(loadPayloadInfo)]) {
            id mdmInfo = [mdmSettings performSelector:@selector(loadPayloadInfo)];
            self.payloadInfo = [mdmInfo isKindOfClass:[NSDictionary class]] ? mdmInfo : nil;

            if (self.payloadInfo) {
                NSString *error = self.payloadInfo[@"payloadError"];
                self.tapPayload.enabled = error.length > 0;
                NSString *icon = error.length > 0 ? @"\U0000274C" : @"\U00002705";
                self.labelSettings.text = [NSString stringWithFormat:@"%@: %@ %@", NSLocalizedPONString(@"LABEL_ADMIN_SETTINGS", nil), [self.dateFormat stringFromDate:self.payloadInfo[@"payloadDate"]], icon];
            } else {
                self.labelSettings.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedPONString(@"LABEL_ADMIN_SETTINGS", nil), NSLocalizedPONString(@"LABEL_NEVER", nil)];
            }
        }
        #pragma clang diagnostic pop
    }
}

- (void)settingsChangedNotification:(NSNotification *)notification
{
    [self updatePayload:NO];
}

- (IBAction)tapPayload:(id)sender
{
    if ([self.labelSettings.text hasSuffix:@"\U0000274C"]) {
        self.labelSettings.text = self.payloadInfo[@"payloadError"];
    } else {
        self.labelSettings.text = [NSString stringWithFormat:@"%@: %@ \U0000274C", NSLocalizedPONString(@"LABEL_ADMIN_SETTINGS", nil), [self.dateFormat stringFromDate:self.payloadInfo[@"payloadDate"]]];
    }
}

@end
