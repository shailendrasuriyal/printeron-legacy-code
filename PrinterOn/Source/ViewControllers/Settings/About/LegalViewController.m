//
//  LegalViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2014-04-21.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "LegalViewController.h"

@interface LegalViewController ()

@end

@implementation LegalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    Class mdmSettings = NSClassFromString(@"MDMCustomSetting");
    if (mdmSettings) {
        // Display confirmation dialog
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedPONString(@"LABEL_LEGAL_WARNING", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.printeron.com/legal"]];
                [self.webView loadRequest:request];
            });
        }];
        [alertVC addAction:okAction];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }];
        [alertVC addAction:cancelAction];

        [self presentViewController:alertVC animated:YES completion:nil];
    } else {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.printeron.com/legal"]];
        [self.webView loadRequest:request];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Legal Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_LEGAL", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.webView setHidden:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    // If the page is wider then the webview we scale it to fit
    // Can't use scalesToFit = YES of the webview, for some reason it screws the scaling up
    if (webView.scrollView.contentSize.width > webView.frame.size.width) {
        NSString *js = [NSString stringWithFormat:@"var meta = document.createElement('meta'); "  \
                        "meta.setAttribute( 'name', 'viewport' ); "  \
                        "meta.setAttribute( 'content', 'width = device-width, initial-scale = %f, minimum-scale=0.1, maximum-scale=5.0, user-scalable = yes' ); "  \
                        "document.getElementsByTagName('head')[0].appendChild(meta)", webView.frame.size.width / webView.scrollView.contentSize.width];
        [webView stringByEvaluatingJavaScriptFromString:js];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // NSURLErrorDomain code -999 means another request was made before the previous completed
    if ([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
        [self webViewDidFinishLoad:webView];
        return;
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    // Display the error message
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

@end
