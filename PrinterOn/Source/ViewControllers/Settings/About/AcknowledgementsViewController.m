//
//  AcknowledgementsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2014-04-21.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "AcknowledgementsViewController.h"

@interface AcknowledgementsViewController ()

@end

@implementation AcknowledgementsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSURLRequest *request = [NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"licenses" withExtension:@"html"]];
    [self.webView loadRequest:request];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Acknowledgements Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_ACKNOWLEDGEMENTS", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.webView setHidden:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // If the page is wider then the webview we scale it to fit
    // Can't use scalesToFit = YES of the webview, for some reason it screws the scaling up
    if (webView.scrollView.contentSize.width > webView.frame.size.width) {
        NSString *js = [NSString stringWithFormat:@"var meta = document.createElement('meta'); "  \
                        "meta.setAttribute( 'name', 'viewport' ); "  \
                        "meta.setAttribute( 'content', 'width = device-width, initial-scale = %f, minimum-scale=0.1, maximum-scale=5.0, user-scalable = yes' ); "  \
                        "document.getElementsByTagName('head')[0].appendChild(meta)", webView.frame.size.width / webView.scrollView.contentSize.width];
        [webView stringByEvaluatingJavaScriptFromString:js];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // NSURLErrorDomain code -999 means another request was made before the previous completed
    if ([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
        [self webViewDidFinishLoad:webView];
        return;
    }

    // Display the error message
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

@end
