//
//  LegalViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2014-04-21.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface LegalViewController : BaseViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
