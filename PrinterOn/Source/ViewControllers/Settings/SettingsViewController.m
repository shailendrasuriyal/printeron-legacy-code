//
//  SettingsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "SettingsViewController.h"

#import "BarButtonItem.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([ThemeLoader boolForKey:@"SettingsScreen.HideDiagnosticsScreen"]) {
        [self.diagnosticView setHidden:YES];
        [self.groupAboutLine setHidden:YES];

        [self.groupAboutView setFrame:CGRectMake(self.groupAboutView.frame.origin.x, self.groupAboutView.frame.origin.y, self.groupAboutView.frame.size.width, self.aboutView.frame.size.height)];
        [self.aboutView setFrame:CGRectMake(self.aboutView.frame.origin.x, 0, self.aboutView.frame.size.width, self.aboutView.frame.size.height)];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Settings Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_SETTINGS", nil);
    [self.labelUserAccounts setText:NSLocalizedPONString(@"TITLE_USERACCOUNTS", nil)];
    [self.labelServiceURL setText:NSLocalizedPONString(@"TITLE_SERVICES", nil)];
    [self.labelDiagnostic setText:NSLocalizedPONString(@"TITLE_DIAGNOSTICS", nil)];
    [self.labelAbout setText:NSLocalizedPONString(@"TITLE_ABOUT", nil)];
}

- (void)setupTheme
{
    [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];

    [self.imageUserAccounts setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"SettingsScreen.UserAccountsButton.Image"]]];
    [self.imageServiceURL setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"SettingsScreen.ServiceURLButton.Image"]]];
    [self.imageDiagnostic setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"SettingsScreen.DiagnosticsButton.Image"]]];
    [self.imageAbout setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"SettingsScreen.AboutButton.Image"]]];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Compute the shadow paths after layout
    self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;
}

- (IBAction)backPressed:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
