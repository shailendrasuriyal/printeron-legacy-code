//
//  ServiceSetupViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 1/20/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@class Service, TPKeyboardAvoidingScrollView;

@interface ServiceSetupViewController : BaseViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *serviceView;
@property (weak, nonatomic) IBOutlet UILabel *labelHost;
@property (weak, nonatomic) IBOutlet UITextField *textHost;
@property (weak, nonatomic) IBOutlet UILabel *labelPath;
@property (weak, nonatomic) IBOutlet UITextField *textPath;
@property (weak, nonatomic) IBOutlet UILabel *labelSSL;
@property (weak, nonatomic) IBOutlet UISwitch *switchSSL;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UITextField *textDescription;

@property (weak, nonatomic) IBOutlet ShadowView *defaultView;
@property (weak, nonatomic) IBOutlet UILabel *labelDefault;
@property (weak, nonatomic) IBOutlet UISwitch *switchDefault;

@property (weak, nonatomic) IBOutlet UIView *testingView;
@property (weak, nonatomic) IBOutlet UILabel *labelTesting;

@property (weak, nonatomic) IBOutlet ShadowView *urlView;
@property (weak, nonatomic) IBOutlet UILabel *labelURL;
@property (weak, nonatomic) IBOutlet UIButton *testButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (nonatomic, strong) Service *service;

@end
