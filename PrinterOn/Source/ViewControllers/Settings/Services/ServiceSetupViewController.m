//
//  ServiceSetupViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 1/20/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "ServiceSetupViewController.h"

#import "BarButtonItem.h"
#import "Service.h"
#import "TPKeyboardAvoidingScrollView.h"

#import "AFNetworking.h"

@interface ServiceSetupViewController ()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation ServiceSetupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Add AccessibilityIdentifiers
    [self.textHost setAccessibilityIdentifier:@"Host Field"];
    [self.textPath setAccessibilityIdentifier:@"Path Field"];
    [self.switchSSL setAccessibilityIdentifier:@"SSL Switch"];
    [self.textDescription setAccessibilityIdentifier:@"Description Field"];
    [self.switchDefault setAccessibilityIdentifier:@"Default Switch"];
    [self.labelURL setAccessibilityIdentifier:@"Server URL Preview"];

    // Adjust switch location under iOS7 because the control is a different size
    [self.switchSSL setFrame:CGRectMake(self.switchSSL.frame.origin.x + 6, self.switchSSL.frame.origin.y - 2, self.switchSSL.frame.size.width, self.switchSSL.frame.size.height)];
    [self.switchDefault setFrame:CGRectMake(self.switchDefault.frame.origin.x + 6, self.switchDefault.frame.origin.y - 2, self.switchDefault.frame.size.width, self.switchDefault.frame.size.height)];

    // Set the fields if we are editing an account
    if (self.service) {
        NSURL *serviceURL = [NSURL URLWithString:self.service.serviceURL];
        [self.textDescription setText:[[serviceURL host] isEqualToString:self.service.serviceDescription] ? nil : self.service.serviceDescription];
        [self.textHost setText:[self getHostForURL:serviceURL]];
        [self.textPath setText:[serviceURL path]];
        [self.switchSSL setOn:[[[serviceURL scheme] lowercaseString] isEqualToString:@"https"]];
        [self.switchDefault setOn:[self.service.isDefault boolValue]];

        if ([self.service.serviceLock boolValue]) {
            [self.textDescription setEnabled:NO];
            [self.textHost setEnabled:NO];
            [self.textPath setEnabled:NO];
            [self.switchSSL setEnabled:NO];
            [self.switchDefault setEnabled:NO];
        }
    }

    [self validateFields];
    [self updateServiceURLText];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textDescription];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChangedURL:) name:UITextFieldTextDidChangeNotification object:self.textHost];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChangedURL:) name:UITextFieldTextDidChangeNotification object:self.textPath];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Service Setup Screen";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textDescription];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textHost];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textPath];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [self cancelServiceTest];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_SERVICESETUP", nil);
    [self.labelDescription setText:NSLocalizedPONString(@"LABEL_DESCRIPTION", nil)];
    [self.textDescription setPlaceholder:NSLocalizedPONString(@"LABEL_OPTIONAL", nil)];
    [self.labelHost setText:NSLocalizedPONString(@"LABEL_SERVER", nil)];
    [self.textHost setPlaceholder:NSLocalizedPONString(@"LABEL_SERVER_EXAMPLE", nil)];
    [self.labelPath setText:NSLocalizedPONString(@"LABEL_PATH", nil)];
    [self.textPath setPlaceholder:NSLocalizedPONString(@"LABEL_OPTIONAL", nil)];
    [self.textPath setText:@"/cps"];
    [self.labelSSL setText:NSLocalizedPONString(@"LABEL_SSL", nil)];
    [self.labelDefault setText:NSLocalizedPONString(@"LABEL_DEFAULTSERVICE", nil)];
    [self.testButton setTitle:NSLocalizedPONString(@"LABEL_TEST_SERVICE", nil) forState:UIControlStateNormal];
    [self.labelTesting setText:NSLocalizedPONString(@"LABEL_TEST_SERVICE", nil)];
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.saveButton withImage: [self.service.serviceLock boolValue] ? [[ImageManager sharedImageManager] imageNamed:@"LockIcon-White"] : [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.SelectButton.Image"]]];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];

    self.testButton.layer.cornerRadius = 4.0f;
    self.testingView.layer.cornerRadius = 4.0f;

    THEME_KEYBOARD(self.textDescription);
    THEME_KEYBOARD(self.textHost);
    THEME_KEYBOARD(self.textPath);
}

- (void)backPressed
{
    if ([self serviceHasChanges]) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedPONString(@"LABEL_SAVE_CHANGES", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_YES", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self saveService:nil];
            });
        }];
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_NO", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [super backPressed];
            });
        }];
        [alertVC addAction:yesAction];
        [alertVC addAction:noAction];

        self.serviceView.userInteractionEnabled = NO;
        self.defaultView.userInteractionEnabled = NO;
        self.urlView.userInteractionEnabled = NO;
        self.navigationController.navigationBar.userInteractionEnabled = NO;

        [self presentViewController:alertVC animated:YES completion:nil];
    } else {
        [super backPressed];
    }
}

- (BOOL)serviceHasChanges
{
    if (![self areFieldsValid]) return NO;

    if (self.service) {
        return [self.service hasChangesForService:[self getServiceURLString] description:self.textDescription.text isDefault:self.switchDefault.isOn];
    } else {
        if (self.switchDefault.isOn || !self.switchSSL.isOn || self.textHost.text.length > 0 || self.textDescription.text.length > 0 || ![self.textPath.text isEqualToString:@"/cps"]) return YES;
    }

    return NO;
}

- (void)updateContentHeight
{
    UIView *lastView = self.urlView.isHidden ? self.defaultView : self.urlView;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, lastView.frame.origin.y + lastView.frame.size.height + 12);
}

- (NSString *)getHostForURL:(NSURL *)url
{
    if (url == nil) return nil;

    NSNumber *port = [url port];
    if (port == nil) return [url host];

    NSString *scheme = [[url scheme] lowercaseString];
    int portValue = [port intValue];
    
    if ([scheme isEqualToString:@"https"] && portValue != 443) {
        return [NSString stringWithFormat:@"%@:%d", [url host], portValue];
    }

    if ([scheme isEqualToString:@"http"] && portValue != 80) {
        return [NSString stringWithFormat:@"%@:%d", [url host], portValue];
    }

    return [url host];
}

- (NSString *)getServiceURLString
{
    NSString *path = self.textPath.text;
    if ([path length] > 0  && ![path hasPrefix:@"/"]) {
        path = [NSString stringWithFormat:@"/%@", path];
    }
    NSString *serviceURL = [NSString stringWithFormat:@"%@://%@%@", [self.switchSSL isOn] ? @"https" : @"http", self.textHost.text, path];
    
    if ([serviceURL hasSuffix:@"/"]) {
        serviceURL = [serviceURL substringToIndex:[serviceURL length] - 1];
    }

    return serviceURL;
}

- (IBAction)saveService:(id)sender
{
    [self.backgroundView endEditing:YES];

    Service *restricted = [Service getRestrictedService];
    if (restricted) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }

    NSString *serviceURL = [self getServiceURLString];
    if ([Service isHostedString:serviceURL andPublic:YES]) {
        return;
    }

    if (self.service) {
        if (![self.service.serviceLock boolValue]) {
            [self.service updateService:serviceURL description:self.textDescription.text isDefault:self.switchDefault.isOn isLocked:NO isMDM:NO isRestricted:NO completionBlock:nil];
        }
    } else {
        [Service createService:serviceURL description:self.textDescription.text isDefault:self.switchDefault.isOn isLocked:NO isMDM:NO isRestricted:NO completionBlock:nil];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)validateFields
{
    [self.saveButtonItem setEnabled:[self areFieldsValid]];
}

- (BOOL)areFieldsValid
{
    if ([self.service.serviceLock boolValue]) {
        return NO;
    }

    if ([self.textDescription.text length] > 0 && [[self.textDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return NO;
    }

    if ([self.textPath.text length] > 0 && [[self.textPath.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return NO;
    }

    if ([[self.textHost.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
        return NO;

    return YES;
}

- (void)textChanged:(NSNotification *)notification
{
    [self validateFields];
}

- (void)textChangedURL:(NSNotification *)notification
{
    [self validateFields];
    [self updateServiceURLText];
}

- (IBAction)switchSSLChanged {
    [self updateServiceURLText];
}

- (void)updateServiceURLText
{
    // Stop any currently running test
    [self stopServiceTest];

    [self.labelURL setText:[self getServiceURLString]];
    if (self.urlView.isHidden != ([self.textHost.text length] == 0)) {
        [self.urlView setHidden:[self.textHost.text length] == 0];
        [self updateContentHeight];
    }
}

# pragma mark - Test Service

- (IBAction)pressedTestService
{
    [self.backgroundView endEditing:YES];
    [self.testingView setHidden:NO];
    [self.testButton setHidden:YES];
    [self testService];
}

- (void)testService
{
    self.sessionManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];

    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.validatesDomainName = NO;
    securityPolicy.allowInvalidCertificates = YES;
    self.sessionManager.securityPolicy = securityPolicy;

    self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [self.sessionManager.requestSerializer setTimeoutInterval:30.0];

    __weak __typeof(self) weakSelf = self;
    [self.sessionManager GET:self.labelURL.text parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf testServiceSuccess];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf testServiceFailure:error];
    }];
}

- (void)cancelServiceTest
{
    if (self.sessionManager) {
        [self.sessionManager invalidateSessionCancelingTasks:YES];
        self.sessionManager = nil;
    }
}

- (void)stopServiceTest
{
    [self cancelServiceTest];

    [self.testButton setHidden:NO];
    [self.testingView setHidden:YES];
}

- (void)testServiceSuccess
{
    [self stopServiceTest];

    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedPONString(@"LABEL_SUCCESS_SETTINGS", nil) preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)testServiceFailure:(NSError *)error
{
    [self stopServiceTest];

    // Absorb the errors given when cancelling a job so they don't trigger the code after
    if (([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999)) return;

    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [self.scrollView textFieldShouldReturn:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField != self.textDescription && textField != self.textPath) {
        NSString *temp = [[textField.text stringByReplacingCharactersInRange:range withString:string] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if ([textField.text isEqualToString:temp]) {
            return NO;
        }
    }
    
    return YES;
}

@end
