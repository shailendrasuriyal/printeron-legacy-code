//
//  SettingsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface SettingsViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet ShadowView *userAccountsView;
@property (weak, nonatomic) IBOutlet UIImageView *imageUserAccounts;
@property (weak, nonatomic) IBOutlet UILabel *labelUserAccounts;

@property (weak, nonatomic) IBOutlet ShadowView *serviceURLView;
@property (weak, nonatomic) IBOutlet UIImageView *imageServiceURL;
@property (weak, nonatomic) IBOutlet UILabel *labelServiceURL;

@property (weak, nonatomic) IBOutlet ShadowView *groupAboutView;

@property (weak, nonatomic) IBOutlet UIView *diagnosticView;
@property (weak, nonatomic) IBOutlet UIImageView *imageDiagnostic;
@property (weak, nonatomic) IBOutlet UILabel *labelDiagnostic;

@property (weak, nonatomic) IBOutlet UIView *groupAboutLine;

@property (weak, nonatomic) IBOutlet UIView *aboutView;
@property (weak, nonatomic) IBOutlet UIImageView *imageAbout;
@property (weak, nonatomic) IBOutlet UILabel *labelAbout;

@end
