//
//  SavedPrintersViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/15/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "SavedPrintersViewController.h"

#import "Printer.h"
#import "PrinterCell.h"
#import "PrinterDetailsViewController.h"

@interface SavedPrintersViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation SavedPrintersViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) {
        [self setTitle:NSLocalizedPONString(@"TITLE_SAVED", nil)];
        [[self tabBarItem] setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Saved.Image"]]];

        if ([ThemeLoader boolForKey:@"PrintersScreen.Tabbar.UseOffImage"]) {
            [[self tabBarItem] setImage:[[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Saved.Image.Off"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        }
        [[self tabBarItem] setSelectedImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Saved.Image"]]];
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerForEnterForegroundNotification];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Saved Printers Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)customEnterForeground
{
    // Refresh the fetch controller and table
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    [self updateTableView];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PrintersScreen.BackgroundColor"];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SavedPrinter"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES], [NSSortDescriptor sortDescriptorWithKey:@"organizationLocationDesc" ascending:YES]];

        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;

        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self updateTableView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"printerCell";
    PrinterCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    Printer *printer = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCellWithPrinter:printer];

    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Printer setSingletonPrinter:[self.fetchedResultsController objectAtIndexPath:indexPath] forEntity:@"SelectedPrinter"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printerDetailsSaved"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        [Printer setSingletonPrinter:[self.fetchedResultsController objectAtIndexPath:indexPath] forEntity:@"DetailsPrinter"];
    }
}

@end
