//
//  PrinterDetailsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/6/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrinterDetailsViewController.h"

#import "BarButtonItem.h"
#import "CapabilitiesCell.h"
#import "CSLinearLayoutView.h"
#import "DirSearch.h"
#import "JobDetailsViewController.h"
#import "MapClusterListViewController.h"
#import "MultiplePrintersViewController.h"
#import "MKMapView+Zoom.h"
#import "NSManagedObject+Clone.h"
#import "OAuth2Manager.h"
#import "PONButton.h"
#import "Printer.h"
#import "PrintersViewController.h"
#import "QRScannerViewController.h"
#import "ReleaseViewController.h"
#import "NSString+URL.h"

#import <RHAddressBook/AddressBook.h>

@interface PrinterDetailsViewController ()

@property (nonatomic, strong) CSLinearLayoutView *linearLayout;
@property (nonatomic, strong) CSLinearLayoutItem *overviewItem;
@property (nonatomic, strong) CSLinearLayoutItem *buttonsItem;
@property (nonatomic, strong) CSLinearLayoutItem *pullPrinterItem;
@property (nonatomic, strong) CSLinearLayoutItem *locationItem;
@property (nonatomic, strong) CSLinearLayoutItem *locationDescriptionItem;
@property (nonatomic, strong) CSLinearLayoutItem *hoursItem;
@property (nonatomic, strong) CSLinearLayoutItem *printerItem;

@property (nonatomic, strong) Printer *printer;
@property (nonatomic, strong) NSArray *leftCellData;
@property (nonatomic, strong) NSArray *rightCellData;
@property (nonatomic, assign) CLLocationCoordinate2D mapCoordinates;

@property (nonatomic, strong) RHAddressBook *addressBook;
@property (nonatomic, strong) RHGroup *contactGroup;
@property (nonatomic, strong) RHPerson *contactRecord;

@property (nonatomic, strong) NSManagedObjectContext *scratchSearchObjectContext;
@property (nonatomic, strong) RKObjectManager *searchObjectManager;
@property (nonatomic, strong) RKManagedObjectRequestOperation *currentOperation;
@property (nonatomic, assign) BOOL shouldRotateUpdate;
@property (nonatomic, assign) BOOL isUpdateRotating;

@end

@implementation PrinterDetailsViewController

- (void)viewDidLoad
{
    self.printer = [Printer getSingletonPrinterForEntity:@"DetailsPrinter"];
    [super viewDidLoad];
    [self registerForEnterForegroundNotification];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RHAddressBookExternalChangeNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Set all of the printer details
    [self setPrinterDetails];
    [self.selectButton setHidden:self.releaseUI ? NO : [Printer isSingletonPrinter:self.printer forEntity:@"SelectedPrinter"]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Printer Details Screen";
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self cancelCurrentSearch];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)customEnterForeground
{
    if (self.shouldRotateUpdate) {
        if (self.currentOperation.isFinished || self.currentOperation.isCancelled) {
            self.shouldRotateUpdate = NO;
        } else {
            [self rotateUpdate];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PRINTERDETAILS", nil);
    [self.locationDescriptionTitle setText:NSLocalizedPONString(@"TITLE_LOCATION", nil)];
    [self.hoursTitle setText:NSLocalizedPONString(@"LABEL_HOURSOPERATION", nil)];
    [self.emailTitle setText:NSLocalizedPONString(@"LABEL_EMAILPRINTADDRESS", nil)];
    [self.webTitle setText:NSLocalizedPONString(@"LABEL_WEBPRINTADDRESS", nil)];
    [self updateSaveButton];
    [self updateContactButton];
    [self.updateButton setTitle:NSLocalizedPONString(@"LABEL_UPDATE", nil) forState:UIControlStateNormal];
}

- (void)setupTheme
{
    if (self.showClose && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];
        [self.backButton setHidden:NO];

        // Setup NavigationBar appearance
        [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
        [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
        self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
        self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
        self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
        self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    }

    [BarButtonItem customizeRightBarButton:self.selectButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.SelectButton.Image"]]];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.BackgroundColor"];
    self.buttonView.backgroundColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.BackgroundColor"];

    self.contactsButton.highlightColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.HighlightColor"];
    self.contactsButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.saveButton.highlightColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.HighlightColor"];
    self.saveButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.updateButton.highlightColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.HighlightColor"];
    self.updateButton.titleLabel.adjustsFontSizeToFitWidth = YES;

    [self.saveButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Buttons.Save.Image"]] forState:UIControlStateNormal];
    [self.contactsButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Buttons.Contact.Image"]] forState:UIControlStateNormal];
    [self.updateButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Buttons.Update.Image"]] forState:UIControlStateNormal];

    [self.emailImage setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Email.Image"]]];
    [self.webImage setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Web.Image"]]];

    // Add AccessibilityId
    [self.overviewLabelSubtitle setAccessibilityIdentifier:@"Overview Subtitle"];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    [self updateContentHeight];

    if (self.showClose && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;
    }
}

- (IBAction)closePressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)setShouldRotateUpdate:(BOOL)shouldRotateUpdate
{
    _shouldRotateUpdate = shouldRotateUpdate;
    [self.updateButton setEnabled:!shouldRotateUpdate];
}

- (void)setPrinterDetails
{
    // Initialize Linear Layout
    if (!self.linearLayout) {
        self.linearLayout = [[CSLinearLayoutView alloc] initWithFrame:self.view.bounds];
        self.linearLayout.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.linearLayout.autoAdjustFrameSize = YES;
        self.linearLayout.orientation = CSLinearLayoutViewOrientationVertical;
    } else {
        [self.linearLayout removeAllItems];
    }

    // Overview View
    [self.overviewLabelTitle setText:self.printer.displayName];
    [self.overviewLabelSubtitle setText:self.printer.organizationLocationDesc];
    self.overviewItem = [self setupItem:self.overviewItem withView:self.overView withPadding:CSLinearLayoutMakePadding(10, 8, 12, 9)];
    [self.linearLayout addItem:self.overviewItem];

    // Buttons
    if (!self.releaseUI) {
        self.buttonsItem = [self setupItem:self.buttonsItem withView:self.buttonView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.buttonsItem];
    } else {
        [self.buttonView setHidden:YES];
    }

    // Pull Printer View
    BOOL pullPrinter = [self.printer.printerClass.lowercaseString isEqualToString:@"pull"];
    if (pullPrinter) {
        // Resize the label to hold the text
        [self.pullPrinterText setText:NSLocalizedPONString(@"LABEL_PULL_PRINTER_DESC", nil)];
        CGSize maxSize = CGSizeMake(self.pullPrinterText.frame.size.width, 2000);
        CGSize requiredSize = [self.pullPrinterText sizeThatFits:maxSize];
        [self.pullPrinterText setFrame:CGRectMake(self.pullPrinterText.frame.origin.x, self.pullPrinterText.frame.origin.y, self.pullPrinterText.frame.size.width, requiredSize.height)];

        // Resize the view the label is inside to fit the new size
        [self.pullPrinterView setFrame:CGRectMake(self.pullPrinterView.frame.origin.x, self.pullPrinterView.frame.origin.y, self.pullPrinterView.frame.size.width, self.pullPrinterText.frame.origin.y + self.pullPrinterText.frame.size.height + 8)];

        self.pullPrinterItem = [self setupItem:self.pullPrinterItem withView:self.pullPrinterView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.pullPrinterItem];
    } else {
        [self.pullPrinterView setHidden:YES];
    }

    // Location View, don't show if this is a pull printer
    if (pullPrinter) {
        [self.locationView setHidden:YES];
    } else {
        self.locationItem = [self setupItem:self.locationItem withView:self.locationView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];

        // Update the map coordinates and place a pin on it for the printer
        self.mapCoordinates = [self.printer getCoordinates];
        if (self.mapCoordinates.latitude == 0.0 && self.mapCoordinates.longitude == 0.0) {
            CLGeocoder *geocoder = [CLGeocoder new];
            __weak PrinterDetailsViewController *weakSelf = self;
            [geocoder geocodeAddressString:[NSString stringWithFormat:@"%@, %@, %@", self.printer.addressLine1, [self.printer getAddressLineLabel], [self.printer getCountryLabel]] completionHandler:^(NSArray *placemarks, NSError *error) {
                __strong PrinterDetailsViewController *strongSelf = weakSelf;
                if (!strongSelf) return;

                if (error || [placemarks count] == 0) {
                    [strongSelf setMap:strongSelf.mapCoordinates addPin:NO];
                } else if ([placemarks count] > 0) {
                    CLPlacemark *topResult = placemarks[0];
                    strongSelf.mapCoordinates = topResult.location.coordinate;
                    [strongSelf setMap:strongSelf.mapCoordinates addPin:YES];
                }
            }];
        } else {
            [self setMap:self.mapCoordinates addPin:YES];
        }

        [self.locationLabelAddressTitle setText:self.printer.addressLine1];
        [self.locationAddress1 setText:[self.printer getAddressLineLabel]];
        [self.locationAddress2 setText:[self.printer getCountryLabel]];

        [self.linearLayout addItem:self.locationItem];
    }

    // Location Description View
    // Only show the Location Description view if there is information
    NSString *locationDescriptionText = [self.printer getLocationDescriptionLabel];
    if ([locationDescriptionText length] > 0) {
        // Resize the label to hold the text
        [self.locationDescriptionText setText:locationDescriptionText];
        CGSize maxSize = CGSizeMake(self.locationDescriptionText.frame.size.width, 2000);
        CGSize requiredSize = [self.locationDescriptionText sizeThatFits:maxSize];
        [self.locationDescriptionText setFrame:CGRectMake(self.locationDescriptionText.frame.origin.x, self.locationDescriptionText.frame.origin.y, self.locationDescriptionText.frame.size.width, requiredSize.height)];

        // Resize the view the label is inside to fit the new size
        [self.locationDescriptionView setFrame:CGRectMake(self.locationDescriptionView.frame.origin.x, self.locationDescriptionView.frame.origin.y, self.locationDescriptionView.frame.size.width, self.locationDescriptionText.frame.origin.y + self.locationDescriptionText.frame.size.height + 8)];

        self.locationDescriptionItem = [self setupItem:self.locationDescriptionItem withView:self.locationDescriptionView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.locationDescriptionItem];
    } else {
        [self.locationDescriptionView setHidden:YES];
    }

    // Hours View
    // Only show the Hours of Operation view if there is information
    NSString *hoursText = [self.printer getHoursLabel];
    if ([hoursText length] > 0) {
        // Resize the label to hold the text
        [self.hoursText setText:hoursText];
        CGSize maxSize = CGSizeMake(self.hoursText.frame.size.width, 2000);
        CGSize requiredSize = [self.hoursText sizeThatFits:maxSize];
        [self.hoursText setFrame:CGRectMake(self.hoursText.frame.origin.x, self.hoursText.frame.origin.y, self.hoursText.frame.size.width, requiredSize.height)];
        
        // Resize the view the label is inside to fit the new size
        [self.hoursView setFrame:CGRectMake(self.hoursView.frame.origin.x, self.hoursView.frame.origin.y, self.hoursView.frame.size.width, self.hoursText.frame.origin.y + self.hoursText.frame.size.height + 8)];

        self.hoursItem = [self setupItem:self.hoursItem withView:self.hoursView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.hoursItem];
    } else {
        [self.hoursView setHidden:YES];
    }

    //Printer View
    [self.printerImage setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Printer.Image"]]];
    [self.printerManufacturer setText:[self.printer getManufacturerLabel]];
    [self.printerModel setText:[self.printer getModelLabel]];
    [self.printerStatus setImage:([self.printer.online intValue] == 1) ? [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Online.Image"]] : [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Offline.Image"]]];

    self.leftCellData = @[@{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.Ink.Image"], @"title": NSLocalizedPONString(@"LABEL_INK", nil), @"value": [self.printer getInkTypeLabel]},
        @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.Duplex.Image"], @"title": NSLocalizedPONString(@"LABEL_DUPLEX", nil), @"value": [self.printer getDuplexModeLabel]},
        @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.CoverPage.Image"], @"title": NSLocalizedPONString(@"LABEL_COVERPAGE", nil), @"value": [self.printer getCoverPageLabel]},
        @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.Fees.Image"], @"title": NSLocalizedPONString(@"LABEL_JOBFEE", nil), @"value": [self.printer getJobFeesLabel]}];
    [self.printerLeftTable reloadData];

    self.rightCellData = @[@{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.UserCredentials.Image"], @"title": NSLocalizedPONString(@"LABEL_USERCREDENTIALS", nil), @"value": [self.printer getPrintUserCredentialsLabel]},
        @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.PaperSize.Image"], @"title": NSLocalizedPONString(@"LABEL_PAPERSIZE", nil), @"value": [self.printer getPaperSizesLabel]},
        @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.PageLimit.Image"], @"title": NSLocalizedPONString(@"LABEL_PAGELIMIT", nil), @"value": [self.printer getPageLimitLabel]},
        @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.ReleaseCode.Image"], @"title": NSLocalizedPONString(@"LABEL_RELEASECODE", nil), @"value": [self.printer getReleaseCodeModeLabel]}];
    [self.printerRightTable reloadData];

    // Only show the email printing address if it is enabled
    NSString *emailAddress = [self.printer getEmailAddressOfType:@"name"];
    if (!self.releaseUI && [emailAddress length] > 0) {
        [self.emailAddress setText:emailAddress];
    } else {
        emailAddress = [self.printer getEmailAddressOfType:@"num"];
        if (!self.releaseUI && [emailAddress length] > 0) {
            [self.emailAddress setText:emailAddress];
        } else {
            [self.emailView setHidden:YES];
            [self.webView setFrame:CGRectMake(self.webView.frame.origin.x, self.emailView.frame.origin.y, self.webView.frame.size.width, self.webView.frame.size.height)];
            [self.printerView setFrame:CGRectMake(self.printerView.frame.origin.x, self.printerView.frame.origin.y, self.printerView.frame.size.width, self.webView.frame.origin.y + self.webView.frame.size.height + 12)];
        }
    }

    // Only show the web printing address if it is enabled
    NSString *webAddress = [self.printer getWebPortalAddressStripPrefix:YES];
    if (!self.releaseUI && [webAddress length] > 0) {
        [self.webAddress setText:webAddress];
    } else {
        [self.webView setHidden:YES];
        if ([self.emailView isHidden]) {
            [self.printerBottomLine setHidden:YES];
            [self.printerView setFrame:CGRectMake(self.printerView.frame.origin.x, self.printerView.frame.origin.y, self.printerView.frame.size.width, self.emailView.frame.origin.y)];
        } else {
            [self.printerView setFrame:CGRectMake(self.printerView.frame.origin.x, self.printerView.frame.origin.y, self.printerView.frame.size.width, self.emailView.frame.origin.y + self.emailView.frame.size.height + 12)];
        }
    }

    self.printerItem = [self setupItem:self.printerItem withView:self.printerView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
    [self.linearLayout addItem:self.printerItem];

    // Add linear layout to the scrollview
    [self.scrollView addSubview:self.linearLayout];

    [self updateContentHeight];
}

- (CSLinearLayoutItem *)setupItem:(CSLinearLayoutItem *)item withView:(UIView *)view withPadding:(CSLinearLayoutItemPadding)padding
{
    if (!item) {
        item = [CSLinearLayoutItem layoutItemForView:view];
        item.padding = padding;
        item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    }
    [view setHidden:NO];
    return item;
}

- (void)updateContentHeight
{
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.linearLayout.frame.size.height);
}

- (void)setMap:(CLLocationCoordinate2D)coordinates addPin:(BOOL)pin
{
    MKMapSnapshotOptions *options = [MKMapSnapshotOptions new];
    CLLocationCoordinate2D mapCoords = CLLocationCoordinate2DMake(coordinates.latitude + 0.0005, coordinates.longitude);
    options.region = MKCoordinateRegionMake(mapCoords, [MKMapView coordinateSpanWithView:self.imageMap centerCoordinates:mapCoords andZoomLevel:14]);
    options.scale = [UIScreen mainScreen].scale;
    options.size = self.imageMap.frame.size;
    
    MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
    [snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        if (snapshot == nil) return;
        
        UIImage *image = snapshot.image;
        
        if (pin) {
            UIImage *pinImage = [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"Maps.PrinterPin"]];
            
            // Start to create our final image
            UIGraphicsBeginImageContextWithOptions(image.size, YES, image.scale);
            
            // Draw the image from the snapshotter
            [image drawAtPoint:CGPointMake(0, 0)];
            
            // Now draw the map pin
            CGPoint point = [snapshot pointForCoordinate:coordinates];
            point.x = point.x - (pinImage.size.width/2);
            point.y = point.y - (pinImage.size.height);
            [pinImage drawAtPoint:point];
            
            // Get the final image
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }

        [self.mapIndicator setHidden:YES];
        [self.imageMap setImage:image];
    }];
}

- (IBAction)launchMapApp:(id)sender
{
    MKMapItem *end = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:self.mapCoordinates addressDictionary:[self.printer getAddressDictionary]]];
    [end setName:self.overviewLabelTitle.text];

    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)]) {
        [MKMapItem openMapsWithItems:@[end] launchOptions:nil];
    }
}

- (IBAction)openEmail:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@", self.emailAddress.text]]];
}

- (IBAction)openWeb:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self.printer getWebPortalAddressStripPrefix:NO]]];
}

- (IBAction)buttonPressed:(UIButton *)sender
{
    if ([sender isEqual:self.saveButton]) {
        [self saveAction];
    } else if ([sender isEqual:self.contactsButton]) {
        [self contactAction];
    } else if ([sender isEqual:self.updateButton]) {
        [self updateAction];
    }
}

- (void)updateSaveButton {
    if ([Printer getPrinter:self.printer forEntityName:@"SavedPrinter"]) {
        [self.saveButton setTitle:NSLocalizedPONString(@"LABEL_DELETE", nil) forState:UIControlStateNormal];
    } else {
        [self.saveButton setTitle:NSLocalizedPONString(@"LABEL_SAVE", nil) forState:UIControlStateNormal];
    }
}

- (void)saveAction
{
    NSManagedObject *savedPrinter = [Printer getPrinter:self.printer forEntityName:@"SavedPrinter"];

    NSError *error;
    if (savedPrinter) {
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext deleteObject:savedPrinter];
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:&error];
        if (error) NSLog(@"Error deleting printer: %@", error);
    } else {
        [self.printer clonePrinterInContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext forEntityName:@"SavedPrinter" updateExisting:NO];
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:&error];
        if (error) NSLog(@"Error saving printer: %@", error);
    }

    [self updateSaveButton];
}

- (void)addressBookChanged
{
    [self updateContactButton];
}

- (void)updateContactButton {
    RHAuthorizationStatus authStatus = [RHAddressBook authorizationStatus];
    if (authStatus == RHAuthorizationStatusAuthorized) {
        if (self.contactRecord == nil) self.contactRecord = [self getContactForPrinter];
        if (self.contactRecord && ![self.contactRecord hasBeenRemoved]) {
            [self.contactsButton setTitle:NSLocalizedPONString(@"LABEL_REMOVE", nil) forState:UIControlStateNormal];
        } else {
            [self.contactsButton setTitle:NSLocalizedPONString(@"LABEL_ADD", nil) forState:UIControlStateNormal];
        }
    } else {
        [self.contactsButton setTitle:NSLocalizedPONString(@"LABEL_ADD", nil) forState:UIControlStateNormal];
    }
}

- (void)contactAction
{
    RHAuthorizationStatus authStatus = [RHAddressBook authorizationStatus];
    if (authStatus == RHAuthorizationStatusAuthorized) {
        if (self.contactRecord) {
            [self removePrinterContact];
        } else {
            [self addPrinterContact];
        }
    } else if (authStatus == RHAuthorizationStatusNotDetermined) {
        // Request authorization
        if (self.addressBook == nil) {
            self.addressBook = [RHAddressBook new];
            [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(addressBookChanged) name:RHAddressBookExternalChangeNotification object:nil];
        }
            
        [self.addressBook requestAuthorizationWithCompletion:^(bool granted, NSError *error) {
            if (granted) {
                self.contactRecord = [self getContactForPrinter];
                if (self.contactRecord == nil) {
                    [self addPrinterContact];
                }
            } else {
                [[NSNotificationCenter defaultCenter] removeObserver:self name:RHAddressBookExternalChangeNotification object:nil];
            }
        }];
    } else if (authStatus == RHAuthorizationStatusDenied || authStatus == RHAuthorizationStatusRestricted) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_CONTACTSDENIED", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:cancelAction];

        [self presentViewController:alertVC animated:YES completion:nil];
    }
}

- (RHPerson *)getContactForPrinter
{
    if (self.addressBook == nil) {
        self.addressBook = [RHAddressBook new];
        [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(addressBookChanged) name:RHAddressBookExternalChangeNotification object:nil];
    }

    if (self.contactGroup == nil) {
        NSArray *groups = [self.addressBook groups];
        NSString *groupName = [NSString stringWithFormat:@"%@ %@", [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"], NSLocalizedPONString(@"TITLE_PRINTERS", nil)];
        for (RHGroup *group in groups) {
            if ([group.name isEqualToString:groupName]) {
                self.contactGroup = group;
                break;
            }
        }

        if (self.contactGroup == nil) {
            self.contactGroup = [self.addressBook newGroupInDefaultSource];
            if (self.contactGroup == nil) return nil;

            self.contactGroup.name = groupName;
            [self.contactGroup save];
            [self.addressBook addGroup:self.contactGroup];
            [self.addressBook save];
        }
    }

    NSArray *allContacts = [self.contactGroup members];
    NSString *uniqueNote = [NSString stringWithFormat:@"%@/%@",[[[NSURL URLWithString:self.printer.searchURL] host] lowercaseString], self.printer.printerID];
    for (RHPerson *contact in allContacts) {
        if ([contact.note isEqualToString:uniqueNote]) {
            return contact;
        }
    }
    
    return nil;
}

- (void)removePrinterContact
{
    if (self.addressBook && self.contactRecord && self.contactGroup) {
        [self.contactGroup removeMember:self.contactRecord];
        [self.contactGroup save];
        self.contactGroup = nil;

        [self.contactRecord remove];
        [self.addressBook save];
        self.contactRecord = nil;
    }
}

- (void)addPrinterContact
{
    if (self.addressBook && self.contactGroup) {
        self.contactRecord = [self.addressBook newPersonInDefaultSource];
        [self updateContactRecord];

        [self.contactGroup addMember:self.contactRecord];
        [self.contactGroup save];

        [self.addressBook save];
    }
}

- (void)updateContactRecord
{
    self.contactRecord.firstName = self.printer.organizationDisplayName;
    self.contactRecord.lastName = self.printer.organizationLocationDesc;
    self.contactRecord.organization = self.printer.organizationName;
    self.contactRecord.department = self.printer.organizationDept;
    self.contactRecord.note = [NSString stringWithFormat:@"%@/%@",[[[NSURL URLWithString:self.printer.searchURL] host] lowercaseString], self.printer.printerID];

    // Add Physical Address
    if (![self.printer.printerClass.lowercaseString isEqualToString:@"pull"]) {
        RHMutableMultiDictionaryValue *addressMultiValue = [[RHMutableMultiDictionaryValue alloc] initWithType:kABMultiDictionaryPropertyType];
        NSMutableDictionary *addressDict = [NSMutableDictionary dictionary];
        if ([self.printer.addressLine1 length] > 0) {
            [addressDict setValue:self.printer.addressLine1 forKey:(NSString *)kABPersonAddressStreetKey];
        }
        if ([self.printer.addressPostal length] > 0) {
            addressDict[(NSString *)kABPersonAddressZIPKey] = self.printer.addressPostal;
        }
        if ([self.printer.addressCity length] > 0) {
            addressDict[(NSString *)kABPersonAddressCityKey] = self.printer.addressCity;
        }
        NSString *country = [self.printer getCountryLabel];
        if ([country length] > 0) {
            addressDict[(NSString *)kABPersonAddressCountryKey] = country;
        }
        if ([self.printer.addressState length] > 0 && [country caseInsensitiveCompare:self.printer.addressState] != NSOrderedSame) {
            addressDict[(NSString *)kABPersonAddressStateKey] = self.printer.addressState;
        }
        [addressMultiValue addValue:addressDict withLabel:(NSString *)kABHomeLabel];
        self.contactRecord.addresses = addressMultiValue;
    } else {
        self.contactRecord.addresses = nil;
    }

    // Add Email Address
    NSString *emailAddress = [self.printer getEmailAddressOfType:@"name"];
    if ([emailAddress length] > 0) {
        RHMutableMultiStringValue *mutableEmailMultiValue = [[RHMutableMultiStringValue alloc] initWithType:kABMultiStringPropertyType];
        [mutableEmailMultiValue addValue:emailAddress withLabel:(NSString *)kABHomeLabel];
        self.contactRecord.emails = mutableEmailMultiValue;
    } else {
        emailAddress = [self.printer getEmailAddressOfType:@"num"];
        if ([emailAddress length] > 0) {
            RHMutableMultiStringValue *mutableEmailMultiValue = [[RHMutableMultiStringValue alloc] initWithType:kABMultiStringPropertyType];
            [mutableEmailMultiValue addValue:emailAddress withLabel:(NSString *)kABHomeLabel];
            self.contactRecord.emails = mutableEmailMultiValue;
        }
    }
    
    // Add Web Portal Address
    NSString *webAddress = [self.printer getWebPortalAddressStripPrefix:NO];
    if ([webAddress length] > 0) {
        RHMutableMultiStringValue *mutableURLMultiValue = [[RHMutableMultiStringValue alloc] initWithType:kABMultiStringPropertyType];
        [mutableURLMultiValue addValue:webAddress withLabel:(NSString *)kABHomeLabel];
        self.contactRecord.urls = mutableURLMultiValue;
    }
    
    [self.contactRecord save];
}

- (void)updateAction
{
    [self.updateButton setEnabled:NO];

    // Setup a scratch context in Core Data to use for DirSearch
    if (self.scratchSearchObjectContext == nil) {
        self.scratchSearchObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
        self.scratchSearchObjectContext.undoManager = nil;
    }

    // Setup an object manager to use for DirSearch
    if (self.searchObjectManager == nil) {
        // Get the search service URL for the printer
        NSURL *serviceURL = [NSURL URLWithString:self.printer.searchURL];
        if ([[[serviceURL lastPathComponent] lowercaseString] isEqualToString:@"dirsearch"]) {
            serviceURL = [serviceURL URLByDeletingLastPathComponent];
        }
        NSString *URL = [serviceURL absoluteString];

        self.searchObjectManager = [DirSearch setupSearchForEntity:@"QRPrinter" withService:URL];
    }

    [self performSearch];
}

- (IBAction)selectPrinter:(id)sender
{
    if (self.releaseUI) {
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isMemberOfClass:[JobDetailsViewController class]]) {
                [(JobDetailsViewController *)vc remoteReleaseAPIFromPrinter:self.printer.printerID];
                break;
            }
        }
    } else {
        [Printer setSingletonPrinter:self.printer forEntity:@"SelectedPrinter"];
    }

    // TODO Redo this logic, it's a mess.  Should probably have a recursive function to determine screens to pop
    if (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController && !self.releaseUI) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        UIViewController *backVC = [self backViewController:self.navigationController.viewControllers];
        if ([backVC isMemberOfClass:[PrintersViewController class]] || [backVC isMemberOfClass:[ReleaseViewController class]]) {
            NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];

            // Remove this controller and the Printers controller below it
            [controllers removeObjectAtIndex:controllers.count - 1];
            [controllers removeObjectAtIndex:controllers.count - 1];

            [self.navigationController setViewControllers:controllers animated:YES];
        } else if ([backVC isMemberOfClass:[MapClusterListViewController class]]) {
            NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];

            // Remove this controller, the map cluster list, and the Printers controller below it
            [controllers removeObjectAtIndex:controllers.count - 1];
            [controllers removeObjectAtIndex:controllers.count - 1];
            [controllers removeObjectAtIndex:controllers.count - 1];

            [self.navigationController setViewControllers:controllers animated:YES];
        } else if ([backVC isMemberOfClass:[QRScannerViewController class]]) {
            if (((QRScannerViewController *)backVC).hideCloseButton) {
                NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];

                // Remove this controller, the QR Scanner controller, and the Printers controller below it
                [controllers removeObjectAtIndex:controllers.count - 1];
                [controllers removeObjectAtIndex:controllers.count - 1];
                [controllers removeObjectAtIndex:controllers.count - 1];

                [self.navigationController setViewControllers:controllers animated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        } else if ([backVC isMemberOfClass:[MultiplePrintersViewController class]]) {
            NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];

            // Remove this controller
            [controllers removeObjectAtIndex:controllers.count - 1];
            UIViewController *newBackVC = [self backViewController:controllers];
            
            if ([newBackVC isMemberOfClass:[QRScannerViewController class]]) {
                if (((QRScannerViewController *)newBackVC).hideCloseButton) {
                    // Remove the Multiple Printer controller, the QR Scanner controller, and the Printers controller below it
                    [controllers removeObjectAtIndex:controllers.count - 1];
                    [controllers removeObjectAtIndex:controllers.count - 1];
                    [controllers removeObjectAtIndex:controllers.count - 1];

                    [self.navigationController setViewControllers:controllers animated:YES];
                } else {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
            } else {
                [controllers removeObjectAtIndex:controllers.count - 1];
                [self.navigationController setViewControllers:controllers animated:YES];
            }
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (UIViewController *)backViewController:(NSArray *)controllers {
    NSInteger numberOfViewControllers = controllers.count;

    if (numberOfViewControllers < 2) {
        return nil;
    } else {
        return controllers[numberOfViewControllers - 2];
    }
}

- (void)rotateUpdate
{
    self.shouldRotateUpdate = YES;
    if (!self.isUpdateRotating) {
        [UIView animateWithDuration:0.25f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            self.isUpdateRotating = YES;
            self.updateButton.imageView.transform = CGAffineTransformRotate(self.updateButton.imageView.transform, M_PI_2);
        } completion:^(BOOL finished){
            self.isUpdateRotating = NO;
            if (finished && self.shouldRotateUpdate) {
                [self rotateUpdate];
            }
        }];
    }
}

#pragma mark - DirSearch

- (void)performSearch {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
        @"searchType": @"searchByPrinterNum",
        @"searchPrinterNum": self.printer.printerID ?: @"",
        @"showChildren": @"1",
        @"maxResults": @"1",
    }];

    // Cancel the previous search if it is still running
    [self cancelCurrentSearch];

    // Clear previous results
    [self.scratchSearchObjectContext reset];

    [self rotateUpdate];

    __weak PrinterDetailsViewController *weakSelf = self;
    [DirSearch createSearchWithParameters:params objectManager:self.searchObjectManager managedObjectContext:self.scratchSearchObjectContext success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        __strong PrinterDetailsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchSuccess:mappingResult];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        __strong PrinterDetailsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchFailure:error];
    } completionHandler:^(RKManagedObjectRequestOperation *operation, NSError *error) {
        __strong PrinterDetailsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf searchFailure:error];
        } else {
            strongSelf.currentOperation = operation;
            strongSelf.currentOperation.savesToPersistentStore = NO;
            [[RKObjectManager sharedManager] enqueueObjectRequestOperation:strongSelf.currentOperation];
        }
    }];
}

- (void)cancelCurrentSearch {
    if (self.currentOperation) [self.currentOperation cancel];
}

- (void)searchSuccess:(RKMappingResult *)mappingResult {
    for (id value in [mappingResult array]) {
        if ([value isMemberOfClass:[DirSearch class]]) {
            DirSearch *info = value;

            // The result is actually an error so send to failure
            if (![info.returnCode isEqualToString:@"0"]) {
                NSString *errorText = info.errText ? info.errText : info.returnCode;
                NSError *error = [[NSError alloc] initWithDomain:@"DirSearch" code:-999 userInfo:@{ NSLocalizedDescriptionKey : errorText ?: @""}];
                [self searchFailure:error];
                return;
            }

            if (info.resultCount > 0) {
                [Printer connectRelationshipsFromMapping:mappingResult.dictionary inContext:self.scratchSearchObjectContext];

                for (id printer in [mappingResult array]) {
                    if ([printer isKindOfClass:[Printer class]] && [printer isKindOfClass:[NSManagedObject class]]) {
                        NSManagedObject *managedObject = (NSManagedObject *)printer;
                        if ([managedObject.entity.name isEqualToString:@"ParentPrinter"]) continue;

                        Printer *updatedPrinter = printer;
                        [self.printer updateWithPrinter:updatedPrinter];

                        [Printer setSingletonPrinter:updatedPrinter forEntity:@"DetailsPrinter"];
                        self.printer = [Printer getSingletonPrinterForEntity:@"DetailsPrinter"];

                        // Update the contact record in address book if it exists
                        RHAuthorizationStatus authStatus = [RHAddressBook authorizationStatus];
                        if (authStatus == RHAuthorizationStatusAuthorized) {
                            if (self.contactRecord == nil) self.contactRecord = [self getContactForPrinter];
                            if (self.contactRecord && ![self.contactRecord hasBeenRemoved]) {
                                [self updateContactRecord];
                            }
                        }

                        [self setPrinterDetails];
                        break;
                    }
                }
            }

            break;
        }
    }

    // Display success message
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedPONString(@"LABEL_PRINTER_UPDATED", nil) preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [self presentViewController:alertVC animated:YES completion:nil];

    self.shouldRotateUpdate = NO;
}

- (void)searchFailure:(NSError *)error {
    // Absorb the errors given when cancelling a job so they don't trigger the code after
    if (([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) ||
        ([error.domain isEqualToString:@"org.restkit.RestKit.ErrorDomain"] && error.code == 2)) return;
    
    // Display the error message
    if ([OAuth2Manager isAuthSettingsError:error]) {
        [OAuth2Manager showAuthSettingsError:error overController:self.navigationController];
    } else {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:cancelAction];

        [self presentViewController:alertVC animated:YES completion:nil];
    }

    self.shouldRotateUpdate = NO;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.printerLeftTable) {
        return [self.leftCellData count];
    } else if (tableView == self.printerRightTable) {
        return [self.rightCellData count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"capabilitiesCell";
    CapabilitiesCell *cell = [self.printerLeftTable dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    NSDictionary *data;
    if (tableView == self.printerLeftTable) {
        data = [self leftCellData][indexPath.row];
    } else if (tableView == self.printerRightTable) {
        data = [self rightCellData][indexPath.row];
    }

    [cell setupCell:data];

    return cell;
}

@end
