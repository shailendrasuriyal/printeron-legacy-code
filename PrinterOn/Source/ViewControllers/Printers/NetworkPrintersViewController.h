//
//  NetworkPrintersViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 1/24/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface NetworkPrintersViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet ShadowView *expandHeaderView;
@property (weak, nonatomic) IBOutlet UIImageView *expandHeaderImage;
@property (weak, nonatomic) IBOutlet UILabel *expandHeaderLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *expandScrollView;
@property (weak, nonatomic) IBOutlet ShadowView *expandContentView;
@property (weak, nonatomic) IBOutlet UITableView *expandTableView;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *headerTap;

@end
