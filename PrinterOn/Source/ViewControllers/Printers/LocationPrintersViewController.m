//
//  LocationPrintersViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/18/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "LocationPrintersViewController.h"

#import "CCHMapClusterController.h"
#import "CCHMapClusterAnnotation.h"
#import "DirSearch.h"
#import "JobDetailsViewController.h"
#import "MapClusterListViewController.h"
#import "MKMapView+Zoom.h"
#import "OAuth2Manager.h"
#import "PONAnnotation.h"
#import "Printer.h"
#import "PrinterCell.h"
#import "PrinterDetailsViewController.h"
#import "ReleaseViewController.h"
#import "ResultsCell.h"
#import "UIImage+Color.h"

#import <AddressBookUI/AddressBookUI.h>

@interface LocationPrintersViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *scratchSearchObjectContext;

@property (nonatomic, strong) NSMutableDictionary *searchParameters;
@property (nonatomic, strong) RKObjectManager *searchObjectManager;
@property (nonatomic, strong) RKManagedObjectRequestOperation *currentOperation;

@property (nonatomic, strong) ResultsCell *resultsCell;
@property (nonatomic, strong) UITableViewCell *loadingCell;

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) int totalCount;
@property (nonatomic, assign) int modifyTotalCount;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, assign) CLLocationCoordinate2D searchLocation;
@property (nonatomic, assign) CLLocationCoordinate2D currentLocation;
@property (nonatomic, assign) BOOL hasCurrentLocation;
@property (nonatomic, assign) BOOL updatedUser;

@property (nonatomic, strong) CCHMapClusterController *mapClusterController;
@property (nonatomic, strong) UIImage *mapPin;

@property (nonatomic, assign) BOOL listOpen;
@property (assign, nonatomic) BOOL releaseUI;
@property (strong, nonatomic) NSString *parentNum;
@property (strong, nonatomic) NSString *parentSearchURL;

@end

@implementation LocationPrintersViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        if ([self.tabBarController isMemberOfClass:[ReleaseViewController class]]) {
            self.releaseUI = YES;
        }

        [self setTitle:NSLocalizedPONString(@"TITLE_LOCATION", nil)];
        [[self tabBarItem] setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Location.Image"]]];

        if ([ThemeLoader boolForKey:@"PrintersScreen.Tabbar.UseOffImage"]) {
            [[self tabBarItem] setImage:[[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Location.Image.Off"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        }
        [[self tabBarItem] setSelectedImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Location.Image"]]];

        self.mapPin = [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"Maps.PrinterPin"]];
    }
    
    return self;
}

- (void)viewDidLoad
{
    self.resultsCell = [self.tableView dequeueReusableCellWithIdentifier:@"resultsCell"];
    [self.resultsCell addTarget:self action:@selector(performNextSearch)];
    self.loadingCell = [self.tableView dequeueReusableCellWithIdentifier:@"loadingCell"];

    [super viewDidLoad];

    // Test for location services, will prompt for user authorization if necessary
    [self checkLocationServices];

    // Get the printer ID for the pull printer if this is in release mode from the TabBar controller
    if (self.releaseUI) {
        self.parentNum = ((ReleaseViewController *)self.tabBarController).parentNum;
        self.parentSearchURL = ((ReleaseViewController *)self.tabBarController).parentSearchURL;
    }

    // Setup a scratch context in Core Data to use for DirSearch
    self.scratchSearchObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
    self.scratchSearchObjectContext.undoManager = nil;

    // Setup an object manager to use for DirSearch
    self.searchObjectManager = self.releaseUI ? [DirSearch setupSearchForEntity:@"LocationPrinter" withService:self.parentSearchURL] : [DirSearch setupSearchForEntity:@"LocationPrinter"];

    self.mapClusterController = [[CCHMapClusterController alloc] initWithMapView:self.mapView];
    self.mapClusterController.delegate = self;
    self.mapClusterController.cellSize = self.mapPin.size.height;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    if ([self.fetchedResultsController.fetchedObjects count] > 0) {
        [self updateTableView];
    } else {
        [self updateContentHeight];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = self.releaseUI ? @"Location Release Screen" : @"Location Printers Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];

    if (self.listOpen) [self recenterMapForAnnotations:[self getMapAnnotationsUserLocation:YES] withLocation:self.searchLocation withPadding:[self getPaddingForMapAnnotations]];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.searchBar.placeholder = NSLocalizedPONString(@"LABEL_SEARCHLOCATION", nil);
    [self.searchLocationButton setTitle:NSLocalizedPONString(@"LABEL_SEARCHHERE", nil) forState:UIControlStateNormal];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PrintersScreen.BackgroundColor"];

    [self.userLocationButton setBackgroundColor:[ThemeLoader colorForKey:@"Maps.Button.BackgroundColor"]];
    [self.userLocationButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"Maps.Button.CurrentLocation.Image"]] forState:UIControlStateNormal];
    self.userLocationButton.layer.masksToBounds = NO;
    self.userLocationButton.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.userLocationButton.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.userLocationButton.layer.shadowOpacity = 0.75;
    self.userLocationButton.layer.shadowRadius = 1.0;

    [self.listIcon setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"Maps.Button.List.Image"]]];
    // Add AccessibilityID
    [self.listIcon setAccessibilityIdentifier:@"ListIcon"];
    
    [self.searchLocationButton setBackgroundColor:[ThemeLoader colorForKey:@"Maps.Button.BackgroundColor"]];
    self.searchLocationButton.layer.masksToBounds = NO;
    self.searchLocationButton.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.searchLocationButton.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.searchLocationButton.layer.shadowOpacity = 0.75;
    self.searchLocationButton.layer.shadowRadius = 1.0;

    [self.loadingView setBackgroundColor:[ThemeLoader colorForKey:@"Maps.Button.BackgroundColor"]];
    self.loadingView.layer.masksToBounds = NO;
    self.loadingView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.loadingView.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.loadingView.layer.shadowOpacity = 0.75;
    self.loadingView.layer.shadowRadius = 1.0;
    [self.loadingIndicator setColor:[ThemeLoader colorForKey:@"Maps.Button.IndicatorColor"]];

    // Add mask to top of the view to fade the edge into the background
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.listView.bounds;
    gradient.colors = @[(id)[[UIColor clearColor] CGColor], (id)[[UIColor whiteColor] CGColor]];
    gradient.endPoint = CGPointMake(0.5, 0.035);
    [self.listView.layer setMask:gradient];

    // Customize the Search Bar
    self.searchBar.barStyle = UISearchBarStyleDefault;
    self.searchBar.barTintColor = [ThemeLoader colorForKey:@"PrintersScreen.SearchBar.BackgroundColor"];
    self.searchBar.tintColor = [ThemeLoader colorForKey:@"PrintersScreen.SearchBar.TextColor"];
    // Add AccessibilityId
    [self.searchBar setAccessibilityIdentifier:@"Search Location"];

    UITextField *sbTextField = [self.searchBar valueForKey:@"_searchField"];
    sbTextField.tintColor = [UIColor colorWithRed:20.0/255.0 green:111.0/255.0 blue:225.0/255.0 alpha:1.0];

    // Set the keyboard color
    UITextField *searchTextField = [self.searchBar valueForKey:@"_searchField"];
    THEME_KEYBOARD(searchTextField);
}

- (void)checkLocationServices {
    if ([CLLocationManager locationServicesEnabled]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) {
            self.mapView.showsUserLocation = NO;
        } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            self.locationManager = [CLLocationManager new];
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
}

- (void)showLoadingView
{
    [self.listIcon setHidden:self.isLoading];
    [self.loadingIndicator setHidden:!self.isLoading];

    [self.loadingView setHidden:self.listOpen || !(self.isLoading || self.searchParameters != nil)];
}

- (void)showUserLocationButton:(BOOL)show
{
    [self.userLocationButton setHidden:!show || self.listOpen];
}

- (void)showSearchButton:(BOOL)show
{
    BOOL willHide = !show || self.listOpen;

    if (willHide) {
        [UIView animateWithDuration:0.3 animations:^{
            self.searchLocationButton.alpha = 0;
        } completion:^(BOOL finished) {
            [self.searchLocationButton setHidden:willHide];
        }];
    } else {
        [self.searchLocationButton setHidden:willHide];
        [UIView animateWithDuration:0.3 animations:^{
            self.searchLocationButton.alpha = 1;
        } completion:nil];
    }
}

- (UIEdgeInsets)getPaddingForMapAnnotations
{
    if (!self.listOpen) {
        return UIEdgeInsetsMake(self.mapPin.size.height * 1.25, self.mapPin.size.width * 0.75, self.mapPin.size.height * 1.25, self.mapPin.size.width * 0.75);
    } else {
        int bottom = self.listView.frame.size.height + (self.mapPin.size.height * 0.1);
        return UIEdgeInsetsMake(self.mapPin.size.height * 1.1, self.mapPin.size.width * 0.6, bottom, self.mapPin.size.width * 0.6);
    }
}

- (IBAction)toggleListView:(id)sender {
    self.listOpen = !self.listOpen;

    if (self.listOpen) {
        [self openList];
    } else {
        [self closeList];
    }
}

- (void)openList
{
    // Show the view over top of the map that will stop interaction on the map while the list is open
    [self.blockView setUserInteractionEnabled:NO];
    [self.blockView setHidden:NO];

    // Close all annotation callouts
    for (id <MKAnnotation> annotation in [self.mapView selectedAnnotations]) {
        [self.mapView deselectAnnotation:annotation animated:NO];
    }

    [UIView animateWithDuration:0.3 animations:^{
        // Hide all of the buttons
        self.userLocationButton.alpha = 0;
        self.loadingView.alpha = 0;
        self.searchLocationButton.alpha = 0;
    } completion:^(BOOL finished) {
        [self showUserLocationButton:self.mapView.userLocation.location != nil];
        [self showLoadingView];
        [self.searchLocationButton setHidden:YES];
    }];

    [self recenterMapForAnnotations:[self getMapAnnotationsUserLocation:YES] withLocation:self.searchLocation withPadding:[self getPaddingForMapAnnotations]];

    // Animate the views
    int totalHeight = self.mapView.frame.size.height - 128;
    int listY = self.mapView.frame.origin.y + self.mapView.frame.size.height - self.listView.frame.size.height + 2;
    [UIView animateWithDuration:1.0 * (self.listView.frame.size.height / totalHeight) animations:^{
        [self.listView setFrame:CGRectMake(self.listView.frame.origin.x, listY, self.listView.frame.size.width, self.listView.frame.size.height)];
    } completion:^(BOOL finished) {
        [self.blockView setUserInteractionEnabled:YES];
    }];
}

- (void)closeList
{
    [self.blockView setUserInteractionEnabled:NO];

    [self recenterMapForAnnotations:[self getMapAnnotationsUserLocation:YES] withLocation:self.searchLocation withPadding:[self getPaddingForMapAnnotations]];

    // Animate the views
    int totalHeight = self.mapView.frame.size.height - 128;
    int listY = self.mapView.frame.origin.y + self.mapView.frame.size.height;
    [UIView animateWithDuration:1.0 * (self.listView.frame.size.height / totalHeight) animations:^{
        [self.listView setFrame:CGRectMake(self.listView.frame.origin.x, listY, self.listView.frame.size.width, self.listView.frame.size.height)];
    } completion:^(BOOL finished) {
        [self showUserLocationButton:self.mapView.userLocation.location != nil];
        [self showLoadingView];

        [UIView animateWithDuration:0.3 animations:^{
            // Show the buttons
            self.userLocationButton.alpha = 1;
            self.searchLocationButton.alpha = 1;
            self.loadingView.alpha = 1;
        } completion:nil];

        [self.blockView setHidden:YES];
    }];
}

- (IBAction)pressedCurrentLocation
{
    if (!self.mapView.userLocation.location) return;

    [self showSearchButton:NO];
    self.currentLocation = CLLocationCoordinate2DMake(self.mapView.userLocation.coordinate.latitude, self.mapView.userLocation.coordinate.longitude);
    self.hasCurrentLocation = YES;
    [self.mapView setCenterCoordinate:self.mapView.userLocation.coordinate zoomLevel:12 animated:YES];
    [self reverseGeocodeLocation:self.mapView.userLocation.location];
    [self performSearchWithCoordinates:self.currentLocation calcDistance:YES];
}

- (IBAction)pressedSearchLocation
{
    self.currentLocation = CLLocationCoordinate2DMake(self.mapView.centerCoordinate.latitude, self.mapView.centerCoordinate.longitude);
    self.hasCurrentLocation = YES;
    [self reverseGeocodeLocation:[[CLLocation alloc] initWithLatitude:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude]];
    [self showSearchButton:NO];
    [self performSearchWithCoordinates:self.currentLocation calcDistance:NO];
}

- (void)findLocation
{
    CLGeocoder *geocoder = [CLGeocoder new];
    __weak LocationPrintersViewController *weakSelf = self;
    [geocoder geocodeAddressString:self.searchBar.text completionHandler:^(NSArray *placemarks, NSError *error) {
        __strong LocationPrintersViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error || [placemarks count] == 0) {
            // Display an error message
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_LOCATIONNOTFOUND", nil) preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [strongSelf presentViewController:alertVC animated:YES completion:nil];
        } else if ([placemarks count] > 0) {
            CLPlacemark *topResult = placemarks[0];

            [strongSelf.searchBar setText:ABCreateStringWithAddressDictionary(topResult.addressDictionary, NO)];
            [strongSelf showSearchButton:NO];

            CLLocationCoordinate2D coordinates = topResult.location.coordinate;
            strongSelf.currentLocation = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude);
            strongSelf.hasCurrentLocation = YES;
            [strongSelf.mapView setCenterCoordinate:coordinates zoomLevel:10 animated:YES];
            [strongSelf performSearchWithCoordinates:strongSelf.currentLocation calcDistance:NO];
        }
    }];
}

- (BOOL)compareCoordinates:(CLLocationCoordinate2D)one with:(CLLocationCoordinate2D)two
{
    return (fabs(one.latitude - two.latitude) <= 0.003f && fabs(one.longitude - two.longitude) <= 0.003f);
}

- (void)reverseGeocodeLocation:(CLLocation *)location
{
    CLGeocoder *geoCoder = [CLGeocoder new];
    __weak LocationPrintersViewController *weakSelf = self;
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        __strong LocationPrintersViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error || [placemarks count] == 0) {
            return;
        } else if ([placemarks count] > 0) {
            CLPlacemark *topResult = placemarks[0];
            [strongSelf.searchBar setText:ABCreateStringWithAddressDictionary(topResult.addressDictionary, NO)];
        }
    }];
}

- (BOOL)isMetric
{
    return [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
}

- (NSArray *)getMapAnnotationsUserLocation:(BOOL)check
{
    NSArray *annotations = self.mapClusterController.annotations.allObjects;
    if (self.mapView.userLocation.location) {
        if (check) {
            if (self.mapView.userLocation.location.coordinate.latitude == self.searchLocation.latitude && self.mapView.userLocation.location.coordinate.longitude == self.searchLocation.longitude) {

                NSMutableArray * annotationsPlusLocations = [annotations mutableCopy];
                [annotationsPlusLocations addObject:self.mapView.userLocation];
                return annotationsPlusLocations;
            }
        }
        return annotations;
    } else {
        return annotations;
    }
}

- (void)recenterMapForAnnotations:(NSArray *)annotationArray withLocation:(CLLocationCoordinate2D)location withPadding:(UIEdgeInsets)padding
{
    if ([annotationArray count] == 0) return;

    // Add the annotations
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in annotationArray) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        if (MKMapRectIsNull(zoomRect)) {
            zoomRect = pointRect;
        } else {
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
    }

    // Add the location
    MKMapPoint annotationPoint = MKMapPointForCoordinate(location);
    MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
    zoomRect = MKMapRectUnion(zoomRect, pointRect);

    // Get the center for the new region and set it as the current location so the search here button doesn't show up
    MKCoordinateRegion region =  MKCoordinateRegionForMapRect(zoomRect);
    self.currentLocation = region.center;
    self.hasCurrentLocation = YES;

    [self.mapView setVisibleMapRect:zoomRect edgePadding:padding animated:YES];
}

- (BOOL)printerExistsOnMap:(Printer *)printer withCoords:(NSString *)coords
{
    return [self printerExists:printer inSet:self.mapClusterController.annotations.allObjects withCoords:coords];
}

- (BOOL)printerExists:(Printer *)printer inSet:(NSArray *)set withCoords:(NSString *)coords
{
    if (set.count > 0) {
        for (PONAnnotation *annotation in set) {
            if ([annotation.printer.printerID isEqualToString:printer.printerID]) {
                NSArray *coordsArray = [coords componentsSeparatedByString:@","];
                if (coordsArray.count != 2) continue;
                
                CLLocationCoordinate2D printerCoord = CLLocationCoordinate2DMake([coordsArray.firstObject doubleValue], [coordsArray.lastObject doubleValue]);
                
                if (annotation.coordinate.latitude == printerCoord.latitude && annotation.coordinate.longitude == printerCoord.longitude) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

#pragma mark - DirSearch

- (void)performSearchWithCoordinates:(CLLocationCoordinate2D)coords calcDistance:(BOOL)distance
{
    // Set the search coordinates so we can always know where the search is centered
    self.searchLocation = coords;

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
        @"geoLocation": [NSString stringWithFormat:@"%f,%f", coords.latitude, coords.longitude],
        @"geoProximity": @"50",
        @"geoProximityUnits": @"miles",
        @"calcDistanceFrom": distance ? @"1" : @"0",
        @"calcDistanceUnits": [self isMetric] ? @"km" : @"miles",
        @"showChildren": @"1",
        @"maxResults": @"10000",
        @"pageNum": @"1",
        @"pageSize": @"25",
    }];

    if (self.releaseUI && self.parentNum) {
        params[@"searchParentNum"] = self.parentNum;
    }

    [self performSearch:params];
}

- (void)performSearch:(NSMutableDictionary *)parameters
{
    self.searchParameters = parameters;

    // Cancel the previous search if it is still running
    [self cancelCurrentSearch];

    // If it is a brand new search reset the context and refresh the fetched result controller
    if ([[self searchParameters][@"pageNum"] intValue] == 1) {
        // Remove all annotations except the user location
        [self.mapClusterController removeAnnotations:[self.mapClusterController.annotations allObjects] withCompletionHandler:NULL];
        self.modifyTotalCount = 0;

        [self.scratchSearchObjectContext reset];
        [self performFetch];
    }

    self.isLoading = YES;
    [self showLoadingView];
    [self updateTableView];

    __weak LocationPrintersViewController *weakSelf = self;
    [DirSearch createSearchWithParameters:self.searchParameters objectManager:self.searchObjectManager managedObjectContext:self.scratchSearchObjectContext success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        __strong LocationPrintersViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchSuccess:mappingResult];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        __strong LocationPrintersViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchFailure:error];
    } completionHandler:^(RKManagedObjectRequestOperation *operation, NSError *error) {
        __strong LocationPrintersViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf searchFailure:error];
        } else {
            strongSelf.currentOperation = operation;
            strongSelf.currentOperation.savesToPersistentStore = NO;
            [[RKObjectManager sharedManager] enqueueObjectRequestOperation:strongSelf.currentOperation];
        }
    }];
}

- (void)performNextSearch {
    [self performSearch:self.searchParameters];
}

- (void)cancelCurrentSearch {
    if (self.currentOperation) [self.currentOperation cancel];
}

- (void)searchSuccess:(RKMappingResult *)mappingResult {
    self.isLoading = NO;

    // Update the total count returned
    for (id value in [[mappingResult dictionary] objectEnumerator]) {
        if ([value isMemberOfClass:[DirSearch class]]) {
            DirSearch *info = value;
            
            // The result is actually an error so send to failure
            if (![info.returnCode isEqualToString:@"0"]) {
                NSString *errorText = info.errText ? info.errText : info.returnCode;
                NSError *error = [[NSError alloc] initWithDomain:@"DirSearch" code:-999 userInfo:@{ NSLocalizedDescriptionKey : errorText ?: @""}];
                [self searchFailure:error];
                return;
            }
            
            self.totalCount = info.totalCount;
            break;
        }
    }

    // If we received the results for the first page it's a new search and we should scroll to the top
    int pageNumber = [[self searchParameters][@"pageNum"] intValue];
    if (pageNumber == 1) {
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }

    // Loop through the results and set the page number to each printer to be used for sorting, this is needed to maintain order when paging results.
    for (id object in [mappingResult array]) {
        if ([object isKindOfClass:[Printer class]] && [object isKindOfClass:[NSManagedObject class]]) {
            NSManagedObject *managedObject = (NSManagedObject *)object;
            if ([managedObject.entity.name isEqualToString:@"ParentPrinter"]) continue;

            Printer *printerObject = (Printer *)object;
            [printerObject setValue:@(pageNumber) forKey:@"searchPageNum"];
        }
    }

    [Printer connectRelationshipsFromMapping:mappingResult.dictionary inContext:self.scratchSearchObjectContext];

    NSArray *pins = [self createMapPinsWithData:[mappingResult array]];
    [self.mapClusterController addAnnotations:pins withCompletionHandler:NULL];

    // Update the page number for the next search
    pageNumber++;
    [self.searchParameters setValue:[@(pageNumber) stringValue] forKey:@"pageNum"];

    [self performFetch];
    [self showLoadingView];
    [self updateTableView];

    // Zoom to show all of the added printers
    [self recenterMapForAnnotations:[self getMapAnnotationsUserLocation:YES] withLocation:self.searchLocation withPadding:[self getPaddingForMapAnnotations]];
}

- (void)searchFailure:(NSError *)error {
    // Absorb the errors given when cancelling a job so they don't trigger the code after
    if (([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) ||
        ([error.domain isEqualToString:@"org.restkit.RestKit.ErrorDomain"] && error.code == 2)) return;
    
    self.isLoading = NO;
    
    // If this is the first page mark that we ended in error
    if ([[self searchParameters][@"pageNum"] intValue] == 1) {
        self.totalCount = -1;
        self.modifyTotalCount = 0;
    }

    [self showLoadingView];
    [self updateTableView];

    // Display the error message
    if (self.tabBarController.selectedViewController == self) {
        if ([OAuth2Manager isAuthSettingsError:error]) {
            [OAuth2Manager showAuthSettingsError:error overController:self.navigationController];
        } else {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [self presentViewController:alertVC animated:YES completion:nil];
        }
    }
}

- (NSArray *)createMapPinsWithData:(NSArray *)data
{
    NSMutableArray *pins = [NSMutableArray array];
    NSMutableArray *children = nil;

    // If in release mode just add all child printers to the map
    if (self.releaseUI) {
        for (id object in data) {
            if ([object isKindOfClass:[Printer class]] && [object isKindOfClass:[NSManagedObject class]]) {
                NSManagedObject *managedObject = (NSManagedObject *)object;
                if ([managedObject.entity.name isEqualToString:@"ParentPrinter"]) continue;

                Printer *printer = (Printer *)object;
                [self addPinForPrinter:printer toSet:pins withCoords:printer.addressGeoLocation];
            }
        }

        return pins;
    }

    for (id object in data) {
        if ([object isKindOfClass:[Printer class]] && [object isKindOfClass:[NSManagedObject class]]) {
            NSManagedObject *managedObject = (NSManagedObject *)object;
            if ([managedObject.entity.name isEqualToString:@"ParentPrinter"]) continue;

            Printer *printer = (Printer *)object;
            if (printer.parentIDs.count > 0) {
                // Make an array of all children in the result set
                if (children) [children addObject:printer];
                else children = [NSMutableArray arrayWithObject:printer];
            } else if ([printer.printerClass.lowercaseString isEqualToString:@"pull"]) {
                // We should remove any pull printers from the results because they have no physical location themselves
                [self.scratchSearchObjectContext deleteObject:printer];
                self.modifyTotalCount -= 1;
            } else {
                // If it's a regular printer add it as a pin right away
                [self addPinForPrinter:printer toSet:pins withCoords:printer.addressGeoLocation];
            }
        }
    }

    if (children && children.count > 0) {
        // Get a list of all parents so we don't have to find them each loop
        NSDictionary *parents = [self fetchParentsDictionary];

        for (Printer *child in children) {
            for (NSString *parentID in child.parentIDs) {
                Printer *parent = parents[parentID];
                if (parent) {
                    // Check if the parent already exists as a Location Printer in the results
                    Printer *clone = [self findPrinter:parent forEntityName:child.entity.name inContext:self.scratchSearchObjectContext];
                    if (!clone) {
                        // Parent didn't exist so we clone it in the temp context as a Location Printer
                        clone = [parent clonePrinterInContext:self.scratchSearchObjectContext forEntityName:child.entity.name updateExisting:NO];
                        self.modifyTotalCount += 1;
                    }
                    [self addPinForPrinter:clone toSet:pins withCoords:child.addressGeoLocation];
                }
            }

            // We have added the child as parent printer pins to the map so we can now remove the child from the result set
            [self.scratchSearchObjectContext deleteObject:child];
            self.modifyTotalCount -= 1;
        }
    }

    return pins;
}

- (Printer *)findPrinter:(Printer *)printer forEntityName:(NSString *)entityName inContext:(NSManagedObjectContext *)context
{
    __block Printer *result = nil;

    [context performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(printerID == %@)", printer.printerID];
        fetchRequest.predicate = predicate;

        NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
        if (results.count > 0) {
            result = results.firstObject;
        }
    }];

    return result;
}

- (BOOL)addPinForPrinter:(Printer *)printer toSet:(NSMutableArray *)pins withCoords:(NSString *)coords
{
    if (printer == nil || pins == nil || coords == nil) {
        return NO;
    }

    if (![self printerExistsOnMap:printer withCoords:coords] && ![self printerExists:printer inSet:pins withCoords:coords]) {
        NSArray *coordsArray = [coords componentsSeparatedByString:@","];
        PONAnnotation *pin = [PONAnnotation new];
        [pin setCoordinate:CLLocationCoordinate2DMake([[coordsArray firstObject] doubleValue], [[coordsArray lastObject] doubleValue])];
        [pin setTitle:printer.displayName];
        [pin setSubtitle:printer.organizationLocationDesc];
        [pin setPrinter:printer];
        [pins addObject:pin];
        return YES;
    }

    return NO;
}

- (NSDictionary *)fetchParentsDictionary
{
    __block NSMutableDictionary *result = [NSMutableDictionary dictionary];

    [self.scratchSearchObjectContext performBlockAndWait:^() {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        fetchRequest.entity = [NSEntityDescription entityForName:@"ParentPrinter" inManagedObjectContext:self.scratchSearchObjectContext];

        NSArray *results = [self.scratchSearchObjectContext executeFetchRequest:fetchRequest error:nil];

        if (results.count > 0) {
            for (Printer *parent in results) {
                if (![result objectForKey:parent.printerID]) {
                    [result setObject:parent forKey:parent.printerID];
                }
            }
        }
    }];

    return result;
}

- (NSArray *)annotationsNoDuplicates:(NSArray *)annotations
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (PONAnnotation *annotation in annotations) {
        if (annotation.printer.printerID) {
            dict[annotation.printer.printerID] = annotation;
        }
    }
    return dict.allValues;
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"LocationPrinter"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"searchPageNum" ascending:YES], [NSSortDescriptor sortDescriptorWithKey:@"searchOrder" ascending:YES]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.scratchSearchObjectContext sectionNameKeyPath:nil cacheName:nil];
        
        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];
    
    if (self.isLoading) {
        [self.tableView setTableFooterView:self.loadingCell.contentView];
        [self.tableView scrollRectToVisible:[self.tableView convertRect:self.tableView.tableFooterView.bounds fromView:self.tableView.tableFooterView] animated:NO];
    } else if ([[self searchParameters][@"pageNum"] intValue] == 1 && self.totalCount == -1) {
        [self.tableView setTableFooterView:nil];
    } else {
        int rowCount = (int)[self.fetchedResultsController.fetchedObjects count];
        int pageSize = [[self searchParameters][@"pageSize"] intValue];
        int pageNum = [[self searchParameters][@"pageNum"] intValue];
        
        if (self.totalCount == 0 || rowCount >= (self.totalCount + self.modifyTotalCount) || (pageSize * (pageNum-1)) >= self.totalCount) {
            [self.resultsCell setupCell:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_PRINTERSFOUND", nil), rowCount] isTap:NO];
        } else {
            [self.resultsCell setupCell:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_PRINTERSTOTAL", nil), rowCount, self.totalCount + self.modifyTotalCount] isTap:YES];
        }
        [self.tableView setTableFooterView:self.resultsCell.contentView];
    }
    
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    int visibleMapSize = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 254 : 128;
    int listViewHeight = self.mapView.frame.size.height - visibleMapSize;
    int listY = self.listOpen ? visibleMapSize + 2 : self.mapView.frame.origin.y + self.mapView.frame.size.height;

    int newHeight = self.tableView.contentSize.height + self.scrollContentView.frame.origin.y + 12;
    if (newHeight < listViewHeight) {
        listViewHeight = newHeight;
        listY = self.listOpen ? self.mapView.frame.origin.y + self.mapView.frame.size.height - listViewHeight + 2: self.mapView.frame.origin.y + self.mapView.frame.size.height;
    }

    if (self.listOpen) {
        int totalHeight = self.mapView.frame.size.height - visibleMapSize;

        [UIView animateWithDuration:1.0 * (self.listView.frame.size.height / totalHeight) animations:^{
            [self.listView setFrame:CGRectMake(self.listView.frame.origin.x, listY, self.listView.frame.size.width, listViewHeight)];
        } completion:nil];
    } else {
        [self.listView setFrame:CGRectMake(self.listView.frame.origin.x, listY, self.listView.frame.size.width, listViewHeight)];
    }

    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.scrollContentView setFrame:CGRectMake(self.scrollContentView.frame.origin.x, self.scrollContentView.frame.origin.y, self.scrollContentView.frame.size.width, self.tableView.contentSize.height)];
}

#pragma mark - CCHMapClusterControllerDelegate

- (NSString *)mapClusterController:(CCHMapClusterController *)mapClusterController titleForMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    NSUInteger numAnnotations = mapClusterAnnotation.annotations.count;
    if (numAnnotations == 1) {
        PONAnnotation *annotation = [mapClusterAnnotation.annotations allObjects][0];
        return annotation.title;
    } else {
        NSArray *annotations = [self annotationsNoDuplicates:mapClusterAnnotation.annotations.allObjects];
        return [NSString stringWithFormat:@"%lu %@", (unsigned long)annotations.count, NSLocalizedPONString(@"TITLE_PRINTERS", nil)];
    }
}

- (NSString *)mapClusterController:(CCHMapClusterController *)mapClusterController subtitleForMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    NSUInteger numAnnotations = mapClusterAnnotation.annotations.count;
    if (numAnnotations == 1) {
        PONAnnotation *annotation = [mapClusterAnnotation.annotations allObjects][0];
        return annotation.subtitle;
    }
    return nil;
}

#pragma mark - MKMapViewDelegate

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (!self.updatedUser && !self.hasCurrentLocation) {
        [self showSearchButton:NO];
        [self.mapView setCenterCoordinate:self.mapView.userLocation.coordinate zoomLevel:12 animated:YES];
        self.currentLocation = CLLocationCoordinate2DMake(self.mapView.userLocation.coordinate.latitude, self.mapView.userLocation.coordinate.longitude);
        self.hasCurrentLocation = YES;
        self.updatedUser = YES;
        [self performSearchWithCoordinates:self.currentLocation calcDistance:YES];
    }

    [self showUserLocationButton:YES];
}

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{
    if ([error domain] == kCLErrorDomain && error.code == kCLErrorDenied) {
        [self showUserLocationButton:NO];
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (self.hasCurrentLocation && [self compareCoordinates:self.currentLocation with:self.mapView.centerCoordinate]) {
        [self showSearchButton:NO];
    } else {
        [self showSearchButton:YES];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)view viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    if (annotation != view.userLocation) {
        static NSString *pinID = @"printerPin";
        pinView = (MKAnnotationView *)[view dequeueReusableAnnotationViewWithIdentifier:pinID];
        if (pinView == nil) {
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pinID];
        }
        pinView.image = self.mapPin;
        pinView.canShowCallout = YES;
        pinView.centerOffset = CGPointMake(0, -(pinView.image.size.height/2));
        pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    }
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if ([view.annotation isMemberOfClass:[CCHMapClusterAnnotation class]]) {
        CCHMapClusterAnnotation *annotation = view.annotation;
        NSUInteger numAnnotations = annotation.annotations.count;
        if (numAnnotations == 1) {
            [self performSegueWithIdentifier:@"printerDetailsMap" sender:[annotation.annotations allObjects][0]];
        } else {
            NSArray *annotations = [self annotationsNoDuplicates:annotation.annotations.allObjects];
            [self performSegueWithIdentifier:@"mapClusterList" sender:annotations];
        }
    }
}

/*
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    MKAnnotationView *aV;
    
    for (aV in views) {
        // Don't pin drop if annotation is user location
        if ([aV.annotation isKindOfClass:[MKUserLocation class]]) {
            continue;
        }
        
        // Check if current annotation is inside visible map rect, else go to next one
        MKMapPoint point =  MKMapPointForCoordinate(aV.annotation.coordinate);
        if (!MKMapRectContainsPoint(self.mapView.visibleMapRect, point)) {
            continue;
        }
        
        CGRect endFrame = aV.frame;
        
        // Move annotation out of view
        aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - self.view.frame.size.height, aV.frame.size.width, aV.frame.size.height);
        
        // Animate drop
        [UIView animateWithDuration:0.5 delay:0.04*[views indexOfObject:aV] options:(UIViewAnimationOptions)UIViewAnimationCurveLinear animations:^{
            aV.frame = endFrame;
        } completion:^(BOOL finished) {
            // Animate squash
            if (finished) {
                [UIView animateWithDuration:0.05 animations:^{
                    aV.transform = CGAffineTransformMake(1.0, 0, 0, 0.8, 0, + aV.frame.size.height*0.1);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.1 animations:^{
                        aV.transform = CGAffineTransformIdentity;
                    }];
                }];
            }
        }];
    }
}
*/

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar endEditing:YES];
    [self findLocation];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [self.contentView setUserInteractionEnabled:NO];
    //[self.searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    [self.contentView setUserInteractionEnabled:YES];
    //[self.searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.searchBar isFirstResponder] && [touch view] != self.searchBar) {
        [self.searchBar resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"printerCell";
    PrinterCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Printer *printer = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCellWithPrinter:printer];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.releaseUI) {
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isMemberOfClass:[JobDetailsViewController class]]) {
                Printer *printer = [self.fetchedResultsController objectAtIndexPath:indexPath];
                [(JobDetailsViewController *)vc remoteReleaseAPIFromPrinter:printer.printerID];
                break;
            }
        }
    } else {
        [Printer setSingletonPrinter:[self.fetchedResultsController objectAtIndexPath:indexPath] forEntity:@"SelectedPrinter"];
    }

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && !self.releaseUI) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printerDetailsLocation"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        [Printer setSingletonPrinter:[self.fetchedResultsController objectAtIndexPath:indexPath] forEntity:@"DetailsPrinter"];
        PrinterDetailsViewController *destViewController = segue.destinationViewController;
        destViewController.releaseUI = self.releaseUI;
    } else if ([segue.identifier isEqualToString:@"printerDetailsMap"]) {
        PONAnnotation *annotation = (PONAnnotation *)sender;
        [Printer setSingletonPrinter:annotation.printer forEntity:@"DetailsPrinter"];
        PrinterDetailsViewController *destViewController = segue.destinationViewController;
        destViewController.releaseUI = self.releaseUI;
    } else if ([segue.identifier isEqualToString:@"mapClusterList"]) {
        MapClusterListViewController *destViewController = segue.destinationViewController;
        destViewController.printersList = [sender copy];
        destViewController.releaseUI = self.releaseUI;
    }
}

@end
