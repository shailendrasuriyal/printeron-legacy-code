//
//  NetworkPrintersViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 1/24/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "NetworkPrintersViewController.h"

#import "DirSearch.h"
#import "NetworkBrowser.h"
#import "NetworkServiceCell.h"
#import "NetworkServiceHeader.h"
#import "OAuth2Manager.h"
#import "PONNetworkService.h"
#import "Printer.h"
#import "PrinterCell.h"
#import "PrinterDetailsViewController.h"
#import "ResultsCell.h"

@interface NetworkPrintersViewController ()

@property (nonatomic, strong) NSArray *services;
@property (nonatomic, strong) NSPredicate *servicePredicate;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *scratchSearchObjectContext;

@property (nonatomic, strong) NSMutableDictionary *searchParameters;
@property (nonatomic, strong) RKObjectManager *searchObjectManager;
@property (nonatomic, strong) RKManagedObjectRequestOperation *currentOperation;

@property (nonatomic, strong) ResultsCell *resultsCell;
@property (nonatomic, strong) UITableViewCell *loadingCell;

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) int totalCount;

@end

@implementation NetworkPrintersViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) {
        [self setTitle:NSLocalizedPONString(@"TITLE_NETWORK", nil)];
        [[self tabBarItem] setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Network.Image"]]];

        if ([ThemeLoader boolForKey:@"PrintersScreen.Tabbar.UseOffImage"]) {
            [[self tabBarItem] setImage:[[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Network.Image.Off"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        }
        [[self tabBarItem] setSelectedImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Network.Image"]]];
    }

    return self;
}

- (void)viewDidLoad
{
    self.resultsCell = [self.expandTableView dequeueReusableCellWithIdentifier:@"resultsCell"];
    [self.resultsCell addTarget:self action:@selector(performNextSearch)];
    self.loadingCell = [self.expandTableView dequeueReusableCellWithIdentifier:@"loadingCell"];

    [super viewDidLoad];
    self.services = [NetworkBrowser getPONServicesArray];

    // Setup a scratch context in Core Data to use for DirSearch
    self.scratchSearchObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
    self.scratchSearchObjectContext.undoManager = nil;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Register for changes to Network Services
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(serviceChangedNotification:) name:kNetworkBrowserServiceChangeNotification object:nil];

    [self updateTableView];

    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    if ([self.fetchedResultsController.fetchedObjects count] > 0) {
        [self updateExpandedTableView];
    } else {
        [self updateExpandedContentHeight];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Network Printers Screen";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Unregister notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNetworkBrowserServiceChangeNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
    [self updateExpandedContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PrintersScreen.BackgroundColor"];
    self.expandHeaderView.backgroundColor = [ThemeLoader colorForKey:@"NetworkPrinters.TableHeader.BackgroundColor"];

    [self.expandHeaderImage setImage:[[ImageManager sharedImageManager] imageNamed:@"ChevronLeft"]];
}

- (void)serviceChangedNotification:(NSNotification *)notification
{
    [self updateTableView];
}

- (IBAction)backOverallPressed:(id)sender {
    [self cancelCurrentSearch];

    [UIView animateWithDuration:0.25 animations:^{
        self.expandHeaderView.alpha = 0;
        self.expandScrollView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.expandHeaderView setHidden:YES];
        [self.expandScrollView setHidden:YES];

        [self.scrollView setHidden:NO];

        [UIView animateWithDuration:0.25 animations:^{
            self.scrollView.alpha = 1;
        } completion:nil];
    }];
}

- (void)updateTableView
{
    if (self.servicePredicate == nil) {
        self.servicePredicate = [NSPredicate predicateWithFormat:@"printerCount != %ld || isUntrustedCert == YES", NSNotFound];
    }
    self.services = [[NetworkBrowser getPONServicesArray] filteredArrayUsingPredicate:self.servicePredicate];
    [self.tableView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

#pragma mark - DirSearch

- (void)performSearch:(NSMutableDictionary *)parameters {
    self.searchParameters = parameters;

    // Cancel the previous search if it is still running
    [self cancelCurrentSearch];

    // If it is a brand new search reset the context and refresh the fetched result controller
    if ([[self searchParameters][@"pageNum"] intValue] == 1) {
        [self.scratchSearchObjectContext reset];
        [self performFetch];
    }

    self.isLoading = YES;
    [self updateExpandedTableView];

    __weak NetworkPrintersViewController *weakSelf = self;
    [DirSearch createSearchWithParameters:self.searchParameters objectManager:self.searchObjectManager managedObjectContext:self.scratchSearchObjectContext success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        __strong NetworkPrintersViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchSuccess:mappingResult];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        __strong NetworkPrintersViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchFailure:error];
    } completionHandler:^(RKManagedObjectRequestOperation *operation, NSError *error) {
        __strong NetworkPrintersViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf searchFailure:error];
        } else {
            strongSelf.currentOperation = operation;
            strongSelf.currentOperation.savesToPersistentStore = NO;
            [[RKObjectManager sharedManager] enqueueObjectRequestOperation:strongSelf.currentOperation];
        }
    }];
}

- (void)performNetworkSearch:(PONNetworkService *)service {
    self.searchObjectManager = [DirSearch setupSearchForEntity:@"NetworkPrinter" withService:service.serviceURL];

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"maxResults": @"10000",
                                                                                  @"pageNum": @"1",
                                                                                  @"pageSize": @"25",
                                                                                }];

    if (service.serviceType == 2) {
        [params setValue:@"searchByServiceID" forKey:@"searchType"];
        [params setValue:service.serviceID forKey:@"searchServiceID"];
    }

    [self performSearch:params];
}

- (void)performNextSearch {
    [self performSearch:self.searchParameters];
}

- (void)cancelCurrentSearch {
    if (self.currentOperation) [self.currentOperation cancel];
}

- (void)searchSuccess:(RKMappingResult *)mappingResult {
    self.isLoading = NO;
    // Update the total count returned
    for (id value in [[mappingResult dictionary] objectEnumerator]) {
        if ([value isMemberOfClass:[DirSearch class]]) {
            DirSearch *info = value;
            
            // The result is actually an error so send to failure
            if (![info.returnCode isEqualToString:@"0"]) {
                NSString *errorText = info.errText ? info.errText : info.returnCode;
                NSError *error = [[NSError alloc] initWithDomain:@"DirSearch" code:-999 userInfo:@{ NSLocalizedDescriptionKey : errorText ?: @""}];
                [self searchFailure:error];
                return;
            }
            
            self.totalCount = info.totalCount;
            break;
        }
    }

    // If we received the results for the first page it's a new search and we should scroll to the top
    int pageNumber = [[self searchParameters][@"pageNum"] intValue];
    if (pageNumber == 1) {
        [self.expandTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }
    
    // Loop through the results and set the page number to each printer to be used for sorting, this is needed to maintain order when paging results
    for (id object in [mappingResult array]) {
        if ([object isKindOfClass:[Printer class]]) {
            [((Printer *)object) setValue:@(pageNumber) forKey:@"searchPageNum"];
        }
    }

    [Printer connectRelationshipsFromMapping:mappingResult.dictionary inContext:self.scratchSearchObjectContext];
    
    // Update the page number for the next search
    pageNumber++;
    [self.searchParameters setValue:[@(pageNumber) stringValue] forKey:@"pageNum"];
    
    [self performFetch];
    [self updateExpandedTableView];
}

- (void)searchFailure:(NSError *)error {
    // Absorb the errors given when cancelling a job so they don't trigger the code after
    if (([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) ||
        ([error.domain isEqualToString:@"org.restkit.RestKit.ErrorDomain"] && error.code == 2)) return;
    
    self.isLoading = NO;
    
    // If this is the first page mark that we ended in error
    if ([[self searchParameters][@"pageNum"] intValue] == 1) {
        self.totalCount = -1;
    }
    
    [self updateExpandedTableView];
    
    // Display the error message
    if (self.tabBarController.selectedViewController == self && !self.expandHeaderView.isHidden) {
        if ([OAuth2Manager isAuthSettingsError:error]) {
            [OAuth2Manager showAuthSettingsError:error overController:self.navigationController];
        } else {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [self presentViewController:alertVC animated:YES completion:nil];
        }
    }
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"NetworkPrinter"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"searchPageNum" ascending:YES], [NSSortDescriptor sortDescriptorWithKey:@"searchOrder" ascending:YES]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.scratchSearchObjectContext sectionNameKeyPath:nil cacheName:nil];
        
        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateExpandedTableView {
    [self.expandTableView reloadData];
    
    if (self.isLoading) {
        [self.expandTableView setTableFooterView:self.loadingCell.contentView];
        [self.expandTableView scrollRectToVisible:[self.expandTableView convertRect:self.expandTableView.tableFooterView.bounds fromView:self.expandTableView.tableFooterView] animated:NO];
    } else if ([[self searchParameters][@"pageNum"] intValue] == 1 && self.totalCount == -1) {
        [self.expandTableView setTableFooterView:nil];
    } else {
        int rowCount = (int)[self.fetchedResultsController.fetchedObjects count];
        int pageSize = [[self searchParameters][@"pageSize"] intValue];
        int pageNum = [[self searchParameters][@"pageNum"] intValue];
        
        if (self.totalCount == 0 || rowCount >= self.totalCount || (pageSize * (pageNum-1)) >= self.totalCount) {
            [self.resultsCell setupCell:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_PRINTERSFOUND", nil), rowCount] isTap:NO];
        } else {
            [self.resultsCell setupCell:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_PRINTERSTOTAL", nil), rowCount, self.totalCount] isTap:YES];
        }
        [self.expandTableView setTableFooterView:self.resultsCell.contentView];
    }
    
    [self updateExpandedContentHeight];
}

- (void)updateExpandedContentHeight
{
    [self.expandScrollView setContentSize:CGSizeMake(self.expandScrollView.frame.size.width, self.expandTableView.contentSize.height)];
    [self.expandContentView setFrame:CGRectMake(self.expandContentView.frame.origin.x, self.expandContentView.frame.origin.y, self.expandContentView.frame.size.width, self.expandTableView.contentSize.height)];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView isEqual:self.expandTableView]) {
        return [[self.fetchedResultsController sections] count];
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tableView]) {
        if ([self.tableView.dataSource tableView:self.tableView numberOfRowsInSection:section] == 0) {
            return 0;
        } else {
            return 22;
        }
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tableView]) {
        static NSString *cellIdentifier = @"sectionHeader";
        NetworkServiceHeader *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        [cell.contentView setBackgroundColor:[ThemeLoader colorForKey:@"NetworkPrinters.TableHeader.BackgroundColor"]];
        [cell.labelTitle setText:NSLocalizedPONString(@"LABEL_SERVICES", nil)];
        return cell;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.expandTableView]) {
        id sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    } else {
        return [self.services count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.expandTableView]) {
        static NSString *cellIdentifier = @"printerCell";
        PrinterCell *cell = [self.expandTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
        Printer *printer = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [cell setupCellWithPrinter:printer];
    
        return cell;
    } else {
        static NSString *cellIdentifier = @"netServiceCell";
        NetworkServiceCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
        PONNetworkService *service = [self services][indexPath.row];
        [cell setupCellWithService:service];
    
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tableView]) {
        PONNetworkService *service = [self services][indexPath.row];
        if (!service.isUntrustedCert) {
            [self.expandHeaderLabel setText:service.serviceName];

            [UIView animateWithDuration:0.25 animations:^{
                self.scrollView.alpha = 0;
            } completion:^(BOOL finished) {
                [self.scrollView setHidden:YES];
                [self.expandHeaderView setHidden:NO];
                [self.expandScrollView setHidden:NO];

                [self performNetworkSearch:service];

                [UIView animateWithDuration:0.25 animations:^{
                    self.expandHeaderView.alpha = 1;
                    self.expandScrollView.alpha = 1;
                } completion:nil];
            }];
        }
    } else {
        [Printer setSingletonPrinter:[self.fetchedResultsController objectAtIndexPath:indexPath] forEntity:@"SelectedPrinter"];

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printerDetailsNetwork"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.expandTableView];
        NSIndexPath *indexPath = [self.expandTableView indexPathForRowAtPoint:buttonPosition];
        [Printer setSingletonPrinter:[self.fetchedResultsController objectAtIndexPath:indexPath] forEntity:@"DetailsPrinter"];
    }
}

@end
