//
//  PrinterDetailsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/6/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class PONButton;

@interface PrinterDetailsViewController : BaseViewController <UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet ShadowView *overView;
@property (weak, nonatomic) IBOutlet UILabel *overviewLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *overviewLabelSubtitle;

@property (weak, nonatomic) IBOutlet ShadowView *buttonView;
@property (weak, nonatomic) IBOutlet PONButton *saveButton;
@property (weak, nonatomic) IBOutlet PONButton *contactsButton;
@property (weak, nonatomic) IBOutlet PONButton *updateButton;

@property (weak, nonatomic) IBOutlet ShadowView *pullPrinterView;
@property (weak, nonatomic) IBOutlet UILabel *pullPrinterText;

@property (weak, nonatomic) IBOutlet ShadowView *locationView;
@property (weak, nonatomic) IBOutlet UIImageView *imageMap;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mapIndicator;
@property (weak, nonatomic) IBOutlet UILabel *locationLabelAddressTitle;
@property (weak, nonatomic) IBOutlet UILabel *locationAddress1;
@property (weak, nonatomic) IBOutlet UILabel *locationAddress2;

@property (weak, nonatomic) IBOutlet ShadowView *printerView;
@property (weak, nonatomic) IBOutlet UIImageView *printerImage;
@property (weak, nonatomic) IBOutlet UILabel *printerManufacturer;
@property (weak, nonatomic) IBOutlet UILabel *printerModel;
@property (weak, nonatomic) IBOutlet UIImageView *printerStatus;
@property (weak, nonatomic) IBOutlet UITableView *printerLeftTable;
@property (weak, nonatomic) IBOutlet UITableView *printerRightTable;
@property (weak, nonatomic) IBOutlet UIView *printerBottomLine;

@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIImageView *emailImage;
@property (weak, nonatomic) IBOutlet UILabel *emailTitle;
@property (weak, nonatomic) IBOutlet UILabel *emailAddress;

@property (weak, nonatomic) IBOutlet UIView *webView;
@property (weak, nonatomic) IBOutlet UIImageView *webImage;
@property (weak, nonatomic) IBOutlet UILabel *webTitle;
@property (weak, nonatomic) IBOutlet UILabel *webAddress;

@property (weak, nonatomic) IBOutlet ShadowView *hoursView;
@property (weak, nonatomic) IBOutlet UILabel *hoursTitle;
@property (weak, nonatomic) IBOutlet UILabel *hoursText;

@property (weak, nonatomic) IBOutlet ShadowView *locationDescriptionView;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionTitle;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionText;

@property (nonatomic, assign) BOOL showClose;
@property (assign, nonatomic) BOOL releaseUI;

@end
