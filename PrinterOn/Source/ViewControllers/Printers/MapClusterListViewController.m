//
//  MapClusterListViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 7/20/2015.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "MapClusterListViewController.h"

#import "JobDetailsViewController.h"
#import "PONAnnotation.h"
#import "PrinterDetailsViewController.h"
#import "Printer.h"
#import "PrinterCell.h"

@interface MapClusterListViewController ()

@end

@implementation MapClusterListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.printersList && self.printersList.count > 0) {
        NSSortDescriptor *firstSort = [NSSortDescriptor sortDescriptorWithKey:@"printer.displayName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        NSSortDescriptor *secondSort = [NSSortDescriptor sortDescriptorWithKey:@"printer.organizationLocationDesc" ascending:YES  selector:@selector(caseInsensitiveCompare:)];
        self.printersList = [self.printersList sortedArrayUsingDescriptors:@[firstSort,secondSort]];
        [self.tableView reloadData];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Map Cluster List Screen";
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = [NSString stringWithFormat:@"%lu %@", (unsigned long)self.printersList.count, NSLocalizedPONString(@"TITLE_PRINTERS", nil)];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PrintersScreen.BackgroundColor"];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.printersList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"printerCell";
    PrinterCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    PONAnnotation *annotation = [self printersList][indexPath.row];
    Printer *printer = annotation.printer;
    [cell setupCellWithPrinter:printer];

    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.releaseUI) {
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isMemberOfClass:[JobDetailsViewController class]]) {
                PONAnnotation *annotation = [self printersList][indexPath.row];
                Printer *printer = annotation.printer;
                [(JobDetailsViewController *)vc remoteReleaseAPIFromPrinter:printer.printerID];
                break;
            }
        }
    } else {
        PONAnnotation *annotation = [self printersList][indexPath.row];
        [Printer setSingletonPrinter:annotation.printer forEntity:@"SelectedPrinter"];
    }

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && !self.releaseUI) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        // Remove this controller and the Printers controller below it
        NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];
        [controllers removeObjectAtIndex:controllers.count - 1];
        [controllers removeObjectAtIndex:controllers.count - 1];

        [self.navigationController setViewControllers:controllers animated:YES];
    }
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printerDetailsClusterList"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        PONAnnotation *annotation = [self printersList][indexPath.row];
        [Printer setSingletonPrinter:annotation.printer forEntity:@"DetailsPrinter"];
        PrinterDetailsViewController *destViewController = segue.destinationViewController;
        destViewController.releaseUI = self.releaseUI;
    }
}

@end
