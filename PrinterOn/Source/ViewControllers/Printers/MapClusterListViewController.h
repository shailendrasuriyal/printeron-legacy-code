//
//  MapClusterListViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 7/20/2015.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface MapClusterListViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *printersList;
@property (assign, nonatomic) BOOL releaseUI;

@end
