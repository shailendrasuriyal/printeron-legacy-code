//
//  SearchResultsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 10/29/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface SearchResultsViewController : BaseViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
