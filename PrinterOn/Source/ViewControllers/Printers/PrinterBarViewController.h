//
//  PrinterBarViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-15.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class BBCyclingLabel;

@interface PrinterBarViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet ShadowView *cellView;
@property (weak, nonatomic) IBOutlet UIButton *buttonLeft;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet BBCyclingLabel *labelCycling;
@property (weak, nonatomic) IBOutlet UIButton *buttonRight;

- (void)registerPrinterNotification;

@end
