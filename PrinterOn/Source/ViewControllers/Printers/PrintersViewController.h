//
//  PrintersViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-20.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface PrintersViewController : BaseTabBarController

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *scanQRButton;

@property (nonatomic, assign) BOOL wasOpenedIn;

@end
