//
//  LocationPrintersViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/18/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "CCHMapClusterControllerDelegate.h"

@interface LocationPrintersViewController : BaseViewController <CCHMapClusterControllerDelegate, MKMapViewDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *userLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *searchLocationButton;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIImageView *listIcon;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIView *blockView;
@property (weak, nonatomic) IBOutlet UIView *listView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *scrollContentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

// These gestures have to be defined and connected or this view crashes in iOS5 because for some reason the gestures are released
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *openTap;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *closeTap;

@end
