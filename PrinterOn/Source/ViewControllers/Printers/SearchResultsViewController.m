//
//  SearchResultsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 10/29/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "SearchResultsViewController.h"

#import "DirSearch.h"
#import "JobDetailsViewController.h"
#import "OAuth2Manager.h"
#import "Printer.h"
#import "PrinterCell.h"
#import "PrinterDetailsViewController.h"
#import "ReleaseViewController.h"
#import "ResultsCell.h"
#import "UIImage+Color.h"

@interface SearchResultsViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *scratchSearchObjectContext;

@property (nonatomic, strong) NSMutableDictionary *searchParameters;
@property (nonatomic, strong) RKObjectManager *searchObjectManager;
@property (nonatomic, strong) RKManagedObjectRequestOperation *currentOperation;

@property (nonatomic, strong) ResultsCell *resultsCell;
@property (nonatomic, strong) UITableViewCell *loadingCell;

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) int totalCount;

@property (assign, nonatomic) BOOL releaseUI;
@property (strong, nonatomic) NSString *parentNum;
@property (strong, nonatomic) NSString *parentSearchURL;

@end

@implementation SearchResultsViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        if ([self.tabBarController isMemberOfClass:[ReleaseViewController class]]) {
            self.releaseUI = YES;
        }

        [self setTitle:NSLocalizedPONString(@"TITLE_SEARCH", nil)];
        [[self tabBarItem] setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Search.Image"]]];

        if ([ThemeLoader boolForKey:@"PrintersScreen.Tabbar.UseOffImage"]) {
            [[self tabBarItem] setImage:[[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Search.Image.Off"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        }
        [[self tabBarItem] setSelectedImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Search.Image"]]];
    }
    
    return self;
}

- (void)viewDidLoad
{
    self.resultsCell = [self.tableView dequeueReusableCellWithIdentifier:@"resultsCell"];
    [self.resultsCell addTarget:self action:@selector(performNextSearch)];
    self.loadingCell = [self.tableView dequeueReusableCellWithIdentifier:@"loadingCell"];

    [super viewDidLoad];

    // Get the printer ID for the pull printer if this is in release mode from the TabBar controller
    if (self.releaseUI) {
        self.parentNum = ((ReleaseViewController *)self.tabBarController).parentNum;
        self.parentSearchURL = ((ReleaseViewController *)self.tabBarController).parentSearchURL;
    }

    // Setup a scratch context in Core Data to use for DirSearch
    self.scratchSearchObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
    self.scratchSearchObjectContext.undoManager = nil;

    // Setup an object manager to use for DirSearch
    self.searchObjectManager = self.releaseUI ? [DirSearch setupSearchForEntity:@"SearchPrinter" withService:self.parentSearchURL] : [DirSearch setupSearchForEntity:@"SearchPrinter"];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    if ([self.fetchedResultsController.fetchedObjects count] > 0) {
        [self updateTableView];
    } else {
        [self updateContentHeight];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = self.releaseUI ? @"Search Release Screen" : @"Search Printers Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.searchBar.placeholder = NSLocalizedPONString(@"LABEL_SEARCHKEYWORD", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PrintersScreen.BackgroundColor"];

    // Customize the Search Bar
    self.searchBar.barStyle = UISearchBarStyleDefault;
    self.searchBar.barTintColor = [ThemeLoader colorForKey:@"PrintersScreen.SearchBar.BackgroundColor"];
    self.searchBar.tintColor = [ThemeLoader colorForKey:@"PrintersScreen.SearchBar.TextColor"];
    // Add AccessibilityId
    [self.searchBar setAccessibilityIdentifier:@"Search Keywords"];

    UITextField *sbTextField = [self.searchBar valueForKey:@"_searchField"];
    sbTextField.tintColor = [UIColor colorWithRed:20.0/255.0 green:111.0/255.0 blue:225.0/255.0 alpha:1.0];
    
    // Set the keyboard color
    UITextField *searchTextField = [self.searchBar valueForKey:@"_searchField"];
    THEME_KEYBOARD(searchTextField);
}

#pragma mark - DirSearch

- (void)performSearch:(NSMutableDictionary *)parameters {
    self.searchParameters = parameters;

    // Cancel the previous search if it is still running
    [self cancelCurrentSearch];

    // If it is a brand new search reset the context and refresh the fetched result controller
    if ([[self searchParameters][@"pageNum"] intValue] == 1) {
        [self.scratchSearchObjectContext reset];
        [self performFetch];
    }

    self.isLoading = YES;
    [self updateTableView];

    __weak SearchResultsViewController *weakSelf = self;
    [DirSearch createSearchWithParameters:self.searchParameters objectManager:self.searchObjectManager managedObjectContext:self.scratchSearchObjectContext success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        __strong SearchResultsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchSuccess:mappingResult];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        __strong SearchResultsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchFailure:error];
    } completionHandler:^(RKManagedObjectRequestOperation *operation, NSError *error) {
        __strong SearchResultsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf searchFailure:error];
        } else {
            strongSelf.currentOperation = operation;
            strongSelf.currentOperation.savesToPersistentStore = NO;
            [[RKObjectManager sharedManager] enqueueObjectRequestOperation:strongSelf.currentOperation];
        }
    }];
}

- (void)performNextSearch {
    [self performSearch:self.searchParameters];
}

- (void)cancelCurrentSearch {
    if (self.currentOperation) [self.currentOperation cancel];
}

- (void)searchSuccess:(RKMappingResult *)mappingResult {
    self.isLoading = NO;

    // Update the total count returned
    for (id value in [[mappingResult dictionary] objectEnumerator]) {
        if ([value isMemberOfClass:[DirSearch class]]) {
            DirSearch *info = value;
            
            // The result is actually an error so send to failure
            if (![info.returnCode isEqualToString:@"0"]) {
                NSString *errorText = info.errText ? info.errText : info.returnCode;
                NSError *error = [[NSError alloc] initWithDomain:@"DirSearch" code:-999 userInfo:@{ NSLocalizedDescriptionKey : errorText ?: @""}];
                [self searchFailure:error];
                return;
            }
            
            self.totalCount = info.totalCount;
            break;
        }
    }

    // If we received the results for the first page it's a new search and we should scroll to the top
    int pageNumber = [[self searchParameters][@"pageNum"] intValue];
    if (pageNumber == 1) {
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }

    // Loop through the results and set the page number to each printer to be used for sorting, this is needed to maintain order when paging results
    for (id object in [mappingResult array]) {
        if ([object isKindOfClass:[Printer class]]) {
            [((Printer *)object) setValue:@(pageNumber) forKey:@"searchPageNum"];
        }
    }

    [Printer connectRelationshipsFromMapping:mappingResult.dictionary inContext:self.scratchSearchObjectContext];

    // Update the page number for the next search
    pageNumber++;
    [self.searchParameters setValue:[@(pageNumber) stringValue] forKey:@"pageNum"];

    [self performFetch];
    [self updateTableView];
}

- (void)searchFailure:(NSError *)error {
    // Absorb the errors given when cancelling a job so they don't trigger the code after
    if (([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) ||
        ([error.domain isEqualToString:@"org.restkit.RestKit.ErrorDomain"] && error.code == 2)) return;

    self.isLoading = NO;

    // If this is the first page mark that we ended in error
    if ([[self searchParameters][@"pageNum"] intValue] == 1) {
        self.totalCount = -1;
    }

    [self updateTableView];

    // Display the error message
    if (self.tabBarController.selectedViewController == self) {
        if ([OAuth2Manager isAuthSettingsError:error]) {
            [OAuth2Manager showAuthSettingsError:error overController:self.navigationController];
        } else {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [self presentViewController:alertVC animated:YES completion:nil];
        }
    }
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SearchPrinter"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"searchPageNum" ascending:YES], [NSSortDescriptor sortDescriptorWithKey:@"searchOrder" ascending:YES]];

        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.scratchSearchObjectContext sectionNameKeyPath:nil cacheName:nil];

        [self performFetch];
    }

    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];

    if (self.isLoading) {
        [self.tableView setTableFooterView:self.loadingCell.contentView];
        [self.tableView scrollRectToVisible:[self.tableView convertRect:self.tableView.tableFooterView.bounds fromView:self.tableView.tableFooterView] animated:NO];
    } else if ([[self searchParameters][@"pageNum"] intValue] == 1 && self.totalCount == -1) {
        [self.tableView setTableFooterView:nil];
    } else {
        int rowCount = (int)[self.fetchedResultsController.fetchedObjects count];
        int pageSize = [[self searchParameters][@"pageSize"] intValue];
        int pageNum = [[self searchParameters][@"pageNum"] intValue];

        if (self.totalCount == 0 || rowCount >= self.totalCount || (pageSize * (pageNum-1)) >= self.totalCount) {
            [self.resultsCell setupCell:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_PRINTERSFOUND", nil), rowCount] isTap:NO];
        } else {
            [self.resultsCell setupCell:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_PRINTERSTOTAL", nil), rowCount, self.totalCount] isTap:YES];
        }
        [self.tableView setTableFooterView:self.resultsCell.contentView];
    }

    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar endEditing:YES];

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
        @"searchText": searchBar.text ?: @"",
        @"maxResults": @"10000",
        @"pageNum": @"1",
        @"pageSize": @"25",
    }];

    if (self.releaseUI && self.parentNum) {
        params[@"searchParentNum"] = self.parentNum;
        params[@"showChildren"] = @"1";
    }

    [self performSearch:params];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [self.scrollView setUserInteractionEnabled:NO];
    //[self.searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    [self.scrollView setUserInteractionEnabled:YES];
    //[self.searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.searchBar isFirstResponder] && [touch view] != self.searchBar) {
        [self searchBarCancelButtonClicked:self.searchBar];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"printerCell";
    PrinterCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    Printer *printer = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCellWithPrinter:printer];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.releaseUI) {
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isMemberOfClass:[JobDetailsViewController class]]) {
                Printer *printer = [self.fetchedResultsController objectAtIndexPath:indexPath];
                [(JobDetailsViewController *)vc remoteReleaseAPIFromPrinter:printer.printerID];
                break;
            }
        }
    } else {
        [Printer setSingletonPrinter:[self.fetchedResultsController objectAtIndexPath:indexPath] forEntity:@"SelectedPrinter"];
    }

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && !self.releaseUI) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printerDetailsSearch"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        [Printer setSingletonPrinter:[self.fetchedResultsController objectAtIndexPath:indexPath] forEntity:@"DetailsPrinter"];
        PrinterDetailsViewController *destViewController = segue.destinationViewController;
        destViewController.releaseUI = self.releaseUI;
    }
}

@end
