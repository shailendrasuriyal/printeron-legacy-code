//
//  PrintersViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-20.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrintersViewController.h"

#import "BarButtonItem.h"
#import "LocationPrintersViewController.h"
#import "NetworkPrintersViewController.h"
#import "QRScannerViewController.h"
#import "SearchResultsViewController.h"
#import "SplashScreen.h"

@interface PrintersViewController ()

@end

@implementation PrintersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Hide QR scan button if no camera is present
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] || [ThemeLoader boolForKey:@"QRScanner.isHidden"]) {
        [self.scanQRButton setHidden:YES];
    }

    NSMutableArray *hideTabs = [NSMutableArray array];
    if ([ThemeLoader boolForKey:@"PrintersScreen.Tabbar.HideNetworkBrowser"]) {
        [hideTabs addObject:[NetworkPrintersViewController class]];
    }
    if ([ThemeLoader boolForKey:@"PrintersScreen.Tabbar.HideKeywordSearch"]) {
        [hideTabs addObject:[SearchResultsViewController class]];
    }

    // Hide tabs according to the apps theme
    if (hideTabs.count > 0) {
        NSMutableArray *tabItems = [NSMutableArray arrayWithArray:self.viewControllers];
        for (Class class in hideTabs) {
            int foundIndex = -1;

            for (int i = 0; i < tabItems.count; i++) {
                UIViewController *item = tabItems[i];
                if ([item isMemberOfClass:class]) {
                    foundIndex = i;
                    break;
                }
            }

            if (foundIndex >= 0) {
                [tabItems removeObjectAtIndex:foundIndex];
            }
        }
        self.viewControllers = tabItems;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Printers Screen";

    if (self.wasOpenedIn) {
        self.wasOpenedIn = NO;

        for (int i = 0; i < self.viewControllers.count; i++) {
            UIViewController *item = self.viewControllers[i];
            if ([item isMemberOfClass:[LocationPrintersViewController class]]) {
                [self setSelectedIndex:i];
                break;
            }
        }

        [SplashScreen hide];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_SELECT_PRINTER", nil);
}

- (void)setupTheme
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];
    }
    [BarButtonItem customizeRightBarButton:self.scanQRButton withImage:[[ImageManager sharedImageManager] imageNamed:@"QRWhiteIcon"]];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];

    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    // Set the TabBar appearance
    [self.tabBar setTintColor:[ThemeLoader colorForKey:@"PrintersScreen.Tabbar.SelectedImageColor"]];
    [self.tabBar setBarTintColor:[ThemeLoader colorForKey:@"PrintersScreen.Tabbar.BackgroundColor"]];

    NSShadow *shadow = [NSShadow new];
    shadow.shadowColor = [UIColor darkGrayColor];
    shadow.shadowOffset = CGSizeMake(0.5, 0.5);

    for (UITabBarItem *item in self.tabBar.items) {
        [item setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:11.0], NSForegroundColorAttributeName: [ThemeLoader colorForKey:@"PrintersScreen.Tabbar.SelectedTextColor"], NSShadowAttributeName: shadow} forState:UIControlStateSelected];
        [item setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:11.0], NSForegroundColorAttributeName: [ThemeLoader colorForKey:@"PrintersScreen.Tabbar.NormalTextColor"], NSShadowAttributeName: shadow} forState:UIControlStateNormal];
    }

    self.tabBar.layer.shadowOpacity = 0.75f;
    self.tabBar.layer.shadowRadius = 1.5f;
    self.tabBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabBar.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    // Compute the shadow paths after layout
    self.tabBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.tabBar.layer.bounds.origin.x - 3, self.tabBar.layer.bounds.origin.y, self.tabBar.layer.bounds.size.width + 6, self.tabBar.layer.bounds.size.height)].CGPath;

    self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;
}

- (IBAction)closePressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"scanQRCode"]) {
        QRScannerViewController *destViewController = segue.destinationViewController;
        destViewController.hideCloseButton = YES;
    }
}

@end
