//
//  SavedPrintersViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/15/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface SavedPrintersViewController : BaseViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
