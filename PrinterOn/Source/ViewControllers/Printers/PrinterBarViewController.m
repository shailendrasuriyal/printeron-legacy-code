//
//  PrinterBarViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-15.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrinterBarViewController.h"

#import "BBCyclingLabel.h"
#import "DocProcess.h"
#import "PreviewViewController.h"
#import "Printer.h"
#import "PrinterDetailsViewController.h"
#import "ServiceDescription.h"

#import <MMWormhole/MMWormhole.h>

@interface PrinterBarViewController ()

@property (nonatomic, strong) NSTimer *labelTimer;

@end

@implementation PrinterBarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Setup the Cycling Label
    self.labelCycling.font = [UIFont systemFontOfSize:12];
    self.labelCycling.textColor = [UIColor colorWithRed:120/255.0f green:120/255.0f blue:120/255.0f alpha:1.0f];
    self.labelCycling.numberOfLines = 1;
    self.labelCycling.textAlignment = NSTextAlignmentCenter;
    self.labelCycling.transitionEffect = BBCyclingLabelTransitionEffectCrossFade;
    self.labelCycling.transitionDuration = 1.25;

    // Hide QR scan button if no camera is present
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] || [ThemeLoader boolForKey:@"QRScanner.isHidden"]) {
        [self.buttonRight setHidden:YES];
    }

    // Register for changes to the printer
    [self registerPrinterNotification];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updatePrinter:NO onAppear:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.labelTimer) {
        [self.labelTimer invalidate];
        self.labelTimer = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    [self updatePrinter:YES onAppear:NO];
}

- (void)setupTheme
{
    [self.buttonRight setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterBar.QRImage"]] forState:UIControlStateNormal];

    // Add AccessibilityId
    [self.labelTitle setAccessibilityIdentifier:@"Printer Selected"];
    [self.buttonLeft setAccessibilityIdentifier:@"PrinterIcon"];
    [self.buttonRight setAccessibilityIdentifier:@"QRIcon"];
}

- (void)updatePrinter:(BOOL)initialSetup onAppear:(BOOL)appear
{
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    if (printer) {
        if (self.labelTimer) {
            [self.labelTimer invalidate];
            self.labelTimer = nil;
        }

        // If we have no PrinterInfoImage icon defined in the Theme use the PrinterImage instead
        NSString *imageInfo = [ThemeLoader stringForKey:@"PrinterBar.PrinterInfoImage"];
        if (imageInfo.length == 0) imageInfo = [ThemeLoader stringForKey:@"PrinterBar.PrinterImage"];

        [self.buttonLeft setImage:[[ImageManager sharedImageManager] imageNamed:imageInfo] forState:UIControlStateNormal];
        [self.buttonLeft setUserInteractionEnabled:YES];
        self.labelTitle.text = printer.displayName;
        [self.labelCycling setText:printer.organizationLocationDesc animated:NO];

        if (!initialSetup) {
            self.labelTimer = [NSTimer timerWithTimeInterval:6.5 target:self selector:@selector(updateCyclingLabel:) userInfo:printer repeats:NO];
            [[NSRunLoop mainRunLoop] addTimer:self.labelTimer forMode:NSRunLoopCommonModes];
        }

        if (!appear) {
            if (initialSetup && [self.parentViewController isMemberOfClass:PreviewViewController.class]) {
                PreviewViewController *pvc = (PreviewViewController *)self.parentViewController;
                if ([pvc.document isMemberOfClass:NSClassFromString(@"OtherDocument")]) {
                    return;
                }
            }

            // Check to see if we need to update the Service Capabilities for the selected printers service
            NSString *serviceURL = [DocProcess getBaseURLForService:[printer getDocAPIAddress].absoluteString];
            [ServiceDescription capabilitiesForServiceWithURL:[NSURL URLWithString:serviceURL] forceUpdate:NO completionBlock:nil];
        }
    } else {
        [self.buttonLeft setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterBar.PrinterImage"]] forState:UIControlStateNormal];
        [self.buttonLeft setUserInteractionEnabled:NO];
        self.labelTitle.text = NSLocalizedPONString(@"LABEL_NOPRINTER", nil);
        self.labelCycling.text = NSLocalizedPONString(@"LABEL_NOPRINTERTAP", nil);
    }
}

- (void)updateCyclingLabel:(NSTimer *)timer
{
    if (timer == nil) return;

    Printer *printer = timer.userInfo;

    if ([self.labelCycling.text isEqualToString:NSLocalizedPONString(@"LABEL_PRINTERCHANGE", nil)]) {
        self.labelCycling.text = printer.organizationLocationDesc;
        self.labelTimer = [NSTimer timerWithTimeInterval:6.5 target:self selector:@selector(updateCyclingLabel:) userInfo:printer repeats:NO];
    } else {
        self.labelCycling.text = NSLocalizedPONString(@"LABEL_PRINTERCHANGE", nil);
        self.labelTimer = [NSTimer timerWithTimeInterval:2.5 target:self selector:@selector(updateCyclingLabel:) userInfo:printer repeats:NO];
    }
    [[NSRunLoop mainRunLoop] addTimer:self.labelTimer forMode:NSRunLoopCommonModes];
}

- (void)registerPrinterNotification
{
    __weak PrinterBarViewController *weakSelf = self;
    [[[WormholeManager sharedWormholeManager] wormHole] listenForMessageWithIdentifier:@"PONPrinterChangedNotification" listener:^(id messageObject) {
        [weakSelf printerChangedNotification:nil];
    }];
}

- (void)printerChangedNotification:(NSNotification *)notification
{
    [self updatePrinter:NO onAppear:NO];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printerDetailsPrinterBar"]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            UINavigationController *navController = segue.destinationViewController;
            PrinterDetailsViewController *destViewController = [navController childViewControllers][0];
            destViewController.showClose = YES;
        }
        [Printer setSingletonPrinter:[Printer getSingletonPrinterForEntity:@"SelectedPrinter"] forEntity:@"DetailsPrinter"];
    }
}

@end
