//
//  JobDetailsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 1/14/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "JobDetailsViewController.h"

#import "AuthorizeJobViewController.h"
#import "BarButtonItem.h"
#import "CSLinearLayoutView.h"
#import "DocProcess.h"
#import "JCActivityBar.h"
#import "MainViewController.h"
#import "Media.h"
#import "OAuth2Manager.h"
#import "PaperSize.h"
#import "PreviewViewController.h"
#import "PrintDocument.h"
#import "Printer.h"
#import "PrinterDetailsViewController.h"
#import "PrintJob.h"
#import "PrintJobItem.h"
#import "PrintJobManager.h"
#import "ReleaseViewController.h"
#import "RemoteRelease.h"

@interface JobDetailsViewController () <PrintDocumentDelegate>

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (nonatomic, strong) CSLinearLayoutView *linearLayout;
@property (nonatomic, strong) CSLinearLayoutView *detailsLayout;

@property (nonatomic, strong) CSLinearLayoutItem *statusItem;
@property (nonatomic, strong) CSLinearLayoutItem *uploadItem;
@property (nonatomic, strong) CSLinearLayoutItem *progressItem;
@property (nonatomic, strong) CSLinearLayoutItem *errorItem;
@property (nonatomic, strong) CSLinearLayoutItem *showReleaseItem;
@property (nonatomic, strong) CSLinearLayoutItem *releaseItem;
@property (nonatomic, strong) CSLinearLayoutItem *releaseButtonItem;
@property (nonatomic, strong) CSLinearLayoutItem *authorizeItem;
@property (nonatomic, strong) CSLinearLayoutItem *settingsItem;
@property (nonatomic, strong) CSLinearLayoutItem *previewItem;
@property (nonatomic, strong) CSLinearLayoutItem *jobTypeItem;
@property (nonatomic, strong) CSLinearLayoutItem *pageCountItem;
@property (nonatomic, strong) CSLinearLayoutItem *field1Item;
@property (nonatomic, strong) CSLinearLayoutItem *field2Item;
@property (nonatomic, strong) CSLinearLayoutItem *fileNameItem;
@property (nonatomic, strong) CSLinearLayoutItem *referenceItem;
@property (nonatomic, strong) CSLinearLayoutItem *detailsItem;
@property (nonatomic, strong) CSLinearLayoutItem *printerItem;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) PaperSize *paperSize;
@property (nonatomic, strong) PrintDocument *document;
@property (nonatomic, strong) JCActivityBar *activityBar;
@property (nonatomic, assign) BOOL lockRelease;
@property (nonatomic, strong) NSProgress *progress;

@end

@implementation JobDetailsViewController

- (void)viewDidLoad
{
    // Initilaze the FRC with the PrintJob
    if (self.jobUUID) {
        [self fetchedResultsController];
        PrintJobItem *item = [[PrintJobManager sharedPrintJobManager] getJobItemForJobIdentifier:self.jobUUID];
        if (item.progress) {
            self.progress = item.progress;
            [self updateProgress:NO];
            [self.progress addObserver:self forKeyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew context:NULL];
        }
    }

    // Set paper Size for preview
    self.paperSize = [PaperSize getPaperSizeFromPrintOptions:self.printJob.printOptions];
    [self.previewView setFrame:[PaperSize fitPaperSize:self.paperSize toAreaOfSize:self.previewView.frame.size]];

    // Initialize release code view lock
    self.lockRelease = [TouchIDAuth isTouchIDAvailable];

    // Create the ActivityBar
    self.activityBar = [JCActivityBar new];
    [self.activityBar positionInBottomOfView:self.backgroundView withBottomOffset:24.0];
    [self.backgroundView addSubview:self.activityBar];

    [super viewDidLoad];

    self.dateFormatter = [NSDateFormatter new];
    self.dateFormatter.dateStyle = NSDateFormatterShortStyle;
    self.dateFormatter.timeStyle = NSDateFormatterShortStyle;

    [self loadPreview];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Reload/recreate the fetch controller
    if (self.jobUUID) {
        [self fetchedResultsController];
    }

    // Set the job details
    [self updateJobDetails];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Job Details Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    // Remove the fetch controller to save memory when the view disappears
    if (self.jobUUID) {
        _fetchedResultsController.delegate = nil;
        _fetchedResultsController = nil;
        self.printJob = nil;
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    if (self.progress) {
        [self.progress removeObserver:self forKeyPath:@"fractionCompleted" context:NULL];
    }
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_JOBDETAILS", nil);
    [self.showReleaseButton setTitle:NSLocalizedPONString(@"LABEL_UNLOCK_RELEASE", nil) forState:UIControlStateNormal];
    [self.releaseTitle setText:[NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_RELEASECODE", nil)]];
    [self.releaseButtonTitle setText:[NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_RELEASECODE", nil)]];
    [self.releaseButton setTitle:NSLocalizedPONString(@"LABEL_RELEASEJOB", nil) forState:UIControlStateNormal];
    [self.authorizeButton setTitle:NSLocalizedPONString(@"TITLE_AUTHJOB", nil) forState:UIControlStateNormal];
    [self.settingsButton setTitle:NSLocalizedPONString(@"LABEL_REAUTHENTICATE", nil) forState:UIControlStateNormal];
    [self.detailsJobTypeLabel setText:[NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_JOB", nil)]];
    [self.detailsPageCountLabel setText:[NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_PAGES", nil)]];
    [self.detailsReferenceLabel setText:[NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_REFERENCE", nil)]];
    [self.detailsFilenameLabel setText:[NSString stringWithFormat:@"%@:", NSLocalizedPONString(@"LABEL_FILENAME", nil)]];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"JobDetailsScreen.BackgroundColor"];
    [BarButtonItem customizeRightBarButton:self.reprintButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.PrintButton.Image"]]];

    [self.reprintButton setHidden:self.jobUUID == nil];

    [self.showReleaseButton setImage:[[ImageManager sharedImageManager] imageNamed:@"LockIcon"] forState:UIControlStateNormal];
    self.showReleaseButton.layer.cornerRadius = 4.0f;

    self.releaseButton.backgroundColor = [ThemeLoader colorForKey:@"JobDetailsScreen.ReleaseButton.BackgroundColor"];
    [self.releaseButton setTitleColor:[ThemeLoader colorForKey:@"JobDetailsScreen.ReleaseButton.TextColor"] forState:UIControlStateNormal];
    self.releaseButton.layer.cornerRadius = 4.0f;

    self.authorizeButton.layer.cornerRadius = 4.0f;
    self.settingsButton.layer.cornerRadius = 4.0f;

    // In iOS 7.0+ the Progress View height is much smaller and needs to have it's positioning adjusted
    [self.uploadProgress setFrame:CGRectMake(self.uploadProgress.frame.origin.x, 35, self.uploadProgress.frame.size.width, self.uploadProgress.frame.size.height)];

    [self.printerImage setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterBar.PrinterImage"]]];
    [self.printerInfoImage setImage:[[ImageManager sharedImageManager] imageNamed:@"InfoIcon"]];
}

- (void)loadPreview
{
    NSURL *documentURL = [NSURL URLWithString:[self.printJob getDocumentPath]];
    NSString *previewPath = [NSString stringWithFormat:@"%@/preview.jpg", [documentURL URLByDeletingLastPathComponent].path];
    if ([[NSFileManager defaultManager] fileExistsAtPath:previewPath]) {
        NSData *data = [NSData dataWithContentsOfFile:previewPath];
        [self.previewImage setImage:[UIImage imageWithData:data]];
        [self.previewLabel setHidden:YES];
    } else {
        self.document = [PrintDocument getDocumentFromFile:documentURL fromSource:nil];
        self.document.delegate = self;

        if ([self.document canBePreviewed] && ![self.document previewIsServerRendered]) {
            // If we can, generate a local preview and save it for future use
            UIImage *image = [self.document getPagePreviewAtIndex:0 withSize:self.previewImage.frame.size forPaperSize:self.paperSize.size];
            if (image) {
                [self.previewImage setImage:image];
                [self.previewLabel setHidden:YES];
                
                NSData *data = UIImageJPEGRepresentation(image, 0.85);
                [data writeToFile:previewPath atomically:YES];
                self.document = nil;
            }
        }

        [self.previewLabel setText:NSLocalizedPONString(@"LABEL_NOPREVIEWSHORT", nil)];
    }
}

- (IBAction)pressedReprint
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

    NSURL *fileURL = [NSURL URLWithString:[self.printJob getDocumentPath]];
    PrintDocument *document = [PrintDocument getDocumentFromFile:fileURL fromSource:nil];
    document.reprint = YES;
    document.reprintType = self.printJob.docType;
    document.reprintData = self.printJob.docData;
    document.reprintOptions = [NSMutableDictionary dictionaryWithDictionary:self.printJob.printOptions];

    [PreviewViewController validatePrintOptions:document.reprintOptions];

    [[PrintJobManager sharedPrintJobManager].mainView showPrintPreview:document];
}

- (IBAction)pressedUnlock
{
    NSString *dialogTitle = ([self.printJob.printer getRemoteReleaseCMDAddress] || ([self.printJob.printer getRemoteReleaseAPIAddress] && [self.printJob.printer.printerClass.lowercaseString isEqualToString:@"pull"])) ? NSLocalizedPONString(@"LABEL_UNLOCK_REMOTE", nil) : NSLocalizedPONString(@"LABEL_UNLOCK_CODE", nil);
    [TouchIDAuth authenticateTouchIDWithTitle:dialogTitle setDelegate:self];
}

- (IBAction)pressedRemoteRelease:(id)sender
{
    if ([self.printJob.printer.printerClass.lowercaseString isEqualToString:@"pull"]) {
        // Go to remote release UI to select child printer to release job from
        [self performSegueWithIdentifier:@"showReleaseUI" sender:nil];
    } else {
        // Display remote release confirmation dialog
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_CONFIRMATION", nil) message:NSLocalizedPONString(@"LABEL_RELEASECONFIRM", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_YES", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [self.releaseButton setEnabled:NO];
                [self.activityBar displayActivityWithMessage:NSLocalizedPONString(@"LABEL_RELEASING", nil)];

                NSString *url = [self.printJob.printer getRemoteReleaseCMDAddress];
                NSDictionary *parameters = [self getRemoteReleaseCMDParameters];

                __weak JobDetailsViewController *weakSelf = self;
                [RemoteRelease remoteReleaseCMD:[NSURL URLWithString:url] withParameters:parameters success:^(void) {
                    __strong JobDetailsViewController *strongSelf = weakSelf;
                    if (!strongSelf) return;

                    [strongSelf remoteReleaseSuccess];
                } failure:^(NSError *error) {
                    __strong JobDetailsViewController *strongSelf = weakSelf;
                    if (!strongSelf) return;

                    [strongSelf remoteReleaseFailure:error isAPI:NO];
                }];
            });
        }];
        [alertVC addAction:yesAction];
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_NO", nil) style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:noAction];

        [self presentViewController:alertVC animated:YES completion:nil];
    }
}

- (IBAction)pressedUserSettings
{
    NSString *serviceURL = [DocProcess getBaseURLForService:[self.printJob.printer getDocAPIAddress].absoluteString];
    if (serviceURL && serviceURL.length > 0) {
        [OAuth2Manager showUserSettingsForService:serviceURL overController:self.navigationController];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:self.progress] && [keyPath isEqualToString:@"fractionCompleted"]) {
        [self updateProgress:YES];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)updateProgress:(BOOL)animated
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.uploadProgress setProgress:self.progress.fractionCompleted animated:animated];
        [self.uploadPercent setText:[NSString stringWithFormat:@"%.0f%%", self.progress.fractionCompleted * 100]];
    });
}

- (void)updateJobDetails
{
    // Initialize Linear Layout
    if (!self.linearLayout) {
        self.linearLayout = [[CSLinearLayoutView alloc] initWithFrame:self.view.bounds];
        self.linearLayout.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.linearLayout.autoAdjustFrameSize = YES;
        self.linearLayout.orientation = CSLinearLayoutViewOrientationVertical;
    } else {
        [self.linearLayout removeAllItems];
    }

    // Add Status View
    long jobStatus = [self.printJob getJobStatusValue];
    if (jobStatus == JobStatusCodeSuccess || jobStatus == JobStatusCodeFailed || jobStatus == JobStatusCodeCancelled || jobStatus == JobStatusCodeHeld) {
        [self.statusImage setImage:[self.printJob getJobStatusImage]];
        [self.statusTitle setText:[self.printJob getJobStatusLabel]];
        [self.statusDateTime setText:self.printJob.dateTime ? [[self.dateFormatter stringFromDate:self.printJob.dateTime] lowercaseString] : @""];

        [self.progressView setHidden:YES];
        [self.uploadView setHidden:YES];
        self.statusItem = [self setupItem:self.statusItem withView:self.statusView withPadding:CSLinearLayoutMakePadding(10, 8, 12, 9)];
        [self.linearLayout addItem:self.statusItem];
    } else if (jobStatus == JobStatusCodeCreated || jobStatus == JobStatusCodeUpload) {
        [self.uploadTitle setText:[self.printJob getJobStatusLabel]];

        [self.progressView setHidden:YES];
        [self.statusView setHidden:YES];
        self.uploadItem = [self setupItem:self.uploadItem withView:self.uploadView withPadding:CSLinearLayoutMakePadding(10, 8, 12, 9)];
        [self.linearLayout addItem:self.uploadItem];
    } else if (jobStatus == JobStatusCodePending || jobStatus == JobStatusCodeUploadExtension) {
        [self.progressTitle setText:[self.printJob getJobStatusLabel]];

        [self.uploadView setHidden:YES];
        [self.statusView setHidden:YES];
        self.progressItem = [self setupItem:self.progressItem withView:self.progressView withPadding:CSLinearLayoutMakePadding(10, 8, 12, 9)];
        [self.linearLayout addItem:self.progressItem];
    }

    // Add Error View if necessary
    if (jobStatus == JobStatusCodeCancelled || jobStatus == JobStatusCodeFailed || jobStatus == JobStatusCodeHeld) {
        NSString *errorText = (jobStatus == JobStatusCodeHeld) ? NSLocalizedPONString(@"LABEL_REQUIRESAUTH", nil) : [self.printJob getJobErrorText];

        if ([errorText length] > 0) {
            [self resizeLabel:self.errorText withText:errorText inView:self.errorView addHeight:9];

            self.errorItem = [self setupItem:self.errorItem withView:self.errorView withPadding:CSLinearLayoutMakePadding(0, 8, (jobStatus == JobStatusCodeHeld) ? 0 : 12, 9)];
            if (jobStatus != JobStatusCodeHeld) self.errorItem.padding = CSLinearLayoutMakePadding(0, 8, 12, 9);
            [self.linearLayout addItem:self.errorItem];
        } else {
            [self.errorView setHidden:YES];
        }
    } else {
        [self.errorView setHidden:YES];
    }

    // Add Release Code View if necessary
    if (jobStatus == JobStatusCodeSuccess && [self.printJob.releaseCode length] > 0) {
        if (self.lockRelease) {
            [self.releaseView setHidden:YES];
            [self.releaseButtonView setHidden:YES];

            self.showReleaseItem = [self setupItem:self.showReleaseItem withView:self.showReleaseView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
            [self.linearLayout addItem:self.showReleaseItem];
        } else {
            [self.showReleaseView setHidden:YES];

            if ([self.printJob.printer getRemoteReleaseCMDAddress] || ([self.printJob.printer getRemoteReleaseAPIAddress] && [self.printJob.printer.printerClass.lowercaseString isEqualToString:@"pull"])) {
                [self.releaseButtonCode setText:self.printJob.releaseCode];

                [self.releaseView setHidden:YES];
                self.releaseButtonItem = [self setupItem:self.releaseButtonItem withView:self.releaseButtonView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
                [self.linearLayout addItem:self.releaseButtonItem];
            } else {
                [self.releaseCode setText:self.printJob.releaseCode];

                [self.releaseButtonView setHidden:YES];
                self.releaseItem = [self setupItem:self.releaseItem withView:self.releaseView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
                [self.linearLayout addItem:self.releaseItem];
            }
        }
    } else {
        [self.showReleaseView setHidden:YES];
        [self.releaseView setHidden:YES];
        [self.releaseButtonView setHidden:YES];
    }

    // Add Authorize View if necessary
    if (jobStatus == JobStatusCodeHeld && [self.printJob.jobAuthURI length] > 0) {
        self.authorizeItem = [self setupItem:self.authorizeItem withView:self.authorizeView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.authorizeItem];
    } else {
        [self.authorizeView setHidden:YES];
    }

    // Add User Settings View if necessary
    if (jobStatus == JobStatusCodeFailed && [[self.printJob getJobErrorText] isEqualToString:NSLocalizedPONString(@"ERROR_CHECK_AUTHENTICATION", nil)]) {
        self.settingsItem = [self setupItem:self.settingsItem withView:self.settingsView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.settingsItem];
    } else {
        [self.settingsView setHidden:YES];
    }

    // Add Preview View
    self.previewItem = [self setupItem:self.previewItem withView:self.previewView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
    self.previewItem.verticalAlignment = CSLinearLayoutItemVerticalAlignmentCenter;
    [self.linearLayout addItem:self.previewItem];

    // Initialize Details Layout
    if (!self.detailsLayout) {
        self.detailsLayout = [[CSLinearLayoutView alloc] initWithFrame:self.detailsView.bounds];
        self.detailsLayout.autoAdjustFrameSize = YES;
        self.detailsLayout.orientation = CSLinearLayoutViewOrientationVertical;
    } else {
        [self.detailsLayout removeAllItems];
    }

    // Add Job Type details
    [self.detailsJobTypeImage setImage:[self.printJob getJobTypeImage]];
    [self.detailsJobTypeValue setText:[self.printJob getJobTypeLabel]];
    self.jobTypeItem = [self setupItem:self.jobTypeItem withView:self.detailsJobTypeView withPadding:CSLinearLayoutMakePadding(8, 7, 0, 8)];
    [self.detailsLayout addItem:self.jobTypeItem];

    // Add Page Count details
    if (jobStatus == JobStatusCodeSuccess) {
        [self.detailsPageCountImage setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.Pages.Image"]]];
        [self.detailsPageCountValue setText:[NSString stringWithFormat:@"%ld", (long)[self.printJob getJobPageCountValue]]];
        self.pageCountItem = [self setupItem:self.pageCountItem withView:self.detailsPageCountView withPadding:CSLinearLayoutMakePadding(2, 8, 0, 8)];
        [self.detailsLayout addItem:self.pageCountItem];
    } else {
        [self.detailsPageCountView setHidden:YES];
    }

    // Add Field 1 details
    NSString *value1 = [self.printJob getDocumentDataValue:1];
    if (value1 && [value1 length] > 0) {
        [self setupItem:self.field1Item withView:self.detailsField1View withPadding:CSLinearLayoutMakePadding(2, 8, 0, 8)];
        [self resizeLabel:self.detailsField1Value withText:value1 inView:self.detailsField1View];
        [self.detailsField1Label setText:[self.printJob getDocumentDataLabel:1]];
        self.field1Item = [self setupItem:self.field1Item withView:self.detailsField1View withPadding:CSLinearLayoutMakePadding(2, 8, 0, 8)];
        [self.detailsLayout addItem:self.field1Item];
    } else {
        [self.detailsField1View setHidden:YES];
    }

    // Add Field 2 details
    NSString *value2 = [self.printJob getDocumentDataValue:2];
    if (value2 && [value2 length] > 0) {
        [self resizeLabel:self.detailsField2Value withText:value2 inView:self.detailsField2View];
        [self.detailsField2Label setText:[self.printJob getDocumentDataLabel:2]];
        self.field2Item = [self setupItem:self.field2Item withView:self.detailsField2View withPadding:CSLinearLayoutMakePadding(2, 8, 0, 8)];
        [self.detailsLayout addItem:self.field2Item];
    } else {
        [self.detailsField2View setHidden:YES];
    }

    // Add Job Filename details
    NSString *fileName = [[self.printJob.documentURI lastPathComponent] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if ([fileName length] > 0) {
        [self resizeLabel:self.detailsFilenameValue withText:fileName inView:self.detailsFilenameView];
        [self.detailsFilenameValue setText:fileName];
        self.fileNameItem = [self setupItem:self.fileNameItem withView:self.detailsFilenameView withPadding:CSLinearLayoutMakePadding(2, 8, 0, 8)];
        [self.detailsLayout addItem:self.fileNameItem];
    } else {
        [self.detailsFilenameView setHidden:YES];
    }

    // Add Job Reference details
    if ([self.printJob.referenceID length] > 0) {
        [self.detailsReferenceValue setText:self.printJob.referenceID];
        self.referenceItem = [self setupItem:self.referenceItem withView:self.detailsReferenceView withPadding:CSLinearLayoutMakePadding(2, 8, 0, 8)];
        [self.detailsLayout addItem:self.referenceItem];
    } else {
        [self.detailsReferenceView setHidden:YES];
    }

    [self.detailsView setFrame:CGRectMake(self.detailsLayout.frame.origin.x, self.detailsLayout.frame.origin.y, self.detailsLayout.frame.size.width, self.detailsLayout.frame.size.height + 8)];
    [self.detailsView addSubview:self.detailsLayout];

    self.detailsItem = [self setupItem:self.detailsItem withView:self.detailsView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
    [self.linearLayout addItem:self.detailsItem];

    // Add Printer View
    [self.printerTitle setText:self.printJob.printer.displayName];
    [self.printerSubtitle setText:self.printJob.printer.organizationLocationDesc];
    self.printerItem = [self setupItem:self.printerItem withView:self.printerView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
    [self.linearLayout addItem:self.printerItem];
    
    // Add linear layout to the scrollview
    [self.scrollView addSubview:self.linearLayout];

    [self updateContentHeight];
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text inView:(UIView *)view
{
    [self resizeLabel:label withText:text inView:view addHeight:0];
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text inView:(UIView *)view addHeight:(NSUInteger)add
{
    // Resize the label to hold the text
    [label setText:text];
    CGSize maxSize = CGSizeMake(label.frame.size.width, 2000);
    CGSize requiredSize = [label sizeThatFits:maxSize];

    if (requiredSize.height > label.frame.size.height) {
        [label setFrame:CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, requiredSize.height)];

        // Resize the view the label is inside to fit the new size
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, label.frame.origin.y + label.frame.size.height + add)];
    }
}

- (void)updateContentHeight
{
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.linearLayout.frame.size.height);
}

- (CSLinearLayoutItem *)setupItem:(CSLinearLayoutItem *)item withView:(UIView *)view withPadding:(CSLinearLayoutItemPadding)padding
{
    if (!item) {
        item = [CSLinearLayoutItem layoutItemForView:view];
        item.padding = padding;
        item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    }
    [view setHidden:NO];
    return item;
}

- (void)remoteReleaseAPIFromPrinter:(NSString *)printerID
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.releaseButton setEnabled:NO];
        [self.activityBar displayActivityWithMessage:NSLocalizedPONString(@"LABEL_RELEASING", nil)];
        
        NSString *url = [self.printJob.printer getRemoteReleaseAPIAddress];
        NSDictionary *parameters = [self getRemoteReleaseAPIParameters:printerID];
        
        __weak JobDetailsViewController *weakSelf = self;
        [RemoteRelease remoteReleaseAPI:[NSURL URLWithString:url] withParameters:parameters success:^(void) {
            __strong JobDetailsViewController *strongSelf = weakSelf;
            if (!strongSelf) return;
            
            [strongSelf remoteReleaseSuccess];
        } failure:^(NSError *error) {
            __strong JobDetailsViewController *strongSelf = weakSelf;
            if (!strongSelf) return;
            
            [strongSelf remoteReleaseFailure:error isAPI:YES];
        }];
    });
}

- (NSDictionary *)getRemoteReleaseCMDParameters
{
    Printer *printer = self.printJob.printer;
    
    return @{
             @"aliasNum":printer.aliasNumeric ?: @"",
             @"PRINTSPOT":printer.serviceID ?: @"",
             @"lang":NSLocalizedPONString(@"USER_LANG", nil),
             @"isReleased":@"true",
             @"releaseCode":self.printJob.releaseCode ?: @"",
             };
}

- (NSDictionary *)getRemoteReleaseAPIParameters:(NSString *)printerID
{
    return @{
             @"printerNum":printerID ?: @"",
             @"releaseCode":self.printJob.releaseCode ?: @""
             };
}

- (void)remoteReleaseSuccess
{
    [self.activityBar finishWithSuccess:NSLocalizedPONString(@"LABEL_SUCCESS_SETTINGS", nil)];
    [self.releaseButton setEnabled:YES];
}

- (void)remoteReleaseFailure:(NSError *)error isAPI:(BOOL)api
{
    NSInteger statusCode = 0;
    if (error.userInfo && [error.userInfo[@"AFNetworkingOperationFailingURLResponseErrorKey"] isMemberOfClass:[NSHTTPURLResponse class]]) {
        NSHTTPURLResponse *response = error.userInfo[@"AFNetworkingOperationFailingURLResponseErrorKey"];
        statusCode = response.statusCode;
    }

    NSString *errorString = nil;
    if (api) {
        errorString = (statusCode == 403) ? NSLocalizedPONString(@"ERROR_RELEASE_NOTENABLED", nil) : (statusCode > 0) ? [NSString stringWithFormat:NSLocalizedPONString(@"ERROR_RELEASE_STATUS", nil), statusCode] : NSLocalizedPONString(@"ERROR_RELEASE", nil);
    } else {
        errorString = (statusCode > 0) ? [NSString stringWithFormat:NSLocalizedPONString(@"ERROR_RELEASE_STATUS", nil), statusCode] : NSLocalizedPONString(@"ERROR_RELEASE", nil);
    }

    [self.activityBar finishWithError:errorString];
    [self.releaseButton setEnabled:YES];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"PrintJob"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"jobUUID == '%@'", self.jobUUID]];
        [fetchRequest setPredicate:predicate];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:NO]];

        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;

        [self performFetch];
    }

    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);

    NSArray *array = [self.fetchedResultsController fetchedObjects];
    if ([array count] > 0) {
        self.printJob = array[0];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self updateJobDetails];
}

#pragma mark - TouchIDAuthDelegate

- (void)didFinishAuthWithSuccess
{
    self.lockRelease = NO;
    [self updateJobDetails];
}

#pragma mark - PrintDocumentDelegate

- (void)setItemImage:(UIImage *)image atIndex:(NSInteger)index
{
    if (image) {
        [self.previewImage setImage:image];
        [self.previewLabel setHidden:YES];

        NSData *data = UIImageJPEGRepresentation(image, 0.85);

        NSURL *documentURL = [NSURL URLWithString:[self.printJob getDocumentPath]];
        NSString *previewPath = [NSString stringWithFormat:@"%@/preview.jpg", [documentURL URLByDeletingLastPathComponent].path];
        [data writeToFile:previewPath atomically:YES];
    }
    self.document = nil;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printerDetailsJob"]) {
        [Printer setSingletonPrinter:self.printJob.printer forEntity:@"DetailsPrinter"];
    } else if ([segue.identifier isEqualToString:@"launchAuthorize"]) {
        AuthorizeJobViewController *destViewController = segue.destinationViewController;
        destViewController.requestURL = [NSURL URLWithString:self.printJob.jobAuthURI];
        destViewController.jobUUID = self.printJob.jobUUID;
    } else if ([segue.identifier isEqualToString:@"reprintPreview"]) {
        PreviewViewController *destViewController = segue.destinationViewController;
        NSURL *fileURL = [NSURL URLWithString:[self.printJob getDocumentPath]];
        destViewController.document = [PrintDocument getDocumentFromFile:fileURL fromSource:nil];
        destViewController.document.reprint = YES;
        destViewController.document.reprintType = self.printJob.docType;
        destViewController.document.reprintData = self.printJob.docData;
        destViewController.document.reprintOptions = [NSMutableDictionary dictionaryWithDictionary:self.printJob.printOptions];
        [PreviewViewController validatePrintOptions:destViewController.document.reprintOptions];
    } else if ([segue.identifier isEqualToString:@"showReleaseUI"]) {
        ReleaseViewController *destViewController = segue.destinationViewController;
        destViewController.parentNum = self.printJob.printer.printerID;
        destViewController.parentSearchURL = self.printJob.printer.searchURL;
    }
}

@end
