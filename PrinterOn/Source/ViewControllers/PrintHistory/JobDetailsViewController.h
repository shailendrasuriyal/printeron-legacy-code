//
//  JobDetailsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 1/14/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "TouchIDAuth.h"

@class PrintJob;

@interface JobDetailsViewController : BaseViewController <NSFetchedResultsControllerDelegate, TouchIDAuthDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *statusView;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;
@property (weak, nonatomic) IBOutlet UILabel *statusTitle;
@property (weak, nonatomic) IBOutlet UILabel *statusDateTime;

@property (weak, nonatomic) IBOutlet ShadowView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *progressTitle;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressActivity;

@property (weak, nonatomic) IBOutlet ShadowView *uploadView;
@property (weak, nonatomic) IBOutlet UILabel *uploadTitle;
@property (weak, nonatomic) IBOutlet UILabel *uploadPercent;
@property (weak, nonatomic) IBOutlet UIProgressView *uploadProgress;

@property (weak, nonatomic) IBOutlet ShadowView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *errorText;

@property (weak, nonatomic) IBOutlet ShadowView *showReleaseView;
@property (weak, nonatomic) IBOutlet UIButton *showReleaseButton;

@property (weak, nonatomic) IBOutlet ShadowView *releaseView;
@property (weak, nonatomic) IBOutlet UILabel *releaseTitle;
@property (weak, nonatomic) IBOutlet UILabel *releaseCode;

@property (weak, nonatomic) IBOutlet ShadowView *releaseButtonView;
@property (weak, nonatomic) IBOutlet UILabel *releaseButtonTitle;
@property (weak, nonatomic) IBOutlet UILabel *releaseButtonCode;
@property (weak, nonatomic) IBOutlet UIButton *releaseButton;

@property (weak, nonatomic) IBOutlet ShadowView *previewView;
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property (weak, nonatomic) IBOutlet UILabel *previewLabel;

@property (weak, nonatomic) IBOutlet ShadowView *detailsView;
@property (weak, nonatomic) IBOutlet UIView *detailsJobTypeView;
@property (weak, nonatomic) IBOutlet UILabel *detailsJobTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *detailsJobTypeImage;
@property (weak, nonatomic) IBOutlet UILabel *detailsJobTypeValue;
@property (weak, nonatomic) IBOutlet UIView *detailsPageCountView;
@property (weak, nonatomic) IBOutlet UILabel *detailsPageCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *detailsPageCountImage;
@property (weak, nonatomic) IBOutlet UILabel *detailsPageCountValue;
@property (weak, nonatomic) IBOutlet UIView *detailsReferenceView;
@property (weak, nonatomic) IBOutlet UILabel *detailsReferenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsReferenceValue;
@property (weak, nonatomic) IBOutlet UIView *detailsField1View;
@property (weak, nonatomic) IBOutlet UILabel *detailsField1Label;
@property (weak, nonatomic) IBOutlet UILabel *detailsField1Value;
@property (weak, nonatomic) IBOutlet UIView *detailsField2View;
@property (weak, nonatomic) IBOutlet UILabel *detailsField2Label;
@property (weak, nonatomic) IBOutlet UILabel *detailsField2Value;
@property (weak, nonatomic) IBOutlet UIView *detailsFilenameView;
@property (weak, nonatomic) IBOutlet UILabel *detailsFilenameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsFilenameValue;

@property (weak, nonatomic) IBOutlet ShadowView *printerView;
@property (weak, nonatomic) IBOutlet UIImageView *printerImage;
@property (weak, nonatomic) IBOutlet UILabel *printerTitle;
@property (weak, nonatomic) IBOutlet UILabel *printerSubtitle;
@property (weak, nonatomic) IBOutlet UIImageView *printerInfoImage;

@property (weak, nonatomic) IBOutlet ShadowView *authorizeView;
@property (weak, nonatomic) IBOutlet UIButton *authorizeButton;

@property (weak, nonatomic) IBOutlet ShadowView *settingsView;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;

@property (weak, nonatomic) IBOutlet UIButton *reprintButton;

@property (nonatomic, strong) PrintJob *printJob;
@property (nonatomic, strong) NSString *jobUUID;

- (void)remoteReleaseAPIFromPrinter:(NSString *)printerID;

@end
