//
//  PrintHistoryViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 12/12/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrintHistoryViewController.h"

#import "ALAlertBanner.h"
#import "BarButtonItem.h"
#import "JobDetailsViewController.h"
#import "JobsList.h"
#import "OAuth2Manager.h"
#import "PrintJob.h"
#import "PrintJobCell.h"
#import "PrintJobFetched.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "UserAccount.h"

#import "MJRefresh.h"

@interface PrintHistoryViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation PrintHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.dateFormatter = [NSDateFormatter new];
    self.dateFormatter.dateStyle = NSDateFormatterShortStyle;
    self.dateFormatter.timeStyle = NSDateFormatterShortStyle;

    // Clear all notifications.  We do this because we are displaying a list of all print jobs and
    // all of the status for each job on this screen so there is no need to keep them.
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [ALAlertBanner hideAllAlertBanners];

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];

    Service *service = [Service getDefaultService];
    NSString *serviceURL = service.serviceURL;
    BOOL shouldFetch = [self shouldFetchJobsForService:serviceURL];

    __weak __typeof(self)weakSelf = self;
    MJRefreshNormalHeader *jobsHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^ {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        if (shouldFetch) {
            [strongSelf fetchJobsForService:serviceURL];
        } else {
            [strongSelf.scrollView.mj_header endRefreshing];
        }
    }];
    jobsHeader.lastUpdatedTimeLabel.hidden = YES;
    jobsHeader.stateLabel.hidden = YES;
    self.scrollView.mj_header = jobsHeader;

    if (shouldFetch) {
        [self.scrollView.mj_header beginRefreshing];
    }

    [self registerForEnterForegroundNotification];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Print History Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)customEnterForeground
{
    // Refresh the fetch controller and table
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    [self updateTableView];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PRINTHISTORY", nil);
}

- (void)setupTheme
{
    [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];
    [BarButtonItem customizeRightBarButton:self.deleteButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.DeleteButton.Image"]]];

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"HistoryScreen.BackgroundColor"];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    [self updateContentHeight];

    // Compute the shadow paths after layout
    self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pressedDeleteJobs
{
    // Display confirmation dialog
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedPONString(@"LABEL_DELETEALL_CONFIRM", nil) preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_YES", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

        dispatch_async(dispatch_get_main_queue(), ^{
            [self deleteAllJobs];
        });
    }];
    [alertVC addAction:yesAction];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_NO", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:noAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)deleteAllJobs
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"PrintJob"];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID

    NSManagedObjectContext *context = [[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:nil];
    for (NSManagedObject *object in fetchedObjects) {
        [context deleteObject:object];
    }

    [context saveToPersistentStore:nil];
}

- (BOOL)shouldFetchJobsForService:(NSString *)serviceURL
{
    if ([ServiceCapabilities getJobsURLForService:serviceURL] == nil) {
        return NO;
    }

    UserAccount *account = [UserAccount getUserAccountForURL:[NSURL URLWithString:serviceURL]];
    if (account && account.isAnonymous.boolValue) {
        return NO;
    }

    return YES;
}

- (void)fetchJobsForService:(NSString *)serviceURL
{
    UserAccount *account = [UserAccount getUserAccountForURL:[NSURL URLWithString:serviceURL]];

    __block NSMutableIndexSet *idSet = [PrintJobFetched getJobIDsForService:serviceURL forUser:account];
    if (idSet == nil) {
        idSet = [NSMutableIndexSet indexSetWithIndex:0];
    }

    __weak __typeof(self)weakSelf = self;
    [JobsList jobsForServiceURL:[NSURL URLWithString:serviceURL] startID:[NSString stringWithFormat:@"%tu", idSet.lastIndex] endID:nil maxResults:nil completionBlock:^(NSError *error, NSIndexSet *addedIDs) {

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            if (addedIDs) {
                [idSet addIndexes:addedIDs];
                [PrintJobFetched updateJobIDsForService:serviceURL forUser:account withIDs:idSet];
            }

            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            dispatch_async(dispatch_get_main_queue(), ^{
                if (error && [OAuth2Manager isAuthSettingsError:error]) {
                    [OAuth2Manager showAuthSettingsError:error overController:strongSelf.navigationController];
                }

                [strongSelf.scrollView.mj_header endRefreshing];
            });
        });
    }];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"PrintJob"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:NO]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;
        
        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
    [self.deleteButton setHidden:[self.fetchedResultsController.fetchedObjects count] <= 0];
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self updateContentHeight];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    //Lets the tableview know we're potentially doing a bunch of updates.
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    //We're finished updating the tableview's data.
    [self.tableView endUpdates];
    self.tableView.contentSize = [self.tableView sizeThatFits:CGSizeMake(CGRectGetWidth(self.tableView.bounds), CGFLOAT_MAX)];
    [self updateContentHeight];
    [self.deleteButton setHidden:[self.fetchedResultsController.fetchedObjects count] <= 0];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    if (type == NSFetchedResultsChangeMove && [indexPath isEqual:newIndexPath]) {
        type = NSFetchedResultsChangeUpdate;
    }

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;

        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;

        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {

        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"printJobCell";
    PrintJobCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    PrintJob *job = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCell:job withFormatter:self.dateFormatter];

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        PrintJob *printJob = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if (printJob) {
            [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext deleteObject:printJob];
            [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];
        }
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showJobDetails"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

        id object = [self.fetchedResultsController objectAtIndexPath:indexPath];
        if ([object isKindOfClass:[PrintJob class]]) {
            PrintJob *job = (PrintJob *)object;
            JobDetailsViewController *destViewController = segue.destinationViewController;
            if (job.jobUUID == nil) {
                destViewController.printJob = job;
            } else {
                destViewController.jobUUID = job.jobUUID;
            }
        }
    }
}

@end
