//
//  AuthorizeJobViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2/13/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "AuthorizeJobViewController.h"

#import "NSString+Parameters.h"
#import "NSString+URL.h"
#import "PrintJobManager.h"

@interface AuthorizeJobViewController ()

@end

@implementation AuthorizeJobViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.requestURL];
    [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:request];
    [self.webView loadRequest:request];
}

- (void)dealloc
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zoomOutWebView) name:UIKeyboardDidHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Authorize Job Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_AUTHJOB", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"WebScreen.BackgroundColor"];
}

- (void)zoomOutWebView
{
    UIScrollView *sv = [self.webView subviews][0];
    [sv zoomToRect:CGRectMake(0, 0, sv.contentSize.width, sv.contentSize.height) animated:YES];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([NSURLProtocol propertyForKey:@"PONBrowserRequest" inRequest:request] == nil) {
        if ([request isMemberOfClass:[NSMutableURLRequest class]]) {
            [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:(NSMutableURLRequest *)request];
        }
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.webView setHidden:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    // If the page is wider then the webview we scale it to fit
    // Can't use scalesToFit = YES of the webview, for some reason it screws the scaling up
    if (webView.scrollView.contentSize.width > webView.frame.size.width) {
        NSString *js = [NSString stringWithFormat:@"var meta = document.createElement('meta'); "  \
                        "meta.setAttribute( 'name', 'viewport' ); "  \
                        "meta.setAttribute( 'content', 'width = device-width, initial-scale = %f, minimum-scale=0.1, maximum-scale=5.0, user-scalable = yes' ); "  \
                        "document.getElementsByTagName('head')[0].appendChild(meta)", webView.frame.size.width / webView.scrollView.contentSize.width];
        [webView stringByEvaluatingJavaScriptFromString:js];
    }

    NSDictionary *parameters = [[[webView.request.URL query] URLDecode] parametersSeparatedByInnerString:@"=" andOuterString:@"&" lowerCaseKeys:YES];
    NSString *approveUse = parameters[@"approveuse"];
    if ([approveUse length] > 0 && [[approveUse lowercaseString] isEqualToString:@"true"]) {
        [[PrintJobManager sharedPrintJobManager] authorizedPrintJob:self.jobUUID];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    // Hide the webview if it is empty so you don't have scrollbars showing for blank content
    if (webView.request.URL.absoluteString.length == 0) {
        [webView setHidden:YES];
    }

    // NSURLErrorDomain code -999 means another request was made before the previous completed
    if ([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
        return;
    }

    // Display the error message
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

@end
