//
//  AuthorizeJobViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2/13/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface AuthorizeJobViewController : BaseViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, strong) NSURL *requestURL;
@property (nonatomic, strong) NSString *jobUUID;

@end
