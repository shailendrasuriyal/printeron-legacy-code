//
//  ReleaseViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-01-06.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@interface ReleaseViewController : BaseTabBarController

@property (weak, nonatomic) IBOutlet UIButton *scanQRButton;

@property (strong, nonatomic) NSString *parentNum;
@property (strong, nonatomic) NSString *parentSearchURL;

@end
