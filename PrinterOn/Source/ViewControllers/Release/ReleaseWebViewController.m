//
//  ReleaseWebViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2/5/2016.
//  Copyright (c) 2016 PrinterOn Inc. All rights reserved.
//

#import "ReleaseWebViewController.h"

#import "JobAuthViewController.h"
#import "ServiceCapabilities.h"
#import "TouchIDAuth.h"
#import "UserAccount.h"
#import "UserSetupViewController.h"

#import "AFNetworking.h"

@interface ReleaseWebViewController () <TouchIDAuthDelegate>

@property (nonatomic, assign) BOOL isThirdParty;

@end

@implementation ReleaseWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.isThirdParty = [ServiceCapabilities serviceSupportsOAuthThirdParty:self.serviceURL];
    UserAccount *userAccount = [UserAccount getUserAccountForURL:self.requestURL];

    NSMutableURLRequest *request = nil;
    if (userAccount && ![userAccount.userName isEqualToString:[UserAccount anonymousUser].userName] && !self.isThirdParty) {
        if ([TouchIDAuth isTouchIDAvailable]) {
            [TouchIDAuth authenticateTouchIDWithTitle:NSLocalizedPONString(@"LABEL_UNLOCK_RELEASE", nil) setDelegate:self];
            return;
        } else {
            request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:self.requestURL.absoluteString parameters:@{ @"action": @"username", @"username": userAccount.userName, @"password": [userAccount getUserAccountPassword] ?: @"" } error:nil];
        }
    } else {
        request = [NSMutableURLRequest requestWithURL:self.requestURL];
    }

    [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:request];
    [self.webView loadRequest:request];
}

- (void)dealloc
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zoomOutWebView) name:UIKeyboardDidHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Release Web UI Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_RELEASE_JOB", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"WebScreen.BackgroundColor"];
}

- (void)zoomOutWebView
{
    UIScrollView *sv = [self.webView subviews][0];
    [sv zoomToRect:CGRectMake(0, 0, sv.contentSize.width, sv.contentSize.height) animated:YES];
}

#pragma mark - TouchIDAuthDelegate

- (void)didFinishAuthWithSuccess
{
    UserAccount *userAccount = [UserAccount getUserAccountForURL:self.requestURL];

    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:self.requestURL.absoluteString parameters:@{ @"action": @"username", @"username": userAccount.userName, @"password": [userAccount getUserAccountPassword] ?: @"" } error:nil];
    [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:request];
    [self.webView loadRequest:request];
}

- (void)didFinishAuthWithError
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.requestURL];
    [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:request];
    [self.webView loadRequest:request];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([NSURLProtocol propertyForKey:@"PONBrowserRequest" inRequest:request] == nil) {
        if ([request isMemberOfClass:[NSMutableURLRequest class]]) {
            [NSURLProtocol setProperty:@YES forKey:@"PONBrowserRequest" inRequest:(NSMutableURLRequest *)request];
        }
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    NSCachedURLResponse *cached = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
    NSHTTPURLResponse *response = (NSHTTPURLResponse *)cached.response;
    if (response && !self.isThirdParty && [response.URL.absoluteString.lowercaseString rangeOfString:@"invalidcredentials=true"].location != NSNotFound) {
        [webView setHidden:YES];

        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"ERROR_INVALID_CREDENTIALS", nil) message:NSLocalizedPONString(@"ERROR_CHECK_USERNAME_PASSWORD", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"TITLE_SETTINGS", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                NSMutableArray *vcs = [NSMutableArray array];
                for (UIViewController *vc in self.navigationController.viewControllers) {
                    if ([vc isMemberOfClass:[ReleaseWebViewController class]]) {
                        UserAccount *account = [UserAccount getUserAccountForURL:self.requestURL];
                        if ([account.userName isEqualToString:[UserAccount anonymousUser].userName]) {
                            [self performSegueWithIdentifier:@"showJobAuth" sender:nil];
                        } else {
                            [self performSegueWithIdentifier:@"showUserSetup" sender:nil];
                        }
                        [vcs addObject:self.navigationController.viewControllers.lastObject];
                        break;
                    }
                    [vcs addObject:vc];
                }
                [self.navigationController setViewControllers:vcs animated:NO];
            });
        }];
        [alertVC addAction:settingsAction];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }];
        [alertVC addAction:cancelAction];

        [self presentViewController:alertVC animated:YES completion:nil];
        return;
    }

    // If the page is wider then the webview we scale it to fit
    // Can't use scalesToFit = YES of the webview, for some reason it screws the scaling up
    if (webView.scrollView.contentSize.width > webView.frame.size.width) {
        NSString *js = [NSString stringWithFormat:@"var meta = document.createElement('meta'); "  \
                        "meta.setAttribute( 'name', 'viewport' ); "  \
                        "meta.setAttribute( 'content', 'width = device-width, initial-scale = %f, minimum-scale=0.1, maximum-scale=5.0, user-scalable = yes' ); "  \
                        "document.getElementsByTagName('head')[0].appendChild(meta)", webView.frame.size.width / webView.scrollView.contentSize.width];
        [webView stringByEvaluatingJavaScriptFromString:js];
    }

    [webView setHidden:NO];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    // Hide the webview if it is empty so you don't have scrollbars showing for blank content
    if (webView.request.URL.absoluteString.length == 0) {
        [webView setHidden:YES];
    }

    // NSURLErrorDomain code -999 means another request was made before the previous completed
    if ([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
        return;
    }

    // Display the error message
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showUserSetup"]) {
        UserSetupViewController *destViewController = segue.destinationViewController;
        destViewController.userAccount = [UserAccount getUserAccountForURL:self.requestURL];
        destViewController.hideServices = YES;
    } else if ([segue.identifier isEqualToString:@"showJobAuth"]) {
        JobAuthViewController *destViewController = segue.destinationViewController;
        destViewController.delegate = self.delegate;
    }
}

@end
