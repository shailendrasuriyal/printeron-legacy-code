//
//  ReleaseInfoViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-01-10.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "ReleaseInfoViewController.h"

@interface ReleaseInfoViewController ()

@end

@implementation ReleaseInfoViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setTitle:NSLocalizedPONString(@"TITLE_INFO", nil)];
        [[self tabBarItem] setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"ReleaseScreen.Tabbar.Info.Image"]]];

        if ([ThemeLoader boolForKey:@"PrintersScreen.Tabbar.UseOffImage"]) {
            [[self tabBarItem] setImage:[[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"ReleaseScreen.Tabbar.Info.Image.Off"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        }
        [[self tabBarItem] setSelectedImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"ReleaseScreen.Tabbar.Info.Image"]]];
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Info Release Screen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    [self resizeLabel:self.releaseLabel withText:NSLocalizedPONString(@"LABEL_RELEASE_INFO", nil)];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PrintersScreen.BackgroundColor"];
    [self.releaseImageView setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"ReleaseScreen.Info.Image"]]];

    // Adjust the sizing of the views based on the label size
    [self.releaseImageView setFrame:CGRectMake(self.releaseImageView.frame.origin.x, self.releaseLabel.frame.origin.y + self.releaseLabel.frame.size.height + 37, self.releaseImageView.frame.size.width, self.releaseImageView.frame.size.height)];
    [self.releaseView setFrame:CGRectMake(self.releaseView.frame.origin.x, self.releaseView.frame.origin.y, self.releaseView.frame.size.width, self.releaseImageView.frame.origin.y + self.releaseImageView.frame.size.height + 38)];
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text
{
    // Resize the label to hold the text
    [label setText:text];
    CGSize maxSize = CGSizeMake(label.frame.size.width, 2000);
    CGSize requiredSize = [label sizeThatFits:maxSize];
    
    if (requiredSize.height > label.frame.size.height) {
        [label setFrame:CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, requiredSize.height)];
    }
}

@end
