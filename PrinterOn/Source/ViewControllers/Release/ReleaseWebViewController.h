//
//  ReleaseWebViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2/5/2016.
//  Copyright (c) 2016 PrinterOn Inc. All rights reserved.
//

#import "UserAccount.h"

@interface ReleaseWebViewController : BaseViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, strong) NSString *serviceURL;
@property (nonatomic, strong) NSURL *requestURL;
@property (nonatomic, weak) id <UserAccountDelegate> delegate;

@end
