//
//  ReleaseInfoViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-01-10.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@interface ReleaseInfoViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet ShadowView *releaseView;
@property (weak, nonatomic) IBOutlet UIImageView *releaseImageView;
@property (weak, nonatomic) IBOutlet UILabel *releaseLabel;

@end
