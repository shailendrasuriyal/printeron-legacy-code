//
//  DefaultServiceViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-05-12.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "DefaultServiceViewController.h"

#import "AuthViewController.h"
#import "LoadPrinterViewController.h"
#import "Service.h"
#import "SplashScreen.h"
#import "UserAccount.h"

@interface DefaultServiceViewController ()

@end

@implementation DefaultServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Default Service Screen";

    if (self.wasOpenedIn) {
        self.wasOpenedIn = NO;
        [SplashScreen hide];
    }

    NSString *message = [NSString stringWithFormat:@"%@\n\n%@", NSLocalizedPONString(@"LABEL_WARN_DEFAULTSERVICE", nil), [self parameters][@"surl"]];
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_YES", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [Service createService:[self parameters][@"surl"] description:[self parameters][@"desc"] isDefault:YES isLocked:NO isMDM:NO isRestricted:NO completionBlock:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self transitionToNextScreen];
        });
    }];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_NO", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self transitionToNextScreen];
        });
    }];
    [alertVC addAction:yesAction];
    [alertVC addAction:noAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_SERVICESETUP", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"LoadPrinterScreen.BackgroundColor"];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
}

- (UIViewController *)backViewController {
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2) {
        return nil;
    } else {
        return [self.navigationController viewControllers][numberOfViewControllers - 2];
    }
}

- (void)pushToViewController:(UIViewController *)pushController fromViewController:(UIViewController *)fromController
{
    // Create an empty mutable array that will be used as a new stack of view controllers
    NSMutableArray *newControllers = [NSMutableArray array];
    
    // Go through the stack of current view controllers and add them to the new stack until we get to the base "from"
    // view controller.  Any view controllers between the "from" controller and the "push" controller are removed.
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        [newControllers addObject:vc];
        if ([vc isEqual:fromController]) {
            break;
        }
    }
    
    // Add to the top the view controller we want to transition too
    [newControllers addObject:pushController];
    
    // Perform the transition
    [self.navigationController setViewControllers:newControllers animated:YES];
}

- (void)transitionToNextScreen
{
    NSString *serviceURL = [self parameters][@"surl"];
    NSString *searchAuth = [self parameters][@"sauth"];

    if (searchAuth && [searchAuth boolValue]) {
        UserAccount *userAccount = [UserAccount getUserAccountForURL:[NSURL URLWithString:serviceURL]];
        if (userAccount && [userAccount.isAnonymous boolValue]) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];

            AuthViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"searchUserAuth"];
            controller.parameters = self.parameters;
            [self pushToViewController:controller fromViewController:[self backViewController]];
            return;
        }
    }

    NSString *printerID = [self parameters][@"pid"];

    if ([printerID length] > 0) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];

        LoadPrinterViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoadPrinter"];
        controller.parameters = self.parameters;
        [self pushToViewController:controller fromViewController:[self backViewController]];
        return;
    }

    [self.navigationController popViewControllerAnimated:YES];
}

@end
