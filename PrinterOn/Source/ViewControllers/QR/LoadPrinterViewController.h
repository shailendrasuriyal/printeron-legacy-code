//
//  LoadPrinterViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 12/31/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface LoadPrinterViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *activityView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activitySpinner;
@property (weak, nonatomic) IBOutlet UILabel *activityLabel;

@property (nonatomic, strong) NSDictionary *parameters;
@property (nonatomic, assign) BOOL wasOpenedIn;

@end
