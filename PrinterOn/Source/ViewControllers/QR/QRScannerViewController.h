//
//  QRScannerViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 12/23/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface QRScannerViewController : BaseViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIView *readerView;
@property (weak, nonatomic) IBOutlet UIButton *buttonTorch;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UILabel *labelToolbar;

@property (nonatomic, assign) BOOL hideCloseButton;
@property (nonatomic, strong) NSString *releaseParent;

@end
