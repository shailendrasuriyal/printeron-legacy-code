//
//  AuthViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 06/03/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "AuthViewController.h"

#import "AccountCell.h"
#import "CSLinearLayoutView.h"
#import "JobInputsViewController.h"
#import "LoadPrinterViewController.h"
#import "OAuth2Manager.h"
#import "QRScannerViewController.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "ServiceDescription.h"
#import "SplashScreen.h"
#import "UserSetupViewController.h"

@interface AuthViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) AccountCell *createAccountCell;
@property (nonatomic, strong) UITableViewCell *loadingCell;
@property (nonatomic, strong) UserAccount *userAccount;

@property (nonatomic, strong) CSLinearLayoutView *linearLayout;
@property (nonatomic, strong) CSLinearLayoutItem *descriptionItem;
@property (nonatomic, strong) CSLinearLayoutItem *serviceItem;
@property (nonatomic, strong) CSLinearLayoutItem *tableItem;

@property (nonatomic, assign) BOOL isPublicHosted;
@property (nonatomic, assign) BOOL isLoadingCapabilities;

@end

@implementation AuthViewController

- (void)viewDidLoad
{
    self.isLoadingCapabilities = YES;
    self.loadingCell = [self.tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
    self.createAccountCell = [self.tableView dequeueReusableCellWithIdentifier:@"createAccountCell"];
    [self.createAccountCell setupCell:NSLocalizedPONString(@"LABEL_ADDACCOUNT", nil) withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.AddButton.Dark.Image"]] showLock:NO];
    [self.tableView setTableHeaderView:self.loadingCell.contentView];

    [super viewDidLoad];

    self.isPublicHosted = [Service isHostedString:[self parameters][@"surl"] andPublic:YES];
    [self registerForEnterForegroundNotification];

    [self trustService];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Search Auth Screen";

    if (self.wasOpenedIn) {
        self.wasOpenedIn = NO;
        [SplashScreen hide];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)customEnterForeground
{
    // Refresh the fetch controller and table
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    [self updateTableView];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"LABEL_USERCREDENTIALS", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"LoadPrinterScreen.BackgroundColor"];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
}

- (void)updateAccounts
{
    // Initialize Linear Layout
    if (!self.linearLayout) {
        self.linearLayout = [[CSLinearLayoutView alloc] initWithFrame:self.view.bounds];
        self.linearLayout.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.linearLayout.autoAdjustFrameSize = YES;
        self.linearLayout.orientation = CSLinearLayoutViewOrientationVertical;
    } else {
        [self.linearLayout removeAllItems];
    }

    // Add Description View
    NSString *descText = NSLocalizedPONString(@"LABEL_SERVICE_AUTH", nil);
    [self resizeLabel:self.descriptionLabel withText:descText inView:self.descriptionView addHeight:9];

    self.descriptionItem = [self setupItem:self.descriptionItem withView:self.descriptionView withPadding:CSLinearLayoutMakePadding(10, 8, 12, 9)];
    [self.linearLayout addItem:self.descriptionItem];

    // Add Service View
    NSString *serviceText = self.isPublicHosted ? [Service hostedService].serviceDescription : [self parameters][@"surl"];
    [self resizeLabel:self.serviceLabel withText:serviceText inView:self.serviceView addHeight:9];

    self.serviceItem = [self setupItem:self.serviceItem withView:self.serviceView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
    [self.linearLayout addItem:self.serviceItem];

    // Add Table View
    self.tableItem = [self setupItem:self.tableItem withView:self.contentView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
    [self.linearLayout addItem:self.tableItem];

    // Add linear layout to the scrollview
    [self.scrollView addSubview:self.linearLayout];

    [self updateContentHeight];
}

- (CSLinearLayoutItem *)setupItem:(CSLinearLayoutItem *)item withView:(UIView *)view withPadding:(CSLinearLayoutItemPadding)padding
{
    if (!item) {
        item = [CSLinearLayoutItem layoutItemForView:view];
        item.padding = padding;
        item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    }
    [view setHidden:NO];
    return item;
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text inView:(UIView *)view
{
    [self resizeLabel:label withText:text inView:view addHeight:0];
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text inView:(UIView *)view addHeight:(NSUInteger)add
{
    // Resize the label to hold the text
    [label setText:text];
    CGSize maxSize = CGSizeMake(label.frame.size.width, 2000);
    CGSize requiredSize = [label sizeThatFits:maxSize];
    
    if (requiredSize.height > label.frame.size.height) {
        [label setFrame:CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, requiredSize.height)];
        
        // Resize the view the label is inside to fit the new size
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, label.frame.origin.y + label.frame.size.height + add)];
    }
}

- (void)trustService
{
    // Get the search URL
    NSString *URL = [self parameters][@"surl"];

    // Get the Service if it already exists
    Service *service = nil;
    if (self.isPublicHosted) {
        service = [Service hostedService];
    } else {
        NSArray *results = [Service doesServiceExist:URL inContext:nil];
        service = (results && ([results count] > 0)) ? results.firstObject : nil;
    }

    // Service exists so we can continue without asking for trust
    if (service) {
        // Update service capabilities so we have accurate authentication information before allowing selection of a user account
        __weak __typeof(self) weakSelf = self;
        [ServiceDescription capabilitiesForServiceWithURL:[NSURL URLWithString:URL] forceUpdate:NO completionBlock:^(NSManagedObjectID *capabilities) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (!strongSelf) return;

            strongSelf.isLoadingCapabilities = NO;
            [strongSelf.tableView setTableHeaderView:strongSelf.createAccountCell.contentView];
            [strongSelf updateTableView];
        }];
        return;
    }

    self.contentView.userInteractionEnabled = NO;

    // Display confirmation dialog to trust service
    NSString *message = [NSString stringWithFormat:@"%@\n\n%@", NSLocalizedPONString(@"LABEL_WARN_SERVICE", nil), URL];
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_TRUST_SERVICE", nil) message:message preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_YES", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            __weak __typeof(self) weakSelf = self;
            [Service createService:URL description:nil isDefault:NO isLocked:NO isMDM:NO isRestricted:NO completionBlock:^(NSManagedObjectID *capabilities) {

                __strong __typeof(weakSelf) strongSelf = weakSelf;
                if (!strongSelf) return;

                strongSelf.isLoadingCapabilities = NO;
                strongSelf.contentView.userInteractionEnabled = YES;
                [strongSelf.tableView setTableHeaderView:strongSelf.createAccountCell.contentView];
                [strongSelf updateTableView];
            }];
        });
    }];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_NO", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    }];
    [alertVC addAction:yesAction];
    [alertVC addAction:noAction];

    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)setUserAccountForService:(UserAccount *)account
{
    self.userAccount = account;

    // Get the search URL
    NSString *URL = [self parameters][@"surl"];

    // Get the Service if it already exists or create it
    Service *service = nil;
    if (self.isPublicHosted) {
        service = [Service hostedService];
    } else {
        NSArray *results = [Service doesServiceExist:URL inContext:nil];
        service = (results && ([results count] == 0)) ? nil : results[0];
    }

    void (^popViewBlock)(void) = ^{
        if (self.isPublicHosted) {
            [UserAccount removeHostedDefault:nil];
            [self.userAccount setHostedDefault:nil];
        } else {
            NSMutableSet *services = [NSMutableSet setWithSet:self.userAccount.serviceDefaults];
            if (service && ![services containsObject:service]) {
                [services addObject:service];
                [self.userAccount setServiceDefaults:services inContext:nil];
            }
        }

        NSString *printerID = [self parameters][@"pid"];

        if ([printerID length] > 0) {
            LoadPrinterViewController *destViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"LoadPrinter"];
            destViewController.parameters = self.parameters;
            [self pushToViewController:destViewController fromViewController:[self backViewController]];
            return;
        }

        if (self.navigationController.viewControllers.count > [self.navigationController.viewControllers indexOfObject:self] + 1) {
            NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];
            [controllers removeObjectAtIndex:controllers.count - 1];
            [controllers removeObjectAtIndex:controllers.count - 1];
            [self.navigationController setViewControllers:controllers animated:YES];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    };

    if ([ServiceCapabilities serviceSupportsOAuthThirdParty:URL]) {
        [self showThirdPartyOAuthLogin:URL completionHandler:popViewBlock];
        return;
    }

    popViewBlock();
}

- (UIViewController *)backViewController {
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];

    if (index == 0 || index == NSNotFound) {
        return self;
    } else {
        return [self.navigationController viewControllers][index - 1];
    }
}

- (void)pushToViewController:(UIViewController *)pushController fromViewController:(UIViewController *)fromController
{
    // Create an empty mutable array that will be used as a new stack of view controllers
    NSMutableArray *newControllers = [NSMutableArray array];
    
    // Go through the stack of current view controllers and add them to the new stack until we get to the base "from"
    // view controller.  Any view controllers between the "from" controller and the "push" controller are removed.
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        [newControllers addObject:vc];
        if ([vc isEqual:fromController]) {
            break;
        }
    }
    
    // Add to the top the view controller we want to transition too
    if (pushController) {
        [newControllers addObject:pushController];
    }
    
    // Perform the transition
    [self.navigationController setViewControllers:newControllers animated:YES];
}

- (void)showThirdPartyOAuthLogin:(NSString *)serviceURL completionHandler:(void (^)(void))handler
{
    __weak __typeof(self) weakSelf = self;
    GTMOAuth2ViewControllerCompletionHandler GTMOHandler = ^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error)
    {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        BOOL userCancelled = error && ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorWindowClosed);
        if (userCancelled) {
            return;
        }

        if (error) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [strongSelf presentViewController:alertVC animated:YES completion:nil];
            return;
        }

        [strongSelf.userAccount saveOAuth2Authentication:auth forService:serviceURL];
        handler();
    };

    GTMOAuth2ViewControllerTouch *vc = [OAuth2Manager viewControllerForPON:GTMOHandler serviceURL:serviceURL];
    vc.navigationItem.title = self.navigationItem.title;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"UserAccount"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"accountDescription" ascending:YES]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;
        
        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
    [self updateAccounts];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.linearLayout.frame.size.height)];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self updateTableView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.isLoadingCapabilities) {
        return 1;
    } else {
        return [[self.fetchedResultsController sections] count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isLoadingCapabilities) {
        return 0;
    } else {
        if ([[self.fetchedResultsController sections] count] > 0) {
            id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
            return [sectionInfo numberOfObjects];
        } else {
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"userAccountCell";
    AccountCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UserAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCell:account.accountDescription withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"UserAccounts.User.Image"]] showLock:NO];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self setUserAccountForService:account];
    return indexPath;
}

#pragma mark - UserAccountDelegate

- (void)didFinishWithUserAccount:(UserAccount *)account
{
    [self setUserAccountForService:account];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"setupAccount"]) {
        UserSetupViewController *destViewController = segue.destinationViewController;
        destViewController.delegate = self;
        destViewController.hideServices = YES;
    }
}

@end
