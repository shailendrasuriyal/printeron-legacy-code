//
//  QRScannerViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 12/23/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "QRScannerViewController.h"

#import "AuthViewController.h"
#import "BarButtonItem.h"
#import "DefaultServiceViewController.h"
#import "JCActivityBar.h"
#import "LoadPrinterViewController.h"
#import "NSString+Parameters.h"
#import "NSString+URL.h"
#import "Service.h"

@interface QRScannerViewController ()

@property (nonatomic, strong) AVCaptureDevice *captureDevice;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureMetadataOutput *captureMetadataOutput;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@property (nonatomic, strong) JCActivityBar *activityBar;
@property (nonatomic, strong) UIView *shutterView;
@property (nonatomic, strong) UIView *overlayTop, *overlayBottom, *overlayRight, *overlayLeft;
@property (nonatomic, strong) UIView *flashView;
@property (nonatomic, strong) UIView *borderView;
@property (nonatomic, strong) CAShapeLayer *dashLayer;
@property (nonatomic, strong) CAKeyframeAnimation *flashAnimation;
@property (nonatomic, assign) BOOL scannedCode;

@end

@implementation QRScannerViewController

- (void)viewDidLoad
{
    // Create the ActivityBar
    self.activityBar = [JCActivityBar new];
    [self.activityBar positionInBottomOfView:self.backgroundView withBottomOffset:53.0];
    [self.backgroundView addSubview:self.activityBar];

    [super viewDidLoad];

    [self initCamera];
    [self initOverlay];
    [self setupScanFlash];
    [self initTorch];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureSessionDidStartRunningNotification object:self.captureSession];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureSessionInterruptionEndedNotification object:self.captureSession];
}

- (void) viewDidDisappear: (BOOL) animated
{
    // Stop the embeded camera reader
    self.shutterView.alpha = 1;
    self.shutterView.hidden = NO;
    [self.captureSession stopRunning];

    [super viewDidDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"QR Scanner Screen";

    // Start the embeded camera reader
    self.scannedCode = NO;
    [self.captureSession startRunning];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];

    AVCaptureConnection *connection = self.videoPreviewLayer.connection;
    if (connection.supportsVideoOrientation && toInterfaceOrientation != UIInterfaceOrientationUnknown) {
        connection.videoOrientation = (AVCaptureVideoOrientation)toInterfaceOrientation;
    }
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_QRSCANNER", nil);
    [self.labelToolbar setText:NSLocalizedPONString(@"LABEL_SCANCODE", nil)];
}

- (void)setupTheme
{
    [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];
    if (self.hideCloseButton) {
        [self.backButton setHidden:YES];
    }

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"QRScannerScreen.BackgroundColor"];

    self.buttonTorch.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.buttonTorch.layer.borderWidth = 1;
    self.buttonTorch.layer.cornerRadius = 5;

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    // Set ToolBar appearance
    [self.toolBar setTintColor:[ThemeLoader colorForKey:@"QRScannerScreen.Toolbar.TextColor"]];
    [self.toolBar setBarTintColor:[ThemeLoader colorForKey:@"QRScannerScreen.Toolbar.BackgroundColor"]];
    self.toolBar.layer.shadowOpacity = 0.75f;
    self.toolBar.layer.shadowRadius = 1.5f;
    self.toolBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.toolBar.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);

    self.labelToolbar.textColor = [ThemeLoader colorForKey:@"QRScannerScreen.Toolbar.TextColor"];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    [self updateContentHeight];

    // Compute the shadow paths after layout
    self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 3, self.navigationController.navigationBar.layer.bounds.origin.y, self.navigationController.navigationBar.layer.bounds.size.width + 6, self.navigationController.navigationBar.layer.bounds.size.height)].CGPath;

    self.toolBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.toolBar.layer.bounds.origin.x - 3, self.toolBar.layer.bounds.origin.y, self.toolBar.layer.bounds.size.width + 6, self.toolBar.layer.bounds.size.height)].CGPath;
}

- (void)updateContentHeight
{
    [self.readerView setFrame:CGRectMake(self.readerView.frame.origin.x, self.readerView.frame.origin.y, self.readerView.frame.size.width, self.toolBar.frame.origin.y)];

    float totalHeight = self.readerView.frame.size.height;
    float totalWidth = self.readerView.frame.size.width;
    int windowWidth = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 328 : 220;
    int windowHeight = windowWidth;
    int windowX = (totalWidth - windowWidth) / 2;
    int windowY = (totalHeight - windowHeight) / 2;

    // Normalize the co-ordinates to set the crop region
    float cropX = windowY / totalHeight;
    float cropY = windowX / totalWidth;
    float cropWidth = windowHeight / totalHeight;
    float cropHeight = windowWidth / totalWidth;
    self.captureMetadataOutput.rectOfInterest = CGRectMake(cropX, cropY, cropWidth, cropHeight);

    [self.shutterView setFrame:self.readerView.frame];

    [self.overlayTop setFrame:CGRectMake(windowX, 0, windowWidth, windowY)];
    [self.overlayLeft setFrame:CGRectMake(0, 0, windowX, totalHeight)];
    [self.overlayRight setFrame:CGRectMake(windowX + windowWidth, 0, windowX, totalHeight)];
    [self.overlayBottom setFrame:CGRectMake(windowX, windowY + windowHeight, windowWidth, windowY)];

    [self.borderView setFrame:CGRectMake(windowX, windowY, windowWidth, windowHeight)];

    self.dashLayer.path = [UIBezierPath bezierPathWithRect:self.borderView.bounds].CGPath;
    self.dashLayer.frame = self.borderView.bounds;

    [self.flashView setFrame:self.readerView.frame];
}

- (void) initCamera
{
    self.captureSession = [AVCaptureSession new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onVideoStart) name:AVCaptureSessionDidStartRunningNotification object:self.captureSession];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onVideoStart) name:AVCaptureSessionInterruptionEndedNotification object:self.captureSession];

    self.captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];

    if ([self.captureDevice lockForConfiguration:nil]) {
        if ([self.captureDevice isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
            self.captureDevice.focusMode = AVCaptureFocusModeContinuousAutoFocus;
        }

        if ([self.captureDevice isSmoothAutoFocusSupported]) {
            self.captureDevice.smoothAutoFocusEnabled = YES;
        }

        if ([self.captureDevice isAutoFocusRangeRestrictionSupported]) {
            self.captureDevice.autoFocusRangeRestriction = AVCaptureAutoFocusRangeRestrictionNear;
        }

        if ([self.captureDevice isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance]) {
            self.captureDevice.whiteBalanceMode = AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance;
        }

        if ([self.captureDevice isTorchModeSupported:AVCaptureTorchModeAuto]) {
            self.captureDevice.torchMode = AVCaptureTorchModeAuto;
        }

        if ([self.captureDevice isLowLightBoostSupported]) {
            self.captureDevice.automaticallyEnablesLowLightBoostWhenAvailable = YES;
        }

        [self.captureDevice unlockForConfiguration];
    }

    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice error:nil];
    if (input) {
        [self.captureSession addInput:input];

        self.captureMetadataOutput = [AVCaptureMetadataOutput new];
        [self.captureSession addOutput:self.captureMetadataOutput];
        [self.captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [self.captureMetadataOutput setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    }

    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.videoPreviewLayer setFrame:self.readerView.layer.bounds];
    [self.readerView.layer addSublayer:self.videoPreviewLayer];

    AVCaptureConnection *connection = self.videoPreviewLayer.connection;
    if (connection.supportsVideoOrientation) {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        if (orientation != UIInterfaceOrientationUnknown) {
            connection.videoOrientation = (AVCaptureVideoOrientation)orientation;
        }
    }
}

- (void) initOverlay
{
    [self.readerView setFrame:CGRectMake(self.readerView.frame.origin.x, self.readerView.frame.origin.y, self.readerView.frame.size.width, self.toolBar.frame.origin.y)];

    // Add black shutter view
    self.shutterView = [[UIView alloc] initWithFrame:self.readerView.frame];
    self.shutterView.alpha = 1;
    self.shutterView.backgroundColor = [UIColor blackColor];
    self.shutterView.hidden = NO;
    [self.readerView addSubview:self.shutterView];

    // Add semi transparent masks to the view
    self.overlayTop = [UIView new];
    self.overlayBottom = [UIView new];
    self.overlayRight = [UIView new];
    self.overlayLeft = [UIView new];

    NSArray *masks = @[self.overlayTop, self.overlayBottom, self.overlayRight, self.overlayLeft];

    for(UIView *mask in masks) {
        mask.backgroundColor = [UIColor colorWithWhite:0 alpha:.5];
        [self.readerView addSubview: mask];
    }

    // Add the dash line border
    self.borderView = [UIView new];
    self.borderView.backgroundColor = [UIColor clearColor];

    self.dashLayer = [CAShapeLayer layer];
    self.dashLayer.strokeColor = [ThemeLoader colorForKey:@"QRScannerScreen.BorderColor"].CGColor;
    self.dashLayer.fillColor = nil;
    self.dashLayer.lineWidth = 4;
    self.dashLayer.lineDashPattern = @[@12, @8];
    [self.borderView.layer addSublayer:self.dashLayer];

    [self.readerView addSubview:self.borderView];
}

- (void)setupScanFlash
{
    self.flashView = [[UIView alloc] initWithFrame:self.readerView.frame];
    self.flashView.layer.opacity = 0.0f;
    self.flashView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [self.backgroundView addSubview:self.flashView];

    self.flashAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    NSArray *animationValues = @[ @0.8f, @0.0f ];
    NSArray *animationTimes = @[ @0.3f, @1.0f ];
    id timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    NSArray *animationTimingFunctions = @[ timingFunction, timingFunction ];
    [self.flashAnimation setValues:animationValues];
    [self.flashAnimation setKeyTimes:animationTimes];
    [self.flashAnimation setTimingFunctions:animationTimingFunctions];
    self.flashAnimation.fillMode = kCAFillModeForwards;
    self.flashAnimation.removedOnCompletion = YES;
    self.flashAnimation.duration = 0.4;
}

- (void)doScanFlash
{
    [self.flashView.layer addAnimation:self.flashAnimation forKey:@"animation"];
}

- (void)initTorch
{
    if (![self.captureDevice hasTorch]) {
        self.buttonTorch.hidden = YES;
        return;
    }

    [self updateTorchButton];
}

- (void)updateTorchButton
{
    if (self.captureDevice.torchMode == AVCaptureTorchModeAuto) {
        [self.buttonTorch setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"QRScannerScreen.TorchAuto.Image"]] forState:UIControlStateNormal];
    } else if (self.captureDevice.torchMode == AVCaptureTorchModeOff) {
        [self.buttonTorch setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"QRScannerScreen.TorchOff.Image"]] forState:UIControlStateNormal];
    } else if (self.captureDevice.torchMode == AVCaptureTorchModeOn) {
        [self.buttonTorch setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"QRScannerScreen.TorchOn.Image"]] forState:UIControlStateNormal];
    }
}

- (IBAction)toggleTorch:(id)sender {
    if ([self.captureDevice lockForConfiguration:nil]) {
        if (self.captureDevice.torchMode == AVCaptureTorchModeOff) {
            self.captureDevice.torchMode = [self.captureDevice isTorchModeSupported:AVCaptureTorchModeAuto] ? AVCaptureTorchModeAuto : AVCaptureTorchModeOn;
        } else if (self.captureDevice.torchMode == AVCaptureTorchModeOn) {
            self.captureDevice.torchMode = AVCaptureTorchModeOff;
        } else if (self.captureDevice.torchMode == AVCaptureTorchModeAuto) {
            self.captureDevice.torchMode = AVCaptureTorchModeOn;
        }
        [self.captureDevice unlockForConfiguration];
    }

    [self updateTorchButton];
}

- (void)onVideoStart
{
    if(!self.shutterView.hidden) {
        [UIView animateWithDuration:.25 animations: ^{
            self.shutterView.alpha = 0;
        } completion: ^(BOOL finished) {
            self.shutterView.hidden = YES;
        }];
    }
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate

- (NSString *)segueFromParameters:(NSDictionary *)parameters
{
    NSString *serviceURL = parameters[@"surl"];

    if ([serviceURL length] > 0) {
        // Check to see if we have a restricted service URL
        Service *restricted = [Service getRestrictedService];
        if (restricted) {
            NSString *URL = [Service cleanServiceURL:serviceURL];
            if (![Service compareURL:URL toURL:restricted.serviceURL]) {
                // If the service URL is equal to the restricted URL then we can continue, otherwise we return and do nothing
                return @"restricted";
            }
        }

        NSString *serviceDefault = parameters[@"sdef"];
        if ([serviceDefault length] > 0 && [serviceDefault intValue] == 1) {
            BOOL setService = YES;

            Service *mdm = [Service getMDMService];
            if (mdm && mdm.isDefault.boolValue && mdm.serviceLock.boolValue) {
                setService = NO;
            } else {
                NSArray *results = [Service doesServiceExist:serviceURL inContext:nil];
                if ([results count] > 0) {
                    Service *service = results[0];
                    if ([service.isDefault boolValue]) {
                        setService = NO;
                    }
                }
            }

            if (setService) {
                return @"defaultService";
            }
        }

        NSString *serviceAuth = parameters[@"sauth"];
        if ([serviceAuth length] > 0 && [serviceAuth intValue] == 1) {
            UserAccount *userAccount = [UserAccount getUserAccountForURL:[NSURL URLWithString:serviceURL]];
            if ([userAccount.isAnonymous boolValue]) {
                return @"searchAuth";
            }
        }

        NSString *printerID = parameters[@"pid"];
        if ([printerID length] > 0) {
            return @"loadPrinter";
        }
    }

    return nil;
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    if (!self.scannedCode && metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = metadataObjects[0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            self.scannedCode = YES;
            [self doScanFlash];

            NSURL *url = [NSURL URLWithString:[metadataObj stringValue]];
            if ([url.scheme isEqualToString:@"pon"]) {
                // Create a dictionary from the URL querystring
                NSMutableDictionary *parameters = [[[url query] URLDecode] parametersSeparatedByInnerString:@"=" andOuterString:@"&" lowerCaseKeys:YES];
                if (self.releaseParent && self.releaseParent.length > 0) {
                    parameters[@"releaseParent"] = self.releaseParent;
                    parameters[@"dp"] = @"1";
                }

                NSString *segueIdentifier = [self segueFromParameters:parameters];
                if (segueIdentifier != nil) {
                    if ([segueIdentifier isEqualToString:@"restricted"]) {
                        [self.activityBar displayActivityWithMessage:@""];
                        [self.activityBar finishWithError:NSLocalizedPONString(@"ERROR_RESTRICTED", nil)];
                    } else {
                        [self performSegueWithIdentifier:segueIdentifier sender:parameters];
                        return;
                    }
                }
            } else {
                [self.activityBar displayActivityWithMessage:@""];
                [self.activityBar finishWithError:NSLocalizedPONString(@"ERROR_QRINVALIDCODE", nil)];
            }

            // Re-activate processing scanning after time period
            __weak __typeof(&*self)weakSelf = self;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                __strong __typeof(&*weakSelf)strongSelf = weakSelf;
                if (!strongSelf) {
                    return;
                }
                strongSelf.scannedCode = NO;
            });
        }
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"loadPrinter"]) {
        LoadPrinterViewController *destViewController = segue.destinationViewController;
        destViewController.parameters = sender;
    } else if ([segue.identifier isEqualToString:@"searchAuth"]) {
        AuthViewController *destViewController = segue.destinationViewController;
        destViewController.parameters = sender;
    } else if ([segue.identifier isEqualToString:@"defaultService"]) {
        DefaultServiceViewController *destViewController = segue.destinationViewController;
        destViewController.parameters = sender;
    }
}

@end
