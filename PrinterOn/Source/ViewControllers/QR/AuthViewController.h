//
//  AuthViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 06/03/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "UserAccount.h"

@interface AuthViewController : BaseViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UserAccountDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *descriptionView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet ShadowView *serviceView;
@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;

@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSDictionary *parameters;
@property (nonatomic, assign) BOOL wasOpenedIn;

@end
