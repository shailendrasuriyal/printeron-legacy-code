//
//  MultiplePrintersViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-12-15.
//  Copyright © 2015 PrinterOn Inc. All rights reserved.
//

#import "MultiplePrintersViewController.h"

#import "Printer.h"
#import "PrinterCell.h"
#import "QRScannerViewController.h"
#import "ReleaseWebViewController.h"
#import "Service.h"
#import "ShadowButton.h"
#import "UserAccount.h"

@interface MultiplePrintersViewController () <UserAccountDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation MultiplePrintersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.printersList && self.printersList.count > 0) {
        NSSortDescriptor *firstSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        NSSortDescriptor *secondSort = [NSSortDescriptor sortDescriptorWithKey:@"organizationLocationDesc" ascending:YES  selector:@selector(caseInsensitiveCompare:)];
        self.printersList = [self.printersList sortedArrayUsingDescriptors:@[firstSort,secondSort]];
        [self.tableView reloadData];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    if (self.remoteReleaseURL == nil) {
        self.releaseView.hidden = YES;
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, self.scrollView.frame.origin.y, self.scrollView.frame.size.width, self.backgroundView.frame.size.height);
    } else {
        self.releaseView.hidden = NO;
        self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, self.scrollView.frame.origin.y, self.scrollView.frame.size.width, self.backgroundView.frame.size.height - self.releaseView.frame.size.height);
        self.releaseView.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.releaseView.layer.bounds.origin.x - 3, self.releaseView.layer.bounds.origin.y, self.releaseView.layer.bounds.size.width + 6, self.releaseView.layer.bounds.size.height)].CGPath;
    }

    [self updateContentHeight];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Multiple Printers Screen";
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_SELECT_PRINTER", nil);
    self.releaseLabel.text = NSLocalizedPONString(@"LABEL_RELEASE_WEB_INFO", nil);
    [self.releaseButton setTitle:NSLocalizedPONString(@"TITLE_RELEASE_JOB", nil) forState:UIControlStateNormal];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PrintersScreen.BackgroundColor"];

    self.releaseView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.Bottom.BackgroundColor"];
    self.releaseView.layer.shadowOpacity = 0.75f;
    self.releaseView.layer.shadowRadius = 1.5f;
    self.releaseView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.releaseView.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);

    self.releaseButton.layer.cornerRadius = 4.0f;
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.tableView.contentSize.height)];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
}

- (UIViewController *)backViewController {
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2) {
        return nil;
    } else {
        return [self.navigationController viewControllers][numberOfViewControllers - 2];
    }
}

- (IBAction)pressedReleaseJob
{
    [self performSegueWithIdentifier:@"remoteReleaseWeb" sender:nil];
}

- (BOOL)popToViewControllerOfClass:(Class)vcClass animated:(BOOL)animated
{
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        if ([vc isMemberOfClass:vcClass]) {
            [self.navigationController popToViewController:vc animated:animated];
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.printersList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"printerCell";
    PrinterCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell setupCellWithPrinter:[self printersList][indexPath.row]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Printer setSingletonPrinter:[self printersList][indexPath.row] forEntity:@"SelectedPrinter"];
    if (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        UIViewController *backVC = [self backViewController];
        if ([backVC isMemberOfClass:[QRScannerViewController class]]) {
            if (((QRScannerViewController *)backVC).hideCloseButton) {
                NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];

                // Remove this controller, the QR Scanner controller, and the Printers controller below it
                [controllers removeObjectAtIndex:controllers.count - 1];
                [controllers removeObjectAtIndex:controllers.count - 1];
                [controllers removeObjectAtIndex:controllers.count - 1];

                [self.navigationController setViewControllers:controllers animated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    return indexPath;
}

#pragma mark - UserAccountDelegate

- (void)didFinishWithUserAccount:(UserAccount *)account
{
    // Get the Service if it already exists or create it
    NSURL *serviceURL = [NSURL URLWithString:self.serviceURL];
    if ([Service isHostedURL:serviceURL andPublic:YES]) {
        [UserAccount removeHostedDefault:nil];
        [account setHostedDefault:nil];
    } else {
        NSArray *results = [Service doesServiceExist:self.serviceURL inContext:nil];
        Service *service = (results && ([results count] == 0)) ? [Service createService:self.serviceURL description:nil isDefault:NO isLocked:NO isMDM:NO isRestricted:NO completionBlock:nil] : results[0];

        // Add the UserAccount to the Service as a default
        NSMutableSet *services = [NSMutableSet setWithSet:account.serviceDefaults];
        if (![services containsObject:service]) {
            [services addObject:service];
            [account setServiceDefaults:services inContext:nil];
        }
    }

    [self popToViewControllerOfClass:self.class animated:YES];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printerDetailsMultiple"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        [Printer setSingletonPrinter:[self printersList][indexPath.row] forEntity:@"DetailsPrinter"];
    } else if ([segue.identifier isEqualToString:@"remoteReleaseWeb"]) {
        ReleaseWebViewController *destViewController = segue.destinationViewController;
        destViewController.serviceURL = self.serviceURL;
        destViewController.requestURL = [NSURL URLWithString:self.remoteReleaseURL];
        destViewController.delegate = self;
    }
}

@end
