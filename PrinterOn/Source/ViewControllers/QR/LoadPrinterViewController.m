//
//  LoadPrinterViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 12/31/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "LoadPrinterViewController.h"

#import "DirSearch.h"
#import "MultiplePrintersViewController.h"
#import "OAuth2Manager.h"
#import "Printer.h"
#import "PrinterDetailsViewController.h"
#import "SplashScreen.h"

@interface LoadPrinterViewController ()

@property (nonatomic, strong) NSManagedObjectContext *scratchSearchObjectContext;
@property (nonatomic, strong) RKObjectManager *searchObjectManager;
@property (nonatomic, strong) RKManagedObjectRequestOperation *currentOperation;

@end

@implementation LoadPrinterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Setup a scratch context in Core Data to use for DirSearch
    self.scratchSearchObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
    self.scratchSearchObjectContext.undoManager = nil;

    // Setup an object manager to use for DirSearch
    self.searchObjectManager = [DirSearch setupSearchForEntity:@"QRPrinter" withService:[self parameters][@"surl"]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Load Printer Screen";

    if (self.wasOpenedIn) {
        self.wasOpenedIn = NO;
        [SplashScreen hide];
    }

    [self performSearch];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self cancelCurrentSearch];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PRINTERDETAILS", nil);
    [self.activityLabel setText:NSLocalizedPONString(@"LABEL_LOADINGPRINTER", nil)];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"LoadPrinterScreen.BackgroundColor"];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    self.activityView.backgroundColor = [ThemeLoader colorForKey:@"LoadPrinterScreen.Activity.BackgroundColor"];
    self.activityView.layer.borderWidth = 1.5f;
    self.activityView.layer.borderColor = [ThemeLoader colorForKey:@"LoadPrinterScreen.Activity.BorderColor"].CGColor;
    self.activityView.layer.cornerRadius = 10;
    self.activityView.layer.masksToBounds = NO;
    self.activityView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.activityView.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    self.activityView.layer.shadowOpacity = 0.75f;
    self.activityView.layer.shadowRadius = 2.0f;

    self.activitySpinner.color = [ThemeLoader colorForKey:@"LoadPrinterScreen.Activity.SpinnerColor"];
    self.activityLabel.textColor = [ThemeLoader colorForKey:@"LoadPrinterScreen.Activity.TextColor"];
}

- (UIViewController *)backViewController {
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2) {
        return nil;
    } else {
        return [self.navigationController viewControllers][numberOfViewControllers - 2];
    }
}

- (void)pushToViewController:(UIViewController *)pushController fromViewController:(UIViewController *)fromController
{
    // Create an empty mutable array that will be used as a new stack of view controllers
    NSMutableArray *newControllers = [NSMutableArray array];
    
    // Go through the stack of current view controllers and add them to the new stack until we get to the base "from"
    // view controller.  Any view controllers between the "from" controller and the "push" controller are removed.
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        [newControllers addObject:vc];
        if ([vc isEqual:fromController]) {
            break;
        }
    }
    
    // Add to the top the view controller we want to transition too
    [newControllers addObject:pushController];
    
    // Perform the transition
    [self.navigationController setViewControllers:newControllers animated:YES];
}

#pragma mark - DirSearch

- (void)performSearch {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
        @"searchType": @"searchByPrinterNum",
        @"searchPrinterNum": [self parameters][@"pid"] ?: @"",
        @"showChildren": @"1",
        @"maxResults": @"1",
    }];

    if (self.parameters[@"releaseParent"]) {
        params[@"searchParentNum"] = self.parameters[@"releaseParent"];
    }

    // Cancel the previous search if it is still running
    [self cancelCurrentSearch];

    // Clear previous results
    [self.scratchSearchObjectContext reset];

    __weak LoadPrinterViewController *weakSelf = self;
    [DirSearch createSearchWithParameters:params objectManager:self.searchObjectManager managedObjectContext:self.scratchSearchObjectContext success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        __strong LoadPrinterViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchSuccess:mappingResult];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        __strong LoadPrinterViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchFailure:error];
    } completionHandler:^(RKManagedObjectRequestOperation *operation, NSError *error) {
        __strong LoadPrinterViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf searchFailure:error];
        } else {
            strongSelf.currentOperation = operation;
            strongSelf.currentOperation.savesToPersistentStore = NO;
            [[RKObjectManager sharedManager] enqueueObjectRequestOperation:strongSelf.currentOperation];
        }
    }];
}

- (void)cancelCurrentSearch {
    if (self.currentOperation) [self.currentOperation cancel];
}

- (void)searchSuccess:(RKMappingResult *)mappingResult {
    for (id value in [[mappingResult dictionary] objectEnumerator]) {
        if ([value isMemberOfClass:[DirSearch class]]) {
            DirSearch *info = value;

            // The result is actually an error so send to failure
            if (![info.returnCode isEqualToString:@"0"]) {
                NSString *errorText = info.errText ? info.errText : info.returnCode;
                NSError *error = [[NSError alloc] initWithDomain:@"DirSearch" code:-999 userInfo:@{ NSLocalizedDescriptionKey : errorText ?: @""}];
                [self searchFailure:error];
                return;
            }

            if (info.resultCount > 0) {
                [Printer connectRelationshipsFromMapping:mappingResult.dictionary inContext:self.scratchSearchObjectContext];

                for (id printer in [mappingResult array]) {
                    if ([printer isKindOfClass:[Printer class]] && [printer isKindOfClass:[NSManagedObject class]]) {
                        NSManagedObject *managedObject = (NSManagedObject *)printer;
                        if ([managedObject.entity.name isEqualToString:@"ParentPrinter"]) continue;

                        Printer *result = (Printer *)printer;

                        NSString *remoteReleaseURL = [result getWebReleaseUIAddress];
                        if (remoteReleaseURL == nil) remoteReleaseURL = [result getRemoteReleaseCMDAddress];

                        NSString *directPrint = self.parameters[@"dp"];
                        if (result.parentIDs.count > 0 && directPrint != nil && directPrint.length > 0 && [directPrint isEqualToString:@"0"]) {
                            // This is a child printer that doesn't support direct printing.  So we actually need to display it's parents
                            // as printers to choose from.  We can delete the child printer from the result.
                            [self.scratchSearchObjectContext deleteObject:result];

                            // Get an array of all the parent printers from the result set
                            id resultParents = mappingResult.dictionary[@"DirSearch.parent"];
                            NSArray *parents = resultParents == nil ? nil : [resultParents isKindOfClass:[NSArray class]] ? [NSArray arrayWithArray:resultParents] : [NSArray arrayWithObject:resultParents];

                            // If there is more then one parent we have to display a screen to select between them.
                            if (remoteReleaseURL || parents.count > 1) {
                                MultiplePrintersViewController *multipleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"loadMultiplePrinters"];
                                multipleVC.scratchSearchObjectContext = self.scratchSearchObjectContext;
                                multipleVC.printersList = parents;
                                multipleVC.remoteReleaseURL = remoteReleaseURL;
                                multipleVC.serviceURL = self.parameters[@"surl"];
                                [self pushToViewController:multipleVC fromViewController:[self backViewController]];

                                return;
                            } else if (parents.count == 1) {
                                // There is only one parent so we can go directly to it's details
                                result = parents.firstObject;
                            }
                        }

                        BOOL releaseUI = self.parameters[@"releaseParent"] ? YES : NO;
                        if (remoteReleaseURL && !releaseUI && ![result.printerClass.lowercaseString isEqualToString:@"pull"]) {
                            MultiplePrintersViewController *multipleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"loadMultiplePrinters"];
                            multipleVC.scratchSearchObjectContext = self.scratchSearchObjectContext;
                            multipleVC.printersList = [NSArray arrayWithObject:result];
                            multipleVC.remoteReleaseURL = remoteReleaseURL;
                            multipleVC.serviceURL = self.parameters[@"surl"];
                            [self pushToViewController:multipleVC fromViewController:[self backViewController]];
                            
                            return;
                        } else {
                            // There is only one printer so we can go directly to it's details
                            PrinterDetailsViewController *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrinterDetails"];
                            detailsVC.releaseUI = releaseUI;
                            [Printer setSingletonPrinter:result forEntity:@"DetailsPrinter"];
                            [self pushToViewController:detailsVC fromViewController:[self backViewController]];

                            return;
                        }
                    }
                }
            }

            break;
        }
    }

    // No printers matched, display a message and exit the screen
    BOOL releaseUI = self.parameters[@"releaseParent"] ? YES : NO;

    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(releaseUI ? @"ERROR_QRRELEASENOTFOUND" : @"ERROR_QRNOTFOUND", nil) preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    }];
    [alertVC addAction:cancelAction];

    [self.activityView setHidden:YES];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)searchFailure:(NSError *)error {
    // Absorb the errors given when cancelling a job so they don't trigger the code after
    if (([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) ||
        ([error.domain isEqualToString:@"org.restkit.RestKit.ErrorDomain"] && error.code == 2)) return;

    [self.activityView setHidden:YES];

    // Display the error message
    if ([OAuth2Manager isAuthSettingsError:error]) {
        [OAuth2Manager showAuthSettingsError:error overController:self.navigationController];
    } else {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *retryAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_RETRY", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityView setHidden:NO];
                [self performSearch];
            });
        }];
        [alertVC addAction:retryAction];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }];
        [alertVC addAction:cancelAction];

        [self presentViewController:alertVC animated:YES completion:nil];
    }
}

@end
