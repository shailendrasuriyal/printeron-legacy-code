//
//  DefaultServiceViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-05-12.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface DefaultServiceViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;

@property (nonatomic, strong) NSDictionary *parameters;
@property (nonatomic, assign) BOOL wasOpenedIn;

@end
