//
//  MultiplePrintersViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-12-15.
//  Copyright © 2015 PrinterOn Inc. All rights reserved.
//

@class ShadowButton;

@interface MultiplePrintersViewController : BaseViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *releaseView;
@property (weak, nonatomic) IBOutlet UILabel *releaseLabel;
@property (weak, nonatomic) IBOutlet ShadowButton *releaseButton;

@property (nonatomic, strong) NSManagedObjectContext *scratchSearchObjectContext;

@property (nonatomic, strong) NSArray *printersList;
@property (nonatomic, strong) NSString *remoteReleaseURL;
@property (nonatomic, strong) NSString *serviceURL;

@end
