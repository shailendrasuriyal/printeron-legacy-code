//
//  PrintOptionsTableViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2014-11-11.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PrintOptionsTableViewController.h"

#import "Media.h"
#import "MZFormSheetController.h"
#import "PrintDocument.h"
#import "Printer.h"

@interface PrintOptionsTableViewController ()

@property (nonatomic, strong) NSMutableArray *tableData;

@end

@implementation PrintOptionsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(self.type == OptionsTableTypePaperSize ? @"LABEL_PAPER_SIZE" : self.type == OptionsTableTypeDuplex ? @"LABEL_DOUBLE_SIDED" : self.type == OptionsTableTypeWorksheet ? @"LABEL_WORKSHEET" : @"LABEL_ORIENTATION", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.Options.BackgroundColor"];
}

- (void)setupTableData
{
    self.tableData = [NSMutableArray array];

    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];

    if (self.type == OptionsTableTypePaperSize) {
        Media *selected = nil;
        for (Media *media in [printer mediaSupported]) {
            if ([[media.mediaSizeNum stringValue] isEqualToString:self.typeValue]) {
                selected = media;
            } else {
                [self.tableData addObject:media];
            }
        }
 
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"mediaSizeDim" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
        [self.tableData sortUsingDescriptors:@[descriptor]];
        if (selected) [self.tableData insertObject:selected atIndex:0];
    } else if (self.type == OptionsTableTypeDuplex) {
        if (![[printer.duplexPermit lowercaseString] isEqualToString:@"duplex_only"]) {
            [self.tableData addObject:@(DuplexOptionOff).stringValue];
        }
        [self.tableData addObject:@(DuplexOptionLong).stringValue];
        [self.tableData addObject:@(DuplexOptionShort).stringValue];
    } else if (self.type == OptionsTableTypeOrientation) {
        if (self.document && [self.document showDocumentDefaultOrientation]) {
            [self.tableData addObject:@(OrientationOptionDocument).stringValue];
        }
        [self.tableData addObject:@(OrientationOptionLandscape).stringValue];
        [self.tableData addObject:@(OrientationOptionPortrait).stringValue];
    } else if (self.type == OptionsTableTypeWorksheet) {
        [self.tableData addObject:@(WorksheetOptionActive).stringValue];
        [self.tableData addObject:@(WorksheetOptionAll).stringValue];
    }
}

- (NSString *)convertDuplexOptionToLabel:(DuplexOption)option
{
    if (option == DuplexOptionOff) {
        return NSLocalizedPONString(@"LABEL_OFF", nil);
    } else if (option == DuplexOptionLong) {
        return NSLocalizedPONString(@"LABEL_DUPLEXLONG", nil);
    } else if (option == DuplexOptionShort) {
        return NSLocalizedPONString(@"LABEL_DUPLEXSHORT", nil);
    }
    return nil;
}

- (DuplexOption)convertStringToDuplexOption:(NSString *)string
{
    if ([string isEqualToString:@"Simplex"]) {
        return DuplexOptionOff;
    } else if ([string isEqualToString:@"DuplexLong"]) {
        return DuplexOptionLong;
    } else if ([string isEqualToString:@"DuplexShort"]) {
        return DuplexOptionShort;
    }
    return -1;
}

- (NSString *)convertDuplexOptionToString:(DuplexOption)option
{
    if (option == DuplexOptionOff) {
        return @"Simplex";
    } else if (option == DuplexOptionLong) {
        return @"DuplexLong";
    } else if (option == DuplexOptionShort) {
        return @"DuplexShort";
    }
    return nil;
}

- (NSString *)convertOrientationOptionToLabel:(OrientationOption)option
{
    if (option == OrientationOptionDocument) {
        return NSLocalizedPONString(@"LABEL_DOCUMENT", nil);
    } else if (option == OrientationOptionLandscape) {
        return NSLocalizedPONString(@"LABEL_LANDSCAPE", nil);
    } else if (option == OrientationOptionPortrait) {
        return NSLocalizedPONString(@"LABEL_PORTRAIT", nil);
    }
    return nil;
}

- (OrientationOption)convertStringToOrientationOption:(NSString *)string
{
    if (string == nil) {
        return OrientationOptionDocument;
    } else if ([string isEqualToString:@"Landscape"]) {
        return OrientationOptionLandscape;
    } else if ([string isEqualToString:@"Portrait"]) {
        return OrientationOptionPortrait;
    }
    return -1;
}

- (NSString *)convertOrientationOptionToString:(OrientationOption)option
{
    if (option == OrientationOptionDocument) {
        return nil;
    } else if (option == OrientationOptionLandscape) {
        return @"Landscape";
    } else if (option == OrientationOptionPortrait) {
        return @"Portrait";
    }
    return nil;
}

- (NSString *)convertWorksheetOptionToLabel:(OrientationOption)option
{
    if (option == WorksheetOptionActive) {
        return NSLocalizedPONString(@"LABEL_ACTIVE", nil);
    } else if (option == WorksheetOptionAll) {
        return NSLocalizedPONString(@"LABEL_ALL", nil);
    }
    return nil;
}

- (WorksheetOption)convertStringToWorksheetOption:(NSString *)string
{
    if ([string isEqualToString:@"Active"]) {
        return WorksheetOptionActive;
    } else if ([string isEqualToString:@"All"]) {
        return WorksheetOptionAll;
    }
    return -1;
}

- (NSString *)convertWorksheetOptionToString:(OrientationOption)option
{
    if (option == WorksheetOptionActive) {
        return @"Active";
    } else if (option == WorksheetOptionAll) {
        return @"All";
    }
    return nil;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.tableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 2)];
    [view setBackgroundColor:[ThemeLoader colorForKey:@"PreviewScreen.Options.BackgroundColor"]];
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"printOptionCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    if (self.type == OptionsTableTypeDuplex) {
        int duplexInt = [[self tableData][indexPath.section] intValue];
        [cell.textLabel setText:[self convertDuplexOptionToLabel:duplexInt]];

        if ([self convertStringToDuplexOption:self.typeValue] == duplexInt) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else if (self.type == OptionsTableTypePaperSize) {
        Media *media = [self tableData][indexPath.section];
        [cell.textLabel setText:media.mediaSizeDim];

        if ([[media.mediaSizeNum stringValue] isEqualToString:self.typeValue]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else if (self.type == OptionsTableTypeOrientation) {
        int orientationInt = [[self tableData][indexPath.section] intValue];
        [cell.textLabel setText:[self convertOrientationOptionToLabel:orientationInt]];
        
        if ([self convertStringToOrientationOption:self.typeValue] == orientationInt) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else if (self.type == OptionsTableTypeWorksheet) {
        int worksheetInt = [[self tableData][indexPath.section] intValue];
        [cell.textLabel setText:[self convertWorksheetOptionToLabel:worksheetInt]];
        
        if ([self convertStringToWorksheetOption:self.typeValue] == worksheetInt) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate) {
        if (self.type == OptionsTableTypeDuplex) {
            [self.delegate didPickOptionOfType:self.type WithValue:[self convertDuplexOptionToString:[(self.tableData)[indexPath.section] intValue]]];
        } else if (self.type == OptionsTableTypePaperSize) {
            Media *media = (self.tableData)[indexPath.section];
            [self.delegate didPickOptionOfType:self.type WithValue:[media.mediaSizeNum stringValue]];
        } else if (self.type == OptionsTableTypeOrientation) {
            [self.delegate didPickOptionOfType:self.type WithValue:[self convertOrientationOptionToString:[(self.tableData)[indexPath.section] intValue]]];
        } else if (self.type == OptionsTableTypeWorksheet) {
            [self.delegate didPickOptionOfType:self.type WithValue:[self convertWorksheetOptionToString:[(self.tableData)[indexPath.section] intValue]]];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
    return indexPath;
}

@end
