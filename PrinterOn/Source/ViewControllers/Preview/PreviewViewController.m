//
//  PreviewViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 11/21/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PreviewViewController.h"

#import "BarButtonItem.h"
#import "DocProcess.h"
#import "JobAuthViewController.h"
#import "JobInputsViewController.h"
#import "MainViewController.h"
#import "Media.h"
#import "MZFormSheetController.h"
#import "OAuth2Manager.h"
#import "PaperSize.h"
#import "PreviewPageView.h"
#import "Printer.h"
#import "PrinterBarViewController.h"
#import "PrintHistoryViewController.h"
#import "PrintJobManager.h"
#import "PrintOptionsViewController.h"
#import "Service.h"
#import "ServiceCapabilities.h"
#import "ServiceDescription.h"
#import "ShadowButton.h"
#import "SplashScreen.h"
#import "UIViewController+Container.h"

@interface PreviewViewController ()

@property (nonatomic, strong) UIViewController *containerController;
@property (nonatomic, strong) NSMutableDictionary *printOptions;
@property (nonatomic, strong) NSDictionary *jobInputs;
@property (nonatomic, strong) PaperSize *paperSize;
@property (nonatomic, assign) CGRect carouselPaperSize;
@property (nonatomic, assign) BOOL firstRender;
@property (nonatomic, assign) BOOL ignoreFirstActivate;

@end

@implementation PreviewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.document.delegate = self;

    // Setup the preview
    [self setupPreview];

    // Add PrinterBar view controller from our storyboard to the container view
    self.containerController = [self addViewControllerToContainer:self.printerBarContainer storyboardIdentifier:@"PrinterBar"];

    // Create initial print options
    [self generatePrintOptionsUsingOptions:self.document.reprint ? self.document.reprintOptions : nil];

    // Register for application activation so we can refresh service capabilities
    self.ignoreFirstActivate = self.wasOpenedIn;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkPrinterServiceCapabilities:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)dealloc
{
    // Re-register printer notifications to the main screen printer bar
    UIViewController *vc = [PrintJobManager sharedPrintJobManager].mainView.childViewControllers.firstObject;
    if ([vc isMemberOfClass:[PrinterBarViewController class]]) {
        PrinterBarViewController *printerBar = (PrinterBarViewController *)vc;
        [printerBar registerPrinterNotification];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updatePageLabel];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Print Preview Screen";

    if (self.wasOpenedIn) {
        self.wasOpenedIn = NO;
        [SplashScreen hide];
    }

    // First time the view was displayed we render the preview if needed
    if (self.firstRender) {
        [self renderDocument];
        self.firstRender = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    self.carouselPaperSize = [PaperSize fitPaperSize:self.paperSize toAreaOfSize:[self getCarouselContentSize]];
    [self.carousel reloadData];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PREVIEW", nil);
    [self.renderLabel setText:NSLocalizedPONString(@"LABEL_RENDERDOC", nil)];
    [self.noPreviewLabel setText:NSLocalizedPONString(@"LABEL_NOPREVIEW", nil)];
    [self.cancelButton setTitle:NSLocalizedPONString(@"LABEL_CANCEL", nil) forState:UIControlStateNormal];
    [self.printButton setTitle:NSLocalizedPONString(@"LABEL_PRINT", nil) forState:UIControlStateNormal];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.BackgroundColor"];
    [BarButtonItem customizeRightBarButton:self.printOptionsButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PreviewScreen.OptionsButton.Image"]]];

    self.renderView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.Render.BackgroundColor"];
    self.renderView.layer.borderWidth = 1.5f;
    self.renderView.layer.borderColor = [ThemeLoader colorForKey:@"PreviewScreen.Render.BorderColor"].CGColor;
    self.renderView.layer.cornerRadius = 10;
    self.renderView.layer.masksToBounds = NO;
    self.renderView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.renderView.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    self.renderView.layer.shadowOpacity = 0.75f;
    self.renderView.layer.shadowRadius = 2.0f;
    self.renderView.center = CGPointMake(self.backgroundView.frame.size.width / 2, self.bottomView.frame.origin.y / 2);
    self.renderLabel.textColor = [ThemeLoader colorForKey:@"PreviewScreen.Render.TextColor"];

    self.noPreviewLabel.center = CGPointMake(self.backgroundView.frame.size.width / 2, self.bottomView.frame.origin.y / 2);
    self.noPreviewLabel.textColor = [ThemeLoader colorForKey:@"PreviewScreen.NoPreviewLabel.TextColor"];

    self.pageLabel.layer.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.PageLabel.BackgroundColor"].CGColor;
    self.pageLabel.layer.borderWidth = 1.5f;
    self.pageLabel.layer.borderColor = [ThemeLoader colorForKey:@"PreviewScreen.PageLabel.BorderColor"].CGColor;
    self.pageLabel.layer.cornerRadius = self.pageLabel.frame.size.height / 2;
    self.pageLabel.layer.masksToBounds = NO;
    self.pageLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.pageLabel.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    self.pageLabel.layer.shadowOpacity = 0.75f;
    self.pageLabel.layer.shadowRadius = 2.0f;
    self.pageLabel.textColor = [ThemeLoader colorForKey:@"PreviewScreen.PageLabel.TextColor"];
    self.pageLabel.accessibilityIdentifier = @"Page Label";

    self.carousel.type = iCarouselTypeLinear;
    self.carousel.bounceDistance = 0.5f;
    self.carousel.decelerationRate = 0.7f;

    self.bottomView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.Bottom.BackgroundColor"];
    self.bottomView.layer.shadowOpacity = 0.75f;
    self.bottomView.layer.shadowRadius = 1.5f;
    self.bottomView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bottomView.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);

    [self.cancelButton setBackgroundColor:[UIColor colorWithRed:215/255.0 green:76/255.0 blue:69/255.0 alpha:1.0]];
    [self.cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.cancelButton.layer.cornerRadius = 4.0f;
    [self.printButton setBackgroundColor:[UIColor colorWithRed:74/255.0 green:209/255.0 blue:95/255.0 alpha:1.0]];
    [self.printButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.printButton.layer.cornerRadius = 4.0f;

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
}

- (void)setupPreview
{
    [self.renderView setHidden:YES];

    if ([self.document canBePreviewed]) {
        [self.noPreviewLabel setHidden:YES];

        if ([self.document previewIsLocalRendered] || [self.document previewIsServerRendered]) {
            self.firstRender = YES;
            if ([self.document renderBlocksMainUI]) [self didStartRendering];
            [self.printOptionsButton setEnabled:![self.document previewIsLocalRendered]];

            [self.carousel setHidden:YES];
            [self.pageLabel setHidden:YES];
        } else {
            [self.carousel setHidden:NO];
            [self.pageLabel setHidden:NO];
            [self.printOptionsButton setEnabled:YES];
        }
    } else {
        [self.noPreviewLabel setHidden:NO];
        [self.carousel setHidden:YES];
        [self.pageLabel setHidden:YES];
        [self.printOptionsButton setEnabled:YES];
    }
}

- (void)printerChangedNotification:(NSNotification *)notification
{
    [self.class validatePrintOptions:self.printOptions];

    if ([self generatePrintOptionsUsingOptions:self.printOptions] || [self.document previewIsServerRendered]) {

        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wundeclared-selector"
        if ([self.document respondsToSelector:@selector(updatedServiceURL:)]) {
            Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
            [self.document performSelector:@selector(updatedServiceURL:) withObject:[printer getDocAPIAddress]];
        }
        #pragma clang diagnostic pop

        [self renderDocument];
    }
}

- (void)checkPrinterServiceCapabilities:(NSNotification *)notification
{
    // Check to see if we need to update the Service Capabilities for the selected printers service
    if (self.ignoreFirstActivate) {
        self.ignoreFirstActivate = NO;
        return;
    }

    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    if (printer) {
        NSString *serviceURL = [DocProcess getBaseURLForService:[printer getDocAPIAddress].absoluteString];
        [ServiceDescription capabilitiesForServiceWithURL:[NSURL URLWithString:serviceURL] forceUpdate:NO completionBlock:nil];
    }
}

+ (void)validatePrintOptions:(NSMutableDictionary *)options
{
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];

    // Check to see if we need to change paper size option
    NSString *paperSize = options[@"poMediaSizeNum"];
    if (paperSize != nil) {
        BOOL found = NO;
        for (Media *media in [printer mediaSupported]) {
            if ([[media.mediaSizeNum stringValue] isEqualToString:paperSize]) {
                found = YES;
                break;
            }
        }
        if (!found) {
            [options removeObjectForKey:@"poMediaSizeNum"];
        }
    }

    // Check to see if we need to change duplex option
    NSString *duplex = options[@"poDuplex"];
    if (duplex != nil) {
        if ([printer doesSupportDuplex]) {
            NSString *duplexPermit = [printer.duplexPermit lowercaseString];
            if ([duplexPermit isEqualToString:@"duplex_only"] && [duplex isEqualToString:@"Simplex"]) {
                [options removeObjectForKey:@"poDuplex"];
            } else if ([duplexPermit isEqualToString:@"simplex_only"] && [duplex hasPrefix:@"Duplex"]) {
                [options removeObjectForKey:@"poDuplex"];
            }
        } else {
            [options removeObjectForKey:@"poDuplex"];
        }
    }

    // Check to see if we need to change color option
    NSString *color = options[@"poColor"];
    if (color != nil) {
        if ([printer doesSupportColor]) {
            NSString *colorPermit = [printer.colorPermit lowercaseString];
            if ([colorPermit isEqualToString:@"mono_only"] && [color isEqualToString:@"1"]) {
                [options removeObjectForKey:@"poColor"];
            } else if ([colorPermit isEqualToString:@"color_only"] && [color isEqualToString:@"0"]) {
                [options removeObjectForKey:@"poColor"];
            }
        } else {
            [options removeObjectForKey:@"poColor"];
        }
    }
}

- (BOOL)generatePrintOptionsUsingOptions:(NSMutableDictionary *)options
{
    BOOL requiresRender = NO;

    if (options == nil) {
        options = [self.document initialPrintOptions];
    }

    // Printer specific options
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    if (printer) {
        // Paper Size
        BOOL mediaSizeExists = options[@"poMediaSizeNum"] != nil;
        int mediaNum = mediaSizeExists ? [PaperSize getMediaSizeNumberFromPrintOptions:options] : [printer getDefaultPaperSizeNum];
        if (mediaNum != -1) {
            if (!mediaSizeExists) [options setValue:[NSString stringWithFormat:@"%d", mediaNum] forKey:@"poMediaSizeNum"];
            PaperSize *tempPaperSize = [PaperSize getPaperSizeFromPrintOptions:options];
            if (!CGSizeEqualToSize(tempPaperSize.size, self.paperSize.size)) {
                requiresRender = YES;
            }
        }

        // Duplex
        if ([printer doesSupportDuplex]) {
            BOOL duplexExists = options[@"poDuplex"] != nil;
            if (!duplexExists) {
                NSString *duplexPermit = [printer.duplexPermit lowercaseString];
                if ([duplexPermit isEqualToString:@"duplex_only"] || [duplexPermit isEqualToString:@"duplex_default"]) {
                    [options setValue:@"DuplexLong" forKey:@"poDuplex"];
                } else if ([duplexPermit isEqualToString:@"simplex_only"] || [duplexPermit isEqualToString:@"simplex_default"]) {
                    [options setValue:@"Simplex" forKey:@"poDuplex"];
                }
            }
        }

        // Color
        if ([printer doesSupportColor]) {
            BOOL colorExists = options[@"poColor"] != nil;
            if (!colorExists) {
                NSString *colorPermit = [printer.colorPermit lowercaseString];
                if ([colorPermit isEqualToString:@"color_only"] || [colorPermit isEqualToString:@"color_default"]) {
                    [options setValue:@"1" forKey:@"poColor"];
                } else if ([colorPermit isEqualToString:@"mono_only"] || [colorPermit isEqualToString:@"mono_default"]) {
                    [options setValue:@"0" forKey:@"poColor"];
                }
            }
        }
    }

    PaperSize *newPaperSize = [PaperSize getPaperSizeFromPrintOptions:options];
    if (!CGSizeEqualToSize(newPaperSize.size, self.paperSize.size)) {
        requiresRender = YES;
    }
    self.paperSize = newPaperSize;
    self.carouselPaperSize = [PaperSize fitPaperSize:self.paperSize toAreaOfSize:[self getCarouselContentSize]];

    self.printOptions = options;

    return requiresRender;
}

- (IBAction)openPrintOptions:(id)sender {
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"PrintOptions"];
    if ([navController.topViewController isKindOfClass:[PrintOptionsViewController class]]) {
        PrintOptionsViewController  *optionsController = (PrintOptionsViewController *)navController.topViewController;
        optionsController.document = self.document;
        optionsController.printOptions = self.printOptions;

        [optionsController.view setFrame:CGRectMake(0, 0, 304, 284)];
        [optionsController.view setNeedsLayout];
        [optionsController.view layoutIfNeeded];

        MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:navController];
        formSheet.presentedFormSheetSize = CGSizeMake(optionsController.view.frame.size.width, optionsController.view.frame.size.height + navController.navigationBar.frame.size.height);
        formSheet.shouldDismissOnBackgroundViewTap = YES;
        formSheet.landscapeTopInset = 52;
        formSheet.portraitTopInset = 52;
        formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsMoveToTop;
        formSheet.transitionStyle = MZFormSheetTransitionStyleDropDown;

        formSheet.didDismissCompletionHandler = ^(UIViewController *presentedController) {
            if ([self generatePrintOptionsUsingOptions:[optionsController generatePrintOptions]]) {
                [self renderDocument];
            }
        };

        [self mz_presentFormSheetController:formSheet animated:YES completionHandler:nil];
    }
}

- (void)renderDocument
{
    if ([self.document canBePreviewed]) {
        if ([self.document previewIsLocalRendered] || [self.document previewIsServerRendered]) {
            [self.document renderDocumentWithPrintOptions:self.printOptions];
        } else {
            [self.carousel reloadData];
            [self updatePageRange];
            [self.printOptionsButton setEnabled:YES];
        }
    } else {
        [self.printOptionsButton setEnabled:YES];
    }
}

- (void)updatePageRange
{
    // When we re-render make sure we correct any page range issues detected
    if (self.printOptions && [self printOptions][@"poPageRange"] != nil) {
        unsigned long maxPages = [self.document getPageCount];
        if (maxPages == 1) {
            [self.printOptions removeObjectForKey:@"poPageRange"];
        } else {
            NSScanner *scanner = [NSScanner scannerWithString:[self printOptions][@"poPageRange"]];
            NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            int lower;
            [scanner scanInt:&lower];
            int upper;
            [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
            [scanner scanInt:&upper];

            if (upper > maxPages) upper = (int)maxPages;
            if (lower > upper) lower = upper;

            [self.printOptions setValue:[NSString stringWithFormat:@"%d-%d", lower, upper] forKey:@"poPageRange"];
        }
    }
}

- (IBAction)printDocument
{
    [self.printButton setEnabled:NO];

    // Check that a printer is selected
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    if (!printer) {
        [self.printButton setEnabled:YES];
        return;
    }

    // Check that the printer has a valid docAPI URL to submit too
    NSString *docApiURL = [[[printer getDocAPIAddress] absoluteString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (docApiURL == nil || [docApiURL length] == 0) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_MISSING_DOCAPIURL", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:cancelAction];

        [self presentViewController:alertVC animated:YES completion:nil];

        [self.printButton setEnabled:YES];
        return;
    }

    // Check if printing is restricted to a service
    Service *restricted = [Service getRestrictedService];
    if (restricted) {
        NSURL *URL = [printer getDocAPIAddress];
        NSString *serviceURL = [DocProcess getBaseURLForService:URL.absoluteString];

        if (![Service compareURL:serviceURL toURL:restricted.serviceURL]) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_RESTRICTED", nil) preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [self presentViewController:alertVC animated:YES completion:nil];

            [self.printButton setEnabled:YES];
            return;
        }
    }

    // Check if the printer requires authentication
    if ([printer requiresAuthToPrint]) {
        UserAccount *userAccount = [UserAccount getUserAccountForURL:[printer getDocAPIAddress]];
        if (userAccount && [userAccount.isAnonymous boolValue]) {
            JobAuthViewController *destViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"printerUserAuth"];
            destViewController.delegate = self;

            [self pushToViewController:destViewController fromViewController:self];
            [self.printButton setEnabled:YES];
            return;
        }
    }

    // Check if the printer has any job accounting inputs to be entered
    if ([JobAccountingInputs printerRequiresJobInputs:printer]) {
        if (self.jobInputs == nil) {
            JobInputsViewController *destViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"printerJobInputs"];
            destViewController.delegate = self;
            destViewController.printer = printer;
            
            [self pushToViewController:destViewController fromViewController:self];
            [self.printButton setEnabled:YES];
            return;
        }
    }

    [[PrintJobManager sharedPrintJobManager] createPrintJob:printer withDocument:self.document withOptions:self.printOptions withInputs:self.jobInputs ? : [JobAccountingInputs generatePopulatedInputs:printer]];

    [self returnHome];
}

- (IBAction)returnHome
{
    if (![self popToViewControllerOfClass:[MainViewController class] animated:YES]) {
        if (![self popToViewControllerOfClass:[PrintHistoryViewController class] animated:YES]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (BOOL)popToViewControllerOfClass:(Class)vcClass animated:(BOOL)animated
{
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        if ([vc isMemberOfClass:vcClass]) {
            [self.navigationController popToViewController:vc animated:animated];
            return YES;
        }
    }

    return NO;
}

- (void)pushToViewController:(UIViewController *)pushController fromViewController:(UIViewController *)fromController
{
    // Create an empty mutable array that will be used as a new stack of view controllers
    NSMutableArray *newControllers = [NSMutableArray array];

    // Go through the stack of current view controllers and add them to the new stack until we get to the base "from"
    // view controller.  Any view controllers between the "from" controller and the "push" controller are removed.
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        [newControllers addObject:vc];
        if ([vc isEqual:fromController]) {
            break;
        }
    }

    // Add to the top the view controller we want to transition too
    [newControllers addObject:pushController];

    // Perform the transition
    [self.navigationController setViewControllers:newControllers animated:YES];
}

- (void)updatePageLabel
{
    if ([self.document canBePreviewed]) {
        NSString *newLabel = [NSString stringWithFormat:@"%@ %ld / %ld", NSLocalizedPONString(@"LABEL_PAGE", nil), (long)self.carousel.currentItemIndex + 1, (long)self.carousel.numberOfItems];
        CGSize newSize = [newLabel sizeWithAttributes:@{NSFontAttributeName: self.pageLabel.font}];
        self.pageLabel.frame = CGRectMake((self.carousel.frame.size.width / 2) - ((newSize.width + 32) / 2), self.pageLabel.frame.origin.y, newSize.width + 32, self.pageLabel.frame.size.height);
        self.pageLabel.text = newLabel;
    }
}

- (CGSize)getCarouselContentSize
{
    // Return the maximum content area of the carousel to be rendered into
    float width = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? 530.0f : 784.0f : 254.0f;
    float height = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? 784.0f : 530.0f : IS_SCREEN_R4 ? 328.0f : 240.0f;
    return CGSizeMake(width, height);
}

#pragma mark - iCarouselDataSource

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.document getPageCount];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    //Create new view if no view is available for recycling
    if (view == nil) {
        view = [[PreviewPageView alloc] initWithFrame:self.carouselPaperSize];
    }

    if ([view isMemberOfClass:[PreviewPageView class]]) {
        // Get the preview image for the page and set it
        PreviewPageView *previewPage = (PreviewPageView *)view;
        UIImage *newImage = [self.document getPagePreviewAtIndex:index withSize:view.bounds.size forPaperSize:self.paperSize.size];
        [previewPage.pageImage setImage:newImage];

        // Show/hide the activity indicator
        if (newImage) {
            [previewPage stopActivityIndicator];
        } else {
            [previewPage startActivityIndicator];
        }
    }

    return view;
}

#pragma mark - iCarouselDelegate

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    [self updatePageLabel];
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing) {
        return value * 1.05f;
    }
    return value;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    return self.carouselPaperSize.size.width;
}

#pragma mark - JobAccountingInputsDelegate

- (void)didFinishWithJobInputs:(NSDictionary *)inputs
{
    self.jobInputs = inputs;
    [self printDocument];
}

#pragma mark - PrintDocumentDelegate

- (void)didStartRendering
{
    [self.printOptionsButton setEnabled:NO];
    [self.containerController.view setUserInteractionEnabled:NO];
    [self.printButton setUserInteractionEnabled:NO];

    [self.carousel setHidden:YES];
    [self.pageLabel setHidden:YES];
    [self.renderView setHidden:NO];
}

- (void)didFinishRendering
{
    BOOL preview = [self.document getPageCount] > 0;

    [self.carousel reloadData];
    [self updatePageRange];
    [self updatePageLabel];

    [self.renderView setHidden:YES];
    [self.noPreviewLabel setHidden:preview];
    [self.carousel setHidden:!preview];
    [self.pageLabel setHidden:!preview];

    [self.printOptionsButton setEnabled:YES];
    [self.containerController.view setUserInteractionEnabled:YES];
    [self.printButton setUserInteractionEnabled:YES];
}

- (void)didStartPreviewing
{
    [self.printOptionsButton setEnabled:YES];
    [self.containerController.view setUserInteractionEnabled:YES];
    [self.printButton setUserInteractionEnabled:YES];

    [self.carousel setHidden:YES];
    [self.pageLabel setHidden:YES];
    [self.renderView setHidden:NO];
}

- (void)didFinishPreviewing
{
    BOOL preview = [self.document getPageCount] > 0;

    [self.carousel reloadData];
    [self updatePageRange];
    [self updatePageLabel];

    [self.renderView setHidden:YES];
    [self.noPreviewLabel setHidden:preview];
    [self.carousel setHidden:!preview];
    [self.pageLabel setHidden:!preview];
}

- (void)setItemImage:(UIImage *)image atIndex:(NSInteger)index
{
    UIView *view = [self.carousel itemViewAtIndex:index];

    if ([view isMemberOfClass:[PreviewPageView class]]) {
        PreviewPageView *previewPage = (PreviewPageView *)view;
        [previewPage.pageImage setImage:image];
    }
}

- (void)startActivityAtIndex:(NSInteger)index
{
    UIView *view = [self.carousel itemViewAtIndex:index];

    if ([view isMemberOfClass:[PreviewPageView class]]) {
        PreviewPageView *previewPage = (PreviewPageView *)view;
        [previewPage startActivityIndicator];
    }
}

- (void)endActivityAtIndex:(NSInteger)index
{
    UIView *view = [self.carousel itemViewAtIndex:index];
    
    if ([view isMemberOfClass:[PreviewPageView class]]) {
        PreviewPageView *previewPage = (PreviewPageView *)view;
        [previewPage stopActivityIndicator];
    }
}

#pragma mark - UserAccountDelegate

- (void)didFinishWithUserAccount:(UserAccount *)account
{
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];

    // Get the service URL that is used for submitting documents to
    NSURL *serviceURL = [printer getDocAPIAddress];
    NSString *URL = [DocProcess getBaseURLForService:serviceURL.absoluteString];

    // Get the Service if it already exists or create it
    BOOL publicHosted = [Service isHostedURL:serviceURL andPublic:YES];
    Service *service = nil;
    if (publicHosted) {
        service = [Service hostedService];
    } else {
        NSArray *results = [Service doesServiceExist:URL inContext:nil];
        service = (results && ([results count] == 0)) ? [Service createService:URL description:nil isDefault:NO isLocked:NO isMDM:NO isRestricted:NO completionBlock:nil] : results[0];
    }

    void (^popViewBlock)(GTMOAuth2Authentication *auth) = ^(GTMOAuth2Authentication *auth) {
        if (publicHosted) {
            [UserAccount removeHostedDefault:nil];
            [account setHostedDefault:nil];
        } else {
            NSMutableSet *services = [NSMutableSet setWithSet:account.serviceDefaults];
            if (service && ![services containsObject:service]) {
                [services addObject:service];
                [account setServiceDefaults:services inContext:nil];
            }
        }

        if (auth) {
            [account saveOAuth2Authentication:auth forService:URL];
        }

        [self printDocument];
    };

    if ([ServiceCapabilities serviceSupportsOAuthThirdParty:service.serviceURL]) {
        [self showThirdPartyOAuthLogin:service.serviceURL completionHandler:popViewBlock];
        return;
    }

    popViewBlock(nil);
}

- (void)showThirdPartyOAuthLogin:(NSString *)serviceURL completionHandler:(void (^)(GTMOAuth2Authentication *auth))handler
{
    __weak __typeof(self) weakSelf = self;
    GTMOAuth2ViewControllerCompletionHandler GTMOHandler = ^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error)
    {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        BOOL userCancelled = error && ([error.domain isEqualToString:kGTMOAuth2ErrorDomain] && error.code == GTMOAuth2ErrorWindowClosed);
        if (userCancelled) {
            return;
        }

        if (error) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];

            [strongSelf presentViewController:alertVC animated:YES completion:nil];
            return;
        }

        handler(auth);
    };

    GTMOAuth2ViewControllerTouch *vc = [OAuth2Manager viewControllerForPON:GTMOHandler serviceURL:serviceURL];
    vc.navigationItem.title = self.navigationItem.title;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
