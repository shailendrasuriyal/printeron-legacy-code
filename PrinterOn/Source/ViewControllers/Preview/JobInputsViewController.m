//
//  JobInputsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "JobInputsViewController.h"

#import "BarButtonItem.h"
#import "ClientUIDInput.h"
#import "CSLinearLayoutView.h"
#import "EmailAddressInput.h"
#import "MainViewController.h"
#import "NetworkLoginInput.h"
#import "NSString+Email.h"
#import "Printer.h"
#import "PrintJobManager.h"
#import "ReleaseCodeInput.h"
#import "SessionMetadataInput.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UserAccount.h"

@interface JobInputsViewController ()

@property (nonatomic, strong) CSLinearLayoutView *linearLayout;
@property (nonatomic, strong) CSLinearLayoutItem *descriptionItem;
@property (nonatomic, strong) CSLinearLayoutItem *emailItem;
@property (nonatomic, strong) CSLinearLayoutItem *clientItem;
@property (nonatomic, strong) CSLinearLayoutItem *networkItem;
@property (nonatomic, strong) CSLinearLayoutItem *sessionItem;
@property (nonatomic, strong) CSLinearLayoutItem *releaseItem;

@end

@implementation JobInputsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.emailText];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.clientText];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.networkText];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.sessionText];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.releaseText];

    // Set the job inputs
    [self updateJobInputs];
    [self validateFields];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Job Inputs Screen";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.emailText];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.clientText];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.networkText];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.sessionText];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.releaseText];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"LABEL_PRINT", nil);
}

- (void)setupTheme
{
    [BarButtonItem customizeRightBarButton:self.selectButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.SelectButton.Image"]]];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.BackgroundColor"];

    THEME_KEYBOARD(self.emailText);
    THEME_KEYBOARD(self.clientText);
    THEME_KEYBOARD(self.networkText);
    THEME_KEYBOARD(self.sessionText);
    THEME_KEYBOARD(self.releaseText);
}

- (NSDictionary *)generateInputs
{
    NSMutableDictionary *options = [NSMutableDictionary dictionary];

    if (![self.emailView isHidden] && [self.emailText.text length] > 0) {
        [options setValue:self.emailText.text forKey:kJobInputTypeEmailAddressKey];
    }

    if (![self.clientView isHidden] && [self.clientText.text length] > 0) {
        [options setValue:self.clientText.text forKey:kJobInputTypeClientUIDKey];
    }

    if (![self.networkView isHidden] && [self.networkText.text length] > 0) {
        [options setValue:self.networkText.text forKey:kJobInputTypeNetworkLoginKey];
    }

    if (![self.sessionView isHidden] && [self.sessionText.text length] > 0) {
        [options setValue:self.sessionText.text forKey:kJobInputTypeSessionMetadataKey];
    }

    if (![self.releaseView isHidden] && [self.releaseText.text length] > 0) {
        [options setValue:self.releaseText.text forKey:kJobInputTypeReleaseCodeKey];
    }

    return options;
}

- (IBAction)printPressed
{
    if (self.delegate) [self.delegate didFinishWithJobInputs:[self generateInputs]];
}

- (void)returnHome
{
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        if ([vc isMemberOfClass:[MainViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}

- (void)updateJobInputs
{
    // Initialize Linear Layout
    if (!self.linearLayout) {
        self.linearLayout = [[CSLinearLayoutView alloc] initWithFrame:self.view.bounds];
        self.linearLayout.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.linearLayout.autoAdjustFrameSize = YES;
        self.linearLayout.orientation = CSLinearLayoutViewOrientationVertical;
    } else {
        [self.linearLayout removeAllItems];
    }

    // Add Description View
    NSString *descText = NSLocalizedPONString(@"LABEL_REQUIRESINPUTS", nil);
    [self resizeLabel:self.descriptionLabel withText:descText inView:self.descriptionView addHeight:9];

    self.descriptionItem = [self setupItem:self.descriptionItem withView:self.descriptionView withPadding:CSLinearLayoutMakePadding(10, 8, 12, 9)];
    [self.linearLayout addItem:self.descriptionItem];

    // Add Email Address View
    UserAccount *account;
    BOOL emailPopulated = NO;

    EmailAddressInput *emailInput = self.printer.jobAccountingInputs.emailAddress;
    if (emailInput && [JobAccountingInputs isJobInputStringRequiredOrOptional:emailInput.use]) {
        [self.emailLabel setText:emailInput.label];
        
        if (!account) account = [UserAccount getUserAccountForURL:[self.printer getDocAPIAddress]];
        if (account && ![account.userName isEqualToString:[UserAccount anonymousUser].userName] && [account.userName isEmailAddress]) {
            [self.emailText setText:account.userName];
            emailPopulated = YES;
        } else {
            [self.emailText setText:emailInput.defaultValue];
        }

        [self.emailText setPlaceholder:[JobAccountingInputs placeholderForInputString:emailInput.use]];

        self.emailItem = [self setupItem:self.emailItem withView:self.emailView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.emailItem];
    } else {
        [self.emailView setHidden:YES];
    }

    // Add Network Login View
    NetworkLoginInput *networkInput = self.printer.jobAccountingInputs.networkLogin;
    if (networkInput && [JobAccountingInputs isJobInputStringRequiredOrOptional:networkInput.use]) {
        [self.networkLabel setText:networkInput.label];
    
        if (!account) account = [UserAccount getUserAccountForURL:[self.printer getDocAPIAddress]];
        if (account && !emailPopulated && ![account.userName isEqualToString:[UserAccount anonymousUser].userName]) {
            [self.networkText setText:account.userName];
        } else {
            [self.networkText setText:networkInput.defaultValue];
        }

        [self.networkText setPlaceholder:[JobAccountingInputs placeholderForInputString:networkInput.use]];

        self.networkItem = [self setupItem:self.networkItem withView:self.networkView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.networkItem];
    } else {
        [self.networkView setHidden:YES];
    }

    // Add Client UID View
    ClientUIDInput *clientInput = self.printer.jobAccountingInputs.clientUID;
    if (clientInput && [JobAccountingInputs isJobInputStringRequiredOrOptional:clientInput.use]) {
        [self.clientLabel setText:clientInput.label];
        [self.clientText setText:clientInput.defaultValue];
        [self.clientText setPlaceholder:[JobAccountingInputs placeholderForInputString:clientInput.use]];
        [self.clientText setSecureTextEntry:[clientInput.secure boolValue]];
        
        self.clientItem = [self setupItem:self.clientItem withView:self.clientView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.clientItem];
    } else {
        [self.clientView setHidden:YES];
    }

    // Add Session Metadata View
    SessionMetadataInput *sessionInput = self.printer.jobAccountingInputs.sessionMetadata;
    if (sessionInput && [JobAccountingInputs isJobInputStringRequiredOrOptional:sessionInput.use]) {
        [self.sessionLabel setText:sessionInput.label];
        [self.sessionText setText:sessionInput.defaultValue];
        [self.sessionText setPlaceholder:[JobAccountingInputs placeholderForInputString:sessionInput.use]];
        [self.sessionText setSecureTextEntry:[sessionInput.secure boolValue]];
        
        self.sessionItem = [self setupItem:self.sessionItem withView:self.sessionView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.sessionItem];
    } else {
        [self.sessionView setHidden:YES];
    }

    // Add Release Code View
    ReleaseCodeInput *releaseInput = self.printer.jobAccountingInputs.releaseCode;
    if (releaseInput && [JobAccountingInputs isJobInputStringRequiredOrOptional:releaseInput.use] && ![releaseInput.autoCreate boolValue]) {
        [self.releaseLabel setText:releaseInput.label];
        [self.releaseText setPlaceholder:[JobAccountingInputs placeholderForInputString:releaseInput.use]];
        if ([[releaseInput.type lowercaseString] isEqualToString:@"numeric"]) {
            [self.releaseText setKeyboardType:UIKeyboardTypeDecimalPad];
        }

        int minLength = [releaseInput.minLength intValue];
        int maxLength = [releaseInput.maxLength intValue];
        if (minLength == maxLength) {
            [self.releaseDesc setText:[NSString stringWithFormat:@"(%@ %d, %@)", NSLocalizedPONString(@"LABEL_LENGTH", nil), maxLength, [JobAccountingInputs releaseCodeTypeString:releaseInput.type]]];
        } else {
            [self.releaseDesc setText:[NSString stringWithFormat:@"(%@ %d-%d, %@)", NSLocalizedPONString(@"LABEL_LENGTH", nil), minLength, maxLength, [JobAccountingInputs releaseCodeTypeString:releaseInput.type]]];
        }

        self.releaseItem = [self setupItem:self.releaseItem withView:self.releaseView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
        [self.linearLayout addItem:self.releaseItem];
    } else {
        [self.releaseView setHidden:YES];
    }

    // Add linear layout to the scrollview
    [self.scrollView addSubview:self.linearLayout];

    [self updateContentHeight];
}

- (CSLinearLayoutItem *)setupItem:(CSLinearLayoutItem *)item withView:(UIView *)view withPadding:(CSLinearLayoutItemPadding)padding
{
    if (!item) {
        item = [CSLinearLayoutItem layoutItemForView:view];
        item.padding = padding;
        item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    }
    [view setHidden:NO];
    return item;
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text inView:(UIView *)view
{
    [self resizeLabel:label withText:text inView:view addHeight:0];
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text inView:(UIView *)view addHeight:(NSUInteger)add
{
    // Resize the label to hold the text
    [label setText:text];
    CGSize maxSize = CGSizeMake(label.frame.size.width, 2000);
    CGSize requiredSize = [label sizeThatFits:maxSize];
    
    if (requiredSize.height > label.frame.size.height) {
        [label setFrame:CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, requiredSize.height)];
        
        // Resize the view the label is inside to fit the new size
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, label.frame.origin.y + label.frame.size.height + add)];
    }
}

- (void)updateContentHeight
{
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.linearLayout.frame.size.height);
}

- (void)validateFields
{
    [self.selectButtonItem setEnabled:[self areFieldsValid]];
}

- (BOOL)areFieldsValid
{
    JobAccountingInputs *inputs = self.printer.jobAccountingInputs;

    if (![self.emailView isHidden]) {
        BOOL required = [JobAccountingInputs isJobInputStringRequired:inputs.emailAddress.use];
        if (required && ![self.emailText.text isEmailAddress]) {
            return NO;
        } else if (!required && [self.emailText.text length] > 0 && ![self.emailText.text isEmailAddress]) {
            return NO;
        }
    }

    if (![self.clientView isHidden]) {
        BOOL required = [JobAccountingInputs isJobInputStringRequired:inputs.clientUID.use];
        if (required && [self.clientText.text length] == 0) {
            return NO;
        }
    }

    if (![self.networkView isHidden]) {
        BOOL required = [JobAccountingInputs isJobInputStringRequired:inputs.networkLogin.use];
        if (required && [self.networkText.text length] == 0) {
            return NO;
        }
    }

    if (![self.sessionView isHidden]) {
        BOOL required = [JobAccountingInputs isJobInputStringRequired:inputs.sessionMetadata.use];
        if (required && [self.sessionText.text length] == 0) {
            return NO;
        }
    }

    if (![self.releaseView isHidden]) {
        BOOL required = [JobAccountingInputs isJobInputStringRequired:inputs.releaseCode.use];

        NSString *pattern = [JobAccountingInputs releaseCodePatternForPrinter:self.printer];
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];

        NSUInteger numOfMatches = [regex numberOfMatchesInString:self.releaseText.text options:0 range:NSMakeRange(0, [self.releaseText.text length])];

        if (required && numOfMatches == 0) {
            return NO;
        } else if (!required && [self.releaseText.text length] > 0 && numOfMatches == 0) {
            return NO;
        }
    }

    return YES;
}

- (void)textChanged:(NSNotification *)notification
{
    [self validateFields];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [self.scrollView textFieldShouldReturn:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    int MAXLENGTH = [self.printer.jobAccountingInputs.releaseCode.maxLength intValue];
    NSUInteger newLength = [textField.text length] - range.length + [string length];
    if (newLength >= MAXLENGTH) {
        textField.text = [[textField.text stringByReplacingCharactersInRange:range withString:string] substringToIndex:MAXLENGTH];
        return NO;
    }
    return YES;
}

@end
