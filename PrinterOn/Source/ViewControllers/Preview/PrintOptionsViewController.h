//
//  PrintOptionsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 1/25/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PrintOptionsTableViewController.h"

@class PrintDocument;

@interface PrintOptionsViewController : BaseViewController <PrintOptionsTableDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIView *copiesView;
@property (weak, nonatomic) IBOutlet UILabel *copiesLabel;
@property (weak, nonatomic) IBOutlet UIStepper *copiesStepper;

@property (weak, nonatomic) IBOutlet UIView *rangeStepperView;
@property (weak, nonatomic) IBOutlet UILabel *rangeStepperLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switchAllPages;

@property (weak, nonatomic) IBOutlet UIView *rangeSelectView;
@property (weak, nonatomic) IBOutlet UITextField *textLowerNumber;
@property (weak, nonatomic) IBOutlet UITextField *textUpperNumber;
@property (weak, nonatomic) IBOutlet UIStepper *stepperLower;
@property (weak, nonatomic) IBOutlet UIStepper *stepperUpper;

@property (weak, nonatomic) IBOutlet UIView *paperSizeView;
@property (weak, nonatomic) IBOutlet UILabel *paperSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *paperSizeChosenLabel;

@property (weak, nonatomic) IBOutlet UIView *duplexView;
@property (weak, nonatomic) IBOutlet UILabel *duplexLabel;
@property (weak, nonatomic) IBOutlet UILabel *duplexChosenLabel;

@property (weak, nonatomic) IBOutlet UIView *collateView;
@property (weak, nonatomic) IBOutlet UILabel *collateLabel;
@property (weak, nonatomic) IBOutlet UISwitch *collateSwitch;

@property (weak, nonatomic) IBOutlet UIView *colorView;

@property (weak, nonatomic) IBOutlet UIView *orientationView;
@property (weak, nonatomic) IBOutlet UILabel *orientationLabel;
@property (weak, nonatomic) IBOutlet UILabel *orientationChosenLabel;

@property (weak, nonatomic) IBOutlet UIView *worksheetView;
@property (weak, nonatomic) IBOutlet UILabel *worksheetLabel;
@property (weak, nonatomic) IBOutlet UILabel *worksheetChosenLabel;

@property (nonatomic, strong) PrintDocument *document;
@property (nonatomic, strong) NSMutableDictionary *printOptions;

- (NSMutableDictionary *)generatePrintOptions;

@end
