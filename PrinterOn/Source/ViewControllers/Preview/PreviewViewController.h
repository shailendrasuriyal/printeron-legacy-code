//
//  PreviewViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 11/21/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "iCarousel.h"
#import "JobAccountingInputs.h"
#import "PrintDocument.h"
#import "UserAccount.h"

@class ShadowButton;

@interface PreviewViewController : BaseViewController <iCarouselDataSource, iCarouselDelegate, JobAccountingInputsDelegate, PrintDocumentDelegate, UserAccountDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *printOptionsButton;

@property (weak, nonatomic) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UILabel *pageLabel;

@property (weak, nonatomic) IBOutlet UIView *renderView;
@property (weak, nonatomic) IBOutlet UILabel *renderLabel;
@property (weak, nonatomic) IBOutlet UILabel *noPreviewLabel;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *printerBarContainer;
@property (weak, nonatomic) IBOutlet ShadowButton *cancelButton;
@property (weak, nonatomic) IBOutlet ShadowButton *printButton;

@property (nonatomic, strong) PrintDocument *document;
@property (nonatomic, assign) BOOL wasOpenedIn;

+ (void)validatePrintOptions:(NSMutableDictionary *)options;

@end
