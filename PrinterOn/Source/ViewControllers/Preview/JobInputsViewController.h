//
//  JobInputsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2/15/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "JobAccountingInputs.h"

@class Printer, TPKeyboardAvoidingScrollView;

@interface JobInputsViewController : BaseViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *emailView;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailText;

@property (weak, nonatomic) IBOutlet ShadowView *clientView;
@property (weak, nonatomic) IBOutlet UILabel *clientLabel;
@property (weak, nonatomic) IBOutlet UITextField *clientText;

@property (weak, nonatomic) IBOutlet ShadowView *networkView;
@property (weak, nonatomic) IBOutlet UILabel *networkLabel;
@property (weak, nonatomic) IBOutlet UITextField *networkText;

@property (weak, nonatomic) IBOutlet ShadowView *sessionView;
@property (weak, nonatomic) IBOutlet UILabel *sessionLabel;
@property (weak, nonatomic) IBOutlet UITextField *sessionText;

@property (weak, nonatomic) IBOutlet ShadowView *releaseView;
@property (weak, nonatomic) IBOutlet UILabel *releaseLabel;
@property (weak, nonatomic) IBOutlet UITextField *releaseText;
@property (weak, nonatomic) IBOutlet UILabel *releaseDesc;

@property (weak, nonatomic) IBOutlet ShadowView *descriptionView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (nonatomic, weak) id <JobAccountingInputsDelegate> delegate;
@property (nonatomic, strong) Printer *printer;

@end
