//
//  JobAuthViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 05/27/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "JobAuthViewController.h"

#import "AccountCell.h"
#import "CSLinearLayoutView.h"
#import "JobInputsViewController.h"
#import "UserSetupViewController.h"

@interface JobAuthViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) AccountCell *createAccountCell;

@property (nonatomic, strong) CSLinearLayoutView *linearLayout;
@property (nonatomic, strong) CSLinearLayoutItem *descriptionItem;
@property (nonatomic, strong) CSLinearLayoutItem *tableItem;

@end

@implementation JobAuthViewController

- (void)viewDidLoad
{
    self.createAccountCell = [self.tableView dequeueReusableCellWithIdentifier:@"createAccountCell"];
    [self.createAccountCell setupCell:NSLocalizedPONString(@"LABEL_ADDACCOUNT", nil) withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.AddButton.Dark.Image"]] showLock:NO];
    [self.tableView setTableHeaderView:self.createAccountCell.contentView];

    [super viewDidLoad];
    [self registerForEnterForegroundNotification];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateContentHeight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenName = @"Job Accounts Screen";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateContentHeight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)customEnterForeground
{
    // Refresh the fetch controller and table
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    [self updateTableView];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"LABEL_USERCREDENTIALS", nil);
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.BackgroundColor"];
}

- (void)updateAccounts
{
    // Initialize Linear Layout
    if (!self.linearLayout) {
        self.linearLayout = [[CSLinearLayoutView alloc] initWithFrame:self.view.bounds];
        self.linearLayout.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.linearLayout.autoAdjustFrameSize = YES;
        self.linearLayout.orientation = CSLinearLayoutViewOrientationVertical;
    } else {
        [self.linearLayout removeAllItems];
    }

    // Add Description View
    NSString *descText = NSLocalizedPONString(@"LABEL_REQUIRES_PRINTAUTH", nil);
    [self resizeLabel:self.descriptionLabel withText:descText inView:self.descriptionView addHeight:9];

    self.descriptionItem = [self setupItem:self.descriptionItem withView:self.descriptionView withPadding:CSLinearLayoutMakePadding(10, 8, 12, 9)];
    [self.linearLayout addItem:self.descriptionItem];

    // Add Table View
    self.tableItem = [self setupItem:self.tableItem withView:self.contentView withPadding:CSLinearLayoutMakePadding(0, 8, 12, 9)];
    [self.linearLayout addItem:self.tableItem];

    // Add linear layout to the scrollview
    [self.scrollView addSubview:self.linearLayout];

    [self updateContentHeight];
}

- (CSLinearLayoutItem *)setupItem:(CSLinearLayoutItem *)item withView:(UIView *)view withPadding:(CSLinearLayoutItemPadding)padding
{
    if (!item) {
        item = [CSLinearLayoutItem layoutItemForView:view];
        item.padding = padding;
        item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    }
    [view setHidden:NO];
    return item;
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text inView:(UIView *)view
{
    [self resizeLabel:label withText:text inView:view addHeight:0];
}

- (void)resizeLabel:(UILabel *)label withText:(NSString *)text inView:(UIView *)view addHeight:(NSUInteger)add
{
    // Resize the label to hold the text
    [label setText:text];
    CGSize maxSize = CGSizeMake(label.frame.size.width, 2000);
    CGSize requiredSize = [label sizeThatFits:maxSize];
    
    if (requiredSize.height > label.frame.size.height) {
        [label setFrame:CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, requiredSize.height)];
        
        // Resize the view the label is inside to fit the new size
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, label.frame.origin.y + label.frame.size.height + add)];
    }
}

- (void)UserAccountSelected:(UserAccount *)account
{
    if (self.delegate) [self.delegate didFinishWithUserAccount:account];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"UserAccount"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"accountDescription" ascending:YES]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;
        
        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.tableView.contentSize.height)];
    [self updateAccounts];
}

- (void)updateContentHeight
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.linearLayout.frame.size.height)];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self updateTableView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"userAccountCell";
    AccountCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UserAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCell:account.accountDescription withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"UserAccounts.User.Image"]] showLock:NO];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self UserAccountSelected:account];
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"setupAccount"]) {
        UserSetupViewController *destViewController = segue.destinationViewController;
        destViewController.delegate = self.delegate;
        destViewController.hideServices = YES;
    }
}

@end
