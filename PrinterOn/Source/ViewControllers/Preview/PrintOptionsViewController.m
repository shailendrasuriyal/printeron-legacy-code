//
//  PrintOptionsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 1/25/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PrintOptionsViewController.h"

#import "BarButtonItem.h"
#import "CSLinearLayoutView.h"
#import "HMSegmentedControl.h"
#import "MZFormSheetController.h"
#import "PaperSize.h"
#import "PrintDocument.h"
#import "Printer.h"
#import "SpreadSheetDocument.h"

@interface PrintOptionsViewController ()

@property (nonatomic, strong) HMSegmentedControl *segmentColor;
@property (nonatomic, strong) CSLinearLayoutView *linearLayout;
@property (nonatomic, strong) CSLinearLayoutItem *copiesItem;
@property (nonatomic, strong) CSLinearLayoutItem *rangeStepperItem;
@property (nonatomic, strong) CSLinearLayoutItem *paperSizeItem;
@property (nonatomic, strong) CSLinearLayoutItem *duplexItem;
@property (nonatomic, strong) CSLinearLayoutItem *collateItem;
@property (nonatomic, strong) CSLinearLayoutItem *colorItem;
@property (nonatomic, strong) CSLinearLayoutItem *orientationItem;
@property (nonatomic, strong) CSLinearLayoutItem *worksheetItem;

@property (nonatomic, strong) NSString *mediaNum;
@property (nonatomic, strong) NSString *duplexString;
@property (nonatomic, strong) NSString *orientationString;
@property (nonatomic, strong) NSString *xlsSheetRangeString;

@end

@implementation PrintOptionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Create and initialize color option segmented control
    self.segmentColor = [[HMSegmentedControl alloc] initWithSectionTitles:@[NSLocalizedPONString(@"LABEL_BW", nil), NSLocalizedPONString(@"LABEL_COLOR", nil)]];
    self.segmentColor.titleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:17.0f]};
    self.segmentColor.selectedTitleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:17.0f],  NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentColor.selectionIndicatorColor = [UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    self.segmentColor.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentColor.selectionIndicatorBoxOpacity = 1.0f;
    self.segmentColor.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
    self.segmentColor.selectedSegmentIndex = 1;
    self.segmentColor.shouldAnimateUserSelection = YES;
    [[[self.segmentColor subviews] objectAtIndex:0] setAccessibilityIdentifier:@"PaperBlack/White Segment"];
    [[[self.segmentColor subviews] objectAtIndex:1] setAccessibilityIdentifier:@"PagerColor Segment"];
    [self.colorView addSubview:self.segmentColor];

    [self initializeOptions];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.segmentColor setFrame:CGRectMake(8, 6, self.colorView.frame.size.width - 16, self.colorView.frame.size.height - 12)];

    if (self.printOptions) {
        [self updateOptions];
        self.printOptions = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PRINT_OPTIONS", nil);
    [self.rangeStepperLabel setText:NSLocalizedPONString(@"LABEL_ALLPAGES", nil)];
    [self.paperSizeLabel setText:NSLocalizedPONString(@"LABEL_PAPER_SIZE", nil)];
    [self.duplexLabel setText:NSLocalizedPONString(@"LABEL_DOUBLE_SIDED", nil)];
    [self.collateLabel setText:NSLocalizedPONString(@"LABEL_COLLATE", nil)];
    [self.orientationLabel setText:NSLocalizedPONString(@"LABEL_ORIENTATION", nil)];
    [self.worksheetLabel setText:NSLocalizedPONString(@"LABEL_WORKSHEET", nil)];
}

- (void)setupTheme
{
    [BarButtonItem customizeLeftBarButton:self.backButton withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]]];

    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.Options.BackgroundColor"];

    [self.paperSizeChosenLabel setTextColor:[UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    [self.duplexChosenLabel setTextColor:[UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    [self.orientationChosenLabel setTextColor:[UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    [self.worksheetChosenLabel setTextColor:[UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0]];

    // Adjust UI controls
    self.switchAllPages.onTintColor = [UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    self.collateSwitch.onTintColor = [UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    [self.copiesStepper setFrame:CGRectMake(self.copiesStepper.frame.origin.x, self.copiesStepper.frame.origin.y - 1, self.copiesStepper.frame.size.width, self.copiesStepper.frame.size.height)];
    [self.switchAllPages setFrame:CGRectMake(self.switchAllPages.frame.origin.x, self.switchAllPages.frame.origin.y - 2, self.switchAllPages.frame.size.width, self.switchAllPages.frame.size.height)];
    [self.collateSwitch setFrame:CGRectMake(self.collateSwitch.frame.origin.x, self.collateSwitch.frame.origin.y - 2, self.collateSwitch.frame.size.width, self.collateSwitch.frame.size.height)];

    // Add AccessibilityId
    [self.switchAllPages setAccessibilityIdentifier:@"All Pages Switch"];
    [self.collateSwitch setAccessibilityIdentifier:@"Collate Switch"];
    [self.textLowerNumber setAccessibilityIdentifier:@"LowerNumber Textfield"];
    [self.textUpperNumber setAccessibilityIdentifier:@"UpperNumber Textfield"];
    [self.duplexChosenLabel setAccessibilityIdentifier:@"DuplexChosen Label"];
    [self.paperSizeChosenLabel setAccessibilityIdentifier:@"PaperSizeChosen Label"];
    [self.orientationChosenLabel setAccessibilityIdentifier:@"OrientationChosen Label"];
    [[[self.copiesStepper subviews] objectAtIndex:0] setAccessibilityIdentifier:@"Copy Decrement"];
    [[[self.copiesStepper subviews] objectAtIndex:1] setAccessibilityIdentifier:@"Copy Increment"];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.PrintOptions.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.PrintOptions.BackgroundColor"]];
}

- (IBAction)backPressed {
    UIViewController *parentController = self.navigationController.parentViewController;
    if ([parentController isKindOfClass:[MZFormSheetController class]]) {
        MZFormSheetController *formSheetController = (MZFormSheetController *)parentController;
        [formSheetController dismissAnimated:YES completionHandler:nil];
    }
}

- (NSMutableDictionary *)generatePrintOptions
{
    NSMutableDictionary *options = [NSMutableDictionary dictionary];

    if (self.copiesStepper.value > 1) {
        [options setValue:[NSString stringWithFormat:@"%.f", self.copiesStepper.value] forKey:@"poCopies"];
        if ([self.document getPageCount] != 1) {
            [options setValue: self.collateSwitch.isOn ? @"1" : @"0" forKey:@"poCollate"];
        }
    }

    if (!self.switchAllPages.isOn) {
        int lower = self.stepperLower.value;
        int upper = self.stepperUpper.value;
            
        if (lower > upper) upper = lower;

        if (lower > self.stepperLower.minimumValue || upper < self.stepperUpper.maximumValue) {
            [options setValue:[NSString stringWithFormat:@"%d-%d", lower, upper] forKey:@"poPageRange"];
        }
    }

    if (!self.colorView.isHidden) {
        [options setValue:[NSString stringWithFormat:@"%d", (int)self.segmentColor.selectedSegmentIndex] forKey:@"poColor"];
    }

    [options setValue:self.mediaNum forKey:@"poMediaSizeNum"];

    if (self.duplexString) {
        [options setValue:self.duplexString forKey:@"poDuplex"];
    }

    if (self.orientationString) {
        [options setValue:self.orientationString forKey:@"poOrientation"];
    }

    if (self.xlsSheetRangeString) {
        [options setValue:self.xlsSheetRangeString forKey:@"poXLSSheetRange"];
    }

    return options;
}

- (void)initializeOptions
{
    [self.copiesStepper setValue:1];
    [self updateCopiesLabel];

    unsigned long pageCount = [self.document getPageCount];

    [self.stepperLower setMinimumValue:1];
    [self.stepperLower setValue:1];
    [self.stepperUpper setMinimumValue:1];
    if (pageCount == 0) {
        [self.stepperLower setMaximumValue:10000];
        [self.stepperUpper setMaximumValue:10000];
        [self.stepperUpper setValue:1];
    } else {
        [self.stepperLower setMaximumValue:pageCount];
        [self.stepperUpper setMaximumValue:pageCount];
        [self.stepperUpper setValue:pageCount];
    }
    [self updateRangeStepperLabel];

    self.mediaNum = [self printOptions][@"poMediaSizeNum"];
    [self updatePaperSizeLabel];

    NSString *colorString = [self printOptions][@"poColor"];
    if (colorString != nil) {
        self.segmentColor.selectedSegmentIndex = [colorString intValue] == 1 ? 1 : 0;
    }

    self.duplexString = [self printOptions][@"poDuplex"];
    [self updateDuplexLabel];

    self.orientationString = [self printOptions][@"poOrientation"];
    [self updateOrientationLabel];

    self.xlsSheetRangeString = [self printOptions][@"poXLSSheetRange"];
    [self updateWorksheetLabel];
}

- (void)updateOptions
{
    // Initialize Linear Layout
    if (!self.linearLayout) {
        self.linearLayout = [[CSLinearLayoutView alloc] initWithFrame:self.backgroundView.bounds];
        self.linearLayout.autoAdjustFrameSize = YES;
        self.linearLayout.orientation = CSLinearLayoutViewOrientationVertical;
    } else {
        [self.linearLayout removeAllItems];
    }

    // Initialize copies values
    unsigned long pageCount = [self.document getPageCount];
    BOOL copiesExists = [self printOptions][@"poCopies"] != nil;
    if (self.printOptions && copiesExists) {
        NSString *copiesString = [self printOptions][@"poCopies"];
        [self.copiesStepper setValue:[copiesString doubleValue]];
        [self updateCopiesLabel];
    }

    // Add Copies View
    self.copiesItem = [self setupItem:self.copiesItem withView:self.copiesView withPadding:CSLinearLayoutMakePadding(2, 0, 0, 0)];
    [self.linearLayout addItem:self.copiesItem];

    // Initialize collate values
    BOOL collateExists = [self printOptions][@"poCollate"] != nil;
    if (self.printOptions && collateExists) {
        NSString *collateString = [self printOptions][@"poCollate"];
        [self.collateSwitch setOn:[collateString boolValue]];
    }
    
    // Add Collate View
    if (self.copiesStepper.value > 1 && pageCount != 1) {
        self.collateItem = [self setupItem:self.collateItem withView:self.collateView withPadding:CSLinearLayoutMakePadding(0, 0, 0, 0)];
        [self.linearLayout addItem:self.collateItem];
    } else {
        [self.collateView setHidden:YES];
    }

    // Initialize page range values
    BOOL pageRangeExists = [self printOptions][@"poPageRange"] != nil;
    int lowerPageRange = 0;
    int upperPageRange = 0;
    if (self.printOptions && pageRangeExists) {
        NSScanner *scanner = [NSScanner scannerWithString:[self printOptions][@"poPageRange"]];
        NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        [scanner scanInt:&lowerPageRange];
        [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
        [scanner scanInt:&upperPageRange];
    }

    // Add Stepper Page Range View
    if (pageCount != 1) {
        if (pageRangeExists) {
            [self.switchAllPages setOn:NO];
            [self.stepperLower setValue: lowerPageRange];
            [self.stepperUpper setValue: upperPageRange];
            [self updateRangeStepperLabel];
        }

        if (self.switchAllPages.isOn) {
            [self.rangeSelectView setHidden:YES];
            [self.rangeStepperView setFrame:CGRectMake(self.rangeStepperView.frame.origin.x, self.rangeStepperView.frame.origin.y, self.rangeStepperView.frame.size.width, self.rangeSelectView.frame.origin.y)];
        } else {
            [self.rangeSelectView setHidden:NO];
            [self.rangeStepperView setFrame:CGRectMake(self.rangeStepperView.frame.origin.x, self.rangeStepperView.frame.origin.y, self.rangeStepperView.frame.size.width, self.rangeSelectView.frame.origin.y + self.rangeSelectView.frame.size.height)];
        }
        
        self.rangeStepperItem = [self setupItem:self.rangeStepperItem withView:self.rangeStepperView withPadding:CSLinearLayoutMakePadding(2, 0, 0, 0)];
        [self.linearLayout addItem:self.rangeStepperItem];
    } else {
        [self.rangeStepperView setHidden:YES];
    }

    // Add WorkSheet View
    if ([self.document isMemberOfClass:[SpreadSheetDocument class]]) {
        self.worksheetItem = [self setupItem:self.worksheetItem withView:self.worksheetView withPadding:CSLinearLayoutMakePadding(2, 0, 0, 0)];
        [self.linearLayout addItem:self.worksheetItem];
    } else {
        [self.worksheetView setHidden:YES];
    }

    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];

    // Add Color View
    if (printer && [printer doesSupportColor] && ![printer.colorPermit.lowercaseString isEqualToString:@"color_only"]) {
        self.colorItem = [self setupItem:self.colorItem withView:self.colorView withPadding:CSLinearLayoutMakePadding(2, 0, 0, 0)];
        [self.linearLayout addItem:self.colorItem];
    } else {
        [self.colorView setHidden:YES];
    }

    // Add Paper Size View
    if (printer) {
        self.paperSizeItem = [self setupItem:self.paperSizeItem withView:self.paperSizeView withPadding:CSLinearLayoutMakePadding(2, 0, 0, 0)];
        [self.linearLayout addItem:self.paperSizeItem];
    } else {
        [self.paperSizeView setHidden:YES];
    }

    // Add Orientation View
    self.orientationItem = [self setupItem:self.orientationItem withView:self.orientationView withPadding:CSLinearLayoutMakePadding(2, 0, 0, 0)];
    [self.linearLayout addItem:self.orientationItem];

    // Add Duplex View
    if (printer && [printer doesSupportDuplex] && pageCount != 1) {
        self.duplexItem = [self setupItem:self.duplexItem withView:self.duplexView withPadding:CSLinearLayoutMakePadding(2, 0, 0, 0)];
        [self.linearLayout addItem:self.duplexItem];
    } else {
        [self.duplexView setHidden:YES];
    }

    [self.scrollView addSubview:self.linearLayout];

    // Update the frame sizes to match the content
    CGFloat minHeight = 4.5 * 45;
    CGFloat maxHeight;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        maxHeight = (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) ? 920 : 664;
    } else {
        maxHeight = (IS_SCREEN_R4) ? 400 : 312;
    }
    maxHeight -= self.navigationController.navigationBar.frame.size.height;

    CGFloat newHeight = MIN(MAX(minHeight, self.linearLayout.frame.size.height), maxHeight);

    [self.backgroundView setFrame:CGRectMake(self.backgroundView.frame.origin.x, self.backgroundView.frame.origin.y, self.backgroundView.frame.size.width, newHeight)];
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.linearLayout.frame.size.height)];
    [self.navigationController.view setFrame:CGRectMake(self.navigationController.view.frame.origin.x, self.navigationController.view.frame.origin.y, self.navigationController.view.frame.size.width, newHeight + self.navigationController.navigationBar.frame.size.height)];
}

- (CSLinearLayoutItem *)setupItem:(CSLinearLayoutItem *)item withView:(UIView *)view withPadding:(CSLinearLayoutItemPadding)padding
{
    if (!item) {
        item = [CSLinearLayoutItem layoutItemForView:view];
        item.padding = padding;
    }
    [view setHidden:NO];
    return item;
}

- (void)updateCopiesLabel
{
    double value = self.copiesStepper.value;
    [self.copiesLabel setText:[NSString stringWithFormat:@"%.f %@", value, (value == 1) ? NSLocalizedPONString(@"LABEL_COPY", nil) : NSLocalizedPONString(@"LABEL_COPIES", nil)]];
}

- (void)updateRangeStepperLabel
{
    [self.textLowerNumber setText:[NSString stringWithFormat:@"%.f", self.stepperLower.value]];
    [self.textUpperNumber setText:[NSString stringWithFormat:@"%.f", self.stepperUpper.value]];
}

- (void)updatePaperSizeLabel
{
    PaperSize *paperSize = [PaperSize getPaperSizeFromMediaSizeNumber:[self.mediaNum intValue]];
    [self.paperSizeChosenLabel setText:paperSize.title];
}

- (void)updateDuplexLabel
{
    if ([self.duplexString isEqualToString:@"Simplex"]) {
        [self.duplexChosenLabel setText:NSLocalizedPONString(@"LABEL_OFF", nil)];
    } else if ([self.duplexString isEqualToString:@"DuplexLong"]) {
        [self.duplexChosenLabel setText:NSLocalizedPONString(@"LABEL_DUPLEXLONG", nil)];
    } else if ([self.duplexString isEqualToString:@"DuplexShort"]) {
        [self.duplexChosenLabel setText:NSLocalizedPONString(@"LABEL_DUPLEXSHORT", nil)];
    }
}

- (void)updateOrientationLabel
{
    if ([self.orientationString isEqualToString:@"Portrait"]) {
        [self.orientationChosenLabel setText:NSLocalizedPONString(@"LABEL_PORTRAIT", nil)];
    } else if ([self.orientationString isEqualToString:@"Landscape"]) {
        [self.orientationChosenLabel setText:NSLocalizedPONString(@"LABEL_LANDSCAPE", nil)];
    } else {
        [self.orientationChosenLabel setText:NSLocalizedPONString(@"LABEL_DOCUMENT", nil)];
    }
}

- (void)updateWorksheetLabel
{
    if ([self.xlsSheetRangeString isEqualToString:@"Active"]) {
        [self.worksheetChosenLabel setText:NSLocalizedPONString(@"LABEL_ACTIVE", nil)];
    } else if ([self.xlsSheetRangeString isEqualToString:@"All"]) {
        [self.worksheetChosenLabel setText:NSLocalizedPONString(@"LABEL_ALL", nil)];
    }
}

- (IBAction)copiesStepperValueChanged {
    [self updateCopiesLabel];
    if (self.copiesStepper.value == 1 || self.copiesStepper.value == 2) {
        [self updateOptions];
    }
}

- (IBAction)switchAllPagesToggle {
    [self updateOptions];
}

- (IBAction)lowerStepperValueChanged {
    [self.textLowerNumber setText:[NSString stringWithFormat:@"%.f", self.stepperLower.value]];
}

- (IBAction)upperStepperValueChanged {
    [self.textUpperNumber setText:[NSString stringWithFormat:@"%.f", self.stepperUpper.value]];
}

- (IBAction)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)validateText:(UIStepper *)sender
{
    UITextField *textField = [sender isEqual:self.stepperLower] ? self.textLowerNumber : self.textUpperNumber;
    int value = [textField.text intValue];
    
    if (value == 0) {
        textField.text = [NSString stringWithFormat:@"%.f", sender.minimumValue];
        sender.value = sender.minimumValue;
    } else if (value > sender.maximumValue) {
        textField.text = [NSString stringWithFormat:@"%.f", sender.maximumValue];
        sender.value = sender.maximumValue;
    } else {
        sender.value = value;
    }
}

#pragma mark - PrintOptionsTableDelegate

- (void)didPickOptionOfType:(OptionsTableType)type WithValue:(NSString *)string
{
    if (type == OptionsTableTypeDuplex) {
        self.duplexString = string;
        [self updateDuplexLabel];
    } else if (type == OptionsTableTypePaperSize) {
        self.mediaNum = string;
        [self updatePaperSizeLabel];
    } else if (type == OptionsTableTypeOrientation) {
        self.orientationString = string;
        [self updateOrientationLabel];
    } else if (type == OptionsTableTypeWorksheet) {
        self.xlsSheetRangeString = string;
        [self updateWorksheetLabel];
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:self.textLowerNumber] || [textField isEqual:self.textUpperNumber]) {
        UIStepper *stepper = [textField isEqual:self.textLowerNumber] ? self.stepperLower : self.stepperUpper;
        [self validateText:stepper];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[1-9]\\d*$" options:NSRegularExpressionCaseInsensitive error:nil];
    
    if ([resultString length] > 0) {
        NSUInteger numOfMatches = [regex numberOfMatchesInString:resultString options:0 range:NSMakeRange(0, [resultString length])];
        if (numOfMatches == 0) {
            return NO;
        } else {
            UIStepper *stepper = [textField isEqual:self.textLowerNumber] ? self.stepperLower : self.stepperUpper;
            stepper.value = [resultString intValue];
        }
    }
    
    return YES;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([self.textLowerNumber isFirstResponder] || [self.textUpperNumber isFirstResponder]) {
        return YES;
    }
    return NO;
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    PrintOptionsTableViewController *destViewController = segue.destinationViewController;
    destViewController.delegate = self;

    if ([segue.identifier isEqualToString:@"paperSizeOption"]) {
        destViewController.type = OptionsTableTypePaperSize;
        destViewController.typeValue = self.mediaNum;
    } else if ([segue.identifier isEqualToString:@"duplexOption"]) {
        destViewController.type = OptionsTableTypeDuplex;
        destViewController.typeValue = self.duplexString;
    } else if ([segue.identifier isEqualToString:@"orientationOption"]) {
        destViewController.type = OptionsTableTypeOrientation;
        destViewController.typeValue = self.orientationString;
        destViewController.document = self.document;
    } else if ([segue.identifier isEqualToString:@"worksheetOption"]) {
        destViewController.type = OptionsTableTypeWorksheet;
        destViewController.typeValue = self.xlsSheetRangeString;
    }
}

@end
