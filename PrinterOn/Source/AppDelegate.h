//
//  AppDelegate.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "COSTouchVisualizerWindow.h"

@protocol OIDAuthorizationFlowSession;

@interface AppDelegate : UIResponder <UIApplicationDelegate, COSTouchVisualizerWindowDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) BOOL showSplash;

@property (strong, nonatomic) id<OIDAuthorizationFlowSession> currentAuthorizationFlow;
@property (nonatomic, copy) void (^currentAuthorizationBlock)(void);

- (NSURL *)applicationDocumentsDirectory;

- (void)setupAnalytics;
- (void)setupManagers;
- (void)setupRestKit;
- (void)setupTheme;
- (void)customSetup:(NSDictionary *)launchOptions;

- (BOOL)showDocuments;
- (BOOL)showEmail;
- (BOOL)showPhotos;
- (BOOL)showWeb;

@end
