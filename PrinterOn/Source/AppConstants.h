//
//  AppConstants.h
//  PrinterOn
//
//  Created by Mark Burns on 2014-08-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

FOUNDATION_EXPORT NSString* const kAppVersionText;
FOUNDATION_EXPORT NSString* const kAppVersionName;
FOUNDATION_EXPORT NSString* const kAboutCopyrightText;
FOUNDATION_EXPORT CGFloat const kAboutCopyrightTextSize;
FOUNDATION_EXPORT NSString* const kImagesBundleIdentifier;

@interface AppConstants : NSObject

+ (NSString *)googleOAuth2ClientID;
+ (NSString *)outlookOAuth2ClientID;
+ (NSString *)outlookOAuth2ClientSecret;

@end
