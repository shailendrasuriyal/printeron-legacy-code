//
//  PONLocalization.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-07-22.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#ifndef NSLocalizedPONString
#define NSLocalizedPONString(key, comment) \
    NSLocalizedString((key), (comment))
#endif
