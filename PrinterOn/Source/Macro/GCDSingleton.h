//
//  GCDSingleton.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-17.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#ifndef SINGLETON_GCD
#define SINGLETON_GCD(classname)                            \
                                                            \
+ (classname *)shared##classname {                          \
    static dispatch_once_t pred = 0;                        \
    __strong static classname *shared##classname = nil;     \
    dispatch_once(&pred, ^{                                 \
        shared##classname = [self new];            \
    });                                                     \
    return shared##classname;                               \
}
#endif
