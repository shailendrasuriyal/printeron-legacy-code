//
//  DeviceCapabilities.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-22.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#ifndef IS_SCREEN_R4
#define IS_SCREEN_R4   ([UIScreen mainScreen].bounds.size.height == 568.0)
#endif

#ifndef IS_IOS_AT_LEAST
#define IS_IOS_AT_LEAST(ver)    ([[[UIDevice currentDevice] systemVersion] compare:ver options:NSNumericSearch] != NSOrderedAscending)
#endif
