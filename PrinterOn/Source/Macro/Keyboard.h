//
//  Keyboard.h
//  PrinterOn
//
//  Created by Mark Burns on 2/6/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#ifndef THEME_KEYBOARD
#define THEME_KEYBOARD(field) { \
    if ([ThemeLoader boolForKey:@"Keyboard.isLight"]) { \
        [field setKeyboardAppearance:UIKeyboardAppearanceLight]; \
    } else { \
        [field setKeyboardAppearance:UIKeyboardAppearanceDark]; \
    } \
}
#endif
