//
//  URLAddressBar.m
//  PrinterOn
//
//  Created by Mark Burns on 11/20/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "URLAddressBar.h"

@implementation URLAddressBar

#pragma mark - Accessors

@synthesize loading = _loading;
@synthesize reloadButton = _reloadButton;
@synthesize stopButton = _stopButton;

- (UIButton *)reloadButton {
    if (!_reloadButton) {
        _reloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _reloadButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _reloadButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [_reloadButton setImage:[[ImageManager sharedImageManager] imageNamed:@"URLAddressBar-Reload"] forState:UIControlStateNormal];
    }
    return _reloadButton;
}

- (UIButton *)stopButton {
    if (!_stopButton) {
        _stopButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _stopButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _stopButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [_stopButton setImage:[[ImageManager sharedImageManager] imageNamed:@"URLAddressBar-Stop"] forState:UIControlStateNormal];
    }
    return _stopButton;
}

- (void)setLoading:(BOOL)isLoading {
    if (_loading == isLoading) {
        return;
    }
    _loading = isLoading;
    
    self.rightView = _loading ? self.stopButton : self.reloadButton;
}


#pragma mark - UIView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self initialize];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)aFrame {
    if ((self = [super initWithFrame:aFrame])) {
        [self initialize];
    }
    return self;
}

#pragma mark - UITextField

- (CGRect)rightViewRectForBounds:(CGRect)bounds {
    CGSize size = self.frame.size;
    return CGRectMake(size.width - 24.0f, roundf((size.height - 16.0f) / 2.0f) + 1, 16.0f, 16.0f);
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return UIEdgeInsetsInsetRect([super textRectForBounds:bounds], UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 24.0f));
}

#pragma mark - Private

- (void)initialize {
    // Configure text field
    self.borderStyle = UITextBorderStyleRoundedRect;
    self.textColor = [UIColor colorWithWhite:0.180f alpha:1.0f];
    self.font = [UIFont systemFontOfSize:14.0f];
    self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.keyboardType = UIKeyboardTypeURL;
    self.returnKeyType = UIReturnKeyGo;
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.enablesReturnKeyAutomatically = YES;
    self.rightViewMode = UITextFieldViewModeUnlessEditing;
    self.tintColor = [UIColor colorWithRed:20.0/255.0 green:111.0/255.0 blue:225.0/255.0 alpha:1.0];
    // Add AccessibilityId for URL AddressBar
    [self setAccessibilityIdentifier:@"URL AddressBar"];
    self.loading = NO;
}

@end
