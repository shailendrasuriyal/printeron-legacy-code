//
//  URLAddressBar.h
//  PrinterOn
//
//  Created by Mark Burns on 11/20/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface URLAddressBar : UITextField

@property (nonatomic, getter = isLoading) BOOL loading;
@property (nonatomic, readonly) UIButton *reloadButton;
@property (nonatomic, readonly) UIButton *stopButton;

@end
