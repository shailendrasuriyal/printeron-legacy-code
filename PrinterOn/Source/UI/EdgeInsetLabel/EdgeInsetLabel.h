//
//  EdgeInsetLabel.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-03-25.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface EdgeInsetLabel : UILabel

- (void)setEdgeInsets:(UIEdgeInsets)edgeInsets;

@end
