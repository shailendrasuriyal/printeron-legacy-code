//
//  EdgeInsetLabel.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-03-25.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "EdgeInsetLabel.h"

@interface EdgeInsetLabel ()

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end

@implementation EdgeInsetLabel

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines
{
    UIEdgeInsets insets = self.edgeInsets;
    CGRect rect = [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, insets)
                    limitedToNumberOfLines:numberOfLines];
    
    rect.origin.x    -= insets.left;
    rect.origin.y    -= insets.top;
    rect.size.width  += (insets.left + insets.right);
    rect.size.height += (insets.top + insets.bottom);
    
    return rect;
}

- (void)drawTextInRect:(CGRect)rect
{
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

- (void)setEdgeInsets:(UIEdgeInsets)edgeInsets
{
    _edgeInsets = edgeInsets;
}

@end
