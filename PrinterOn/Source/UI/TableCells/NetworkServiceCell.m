//
//  NetworkServiceCell.m
//  PrinterOn
//
//  Created by Mark Burns on 2/11/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "NetworkServiceCell.h"

#import "PONNetworkService.h"

@implementation NetworkServiceCell

- (void)setupCellWithService:(PONNetworkService *)service {
    [self.labelTitle setText:service.serviceName];
    [self.labelSubtitle setText:service.isUntrustedCert ? NSLocalizedPONString(@"ERROR_CERTWARN", nil) : nil];
    [self.imageIcon setImage:service.isUntrustedCert ? [[ImageManager sharedImageManager] imageNamed:@"CautionIcon"] : [[ImageManager sharedImageManager] imageNamed:@"PrinterIcon"]];

    if (service.isUntrustedCert) {
        [self setupWarning];
    } else {
        [self setupPrinterCount:service];
    }
}

- (void)setupWarning
{
    [self.labelSubtitle setHidden:NO];
    [self.labelValue setHidden:YES];

    [self.imageIcon setFrame:CGRectMake(self.frame.size.width - self.imageIcon.frame.size.width - 8, self.imageIcon.frame.origin.y, self.imageIcon.frame.size.width, self.imageIcon.frame.size.height)];

    [self.labelTitle setFrame:CGRectMake(self.labelTitle.frame.origin.x, 6, self.frame.size.width - self.imageIcon.frame.size.width - 24, self.labelTitle.frame.size.height)];
}

- (void)setupPrinterCount:(PONNetworkService *)service
{
    [self.labelSubtitle setHidden:YES];
    [self.labelValue setHidden:NO];

    // Resize the label to hold the text
    [self.labelValue setText:[NSString stringWithFormat:@"%ld", service.printerCount]];
    CGSize maxSize = CGSizeMake(42, self.labelValue.frame.size.height);
    CGSize requiredSize = [self.labelValue sizeThatFits:maxSize];
    
    // Align the label and image to the right of the cell
    [self.labelValue setFrame:CGRectMake(self.frame.size.width - 8 - requiredSize.width, self.labelValue.frame.origin.y, requiredSize.width, self.labelValue.frame.size.height)];
    [self.imageIcon setFrame:CGRectMake(self.labelValue.frame.origin.x - self.imageIcon.frame.size.width - 4, self.imageIcon.frame.origin.y, self.imageIcon.frame.size.width, self.imageIcon.frame.size.height)];

    [self.labelTitle setFrame:CGRectMake(self.labelTitle.frame.origin.x, (self.frame.size.height - self.labelTitle.frame.size.height)/2, self.imageIcon.frame.origin.x - 16, self.labelTitle.frame.size.height)];
}

@end
