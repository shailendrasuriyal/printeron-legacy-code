//
//  EmailProviderCell.m
//  PrinterOn
//
//  Created by Mark Burns on 02/25/2016.
//  Copyright (c) 2016 PrinterOn Inc. All rights reserved.
//

#import "EmailProviderCell.h"

#import "EmailAccount.h"

@implementation EmailProviderCell

- (void)setupCell:(NSUInteger)providerType
{
    switch (providerType) {
        case EmailAccountTypeICloud: {
            self.accessibilityIdentifier = @"iCloud";
            self.providerImageView.image = [[ImageManager sharedImageManager] imageNamed:@"icloud"];
            self.providerLabel.hidden = YES;
            break;
        }
        case EmailAccountTypeGMail: {
            self.accessibilityIdentifier = @"Gmail";
            self.providerImageView.image = [[ImageManager sharedImageManager] imageNamed:@"gmail"];
            self.providerLabel.hidden = YES;
            break;
        }
        case EmailAccountTypeOutlook: {
            self.accessibilityIdentifier = @"Outlook";
            self.providerImageView.image = [[ImageManager sharedImageManager] imageNamed:@"outlook"];
            self.providerLabel.hidden = YES;
            break;
        }
        case EmailAccountTypeYahoo: {
            self.accessibilityIdentifier = @"Yahoo";
            self.providerImageView.image = [[ImageManager sharedImageManager] imageNamed:@"yahoo"];
            self.providerLabel.hidden = YES;
            break;
        }
        case EmailAccountTypeOther: {
            self.accessibilityIdentifier = @"Other";
            self.providerImageView.hidden = YES;
            self.providerLabel.text = @"Other";
            break;
        }
        default: {
            self.providerImageView.hidden = YES;
            self.providerLabel.hidden = YES;
            break;
        }
    }
}

@end
