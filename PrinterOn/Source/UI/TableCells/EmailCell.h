//
//  EmailCell.h
//  PrinterOn
//
//  Created by Mark Burns on 11/29/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class MCOIMAPMessage, MCOMessageHeader, MCOMessageParser;

@interface EmailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UIImageView *imageAttach;

- (void)setupIMAPCell:(MCOIMAPMessage *)message withFormatter:(NSDateFormatter *)formatter;

@end
