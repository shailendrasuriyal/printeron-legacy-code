//
//  EmailCell.m
//  PrinterOn
//
//  Created by Mark Burns on 11/29/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "EmailCell.h"

#import <MailCore/MailCore.h>

@implementation EmailCell

- (void)setupIMAPCell:(MCOIMAPMessage *)message withFormatter:(NSDateFormatter *)formatter
{
    [self.labelTitle setText:(!message.header.from.displayName) ? message.header.from.mailbox : message.header.from.displayName];
    [self.labelSubtitle setText:[message.header.subject length] == 0 ? NSLocalizedPONString(@"LABEL_NOSUBJECT", nil) : message.header.subject];
    [self.labelDate setText:message.header.receivedDate ? [formatter stringFromDate:message.header.receivedDate] : @""];
    [self.imageAttach setImage:([message.attachments count] > 0) ? [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"EmailScreen.Attachments.Dark.Image"]] : nil];
}

@end
