//
//  PhotoGroupCell.h
//  PrinterOn
//
//  Created by Mark Burns on 2/2/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@class ALAssetsGroup;

@interface PhotoGroupCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *groupImage;
@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UILabel *groupCount;

- (void)setupCell:(ALAssetsGroup *)assetsGroup;

@end
