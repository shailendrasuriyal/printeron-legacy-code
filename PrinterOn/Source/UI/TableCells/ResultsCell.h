//
//  ResultsCell.h
//  PrinterOn
//
//  Created by Mark Burns on 11/4/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface ResultsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelTop;
@property (strong, nonatomic) IBOutlet UILabel *labelBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLabelTopConstraint;

- (void)addTarget:(id)target action:(SEL)action;
- (void)setupCell:(NSString *)label isTap:(BOOL)tap;

@end
