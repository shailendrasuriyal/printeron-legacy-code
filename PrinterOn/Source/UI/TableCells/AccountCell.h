//
//  AccountCell.h
//  PrinterOn
//
//  Created by Mark Burns on 11/26/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface AccountCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelAccount;
@property (weak, nonatomic) IBOutlet UIImageView *lockImage;

- (void)setupCell:(NSString *)account withImage:(UIImage *)image showLock:(BOOL)locked;

@end
