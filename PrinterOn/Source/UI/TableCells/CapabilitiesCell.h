//
//  CapabilitiesCell.h
//  PrinterOn
//
//  Created by Mark Burns on 11/14/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface CapabilitiesCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelValue;

- (void)setupCell:(NSDictionary *)data;

@end
