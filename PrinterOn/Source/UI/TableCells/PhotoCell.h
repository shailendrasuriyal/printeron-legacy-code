//
//  PhotoCell.h
//  PrinterOn
//
//  Created by Mark Burns on 2/2/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface PhotoCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *image;

+ (NSString*) reuseIdentifier;

@end
