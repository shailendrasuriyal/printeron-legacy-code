//
//  AccountCell.m
//  PrinterOn
//
//  Created by Mark Burns on 11/26/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AccountCell.h"

@implementation AccountCell

@synthesize imageView;

- (void)setupCell:(NSString *)account withImage:(UIImage *)image showLock:(BOOL)locked {
    [self.labelAccount setText:account];
    [self.imageView setImage:image];
    [self.lockImage setImage: locked ? [[ImageManager sharedImageManager] imageNamed:@"LockIcon"] : nil];
    // Add AccessibilityId
    [self.labelAccount setAccessibilityIdentifier:[NSString stringWithFormat:@"%@ Account", account]];
}

@end
