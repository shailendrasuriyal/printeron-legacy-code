//
//  PrinterCell.m
//  PrinterOn
//
//  Created by Mark Burns on 10/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrinterCell.h"

#import "Printer.h"

@implementation PrinterCell

- (void)setupCellWithPrinter:(Printer *)printer {
    [self.labelTitle setText:printer.displayName];
    [self.labelSubtitle setText:printer.organizationLocationDesc];

    if (printer.addressGeoDistanceFrom && printer.addressGeoUnits) {
        [self.labelValue setText:[NSString stringWithFormat:@"%.1f %@", [printer.addressGeoDistanceFrom floatValue], [printer.addressGeoUnits length] > 2 ? [printer.addressGeoUnits substringToIndex:2] : printer.addressGeoUnits]];
    } else {
        [self.labelValue setText:nil];
    }

    [self.buttonInfo setImage:[[ImageManager sharedImageManager] imageNamed:@"InfoIcon"] forState:UIControlStateNormal];
    // Add accessibility id for InfoIcon
    [self.buttonInfo setAccessibilityIdentifier:[NSString stringWithFormat:@"%@ InfoIcon",[self.labelSubtitle text]]];
    [self.labelSubtitle setAccessibilityIdentifier:[NSString stringWithFormat:@"%@ cellSubtitle",[self.labelSubtitle text]]];
}

@end
