//
//  NetworkServiceHeader.h
//  PrinterOn
//
//  Created by Mark Burns on 2/12/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface NetworkServiceHeader : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end
