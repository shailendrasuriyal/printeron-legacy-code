//
//  NetworkServiceCell.h
//  PrinterOn
//
//  Created by Mark Burns on 2/11/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@class PONNetworkService;

@interface NetworkServiceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *labelValue;

- (void)setupCellWithService:(PONNetworkService *)service;

@end
