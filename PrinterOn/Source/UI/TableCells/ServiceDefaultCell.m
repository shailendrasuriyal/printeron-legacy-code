//
//  ServiceDefaultCell.m
//  PrinterOn
//
//  Created by Mark Burns on 1/26/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "ServiceDefaultCell.h"

#import "GTMOAuth2Authentication.h"
#import "ServiceDescription.h"

@implementation ServiceDefaultCell

- (void)layoutSubviews
{
    [super layoutSubviews];

    // Adjust switch location under iOS7 because the control is a different size
    [self.switchDefault setFrame:CGRectMake(self.switchDefault.frame.origin.x + 6, self.switchDefault.frame.origin.y - 2, self.switchDefault.frame.size.width, self.switchDefault.frame.size.height)];
    [self.indicatorLoading setFrame:CGRectMake(self.indicatorLoading.frame.origin.x + 6, self.indicatorLoading.frame.origin.y, self.indicatorLoading.frame.size.width, self.indicatorLoading.frame.size.height)];
}

- (void)setupCell:(Service *)service isDefault:(BOOL)value
{
    self.labelTitle.text = service.serviceDescription;
    self.switchDefault.on = value;
    self.switchDefault.selected = value;
    self.switchDefault.accessibilityIdentifier = [NSString stringWithFormat:@"%@ Switch", service.serviceDescription];
    self.indicatorLoading.hidden = self.switchDefault.isOn;
    self.switchDefault.hidden = !self.switchDefault.isOn;

    self.serviceURL = service.serviceURL;

    self.isFetching = YES;
    __weak __typeof(self) weakSelf = self;
    [ServiceDescription capabilitiesForServiceWithURL:[NSURL URLWithString:self.serviceURL] forceUpdate:NO completionBlock:^(NSManagedObjectID *capabilities) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf.indicatorLoading stopAnimating];
        strongSelf.switchDefault.hidden = NO;
        strongSelf.switchDefault.enabled = YES;
        strongSelf.isFetching = NO;
    }];
}

@end
