//
//  AttachmentCell.m
//  PrinterOn
//
//  Created by Mark Burns on 05/03/2016.
//  Copyright (c) 2016 PrinterOn Inc. All rights reserved.
//

#import "AttachmentCell.h"

#import "EmailAttachmentManager.h"
#import "PrintDocument.h"

#import <MailCore/MailCore.h>

@interface AttachmentCell ()

@property (nonatomic, strong) NSProgress *progress;

@end

@implementation AttachmentCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // In iOS 7.0+ the Progress View height is much smaller and needs to have it's positioning adjusted
    [self.progressUpload setFrame:CGRectMake(self.progressUpload.frame.origin.x, 35, self.progressUpload.frame.size.width, self.progressUpload.frame.size.height)];
}

- (void)setupCellWithAttachment:(NSString *)attachment withPart:(MCOIMAPPart *)part
{
    self.supportedFile = [PrintDocument isSupportedFileType:part.filename];

    [self.labelTitle setText:part.filename];

    if (!self.supportedFile) {
        [self.labelSubtitle setHidden:NO];
        [self.labelSubtitle setText:NSLocalizedPONString(@"LABEL_FILE_NOT_SUPPORTED", nil)];
        [self.labelUpload setHidden:YES];
        [self.progressUpload setHidden:YES];
        [self.imageStatus setImage:nil];
        return;
    }

    id data = [[EmailAttachmentManager sharedEmailAttachmentManager] progressForAttachment:attachment];
    unsigned int fileSize = data == [NSNull null] ? part.decodedSize : part.size;

    if (fileSize < 1024) {
        [self.labelSubtitle setText:[NSString stringWithFormat:@"%u B", fileSize]];
    } else if (fileSize < 1048576) {
        [self.labelSubtitle setText:[NSString stringWithFormat:@"%.1f KB", fileSize / 1024.0]];
    } else {
        [self.labelSubtitle setText:[NSString stringWithFormat:@"%.1f MB", fileSize / 1048576.0]];
    }

    if (data == nil) {
        [self.labelSubtitle setHidden:NO];
        [self.labelUpload setHidden:YES];
        [self.progressUpload setHidden:YES];
        [self.imageStatus setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"EmailScreen.Download.Image"]]];
    } else if ([data isMemberOfClass:[NSProgress class]]) {
        [self.labelSubtitle setHidden:YES];
        [self.labelUpload setHidden:NO];
        [self.progressUpload setHidden:NO];
        [self.imageStatus setImage:nil];

        self.progress = (NSProgress *)data;
        [self updateProgress:NO];
        [self registerForProgressUpdates];
    } else if (data == [NSNull null]) {
        [self.labelSubtitle setHidden:NO];
        [self.labelUpload setHidden:YES];
        [self.progressUpload setHidden:YES];
        [self.imageStatus setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"EmailScreen.Print.Dark.Image"]]];
    }
}

- (void)registerForProgressUpdates
{
    if (self.progress) {
        [self.progress addObserver:self forKeyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew context:NULL];
    }
}

- (void)unregisterProgressUpdates
{
    if (self.progress) {
        [self.progress removeObserver:self forKeyPath:@"fractionCompleted" context:NULL];
        self.progress = nil;
    }
}

- (void)updateProgress:(BOOL)animated
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.progressUpload setProgress:self.progress.fractionCompleted animated:animated];
        [self.labelUpload setText:[NSString stringWithFormat:@"%.0f%%", self.progress.fractionCompleted * 100]];
    });
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:self.progress] && [keyPath isEqualToString:@"fractionCompleted"]) {
        [self updateProgress:YES];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self unregisterProgressUpdates];
}

- (void)dealloc
{
    [self unregisterProgressUpdates];
}

@end
