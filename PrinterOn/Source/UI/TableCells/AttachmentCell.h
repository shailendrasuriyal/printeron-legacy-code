//
//  AttachmentCell.h
//  PrinterOn
//
//  Created by Mark Burns on 05/03/2016.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class MCOIMAPPart;

@interface AttachmentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelUpload;
@property (weak, nonatomic) IBOutlet UIProgressView *progressUpload;

@property (nonatomic, assign) BOOL supportedFile;

- (void)setupCellWithAttachment:(NSString *)attachment withPart:(MCOIMAPPart *)part;

@end
