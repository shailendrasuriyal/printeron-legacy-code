//
//  PrinterCell.h
//  PrinterOn
//
//  Created by Mark Burns on 10/30/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class Printer;

@interface PrinterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *labelValue;
@property (weak, nonatomic) IBOutlet UIButton *buttonInfo;

- (void)setupCellWithPrinter:(Printer *)printer;

@end
