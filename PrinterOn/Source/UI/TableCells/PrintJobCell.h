//
//  PrintJobCell.h
//  PrinterOn
//
//  Created by Mark Burns on 12/12/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@class PrintJob;

@interface PrintJobCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageType;
@property (weak, nonatomic) IBOutlet UIImageView *imageOverlay;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDateTime;
@property (weak, nonatomic) IBOutlet UIImageView *imagePage;
@property (weak, nonatomic) IBOutlet UILabel *labelPageCount;
@property (weak, nonatomic) IBOutlet UILabel *labelUpload;
@property (weak, nonatomic) IBOutlet UIProgressView *progressUpload;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (void)setupCell:(PrintJob *)job withFormatter:(NSDateFormatter *)formatter;

@end
