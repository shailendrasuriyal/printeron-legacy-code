//
//  EmailProviderCell.h
//  PrinterOn
//
//  Created by Mark Burns on 02/25/2016.
//  Copyright (c) 2016 PrinterOn Inc. All rights reserved.
//

@interface EmailProviderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *providerImageView;
@property (weak, nonatomic) IBOutlet UILabel *providerLabel;

- (void)setupCell:(NSUInteger)providerType;

@end
