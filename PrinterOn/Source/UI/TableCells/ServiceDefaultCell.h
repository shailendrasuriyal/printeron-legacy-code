//
//  ServiceDefaultCell.h
//  PrinterOn
//
//  Created by Mark Burns on 1/26/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "Service.h"

@class GTMOAuth2Authentication;

@interface ServiceDefaultCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UISwitch *switchDefault;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorLoading;

@property (nonatomic, assign) BOOL isFetching;
@property (nonatomic, strong) NSString *serviceURL;
@property (nonatomic, strong) GTMOAuth2Authentication *auth;

- (void)setupCell:(Service *)service isDefault:(BOOL)value;

@end
