//
//  ResultsCell.m
//  PrinterOn
//
//  Created by Mark Burns on 11/4/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "ResultsCell.h"

@interface ResultsCell ()

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, assign) NSInteger origLabelY;

@end

@implementation ResultsCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.tapGesture = [UITapGestureRecognizer new];
        [self.contentView addGestureRecognizer:self.tapGesture];
    }
    return self;
}

- (void)addTarget:(id)target action:(SEL)action {
    [self.tapGesture addTarget:target action:action];
}

- (void)setupCell:(NSString *)label isTap:(BOOL)tap {
    // Add AccessibilityId
    [self.labelTop setAccessibilityIdentifier:@"number of printers"];
    
    [self.labelTop setText:label];
    [self.labelBottom setText:NSLocalizedPONString(@"LABEL_LOADMORE", nil)];

    [self.labelBottom setHidden:!tap];
    [self.tapGesture setEnabled:tap];

    if (tap) {
        if (self.topLabelTopConstraint != nil && self.bottomLabelTopConstraint != nil) {
            self.topLabelTopConstraint.constant = 5;
            self.bottomLabelTopConstraint.constant = 2;
        } else {
            if (self.origLabelY) {
                [self.labelTop setFrame:CGRectMake(self.labelTop.frame.origin.x, self.origLabelY, self.labelTop.frame.size.width, self.labelTop.frame.size.height)];
            }
        }
    } else {
        if (self.topLabelTopConstraint != nil && self.bottomLabelTopConstraint != nil) {
            self.topLabelTopConstraint.constant = 15;
            self.bottomLabelTopConstraint.constant = 16;
        } else {
            if (!self.origLabelY) {
                self.origLabelY = self.labelTop.frame.origin.y;
            }
            self.labelTop.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
        }
    }
}

@end
