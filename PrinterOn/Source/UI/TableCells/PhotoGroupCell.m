//
//  PhotoGroupCell.m
//  PrinterOn
//
//  Created by Mark Burns on 2/2/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "PhotoGroupCell.h"

#import <AssetsLibrary/AssetsLibrary.h>

@implementation PhotoGroupCell

- (void)setupCell:(ALAssetsGroup *)assetsGroup
{
    CGImageRef posterImage = assetsGroup.posterImage;
    size_t height = CGImageGetHeight(posterImage);
    float scale = height / self.groupImage.frame.size.height;

    [self.groupImage setImage:[UIImage imageWithCGImage:posterImage scale:scale orientation:UIImageOrientationUp]];
    [self.groupName setText:[assetsGroup valueForProperty:ALAssetsGroupPropertyName]];

    long count = assetsGroup.numberOfAssets;
    [self.groupCount setText:[NSString stringWithFormat:@"%ld %@", count, count == 1 ? NSLocalizedPONString(@"LABEL_PHOTO", nil) : NSLocalizedPONString(@"LABEL_PHOTOS", nil)]];
}

@end
