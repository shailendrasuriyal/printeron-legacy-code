//
//  PrintJobCell.m
//  PrinterOn
//
//  Created by Mark Burns on 12/12/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "PrintJobCell.h"

#import "PrintJob.h"
#import "PrintDocument.h"
#import "PrintJobItem.h"
#import "PrintJobManager.h"

@interface PrintJobCell ()

@property (nonatomic, strong) NSProgress *progress;

@end

@implementation PrintJobCell

- (void)layoutSubviews
{
    [super layoutSubviews];

    // In iOS 7.0+ the Progress View height is much smaller and needs to have it's positioning adjusted
    [self.progressUpload setFrame:CGRectMake(self.progressUpload.frame.origin.x, 35, self.progressUpload.frame.size.width, self.progressUpload.frame.size.height)];
}

- (void)setupCell:(PrintJob *)job withFormatter:(NSDateFormatter *)formatter
{
    [self.labelTitle setText:[job getJobStatusLabel]];
    [self.labelDateTime setText:job.dateTime ? [[formatter stringFromDate:job.dateTime] lowercaseString] : @""];
    [self.imagePage setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"HistoryScreen.Pages.Image"]]];
    [self resizePageCount:job];
    [self.imageType setImage:[job getJobTypeImage]];
    [self.imageOverlay setImage:[job getJobStatusImage]];

    long jobStatus = [job getJobStatusValue];
    long docType = [job getDocumentTypeValue];
    if (jobStatus == JobStatusCodeUploadExtension) {
        [self.labelSubtitle setText:[[job.documentURI lastPathComponent] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    } else if (docType == DocumentTypeWeb) {
        [self.labelSubtitle setText:[job getDocumentDataValue:1]];
    } else if (docType == DocumentTypeFile) {
        [self.labelSubtitle setText:[[job.documentURI lastPathComponent] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    } else if (docType == DocumentTypeEmail) {
        [self.labelSubtitle setText:[job getDocumentDataValue:2]];
    } else if (docType == DocumentTypePhoto) {
        [self.labelSubtitle setText:[job getDocumentDataValue:1]];
    }

    if (jobStatus == JobStatusCodePending || jobStatus == JobStatusCodeUploadExtension) {
        [self.labelSubtitle setHidden:NO];
        [self.labelDateTime setHidden:YES];
        [self.labelUpload setHidden:YES];
        [self.progressUpload setHidden:YES];
        [self.imagePage setHidden:YES];
        [self.labelPageCount setHidden:YES];
        [self.activityIndicator startAnimating];
    } else if (jobStatus == JobStatusCodeCancelled || jobStatus == JobStatusCodeFailed || jobStatus == JobStatusCodeHeld) {
        if (jobStatus == JobStatusCodeHeld) [self.labelSubtitle setText:NSLocalizedPONString(@"LABEL_JOBHELD", nil)];
        [self.labelSubtitle setHidden:NO];
        [self.labelDateTime setHidden:NO];
        [self.labelUpload setHidden:YES];
        [self.progressUpload setHidden:YES];
        [self.imagePage setHidden:YES];
        [self.labelPageCount setHidden:YES];
        [self.activityIndicator stopAnimating];
    } else if (jobStatus == JobStatusCodeSuccess) {
        [self.labelSubtitle setHidden:NO];
        [self.labelDateTime setHidden:NO];
        [self.labelUpload setHidden:YES];
        [self.progressUpload setHidden:YES];
        [self.imagePage setHidden:NO];
        [self.labelPageCount setHidden:NO];
        [self.activityIndicator stopAnimating];
    } else if (jobStatus == JobStatusCodeCreated || jobStatus == JobStatusCodeUpload) {
        [self.labelSubtitle setHidden:YES];
        [self.labelDateTime setHidden:YES];
        [self.labelUpload setHidden:NO];
        [self.progressUpload setHidden:NO];
        [self.imagePage setHidden:YES];
        [self.labelPageCount setHidden:YES];
        [self.activityIndicator stopAnimating];

        PrintJobItem *item = [[PrintJobManager sharedPrintJobManager] getJobItemForPrintJob:job];
        if (item.progress) {
            self.progress = item.progress;
            [self updateProgress:NO];
            [self registerForProgressUpdates];
        }
    }
}

- (void)resizePageCount:(PrintJob *)job
{
    // Resize the label to hold the text
    [self.labelPageCount setText:[NSString stringWithFormat:@"%ld", (long)[job getJobPageCountValue]]];
    CGSize maxSize = CGSizeMake(30, self.labelPageCount.frame.size.height);
    CGSize requiredSize = [self.labelPageCount sizeThatFits:maxSize];

    // Align the label and image to the right of the cell
    [self.labelPageCount setFrame:CGRectMake(self.frame.size.width - 8 - requiredSize.width, self.labelPageCount.frame.origin.y, requiredSize.width, self.labelPageCount.frame.size.height)];
    [self.imagePage setFrame:CGRectMake(self.labelPageCount.frame.origin.x - self.imagePage.frame.size.width - 2, self.imagePage.frame.origin.y, self.imagePage.frame.size.width, self.imagePage.frame.size.height)];
}

- (void)registerForProgressUpdates
{
    if (self.progress) {
        [self.progress addObserver:self forKeyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew context:NULL];
    }
}

- (void)unregisterProgressUpdates
{
    if (self.progress) {
        [self.progress removeObserver:self forKeyPath:@"fractionCompleted" context:NULL];
        self.progress = nil;
    }
}

- (void)updateProgress:(BOOL)animated
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.progressUpload setProgress:self.progress.fractionCompleted animated:animated];
        [self.labelUpload setText:[NSString stringWithFormat:@"%.0f%%", self.progress.fractionCompleted * 100]];
    });
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:self.progress] && [keyPath isEqualToString:@"fractionCompleted"]) {
        [self updateProgress:YES];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self unregisterProgressUpdates];
}

- (void)dealloc
{
    [self unregisterProgressUpdates];
}

@end
