//
//  CapabilitiesCell.m
//  PrinterOn
//
//  Created by Mark Burns on 11/14/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "CapabilitiesCell.h"

@implementation CapabilitiesCell

- (void)setupCell:(NSDictionary *)data {
    if (data == nil) return;

    [self.image setImage:[[ImageManager sharedImageManager] imageNamed:data[@"image"]]];
    [self.labelTitle setText:data[@"title"]];
    [self.labelValue setText:data[@"value"]];
}

@end
