//
//  PONAnnotation.h
//  PrinterOn
//
//  Created by Mark Burns on 1/29/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@class Printer;

@interface PONAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, strong) Printer *printer;

@end
