//
//  SplashScreen.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-10.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "SplashScreen.h"

#define IS_IPAD                 (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_PORTRAIT             UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)

static UIViewController* __originalRootViewController = nil;
static UIWindow* __splashWindow = nil;
static CALayer* __splashLayer = nil;
static CALayer* __copiedRootLayer = nil;
static UIImageView* __secureScreenshot = nil;

@interface _SplashScreenViewController : UIViewController

@property (nonatomic) BOOL rotationEnabled;

@end

@implementation _SplashScreenViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        _rotationEnabled = YES;
    }
    return self;
}

- (BOOL)shouldAutorotate
{
    return _rotationEnabled;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return _rotationEnabled;
}

@end

@implementation SplashScreen

+ (UIImage *)_preferredSplashImage
{
    UIImage *splashImage = nil;
    
    // Xcode5 AssetCatalog LaunchImage filenames
    if (!splashImage) {
        NSMutableString* imageName = @"LaunchImage".mutableCopy;
        
        [imageName appendString:@"-700"];

        if (IS_SCREEN_R4) {
            [imageName appendString:@"-568h"];
        }
        else if (IS_IPAD) {
            if (IS_PORTRAIT) {
                [imageName appendString:@"-Portrait"];
            }
            else {
                [imageName appendString:@"-Landscape"];
            }
        }
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [UIScreen mainScreen].scale > 1) {
            [imageName appendString:@"@2x"];
        }

        splashImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"png"]];
    }
    
    return splashImage;
}

+ (BOOL)_hidesStatusBarDuringAppLaunch
{
    return [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"UIStatusBarHidden"] boolValue];
}

+ (void)show
{
    UIWindow* splashWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    splashWindow.backgroundColor = [UIColor blackColor];    // set black to not show mainWindow
    
    _SplashScreenViewController* splashRootVC = [_SplashScreenViewController new];
    splashRootVC.view.backgroundColor = [UIColor clearColor];
    splashWindow.rootViewController = splashRootVC;
    
    CALayer* splashLayer = [CALayer layer];
    UIImage *splashImage = [self _preferredSplashImage];
    splashLayer.contents = (id)splashImage.CGImage;
    [splashRootVC.view.layer addSublayer:splashLayer];
    
    [splashWindow makeKeyAndVisible];
    
    // adjust frame after makeKeyAndVisible (splashRootVC.view is ready)
    splashLayer.frame = CGRectMake(0,
                                   splashRootVC.view.bounds.size.height-splashImage.size.height,  // mostly 0 or -20
                                   splashImage.size.width,
                                   splashImage.size.height);
    
    if ([self _hidesStatusBarDuringAppLaunch]) {
        // above statusBar (will be set to UIWindowLevelStatusBar-1 on hide)
        splashWindow.windowLevel = UIWindowLevelStatusBar+1;
    }
    else {
        // below statusBar
        splashWindow.windowLevel = UIWindowLevelStatusBar-1;
    }
    
    __splashWindow = splashWindow;
    __splashLayer = splashLayer;
    
    // lock rotation while showing
    splashRootVC.rotationEnabled = NO;
}

+ (void)hide
{
    [self _prepareForAnimation];
    
    // perform hiding animation after iOS7-fading animation finished
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self _performAnimation];
    });
}

#pragma mark - Secure Screenshot

+ (void)showSecureScreenshot:(UIWindow *)window
{
    if (window) {
        CGRect frame = [window frame];

        __secureScreenshot = [[UIImageView alloc]initWithFrame:frame];
        [__secureScreenshot setImage:[self _preferredSplashImage]];

        if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            [[self class] rotateAccordingToStatusBarOrientationAndSupportedOrientations:__secureScreenshot];
        }

        [window addSubview:__secureScreenshot];
        [window bringSubviewToFront:__secureScreenshot];
    }
}

+ (void)hideSecureScreenshot
{
    if (__secureScreenshot != nil) {
        [__secureScreenshot removeFromSuperview];
        __secureScreenshot = nil;
    }
}

+ (void)rotateAccordingToStatusBarOrientationAndSupportedOrientations:(UIView *)view
{
    UIInterfaceOrientation statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
    CGFloat angle = UIInterfaceOrientationAngleOfOrientation(statusBarOrientation);

    CGAffineTransform transform = CGAffineTransformMakeRotation(angle);

    [self setIfNotEqualTransform:transform view:view];
}

+ (void)setIfNotEqualTransform:(CGAffineTransform)transform view:(UIView *)view
{
    CGRect frame = view.frame;
    if(!CGAffineTransformEqualToTransform(view.transform, transform)) {
        view.transform = transform;
    }

    if(!CGRectEqualToRect(view.frame, frame)) {
        view.frame = frame;
    }
}

CGFloat UIInterfaceOrientationAngleOfOrientation(UIInterfaceOrientation orientation)
{
    CGFloat angle;

    switch (orientation) {
        case UIInterfaceOrientationPortraitUpsideDown:
            angle = M_PI;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            angle = -M_PI_2;
            break;
        case UIInterfaceOrientationLandscapeRight:
            angle = M_PI_2;
            break;
        default:
            angle = 0.0;
            break;
    }

    return angle;
}

UIInterfaceOrientationMask UIInterfaceOrientationMaskFromOrientation(UIInterfaceOrientation orientation)
{
    return 1 << orientation;
}

#pragma mark -
#pragma mark Private

+ (void)_prepareForAnimation
{
    [self attachRootViewController];

    // temporarily switch mainWindow to create rootViewController.view
    UIWindow* mainWindow = [UIApplication sharedApplication].delegate.window;
    [mainWindow makeKeyAndVisible];
    [__splashWindow makeKeyAndVisible];

    // move below statusBar
    __splashWindow.windowLevel = UIWindowLevelStatusBar-1;

    // create rootView snapshot
    UIGraphicsBeginImageContextWithOptions(mainWindow.bounds.size, NO, 0);

    if ([mainWindow respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        // tints tabBar-background in iOS7
        [mainWindow drawViewHierarchyInRect:mainWindow.bounds afterScreenUpdates:YES];
    } else {
        CALayer* rootLayer = mainWindow.layer;
        CGContextRef context = UIGraphicsGetCurrentContext();
        // doesn't tint tabBar-background in iOS7
        [rootLayer renderInContext:context];
    }
    UIImage* rootLayerImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CALayer* copiedRootLayer = [CALayer layer];
    copiedRootLayer.frame = [mainWindow convertRect:mainWindow.bounds toView:__splashWindow.rootViewController.view];
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    copiedRootLayer.contents = (id)rootLayerImage.CGImage;
    [__splashWindow.rootViewController.view.layer insertSublayer:copiedRootLayer atIndex:0];
    [CATransaction commit];
    
    __copiedRootLayer = copiedRootLayer;
}

+ (void)_performAnimation
{
    [UIView transitionWithView:__splashWindow.rootViewController.view
                      duration:0.8
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^{
                        [__splashLayer removeFromSuperlayer];
                    }
                    completion:^(BOOL finished) {
                        if ([ThemeLoader boolForKey:@"StatusBar.isLight"]) {
                            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
                        }
                        
                        if (finished) {
                            // clean up
                            [__copiedRootLayer removeFromSuperlayer];
                            __copiedRootLayer = nil;
                            __splashLayer = nil;
                            __splashWindow = nil;
                            __originalRootViewController = nil;
                            
                            UIWindow* mainWindow = [UIApplication sharedApplication].delegate.window;
                            [mainWindow makeKeyAndVisible];
                        }
                    }
     ];
}

@end

#pragma mark -

@implementation SplashScreen (RootDetaching)

+ (void)detachRootViewController
{
    if (!__originalRootViewController) {
        
        UIWindow* mainWindow = [UIApplication sharedApplication].delegate.window;
        __originalRootViewController = mainWindow.rootViewController;
        
        // add dummy rootViewController to prevent console warning
        // "Applications are expected to have a root view controller at the end of application launch".
        mainWindow.rootViewController = [UIViewController new];
        
    }
}

+ (void)attachRootViewController
{
    if (__originalRootViewController) {
        UIWindow* mainWindow = [UIApplication sharedApplication].delegate.window;
        mainWindow.rootViewController = __originalRootViewController;
    }
}

@end
