//
//  SplashScreen.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-10.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface SplashScreen : NSObject

+ (void)show;
+ (void)hide;

+ (void)showSecureScreenshot:(UIWindow *)window;
+ (void)hideSecureScreenshot;

@end

@interface SplashScreen (RootDetaching)

+ (void)detachRootViewController;
+ (void)attachRootViewController;

@end
