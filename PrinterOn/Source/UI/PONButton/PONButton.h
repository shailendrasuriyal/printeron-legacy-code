//
//  PONButton.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-27.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface PONButton : UIButton

@property (nonatomic, copy) UIColor *highlightColor;
@property (nonatomic, copy) UIColor *originalBackgroundColor;

@end
