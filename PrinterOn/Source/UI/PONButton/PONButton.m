//
//  PONButton.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-27.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "PONButton.h"

@implementation PONButton

- (instancetype)init
{
    if (self = [super init]) {
        [self setupListeners];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setupListeners];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupListeners];
    }
    return self;
}

- (void)dealloc
{
    [self removeTarget:self action:@selector(buttonPressedDown) forControlEvents:UIControlEventTouchDown];
    [self removeTarget:self action:@selector(buttonPressedUp) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
}

- (void)setupListeners
{
    [self addTarget:self action:@selector(buttonPressedDown) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(buttonPressedUp) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
}

- (void)buttonPressedDown
{
    [UIView animateWithDuration:0.15f animations:^{
        self.backgroundColor = self.highlightColor;
    } completion:nil];
}

- (void)buttonPressedUp
{
    [UIView animateWithDuration:0.15f animations:^{
        self.backgroundColor = self.originalBackgroundColor ?: [UIColor clearColor];
    } completion:nil];
}

@end
