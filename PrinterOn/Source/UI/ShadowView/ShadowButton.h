//
//  ShadowButton.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-03-25.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface ShadowButton : UIButton

- (void)setShadowWithRadius:(CGFloat)radius withColor:(UIColor *)color withOffset:(CGSize)offset withOpacity:(float)opacity;
- (void)hideShadow;

@end
