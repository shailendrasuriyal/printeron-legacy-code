//
//  PreviewPageView.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-06-23.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "PreviewPageView.h"

#import "ShadowView.h"

@interface PreviewPageView ()

@property (nonatomic, strong) ShadowView *backgroundView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation PreviewPageView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _backgroundView = [[ShadowView alloc] initWithFrame:CGRectMake(frame.origin.x + 2, frame.origin.y + 2, frame.size.width - 6, frame.size.height - 6)];
        _backgroundView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_backgroundView];

        _pageImage = [[UIImageView alloc] initWithFrame:CGRectMake(frame.origin.x + 2, frame.origin.y + 2, frame.size.width - 6, frame.size.height - 6)];
        _pageImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_pageImage];

        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicator.frame = CGRectMake((self.frame.size.width - _activityIndicator.frame.size.width) / 2, (self.frame.size.height - _activityIndicator.frame.size.height) / 2, _activityIndicator.frame.size.width, _activityIndicator.frame.size.height);
        _activityIndicator.hidesWhenStopped = YES;
        [self addSubview:_activityIndicator];
    }

    return self;
}

- (void)startActivityIndicator
{
    if (!_activityIndicator.isAnimating) {
        [_activityIndicator startAnimating];
    }
}

- (void)stopActivityIndicator
{
    [_activityIndicator stopAnimating];
}

@end
