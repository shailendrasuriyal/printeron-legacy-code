//
//  PreviewPageView.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-06-23.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

@interface PreviewPageView : UIView

@property (nonatomic, strong) UIImageView *pageImage;

- (void)startActivityIndicator;
- (void)stopActivityIndicator;

@end
