//
//  ShadowView.h
//  PrinterOn
//
//  Created by Mark Burns on 11/5/2013.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface ShadowView : UIView

- (void)setShadowWithRadius:(CGFloat)radius withColor:(UIColor *)color withOffset:(CGSize)offset withOpacity:(float)opacity;
- (void)hideShadow;

@end
