//
//  ShadowButton.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-03-25.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ShadowButton.h"

@interface ShadowButton()

@property (nonatomic, assign) BOOL hasShadow;

@end

@implementation ShadowButton

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setDefaultShadow];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setDefaultShadow];
    }
    return self;
}

- (void)setDefaultShadow
{
    self.hasShadow = YES;
    
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    self.layer.shadowOpacity = 0.75f;
    self.layer.shadowRadius = 2.0f;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
}

- (void)setShadowWithRadius:(CGFloat)radius withColor:(UIColor *)color withOffset:(CGSize)offset withOpacity:(float)opacity {
    self.hasShadow = YES;
    
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [color CGColor];
    self.layer.shadowOffset = offset;
    self.layer.shadowOpacity = opacity;
    self.layer.shadowRadius = radius;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
}

- (void)hideShadow {
    self.hasShadow = NO;
    
    self.layer.shadowOpacity = 0.0f;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.hasShadow) {
        self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    }
}

@end
