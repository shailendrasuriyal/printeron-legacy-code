//
//  MBBarButtonItem.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "BarButtonItem.h"

@implementation BarButtonItem

+ (void)customizeLeftBarButton:(UIButton *)item withImage:(UIImage *)image {
    [item setImage:image forState:UIControlStateNormal];
    [item setImageEdgeInsets:UIEdgeInsetsMake(6, 0, 6, 20)];
}

+ (void)customizeRightBarButton:(UIButton *)item withImage:(UIImage *)image {
    [item setImage:image forState:UIControlStateNormal];
    [item setImageEdgeInsets:UIEdgeInsetsMake(6, 20, 6, 0)];
}

+ (UIImage *)drawButtonForRect:(CGRect)rect {
    CGFloat drawScale = [[UIScreen mainScreen] scale];
    
	UIGraphicsBeginImageContextWithOptions(rect.size, NO, drawScale);
	CGContextRef context = UIGraphicsGetCurrentContext();

    CGRect thumbRect = CGRectMake(1.5f, 1.5f, rect.size.width-3, rect.size.height-3);
    CGFloat cornerRadius = 4;

    CGPathRef strokePath = [UIBezierPath bezierPathWithRoundedRect:thumbRect cornerRadius:cornerRadius-1.5].CGPath;

    CGContextAddPath(context, strokePath);
    CGContextAddPath(context, [UIBezierPath bezierPathWithRoundedRect:CGRectInset(thumbRect, 1, 1) cornerRadius:cornerRadius-2.5].CGPath);
    CGContextEOClip(context);

    CGFloat strokeComponents[4] = {1, 0.1, 1, 0.05};

    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGGradientRef strokeGradient = CGGradientCreateWithColorComponents(colorSpace, strokeComponents, NULL, 2);
    CGContextDrawLinearGradient(context, strokeGradient, CGPointMake(0,0), CGPointMake(0,CGRectGetHeight(rect)), 0);
    CGGradientRelease(strokeGradient);
    CGColorSpaceRelease(colorSpace);

	CGImageRef image = CGBitmapContextCreateImage(context);
	UIImage *newImage = [[UIImage alloc] initWithCGImage:image scale:drawScale orientation:UIImageOrientationUp];
	CGImageRelease(image);

	UIGraphicsPopContext();
    
	return newImage;
}

@end
