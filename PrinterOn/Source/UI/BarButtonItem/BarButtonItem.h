//
//  MBBarButtonItem.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

@interface BarButtonItem : NSObject

+ (void)customizeLeftBarButton:(UIButton *)item withImage:(UIImage *)image;
+ (void)customizeRightBarButton:(UIButton *)item withImage:(UIImage *)image;

@end
