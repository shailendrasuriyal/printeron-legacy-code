//
//  AppDelegate.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AppDelegate.h"

#import "AppConstants.h"
#import "AuthViewController.h"
#import "DefaultServiceViewController.h"
#import "GAI.h"
#import "LoadPrinterViewController.h"
#import "MainViewController.h"
#import "Migration.h"
#import "NetworkBrowser.h"
#import "NSString+Parameters.h"
#import "NSString+URL.h"
#import "PONBrowserURLProtocol.h"
#import "PreviewViewController.h"
#import "PrintersViewController.h"
#import "PrintJob.h"
#import "PrintJobManager.h"
#import "PrintJobSessionItem.h"
#import "PrintJobSessionManager.h"
#import "RKXMLReaderSerialization.h"
#import "Service.h"
#import "SplashScreen.h"
#import "UIResponder+KeyboardCache.h"

#import "AppAuth.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.showSplash = YES;
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (self.showSplash) [SplashScreen show];

    [self setupAnalytics];
    [CoreDataManager setupCoreData];
    [Migration performMigration];
    [self setupUserAgent];
    [self setupRestKit];
    [self setupManagers];
    [self setupTheme];

    // Invoke any custom setup that needs to be done by a subclass
    [self customSetup:launchOptions];

    // Start the Network Discovery Browser
    [NetworkBrowser startNetworkSearch];

    // Cache the keyboard so it doesn't stutter and freeze the app the first time the keyboard is opened
    [UIResponder cacheKeyboard:YES];

    // Handle an "Open In..." launch
    // This returns before cleanDocuments so that we don't delete the document file before it is handled
    if (launchOptions && launchOptions[UIApplicationLaunchOptionsURLKey]) {
        return YES;
    }

    // Clean out app temp files
    [self cleanDocuments];

    // Handle a launch that originated from the user interacting with a local system notification
    if (launchOptions && launchOptions[UIApplicationLaunchOptionsLocalNotificationKey]) {
        [self receivedLocalNotification:launchOptions[UIApplicationLaunchOptionsLocalNotificationKey] fromLaunch:YES];
    }

    // The launch wasn't handled so we will be opening to the main screen
    [PrintJobManager sharedPrintJobManager].mainView.wasOpenedIn = self.showSplash;

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

    // Obscure the screen in the task manager when the app is backgrounded
    [SplashScreen showSecureScreenshot:self.window];

    // When we transition to the background fire off any print job timers remaining.  We need to do this or the timers
    // will not fire until the app is foregrounded.  By firing them early this will transition the jobs to the background
    // processing system.
    [[PrintJobManager sharedPrintJobManager] didEnterBackground];

    // Stop the Network Discovery Browser
    [NetworkBrowser stopNetworkSearch];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

    // Restart the Network Discovery Browser
    [NetworkBrowser startNetworkSearch];

    // Check for new print jobs and check the health of any existing print jobs
    [[PrintJobManager sharedPrintJobManager] willEnterForeground];

    // Remove the secure task manager screen when the app is returned to the foreground
    [SplashScreen hideSecureScreenshot];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
    if ([shortcutItem.type isEqualToString:@"com.printeron.scancode"]) {
        // Load the view from the Storyboard
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
        UINavigationController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"scanCode"];

        // Close all views back to root and launch into the desired view
        if ([self.window.rootViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *root = (UINavigationController *)self.window.rootViewController;
            [root dismissViewControllerAnimated:NO completion:nil];
            [root popToRootViewControllerAnimated:NO];

            // Launch into the desired view
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                controller.modalPresentationStyle = UIModalPresentationFormSheet;
            }

            [root presentViewController:controller animated:NO completion:nil];
        }
    }
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    NSString *jobUUID = [[PrintJobSessionManager sharedPrintJobSessionManager] getUUIDFromIdentifier:identifier];
    PrintJobSessionItem *item = [[PrintJobSessionManager sharedPrintJobSessionManager] getJobItemForJobIdentifier:jobUUID];
    if (item == nil) {
        [[PrintJobSessionManager sharedPrintJobSessionManager] recreatePrintJobItem:jobUUID];
    }

    //  Save the completion handler for a session so that it can be called once the session is finished all tasks.
    item.backgroundSessionCompletionHandler = completionHandler;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options
{
    return [self openURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [self openURL:url];
}

- (BOOL)openURL:(NSURL *)url
{
    if (url.fileURL) {
        PrintDocument *document = [PrintDocument getDocumentFromFile:url fromSource:NSLocalizedPONString(@"LABEL_SOURCE_OPENIN", nil)];
        if (document) {
            // Load the view from the Storyboard
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];

            PreviewViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"PrintPreview"];

            // Set any options
            controller.document = document;
            controller.wasOpenedIn = self.showSplash;

            // Close all views back to root and launch into the desired view
            if ([self.window.rootViewController isKindOfClass:[UINavigationController class]]) {
                UINavigationController *root = (UINavigationController *)self.window.rootViewController;
                [root dismissViewControllerAnimated:NO completion:nil];
                [root popToRootViewControllerAnimated:NO];
                [root pushViewController:controller animated:NO];

                return YES;
            }
        }
    }

    if ([url.scheme.lowercaseString isEqualToString:@"com.googleusercontent.apps.102917463087-f4vgp3fl149jf8fjq8g78jie7jo9qi7p"]) {
        if (self.currentAuthorizationBlock) {
            self.currentAuthorizationBlock();
        }
        BOOL authFlow = NO;
        @try {
            authFlow = [self.currentAuthorizationFlow resumeAuthorizationFlowWithURL:url];
        } @catch (NSException *exception) {
        } @finally {
            self.currentAuthorizationFlow = nil;
            self.currentAuthorizationBlock = nil;
        }

        if (authFlow) return YES;
    }

    if ([self doesMatchURLScheme:url.scheme]) {
        // Create a dictionary from the URL querystring
        NSDictionary *parameters = [[url.query URLDecode] parametersSeparatedByInnerString:@"=" andOuterString:@"&" lowerCaseKeys:YES];

        UIViewController *controller = [self launchFromParameters:parameters];

        if (controller) {
            // Close all views back to root and launch into the desired view
            if ([self.window.rootViewController isKindOfClass:[UINavigationController class]]) {
                UINavigationController *root = (UINavigationController *)self.window.rootViewController;
                [root dismissViewControllerAnimated:NO completion:nil];
                [root popToRootViewControllerAnimated:NO];

                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [controller isKindOfClass:[UINavigationController class]]) {
                    controller.modalPresentationStyle = UIModalPresentationFormSheet;
                    [root presentViewController:controller animated:NO completion:nil];
                    return YES;
                }

                [root pushViewController:controller animated:NO];
                return YES;
            }
        }
    }

    // The launch wasn't handled so we will actually be opening to the main screen
    [PrintJobManager sharedPrintJobManager].mainView.wasOpenedIn = self.showSplash;

    return NO;
}

- (BOOL)doesMatchURLScheme:(NSString *)scheme {
    NSArray *urlTypes = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleURLTypes"];
    if (urlTypes) {
        for (NSDictionary *urlType in urlTypes) {
            NSArray *urlSchemes = urlType[@"CFBundleURLSchemes"];
            if (urlSchemes) {
                for(NSString *urlScheme in urlSchemes)
                    if([urlScheme caseInsensitiveCompare:scheme] == NSOrderedSame)
                        return YES;
            }
            
        }
    }
    return NO;
}

- (UIViewController *)launchFromParameters:(NSDictionary *)parameters
{
    NSString *serviceURL = parameters[@"surl"];
    if (serviceURL.length > 0) {
        // Check to see if we have a restricted service URL
        Service *restricted = [Service getRestrictedService];
        if (restricted) {
            NSString *URL = [Service cleanServiceURL:serviceURL];
            if (![Service compareURL:URL toURL:restricted.serviceURL]) {
                // If the service URL is equal to the restricted URL then we can continue, otherwise we return and do nothing
                return nil;
            }
        }

        // Check to see if we should set the service as default
        NSString *serviceDefault = parameters[@"sdef"];
        if (serviceDefault.length > 0 && serviceDefault.intValue == 1) {
            BOOL setService = YES;

            Service *mdm = [Service getMDMService];
            if (mdm && mdm.isDefault.boolValue && mdm.serviceLock.boolValue) {
                setService = NO;
            } else {
                NSArray *results = [Service doesServiceExist:serviceURL inContext:nil];
                if (results.count > 0) {
                    Service *service = results[0];
                    if (service.isDefault.boolValue) {
                        setService = NO;
                    }
                }
            }

            if (setService) {
                // Load the view from the Storyboard
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
                DefaultServiceViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"defaultServiceScan"];
                controller.parameters = parameters;
                controller.wasOpenedIn = self.showSplash;
                return controller;
            }
        }

        // Check to see if we should ask for authentication information
        NSString *serviceAuth = parameters[@"sauth"];
        if (serviceAuth.length > 0 && serviceAuth.intValue == 1) {
            UserAccount *userAccount = [UserAccount getUserAccountForURL:[NSURL URLWithString:serviceURL]];
            if (userAccount && userAccount.isAnonymous.boolValue) {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
                AuthViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"searchUserAuth"];
                controller.parameters = parameters;
                controller.wasOpenedIn = self.showSplash;
                return controller;
            }
        }

        // Check if we should do a search for a printer
        NSString *printerID = parameters[@"pid"];
        if (printerID.length > 0) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
            LoadPrinterViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoadPrinter"];
            controller.parameters = parameters;
            controller.wasOpenedIn = self.showSplash;
            return controller;
        }
    }

    // Check to see if we should open the printer selection screen
    NSString *morePrinters = parameters[@"moreprinters"];
    if (morePrinters.intValue == 1) {
        // Load the view from the Storyboard
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            UINavigationController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"openPrinters"];
            ((PrintersViewController *)controller.topViewController).wasOpenedIn = self.showSplash;
            return controller;
        } else {
            PrintersViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"openPrinters"];
            controller.wasOpenedIn = self.showSplash;
            return controller;
        }
    }

    return nil;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [self receivedLocalNotification:notification fromLaunch:NO];
}

- (void)receivedLocalNotification:(UILocalNotification *)notification fromLaunch:(BOOL)launched
{
    // Close all views back to root and launch into the desired view
    if ([self.window.rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *root = (UINavigationController *)self.window.rootViewController;
        [root dismissViewControllerAnimated:NO completion:nil];
        [root popToRootViewControllerAnimated:NO];
    }

    PrintJob *job = [PrintJob fetchPrintJobByUUID:notification.userInfo[@"jobUUID"] inContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext];
    [PrintJobManager sharedPrintJobManager].mainView.showPrintJob = job;
}

- (void)customSetup:(NSDictionary *)launchOptions
{
    // Override this method and include any custom setup you need to do in a subclass.
}

#pragma mark - Setup Analytics

- (void)setupAnalytics
{
    // Set the app-level opt out based on user settings
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"sendDiagnosticData"] == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"sendDiagnosticData"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [GAI sharedInstance].optOut = NO;
    } else {
        BOOL value = [[[NSUserDefaults standardUserDefaults] stringForKey:@"sendDiagnosticData"] isEqualToString:@"YES"] ? NO : YES;
        [GAI sharedInstance].optOut = value;
    }

    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;

    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;

    // Optional: set dryRun mode which prevents any data from being sent to Google Analytics.
    #ifdef DEBUG
        // Set dryRun mode in Debug builds
        [GAI sharedInstance].dryRun = YES;
    #endif

    // Optional: set Logger level.
    [GAI sharedInstance].logger.logLevel = kGAILogLevelNone;

    // Initialize tracker.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-50225703-1"];
}

#pragma mark - Setup RestKit

- (void)setupRestKit
{
    #ifdef DEBUG
        // Set RestKit logging in Debug builds
        RKLogConfigureByName("*", RKLogLevelError);
    #else
        // Disable RestKit logging in Release builds
        RKLogConfigureByName("*", RKLogLevelOff);
    #endif

    // Setup RestKit to use RKXMLReaderSerialization for parsing "text/xml" responses
    [RKMIMETypeSerialization registerClass:[RKXMLReaderSerialization class] forMIMEType:@"text/xml"];
}

#pragma mark - Setup Theme

- (void)setupTheme
{
    [ThemeLoader configThemeWithPlist:@"Theme"];

    if (self.showSplash == NO) {
        // Set status bar style
        if ([ThemeLoader boolForKey:@"StatusBar.isLight"]) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
    }
}

#pragma mark - Setup Managers

- (void)setupManagers
{
    // Add custom browser URL Protocol
    [NSURLProtocol registerClass:[PONBrowserURLProtocol class]];

    // Setup the Wormhole Manager for IPC
    [WormholeManager sharedWormholeManager];

    // Set the bundle identifier to load image resources from
    [[ImageManager sharedImageManager] setBundleIdentifier:kImagesBundleIdentifier];

    // Start up the PrintJobManager
    [PrintJobManager sharedPrintJobManager];
}

#pragma mark - Setup UserAgent

- (void)setupUserAgent
{
    // Get the default UserAgent from UIWebView to modify
    UIWebView *webview = [UIWebView new];
    NSString *defaultAgent = [webview stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    webview = nil;

    // Modify the default UserAgent to include PrinterOn information
    defaultAgent = [defaultAgent stringByAppendingString:[NSString stringWithFormat:@" PONMobile/%@ (%@)", [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"], [NSString stringWithUTF8String:getprogname()]]];

    // Set the UserAgent to be used for the application by UIWebView
    NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys:defaultAgent, @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].lastObject;
}

// Clean out any unused documents that may be around.
- (void)cleanDocuments
{
    BOOL isDir;

    NSString *inboxPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"Inbox"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:inboxPath isDirectory:&isDir] && isDir) {
        [self deleteFilesAtPath:inboxPath];
    }

    NSString *tempPath = NSTemporaryDirectory();
    if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath isDirectory:&isDir] && isDir) {
        [self deleteFilesAtPath:tempPath];
    }
}

- (void)deleteFilesAtPath:(NSString *)path
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSArray *fileArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
        if (!fileArray) return;

        for (NSString *filename in fileArray) {
            [[NSFileManager defaultManager] removeItemAtPath:[path stringByAppendingPathComponent:filename] error:nil];
        }
    });
}

#pragma mark - COSTouchVisualizer

- (COSTouchVisualizerWindow *)window
{
    static COSTouchVisualizerWindow *visWindow = nil;
    if (!visWindow) {
        visWindow = [[COSTouchVisualizerWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        visWindow.touchVisualizerWindowDelegate = self;
        [visWindow setRippleFillColor:[UIColor colorWithRed:(CGFloat)0/255.0f green:(CGFloat)174/255.0f blue:(CGFloat)239/255.0f alpha:1.0f]];
    }
    return visWindow;
}

- (BOOL)touchVisualizerWindowShouldAlwaysShowFingertip:(COSTouchVisualizerWindow *)window {
    return NO;  // Return YES to make the fingertip always display even if there's no mirrored screen.
    // Return NO or don't implement this method if you want to keep the fingertip display only when
    // the device is connected to a mirrored screen.
}

- (BOOL)touchVisualizerWindowShouldShowFingertip:(COSTouchVisualizerWindow *)window {
    return YES;  // Return YES or don't implement this method to make this window show fingertip when necessary.
    // Return NO to make this window not to show fingertip.
}

#pragma mark - Content

- (BOOL)showDocuments
{
    return YES;
}

- (BOOL)showEmail
{
    return YES;
}

- (BOOL)showPhotos
{
    return YES;
}

- (BOOL)showWeb
{
    return YES;
}

@end
