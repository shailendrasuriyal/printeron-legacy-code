//
//  UIDevice+CtxLoggerExtension.h
//  CtxLogger
//
//  Created by Pranav Bhat T on 16/12/16.
//  Copyright © 2016 Citrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AvailabilityMacros.h>
#import <UIKit/UIKit.h>

#define INDICATE_CODE_DEPRECATION DEPRECATED_MSG_ATTRIBUTE("Deprecated in Logger 2.3. Please use the method [CtxLogManager currentMachineCode] instead")
#define INDICATE_NAME_DEPRECATION DEPRECATED_MSG_ATTRIBUTE("Deprecated in Logger 2.3. Please use the method [CtxLogManager currentHardwareName] instead")

@interface UIDevice (CtxLoggerExtension)

+ (NSString *)machineCode INDICATE_CODE_DEPRECATION ;
+ (NSString *)machineName INDICATE_NAME_DEPRECATION ;

@end
