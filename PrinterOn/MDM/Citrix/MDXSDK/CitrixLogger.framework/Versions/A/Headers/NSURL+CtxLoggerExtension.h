//
//  NSURL+CtxLoggerExtension.h
//  CtxLogger
//
//  Created by Pranav Bhat T on 02/08/16.
//  Copyright © 2016 Citrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AvailabilityMacros.h>

#define INDICATE_QUERY_DEPRECATION DEPRECATED_MSG_ATTRIBUTE("Deprecated in Logger 2.3. Please use the method CtxLogManager obfuscateQueriesInUrl:(NSURL*)url.. instead")

@interface NSURL (CtxLoggerExtension)

- (NSString *) ctxStringByObfuscatingQueryParameters INDICATE_QUERY_DEPRECATION ;

@end
