//
//  WorxAPI.h
//
//  Copyright (c) 2016 Citrix Systems Inc. All rights reserved.
//	Updated: 2016/10/05
//

// MDX App delegate protocol methods
#import "MDXCrashInfo.h"
@protocol MdxAppDelegate

@optional
// Shared device call
-(void) didLogoffActiveUser:(NSDictionary*) userInfo;

//Crash reporting call for using HockeyApp API in app
-(void)mdx_didGetCrashWithInfo:(MDXCrashInfo*)mdxCrashInfo;


#define MDX_STATE_MANAGED_FROM_UNMANAGED       @"MdxManagedFromUnmanaged"

// state tansition delegate call
-(void) mdxStateDidChange:(NSString *) newState;

#define MDX_APP_RESOURCE_APPNAME_IMAGE    @"MdxAppNameImage"
#define MDX_APP_RESOURCE_APP_BGIMAGE      @"MdxAppBGImage"

// delegate call to get app specific assets
-(NSString*)appSpecificResourceFile:(NSString*) resourceName;

@end



@interface MdxManager : NSObject

/**
 *  Returns MDX policies values
 *  @note "Premium" or "Activation" app types Worx SDK framework app MUST embed the default_policies.xml file in its app resources. If not present, app will be treated as a managed app.
 *
 *  @param policyName Name of the policy set in MDX Toolkit or in default_policies.xml
 *  @param error Associated error object in case of any failure.
 *
 *  @return If the app is managed, this API will return the policy value set by the XenMobileServer/AppController administrator.
 *          If the app is unmanaged and the app mode is set to either "Premium" or "Activation" mode, this API will return the policy value set in the default_policies.xml.
 If the app is unmanaged and the app mode is "General" App Store mode, this API would return nil.
 */
+ (NSString *) getValueOfPolicy:(NSString *)policyName error:(NSError **)error;


/**
 *  Checks if MDX Agent on the device (Worx Home, Secure Hub) is installed
 *  @param error Associated error object in case of any failure.
 *  @return YES if installed. NO otherwise
 */
+ (BOOL) isMDXAccessManagerInstalled:(NSError **)error;


/**
 *  Checks if the host application is currently managed by MDX
 *  @return YES if the app is managed now. NO if the app is still running unmanaged.
 */
+ (BOOL) isAppManaged;


/**
 *  Initiates MDX Logon request with Mdx Agent (Worx Home, Secure Hub)
 *  @note This logon request is asynchronous and user can expect a flip to the MDX Agent on the device.
 *  @param force If YES, the MDX Agent would force user for a re-login, even if the user is already logged in with active access tokens.
 *  @param error Associated error object in case of any failure.
 *  @return YES if the app did flip to WorxHome for the logon request. NO otherwise.
 */
+ (BOOL) logonMdxWithFlag:(BOOL)force error:(NSError **)error;


/**
 *  Checks if the host application is launched from the Mdx Agent (Worx Home, Secure Hub).
 *  @discussion For accuracy, this API must be called during or after the following UIApplication delegate event calls only:
 *
 *	    a. Any UIApplication FinishLaunching delegate methods like application:willFinishLaunchingWithOptions:, application:didFinishLaunchingWithOptions:, applicationDidFinishLaunching:,
 *
 *	    b. Any UIApplication OpenURL handler delegate methods like application:openURL:sourceApplication:annotation:, application:handleOpenURL:, OR
 *
 *      c. UIApplication applicationDidBecomeActive: delegate method.
 *
 *	@warning MUST NOT query during applicationWillEnterForeground:. API will always return NO.
 *  @return YES if the app was launched by the MDX Agent. NO otherwise.
 */
+ (BOOL) isAppLaunchedByWorxHome;


/**
 *  Provides MDX Toolkit and MDX Library version details.
 *  @note This info is used by apps using HockeyApp APIs for submitting crash reports
 *  @return A dictionary of version details keyed by strings declared right after the API.
 */
+ (NSDictionary *) getMdxVersionInfo;
extern __attribute__((visibility ("default"))) NSString *const kMDXToolkitVersion;
extern __attribute__((visibility ("default"))) NSString *const kMDXToolkitBuildNumber;
extern __attribute__((visibility ("default"))) NSString *const kMDXLibraryVersion;
extern __attribute__((visibility ("default"))) NSString *const kMDXLibraryBuildNumber;
extern __attribute__((visibility ("default"))) NSString *const kMDXBuildJobName;


/**
 *  Returns managed user information
 *  @return A dictionary of managed user details keyed by strings declared right after the API.
 */
+ (NSDictionary *) managedUserInformation;
extern __attribute__((visibility ("default"))) NSString *const kWorxUsername;
extern __attribute__((visibility ("default"))) NSString *const kSharedDeviceEnabled;
extern __attribute__((visibility ("default"))) NSString *const kMdxLdapUserPrincipalName;
extern __attribute__((visibility ("default"))) NSString *const kMdxLdapSAMAccountName;
extern __attribute__((visibility ("default"))) NSString *const kMdxLdapDisplayName;
extern __attribute__((visibility ("default"))) NSString *const kMdxLdapMail;




//// returns a dictionary with the MDX feature flags and their current values
//// values are represented as NSNumbers
+(NSDictionary<NSString *, NSNumber *> *)getMdxFeatureFlags;

//// returns a dictionary with identifier info shared between WH/MDX/Apps
//// The keys for this dictionary are declared below
+(NSDictionary<NSString *, NSString *> *)getSharedIdentifierInfo;
extern __attribute__((visibility ("default"))) NSString *const MdxPersistentDeviceId;
extern __attribute__((visibility ("default"))) NSString *const MdxStoreUserId;
extern __attribute__((visibility ("default"))) NSString *const MdxStoreCustomerId;

@end
