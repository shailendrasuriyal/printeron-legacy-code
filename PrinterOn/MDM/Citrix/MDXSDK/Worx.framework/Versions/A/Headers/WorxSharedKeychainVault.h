//
//  WorxSharedKeychainVault.h
//
//  Copyright (c) 2015 Citrix Systems, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


extern __attribute__((visibility ("default"))) NSString *const kWorxKeychainAccessGroup;
extern __attribute__((visibility ("default"))) NSString *const kWorxUserCertificatesVault;


enum WorxError
{
	WorxError_SharedVaultError_VaultNotFound		= 5101,		/// To handle, re-load or re-initialize the shared vault object...
	WorxError_SharedVaultError_VaultUnreadable		= 5102,		/// Vault couldn't be read/unarchived/deserialized, maybe because encryption policy of app writing & app reading is different;
	WorxError_SharedVaultError_VaultDataMismatch	= 5111,		/// Usually this error is thrown during vault data synchronization; Reason is that data last loaded by a SharedKeychainVault object has been overwritten/updated in the keychain by some other app or another SharedKeychainVault object; To handle this, re-load vault data again and synchronize again.
	WorxError_SharedVaultError_InvalidInput			= 5121,		/// Invalid inputs provided to the APIs; for example, nil vaultItem paramter sent to the updateAndSynchronizeVaultItem:withValue:error: API,
	WorxError_SharedVaultError_AppUnmanaged			= 5131		/// App isn't yet managed by Worx/MDX framework; SharedKeychain vault is currently only accessible in managed mode. 
} ;
typedef enum WorxError WorxError;



@interface WorxSharedKeychainVault : NSObject


/// Initializes, creates vault if doesn't exist, loads vault data....
- (instancetype) initWithVaultName:(NSString*)vaultName accessGroup:(NSString*)accessGroup;

@property(nonatomic,readonly,strong) NSString* vaultName;
@property(nonatomic,readonly,strong) NSString* accessGroup;

@property(nonatomic,readonly) BOOL exists;
@property(nonatomic,readonly) BOOL isAccessible;
@property(nonatomic,readonly) BOOL isVaultDataLoaded;

@property(nonatomic,strong) NSMutableDictionary* vaultData;


//// To load data again...
////	- If possible, you might want to handle error case(s): WorxError_SharedVaultError_VaultUnreadable, keychain specific errors
- (BOOL)loadDataWithError:(NSError *__autoreleasing *)error;


//// To remove vault...
////	- If possible, you might want to handle error case(s): keychain specific errors
- (BOOL)deleteVaultWithError:(NSError *__autoreleasing *)error;


//// To commit changes to vault...
////	- If possible, you might want to handle error case(s): WorxError_SharedVaultError_VaultNotFound, WorxError_SharedVaultError_VaultDataMismatch, keychain specific errors
- (BOOL)synchronizeWithError:(NSError *__autoreleasing *)error;


////	- If possible, you might want to handle error case(s): WorxError_SharedVaultError_InvalidInput, keychain specific errors
- (BOOL)updateAndSynchronizeVaultItem:(NSString*)vaultItem
							withValue:(id)itemValue
								error:(NSError *__autoreleasing *)error;
- (BOOL)updateAndSynchronizeVaultItems:(NSDictionary*)vaultItems
								 error:(NSError *__autoreleasing *)error;



//// Corresponding class methods...
////	- If possible, you might want to handle error case(s): WorxError_SharedVaultError_VaultUnreadable, keychain specific errors
+ (NSDictionary*)getVaultDataFromVault:(NSString*)vaultName
						   accessGroup:(NSString*)accessGroup
								 error:(NSError *__autoreleasing *)error;

////	- If possible, you might want to handle error case(s): WorxError_SharedVaultError_VaultNotFound, WorxError_SharedVaultError_VaultDataMismatch, keychain specific errors
+ (BOOL)saveVaultData:(NSDictionary*)vaultData
			  toVault:(NSString*)vaultName
		  accessGroup:(NSString*)accessGroup
				error:(NSError *__autoreleasing *)error;

////	- If possible, you might want to handle error case(s): keychain specific errors
+ (BOOL)deleteVault:(NSString*)vaultName
		accessGroup:(NSString*)accessGroup
			  error:(NSError *__autoreleasing *)error;

@end
