//
//  MDXCrashInfo.h
//  MDXSDKFramework
//
//  Created by Abhishek kumar Alam on 6/10/16.
//  Copyright © 2016 Citrix Systems, Inc. All rights reserved.
//

#ifndef MDXCrashInfo_h
#define MDXCrashInfo_h

@interface MDXCrashInfo : NSObject
@property (nonatomic, strong) NSString *crashReportContent;    // The contents of a crash report, formatted for iOS (PLCrashReportTextFormatiOS)
@property (nonatomic, strong) NSString *crashUUID;             // This corresponds to "Incident Identifier" of the crash. This info can be obtained by converting PLCrashReport.uuidRef to NSString
@property (nonatomic, strong) NSString *crashedProcessName;    // This is the name of the crashed process and corresponds to PLCrashReport.processInfo.processName
@property (nonatomic, strong) NSDictionary<NSString *, NSNumber *> *mdxFeatureFlags;   // A dictionary containing the mdx feature flags and their current value (it may be empty, but not nil)
@end

#endif /* MDXCrashInfo_h */
