//
//  CXAppDelegate.m
//  Citrix
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "CXAppDelegate.h"

#import "CXCustomProfile.h"

@implementation CXAppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Disable the splash screen because sometimes the Citrix wrap doesn't call the proper flow of app delegates
    // and the splash screen gets stuck permanently on the screen.
    self.showSplash = NO;
    return YES;
}

- (void)customSetup:(NSDictionary *)launchOptions
{
    [CXCustomProfile setCustomSettings];
}

#pragma mark - Content

- (BOOL)showDocuments
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:info];
        NSValue *documentsValue = dict[@"showDocuments"];
        return [documentsValue isEqualToValue:@YES] ? YES : NO;
    }
    
    return NO;
}

- (BOOL)showEmail
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:info];
        NSValue *emailValue = dict[@"showEmail"];
        return [emailValue isEqualToValue:@YES] ? YES : NO;
    }
    
    return NO;
}

@end
