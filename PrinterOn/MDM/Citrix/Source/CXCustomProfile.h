//
//  CXCustomProfile.h
//  PrinterOn
//
//  Created by Mark Burns on 2016-08-08.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "MDMCustomSetting.h"

@interface CXCustomProfile : MDMCustomSetting

+ (void)setCustomSettings;

@end
