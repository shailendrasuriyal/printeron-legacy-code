//
//  CXCustomProfile.m
//  PrinterOn
//
//  Created by Mark Burns on 2016-08-08.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#import "CXCustomProfile.h"

#import <Worx/WorxAPIs.h>

@implementation CXCustomProfile

+ (void)setCustomSettings
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    NSDictionary *user = [MdxManager managedUserInformation];
    dict[@"userName"] = user ? user[@"Username"] : @"";

    BOOL isEnable = NO;
    if ([[MdxManager getValueOfPolicy:@"PONEnable" error:nil].lowercaseString isEqualToString:@"true"]) {
        isEnable = YES;
    }

    NSString *url = [MdxManager getValueOfPolicy:@"PONServiceURL" error:nil];
    if (url == nil) url = @"";

    NSString *desc = [MdxManager getValueOfPolicy:@"PONServiceDescription" error:nil];
    if (desc == nil) desc = @"";

    BOOL isDefault = NO;
    if ([[MdxManager getValueOfPolicy:@"PONServiceDefault" error:nil].lowercaseString isEqualToString:@"true"]) {
        isDefault = YES;
    }

    BOOL isLock = NO;
    if ([[MdxManager getValueOfPolicy:@"PONServiceLock" error:nil].lowercaseString isEqualToString:@"true"]) {
        isLock = YES;
    }

    BOOL isRestricted = NO;
    if ([[MdxManager getValueOfPolicy:@"PONServiceRestrict" error:nil].lowercaseString isEqualToString:@"true"]) {
        isRestricted = YES;
    }

    NSString *adminInfo = [MdxManager getValueOfPolicy:@"PONAdminMessage" error:nil];
    if (adminInfo == nil) adminInfo = @"";

    BOOL showDocuments = NO;
    if ([[MdxManager getValueOfPolicy:@"PONShowDocuments" error:nil].lowercaseString isEqualToString:@"true"]) {
        showDocuments = YES;
    }

    BOOL showEmail = NO;
    if ([[MdxManager getValueOfPolicy:@"PONShowEmail" error:nil].lowercaseString isEqualToString:@"true"]) {
        showEmail = YES;
    }

    dict[@"enable"] = @(isEnable);
    dict[@"serviceUrl"] = url;
    dict[@"serviceDescription"] = desc;
    dict[@"serviceDefault"] = @(isDefault);
    dict[@"serviceLock"] = @(isLock);
    dict[@"serviceRestrict"] = @(isRestricted);
    dict[@"adminInfo"] = adminInfo;
    dict[@"showDocuments"] = @(showDocuments);
    dict[@"showEmail"] = @(showEmail);

    [super set:dict];
}

@end
