//
//  MDMCustomSetting.h
//  PrinterOn
//
//  Created by Harry Han on 2015-05-22.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface MDMCustomSetting : NSObject

+ (void)set:(NSDictionary*)settings;
+ (NSDictionary *)loadPayloadInfo;

@end
