//
//  MDMCustomSetting.m
//  PrinterOn
//
//  Created by Harry Han on 2015-05-22.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "MDMCustomSetting.h"

#import "Service.h"
#import "UserAccount.h"

@implementation MDMCustomSetting

+ (void)set:(NSDictionary *)settings
{
    if (settings == nil) return;

    BOOL changes = [self isPayloadChanged:settings];
    if (changes) {
        NSMutableDictionary *info = [settings mutableCopy];
        info[@"payloadDate"] = [NSDate date];

        NSValue *isEnableValue = settings[@"enable"];
        BOOL enable = [isEnableValue isEqualToValue:@YES] ? YES : NO;
        if (enable) {
            [self updateService:info];
        } else {
            [self deleteService];
        }

        [self savePayloadInfo:info];

        NSString *adminInfo = settings[@"adminInfo"];
        if (adminInfo) adminInfo = [adminInfo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        if (adminInfo.length > 0) {
            UIWindow* window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            window.rootViewController = [UIViewController new];
            window.windowLevel = UIWindowLevelAlert + 1;

            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedPONString(@"LABEL_MDM_PROFILE_UPDATED", nil), adminInfo] preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // This keeps a reference to the window until the action is invoked.
                    window.hidden = YES;
                });
            }];
            [alertVC addAction:cancelAction];

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [window makeKeyAndVisible];
                [window.rootViewController presentViewController:alertVC animated:YES completion:nil];
            });
        }
    }
}

+ (NSDictionary *)loadPayloadInfo
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:info];
    }
    return nil;
}

+ (void)savePayloadInfo:(NSDictionary *)dictionary
{
    NSData *info = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    [[NSUserDefaults standardUserDefaults] setObject:info forKey:@"PONLastPayloadInfo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PONMDMCustomSettingsReceived" object:nil];
}

+ (BOOL)isPayloadChanged:(NSDictionary *)settings
{
    NSDictionary *oldValues = [self loadPayloadInfo];

    NSValue *oldEnableValue = oldValues[@"enable"];
    NSValue *newEnableValue = settings[@"enable"];
    if (![oldEnableValue isEqualToValue:newEnableValue]) return YES;

    NSValue *oldServiceDefaultValue = oldValues[@"serviceDefault"];
    NSValue *newServiceDefaultValue = settings[@"serviceDefault"];
    if (![oldServiceDefaultValue isEqualToValue:newServiceDefaultValue]) return YES;

    NSValue *oldServiceLockValue = oldValues[@"serviceLock"];
    NSValue *newServiceLockValue = settings[@"serviceLock"];
    if (![oldServiceLockValue isEqualToValue:newServiceLockValue]) return YES;

    NSValue *oldServiceRestrictValue = oldValues[@"serviceRestrict"];
    NSValue *newServiceRestrictValue = settings[@"serviceRestrict"];
    if (![oldServiceRestrictValue isEqualToValue:newServiceRestrictValue]) return YES;

    NSValue *oldShowDocumentsValue = oldValues[@"showDocuments"];
    NSValue *newShowDocumentsValue = settings[@"showDocuments"];
    if (![oldShowDocumentsValue isEqualToValue:newShowDocumentsValue]) return YES;

    NSValue *oldShowEmailValue = oldValues[@"showEmail"];
    NSValue *newShowEmailValue = settings[@"showEmail"];
    if (![oldShowEmailValue isEqualToValue:newShowEmailValue]) return YES;

    NSString *oldURL = oldValues[@"serviceUrl"];
    NSString *newURL = settings[@"serviceUrl"];
    if (![oldURL isEqualToString:newURL]) return YES;

    NSString *oldDescr = oldValues[@"serviceDescription"];
    NSString *newDescr = settings[@"serviceDescription"];
    if (![oldDescr isEqualToString:newDescr]) return YES;

    NSString *oldAdminInfo = oldValues[@"adminInfo"];
    NSString *newAdminInfo = settings[@"adminInfo"];
    if (![oldAdminInfo isEqualToString:newAdminInfo]) return YES;

    return NO;
}

+ (void)updateService:(NSMutableDictionary *)settings
{
    NSString *url = settings[@"serviceUrl"];
    if (url == nil) {
        settings[@"payloadError"] = @"Service URL value missing.";
        return;
    }

    NSString *pattern = @"^(http|https)://([-A-Za-z0-9+&@#/%?=~_|!:,.;]+)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];

    if (![regex firstMatchInString:url.lowercaseString options:0 range:NSMakeRange(0, url.length)]) {
        settings[@"payloadError"] = @"Service URL failed validation.";
        return;
    }

    NSString *descr = settings[@"serviceDescription"];

    NSValue *isLockValue = settings[@"serviceLock"];
    BOOL isLock = [isLockValue isEqualToValue:@YES] ? YES : NO;

    NSValue *isDefaultValue = settings[@"serviceDefault"];
    BOOL isDefault = [isDefaultValue isEqualToValue:@YES] ? YES : NO;

    NSValue *isRestrictValue = settings[@"serviceRestrict"];
    BOOL isRestrict = [isRestrictValue isEqualToValue:@YES] ? YES : NO;
    if (isRestrict) {
        isLock = YES;
        isDefault = YES;
    }

    Service *service = [Service getMDMService];
    if (service) {
        BOOL serviceUpdated = [self updateService:service url:url descr:descr isDefault:isDefault isLocked:isLock isRestricted:isRestrict];
        if (serviceUpdated) {
            [service updateService:url description:descr isDefault:isDefault isLocked:isLock isMDM:YES isRestricted:isRestrict completionBlock:nil];
        }
    } else {
        service = [Service createService:url description:descr isDefault:isDefault isLocked:isLock isMDM:YES isRestricted:isRestrict completionBlock:nil];
    }
}

+ (void)deleteService
{
    Service *service = [Service getMDMService];
    if (service) {
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext deleteObject:service];
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];
    }
}

+(BOOL)isUserAccount:(UserAccount *)account
            userName:(NSString*)userName
         hasPassword:(NSString *)password
{
    if (![userName isEqualToString:account.userName]) {
        return false;
    }
    
    if (![password isEqualToString:[account getUserAccountPassword]]) {
        return false;
    }
    
    return YES;
}

+(BOOL)updateService:(Service*)service
                 url:(NSString*)url
               descr:(NSString*)descr
           isDefault:(BOOL)boolDefault
            isLocked:(BOOL)boolLock
        isRestricted:(BOOL)boolRestrict
{

    if ([url isEqualToString:service.serviceURL] &&
        [descr isEqualToString:service.serviceDescription] &&
        [service.isDefault isEqual:@(boolDefault)] &&
        [service.serviceLock isEqual:@(boolLock)] &&
        [service.isRestricted isEqual:@(boolRestrict)]) {
        return NO;
    }
    
    return YES;
}

+ (NSMutableArray *)fetchServices
{
    NSMutableArray *services = [NSMutableArray arrayWithObject:[Service hostedService]];
    
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];
    NSArray *sortDescriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"serviceDescription" ascending:YES]];
    fetchRequest.sortDescriptors = sortDescriptors;
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    
    [services addObjectsFromArray:results];
    return services;
}

@end
