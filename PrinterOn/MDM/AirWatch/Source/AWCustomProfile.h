//
//  AWCustomProfile.h
//  PrinterOn
//
//  Created by Harry Han on 2015-05-19.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "MDMCustomSetting.h"

@interface AWCustomProfile : MDMCustomSetting

+ (void)setCustomSettingWith:(NSString *)payload username:(NSString*)username password:(NSString*)password;

@end
