//
//  AWCustomProfile.m
//  PrinterOn
//
//  Created by Harry Han on 2015-05-19.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "AWCustomProfile.h"

#import "Service.h"
#import "UserAccount.h"

@implementation AWCustomProfile

+ (void)setCustomSettingWith:(NSString *)payload username:(NSString*)username password:(NSString*)password;
{
    NSMutableDictionary *config = [self parseCustomPayload:payload].mutableCopy;
    if (config) {
        config[@"username"] = username;
        config[@"password"] = password;
    }
    
    [super set:config];

#if 0
    
    NSDictionary *dict = [self parseCustomPayload:payload];
  
    NSString *url = dict[@"serviceUrl"];
    BOOL setDefault = NO;
    Service *defaultService = [Service getDefaultService];
    if([defaultService.serviceURL isEqualToString:url] ||
       [Service isHostedString:defaultService.serviceURL andPublic:YES]){
        setDefault = dict[@"isDefault"] ? YES : NO;
    }
    
    Service *service = [Service createService:dict[@"serviceUrl"]
                                  description:dict[@"serviceDescription"]
                                    isDefault:setDefault
                                    isLocked:dict[@"serviceLock"] ? YES : NO
                                    isMDM:YES];
    NSMutableSet *serviceSet;
    
    UserAccount *currentAccount = [UserAccount getMDMUserAccountForURL];
    if(currentAccount){
        serviceSet = [NSMutableSet new];
        NSMutableArray * services = [self fetchServices];
        for (Service *s in services) {
            if([self isUserAccount:s.defaultUser userName:username hasPassword:password description:nil]){
                [serviceSet addObject:s];
            }
        }
        
        BOOL host = currentAccount.isHostedDefault.boolValue;
        [UserAccount updateUserAccount:currentAccount account:username password:password description:nil hostedDefault:host fromMDM:YES defaultServices:serviceSet];
    }else{
        serviceSet = [NSMutableSet new];
        [serviceSet addObject:service];
        [UserAccount createUserAccount:username password:password description:nil hostedDefault:NO fromMDM:YES defaultServices:serviceSet];
    }
#endif
    
}

+ (NSDictionary *)parseCustomPayload:(NSString *)profile
{
    if (profile == nil || profile.length == 0) {
        return nil;
    }
 
    NSData *jsonData = [profile dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];

    return parsedData;
}

+(BOOL)isUserAccount:(UserAccount *)account
         userName:(NSString*)userName
         hasPassword:(NSString *)password
         description:(NSString *)description
{
    if (![userName isEqualToString:account.userName]) {
        return false;
    }
    
    if (![password isEqualToString:[account getUserAccountPassword]]) {
        return false;
    }
    
    return YES;
}

+ (NSMutableArray *)fetchServices
{
    NSMutableArray *services = [NSMutableArray arrayWithObject:[Service hostedService]];
    
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    fetchRequest.entity = [NSEntityDescription entityForName:@"Service" inManagedObjectContext:context];
    NSArray *sortDescriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"serviceDescription" ascending:YES]];
    fetchRequest.sortDescriptors = sortDescriptors;
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    
    [services addObjectsFromArray:results];
    return services;
}

@end
