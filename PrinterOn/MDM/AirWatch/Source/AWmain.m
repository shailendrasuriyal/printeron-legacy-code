//
//  AWmain.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AWAppDelegate class]));
    }
}
