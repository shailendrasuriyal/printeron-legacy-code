//
//  AWAppDelegate.h
//  AirWatch
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AppDelegate.h"
#import <AWSDK/AWController.h>

@interface AWAppDelegate : AppDelegate <UIApplicationDelegate, AWSDKDelegate>

@end
