//
//  AWAppDelegate.m
//  AirWatch
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AWAppDelegate.h"

#import "AWCustomProfile.h"
#import "MainViewController.h"
#import "PrintJobManager.h"

#import <AWSDK/AWSDKCore.h>

@interface AWAppDelegate ()

@property (nonatomic, strong) AWBeacon *beacon;

@end

@implementation AWAppDelegate

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    [super applicationDidBecomeActive:application];

    // Check if AirWatch Agent is installed
    BOOL agentAvailable = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"awagentenroll:"]];

    if (agentAvailable) {
        // Start the AirWatch controller
        [[AWController clientInstance] start];

        // Poll for any new AirWatch Commands
        [[AWCommandManager sharedManager] loadCommands];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options
{
    if ([url.scheme isEqualToString:[AWController clientInstance].callbackScheme]) {
        // Handle callback from AirWatch Agent or Workspace app
        BOOL result = [[AWController clientInstance] handleOpenURL:url fromApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]];

        // The launch was handled by AirWatch so we will be opening to the main screen
        [PrintJobManager sharedPrintJobManager].mainView.wasOpenedIn = self.showSplash;

        return result;
    }

    return [super application:application openURL:url options:options];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([url.scheme isEqualToString:[AWController clientInstance].callbackScheme]) {
        // Handle callback from AirWatch Agent or Workspace app
        BOOL result = [[AWController clientInstance] handleOpenURL:url fromApplication:sourceApplication];

        // The launch was handled by AirWatch so we will be opening to the main screen
        [PrintJobManager sharedPrintJobManager].mainView.wasOpenedIn = self.showSplash;

        return result;
    }

    return [super application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)customSetup:(NSDictionary *)launchOptions
{
    [self setupAirWatch];
}

#pragma mark - AirWatch

- (void)setupAirWatch
{
    // Configure the AirWatch controller
    AWController *controller = [AWController clientInstance];
    controller.callbackScheme = @"ponaw";
    controller.delegate = self;

    // Add observer for when the AirWatch SDK receives any new profiles
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleUpdatedAWProfile:) name:AWNotificationCommandManagerInstalledNewProfile object:nil];
    
}

- (void)handleUpdatedAWProfile:(NSNotification *)aNotification
{
    AWProfile *profile = aNotification.object;
    if (profile == nil) return;

    if (profile.certificatePayload) {
        // This is an Application profile (IE certificate payload)
    } else {
        // Otherwise we know it is an SDK profile
        AWCustomPayload *customPayload = profile.customPayload;
        if (customPayload) {
            NSString *customSettings = customPayload.settings;
            AWEnrollmentAccount *account = [AWController clientInstance].account;
            [AWCustomProfile setCustomSettingWith:customSettings username:account.username password:account.password];
        }
    }
}

#pragma mark - AWSDKDelegate

- (void)initialCheckDoneWithError:(NSError *)error
{
    if (error == nil) {
        // Initialize the Beacon
        self.beacon = [[AWBeacon alloc] initWithAPNSToken:nil transmitInterval:300 locationGroup:nil locationMode:AWLocationModeDisabled distance:kCLDistanceFilterNone];
        
        // Start the Beacon to send periodic pings to the server
        [self.beacon start];
        
        // Send a ping immediately
        [self.beacon send];
        
        // Initialize the DataSampler Configuration
        AWDataSamplerConfiguration *config = [[AWDataSamplerConfiguration alloc] initWithSampleModules:AWDataSamplerModuleSystem defaultSampleInterval:3600 defaultTransmitInterval:14400 traceLevel:AWTraceLevelError];
        
        // Configure the Data Sampler
        [[AWDataSampler mDataSamplerModule] setConfig:config];
        [AWDataSampler mDataSamplerModule].transmitAtStartUp = YES;
        
        // Start the Data Sampler Service
        [[AWDataSampler mDataSamplerModule] startUp:nil];
    }
}

- (void)receivedProfiles:(NSArray *)profiles
{
    AWCommandManager *manager = [AWCommandManager sharedManager];
    AWProfile *profile = manager.sdkProfile;
    
    AWCustomPayload *customPayload = profile.customPayload;
    if(customPayload){
        NSString *customSettings = customPayload.settings;
        AWEnrollmentAccount *account = [AWController clientInstance].account;
        [AWCustomProfile setCustomSettingWith:customSettings username:account.username password:account.password];
    }
}

- (void)unlock
{
}

- (void)lock
{
}

- (void)wipe
{
}

- (void)stopNetworkActivity:(AWNetworkActivityStatus)networkActivityStatus
{
}

- (void)resumeNetworkActivity
{
}

#pragma mark - Content

- (BOOL)showDocuments
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:info];
        NSValue *documentsValue = dict[@"showDocuments"];
        return [documentsValue isEqualToValue:@YES] ? YES : NO;
    }
    
    return NO;
}

- (BOOL)showEmail
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:info];
        NSValue *emailValue = dict[@"showEmail"];
        return [emailValue isEqualToValue:@YES] ? YES : NO;
    }
    
    return NO;
}

@end
