//
//  GDCustomSetting.m
//  PrinterOn
//
//  Created by Harry Han on 2015-05-22.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "GDCustomSetting.h"

@implementation GDCustomSetting

+ (void)setCustomSettingWith:(NSDictionary*)settings email:(NSString*)email
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    dict[@"username"] = email;
    dict[@"password"] = @"";

    NSDictionary *config = settings[@"config"];

    NSNumber *isEnable = config[@"enable"];
    if (isEnable == nil) isEnable = @NO;
    NSString *url = config[@"serviceUrl"];
    if (url == nil) url = @"";
    NSString *desc = config[@"serviceDescription"];
    if (desc == nil) desc = @"";
    NSNumber *isDefault = config[@"serviceDefault"];
    if (isDefault == nil) isDefault = @NO;
    NSNumber *isLock = config[@"serviceLock"];
    if (isLock == nil) isLock = @NO;
    NSNumber *isRestricted = config[@"serviceRestrict"];
    if (isRestricted == nil) isRestricted = @NO;
    NSString *adminInfo = config[@"adminInfo"];
    if (adminInfo == nil) adminInfo = @"";
    NSNumber *showDocuments = config[@"showDocuments"];
    if (showDocuments == nil) showDocuments = @NO;
    NSNumber *showEmail = config[@"showEmail"];
    if (showEmail == nil) showEmail = @NO;

    dict[@"enable"] = isEnable;
    dict[@"serviceUrl"] = url;
    dict[@"serviceDescription"] = desc;
    dict[@"serviceDefault"] = isDefault;
    dict[@"serviceLock"] = isLock;
    dict[@"serviceRestrict"] = isRestricted;
    dict[@"adminInfo"] = adminInfo;
    dict[@"showDocuments"] = showDocuments;
    dict[@"showEmail"] = showEmail;

    [super set:dict];
}

@end
