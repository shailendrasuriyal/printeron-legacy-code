//
//  GDCustomSetting.h
//  PrinterOn
//
//  Created by Harry Han on 2015-05-22.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "MDMCustomSetting.h"

@interface GDCustomSetting : MDMCustomSetting

+ (void)setCustomSettingWith:(NSDictionary*)settings email:(NSString*)email;

@end
