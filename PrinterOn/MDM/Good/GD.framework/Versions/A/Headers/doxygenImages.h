/*
 *  Copyright (c) Visto Corporation dba Good Technology, 2011. All rights reserved.
 *
 *  Documentation only
 */

/** \internal
 * \page imagesdummy Dummy page to force doxygen to install images
 * The images listed here must be manually copied when publishing a
 * non-internal documentation set
 *
 * \image html "gdn_logo.jpg" "GDN logo"
 * \image html "dropdown-arrow.png" "Drop-down arrow"
 * \image html "more-btn.png" "More button"
 * \image html "search-bar.png" "Search bar ends"
 * \image html "search-bar-repeat.png" "Search bar middle"
 */
