/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 *
 */

#pragma once

#import <Foundation/Foundation.h>
#import "GDPortability.h"
#import "GDServiceProvider.h"
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#import <GD/GDiOS.h>
#else
#import "GDMac.h"
#endif

GD_NS_ASSUME_NONNULL_BEGIN

/** Details of a provided service.
 * This class is used to return information about a provided service. The
 * <tt>services</tt> property of a \reflink GDServiceProvider GDServiceProvider\endlink object is a
 * collection of instances of this class.
 */
@interface GDServiceDetail : NSObject

- (id)initWithService:(NSString*)identifier andVersion:(NSString*)version andType:(GDServiceProviderType)type;

@property (nonatomic, strong, readonly) NSString* identifier;
/** Good Dynamics Service Identifier.
 */

@property (nonatomic, strong, readonly) NSString* version;
/** Good Dynamics Service Version.
 */

@property (nonatomic, readonly) GDServiceProviderType type;
/** Indicator of the type of the provided service, either application-based
 * or server-based.
 */

@end


GD_NS_ASSUME_NONNULL_END
