//
//  GDAppDelegate.m
//  Good
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "GDAppDelegate.h"

#import "GDCustomSetting.h"
#import "LoadingViewController.h"
#import "MainViewController.h"
#import "NSString+Parameters.h"
#import "NSString+URL.h"
#import "PreviewViewController.h"
#import "PrintJobManager.h"

#import <GD/GDAppDetail.h>
#import <GD/GDCReadStream.h>
#import <GD/GDFileManager.h>

@interface GDAppDelegate ()

@property (nonatomic, strong) UIViewController *nextController;
@property (strong, nonatomic) GDService *gdService;
@property (strong, nonatomic) NSString *gdApplication;
@property (strong, nonatomic) NSString *gdRequestID;
@property (strong, nonatomic) NSString *filePath;

@end

@implementation GDAppDelegate

@synthesize window = _window;

- (UIWindow *)window
{
    // This makes sure that the GDWindow set never gets overridden
    return _window;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Disable the splash screen because it interferes with the Good launch process
    self.showSplash = NO;
    self.gdService = [GDService new];
    self.gdService.delegate = self;
    return YES;
}

- (void)customSetup:(NSDictionary *)launchOptions
{
    [self setupGood];
}

#pragma mark - Good Dynamics

- (void)setupGood
{
    started = NO;

    self.window = [[GDiOS sharedInstance] getWindow];
    self.good = [GDiOS sharedInstance];
    _good.delegate = self;
  
    //Show the Good Authentication UI.
    [_good authorize];
}

- (void)handleEvent:(GDAppEvent*)anEvent
{
    // Called from Good SDK when events occur, such as system startup.
    switch (anEvent.type) {
        case GDAppEventAuthorized: {
            [self onAuthorized:anEvent];
			break;
        }

        case GDAppEventNotAuthorized: {
            [self onNotAuthorized:anEvent];
			break;
        }

        case GDAppEventRemoteSettingsUpdate: {
            // handle app config changes
            [self onSettingUpdated:anEvent];
			break;
        }

        case GDAppEventServicesUpdate: {
            //A change to services-related configuration.
            break;
        }

        case GDAppEventPolicyUpdate: {
            //A change to one or more application-specific policy settings has been received.
            break;
        }

        case GDAppEventEntitlementsUpdate: {
            // handle entitlements changes
            break ;
        }

        default: {
            NSLog(@"Unhandled Event");
            break;
        }
    }
}

-(void) onNotAuthorized:(GDAppEvent*)anEvent
{
    // Handle the Good Libraries not authorized event.
    switch (anEvent.code) {
        case GDErrorActivationFailed:
        case GDErrorProvisioningFailed:
        case GDErrorPushConnectionTimeout:
        case GDErrorSecurityError:
        case GDErrorAppDenied:
        case GDErrorBlocked:
        case GDErrorWiped:
        case GDErrorRemoteLockout:
        case GDErrorPasswordChangeRequired: {
            // A condition has occured denying authorization, log the event
            NSLog(@"onNotAuthorized %@", anEvent.message);
            break;
        }

        case GDErrorIdleLockout: {
            // idle lockout is benign & informational
            break;
        }

        default: {
            NSAssert(false, @"Unhandled not authorized event");
            break;
        }
    }
}

-(void) onAuthorized:(GDAppEvent*)anEvent
{
    // Handle the Good Libraries authorized event.
    switch (anEvent.code) {
        case GDErrorNone: {
            if (!started) {
                // launch application UI here
                started = YES;

                if (self.nextController) {
                    UINavigationController *root = (UINavigationController *)self.window.rootViewController;
                    [root pushViewController:self.nextController animated:NO];

                    if (showLoading) {
                        // Load the view from the Storyboard
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
                        LoadingViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"loadingFile"];
                        [root pushViewController:controller animated:NO];

                        showLoading = NO;
                    }
                    self.nextController = nil;
                }
                NSString *userId =[[GDiOS sharedInstance] getApplicationConfig][@"userId"];
                [GDCustomSetting setCustomSettingWith:[[GDiOS sharedInstance] getApplicationPolicy] email:userId];
            }
            break;
        }

        default: {
            NSAssert(false, @"Authorized startup with an error");
            break;
        }
    }
}

-(void) onSettingUpdated:(GDAppEvent*)anEvent
{
    switch (anEvent.code) {
        case GDErrorNone: {
            if (!started) {
                NSString *userId =[[GDiOS sharedInstance] getApplicationConfig][@"userId"];
                [GDCustomSetting setCustomSettingWith:[[GDiOS sharedInstance] getApplicationPolicy] email:userId];
            }
            break;
        }

        default: {
            NSAssert(false, @"Setting update with an error");
            break;
        }
    }
}

- (void)showLoadingView
{
    if (started) {
        // Load the view from the Storyboard
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
        LoadingViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"loadingFile"];

        // Close all views back to root and launch into the desired view
        UINavigationController *root = (UINavigationController *)self.window.rootViewController;
        [root popToRootViewControllerAnimated:NO];

        // By adding the loading vc here it is instantly available in the viewcontrollers array.
        // This is important because we could check for the loading view before the dismiss animation is done.
        [root pushViewController:controller animated:NO];

        [root dismissViewControllerAnimated:NO completion:nil];
    } else {
        showLoading = YES;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"Storyboard-iPad" : @"Storyboard-iPhone" bundle:nil];
        MainViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"MainView"];
        self.nextController = controller;
    }
}

- (LoadingViewController *)getLoadingView
{
    UIViewController *vc = self.window.rootViewController;
    if (vc && [vc isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nav = (UINavigationController *)vc;
        for (UIViewController *child in nav.viewControllers) {
            if ([child isMemberOfClass:[LoadingViewController class]]) {
                return (LoadingViewController *)child;
            }
        }
    }
    return nil;
}

#pragma mark - GDService Delegate

- (void) GDServiceWillStartReceivingFrom:(NSString*)application
                          attachmentPath:(NSString*)path
                                fileSize:(NSNumber*)size
                            forRequestID:(NSString*)requestID;
{
    [self showLoadingView];
}

- (void)GDServiceDidReceiveFrom:(NSString *)application
                     forService:(NSString *)service
                    withVersion:(NSString *)version
                      forMethod:(NSString *)method
                     withParams:(id)params
                withAttachments:(NSArray *)attachments
                   forRequestID:(NSString *)requestID
{
    // If the loading view is closed we shouldn't do anything (User cancelled the action).
    __weak LoadingViewController *loadVC = [self getLoadingView];
    if (loadVC == nil) return;

    UINavigationController *root = (UINavigationController *)self.window.rootViewController;

    if (![service isEqualToString:@"com.good.gdservice.print-file"] &&
        ![service isEqualToString:@"com.good.gdservice.transfer-file"]) {
        [root popToRootViewControllerAnimated:YES];
        return;
    }

    if (attachments.count < 1) {
        [root popToRootViewControllerAnimated:YES];
        return;
    }

    self.gdApplication = application;
    self.gdRequestID = requestID;

    [GDService replyTo:application withParams:nil bringClientToFront:GDENoForegroundPreference withAttachments:nil requestID:requestID error:nil];

    if ([PrintDocument isSupportedFileType:attachments[0]]) {
        [self extractReceivedDataFromAttachments:attachments];
    } else {
        // Display the error message
        UIWindow* window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        window.rootViewController = [UIViewController new];
        window.windowLevel = UIWindowLevelAlert + 1;

        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_DOCUMENT_NOT_SUPPORT", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            dispatch_async(dispatch_get_main_queue(), ^{
                // This keeps a reference to the window until the action is invoked.
                window.hidden = YES;
            });
        }];
        [alertVC addAction:cancelAction];

        [window makeKeyAndVisible];
        [window.rootViewController presentViewController:alertVC animated:YES completion:nil];

        [root popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - private methods

- (void)reportErrorToServiceClient:(NSString *)application
                         requestID:(NSString *)requestID
                       withMessage:(NSString *)message
                           andCode:(NSInteger)code
{
    NSError *error;
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:message forKey:NSLocalizedDescriptionKey];
    NSError *replyParams = [NSError errorWithDomain:GDServicesErrorDomain
                                               code:code
                                           userInfo:userInfo];

    BOOL replyResult = [GDService replyTo:application
                               withParams:replyParams
                       bringClientToFront:GDENoForegroundPreference
                          withAttachments:nil
                                requestID:requestID
                                    error:&error];

    if (!replyResult || error) {
        NSLog(@"GDServiceDidReceiveFrom failed to reply \"%@\" %ld \"%@\"", error.domain, (long)error.code, error.localizedDescription);
    }

    // If the loading view is closed we shouldn't pop up an error dialog out of context.
    __weak LoadingViewController *loadVC = [self getLoadingView];
    if (loadVC == nil) return;

    UINavigationController *root = (UINavigationController *)self.window.rootViewController;

    // Display the error message
    UIWindow* window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    window.rootViewController = [UIViewController new];
    window.windowLevel = UIWindowLevelAlert + 1;

    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_LOADDOC", nil) preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

        dispatch_async(dispatch_get_main_queue(), ^{
            // This keeps a reference to the window until the action is invoked.
            window.hidden = YES;
        });
    }];
    [alertVC addAction:cancelAction];

    [window makeKeyAndVisible];
    [window.rootViewController presentViewController:alertVC animated:YES completion:nil];

    [root popToRootViewControllerAnimated:YES];
}

- (void)extractReceivedDataFromAttachments:(NSArray *)attachments
{
    NSString *localFilePath = attachments[0];
    if (![[GDFileManager defaultManager] fileExistsAtPath:localFilePath isDirectory:false]) {
        [self reportErrorToServiceClient:self.gdApplication
                               requestID:self.gdRequestID
                             withMessage:[NSString stringWithFormat:@"Attachment was not found \"%@\"", localFilePath]
                                 andCode:GDServicesErrorInvalidParams];
        return;
    }

    NSData *data = [[GDFileManager defaultManager] contentsAtPath:localFilePath];
    if (data == nil) {
        NSString *description = [NSString stringWithFormat:@"Error reading attachment \"%@\"", localFilePath];
        [self reportErrorToServiceClient:self.gdApplication
                               requestID:self.gdRequestID
                             withMessage:description
                                 andCode:GDServicesErrorGeneral];
        return;
    }

    // If the loading view is closed we shouldn't do anything (User cancelled the action).
    __weak LoadingViewController *loadVC = [self getLoadingView];
    if (loadVC == nil) return;

    NSString *filePath = [NSTemporaryDirectory() stringByAppendingString:localFilePath.pathComponents.lastObject];
    if ([data writeToFile:filePath atomically:NSDataWritingAtomic]) {

        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
        PrintDocument *document = [PrintDocument getDocumentFromFile:fileURL fromSource:NSLocalizedPONString(@"LABEL_SOURCE_OPENIN", nil)];

        if (document && loadVC != nil) {
            [[PrintJobManager sharedPrintJobManager].mainView showPrintPreview:document];
            return;
        }
    }

    NSString *description = [NSString stringWithFormat:@"Error processing attachment \"%@\"", localFilePath];
    [self reportErrorToServiceClient:self.gdApplication
                           requestID:self.gdRequestID
                         withMessage:description
                             andCode:GDServicesErrorGeneral];
}

#pragma mark - Content

- (BOOL)showDocuments
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:info];
        NSValue *documentsValue = dict[@"showDocuments"];
        return [documentsValue isEqualToValue:@YES] ? YES : NO;
    }

    return NO;
}

- (BOOL)showEmail
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:info];
        NSValue *emailValue = dict[@"showEmail"];
        return [emailValue isEqualToValue:@YES] ? YES : NO;
    }

    return NO;
}

@end
