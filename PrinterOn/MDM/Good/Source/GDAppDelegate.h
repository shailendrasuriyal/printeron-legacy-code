//
//  GDAppDelegate.h
//  Good
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AppDelegate.h"
#import <GD/GDiOS.h>
#import <GD/GDServices.h>

@interface GDAppDelegate : AppDelegate <UIApplicationDelegate, GDiOSDelegate, GDServiceDelegate>
{
    BOOL started;
    BOOL showLoading;
}

@property (strong, nonatomic) GDiOS *good;

- (void)onAuthorized:(GDAppEvent*)anEvent;
- (void)onNotAuthorized:(GDAppEvent*)anEvent;
- (void)showLoadingView;

@end
