//
//  LoadingViewController.m
//  PrinterOn
//
//  Created by Harry Han on 2015-04-22.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "LoadingViewController.h"

@interface LoadingViewController ()

@end

@implementation LoadingViewController

#pragma mark - Overrides

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"LoadDocumentScreen.BackgroundColor"];
    
    self.activityView.backgroundColor = [ThemeLoader colorForKey:@"LoadDocumentScreen.Activity.BackgroundColor"];
    self.activityView.layer.borderWidth = 1.5f;
    self.activityView.layer.borderColor = [ThemeLoader colorForKey:@"LoadDocumentScreen.Activity.BorderColor"].CGColor;
    self.activityView.layer.cornerRadius = 10;
    self.activityView.layer.masksToBounds = NO;
    self.activityView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.activityView.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    self.activityView.layer.shadowOpacity = 0.75f;
    self.activityView.layer.shadowRadius = 2.0f;

    self.activitySpinner.color = [ThemeLoader colorForKey:@"LoadDocumentScreen.Activity.SpinnerColor"];
    self.activityLabel.textColor = [ThemeLoader colorForKey:@"LoadDocumentScreen.Activity.TextColor"];

    self.navigationController.navigationBar.tintColor = [ThemeLoader colorForKey:@"NavigationBar.TextColor"];
    self.navigationController.navigationBar.barTintColor = [ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"];

    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_LOADDOC", nil);
    self.activityLabel.text = NSLocalizedPONString(@"LABEL_LOADDOC", nil);
}

@end
