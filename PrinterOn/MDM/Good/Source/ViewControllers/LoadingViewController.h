//
//  LoadingViewController.h
//  PrinterOn
//
//  Created by Harry Han on 2015-04-22.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface LoadingViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *activityView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activitySpinner;
@property (weak, nonatomic) IBOutlet UILabel *activityLabel;

@end
