//
//  MICustomProfile.h
//  PrinterOn
//
//  Created by Harry Han on 2015-06-29.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "MDMCustomSetting.h"

@interface MICustomProfile : MDMCustomSetting

+ (void)setCustomSettingWith:(NSDictionary*)settings;

@end
