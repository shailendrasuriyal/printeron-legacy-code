//
//  MIAppDelegate.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "MIAppDelegate.h"

#import "MICustomProfile.h"

@implementation MIAppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Disable the splash screen because it interferes with the Mobile Iron launch process
    self.showSplash = NO;
    return YES;
}

- (void)customSetup:(NSDictionary *)launchOptions
{
    [self setupAppConnect:launchOptions];
}

#pragma mark - Mobile Iron AppConnect

- (void)setupAppConnect:(NSDictionary *)launchOptions
{
    [AppConnect initWithDelegate:self];
    [self setAppConnect:[AppConnect sharedInstance]];
    [self.appConnect startWithLaunchOptions:launchOptions];
}

-(void)appConnectIsReady:(AppConnect *)appConnect
{
    if (appConnect.isReady) {
        [MICustomProfile setCustomSettingWith:appConnect.config];
    }
}

- (void)appConnect:(AppConnect *)appConnect managedPolicyChangedTo:(ACManagedPolicy)newManagedPolicy
{
}

-(void) appConnect:(AppConnect *)appConnect authStateChangedTo:(ACAuthState)newAuthState withMessage:(NSString *)newMessage
{
    [appConnect authStateApplied:ACPOLICY_APPLIED message:nil];
}

-(void) appConnect:(AppConnect *)appConnect pasteboardPolicyChangedTo:(ACPasteboardPolicy)newPasteboardPolicy
{
    [appConnect pasteboardPolicyApplied:ACPOLICY_APPLIED message:nil];
}

-(void) appConnect:(AppConnect *)appConnect openInPolicyChangedTo:(ACOpenInPolicy)newOpenInPolicy whitelist:(NSSet *)newWhitelist
{
    [appConnect openInPolicyApplied:ACPOLICY_APPLIED message:nil];    
}

-(void) appConnect:(AppConnect *)appConnect printPolicyChangedTo:(ACPrintPolicy)newPrintPolicy
{
    [appConnect printPolicyApplied:ACPOLICY_APPLIED message:nil];
}

-(void) appConnect:(AppConnect *)appConnect secureFileIOPolicyChangedTo:(ACSecureFileIOPolicy)newSecureFileIOPolicy
{
    [appConnect secureFileIOPolicyApplied:ACPOLICY_APPLIED message:nil];
}

-(void) appConnect:(AppConnect *)appConnect secureServicesAvailabilityChangedTo:(ACSecureServicesAvailability)secureServicesAvailability
{
}

-(void) appConnect:(AppConnect *)appConnect logLevelChangedTo:(ACLogLevel)logLevel
{
}

-(void) appConnect:(AppConnect *)appConnect configChangedTo:(NSDictionary *)newConfig
{
    [MICustomProfile setCustomSettingWith:newConfig];
    [appConnect configApplied:ACPOLICY_APPLIED message:nil];
}

#pragma mark - Content

- (BOOL)showDocuments
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:info];
        NSValue *documentsValue = dict[@"showDocuments"];
        return [documentsValue isEqualToValue:@YES] ? YES : NO;
    }
    
    return NO;
}

- (BOOL)showEmail
{
    NSData *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"PONLastPayloadInfo"];
    if (info) {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:info];
        NSValue *emailValue = dict[@"showEmail"];
        return [emailValue isEqualToValue:@YES] ? YES : NO;
    }
    
    return NO;
}

@end
