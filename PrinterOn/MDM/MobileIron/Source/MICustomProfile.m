//
//  MICustomProfile.m
//  PrinterOn
//
//  Created by Harry Han on 2015-06-29.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "MICustomProfile.h"

@implementation MICustomProfile

+ (void)setCustomSettingWith:(NSDictionary*)settings
{
    if (settings.count == 0) {
        return;
    }

    NSMutableDictionary *config = settings.mutableCopy;
    
    BOOL isEnable = NO;
    if ([((NSString *)config[@"enable"]).lowercaseString isEqualToString:@"yes"]) {
        isEnable = YES;
    }

    BOOL isDefault = NO;
    if ([((NSString *)config[@"serviceDefault"]).lowercaseString isEqualToString:@"yes"]) {
        isDefault = YES;
    }

    BOOL isLock = NO;
    if ([((NSString *)config[@"serviceLock"]).lowercaseString isEqualToString:@"yes"]) {
        isLock = YES;
    }

    BOOL isRestricted = NO;
    if ([((NSString *)config[@"serviceRestrict"]).lowercaseString isEqualToString:@"yes"]) {
        isRestricted = YES;
    }

    BOOL showDocuments = NO;
    if ([((NSString *)config[@"showDocuments"]).lowercaseString isEqualToString:@"yes"]) {
        showDocuments = YES;
    }

    BOOL showEmail = NO;
    if ([((NSString *)config[@"showEmail"]).lowercaseString isEqualToString:@"yes"]) {
        showEmail = YES;
    }

    config[@"enable"] = @(isEnable);
    config[@"serviceDefault"] = @(isDefault);
    config[@"serviceLock"] = @(isLock);
    config[@"serviceRestrict"] = @(isRestricted);
    config[@"showDocuments"] = @(showDocuments);
    config[@"showEmail"] = @(showEmail);

    [super set:config];
}

@end
