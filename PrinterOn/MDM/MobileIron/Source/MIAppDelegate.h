//
//  MIAppDelegate.h
//  PrinterOn
//
//  Created by Mark Burns on 2013-10-08.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AppDelegate.h"

#import <AppConnect/AppConnect.h>

@interface MIAppDelegate : AppDelegate <UIApplicationDelegate, AppConnectDelegate>

@property (strong, nonatomic) AppConnect *appConnect;

@end
