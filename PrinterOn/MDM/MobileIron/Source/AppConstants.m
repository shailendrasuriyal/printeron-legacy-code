//
//  AppConstants.m
//  MobileIron
//
//  Created by Mark Burns on 2013-08-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AppConstants.h"

#import "NSData+Transform.h"
#import "UAObfuscatedString.h"

NSString* const kAppVersionText = @"v%@-%@";
NSString* const kAppVersionName = @"MobileIron";
NSString* const kAboutCopyrightText = @"LABEL_COPYRIGHT_MOBILEIRON";
CGFloat const kAboutCopyrightTextSize = 9.0;
NSString* const kImagesBundleIdentifier = @"com.printeron.printeron.PONResources";

@implementation AppConstants

+ (NSString *)googleOAuth2ClientID
{
    // Encode: Key string -> NSData -> XOR -> Hex string
    //NSData *temp = [@"102917463087-f4vgp3fl149jf8fjq8g78jie7jo9qi7p.apps.googleusercontent.com" dataUsingEncoding:NSUTF8StringEncoding];
    //[temp transform];
    //NSLog(@"%@", temp.hexString);
    
    NSData *bytes = [NSData toBytes:NSMutableString.new._5.f._5._7._4._6._5._1._0.b._5._1._4._6._5._3._5._6._6._7._5._0._5._2._4._3._2._2._5.b._1._8._0._2._4.a._5.a._0._8._0._5._4._5._6._3._5._0._1.e._0.e._7.b._0.e._0.b._0._3._5._9._0._4._4._3._5.d._1._8._1.a._2.b._5._8._2._9._0._0._4._9._0._8._5._3._5.b._1._5._4._0._0._6._0._4._1._8._4._9._4._8._1._5._0.a._0.a._3._0._0._4._0._0._1.b._3._7._0.a._1.c._0._6._5._5._0._7._1.a._0.c._1.a._2._3._4._7._1._7._0._7._2.e];
    [bytes transform];
    return [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
}

+ (NSString *)outlookOAuth2ClientID
{
    // Encode: Key string -> NSData -> XOR -> Hex string
    //NSData *temp = [@"0000000044177997" dataUsingEncoding:NSUTF8StringEncoding];
    //[temp transform];
    //NSLog(@"%@", temp.hexString);
    
    NSData *bytes = [NSData toBytes:NSMutableString.new._4._2._4._3._7.e._5.f._7._3._5.f._4._0._4._9._0.e._5._8._5._4._5._9._5._0._4.d._5._1._0.d];
    [bytes transform];
    return [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
}

+ (NSString *)outlookOAuth2ClientSecret
{
    // Encode: Key string -> NSData -> XOR -> Hex string
    //NSData *temp = [@"ZuIZcDVr7HBpdavx3IShR-GD2VCLBlwU" dataUsingEncoding:NSUTF8StringEncoding];
    //[temp transform];
    //NSLog(@"%@", temp.hexString);
    
    NSData *bytes = [NSData toBytes:NSMutableString.new._3.c._0._7._2.c._3.f._3._4._2.c._3._3._1.c._7._3._2._7._2.c._1._5._5.e._0._8._1._8._1._1._4._7._1.e._3.a._1.c._3.a._6.e._2.f._2._5._4._0._3._7._2._0._3._8._2._7._1.e._0._4._1.b];
    [bytes transform];
    return [[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];
}

@end
