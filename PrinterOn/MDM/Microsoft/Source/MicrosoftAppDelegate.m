//
//  MicrosoftAppDelegate.m
//  PrinterOn
//
//  Created by Pengbo Zheng on 2017-05-17.
//  Copyright © 2017 PrinterOn Inc. All rights reserved.
//

#import "MicrosoftAppDelegate.h"
#import <IntuneMAM/IntuneMAMEnrollmentManager.h>

@implementation MicrosoftAppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Disable the splash screen because it interferes with the Microsoft Intune launch process
    self.showSplash = NO;

    // Retrieve app protection policy
    [[IntuneMAMEnrollmentManager alloc] loginAndEnrollAccount: nil];

    return YES;
}

// MARK: - Intune Enrollment Status
- (void)enrollmentRequestWithStatus:(IntuneMAMEnrollmentStatus *)status
{
    NSLog(@"enrollment result for identity %@ with status code %ld", status.identity, (unsigned long)status.statusCode);
    NSLog(@"Debug Message: %@", status.errorString);
}


- (void)policyRequestWithStatus:(IntuneMAMEnrollmentStatus *)status
{
    NSLog(@"policy check-in result for identity %@ with status code %ld", status.identity, (unsigned long)status.statusCode);
    NSLog(@"Debug Message: %@", status.errorString);
}

- (void)unenrollRequestWithStatus:(IntuneMAMEnrollmentStatus *)status
{
    NSLog(@"un-enroll result for identity %@ with status code %ld", status.identity, (unsigned long)status.statusCode);
    NSLog(@"Debug Message: %@", status.errorString);
}

@end
