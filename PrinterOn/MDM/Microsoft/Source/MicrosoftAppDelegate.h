//
//  MicrosoftAppDelegate.h
//  PrinterOn
//
//  Created by Pengbo Zheng on 2017-05-17.
//  Copyright © 2017 PrinterOn Inc. All rights reserved.
//

#import "AppDelegate.h"
#import <IntuneMAM/IntuneMAMEnrollmentDelegate.h>

@interface MicrosoftAppDelegate : AppDelegate <IntuneMAMEnrollmentDelegate>

@end
