//
//  MicrosoftMain.m
//  PrinterOn
//
//  Created by Pengbo Zheng on 2017-05-17.
//  Copyright © 2017 PrinterOn Inc. All rights reserved.
//

#import "MicrosoftAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MicrosoftAppDelegate class]));
    }
}
