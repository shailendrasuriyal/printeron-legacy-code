#!/bin/sh

PATH="/usr/local/bin:$PATH"
export FASTLANE_DISABLE_COLORS=1

PLISTVAR=""
SCHEMEVAR=""
APPNAMEVAR=""
EXTPLISTVAR=""
CITRIXVAR=""

while [ $# -gt 0 ]
do
    case "$1" in
        -plist) PLISTVAR="$2"; shift;;
        -scheme) SCHEMEVAR="$2"; shift;;
        -name) APPNAMEVAR="$2"; shift;;
        -extplist) EXTPLISTVAR="$2"; shift;;
        -citrix) CITRIXVAR="$2"; shift;;
        *)  break;;	# terminate while loop
    esac
    shift
done

LASTSVN=$bamboo_buildNumber
WORKDIR=$bamboo_build_working_directory

PLIST="${WORKDIR}/${PLISTVAR}"
BUILDVERSION=$(/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "${PLIST}")

echo
echo "===[ Replace Bundle Version with last SVN Version ]==="
echo

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion ${LASTSVN}" "${PLIST}"

if [ -n "$EXTPLISTVAR" ]; then
    EXTPLIST="${WORKDIR}/${EXTPLISTVAR}"
    /usr/libexec/PlistBuddy -c "Set :CFBundleVersion ${LASTSVN}" "${EXTPLIST}"
fi

mkdir -p "${WORKDIR}/Output"

echo
echo "===[ Sync Certificates & Profiles ]==="
echo

fastlane certificates

echo
echo "===[ Create AppStore Archive ]==="
echo

set -o pipefail && /Applications/Xcode.app/Contents/Developer/usr/bin/xcodebuild clean archive -workspace PrinterOn.xcworkspace -scheme ${SCHEMEVAR} -configuration Release -archivePath "${WORKDIR}/Output/${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}.xcarchive" | tee "${WORKDIR}/Output/xcodebuild-AppStore.log" | xcpretty
if [ "$?" != "0" ]; then
    exit 1
fi

echo
echo "===[ Zip AppStore Archive ]==="
echo

pushd "${WORKDIR}/Output"
zip -r -X "${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}-AppStore.zip" "${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}.xcarchive"
popd

echo
echo "===[ Sync Enterprise Certificates & Profiles ]==="
echo

fastlane enterprise_certificates

echo
echo "===[ Create Enterprise Archive ]==="
echo

set -o pipefail && /Applications/Xcode.app/Contents/Developer/usr/bin/xcodebuild clean archive -workspace PrinterOn.xcworkspace -scheme ${SCHEMEVAR} -configuration Enterprise -archivePath "${WORKDIR}/Output/${APPNAMEVAR}-Enterprise.xcarchive" | tee "${WORKDIR}/Output/xcodebuild-Enterprise.log" | xcpretty
if [ "$?" != "0" ]; then
    exit 1
fi

echo
echo "===[ Create Enterprise IPA ]==="
echo

/Applications/Xcode.app/Contents/Developer/usr/bin/xcodebuild -exportArchive -exportOptionsPlist "${WORKDIR}/PrinterOn/Support/Enterprise-ExportOptions.plist" -archivePath "${WORKDIR}/Output/${APPNAMEVAR}-Enterprise.xcarchive" -exportPath "${WORKDIR}/Output"
if [ "$?" != "0" ]; then
    exit 1
fi

mv "${WORKDIR}/Output/${SCHEMEVAR}.ipa" "${WORKDIR}/Output/${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}-Enterprise.ipa"

if [ -n "$CITRIXVAR" ]; then

    echo
    echo "===[ Embed IPA in Citrix MDX ]==="
    echo

    /Applications/Citrix/MDXToolkit/CGAppCLPrepTool SetInfo -in "${WORKDIR}/Output/${APPNAMEVAR}-Enterprise-Input.mdx" -out "${WORKDIR}/Output/${APPNAMEVAR}-Enterprise-Embedded.mdx" -embedBundle "${WORKDIR}/Output/${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}-Enterprise.ipa"
    if [ "$?" != "0" ]; then
        exit 1
    fi

    rm "${WORKDIR}/Output/${APPNAMEVAR}-Enterprise-Input.mdx"
fi

echo
echo "===[ Update Enterprise OTA Distribution ]==="
echo

unzip -j "${WORKDIR}/Output/${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}-Enterprise.ipa" Payload/${APPNAMEVAR}.app/Info.plist
BUNDLEID=$(/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" "Info.plist")

OTAPATH="${WORKDIR}/PrinterOn/Support/PrinterOn-OTA.plist"
BUNDLENAME=$(/usr/libexec/PlistBuddy -c "Print :CFBundleName" "${PLIST}")
WEBDEPOTPATH="https://webdepot.qa.printeron.net/mobile"
IPAPATH="${WEBDEPOTPATH}/releases/iOS/${APPNAMEVAR}/${BUILDVERSION}/${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}-Enterprise.ipa"
ICONPATH="${WEBDEPOTPATH}/images/releases/${APPNAMEVAR}.png"

/usr/libexec/PlistBuddy -c "Set :items:0:assets:0:url ${IPAPATH}" "${OTAPATH}"
/usr/libexec/PlistBuddy -c "Set :items:0:assets:1:url ${ICONPATH}" "${OTAPATH}"
/usr/libexec/PlistBuddy -c "Set :items:0:assets:2:url ${ICONPATH}" "${OTAPATH}"
/usr/libexec/PlistBuddy -c "Set :items:0:metadata:bundle-identifier ${BUNDLEID}" "${OTAPATH}"
/usr/libexec/PlistBuddy -c "Set :items:0:metadata:bundle-version ${BUILDVERSION}" "${OTAPATH}"
/usr/libexec/PlistBuddy -c "Set :items:0:metadata:title ${BUNDLENAME}" "${OTAPATH}"

cp "${OTAPATH}" "${WORKDIR}/Output/${APPNAMEVAR}-${LASTSVN}.plist"

echo
echo "===[ Deploy to Web Depot ]==="
echo

curl -T "{${WORKDIR}/Output/${APPNAMEVAR}-${LASTSVN}.plist,${WORKDIR}/Output/${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}-Enterprise.ipa}" -u "${bamboo_webDepotUser}":"${bamboo_webDepotPassword}" "ftp://webdepot.printeron.local/mobile/releases/iOS/${APPNAMEVAR}/${BUILDVERSION}/" --ftp-create-dirs
