#!/bin/sh

PATH="/usr/local/bin:$PATH"
export FASTLANE_DISABLE_COLORS=1

PLISTVAR=""
SCHEMEVAR=""
APPNAMEVAR=""
EXTPLISTVAR=""

while [ $# -gt 0 ]
do
    case "$1" in
        -plist) PLISTVAR="$2"; shift;;
        -scheme) SCHEMEVAR="$2"; shift;;
        -name) APPNAMEVAR="$2"; shift;;
        -extplist) EXTPLISTVAR="$2"; shift;;
        *)  break;;	# terminate while loop
    esac
    shift
done

LASTSVN=$bamboo_buildNumber
WORKDIR=$bamboo_build_working_directory

PLIST="${WORKDIR}/${PLISTVAR}"
BUILDVERSION=$(/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "${PLIST}")

echo
echo "===[ Replace Bundle Version with last SVN Version ]==="
echo

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion ${LASTSVN}" "${PLIST}"

if [ -n "$EXTPLISTVAR" ]; then
    EXTPLIST="${WORKDIR}/${EXTPLISTVAR}"
    /usr/libexec/PlistBuddy -c "Set :CFBundleVersion ${LASTSVN}" "${EXTPLIST}"
fi

mkdir -p "${WORKDIR}/Output"

echo
echo "===[ Sync Certificates & Profiles ]==="
echo

fastlane certificates app_identifier:com.printeron.printeron.b2b,com.printeron.printeron.b2b.ponextension

echo
echo "===[ Create B2B Archive ]==="
echo

set -o pipefail && /Applications/Xcode.app/Contents/Developer/usr/bin/xcodebuild clean archive -workspace PrinterOn.xcworkspace -scheme ${SCHEMEVAR} -configuration B2B -archivePath "${WORKDIR}/Output/${APPNAMEVAR}-B2B-${BUILDVERSION}-${LASTSVN}.xcarchive" | tee "${WORKDIR}/Output/xcodebuild-B2B.log" | xcpretty
if [ "$?" != "0" ]; then
    exit 1
fi

echo
echo "===[ Zip B2B Archive ]==="
echo

pushd "${WORKDIR}/Output"
zip -r -X "${APPNAMEVAR}-${BUILDVERSION}-${LASTSVN}-B2B.zip" "${APPNAMEVAR}-B2B-${BUILDVERSION}-${LASTSVN}.xcarchive"
popd
