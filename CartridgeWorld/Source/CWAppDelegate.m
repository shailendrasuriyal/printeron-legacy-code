//
//  CWAppDelegate.m
//  CartridgeWorld
//
//  Created by Mark Burns on 2016-04-26.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "CWAppDelegate.h"

#import <GoogleAnalytics/GAI.h>

@implementation CWAppDelegate

#pragma mark - Setup Analytics

- (void)setupAnalytics
{
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;

    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;

    // Optional: set dryRun mode which prevents any data from being sent to Google Analytics.
    [GAI sharedInstance].dryRun = YES;

    // Optional: set Logger level.
    [GAI sharedInstance].logger.logLevel = kGAILogLevelNone;

    // Initialize tracker.
    [[GAI sharedInstance] trackerWithTrackingId:@""];
}

#pragma mark - Content

- (BOOL)showEmail
{
    return NO;
}

@end
