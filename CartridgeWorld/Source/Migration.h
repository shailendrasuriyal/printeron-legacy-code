//
//  Migration.h
//  CartridgeWorld
//
//  Created by Mark Burns on 2016-04-26.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

@interface Migration : NSObject

+ (void)performMigration;

@end
