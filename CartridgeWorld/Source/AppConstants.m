//
//  AppConstants.m
//  CartridgeWorld
//
//  Created by Mark Burns on 2016-04-26.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AppConstants.h"

NSString* const kAppVersionText = @"v%@";
NSString* const kAppVersionName = @"";
NSString* const kAboutCopyrightText = @"LABEL_COPYRIGHT_CARTRIDGEWORLD";
CGFloat const kAboutCopyrightTextSize = 11.0;
NSString* const kImagesBundleIdentifier = @"com.cartridgeworld.cwapp.CWResources";

@implementation AppConstants

+ (NSString *)googleOAuth2ClientID
{
    return nil;
}

+ (NSString *)outlookOAuth2ClientID
{
    return nil;
}

+ (NSString *)outlookOAuth2ClientSecret
{
    return nil;
}

@end
