//
//  CWLocalization.h
//  CartridgeWorld
//
//  Created by Mark Burns on 2016-07-22.
//  Copyright © 2016 PrinterOn Inc. All rights reserved.
//

#ifndef NSLocalizedPONString
#define NSLocalizedPONString(key, comment) \
    [NSLocalizedStringFromTable((key), @"CWLocalizable", (comment)) isEqualToString:(key)] ? \
        NSLocalizedStringFromTable((key), @"Localizable", (comment)) : \
        NSLocalizedStringFromTable((key), @"CWLocalizable", (comment))
#endif
