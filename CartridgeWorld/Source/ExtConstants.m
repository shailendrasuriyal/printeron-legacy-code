//
//  ExtConstants.m
//  CartridgeWorld
//
//  Created by Mark Burns on 2016-04-26.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

NSString* const kImagesBundleIdentifier = @"com.cartridgeworld.cwapp.CWResources";
NSString* const kAppURLScheme = @"cwapp";
