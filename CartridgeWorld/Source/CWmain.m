//
//  CWmain.m
//  CartridgeWorld
//
//  Created by Mark Burns on 2016-04-26.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "CWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CWAppDelegate class]));
    }
}
