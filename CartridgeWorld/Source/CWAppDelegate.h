//
//  CWAppDelegate.h
//  CartridgeWorld
//
//  Created by Mark Burns on 2016-04-26.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

#import "AppDelegate.h"

@interface CWAppDelegate : AppDelegate <UIApplicationDelegate>

@end
