//
//  Migration.m
//  CartridgeWorld
//
//  Created by Mark Burns on 2016-04-26.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "Migration.h"

#import "NetworkBrowser.h"
#import "Service.h"

@implementation Migration

+ (void)performMigration
{
    NSString *lastRunVersion = [Migration getLastRunVersion];
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];

    if ([Migration isVersion:currentVersion higherThan:lastRunVersion]) {
        // Use a switch without breaks for each case so that if you are migrating from an older version you
        // will do all of the migration code from the previous version all the way to the current.
        NSArray *versions = @[];
        int versionNum = (int)[versions indexOfObject:lastRunVersion];
        switch (versionNum) {
            default:
                break;
        }

        // Store the current version so we don't do migration next run
        [Migration updateVersion];
    }
}

+ (NSString *)getLastRunVersion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ver = [defaults stringForKey:@"CURRENT_VERSION"];
    return !ver ? @"1.0" : ver;
}

+ (void)updateVersion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"] forKey:@"CURRENT_VERSION"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isVersion:(NSString *)oneString higherThan:(NSString *)otherString {
    // LOWER
    if ([oneString compare:otherString options:NSNumericSearch] == NSOrderedAscending) {
        return NO;
    }

    // EQUAL
    if ([oneString compare:otherString options:NSNumericSearch] == NSOrderedSame) {
        return NO;
    }

    //HIGHER
    return YES;
}

@end
