//
//  ExtOptionsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-20.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtOptionsTableViewController.h"

@class HMSegmentedControl, PrintDocument;

@protocol PrintOptionsDelegate

- (void)didFinishWithOptions:(NSMutableDictionary *)options;

@end

@interface ExtOptionsViewController : BaseViewController <PrintOptionsTableDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *copiesHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collateHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collateBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stepperRangeHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stepperRangeBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paperSizeHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paperSizeBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orientationBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *duplexHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colorHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colorBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stepperSelectHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *worksheetHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *worksheetBottomConstraint;

@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *viewCopies;
@property (weak, nonatomic) IBOutlet UILabel *labelCopies;
@property (weak, nonatomic) IBOutlet UIStepper *stepperCopies;

@property (weak, nonatomic) IBOutlet UIView *viewRangeStepper;
@property (weak, nonatomic) IBOutlet UISwitch *switchRange;
@property (weak, nonatomic) IBOutlet UILabel *labelRangeStepper;
@property (weak, nonatomic) IBOutlet UIView *viewSelectRange;
@property (weak, nonatomic) IBOutlet UITextField *textRangeLower;
@property (weak, nonatomic) IBOutlet UITextField *textRangeUpper;
@property (weak, nonatomic) IBOutlet UIStepper *stepperRangeLower;
@property (weak, nonatomic) IBOutlet UIStepper *stepperRangeUpper;

@property (weak, nonatomic) IBOutlet UIView *viewPaperSize;
@property (weak, nonatomic) IBOutlet UILabel *labelPaperSize;
@property (weak, nonatomic) IBOutlet UIImageView *imagePaperSize;
@property (weak, nonatomic) IBOutlet UILabel *labelChosenPaper;

@property (weak, nonatomic) IBOutlet UIView *viewDuplex;
@property (weak, nonatomic) IBOutlet UILabel *labelDuplex;
@property (weak, nonatomic) IBOutlet UIImageView *imageDuplex;
@property (weak, nonatomic) IBOutlet UILabel *labelChosenDuplex;

@property (weak, nonatomic) IBOutlet UIView *viewCollate;
@property (weak, nonatomic) IBOutlet UILabel *labelCollate;
@property (weak, nonatomic) IBOutlet UISwitch *switchCollate;

@property (weak, nonatomic) IBOutlet UIView *viewColor;
@property (weak, nonatomic) IBOutlet HMSegmentedControl *segmentColor;

@property (weak, nonatomic) IBOutlet UIView *viewOrientation;
@property (weak, nonatomic) IBOutlet UILabel *labelOrientation;
@property (weak, nonatomic) IBOutlet UIImageView *imageOrientation;
@property (weak, nonatomic) IBOutlet UILabel *labelChosenOrientation;

@property (weak, nonatomic) IBOutlet UIView *viewWorksheet;
@property (weak, nonatomic) IBOutlet UILabel *labelWorksheet;
@property (weak, nonatomic) IBOutlet UIImageView *imageWorksheet;
@property (weak, nonatomic) IBOutlet UILabel *labelChosenWorksheet;

@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (nonatomic, strong) PrintDocument *document;
@property (nonatomic, strong) NSMutableDictionary *printOptions;
@property (nonatomic, weak) id <PrintOptionsDelegate> delegate;

- (NSMutableDictionary *)generatePrintOptions;

@end
