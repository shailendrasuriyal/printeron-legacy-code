//
//  ExtOptionsTableViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-20.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@class PrintDocument;

typedef NS_ENUM(NSUInteger, OptionsTableType) {
    OptionsTableTypePaperSize = 0,
    OptionsTableTypeDuplex = 1,
    OptionsTableTypeOrientation = 2,
    OptionsTableTypeWorksheet = 3,
};

typedef NS_ENUM(NSUInteger, DuplexOption) {
    DuplexOptionOff = 0,
    DuplexOptionLong = 1,
    DuplexOptionShort = 2,
};

typedef NS_ENUM(NSUInteger, OrientationOption) {
    OrientationOptionPortrait = 0,
    OrientationOptionLandscape = 1,
    OrientationOptionDocument = 2,
};

typedef NS_ENUM(NSUInteger, WorksheetOption) {
    WorksheetOptionActive = 0,
    WorksheetOptionAll = 1,
};

@protocol PrintOptionsTableDelegate

- (void)didPickOptionOfType:(OptionsTableType)type WithValue:(NSString *)string;

@end

@interface ExtOptionsTableViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, weak) id <PrintOptionsTableDelegate> delegate;

@property (nonatomic, assign) OptionsTableType type;
@property (nonatomic, strong) NSString *typeValue;
@property (nonatomic, strong) PrintDocument *document;

@end
