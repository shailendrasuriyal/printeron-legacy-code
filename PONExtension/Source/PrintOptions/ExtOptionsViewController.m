//
//  ExtOptionsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-20.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtOptionsViewController.h"

#import "HMSegmentedControl.h"
#import "PaperSize.h"
#import "PrintDocument.h"
#import "Printer.h"
#import "SpreadSheetDocument.h"

@interface ExtOptionsViewController ()

@property (nonatomic, strong) NSString *mediaNum;
@property (nonatomic, strong) NSString *duplexString;
@property (nonatomic, strong) NSString *orientationString;
@property (nonatomic, strong) NSString *worksheetString;

@property (nonatomic, assign) CGFloat collateHeight;
@property (nonatomic, assign) CGFloat stepperRangeOpenHeight;
@property (nonatomic, assign) CGFloat stepperRangeClosedHeight;
@property (nonatomic, assign) CGFloat stepperSelectHeight;
@property (nonatomic, assign) CGFloat colorHeight;
@property (nonatomic, assign) CGFloat paperSizeHeight;
@property (nonatomic, assign) CGFloat duplexHeight;
@property (nonatomic, assign) CGFloat worksheetHeight;

@end

@implementation ExtOptionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initializeOptions];

    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    if (self.printOptions) {
        [self updateOptions];
        self.printOptions = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PRINT_OPTIONS", nil);
    [self.labelRangeStepper setText:NSLocalizedPONString(@"LABEL_ALLPAGES", nil)];
    [self.labelPaperSize setText:NSLocalizedPONString(@"LABEL_PAPER_SIZE", nil)];
    [self.labelDuplex setText:NSLocalizedPONString(@"LABEL_DOUBLE_SIDED", nil)];
    [self.labelCollate setText:NSLocalizedPONString(@"LABEL_COLLATE", nil)];
    [self.labelOrientation setText:NSLocalizedPONString(@"LABEL_ORIENTATION", nil)];
    [self.labelWorksheet setText:NSLocalizedPONString(@"LABEL_WORKSHEET", nil)];
}

- (void)setupTheme
{
    self.viewBackground.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.Options.BackgroundColor"];

    // Set images
    [self.buttonClose setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]] forState:UIControlStateNormal];
    [self.imageDuplex setImage:[[ImageManager sharedImageManager] imageNamed:@"Disclosure"]];
    [self.imagePaperSize setImage:[[ImageManager sharedImageManager] imageNamed:@"Disclosure"]];
    [self.imageOrientation setImage:[[ImageManager sharedImageManager] imageNamed:@"Disclosure"]];
    [self.imageWorksheet setImage:[[ImageManager sharedImageManager] imageNamed:@"Disclosure"]];

    // Setup NavigationBar appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.PrintOptions.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.PrintOptions.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    // Create and initialize color option segmented control
    self.segmentColor.sectionTitles = @[NSLocalizedPONString(@"LABEL_BW", nil), NSLocalizedPONString(@"LABEL_COLOR", nil)];
    self.segmentColor.titleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:17.0f]};
    self.segmentColor.selectedTitleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:17.0f],  NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentColor.selectionIndicatorColor = [UIColor colorWithRed:4.0/255.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    self.segmentColor.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentColor.selectionIndicatorBoxOpacity = 1.0f;
    self.segmentColor.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
    self.segmentColor.selectedSegmentIndex = 1;
    self.segmentColor.shouldAnimateUserSelection = YES;
}

- (IBAction)closePressed {
    if (self.delegate) [self.delegate didFinishWithOptions:[self generatePrintOptions]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)initializeOptions
{
    self.collateHeight = self.collateHeightConstraint.constant;
    self.stepperRangeOpenHeight = self.stepperRangeHeightConstraint.constant;
    self.stepperRangeClosedHeight = self.stepperRangeHeightConstraint.constant - self.stepperSelectHeightConstraint.constant;
    self.stepperSelectHeight = self.stepperSelectHeightConstraint.constant;
    self.colorHeight = self.colorHeightConstraint.constant;
    self.paperSizeHeight = self.paperSizeHeightConstraint.constant;
    self.duplexHeight = self.duplexHeightConstraint.constant;
    self.worksheetHeight = self.worksheetHeightConstraint.constant;

    [self.stepperCopies setValue:1];
    [self updateCopiesLabel];

    unsigned long pageCount = [self.document getPageCount];

    [self.stepperRangeLower setMinimumValue:1];
    [self.stepperRangeLower setValue:1];
    [self.stepperRangeUpper setMinimumValue:1];
    if (pageCount == 0) {
        [self.stepperRangeLower setMaximumValue:10000];
        [self.stepperRangeUpper setMaximumValue:10000];
        [self.stepperRangeUpper setValue:1];
    } else {
        [self.stepperRangeLower setMaximumValue:pageCount];
        [self.stepperRangeUpper setMaximumValue:pageCount];
        [self.stepperRangeUpper setValue:pageCount];
    }
    [self updateRangeStepperLabel];

    self.mediaNum = [self printOptions][@"poMediaSizeNum"];
    [self updatePaperSizeLabel];

    NSString *colorString = [self printOptions][@"poColor"];
    if (colorString != nil) {
        self.segmentColor.selectedSegmentIndex = [colorString intValue] == 1 ? 1 : 0;
    }

    self.duplexString = [self printOptions][@"poDuplex"];
    [self updateDuplexLabel];

    self.orientationString = [self printOptions][@"poOrientation"];
    [self updateOrientationLabel];

    self.worksheetString = [self printOptions][@"poXLSSheetRange"];
    [self updateWorksheetLabel];
}

- (void)updateOptions
{
    // Initialize copies values
    unsigned long pageCount = [self.document getPageCount];
    BOOL copiesExists = [self printOptions][@"poCopies"] != nil;
    if (self.printOptions && copiesExists) {
        NSString *copiesString = [self printOptions][@"poCopies"];
        [self.stepperCopies setValue:[copiesString doubleValue]];
        [self updateCopiesLabel];
    }

    // Initialize collate values
    BOOL collateExists = [self printOptions][@"poCollate"] != nil;
    if (self.printOptions && collateExists) {
        NSString *collateString = [self printOptions][@"poCollate"];
        [self.switchCollate setOn:[collateString boolValue]];
    }

    // Add Collate View
    if (self.stepperCopies.value > 1 && pageCount != 1) {
        self.collateHeightConstraint.constant = self.collateHeight;
        [self.viewCollate setHidden:NO];
    } else {
        [self.viewCollate setHidden:YES];
        self.collateHeightConstraint.constant = 0.0;
    }

    // Initialize page range values
    BOOL pageRangeExists = [self printOptions][@"poPageRange"] != nil;
    int lowerPageRange = 0;
    int upperPageRange = 0;
    if (self.printOptions && pageRangeExists) {
        NSScanner *scanner = [NSScanner scannerWithString:[self printOptions][@"poPageRange"]];
        NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        [scanner scanInt:&lowerPageRange];
        [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
        [scanner scanInt:&upperPageRange];
    }

    // Add Stepper Page Range View
    if (pageCount != 1) {
        if (pageRangeExists) {
            [self.switchRange setOn:NO];
            [self.stepperRangeLower setValue:lowerPageRange];
            [self.stepperRangeUpper setValue:upperPageRange];
            [self updateRangeStepperLabel];
        }

        if (self.switchRange.isOn) {
            self.stepperRangeHeightConstraint.constant = self.stepperRangeClosedHeight;
            [self.viewSelectRange setHidden:YES];
            self.stepperSelectHeightConstraint.constant = 0.0;
        } else {
            self.stepperRangeHeightConstraint.constant = self.stepperRangeOpenHeight;
            self.stepperSelectHeightConstraint.constant = self.stepperSelectHeight;
            [self.viewSelectRange setHidden:NO];
        }

        self.collateBottomConstraint.constant = 2.0;
        [self.viewRangeStepper setHidden:NO];
    } else {
        [self.viewRangeStepper setHidden:YES];
        self.stepperRangeHeightConstraint.constant = 0.0;
        self.collateBottomConstraint.constant = 0.0;
    }

    // Add WorkSheet View
    if ([self.document isMemberOfClass:[SpreadSheetDocument class]]) {
        self.worksheetHeightConstraint.constant = self.worksheetHeight;
        self.stepperRangeBottomConstraint.constant = 2.0;
        [self.viewWorksheet setHidden:NO];
    } else {
        [self.viewWorksheet setHidden:YES];
        self.worksheetHeightConstraint.constant = 0.0;
        self.stepperRangeBottomConstraint.constant = 0.0;
    }

    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];

    // Add Color View
    if (printer && [printer doesSupportColor] && ![printer.colorPermit.lowercaseString isEqualToString:@"color_only"]) {
        self.colorHeightConstraint.constant = self.colorHeight;
        self.worksheetBottomConstraint.constant = 2.0;
        [self.viewColor setHidden:NO];
    } else {
        [self.viewColor setHidden:YES];
        self.colorHeightConstraint.constant = 0.0;
        self.worksheetBottomConstraint.constant = 0.0;
    }

    // Add Paper Size View
    if (printer) {
        self.paperSizeHeightConstraint.constant = self.paperSizeHeight;
        self.colorBottomConstraint.constant = 2.0;
        [self.viewPaperSize setHidden:NO];
    } else {
        [self.viewPaperSize setHidden:YES];
        self.paperSizeHeightConstraint.constant = 0.0;
        self.colorBottomConstraint.constant = 0.0;
    }

    // Add Duplex View
    if (printer && [printer doesSupportDuplex] && pageCount != 1) {
        self.duplexHeightConstraint.constant = self.duplexHeight;
        self.orientationBottomConstraint.constant = 2.0;
        [self.viewDuplex setHidden:NO];
    } else {
        self.duplexHeightConstraint.constant = 0.0;
        self.orientationBottomConstraint.constant = 0.0;
        [self.viewDuplex setHidden:YES];
    }
}

- (NSMutableDictionary *)generatePrintOptions
{
    NSMutableDictionary *options = [NSMutableDictionary dictionary];

    if (self.stepperCopies.value > 1) {
        [options setValue:[NSString stringWithFormat:@"%.f", self.stepperCopies.value] forKey:@"poCopies"];
        if ([self.document getPageCount] != 1) {
            [options setValue: self.switchCollate.isOn ? @"1" : @"0" forKey:@"poCollate"];
        }
    }

    if (!self.switchRange.isOn) {
        int lower = self.stepperRangeLower.value;
        int upper = self.stepperRangeUpper.value;

        if (lower > upper) upper = lower;

        if (lower > self.stepperRangeLower.minimumValue || upper < self.stepperRangeUpper.maximumValue) {
            [options setValue:[NSString stringWithFormat:@"%d-%d", lower, upper] forKey:@"poPageRange"];
        }
    }

    if (!self.viewColor.isHidden) {
        [options setValue:[NSString stringWithFormat:@"%d", (int)self.segmentColor.selectedSegmentIndex] forKey:@"poColor"];
    }

    [options setValue:self.mediaNum forKey:@"poMediaSizeNum"];

    if (self.duplexString) {
        [options setValue:self.duplexString forKey:@"poDuplex"];
    }

    if (self.orientationString) {
        [options setValue:self.orientationString forKey:@"poOrientation"];
    }

    if (self.worksheetString) {
        [options setValue:self.worksheetString forKey:@"poXLSSheetRange"];
    }

    return options;
}

- (void)updateCopiesLabel
{
    double value = self.stepperCopies.value;
    [self.labelCopies setText:[NSString stringWithFormat:@"%.f %@", value, (value == 1) ? NSLocalizedPONString(@"LABEL_COPY", nil) : NSLocalizedPONString(@"LABEL_COPIES", nil)]];
}

- (void)updateRangeStepperLabel
{
    [self.textRangeLower setText:[NSString stringWithFormat:@"%.f", self.stepperRangeLower.value]];
    [self.textRangeUpper setText:[NSString stringWithFormat:@"%.f", self.stepperRangeUpper.value]];
}

- (void)updatePaperSizeLabel
{
    PaperSize *paperSize = [PaperSize getPaperSizeFromMediaSizeNumber:[self.mediaNum intValue]];
    [self.labelChosenPaper setText:paperSize.title];
}

- (void)updateDuplexLabel
{
    if ([self.duplexString isEqualToString:@"Simplex"]) {
        [self.labelChosenDuplex setText:NSLocalizedPONString(@"LABEL_OFF", nil)];
    } else if ([self.duplexString isEqualToString:@"DuplexLong"]) {
        [self.labelChosenDuplex setText:NSLocalizedPONString(@"LABEL_DUPLEXLONG", nil)];
    } else if ([self.duplexString isEqualToString:@"DuplexShort"]) {
        [self.labelChosenDuplex setText:NSLocalizedPONString(@"LABEL_DUPLEXSHORT", nil)];
    }
}

- (void)updateOrientationLabel
{
    if ([self.orientationString isEqualToString:@"Portrait"]) {
        [self.labelChosenOrientation setText:NSLocalizedPONString(@"LABEL_PORTRAIT", nil)];
    } else if ([self.orientationString isEqualToString:@"Landscape"]) {
        [self.labelChosenOrientation setText:NSLocalizedPONString(@"LABEL_LANDSCAPE", nil)];
    } else {
        [self.labelChosenOrientation setText:NSLocalizedPONString(@"LABEL_DOCUMENT", nil)];
    }
}

- (void)updateWorksheetLabel
{
    if ([self.worksheetString isEqualToString:@"Active"]) {
        [self.labelChosenWorksheet setText:NSLocalizedPONString(@"LABEL_ACTIVE", nil)];
    } else if ([self.worksheetString isEqualToString:@"All"]) {
        [self.labelChosenWorksheet setText:NSLocalizedPONString(@"LABEL_ALL", nil)];
    }
}

- (IBAction)copiesStepperValueChanged {
    [self updateCopiesLabel];
    if (self.stepperCopies.value == 1 || self.stepperCopies.value == 2) {
        [self updateOptions];
    }
}

- (IBAction)switchAllPagesToggle {
    [self updateOptions];
}

- (IBAction)lowerStepperValueChanged {
    [self.textRangeLower setText:[NSString stringWithFormat:@"%.f", self.stepperRangeLower.value]];
}

- (IBAction)upperStepperValueChanged {
    [self.textRangeUpper setText:[NSString stringWithFormat:@"%.f", self.stepperRangeUpper.value]];
}

- (IBAction)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)validateText:(UIStepper *)sender
{
    UITextField *textField = [sender isEqual:self.stepperRangeLower] ? self.textRangeLower : self.textRangeUpper;
    int value = [textField.text intValue];

    if (value == 0) {
        textField.text = [NSString stringWithFormat:@"%.f", sender.minimumValue];
        sender.value = sender.minimumValue;
    } else if (value > sender.maximumValue) {
        textField.text = [NSString stringWithFormat:@"%.f", sender.maximumValue];
        sender.value = sender.maximumValue;
    } else {
        sender.value = value;
    }
}

#pragma mark - PrintOptionsTableDelegate

- (void)didPickOptionOfType:(OptionsTableType)type WithValue:(NSString *)string
{
    if (type == OptionsTableTypeDuplex) {
        self.duplexString = string;
        [self updateDuplexLabel];
    } else if (type == OptionsTableTypePaperSize) {
        self.mediaNum = string;
        [self updatePaperSizeLabel];
    } else if (type == OptionsTableTypeOrientation) {
        self.orientationString = string;
        [self updateOrientationLabel];
    } else if (type == OptionsTableTypeWorksheet) {
        self.worksheetString = string;
        [self updateWorksheetLabel];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([self.textRangeLower isFirstResponder] || [self.textRangeUpper isFirstResponder]) {
        return YES;
    }
    return NO;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:self.textRangeLower] || [textField isEqual:self.textRangeUpper]) {
        UIStepper *stepper = [textField isEqual:self.textRangeLower] ? self.stepperRangeLower : self.stepperRangeUpper;
        [self validateText:stepper];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[1-9]\\d*$" options:NSRegularExpressionCaseInsensitive error:nil];
    
    if ([resultString length] > 0) {
        NSUInteger numOfMatches = [regex numberOfMatchesInString:resultString options:0 range:NSMakeRange(0, [resultString length])];
        if (numOfMatches == 0) {
            return NO;
        } else {
            UIStepper *stepper = [textField isEqual:self.textRangeLower] ? self.stepperRangeLower : self.stepperRangeUpper;
            stepper.value = [resultString intValue];
        }
    }
    
    return YES;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ExtOptionsTableViewController *destViewController = segue.destinationViewController;
    destViewController.delegate = self;
    
    if ([segue.identifier isEqualToString:@"paperSizeOption"]) {
        destViewController.type = OptionsTableTypePaperSize;
        destViewController.typeValue = self.mediaNum;
    } else if ([segue.identifier isEqualToString:@"duplexOption"]) {
        destViewController.type = OptionsTableTypeDuplex;
        destViewController.typeValue = self.duplexString;
    } else if ([segue.identifier isEqualToString:@"orientationOption"]) {
        destViewController.type = OptionsTableTypeOrientation;
        destViewController.typeValue = self.orientationString;
        destViewController.document = self.document;
    } else if ([segue.identifier isEqualToString:@"worksheetOption"]) {
        destViewController.type = OptionsTableTypeWorksheet;
        destViewController.typeValue = self.worksheetString;
    }
}

@end
