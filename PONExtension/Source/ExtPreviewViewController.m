//
//  ExtPreviewViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-03-24.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtPreviewViewController.h"

#import "BBCyclingLabel.h"
#import "DocProcess.h"
#import "EdgeInsetLabel.h"
#import "ExtAuthViewController.h"
#import "ExtConstants.h"
#import "ExtJobInputsViewController.h"
#import "ExtPrintJobManager.h"
#import "ImageDocument.h"
#import "Media.h"
#import "PaperSize.h"
#import "PONButton.h"
#import "PreviewPageView.h"
#import "Printer.h"
#import "RKXMLReaderSerialization.h"
#import "Service.h"
#import "ServiceDescription.h"
#import "ShadowButton.h"
#import "WebDocument.h"

#import <MMWormhole/MMWormhole.h>

@interface ExtPreviewViewController ()

@property (nonatomic, strong) PrintDocument *document;
@property (nonatomic, strong) NSMutableDictionary *printOptions;
@property (nonatomic, strong) NSDictionary *jobInputs;
@property (nonatomic, strong) PaperSize *paperSize;
@property (nonatomic, assign) CGRect carouselPaperSize;
@property (nonatomic, strong) NSTimer *labelTimer;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, assign) BOOL appNeedsLaunch;
@property (nonatomic, assign) BOOL firstLoad;

@property (nonatomic, strong) NSString *safariURL;
@property (nonatomic, strong) NSString *safariBaseURI;
@property (nonatomic, strong) NSString *safariContent;

@end

@implementation ExtPreviewViewController

- (void)viewDidLoad {
    // Perform initilization for the extension
    self.firstLoad = YES;
    [self setupCoreData];
    [self setupRestKit];
    [self setupManagers];

    [super viewDidLoad];

    // Register for changes to the printer
    __weak ExtPreviewViewController *weakSelf = self;
    [[[WormholeManager sharedWormholeManager] wormHole] listenForMessageWithIdentifier:@"PONPrinterChangedNotification" listener:^(id messageObject) {
        [weakSelf printerChangedNotification:nil];
    }];

    // Workaround to save the information coming from our Safari JavaScript processing file.  If you don't save it here on viewDidLoad
    // I found that when you tried to read it later on in lifecycle like viewWillAppear the content is nil.
    for (NSExtensionItem *item in self.extensionContext.inputItems) {
        for (NSItemProvider *itemProvider in item.attachments) {
            if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypePropertyList]) {
                [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypePropertyList options:nil completionHandler:^(NSDictionary *item, NSError *error) {
                    self.safariURL = item[NSExtensionJavaScriptPreprocessingResultsKey][@"URL"];
                    if (self.safariURL == nil || [self.safariURL length] == 0) self.safariURL = @"about:blank";
                    self.safariBaseURI = item[NSExtensionJavaScriptPreprocessingResultsKey][@"baseURI"];
                    self.safariContent = item[NSExtensionJavaScriptPreprocessingResultsKey][@"contentHTML"];
                }];
            }
        }
    }
}

- (void)dealloc
{
    // Unregister printer change notifications
    [[[WormholeManager sharedWormholeManager] wormHole] stopListeningForMessageWithIdentifier:@"PONPrinterChangedNotification"];

    // Unregister for migration changes if the extension was closed
    [[[WormholeManager sharedWormholeManager] wormHole] stopListeningForMessageWithIdentifier:@"PONCoreDataAppGroupMigrated"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (self.appNeedsLaunch) {
        // Show the Launch Migration
        [self showLaunchMigrate];
    } else {
        // Update Printer information in the printer bar
        [self updatePrinterBar:NO onAppear:YES withPrinter:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if (self.firstLoad && !self.appNeedsLaunch) {
        // Get the document passed to the extension from the context
        [self documentFromContext];

        self.firstLoad = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if (self.labelTimer) {
        [self.labelTimer invalidate];
        self.labelTimer = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    self.carouselPaperSize = [PaperSize fitPaperSize:self.paperSize toAreaOfSize:[self getCarouselContentSize]];
    [self.viewCarousel reloadData];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"LABEL_PRINT", nil);
    [self.buttonCancel setTitle:NSLocalizedPONString(@"LABEL_CANCEL", nil) forState:UIControlStateNormal];
    [self.buttonPrint setTitle:NSLocalizedPONString(@"LABEL_PRINT", nil) forState:UIControlStateNormal];
    [self.labelNoPreview setText:NSLocalizedPONString(@"LABEL_NOPREVIEW", nil)];
    [self.labelRender setText:NSLocalizedPONString(@"LABEL_RENDERDOC", nil)];
    [self.labelLaunch setText:NSLocalizedPONString(@"LABEL_LAUNCH_DESC", nil)];
    [self.buttonLaunch setTitle:NSLocalizedPONString(@"LABEL_LAUNCH_APP", nil) forState:UIControlStateNormal];
}

- (void)setupTheme
{
    // Load Theme File
    [ThemeLoader configThemeWithPlist:@"Theme"];

    // Setup NavigationBar Appearance
    [self.navigationController.navigationBar setTintColor:[ThemeLoader colorForKey:@"NavigationBar.TextColor"]];
    [self.navigationController.navigationBar setBarTintColor:[ThemeLoader colorForKey:@"NavigationBar.BackgroundColor"]];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.shadowRadius = 1.5f;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    // Setup Navigation Bar Button Images
    [self.buttonClose setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.CloseButton.Image"]] forState:UIControlStateNormal];
    [self.buttonPrintOptions setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PreviewScreen.OptionsButton.Image"]] forState:UIControlStateNormal];
    [self.buttonPrintOptions setEnabled:NO];

    // Setup View Background Colors
    self.viewBackground.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.BackgroundColor"];

    // Setup Bottom View Appearance
    self.viewBottom.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.Bottom.BackgroundColor"];
    self.viewBottom.layer.shadowOpacity = 0.75f;
    self.viewBottom.layer.shadowRadius = 1.5f;
    self.viewBottom.layer.shadowColor = [UIColor blackColor].CGColor;
    self.viewBottom.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);

    // Setup Printer Bar
    [self.imageBarPrinter setImage:[[ImageManager sharedImageManager] imageNamed:@"PrinterIcon"]];
    [self updatePrinterBar:YES onAppear:NO withPrinter:nil];

    // Setup Buttons
    [self.buttonCancel setBackgroundColor:[UIColor colorWithRed:215/255.0 green:76/255.0 blue:69/255.0 alpha:1.0]];
    [self.buttonCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.buttonCancel.layer.cornerRadius = 4.0f;
    [self.buttonPrint setBackgroundColor:[UIColor colorWithRed:74/255.0 green:209/255.0 blue:95/255.0 alpha:1.0]];
    [self.buttonPrint setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.buttonPrint.layer.cornerRadius = 4.0f;

    // Setup the Cycling Label
    self.labelBarCycling.font = [UIFont systemFontOfSize:12];
    self.labelBarCycling.textColor = [UIColor colorWithRed:120/255.0f green:120/255.0f blue:120/255.0f alpha:1.0f];
    self.labelBarCycling.numberOfLines = 1;
    self.labelBarCycling.textAlignment = NSTextAlignmentCenter;
    self.labelBarCycling.transitionEffect = BBCyclingLabelTransitionEffectCrossFade;

    // Setup the Carousel
    self.viewCarousel.type = iCarouselTypeLinear;
    self.viewCarousel.bounceDistance = 0.5f;
    self.viewCarousel.decelerationRate = 0.7f;

    // Setup Pages Label
    [self.labelPages setEdgeInsets:UIEdgeInsetsMake(0, 16, 0, 16)];
    self.labelPages.layer.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.PageLabel.BackgroundColor"].CGColor;
    self.labelPages.layer.borderWidth = 1.5f;
    self.labelPages.layer.borderColor = [ThemeLoader colorForKey:@"PreviewScreen.PageLabel.BorderColor"].CGColor;
    self.labelPages.layer.cornerRadius = self.labelPages.frame.size.height / 2;
    self.labelPages.layer.masksToBounds = NO;
    self.labelPages.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.labelPages.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    self.labelPages.layer.shadowOpacity = 0.75f;
    self.labelPages.layer.shadowRadius = 2.0f;
    self.labelPages.textColor = [ThemeLoader colorForKey:@"PreviewScreen.PageLabel.TextColor"];

    // Setup the No Preview Label
    self.labelNoPreview.textColor = [ThemeLoader colorForKey:@"PreviewScreen.NoPreviewLabel.TextColor"];

    // Setup Render View
    self.viewRender.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.Render.BackgroundColor"];
    self.viewRender.layer.borderWidth = 1.5f;
    self.viewRender.layer.borderColor = [ThemeLoader colorForKey:@"PreviewScreen.Render.BorderColor"].CGColor;
    self.viewRender.layer.cornerRadius = 10;
    self.viewRender.layer.masksToBounds = NO;
    self.viewRender.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.viewRender.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    self.viewRender.layer.shadowOpacity = 0.75f;
    self.viewRender.layer.shadowRadius = 2.0f;
    self.labelRender.textColor = [ThemeLoader colorForKey:@"PreviewScreen.Render.TextColor"];

    // Setup Launch View
    self.buttonLaunch.backgroundColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.BackgroundColor"];
    self.buttonLaunch.originalBackgroundColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.BackgroundColor"];
    self.buttonLaunch.highlightColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.HighlightColor"];
    self.buttonLaunch.layer.cornerRadius = 4.0f;
    [[self.buttonLaunch imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [self.buttonLaunch setImage:[[self roundCorneredImage:[[ImageManager sharedImageManager] imageNamed:@"AppIcon"] radius:13.0f] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
}

- (UIImage*)roundCorneredImage: (UIImage*) orig radius:(CGFloat) r {
    UIGraphicsBeginImageContextWithOptions(orig.size, NO, 0);
    [[UIBezierPath bezierPathWithRoundedRect:(CGRect){CGPointZero, orig.size} cornerRadius:r] addClip];
    [orig drawInRect:(CGRect){CGPointZero, orig.size}];
    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

- (IBAction)closeExtension
{
    // Saves changes in the application's managed object context before the application terminates.
    [[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] saveToPersistentStore:nil];

    [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
}

- (IBAction)launchApp
{
    UIResponder* responder = self;
    while ((responder = [responder nextResponder]) != nil) {
        if([responder respondsToSelector:@selector(openURL:)] == YES) {
            NSString *openURL = [NSString stringWithFormat:@"%@://", kAppURLScheme];
            [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:openURL]];
        }
    }
}

- (void)showLaunchMigrate
{
    [self.buttonPrintOptions setHidden:YES];
    [self.viewBottom setHidden:YES];

    // Start to listen for when migration finishes
    __weak ExtPreviewViewController *weakSelf = self;
    [[[WormholeManager sharedWormholeManager] wormHole] listenForMessageWithIdentifier:@"PONCoreDataAppGroupMigrated" listener:^(id messageObject) {
        [weakSelf migrationComplete];
    }];

    [self.viewLaunch setHidden:NO];
}

- (void)migrationComplete
{
    // Remove monitoring
    [[[WormholeManager sharedWormholeManager] wormHole] stopListeningForMessageWithIdentifier:@"PONCoreDataAppGroupMigrated"];
    self.appNeedsLaunch = NO;
    
    // Now that we are migrated setup Core Data and update the printer
    [self setupCoreData];
    [self printerChangedNotification:nil];
    
    // Put the UI back to normal
    [self.viewLaunch setHidden:YES];
    [self.buttonPrintOptions setHidden:NO];
    [self.viewBottom setHidden:NO];

    // Get the document from context now that we are done migration
    [self documentFromContext];
}

- (void)updatePrinterBar:(BOOL)initialSetup onAppear:(BOOL)appear withPrinter:(Printer *)printer
{
    if (printer == nil) {
        printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    }

    if (printer) {
        if (self.labelTimer) {
            [self.labelTimer invalidate];
            self.labelTimer = nil;
        }

        [self.buttonBarInfo setImage:[[ImageManager sharedImageManager] imageNamed:@"InfoIcon"] forState:UIControlStateNormal];
        [self.buttonBarInfo setEnabled:YES];

        self.labelBarCycling.transitionDuration = 1.25;
        self.labelBarTitle.text = printer.displayName;
        [self.labelBarCycling setText:printer.organizationLocationDesc animated:NO];

        if (!initialSetup) {
            self.labelTimer = [NSTimer timerWithTimeInterval:6.5 target:self selector:@selector(updateCyclingLabel:) userInfo:printer repeats:NO];
            [[NSRunLoop mainRunLoop] addTimer:self.labelTimer forMode:NSRunLoopCommonModes];
        }

        if (!appear) {
            if ([self.document isMemberOfClass:NSClassFromString(@"OtherDocument")]) {
                return;
            }

            // Check to see if we need to update the Service Capabilities for the selected printers service
            NSString *serviceURL = [DocProcess getBaseURLForService:[printer getDocAPIAddress].absoluteString];
            [ServiceDescription capabilitiesForServiceWithURL:[NSURL URLWithString:serviceURL] forceUpdate:NO completionBlock:nil];
        }
    } else {
        [self.buttonBarInfo setImage:nil forState:UIControlStateNormal];
        [self.buttonBarInfo setEnabled:NO];

        self.labelBarCycling.transitionDuration = 0;
        self.labelBarTitle.text = NSLocalizedPONString(@"LABEL_NOPRINTER", nil);
        self.labelBarCycling.text = NSLocalizedPONString(@"LABEL_NOPRINTERTAP", nil);
    }
}

- (void)updateCyclingLabel:(NSTimer *)timer
{
    if (timer == nil) return;

    Printer *printer = timer.userInfo;

    if ([self.labelBarCycling.text isEqualToString:NSLocalizedPONString(@"LABEL_PRINTERCHANGE", nil)]) {
        self.labelBarCycling.text = printer.organizationLocationDesc;
        self.labelTimer = [NSTimer timerWithTimeInterval:6.5 target:self selector:@selector(updateCyclingLabel:) userInfo:printer repeats:NO];
    } else {
        self.labelBarCycling.text = NSLocalizedPONString(@"LABEL_PRINTERCHANGE", nil);
        self.labelTimer = [NSTimer timerWithTimeInterval:2.5 target:self selector:@selector(updateCyclingLabel:) userInfo:printer repeats:NO];
    }
    [[NSRunLoop mainRunLoop] addTimer:self.labelTimer forMode:NSRunLoopCommonModes];
}

- (void)printerChangedNotification:(NSNotification *)notification
{
    // If the printer changed while the print options were open, update the options, then close the options screen.
    if (self.presentedViewController) {
        if ([self.presentedViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *navController = (UINavigationController *)self.presentedViewController;
            if ([[navController viewControllers][0] isMemberOfClass:[ExtOptionsViewController class]]) {
                ExtOptionsViewController *optionsController = (ExtOptionsViewController *)[navController viewControllers][0];
                [self didFinishWithOptions:[optionsController generatePrintOptions]];
                [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }

    [self validatePrintOptions:self.printOptions];

    if ([self generatePrintOptionsUsingOptions:self.printOptions] || [self.document previewIsServerRendered]) {

        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wundeclared-selector"
        if ([self.document respondsToSelector:@selector(updatedServiceURL:)]) {
            Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
            [self.document performSelector:@selector(updatedServiceURL:) withObject:[printer getDocAPIAddress]];
        }
        #pragma clang diagnostic pop

        [self renderDocument];
    }
}

- (void)validatePrintOptions:(NSMutableDictionary *)options
{
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    [self updatePrinterBar:NO onAppear:NO withPrinter:printer];

    // Check to see if we need to change paper size option
    NSString *paperSize = options[@"poMediaSizeNum"];
    if (paperSize != nil) {
        BOOL found = NO;
        for (Media *media in [printer mediaSupported]) {
            if ([[media.mediaSizeNum stringValue] isEqualToString:paperSize]) {
                found = YES;
                break;
            }
        }
        if (!found) {
            [options removeObjectForKey:@"poMediaSizeNum"];
        }
    }
    
    // Check to see if we need to change duplex option
    NSString *duplex = options[@"poDuplex"];
    if (duplex != nil) {
        if ([printer doesSupportDuplex]) {
            NSString *duplexPermit = [printer.duplexPermit lowercaseString];
            if ([duplexPermit isEqualToString:@"duplex_only"] && [duplex isEqualToString:@"Simplex"]) {
                [options removeObjectForKey:@"poDuplex"];
            } else if ([duplexPermit isEqualToString:@"simplex_only"] && [duplex hasPrefix:@"Duplex"]) {
                [options removeObjectForKey:@"poDuplex"];
            }
        } else {
            [options removeObjectForKey:@"poDuplex"];
        }
    }
    
    // Check to see if we need to change color option
    NSString *color = options[@"poColor"];
    if (color != nil) {
        if ([printer doesSupportColor]) {
            NSString *colorPermit = [printer.colorPermit lowercaseString];
            if ([colorPermit isEqualToString:@"mono_only"] && [color isEqualToString:@"1"]) {
                [options removeObjectForKey:@"poColor"];
            } else if ([colorPermit isEqualToString:@"color_only"] && [color isEqualToString:@"0"]) {
                [options removeObjectForKey:@"poColor"];
            }
        } else {
            [options removeObjectForKey:@"poColor"];
        }
    }
}

- (void)documentFromContext
{
    for (NSExtensionItem *item in self.extensionContext.inputItems) {
        for (NSItemProvider *itemProvider in item.attachments) {
            //NSArray *registeredTypeIdentifiers = itemProvider.registeredTypeIdentifiers;
            __weak ExtPreviewViewController *weakSelf = self;

            if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeFileURL]) {
                [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypeFileURL options:nil completionHandler:^(NSURL *url, NSError *error) {
                    if (url) {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            [weakSelf setDocument:[PrintDocument getDocumentFromFile:url fromSource:nil]];
                        }];
                    }
                }];
                break;
            }

            if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeImage]) {
                [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypeImage options:nil completionHandler:^(UIImage *image, NSError *error) {
                    if (image) {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            [weakSelf setDocument:[[ImageDocument alloc] initWithImage:image fromSource:NSLocalizedPONString(@"LABEL_SOURCE_EXTENSION", nil)]];
                        }];
                    }
                }];
                break;
            }

            if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypePropertyList]) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    weakSelf.webView = [UIWebView new];
                    weakSelf.webView.delegate = weakSelf;
                    if ([self.safariContent length] > 0) {
                        [weakSelf.webView loadHTMLString:self.safariContent baseURL:[NSURL URLWithString:self.safariBaseURI]];
                    } else {
                        [weakSelf.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.safariURL]]];
                    }
                }];
                break;
            }

            if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeURL]) {
                [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypeURL options:nil completionHandler:^(NSURL *url, NSError *error) {
                    if (url) {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            weakSelf.webView = [UIWebView new];
                            weakSelf.webView.delegate = weakSelf;
                            [weakSelf.webView loadRequest:[NSURLRequest requestWithURL:url]];
                        }];
                    }
                }];
                break;
            };
        }
    }
}

- (void)setDocument:(PrintDocument *)document
{
    if (_document) {
        _document.delegate = nil;
    }

    _document = document;

    if (_document) {
        _document.delegate = self;

        // Create initial print options and render
        [self generatePrintOptionsUsingOptions:nil];
        [self renderDocument];
    }
}

- (IBAction)printDocument
{
    [self.buttonPrint setEnabled:NO];

    // Check that a printer is selected
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    if (!printer) {
        [self.buttonPrint setEnabled:YES];
        return;
    }

    // Check that the printer has a valid docAPI URL to submit too
    NSString *docApiURL = [[[printer getDocAPIAddress] absoluteString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (docApiURL == nil || [docApiURL length] == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_MISSING_DOCAPIURL", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        [self.buttonPrint setEnabled:YES];
        return;
    }

    // Check if printing is restricted to a service
    Service *restricted = [Service getRestrictedService];
    if (restricted) {
        NSURL *URL = [printer getDocAPIAddress];
        NSString *serviceURL = [DocProcess getBaseURLForService:URL.absoluteString];

        if (![Service compareURL:serviceURL toURL:restricted.serviceURL]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_RESTRICTED", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];

            [self.buttonPrint setEnabled:YES];
            return;
        }
    }

    // Check if the printer requires authentication
    if ([printer requiresAuthToPrint]) {
        UserAccount *userAccount = [UserAccount getUserAccountForURL:[printer getDocAPIAddress]];
        if (userAccount && [userAccount.isAnonymous boolValue]) {
            ExtAuthViewController *destViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"printerUserAuth"];
            destViewController.delegate = self;

            [self pushToViewController:destViewController fromViewController:self];
            [self.buttonPrint setEnabled:YES];
            return;
        }
    }

    // Check if the printer has any job accounting inputs to be entered
    if ([JobAccountingInputs printerRequiresJobInputs:printer]) {
        if (self.jobInputs == nil) {
            ExtJobInputsViewController *destViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"printerJobInputs"];
            destViewController.delegate = self;
            destViewController.printer = printer;

            [self pushToViewController:destViewController fromViewController:self];
            [self.buttonPrint setEnabled:YES];
            return;
        }
    }

    ExtPrintJobManager *manager = [ExtPrintJobManager new];
    [manager createPrintJob:printer withDocument:self.document withOptions:self.printOptions withInputs:self.jobInputs ? : [JobAccountingInputs generatePopulatedInputs:printer]];

    [self closeExtension];
}

- (void)pushToViewController:(UIViewController *)pushController fromViewController:(UIViewController *)fromController
{
    // Create an empty mutable array that will be used as a new stack of view controllers
    NSMutableArray *newControllers = [NSMutableArray array];
    
    // Go through the stack of current view controllers and add them to the new stack until we get to the base "from"
    // view controller.  Any view controllers between the "from" controller and the "push" controller are removed.
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        [newControllers addObject:vc];
        if ([vc isEqual:fromController]) {
            break;
        }
    }
    
    // Add to the top the view controller we want to transition too
    [newControllers addObject:pushController];
    
    // Perform the transition
    [self.navigationController setViewControllers:newControllers animated:YES];
}

- (void)updatePageLabel
{
    if ([self.document canBePreviewed]) {
        NSString *newLabel = [NSString stringWithFormat:@"%@ %ld / %ld", NSLocalizedPONString(@"LABEL_PAGE", nil), (long)self.viewCarousel.currentItemIndex + 1, (long)self.viewCarousel.numberOfItems];
        [self.labelPages setText:newLabel];
    }
}

- (void)renderDocument
{
    if ([self.document canBePreviewed]) {
        if ([self.document previewIsLocalRendered] || [self.document previewIsServerRendered]) {
            [self.document renderDocumentWithPrintOptions:self.printOptions];
        } else {
            [self.viewCarousel reloadData];
            [self updatePageRange];

            [self.labelNoPreview setHidden:YES];
            [self.viewRender setHidden:YES];
            [self.viewCarousel setHidden:NO];
            [self.labelPages setHidden:NO];
            [self.buttonPrintOptions setEnabled:YES];
        }
    } else {
        [self.labelNoPreview setHidden:NO];
        [self.viewCarousel setHidden:YES];
        [self.labelPages setHidden:YES];
        [self.viewRender setHidden:YES];
        [self.buttonPrintOptions setEnabled:YES];
    }
}

- (void)updatePageRange
{
    // When we re-render make sure we correct any page range issues detected
    if (self.printOptions && [self printOptions][@"poPageRange"] != nil) {
        unsigned long maxPages = [self.document getPageCount];
        if (maxPages == 1) {
            [self.printOptions removeObjectForKey:@"poPageRange"];
        } else {
            NSScanner *scanner = [NSScanner scannerWithString:[self printOptions][@"poPageRange"]];
            NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            int lower;
            [scanner scanInt:&lower];
            int upper;
            [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
            [scanner scanInt:&upper];

            if (upper > maxPages) upper = (int)maxPages;
            if (lower > upper) lower = upper;

            [self.printOptions setValue:[NSString stringWithFormat:@"%d-%d", lower, upper] forKey:@"poPageRange"];
        }
    }
}

- (BOOL)generatePrintOptionsUsingOptions:(NSMutableDictionary *)options
{
    BOOL requiresRender = NO;

    if (options == nil) {
        options = [self.document initialPrintOptions];
    }

    // Printer specific options
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];
    if (printer) {
        // Paper Size
        BOOL mediaSizeExists = options[@"poMediaSizeNum"] != nil;
        int mediaNum = mediaSizeExists ? [PaperSize getMediaSizeNumberFromPrintOptions:options] : [printer getDefaultPaperSizeNum];
        if (mediaNum != -1) {
            if (!mediaSizeExists) [options setValue:[NSString stringWithFormat:@"%d", mediaNum] forKey:@"poMediaSizeNum"];
            PaperSize *tempPaperSize = [PaperSize getPaperSizeFromPrintOptions:options];
            if (!CGSizeEqualToSize(tempPaperSize.size, self.paperSize.size)) {
                requiresRender = YES;
            }
        }

        // Duplex
        if ([printer doesSupportDuplex]) {
            BOOL duplexExists = options[@"poDuplex"] != nil;
            if (!duplexExists) {
                NSString *duplexPermit = [printer.duplexPermit lowercaseString];
                if ([duplexPermit isEqualToString:@"duplex_only"] || [duplexPermit isEqualToString:@"duplex_default"]) {
                    [options setValue:@"DuplexLong" forKey:@"poDuplex"];
                } else if ([duplexPermit isEqualToString:@"simplex_only"] || [duplexPermit isEqualToString:@"simplex_default"]) {
                    [options setValue:@"Simplex" forKey:@"poDuplex"];
                }
            }
        }

        // Color
        if ([printer doesSupportColor]) {
            BOOL colorExists = options[@"poColor"] != nil;
            if (!colorExists) {
                NSString *colorPermit = [printer.colorPermit lowercaseString];
                if ([colorPermit isEqualToString:@"color_only"] || [colorPermit isEqualToString:@"color_default"]) {
                    [options setValue:@"1" forKey:@"poColor"];
                } else if ([colorPermit isEqualToString:@"mono_only"] || [colorPermit isEqualToString:@"mono_default"]) {
                    [options setValue:@"0" forKey:@"poColor"];
                }
            }
        }
    }

    PaperSize *newPaperSize = [PaperSize getPaperSizeFromPrintOptions:options];
    if (!CGSizeEqualToSize(newPaperSize.size, self.paperSize.size)) {
        requiresRender = YES;
    }
    self.paperSize = newPaperSize;
    self.carouselPaperSize = [PaperSize fitPaperSize:self.paperSize toAreaOfSize:[self getCarouselContentSize]];

    self.printOptions = options;

    return requiresRender;
}

- (CGSize)getCarouselContentSize
{
    // Return the maximum content area of the carousel to be rendered into
    float height = self.viewCarousel.frame.size.height - 20.0f;
    float width = self.viewCarousel.frame.size.width * 0.7f;
    return CGSizeMake(width, height);
}

#pragma mark - Setup Core Data

- (void)setupCoreData
{
    // Check if the container app has been launched yet to migrate core data to the shared app group
    NSString *appGroupId = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"APP_GROUP_ID"];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:appGroupId];
    if (![[sharedDefaults stringForKey:@"migratedCoreData"] isEqualToString:@"YES"]) {
        self.appNeedsLaunch = YES;
        return;
    }

    // Clear the RestKit default store.  We need to do this in the extension because if the host app (IE Safari) you launch the extension from stays open
    // the previous default store will be maintained in memory and will cause syncing issues when you launch a new copy of the extension in the same app.
    [RKManagedObjectStore setDefaultStore:nil];

    [CoreDataManager setupCoreData];
}

#pragma mark - Setup RestKit

- (void)setupRestKit
{
    #ifdef DEBUG
        // Set RestKit logging in Debug builds
        RKLogConfigureByName("*", RKLogLevelError);
    #else
        // Disable RestKit logging in Release builds
        RKLogConfigureByName("*", RKLogLevelOff);
    #endif

    // Setup RestKit to use RKXMLReaderSerialization for parsing "text/xml" responses
    [RKMIMETypeSerialization registerClass:[RKXMLReaderSerialization class] forMIMEType:@"text/xml"];
}

#pragma mark - Setup Managers

- (void)setupManagers
{
    // Setup the Wormhole Manager for IPC
    [WormholeManager sharedWormholeManager];

    // Set the bundle identifier to load image resources from
    [[ImageManager sharedImageManager] setBundleIdentifier:kImagesBundleIdentifier];
}

#pragma mark - iCarouselDataSource

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.document getPageCount];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    //Create new view if no view is available for recycling
    if (view == nil) {
        view = [[PreviewPageView alloc] initWithFrame:self.carouselPaperSize];
    }
    
    if ([view isMemberOfClass:[PreviewPageView class]]) {
        // Get the preview image for the page and set it
        PreviewPageView *previewPage = (PreviewPageView *)view;
        UIImage *newImage = [self.document getPagePreviewAtIndex:index withSize:view.bounds.size forPaperSize:self.paperSize.size];
        [previewPage.pageImage setImage:newImage];

        // Show/hide the activity indicator
        if (newImage) {
            [previewPage stopActivityIndicator];
        } else {
            [previewPage startActivityIndicator];
        }
    }
    
    return view;
}

#pragma mark - iCarouselDelegate

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    [self updatePageLabel];
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing) {
        return value * 1.05f;
    }
    return value;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    return self.carouselPaperSize.size.width;
}

#pragma mark - PrintDocumentDelegate

- (void)didStartRendering
{
    [self.buttonPrintOptions setEnabled:NO];
    [self.buttonBarPrinter setUserInteractionEnabled:NO];
    [self.buttonBarInfo setUserInteractionEnabled:NO];
    [self.buttonPrint setEnabled:NO];

    [self.labelNoPreview setHidden:YES];
    [self.viewCarousel setHidden:YES];
    [self.labelPages setHidden:YES];
    [self.viewRender setHidden:NO];
}

- (void)didFinishRendering
{
    BOOL preview = [self.document getPageCount] > 0;

    [self.viewCarousel reloadData];
    [self updatePageRange];
    [self updatePageLabel];

    [self.viewRender setHidden:YES];
    [self.labelNoPreview setHidden:preview];
    [self.viewCarousel setHidden:!preview];
    [self.labelPages setHidden:!preview];

    [self.buttonPrintOptions setEnabled:YES];
    [self.buttonBarPrinter setUserInteractionEnabled:YES];
    [self.buttonBarInfo setUserInteractionEnabled:YES];
    [self.buttonPrint setEnabled:YES];
}

- (void)didStartPreviewing
{
    [self.buttonPrintOptions setEnabled:YES];
    [self.buttonBarPrinter setUserInteractionEnabled:YES];
    [self.buttonBarInfo setUserInteractionEnabled:YES];
    [self.buttonPrint setEnabled:YES];

    [self.labelNoPreview setHidden:YES];
    [self.viewCarousel setHidden:YES];
    [self.labelPages setHidden:YES];
    [self.viewRender setHidden:NO];
}

- (void)didFinishPreviewing
{
    BOOL preview = [self.document getPageCount] > 0;
    
    [self.viewCarousel reloadData];
    [self updatePageRange];
    [self updatePageLabel];
    
    [self.viewRender setHidden:YES];
    [self.labelNoPreview setHidden:preview];
    [self.viewCarousel setHidden:!preview];
    [self.labelPages setHidden:!preview];
}

- (void)setItemImage:(UIImage *)image atIndex:(NSInteger)index
{
    UIView *view = [self.viewCarousel itemViewAtIndex:index];
    
    if ([view isMemberOfClass:[PreviewPageView class]]) {
        PreviewPageView *previewPage = (PreviewPageView *)view;
        [previewPage.pageImage setImage:image];
    }
}

- (void)startActivityAtIndex:(NSInteger)index
{
    UIView *view = [self.viewCarousel itemViewAtIndex:index];
    
    if ([view isMemberOfClass:[PreviewPageView class]]) {
        PreviewPageView *previewPage = (PreviewPageView *)view;
        [previewPage startActivityIndicator];
    }
}

- (void)endActivityAtIndex:(NSInteger)index
{
    UIView *view = [self.viewCarousel itemViewAtIndex:index];
    
    if ([view isMemberOfClass:[PreviewPageView class]]) {
        PreviewPageView *previewPage = (PreviewPageView *)view;
        [previewPage stopActivityIndicator];
    }
}

#pragma mark - JobAccountingInputsDelegate

- (void)didFinishWithJobInputs:(NSDictionary *)inputs
{
    self.jobInputs = inputs;
    [self printDocument];
}

#pragma mark - UserAccountDelegate

- (void)didFinishWithUserAccount:(UserAccount *)account
{
    Printer *printer = [Printer getSingletonPrinterForEntity:@"SelectedPrinter"];

    // Get the service URL that is used for submitting documents to
    NSURL *serviceURL = [printer getDocAPIAddress];
    NSString *URL = [DocProcess getBaseURLForService:serviceURL.absoluteString];

    // Get the Service if it already exists or create it
    if ([Service isHostedURL:serviceURL andPublic:YES]) {
        [UserAccount removeHostedDefault:nil];
        [account setHostedDefault:nil];
    } else {
        NSArray *results = [Service doesServiceExist:URL inContext:nil];
        Service *service = (results && ([results count] == 0)) ? [Service createService:URL description:nil isDefault:NO isLocked:NO isMDM:NO isRestricted:NO completionBlock:nil] : results[0];
        
        // Add the UserAccount to the Service as a default
        NSMutableSet *services = [NSMutableSet setWithSet:account.serviceDefaults];
        if (![services containsObject:service]) {
            [services addObject:service];
            [account setServiceDefaults:services inContext:nil];
        }
    }

    [self printDocument];
}

#pragma mark - PrintOptionsDelegate

- (void)didFinishWithOptions:(NSMutableDictionary *)options
{
    self.carouselPaperSize = [PaperSize fitPaperSize:self.paperSize toAreaOfSize:[self getCarouselContentSize]];
    if ([self generatePrintOptionsUsingOptions:options]) {
        [self renderDocument];
    } else {
        [self.viewCarousel reloadData];
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self didStartRendering];
    self.safariURL = nil;
    self.safariBaseURI = nil;
    self.safariContent = nil;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(timeoutWebView:) object:webView];
    [self performSelector:@selector(timeoutWebView:) withObject:webView afterDelay:1.0f];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(timeoutWebView:) object:webView];
    [self setDocument:[[WebDocument alloc] initWithWebView:webView]];
}

- (void)timeoutWebView:(UIWebView *)webView {
    [self setDocument:[[WebDocument alloc] initWithWebView:webView]];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"printOptions"]) {
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        ExtOptionsViewController *optionsController = (ExtOptionsViewController *)navController.topViewController;
        optionsController.document = self.document;
        optionsController.printOptions = self.printOptions;
        optionsController.delegate = self;
    } else if ([segue.identifier isEqualToString:@"printerDetails"]) {
        [Printer setSingletonPrinter:[Printer getSingletonPrinterForEntity:@"SelectedPrinter"] forEntity:@"ExtensionPrinter"];
    }
}

@end
