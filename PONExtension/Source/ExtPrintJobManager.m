//
//  ExtPrintJobManager.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-14.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtPrintJobManager.h"

#import "DocProcess.h"
#import "JobAccountingInputs.h"
#import "OAuth2Manager.h"
#import "PrintDocument.h"
#import "PrintJob.h"
#import "Printer.h"
#import "Service.h"

#import <AFNetworking/AFNetworking.h>
#import <XMLReader/XMLReader.h>

@interface ExtPrintJobManager ()

@property (nonatomic, strong) AFURLSessionManager *session;
@property (nonatomic, strong) NSURLSessionTask *task;
@property (nonatomic, strong) NSMutableData *response;

@end

@implementation ExtPrintJobManager

- (void)createPrintJob:(Printer *)printer
          withDocument:(PrintDocument *)document
           withOptions:(NSDictionary *)options
            withInputs:(NSDictionary *)inputs
{
    // Create a temporary child context so that we can clone the printer and create the print job in one transaction that will not be
    // affected by other saves to the main context.
    NSManagedObjectContext *tempObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:YES];
    tempObjectContext.undoManager = nil;

    // Clone the printer as a JobPrinter
    Printer *jobPrinter = [printer clonePrinterInContext:tempObjectContext forEntityName:@"JobPrinter" updateExisting:YES];

    // Create the PrintJob and set it's properties
    PrintJob *printJob = [NSEntityDescription insertNewObjectForEntityForName:@"PrintJob" inManagedObjectContext:tempObjectContext];
    printJob.printer = jobPrinter;
    NSString *jobUUID = [self generateJobUUID];
    printJob.jobUUID = jobUUID;
    printJob.jobStatus = @(JobStatusCodeCreated);
    printJob.dateTime = [NSDate date];
    printJob.docType = [self setDocumentType:document];
    printJob.docData = [self setDocumentData:document];
    [self setJobInputs:printJob withInputs:inputs];
    printJob.printOptions = options;
    [self prepareDocument:printJob withURL:[document getDocumentURL] previewPage:document.previewPage];

    // Save the temporary context to the persistent store
    [tempObjectContext saveToPersistentStore:nil];

    // Start the upload
    [self uploadPrintJob:jobUUID];
}

- (NSString *)generateJobUUID
{
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    return (__bridge_transfer NSString *)string;
}

- (NSNumber *)setDocumentType:(PrintDocument *)document
{
    return @([document getDocumentType]);
}

- (NSMutableDictionary *)setDocumentData:(PrintDocument *)document
{
    return [document getDocumentData];;
}

- (void)setJobInputs:(PrintJob *)job withInputs:(NSDictionary *)inputs
{
    JobAccountingInputs *accounting = job.printer.jobAccountingInputs;

    if (!inputs || !accounting) return;

    job.clientInput = [job storeJobInput:inputs[kJobInputTypeClientUIDKey]];
    job.emailInput = [job storeJobInput:inputs[kJobInputTypeEmailAddressKey]];
    job.networkInput = [job storeJobInput:inputs[kJobInputTypeNetworkLoginKey]];
    job.sessionInput = [job storeJobInput:inputs[kJobInputTypeSessionMetadataKey]];
    job.releaseInput = inputs[kJobInputTypeReleaseCodeKey];
}

- (void)prepareDocument:(PrintJob *)job
                withURL:(NSURL *)url
            previewPage:(UIImage *)image
{
    NSFileManager *fileManager = [NSFileManager defaultManager];

    // Check to see if the Jobs directory exists, create it if it doesn't
    NSString *appGroupId = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"APP_GROUP_ID"];
    NSURL *groupURL = [fileManager containerURLForSecurityApplicationGroupIdentifier:appGroupId];
    NSString *jobsPath = [[[groupURL relativePath] stringByAppendingPathComponent:@"Jobs"] stringByAppendingPathComponent:job.jobUUID];
    BOOL isDir;
    BOOL exist = [fileManager fileExistsAtPath:jobsPath isDirectory:&isDir];

    if (!exist || (exist && !isDir)) {
        NSError *error;
        if (![fileManager createDirectoryAtPath:jobsPath withIntermediateDirectories:YES attributes:nil error:&error]) {
            job.errorText = error ? [NSString stringWithFormat:@"%@  %@: %ld.", NSLocalizedPONString(@"ERROR_DIRCREATE", nil), error.domain, (long)error.code] : NSLocalizedPONString(@"ERROR_DIRCREATE", nil);
            [self updatePrintJob:job withStatus:JobStatusCodeFailed];
            return;
        }
    }

    // Copy or move the document to the Jobs directory
    NSURL *destURL = [NSURL fileURLWithPath:[jobsPath stringByAppendingPathComponent:[url lastPathComponent]]];
    if (![fileManager fileExistsAtPath:[destURL path]]) {
        NSError *error;
        BOOL success = [fileManager copyItemAtPath:[url path] toPath:[destURL path] error:&error];
        if (!success) {
            job.errorText = error ? [NSString stringWithFormat:@"%@  %@: %ld.", NSLocalizedPONString(@"ERROR_COPYDOC", nil), error.domain, (long)error.code] : NSLocalizedPONString(@"ERROR_COPYDOC", nil);
            [self updatePrintJob:job withStatus:JobStatusCodeFailed];
            return;
        }
    }

    // Save the preview image to this print jobs directory
    NSString *previewPath = [NSString stringWithFormat:@"%@/preview.jpg", [destURL URLByDeletingLastPathComponent].path];
    NSData *data = UIImageJPEGRepresentation(image, 0.85);
    [data writeToFile:previewPath atomically:YES];

    // Everything was successful so we can set the job's final document uri
    job.documentURI = [destURL absoluteString];
}

- (void)updatePrintJob:(PrintJob *)job
            withStatus:(JobStatusCode)code
{
    JobStatusCode oldcode = (JobStatusCode)[job getJobStatusValue];

    if (oldcode != code) {
        job.jobStatus = @((int)code);
            
            // Only write the status change to core data if the value actually changed
        [[job managedObjectContext] saveToPersistentStore:nil];
    }

    if (code == JobStatusCodeFailed || code == JobStatusCodeCancelled) {
        [self finishPrintJob];
        [self cleanupJobFiles:job];
    }
}

- (void)uploadPrintJob:(NSString *)jobUUID
{
    PrintJob *job = [PrintJob fetchPrintJobByUUID:jobUUID inContext:nil];
    JobStatusCode code = (JobStatusCode)[job getJobStatusValue];
    if (code == JobStatusCodeFailed || code == JobStatusCodeCancelled) {
        return;
    }

    self.session = [self prepareBackgroundSession:jobUUID];
    self.session.attemptsToRecreateUploadTasksForBackgroundSessions = YES;

    [[job managedObjectContext] performBlockAndWait:^() {
        [DocProcess prepareUploadInExtension:job withSession:self.session completionHandler:^(NSURLSessionUploadTask *task, NSError *error) {
            if (error) {
                [[job managedObjectContext] performBlockAndWait:^() {
                    job.errorText = [OAuth2Manager isAuthSettingsError:error] ? NSLocalizedPONString(@"ERROR_CHECK_AUTHENTICATION", nil) : error.localizedDescription;
                    [self updatePrintJob:job withStatus:JobStatusCodeFailed];
                }];
            } else {
                self.task = task;
                [[job managedObjectContext] performBlockAndWait:^() {
                    [self updatePrintJob:job withStatus:JobStatusCodeUploadExtension];
                }];
                [self.task resume];
            }
        }];
    }];
}

- (AFURLSessionManager *)prepareBackgroundSession:(NSString *)jobUUID
{
    // Create a background session configuration and manager based on the unique job UUID
    NSString *appGroupId = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"APP_GROUP_ID"];
    NSString *identifier = [NSString stringWithFormat:@"%@.%@.upload", appGroupId, jobUUID];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];

    // Set the shared container app group identifier
    configuration.sharedContainerIdentifier = appGroupId;

    // Create the session with the above configuration
    AFURLSessionManager *session = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    session.session.sessionDescription = identifier;

    // Set the session managers authentication challenge response
    [session setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential *__autoreleasing *credential) {
        return [Service validateServerTrust:challenge withCredentials:credential];
    }];

    // Set the sessions block to run when response data is received
    [session setDataTaskDidReceiveDataBlock:^(NSURLSession *session, NSURLSessionDataTask *dataTask, NSData *data) {
        [self saveResponseData:data fromSession:session];
    }];

    // Set the sessions block to be run when tasks complete
    [session setTaskDidCompleteBlock:^(NSURLSession *session, NSURLSessionTask *task, NSError *error) {
        [self taskDidComplete:task forPrintJob:jobUUID forSession:session error:error];
    }];

    return session;
}

- (void)saveResponseData:(NSData *)data fromSession:(NSURLSession *)session
{
    NSMutableData *responseData = self.response;
    if (!responseData) {
        self.response = [NSMutableData dataWithData:data];
    } else {
        [responseData appendData:data];
    }
}

- (void)taskDidComplete:(NSURLSessionTask *)task forPrintJob:(NSString *)jobUUID forSession:(NSURLSession *)session error:(NSError *)error
{
    self.task = nil;

    PrintJob *job = [PrintJob fetchPrintJobByUUID:jobUUID inContext:nil];

    if (error) {
        [self uploadJobFailure:job error:error];
        return;
    }

    error = [self checkResponseForErrors:task.response];
    if (error) {
        [self uploadJobFailure:job error:error];
        return;
    }

    NSDictionary *response = [XMLReader dictionaryForXMLData:self.response error:&error];
    self.response = nil;
    if (error) {
        [self uploadJobFailure:job error:error];
        return;
    }

    DocProcess *info = [[DocProcess alloc] initWithDictionary:response];
    if (info == nil) {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        [userInfo setValue:[NSString stringWithFormat:@"%@", NSLocalizedPONString(@"Response data could not be parsed", nil)] forKey:NSLocalizedDescriptionKey];
        error = [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorCannotParseResponse userInfo:userInfo];
        [self uploadJobFailure:job error:error];
    } else {
        [self uploadJobSuccess:job withInfo:info];
    }
}

- (NSError *)checkResponseForErrors:(NSURLResponse *)response
{
    NSUInteger statusCode = ([response isKindOfClass:[NSHTTPURLResponse class]]) ? (NSUInteger)[(NSHTTPURLResponse *)response statusCode] : 200;

    if (statusCode < 200 || statusCode > 299) {
        if (statusCode == 401) {
            return [NSError errorWithDomain:@"com.printeron.printeron.AuthStatus" code:-1013 userInfo:nil];
        }

        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        [userInfo setValue:[NSString stringWithFormat:NSLocalizedPONString(@"Expected status code in (200-299), got %d", nil), statusCode] forKey:NSLocalizedDescriptionKey];

        return [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorBadServerResponse userInfo:userInfo];
    }

    NSString *contentType = [response MIMEType] ?: @"application/octet-stream";
    if (![[contentType lowercaseString] hasPrefix:@"text/xml"]) {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        [userInfo setValue:[NSString stringWithFormat:NSLocalizedPONString(@"Expected content type text/xml, got %@", nil),  [response MIMEType]] forKey:NSLocalizedDescriptionKey];

        return [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorCannotDecodeContentData userInfo:userInfo];
    }

    return nil;
}

- (void)uploadJobFailure:(PrintJob *)job error:(NSError *)error
{
    [[job managedObjectContext] performBlock:^() {
        id backgroundKey = [error userInfo][@"NSURLErrorBackgroundTaskCancelledReasonKey"];
        if (backgroundKey && [error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) {
            if ([backgroundKey integerValue] == NSURLErrorCancelledReasonUserForceQuitApplication) {
                job.errorText = @"Cancelled: Application force quit";
            } else if ([backgroundKey integerValue] == NSURLErrorCancelledReasonBackgroundUpdatesDisabled) {
                job.errorText = @"Cancelled: Background updates disabled";
            } else if ([backgroundKey integerValue] == NSURLErrorCancelledReasonInsufficientSystemResources) {
                job.errorText = @"Cancelled: Insufficient system resources";
            }
        } else if ([OAuth2Manager isAuthSettingsError:error]) {
            job.errorText = NSLocalizedPONString(@"ERROR_CHECK_AUTHENTICATION", nil);
        } else {
            job.errorText = error.localizedDescription;
        }

        [self updatePrintJob:job withStatus:JobStatusCodeFailed];
    }];
}

- (void)uploadJobSuccess:(PrintJob *)job withInfo:(DocProcess *)info
{
    [[job managedObjectContext] performBlock:^() {
        // The result is actually an error so send to failure
        if (![info.returnCode isEqualToString:@"0"]) {
            job.errorCode = info.returnCode;
            job.errorText = info.userMessage;
            [self updatePrintJob:job withStatus:JobStatusCodeFailed];
            return;
        }
        
        // A job with jobState 7 (cancelled) or 8 (aborted) are considered cancelled
        int jobState = [info.jobState intValue];
        if (jobState == 7 || jobState == 8) {
            job.errorText = info.userMessage;
            [self updatePrintJob:job withStatus:JobStatusCodeCancelled];
            return;
        }
        
        job.jobState = @([[info jobState] integerValue]);
        job.referenceID = info.jobReferenceID;
        job.releaseCode = info.jobReleaseCode;
        
        [self updatePrintJob:job withStatus:JobStatusCodePending];
    }];
}

- (void)cleanupJobFiles:(PrintJob *)job
{
    // Delete the temporary upload file
    NSURL *file = [NSURL URLWithString:[[job getDocumentPath] stringByAppendingString:@".upload"]];
    NSString *filePath = [file path];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
}

- (void)finishPrintJob
{
    // Invalidate the session so resources are properly deallocated
    if (self.session) {
        [self.session invalidateSessionCancelingTasks:YES];
        self.session = nil;
    }

    self.task = nil;
}

@end
