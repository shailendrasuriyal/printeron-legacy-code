//
//  ExtConstants.m
//  PrinterOn
//
//  Created by Mark Burns on 2013-08-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

NSString* const kImagesBundleIdentifier = @"com.printeron.printeron.PONResources";
NSString* const kAppURLScheme = @"ponapp";
