//
//  ExtPreviewViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-03-24.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtOptionsViewController.h"
#import "iCarousel.h"
#import "JobAccountingInputs.h"
#import "PrintDocument.h"
#import "UserAccount.h"

@class BBCyclingLabel, EdgeInsetLabel, PONButton, ShadowButton;

@interface ExtPreviewViewController : BaseViewController <iCarouselDataSource, iCarouselDelegate, JobAccountingInputsDelegate, PrintDocumentDelegate, PrintOptionsDelegate, UserAccountDelegate, UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrintOptions;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (weak, nonatomic) IBOutlet iCarousel *viewCarousel;

@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet ShadowButton *buttonCancel;
@property (weak, nonatomic) IBOutlet ShadowButton *buttonPrint;

@property (weak, nonatomic) IBOutlet UIButton *buttonBarPrinter;
@property (weak, nonatomic) IBOutlet UIImageView *imageBarPrinter;
@property (weak, nonatomic) IBOutlet UILabel *labelBarTitle;
@property (weak, nonatomic) IBOutlet BBCyclingLabel *labelBarCycling;
@property (weak, nonatomic) IBOutlet UIButton *buttonBarInfo;

@property (weak, nonatomic) IBOutlet UILabel *labelNoPreview;
@property (weak, nonatomic) IBOutlet EdgeInsetLabel *labelPages;

@property (weak, nonatomic) IBOutlet UIView *viewRender;
@property (weak, nonatomic) IBOutlet UILabel *labelRender;

@property (weak, nonatomic) IBOutlet UIView *viewLaunch;
@property (weak, nonatomic) IBOutlet UILabel *labelLaunch;
@property (weak, nonatomic) IBOutlet PONButton *buttonLaunch;

@end
