//
//  ExtDetailsViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-24.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtDetailsViewController.h"

#import "CapabilitiesCell.h"
#import "DirSearch.h"
#import "ExtSelectPrinterViewController.h"
#import "MKMapView+Zoom.h"
#import "OAuth2Manager.h"
#import "PONButton.h"
#import "Printer.h"

#import <RHAddressBook/AddressBook.h>

@interface ExtDetailsViewController ()

@property (nonatomic, strong) Printer *printer;
@property (nonatomic, strong) NSArray *leftCellData;
@property (nonatomic, strong) NSArray *rightCellData;
@property (nonatomic, assign) CLLocationCoordinate2D mapCoordinates;

@property (nonatomic, strong) RHAddressBook *addressBook;
@property (nonatomic, strong) RHGroup *contactGroup;
@property (nonatomic, strong) RHPerson *contactRecord;

@property (nonatomic, strong) NSManagedObjectContext *scratchSearchObjectContext;
@property (nonatomic, strong) RKObjectManager *searchObjectManager;
@property (nonatomic, strong) RKManagedObjectRequestOperation *currentOperation;
@property (nonatomic, assign) BOOL shouldRotateUpdate;
@property (nonatomic, assign) BOOL isUpdateRotating;

@end

@implementation ExtDetailsViewController

- (void)viewDidLoad {
    self.printer = [Printer getSingletonPrinterForEntity:@"ExtensionPrinter"];
    [super viewDidLoad];
    [self.buttonSelect setHidden:[Printer isSingletonPrinter:self.printer forEntity:@"SelectedPrinter"]];
    [self registerForEnterForegroundNotification];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Set all of the printer details
    [self setPrinterDetails];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self cancelCurrentSearch];
}

- (void)customEnterForeground
{
    if (self.shouldRotateUpdate) {
        if (self.currentOperation.isFinished || self.currentOperation.isCancelled) {
            self.shouldRotateUpdate = NO;
        } else {
            [self rotateUpdate];
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RHAddressBookExternalChangeNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_PRINTERDETAILS", nil);
    [self updateSaveButton];
    [self updateContactButton];
    [self.buttonUpdate setTitle:NSLocalizedPONString(@"LABEL_UPDATE", nil) forState:UIControlStateNormal];
    [self.labelLocationDescriptionTitle setText:NSLocalizedPONString(@"TITLE_LOCATION", nil)];
    [self.labelHoursTitle setText:NSLocalizedPONString(@"LABEL_HOURSOPERATION", nil)];
    [self.labelEmailTitle setText:NSLocalizedPONString(@"LABEL_EMAILPRINTADDRESS", nil)];
    [self.labelWebTitle setText:NSLocalizedPONString(@"LABEL_WEBPRINTADDRESS", nil)];
}

- (void)setupTheme
{
    [self.buttonSelect setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.SelectButton.Image"]] forState:UIControlStateNormal];

    self.viewBackground.backgroundColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.BackgroundColor"];
    self.viewButtons.backgroundColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.BackgroundColor"];
    self.buttonContacts.highlightColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.HighlightColor"];
    self.buttonSave.highlightColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.HighlightColor"];
    self.buttonUpdate.highlightColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.HighlightColor"];

    [self.buttonSave setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Buttons.Save.Image"]] forState:UIControlStateNormal];
    [self.buttonContacts setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Buttons.Contact.Image"]] forState:UIControlStateNormal];
    [self.buttonUpdate setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Buttons.Update.Image"]] forState:UIControlStateNormal];

    [self.imagePrinter setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Printer.Image"]]];
    [self.imageEmail setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Email.Image"]]];
    [self.imageWeb setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Web.Image"]]];
}

- (void)setPrinterDetails
{
    // Title View
    [self.labelTitle setText:self.printer.displayName];
    [self.labelSubtitle setText:self.printer.organizationLocationDesc];

    // Pull Printer View
    BOOL pullPrinter = [self.printer.printerClass.lowercaseString isEqualToString:@"pull"];
    if (pullPrinter) {
        self.pullPrinterHeightConstraint.constant = 1000;
        [self.labelPullPrinter setText:NSLocalizedPONString(@"LABEL_PULL_PRINTER_DESC", nil)];
        [self.viewPullPrinter setNeedsLayout];
        [self.viewPullPrinter layoutIfNeeded];
        self.pullPrinterHeightConstraint.constant = self.viewPullPrinter.frame.size.height;
        self.pullPrinterBottomConstraint.constant = 12;
        [self.viewPullPrinter setHidden:NO];
    } else {
        self.pullPrinterHeightConstraint.constant = 0;
        self.pullPrinterBottomConstraint.constant = 0;
        [self.viewPullPrinter setHidden:YES];
    }

    // Location View, don't show if this is a pull printer
    if (pullPrinter) {
        self.locationHeightConstraint.constant = 0;
        self.locationBottomConstraint.constant = 0;
        [self.viewLocation setHidden:YES];
    } else {
        self.locationHeightConstraint.constant = self.viewLocation.frame.size.height;
        self.locationBottomConstraint.constant = 12;
        [self.viewLocation setHidden:NO];

        [self.labelAddressTitle setText:self.printer.addressLine1];
        [self.labelAddress1 setText:[self.printer getAddressLineLabel]];
        [self.labelAddress2 setText:[self.printer getCountryLabel]];

        // Update the map coordinates and place a pin on it for the printer
        self.mapCoordinates = [self.printer getCoordinates];
        if (self.mapCoordinates.latitude == 0.0 && self.mapCoordinates.longitude == 0.0) {
            CLGeocoder *geocoder = [CLGeocoder new];
            __weak ExtDetailsViewController *weakSelf = self;
            [geocoder geocodeAddressString:[NSString stringWithFormat:@"%@, %@, %@", self.printer.addressLine1, [self.printer getAddressLineLabel], [self.printer getCountryLabel]] completionHandler:^(NSArray *placemarks, NSError *error) {
                __strong ExtDetailsViewController *strongSelf = weakSelf;
                if (!strongSelf) return;

                if (error || [placemarks count] == 0) {
                    [strongSelf setMap:strongSelf.mapCoordinates addPin:NO];
                } else if ([placemarks count] > 0) {
                    CLPlacemark *topResult = placemarks[0];
                    strongSelf.mapCoordinates = topResult.location.coordinate;
                    [strongSelf setMap:strongSelf.mapCoordinates addPin:YES];
                }
            }];
        } else {
            [self setMap:self.mapCoordinates addPin:YES];
        }
    }

    // Location Description View
    NSString *locationDescriptionText = [self.printer getLocationDescriptionLabel];
    if ([locationDescriptionText length] > 0) {
        self.locationDescriptionHeightConstraint.constant = 1000;
        [self.labelLocationDescriptionText setText:locationDescriptionText];
        [self.viewLocationDescription setNeedsLayout];
        [self.viewLocationDescription layoutIfNeeded];
        self.locationDescriptionHeightConstraint.constant = self.viewLocationDescription.frame.size.height;
        self.locationDescriptionBottomConstraint.constant = 12;
        [self.viewLocationDescription setHidden:NO];
    } else {
        self.locationDescriptionHeightConstraint.constant = 0;
        self.locationDescriptionBottomConstraint.constant = 0;
        [self.viewLocationDescription setHidden:YES];
    }

    // Hours View
    NSString *hoursText = [self.printer getHoursLabel];
    if ([hoursText length] > 0) {
        self.hoursHeightConstraint.constant = 1000;
        [self.labelHoursText setText:hoursText];
        [self.viewHours setNeedsLayout];
        [self.viewHours layoutIfNeeded];
        self.hoursHeightConstraint.constant = self.viewHours.frame.size.height;
        self.hoursBottomConstraint.constant = 12;
        [self.viewHours setHidden:NO];
    } else {
        self.hoursHeightConstraint.constant = 0;
        self.hoursBottomConstraint.constant = 0;
        [self.viewHours setHidden:YES];
    }

    //Printer View
    [self.labelManufacturer setText:[self.printer getManufacturerLabel]];
    [self.labelModel setText:[self.printer getModelLabel]];
    [self.imageStatus setImage:([self.printer.online intValue] == 1) ? [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Online.Image"]] : [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrinterDetailsScreen.Offline.Image"]]];

    self.leftCellData = @[@{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.Ink.Image"], @"title": NSLocalizedPONString(@"LABEL_INK", nil), @"value": [self.printer getInkTypeLabel]},
                         @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.Duplex.Image"], @"title": NSLocalizedPONString(@"LABEL_DUPLEX", nil), @"value": [self.printer getDuplexModeLabel]},
                         @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.CoverPage.Image"], @"title": NSLocalizedPONString(@"LABEL_COVERPAGE", nil), @"value": [self.printer getCoverPageLabel]},
                         @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.Fees.Image"], @"title": NSLocalizedPONString(@"LABEL_JOBFEE", nil), @"value": [self.printer getJobFeesLabel]}];
    [self.tablePrinterLeft reloadData];

    self.rightCellData = @[@{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.UserCredentials.Image"], @"title": NSLocalizedPONString(@"LABEL_USERCREDENTIALS", nil), @"value": [self.printer getPrintUserCredentialsLabel]},
                          @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.PaperSize.Image"], @"title": NSLocalizedPONString(@"LABEL_PAPERSIZE", nil), @"value": [self.printer getPaperSizesLabel]},
                          @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.PageLimit.Image"], @"title": NSLocalizedPONString(@"LABEL_PAGELIMIT", nil), @"value": [self.printer getPageLimitLabel]},
                          @{@"image": [ThemeLoader stringForKey:@"PrinterDetailsScreen.ReleaseCode.Image"], @"title": NSLocalizedPONString(@"LABEL_RELEASECODE", nil), @"value": [self.printer getReleaseCodeModeLabel]}];
    [self.tablePrinterRight reloadData];

    // Show the email printing address if it is enabled
    NSString *emailAddress = [self.printer getEmailAddressOfType:@"name"];
    if ([emailAddress length] <= 0) {
        emailAddress = [self.printer getEmailAddressOfType:@"num"];
    }

    if ([emailAddress length] > 0) {
        self.emailHeightConstraint.constant = 100;
        [self.labelEmailAddress setText:emailAddress];
        [self.viewEmail setNeedsLayout];
        [self.viewEmail layoutIfNeeded];
        self.emailHeightConstraint.constant = self.viewEmail.frame.size.height;
        self.emailBottomConstraint.constant = 8;
        [self.viewEmail setHidden:NO];
    } else {
        [self.viewEmail setHidden:YES];
        self.emailHeightConstraint.constant = 0;
        self.emailBottomConstraint.constant = 0;
    }

    // Show the web printing address if it is enabled
    NSString *webAddress = [self.printer getWebPortalAddressStripPrefix:YES];
    if ([webAddress length] > 0) {
        self.webHeightConstraint.constant = 100;
        [self.labelWebAddress setText:webAddress];
        [self.viewWeb setNeedsLayout];
        [self.viewWeb layoutIfNeeded];
        self.webHeightConstraint.constant = self.viewWeb.frame.size.height;
        self.webBottomConstraint.constant = 8;
        [self.viewWeb setHidden:NO];
    } else {
        [self.viewWeb setHidden:YES];
        self.webHeightConstraint.constant = 0;
        self.webBottomConstraint.constant = 0;
    }

    if ([self.viewEmail isHidden] && [self.viewWeb isHidden]) {
        [self.viewBottomLine setHidden:YES];
    }
}

- (IBAction)selectPrinter
{
    [Printer setSingletonPrinter:self.printer forEntity:@"SelectedPrinter"];

    UIViewController *backVC = [self backViewController];
    if ([backVC isMemberOfClass:[ExtSelectPrinterViewController class]]) {
        NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];

        // Remove this controller and the one below it
        [controllers removeObjectAtIndex:controllers.count - 1];
        [controllers removeObjectAtIndex:controllers.count - 1];

        [self.navigationController setViewControllers:controllers animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UIViewController *)backViewController {
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;

    if (numberOfViewControllers < 2) {
        return nil;
    } else {
        return [self.navigationController viewControllers][numberOfViewControllers - 2];
    }
}

- (void)setMap:(CLLocationCoordinate2D)coordinates addPin:(BOOL)pin
{
    MKMapSnapshotOptions *options = [MKMapSnapshotOptions new];
    CLLocationCoordinate2D mapCoords = CLLocationCoordinate2DMake(coordinates.latitude + 0.0005, coordinates.longitude);
    options.region = MKCoordinateRegionMake(mapCoords, [MKMapView coordinateSpanWithView:self.imageMap centerCoordinates:mapCoords andZoomLevel:14]);
    options.scale = [UIScreen mainScreen].scale;
    options.size = self.imageMap.frame.size;

    MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
    [snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        if (snapshot == nil) return;

        UIImage *image = snapshot.image;
        
        if (pin) {
            UIImage *pinImage = [[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"Maps.PrinterPin"]];

            // Start to create our final image
            UIGraphicsBeginImageContextWithOptions(image.size, YES, image.scale);

            // Draw the image from the snapshotter
            [image drawAtPoint:CGPointMake(0, 0)];

            // Now draw the map pin
            CGPoint point = [snapshot pointForCoordinate:coordinates];
            point.x = point.x - (pinImage.size.width/2);
            point.y = point.y - (pinImage.size.height);
            [pinImage drawAtPoint:point];

            // Get the final image
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }

        [self.mapIndicator setHidden:YES];
        [self.imageMap setImage:image];
    }];
}

- (IBAction)launchMapApp
{
    MKMapItem *end = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:self.mapCoordinates addressDictionary:[self.printer getAddressDictionary]]];
    [end setName:self.labelTitle.text];

    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)]) {
        [MKMapItem openMapsWithItems:@[end] launchOptions:nil];
    }
}

#pragma mark - Save/Delete Printer Methods

- (void)updateSaveButton
{
    if ([Printer getPrinter:self.printer forEntityName:@"SavedPrinter"]) {
        [self.buttonSave setTitle:NSLocalizedPONString(@"LABEL_DELETE", nil) forState:UIControlStateNormal];
    } else {
        [self.buttonSave setTitle:NSLocalizedPONString(@"LABEL_SAVE", nil) forState:UIControlStateNormal];
    }
}

- (IBAction)saveButtonAction
{
    NSManagedObject *savedPrinter = [Printer getPrinter:self.printer forEntityName:@"SavedPrinter"];

    NSError *error;
    if (savedPrinter) {
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext deleteObject:savedPrinter];
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:&error];
        if (error) NSLog(@"Error deleting printer: %@", error);
    } else {
        [self.printer clonePrinterInContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext forEntityName:@"SavedPrinter" updateExisting:NO];
        [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:&error];
        if (error) NSLog(@"Error saving printer: %@", error);
    }

    [self updateSaveButton];
}

#pragma mark - Add/Remove Contacts Methods

- (void)updateContactButton {
    RHAuthorizationStatus authStatus = [RHAddressBook authorizationStatus];
    if (authStatus == RHAuthorizationStatusAuthorized) {
        if (self.contactRecord == nil) self.contactRecord = [self getContactForPrinter];
        if (self.contactRecord && ![self.contactRecord hasBeenRemoved]) {
            [self.buttonContacts setTitle:NSLocalizedPONString(@"LABEL_REMOVE", nil) forState:UIControlStateNormal];
        } else {
            [self.buttonContacts setTitle:NSLocalizedPONString(@"LABEL_ADD", nil) forState:UIControlStateNormal];
        }
    } else {
        [self.buttonContacts setTitle:NSLocalizedPONString(@"LABEL_ADD", nil) forState:UIControlStateNormal];
    }
}

- (IBAction)contactButtonAction
{
    RHAuthorizationStatus authStatus = [RHAddressBook authorizationStatus];
    if (authStatus == RHAuthorizationStatusAuthorized) {
        if (self.contactRecord) {
            [self removePrinterContact];
        } else {
            [self addPrinterContact];
        }
    } else if (authStatus == RHAuthorizationStatusNotDetermined) {
        // Request authorization
        if (self.addressBook == nil) {
            self.addressBook = [RHAddressBook new];
            [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(addressBookChanged) name:RHAddressBookExternalChangeNotification object:nil];
        }
        
        [self.addressBook requestAuthorizationWithCompletion:^(bool granted, NSError *error) {
            if (granted) {
                self.contactRecord = [self getContactForPrinter];
                if (self.contactRecord == nil) {
                    [self addPrinterContact];
                }
            } else {
                [[NSNotificationCenter defaultCenter] removeObserver:self name:RHAddressBookExternalChangeNotification object:nil];
            }
        }];
    } else if (authStatus == RHAuthorizationStatusDenied || authStatus == RHAuthorizationStatusRestricted) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:NSLocalizedPONString(@"ERROR_CONTACTSDENIED", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)addressBookChanged
{
    [self updateContactButton];
}

- (void)addPrinterContact
{
    if (self.addressBook && self.contactGroup) {
        self.contactRecord = [self.addressBook newPersonInDefaultSource];
        [self updateContactRecord];
        
        [self.contactGroup addMember:self.contactRecord];
        [self.contactGroup save];
        
        [self.addressBook save];
    }
}

- (void)removePrinterContact
{
    if (self.addressBook && self.contactRecord && self.contactGroup) {
        [self.contactGroup removeMember:self.contactRecord];
        [self.contactGroup save];
        self.contactGroup = nil;
        
        [self.contactRecord remove];
        [self.addressBook save];
        self.contactRecord = nil;
    }
}

- (RHPerson *)getContactForPrinter
{
    if (self.addressBook == nil) {
        self.addressBook = [RHAddressBook new];
        [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(addressBookChanged) name:RHAddressBookExternalChangeNotification object:nil];
    }

    if (self.contactGroup == nil) {
        NSArray *groups = [self.addressBook groups];
        NSString *groupName = [NSString stringWithFormat:@"%@ %@", [[NSBundle mainBundle] infoDictionary][@"CFBundleContainerDisplayName"], NSLocalizedPONString(@"TITLE_PRINTERS", nil)];
        for (RHGroup *group in groups) {
            if ([group.name isEqualToString:groupName]) {
                self.contactGroup = group;
                break;
            }
        }

        if (self.contactGroup == nil) {
            self.contactGroup = [self.addressBook newGroupInDefaultSource];
            if (self.contactGroup == nil) return nil;
            
            self.contactGroup.name = groupName;
            [self.contactGroup save];
            [self.addressBook addGroup:self.contactGroup];
            [self.addressBook save];
        }
    }

    NSArray *allContacts = [self.contactGroup members];
    NSString *uniqueNote = [NSString stringWithFormat:@"%@/%@",[[[NSURL URLWithString:self.printer.searchURL] host] lowercaseString], self.printer.printerID];
    for (RHPerson *contact in allContacts) {
        if ([contact.note isEqualToString:uniqueNote]) {
            return contact;
        }
    }

    return nil;
}

- (void)updateContactRecord
{
    self.contactRecord.firstName = self.printer.organizationDisplayName;
    self.contactRecord.lastName = self.printer.organizationLocationDesc;
    self.contactRecord.organization = self.printer.organizationName;
    self.contactRecord.department = self.printer.organizationDept;
    self.contactRecord.note = [NSString stringWithFormat:@"%@/%@",[[[NSURL URLWithString:self.printer.searchURL] host] lowercaseString], self.printer.printerID];

    // Add Physical Address
    if (![self.printer.printerClass.lowercaseString isEqualToString:@"pull"]) {
        RHMutableMultiDictionaryValue *addressMultiValue = [[RHMutableMultiDictionaryValue alloc] initWithType:kABMultiDictionaryPropertyType];
        NSMutableDictionary *addressDict = [NSMutableDictionary dictionary];
        if ([self.printer.addressLine1 length] > 0) {
            [addressDict setValue:self.printer.addressLine1 forKey:(NSString *)kABPersonAddressStreetKey];
        }
        if ([self.printer.addressPostal length] > 0) {
            addressDict[(NSString *)kABPersonAddressZIPKey] = self.printer.addressPostal;
        }
        if ([self.printer.addressCity length] > 0) {
            addressDict[(NSString *)kABPersonAddressCityKey] = self.printer.addressCity;
        }
        NSString *country = [self.printer getCountryLabel];
        if ([country length] > 0) {
            addressDict[(NSString *)kABPersonAddressCountryKey] = country;
        }
        if ([self.printer.addressState length] > 0 && [country caseInsensitiveCompare:self.printer.addressState] != NSOrderedSame) {
            addressDict[(NSString *)kABPersonAddressStateKey] = self.printer.addressState;
        }
        [addressMultiValue addValue:addressDict withLabel:(NSString *)kABHomeLabel];
        self.contactRecord.addresses = addressMultiValue;
    } else {
        self.contactRecord.addresses = nil;
    }

    // Add Email Address
    NSString *emailAddress = [self.printer getEmailAddressOfType:@"name"];
    if ([emailAddress length] > 0) {
        RHMutableMultiStringValue *mutableEmailMultiValue = [[RHMutableMultiStringValue alloc] initWithType:kABMultiStringPropertyType];
        [mutableEmailMultiValue addValue:emailAddress withLabel:(NSString *)kABHomeLabel];
        self.contactRecord.emails = mutableEmailMultiValue;
    } else {
        emailAddress = [self.printer getEmailAddressOfType:@"num"];
        if ([emailAddress length] > 0) {
            RHMutableMultiStringValue *mutableEmailMultiValue = [[RHMutableMultiStringValue alloc] initWithType:kABMultiStringPropertyType];
            [mutableEmailMultiValue addValue:emailAddress withLabel:(NSString *)kABHomeLabel];
            self.contactRecord.emails = mutableEmailMultiValue;
        }
    }

    // Add Web Portal Address
    NSString *webAddress = [self.printer getWebPortalAddressStripPrefix:NO];
    if ([webAddress length] > 0) {
        RHMutableMultiStringValue *mutableURLMultiValue = [[RHMutableMultiStringValue alloc] initWithType:kABMultiStringPropertyType];
        [mutableURLMultiValue addValue:webAddress withLabel:(NSString *)kABHomeLabel];
        self.contactRecord.urls = mutableURLMultiValue;
    }

    [self.contactRecord save];
}

#pragma mark - Update Printer Methods

- (void)setShouldRotateUpdate:(BOOL)shouldRotateUpdate
{
    _shouldRotateUpdate = shouldRotateUpdate;
    [self.buttonUpdate setEnabled:!shouldRotateUpdate];
}

- (void)rotateUpdate
{
    self.shouldRotateUpdate = YES;
    if (!self.isUpdateRotating) {
        [UIView animateWithDuration:0.25f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            self.isUpdateRotating = YES;
            self.buttonUpdate.imageView.transform = CGAffineTransformRotate(self.buttonUpdate.imageView.transform, M_PI_2);
        } completion:^(BOOL finished){
            self.isUpdateRotating = NO;
            if (finished && self.shouldRotateUpdate) {
                [self rotateUpdate];
            }
        }];
    }
}

- (IBAction)updateAction
{
    [self.buttonUpdate setEnabled:NO];

    // Setup a scratch context in Core Data to use for DirSearch
    if (self.scratchSearchObjectContext == nil) {
        self.scratchSearchObjectContext = [[RKManagedObjectStore defaultStore] newChildManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType tracksChanges:NO];
        self.scratchSearchObjectContext.undoManager = nil;
    }

    // Setup an object manager to use for DirSearch
    if (self.searchObjectManager == nil) {
        // Get the search service URL for the printer
        NSURL *serviceURL = [NSURL URLWithString:self.printer.searchURL];
        if ([[[serviceURL lastPathComponent] lowercaseString] isEqualToString:@"dirsearch"]) {
            serviceURL = [serviceURL URLByDeletingLastPathComponent];
        }
        NSString *URL = [serviceURL absoluteString];

        self.searchObjectManager = [DirSearch setupSearchForEntity:@"QRPrinter" withService:URL];
    }

    [self performSearch];
}

- (void)performSearch {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                        @"searchType": @"searchByPrinterNum",
                                        @"searchPrinterNum": self.printer.printerID ?: @"",
                                        @"showChildren": @"1",
                                        @"maxResults": @"1",
                                    }];

    // Cancel the previous search if it is still running
    [self cancelCurrentSearch];

    // Clear previous results
    [self.scratchSearchObjectContext reset];

    [self rotateUpdate];

    __weak ExtDetailsViewController *weakSelf = self;
    [DirSearch createSearchWithParameters:params objectManager:self.searchObjectManager managedObjectContext:self.scratchSearchObjectContext success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        __strong ExtDetailsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchSuccess:mappingResult];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        __strong ExtDetailsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        [strongSelf searchFailure:error];
    } completionHandler:^(RKManagedObjectRequestOperation *operation, NSError *error) {
        __strong ExtDetailsViewController *strongSelf = weakSelf;
        if (!strongSelf) return;

        if (error) {
            [strongSelf searchFailure:error];
        } else {
            strongSelf.currentOperation = operation;
            strongSelf.currentOperation.savesToPersistentStore = NO;
            [[RKObjectManager sharedManager] enqueueObjectRequestOperation:strongSelf.currentOperation];
        }
    }];
}

- (void)cancelCurrentSearch {
    if (self.currentOperation) [self.currentOperation cancel];
}

- (void)searchSuccess:(RKMappingResult *)mappingResult {
    for (id value in [mappingResult array]) {
        if ([value isMemberOfClass:[DirSearch class]]) {
            DirSearch *info = value;

            // The result is actually an error so send to failure
            if (![info.returnCode isEqualToString:@"0"]) {
                NSString *errorText = info.errText ? info.errText : info.returnCode;
                NSError *error = [[NSError alloc] initWithDomain:@"DirSearch" code:-999 userInfo:@{ NSLocalizedDescriptionKey : errorText ?: @""}];
                [self searchFailure:error];
                return;
            }

            if (info.resultCount > 0) {
                [Printer connectRelationshipsFromMapping:mappingResult.dictionary inContext:self.scratchSearchObjectContext];

                for (id printer in [mappingResult array]) {
                    if ([printer isKindOfClass:[Printer class]] && [printer isKindOfClass:[NSManagedObject class]]) {
                        NSManagedObject *managedObject = (NSManagedObject *)printer;
                        if ([managedObject.entity.name isEqualToString:@"ParentPrinter"]) continue;

                        Printer *updatedPrinter = printer;
                        [self.printer updateWithPrinter:updatedPrinter];
                        [Printer setSingletonPrinter:updatedPrinter forEntity:@"ExtensionPrinter"];
                        self.printer = [Printer getSingletonPrinterForEntity:@"ExtensionPrinter"];

                        // Update the contact record in address book if it exists
                        RHAuthorizationStatus authStatus = [RHAddressBook authorizationStatus];
                        if (authStatus == RHAuthorizationStatusAuthorized) {
                            if (self.contactRecord == nil) self.contactRecord = [self getContactForPrinter];
                            if (self.contactRecord && ![self.contactRecord hasBeenRemoved]) {
                                [self updateContactRecord];
                            }
                        }

                        [self setPrinterDetails];
                        break;
                    }
                }
            }

            break;
        }
    }

    // Display success message
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedPONString(@"LABEL_PRINTER_UPDATED", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];

    self.shouldRotateUpdate = NO;
}

- (void)searchFailure:(NSError *)error {
    // Absorb the errors given when cancelling a job so they don't trigger the code after
    if (([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) ||
        ([error.domain isEqualToString:@"org.restkit.RestKit.ErrorDomain"] && error.code == 2)) return;

    // Display the error message
    if ([OAuth2Manager isAuthSettingsError:error]) {
        [OAuth2Manager showAuthSettingsError:error overController:self.navigationController];
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedPONString(@"LABEL_ERROR", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedPONString(@"LABEL_OK", nil) style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }

    self.shouldRotateUpdate = NO;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tablePrinterLeft]) {
        return [self.leftCellData count];
    } else if ([tableView isEqual:self.tablePrinterRight]) {
        return [self.rightCellData count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"capabilitiesCell";
    CapabilitiesCell *cell = [self.tablePrinterLeft dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    NSDictionary *data;
    if (tableView == self.tablePrinterLeft) {
        data = [self leftCellData][indexPath.row];
    } else if (tableView == self.tablePrinterRight) {
        data = [self rightCellData][indexPath.row];
    }

    [cell setupCell:data];

    return cell;
}

@end
