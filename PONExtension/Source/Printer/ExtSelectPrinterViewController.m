//
//  ExtSelectPrinterViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-29.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtSelectPrinterViewController.h"

@implementation ExtSelectPrinterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_SELECT_PRINTER", nil);
}

- (void)setupTheme
{
    // Set the TabBar appearance
    [self.tabBar setTintColor:[ThemeLoader colorForKey:@"PrintersScreen.Tabbar.SelectedImageColor"]];
    [self.tabBar setBarTintColor:[ThemeLoader colorForKey:@"PrintersScreen.Tabbar.BackgroundColor"]];

    NSShadow *shadow = [NSShadow new];
    shadow.shadowColor = [UIColor darkGrayColor];
    shadow.shadowOffset = CGSizeMake(0.5, 0.5);

    for (UITabBarItem *item in self.tabBar.items) {
        [item setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:11.0], NSForegroundColorAttributeName: [ThemeLoader colorForKey:@"PrintersScreen.Tabbar.SelectedTextColor"], NSShadowAttributeName: shadow} forState:UIControlStateSelected];
        [item setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:11.0], NSForegroundColorAttributeName: [ThemeLoader colorForKey:@"PrintersScreen.Tabbar.NormalTextColor"], NSShadowAttributeName: shadow} forState:UIControlStateNormal];
    }

    self.tabBar.layer.shadowOpacity = 0.75f;
    self.tabBar.layer.shadowRadius = 1.5f;
    self.tabBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabBar.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);
}

@end
