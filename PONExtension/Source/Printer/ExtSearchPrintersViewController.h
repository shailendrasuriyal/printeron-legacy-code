//
//  ExtSearchPrintersViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-30.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface ExtSearchPrintersViewController : BaseViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;

@end
