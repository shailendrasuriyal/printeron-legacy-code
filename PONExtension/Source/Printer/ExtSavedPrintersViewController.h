//
//  ExtSavedPrintersViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-29.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@interface ExtSavedPrintersViewController : BaseViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;

@end
