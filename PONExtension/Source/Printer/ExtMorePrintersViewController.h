//
//  ExtMorePrintersViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-30.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@class PONButton;

@interface ExtMorePrintersViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *viewBackground;

@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imageLocation;
@property (weak, nonatomic) IBOutlet UILabel *labelLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imageNetwork;
@property (weak, nonatomic) IBOutlet UILabel *labelNetwork;
@property (weak, nonatomic) IBOutlet UIImageView *imageQR;
@property (weak, nonatomic) IBOutlet UILabel *labelQR;
@property (weak, nonatomic) IBOutlet PONButton *buttonLaunch;

@end
