//
//  ExtDetailsViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-24.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@class PONButton;

@interface ExtDetailsViewController : BaseViewController <UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *viewBackground;

@property (weak, nonatomic) IBOutlet ShadowView *viewTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;

@property (weak, nonatomic) IBOutlet ShadowView *viewButtons;
@property (weak, nonatomic) IBOutlet PONButton *buttonSave;
@property (weak, nonatomic) IBOutlet PONButton *buttonContacts;
@property (weak, nonatomic) IBOutlet PONButton *buttonUpdate;

@property (weak, nonatomic) IBOutlet ShadowView *viewPullPrinter;
@property (weak, nonatomic) IBOutlet UILabel *labelPullPrinter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pullPrinterHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pullPrinterBottomConstraint;

@property (weak, nonatomic) IBOutlet ShadowView *viewLocation;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mapIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *imageMap;
@property (weak, nonatomic) IBOutlet UILabel *labelAddressTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress1;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationBottomConstraint;

@property (weak, nonatomic) IBOutlet ShadowView *viewLocationDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelLocationDescriptionTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelLocationDescriptionText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationDescriptionHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationDescriptionBottomConstraint;

@property (weak, nonatomic) IBOutlet ShadowView *viewHours;
@property (weak, nonatomic) IBOutlet UILabel *labelHoursTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelHoursText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hoursHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hoursBottomConstraint;

@property (weak, nonatomic) IBOutlet ShadowView *viewPrinter;
@property (weak, nonatomic) IBOutlet UIImageView *imagePrinter;
@property (weak, nonatomic) IBOutlet UILabel *labelManufacturer;
@property (weak, nonatomic) IBOutlet UILabel *labelModel;
@property (weak, nonatomic) IBOutlet UIImageView *imageStatus;
@property (weak, nonatomic) IBOutlet UITableView *tablePrinterLeft;
@property (weak, nonatomic) IBOutlet UITableView *tablePrinterRight;
@property (weak, nonatomic) IBOutlet UIView *viewBottomLine;

@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imageEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelEmailTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelEmailAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *viewWeb;
@property (weak, nonatomic) IBOutlet UIImageView *imageWeb;
@property (weak, nonatomic) IBOutlet UILabel *labelWebTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelWebAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webBottomConstraint;

@property (weak, nonatomic) IBOutlet UIButton *buttonSelect;

@end
