//
//  ExtMorePrintersViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-30.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtMorePrintersViewController.h"

#import "ExtConstants.h"
#import "PONButton.h"

@implementation ExtMorePrintersViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) {
        [self setTitle:NSLocalizedPONString(@"TITLE_MORE", nil)];
        [[self tabBarItem] setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.More.Image"]]];

        if ([ThemeLoader boolForKey:@"PrintersScreen.Tabbar.UseOffImage"]) {
            [[self tabBarItem] setImage:[[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.More.Image.Off"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        }
        [[self tabBarItem] setSelectedImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.More.Image"]]];
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupTheme
{
    self.viewBackground.backgroundColor = [ThemeLoader colorForKey:@"PrintersScreen.BackgroundColor"];

    [self.imageQR setImage:[[ImageManager sharedImageManager] imageNamed:@"QRWhiteIcon"]];
    self.imageQR.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageQR.layer.shadowOffset = CGSizeMake(0, 1);
    self.imageQR.layer.shadowOpacity = 1;
    self.imageQR.layer.shadowRadius = 2.0;
    self.imageQR.clipsToBounds = NO;

    [self.imageLocation setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Location.Image"]]];
    self.imageLocation.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageLocation.layer.shadowOffset = CGSizeMake(0, 1);
    self.imageLocation.layer.shadowOpacity = 1;
    self.imageLocation.layer.shadowRadius = 2.0;
    self.imageLocation.clipsToBounds = NO;

    [self.imageNetwork setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"PrintersScreen.Tabbar.Network.Image"]]];
    self.imageNetwork.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageNetwork.layer.shadowOffset = CGSizeMake(0, 1);
    self.imageNetwork.layer.shadowOpacity = 1;
    self.imageNetwork.layer.shadowRadius = 2.0;
    self.imageNetwork.clipsToBounds = NO;

    // Setup the launch button
    self.buttonLaunch.backgroundColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.BackgroundColor"];
    self.buttonLaunch.originalBackgroundColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.BackgroundColor"];
    self.buttonLaunch.highlightColor = [ThemeLoader colorForKey:@"PrinterDetailsScreen.Buttons.HighlightColor"];
    self.buttonLaunch.layer.cornerRadius = 4.0f;
    [[self.buttonLaunch imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [self.buttonLaunch setImage:[[self roundCorneredImage:[[ImageManager sharedImageManager] imageNamed:@"AppIcon"] radius:13.0f] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
}

- (void)setupLocalizations
{
    [self.buttonLaunch setTitle:NSLocalizedPONString(@"LABEL_FIND_MORE_PRINTERS", nil) forState:UIControlStateNormal];
    [self.labelDescription setText:NSLocalizedPONString(@"LABEL_MOREPRINTERS_DESC", nil)];
    [self.labelQR setText:NSLocalizedPONString(@"LABEL_MOREPRINTERS_SCANQR", nil)];
    [self.labelLocation setText:NSLocalizedPONString(@"LABEL_MOREPRINTERS_LOCATION", nil)];
    [self.labelNetwork setText:NSLocalizedPONString(@"LABEL_MOREPRINTERS_NETWORK", nil)];
}

- (IBAction)launchApp
{
    UIResponder* responder = self;
    while ((responder = [responder nextResponder]) != nil) {
        if([responder respondsToSelector:@selector(openURL:)] == YES) {
            NSString *openURL = [NSString stringWithFormat:@"%@://?moreprinters=1", kAppURLScheme];
            [responder performSelector:@selector(openURL:) withObject:[NSURL URLWithString:openURL]];
        }
    }
}

- (UIImage*)roundCorneredImage: (UIImage*) orig radius:(CGFloat) r {
    UIGraphicsBeginImageContextWithOptions(orig.size, NO, 0);
    [[UIBezierPath bezierPathWithRoundedRect:(CGRect){CGPointZero, orig.size} cornerRadius:r] addClip];
    [orig drawInRect:(CGRect){CGPointZero, orig.size}];
    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

@end
