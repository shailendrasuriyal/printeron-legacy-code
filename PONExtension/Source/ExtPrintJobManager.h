//
//  ExtPrintJobManager.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-14.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

@class PrintDocument, Printer;

@interface ExtPrintJobManager : NSObject

- (void)createPrintJob:(Printer *)printer
          withDocument:(PrintDocument *)document
           withOptions:(NSDictionary *)options
            withInputs:(NSDictionary *)inputs;

@end
