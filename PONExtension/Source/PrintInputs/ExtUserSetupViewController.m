//
//  ExtUserSetupViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-13.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "ExtUserSetupViewController.h"

#import "TPKeyboardAvoidingScrollView.h"

@implementation ExtUserSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self validateFields];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textDescription];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textAccount];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textPassword];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textDescription];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textAccount];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.textPassword];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"TITLE_USERSETUP", nil);
    [self.labelDescription setText:NSLocalizedPONString(@"LABEL_DESCRIPTION", nil)];
    [self.textDescription setPlaceholder:NSLocalizedPONString(@"LABEL_OPTIONAL", nil)];
    [self.labelAccount setText:NSLocalizedPONString(@"LABEL_ACCOUNT", nil)];
    [self.textAccount setPlaceholder:NSLocalizedPONString(@"LABEL_REQUIRED", nil)];
    [self.labelPassword setText:NSLocalizedPONString(@"LABEL_PASSWORD", nil)];
    [self.textPassword setPlaceholder:NSLocalizedPONString(@"LABEL_OPTIONAL", nil)];
}

- (void)setupTheme
{
    [self.saveButton setImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.SelectButton.Image"]] forState:UIControlStateNormal];
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"SettingsScreen.BackgroundColor"];
    
    THEME_KEYBOARD(self.textDescription);
    THEME_KEYBOARD(self.textAccount);
    THEME_KEYBOARD(self.textPassword);
}

- (IBAction)saveAccount
{
    [self.backgroundView endEditing:YES];

    UserAccount *userAccount = [UserAccount createUserAccount:self.textAccount.text password:self.textPassword.text description:self.textDescription.text hostedDefault:NO fromMDM:NO defaultServices:[NSMutableSet set]];

    if (self.delegate) {
        [self.delegate didFinishWithUserAccount:userAccount];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)validateFields
{
    [self.saveButton setEnabled:[self areFieldsValid]];
}

- (BOOL)areFieldsValid
{
    if ([self.textAccount.text length] == 0)
        return NO;
    
    if ([self.textDescription.text length] > 0 && [[self.textDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return NO;
    }
    
    return YES;
}

- (void)textChanged:(NSNotification *)notification
{
    [self validateFields];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [self.scrollView textFieldShouldReturn:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *temp = [[textField.text stringByReplacingCharactersInRange:range withString:string] stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    if ([textField.text isEqualToString:temp]) {
        return NO;
    }
    
    return YES;
}

@end
