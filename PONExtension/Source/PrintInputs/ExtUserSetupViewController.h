//
//  ExtUserSetupViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 2015-04-13.
//  Copyright (c) 2015 PrinterOn Inc. All rights reserved.
//

#import "UserAccount.h"

@class TPKeyboardAvoidingScrollView;

@interface ExtUserSetupViewController : BaseViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *accountView;
@property (weak, nonatomic) IBOutlet UILabel *labelAccount;
@property (weak, nonatomic) IBOutlet UITextField *textAccount;
@property (weak, nonatomic) IBOutlet UILabel *labelPassword;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UITextField *textDescription;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (nonatomic, weak) id <UserAccountDelegate> delegate;

@end
