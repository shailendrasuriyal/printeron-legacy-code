//
//  ExtAuthViewController.m
//  PrinterOn
//
//  Created by Mark Burns on 05/27/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "ExtAuthViewController.h"

#import "AccountCell.h"
#import "UserSetupViewController.h"

@interface ExtAuthViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation ExtAuthViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerForEnterForegroundNotification];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Update the table when the view will appear, we must do this here to reload/recreate the fetch controller
    [self updateTableView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    // Remove the fetch controller to save memory when the view disappears
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)customEnterForeground
{
    // Refresh the fetch controller and table
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    [self updateTableView];
}

- (void)setupLocalizations
{
    self.navigationItem.title = NSLocalizedPONString(@"LABEL_USERCREDENTIALS", nil);
    [self.descriptionLabel setText:NSLocalizedPONString(@"LABEL_REQUIRES_PRINTAUTH", nil)];
}

- (void)setupTheme
{
    self.backgroundView.backgroundColor = [ThemeLoader colorForKey:@"PreviewScreen.BackgroundColor"];
}

-(void)viewDidLayoutSubviews
{
    self.tableHeightConstraint.constant = self.tableView.contentSize.height;
    [self.view layoutIfNeeded];
}

- (void)UserAccountSelected:(UserAccount *)account
{
    if (self.delegate) [self.delegate didFinishWithUserAccount:account];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"UserAccount"];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"accountDescription" ascending:YES]];

        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[RKManagedObjectStore defaultStore] mainQueueManagedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        self.fetchedResultsController.delegate = self;

        [self performFetch];
    }
    
    return _fetchedResultsController;
}

- (void)performFetch {
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch request: %@", error);
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self updateTableView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"userAccountCell";
    AccountCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UserAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setupCell:account.accountDescription withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"UserAccounts.User.Image"]] showLock:NO];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AccountCell *headerCell = [self.tableView dequeueReusableCellWithIdentifier:@"createAccountCell"];
    [headerCell setupCell:NSLocalizedPONString(@"LABEL_ADDACCOUNT", nil) withImage:[[ImageManager sharedImageManager] imageNamed:[ThemeLoader stringForKey:@"NavigationBar.AddButton.Dark.Image"]] showLock:NO];
    return headerCell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserAccount *account = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self UserAccountSelected:account];
    return indexPath;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"setupAccount"]) {
        UserSetupViewController *destViewController = segue.destinationViewController;
        destViewController.delegate = self.delegate;
    }
}

@end
