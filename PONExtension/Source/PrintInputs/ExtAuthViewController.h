//
//  ExtAuthViewController.h
//  PrinterOn
//
//  Created by Mark Burns on 05/27/2014.
//  Copyright (c) 2014 PrinterOn Inc. All rights reserved.
//

#import "UserAccount.h"

@interface ExtAuthViewController : BaseViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet ShadowView *descriptionView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet ShadowView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;

@property (nonatomic, weak) id <UserAccountDelegate> delegate;

@end
