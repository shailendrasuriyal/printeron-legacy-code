var PONExtensionJavaScriptClass = function() {};

PONExtensionJavaScriptClass.prototype = {
    run: function(arguments) {
        // Pass the URL, baseURI, and source of the webpage to the extension.
        arguments.completionFunction({"URL": document.URL, "baseURI": getBaseUrl(), "contentHTML": fullHTML()});
    },
    finalize: function(arguments) {
    }
};

fullHTML = function () {
    var r = document.documentElement.innerHTML, t = document.documentElement.attributes, i = 0, l = '',
    d = '<!DOCTYPE ' + document.doctype.name + (document.doctype.publicId ? ' PUBLIC "' + document.doctype.publicId + '"' : '') + (!document.doctype.publicId && document.doctype.systemId ? ' SYSTEM' : '') + (document.doctype.systemId ? ' "' + document.doctype.systemId + '"' : '') + '>';
    for (; i < t.length; i += 1) l += ' ' + t[i].name + '="' + t[i].value + '"';
    return d+'\n<html' + l + '>' + r + '</html>';
}

getBaseUrl = function () {
    var re = new RegExp(/^.*\//);
    return re.exec(window.location.href)+"";
}

// The JavaScript file must contain a global object named "ExtensionPreprocessingJS".
var ExtensionPreprocessingJS = new PONExtensionJavaScriptClass;
