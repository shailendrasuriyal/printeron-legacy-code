//
//  ExtConstants.h
//  PrinterOn
//
//  Created by Mark Burns on 2014-08-18.
//  Copyright (c) 2013 PrinterOn Inc. All rights reserved.
//

FOUNDATION_EXPORT NSString* const kImagesBundleIdentifier;
FOUNDATION_EXPORT NSString* const kAppURLScheme;
